local assets =
{
    Asset("ANIM", "anim/pigsy_sleepbed.zip"),
	Asset("ATLAS", "images/inventoryimages/pigsy_sleepbed.xml"),
}

local function builder_onbuilt(inst, builder)
	if builder then 
        local bed = SpawnPrefab("pigsy_sleepbed_item")
        if bed then
            bed.Transform:SetPosition(builder.Transform:GetWorldPosition())
            local bufferedaction = BufferedAction(builder, bed, ACTIONS.SLEEPIN)
            builder.components.locomotor:PushAction(bufferedaction)
        end
    end
end

local function fn()

    local inst = CreateEntity()

    inst.entity:AddTransform()

    inst:AddTag("CLASSIFIED")

    inst.persists = false

    inst:DoTaskInTime(0, inst.Remove)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.OnBuiltFn = builder_onbuilt
    return inst
end


local function onwake(inst, sleeper, nostatechange)
    if inst.components.finiteuses == nil or inst.components.finiteuses:GetUses() <= 0 then
        if inst.components.stackable ~= nil then
            inst.components.stackable:Get():Remove()
        else
            inst:Remove()
        end
    end
end

local function temperaturetick(inst, sleeper)
    if sleeper.components.temperature ~= nil then
        if inst.sleep_temp_min ~= nil and sleeper.components.temperature:GetCurrent() < inst.sleep_temp_min then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
        elseif inst.sleep_temp_max ~= nil and sleeper.components.temperature:GetCurrent() > inst.sleep_temp_max then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
        end
    end
end

local function sleep_bed()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst:AddTag("no_watchphase")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst:AddComponent("sleepingbag")
    inst.components.sleepingbag.onwake = onwake
    inst.components.sleepingbag.health_tick = 1.5
    inst.components.sleepingbag.sanity_tick = 1.5
    inst.components.sleepingbag.hunger_tick = -1.5
    inst.components.sleepingbag:SetTemperatureTickFn(temperaturetick)

    return inst
end

local function fxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.entity:AddPhysics()
    inst.Physics:ClearCollisionMask()
    inst.Physics:SetSphere(1)
    inst.AnimState:SetBank("pigsy_sleepbed")
    inst.AnimState:SetBuild("pigsy_sleepbed")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetLayer(LAYER_WORLD_BACKGROUND)
	inst.Transform:SetScale(1.2, 1.2, 1.2)
    inst:AddTag("fx")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst.persists = false
    return inst
end

return Prefab("pigsy_sleepbed", fn),
Prefab("pigsy_sleepbed_item", sleep_bed),
Prefab("pigsy_sleepbed_fx", fxfn, assets)
