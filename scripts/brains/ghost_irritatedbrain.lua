require "behaviours/follow"
require "behaviours/wander"

local Ghost_IrritatedBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function IsValid(target)
    return target ~= nil and target:IsValid() and target.entity:IsVisible() and
        target.components.health ~= nil and not target.components.health:IsDead()
end

local function GetCurseTarget(inst)
    if
        not (
            IsValid(inst.cursetarget)
            and not inst.cursetarget:IsInLimbo()
            and inst:GetDistanceSqToInst(inst.cursetarget) <= TUNING.GHOST_FOLLOW_DSQ
        )
    then
        inst.cursetarget = nil
    end

    if inst.cursetarget == nil then
        local gx, gy, gz = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(
            gx, gy, gz, 10,
            {"_combat", "_health"},
            {"INLIMBO", "noauradamage", "veggie", "structure", "wall", "groundspike", "smashable","playerghost"}
        )
        for _, v in ipairs(ents) do
            if IsValid(v) then
                if v:HasTag("player") then
                    inst.cursetarget = v
                    break
                elseif inst.cursetarget == nil then
                    inst.cursetarget = v
                end
            end
        end
    end

    return inst.cursetarget
end

function Ghost_IrritatedBrain:OnStart()
    local root = PriorityNode({
        --优先跟踪自己的仇恨对象
        WhileNode(function() return self.inst.components.combat.target ~= nil end, "FollowMyEnemy",
            Follow(self.inst,
                function() return self.inst.components.combat.target end, --不用管距离，因为距离限制写在combat:SetKeepTargetFunction里
                TUNING.GHOST_RADIUS*.25, TUNING.GHOST_RADIUS*.5, TUNING.GHOST_RADIUS
            )
        ),

        --其次跟踪可跟踪对象
        Follow(self.inst,
            function() return GetCurseTarget(self.inst) end,
            TUNING.GHOST_RADIUS*.25, TUNING.GHOST_RADIUS*.5, TUNING.GHOST_RADIUS
        ),

        --没有跟踪对象就徘徊
        Wander(self.inst)
    }, 1)

    self.bt = BT(self.inst, root)
end

return Ghost_IrritatedBrain
