
local  yama_statuemanager = Class(function(self, inst)
	self.inst = inst
	self.level = 1
	self.members = {}
	self._onremovemember= function(member)
		self:RemoveMember(member)
	end
end
)

function yama_statuemanager:RemoveMember(member)
    self.members[member] = nil
	self:SetNewLevel()
end

function yama_statuemanager:AddMember(member)
	if not self.members[member] then
		self.members[member] = member.level
		self.inst:ListenForEvent("onremove", self._onremovemember, member)
		self:SetNewLevel()
	end
end

function yama_statuemanager:SetNewLevel()
	local old = self.level
	local new = self:CheckLevel()
	if new ~= old then
		self.level = new
		self.inst:PushEvent("yama_statuelevelchanged", new)
	end
end

function yama_statuemanager:IsMaxLevel()
	return self.level >= 3
end

function yama_statuemanager:GetLevel()
	return self.level
end

function yama_statuemanager:CheckLevel()
	local level = 1
	for k, v in pairs(self.members) do
		level = math.max(level,v)
	end
	return level 
end

return yama_statuemanager


