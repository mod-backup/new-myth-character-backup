
--------------------================================================
local madameweb_spiderspawn = Class(function(self, inst)
    self.inst = inst
	self.spiders = {}
	for k = 1, 6 do
		self.spiders[k] = {spider = nil,health = nil}
	end
	inst:WatchWorldState(TheWorld:HasTag("cave") and "startcaveday" or "startday", function()
		self:Spawn()
	end)
	self.health_taskfn = function()
		for i , v in ipairs(self.spiders) do
			if v.health ~= nil and v.health < TUNING.SPIDER_WARRIOR_HEALTH then
				v.health =  v.health + 2
			end
		end
		if not self:GetHealthCount() then
			self:Stop()
		end
	end
end)

function madameweb_spiderspawn:GetCount()
	local num = 0
	for i , v in ipairs(self.spiders) do
		if v.spider ~= nil then
			num =  num + 1
		end
	end
	return num
end

function madameweb_spiderspawn:GetHealthCount()
	for i , v in ipairs(self.spiders) do
		if v.health ~= nil and v.health < TUNING.SPIDER_WARRIOR_HEALTH then
			return true
		end
	end
	return false
end

function madameweb_spiderspawn:Spawn(health)
	if self:GetCount() < 6 then
		for i , v in ipairs(self.spiders) do
			if v.spider == nil then
				self.spiders[i] = {spider = true,health = health or nil}
				break
			end
		end
		self:Count()
	end
end

function madameweb_spiderspawn:CanStore()
	return self:GetCount() < 6
end

function madameweb_spiderspawn:CanTake()
	return self:GetCount() > 0
end

function madameweb_spiderspawn:Stop()
	if self.health_task then
		self.health_task:Cancel()
		self.health_task = nil
	end
end

function madameweb_spiderspawn:Count()
	self.inst.spider_counts:set(self:GetCount())
	if self:GetHealthCount() then
		if not self.health_task then
			self.health_task = self.inst:DoPeriodicTask(1, self.health_taskfn,1)
		end
	end
end

function madameweb_spiderspawn:SpawnSpider(doer,all)
	local inst = self.inst
	if self:GetCount() > 0 then
		inst.AnimState:PlayAnimation("cocoon_large_hit")
		inst.AnimState:PushAnimation("cocoon_large")
		for i, v in ipairs(self.spiders) do
			if v.spider ~= nil then
				local spider = SpawnPrefab("madameweb_spider_warrior")
				if spider then
					local map = TheWorld.Map
					local x, y, z = inst.Transform:GetWorldPosition()
					local offs = FindValidPositionByFan(math.random() * 2 * PI, 1.25, 5, function(offset)
						local x1 = x + offset.x
						local z1 = z + offset.z
						return map:IsPassableAtPoint(x1, 0, z1)
							and not map:IsPointNearHole(Vector3(x1, 0, z1))
					end)
					if offs ~= nil then
						x = x + offs.x
						z = z + offs.z
					end
					spider.Transform:SetPosition(x, 0, z)
					if v.health ~= nil and spider.components.health then
						spider.components.health:SetCurrentHealth(v.health)
					end
					if doer and doer.components.leader ~= nil then
						doer.components.leader:AddFollower(spider)
					end
				end
				self.spiders[i] = {spider = nil,health = nil}
				if not all then
					break
				end
			end
		end
	end
	self:Count()
end

function madameweb_spiderspawn:OnSave()
    return next(self.spiders) ~= nil and { spiders = self.spiders } or nil
end

function madameweb_spiderspawn:OnLoad(data)
    if data.spiders ~= nil then
        self.spiders = data.spiders
        self:Count()
    end
end
return madameweb_spiderspawn