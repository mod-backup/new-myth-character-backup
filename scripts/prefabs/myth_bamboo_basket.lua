local assets =
{
    Asset("ANIM", "anim/myth_bamboo_basket.zip"),
    Asset("ANIM", "anim/myth_bamboo_basket_apricot.zip"),
    Asset("ANIM", "anim/myth_bamboo_basket_laurel.zip"),
    Asset("ANIM", "anim/myth_bamboo_basket_soprano.zip"),
	Asset("ATLAS", "images/inventoryimages/myth_bamboo_basket.xml"),
    Asset("ATLAS", "images/inventoryimages/myth_bamboo_basket_apricot.xml"),
    Asset("ATLAS", "images/inventoryimages/myth_bamboo_basket_laurel.xml"),
    Asset("ATLAS", "images/inventoryimages/myth_bamboo_basket_soprano.xml"),
}

local prefabs =
{
    "ash",
}

local function onequip(inst, owner)
    if  inst.components.myth_itemskin.skin:value() == "apricot" then
        owner.AnimState:OverrideSymbol("swap_body_tall", "myth_bamboo_basket_apricot", "swap_none")
    else
        inst.components.myth_itemskin:OverrideSymbol(owner, "backpack")
        inst.components.myth_itemskin:OverrideSymbol(owner, "swap_body")
    end
    if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end
end

local function onunequip(inst, owner)
    if  inst.components.myth_itemskin.skin:value() == "apricot" then
        owner.AnimState:ClearOverrideSymbol("swap_body_tall")
    else
        owner.AnimState:ClearOverrideSymbol("swap_body")
        owner.AnimState:ClearOverrideSymbol("backpack")
    end
    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end
end

local function onburnt(inst)
    if inst.components.container ~= nil then
        inst.components.container:DropEverything()
        inst.components.container:Close()
    end

    SpawnPrefab("ash").Transform:SetPosition(inst.Transform:GetWorldPosition())

    inst:Remove()
end

local function onignite(inst)
    if inst.components.container ~= nil then
        inst.components.container.canbeopened = false
    end
end

local function onextinguish(inst)
    if inst.components.container ~= nil then
        inst.components.container.canbeopened = true
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_bamboo_basket")
    inst.AnimState:SetBuild("myth_bamboo_basket")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("backpack")

    inst.MiniMapEntity:SetIcon("myth_bamboo_basket.tex")

    inst.foleysound = "dontstarve/movement/foley/backpack"

    MakeInventoryFloatable(inst, "small", 0.2)

    inst:AddTag("fridge")

	inst:AddComponent("myth_itemskin")
	inst.components.myth_itemskin.prefab = "myth_bamboo_basket"
	inst.components.myth_itemskin.character = "myth_yutu"
	inst.components.myth_itemskin:SetData{
        soprano = {
            swap = {build = "myth_bamboo_basket_soprano", folder = "swap_body"},
            icon = {atlas = "myth_bamboo_basket_soprano.xml", image = "myth_bamboo_basket_soprano"},
            anim = {bank = "myth_bamboo_basket_soprano", build = "myth_bamboo_basket_soprano", anim = "idle"},
        },
		apricot = {
            swap = {build = "myth_bamboo_basket_apricot", folder = "swap_body_tall"},
            icon = {atlas = "myth_bamboo_basket_apricot.xml", image = "myth_bamboo_basket_apricot"},
            anim = {bank = "myth_bamboo_basket_apricot", build = "myth_bamboo_basket_apricot", anim = "idle"},
        },
		laurel = {
            swap = {build = "myth_bamboo_basket_laurel", folder = "swap_body"},
            icon = {atlas = "myth_bamboo_basket_laurel.xml", image = "myth_bamboo_basket_laurel"},
            anim = {bank = "myth_bamboo_basket_laurel", build = "myth_bamboo_basket_laurel", anim = "idle"},
        },
        default = {
        	swap = {build = "myth_bamboo_basket", folder = "swap_body"},
            icon = {atlas = "myth_bamboo_basket.xml", image = "myth_bamboo_basket"},
            anim = {bank = "myth_bamboo_basket", build = "myth_bamboo_basket", anim = "idle"},
        }
	}

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.cangoincontainer = false
	inst.components.inventoryitem.imagename = "myth_bamboo_basket"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_bamboo_basket.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.restrictedtag = "myth_yutu"
    inst.components.equippable.equipslot = EQUIPSLOTS.BACK or EQUIPSLOTS.PACK or  EQUIPSLOTS.BODY
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("myth_bamboo_basket")
    inst.components.container.skipclosesnd = true
    inst.components.container.skipopensnd = true

    MakeSmallBurnable(inst)
    MakeSmallPropagator(inst)
    inst.components.burnable:SetOnBurntFn(onburnt)
    inst.components.burnable:SetOnIgniteFn(onignite)
    inst.components.burnable:SetOnExtinguishFn(onextinguish)

    MakeHauntableLaunchAndDropFirstItem(inst)

    return inst
end

return Prefab("myth_bamboo_basket", fn, assets, prefabs)
