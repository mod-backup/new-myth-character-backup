
--火眼金睛

local function hungerdelate(inst)
    if (inst.components.health and not inst.components.health:IsDead())
    and (inst.components.hunger and inst.components.hunger.current > 1 )then
		local num  = inst.replica.inventory ~= nil and(
		inst.replica.inventory:EquipHasTag("golden_hat_mk") and 0
		or inst.replica.inventory:EquipHasTag("xzhat_mk") and 0.5 
		or 1)
        inst.components.hunger:DoDelta(-0.6 * num ,true)
    end
end

local function downdown(self)
	--等哪天别人也改了这个我在写兼容吧~~~~~~算了吧下次一定
	self.OnUpdate = function(dt)
        if TheNet:IsServerPaused() then return end
        local anim = "neutral"
        if  self.owner ~= nil and
            (self.owner:HasTag("sleeping") or
            (self.owner:HasTag("fireeye") and not self.owner.replica.inventory:EquipHasTag('golden_hat_mk')) or
            (self.owner:HasTag("skyeye") and not self.owner.replica.inventory:EquipHasTag('yangjian_hair'))) and	           
            self.owner.replica.hunger ~= nil and
            self.owner.replica.hunger:GetPercent() > 0 then
    
            anim = "arrow_loop_decrease"
        end
    
        if self.owner:HasDebuff("wintersfeastbuff") or self.owner:HasDebuff("hungerregenbuff") then
            anim = "arrow_loop_increase"
        end
    
        if self.arrowdir ~= anim then
            self.arrowdir = anim
            self.hungerarrow:GetAnimState():PlayAnimation(anim, true)
        end
    end
end
AddClassPostConstruct( "widgets/hungerbadge", downdown)

local function GetSandstormLevel(inst)
	--if inst.GetSandstormLevel ~= nil then
	--	return inst:GetSandstormLevel()
	if inst.GetStormLevel ~= nil then
		return inst:GetStormLevel()
	end
	return 0
end

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end


local function isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if IsEntityDeadOrGhost(inst,true) then
        return false
    end
    if inst.components.playercontroller and not inst.components.playercontroller:IsEnabled() then 
        return false
    end
    if inst.sg and inst.sg:HasStateTag("busy") then
        return false
    end
    if nofly and IsFlying(inst) then
        return false
    end
    if no_astral and inst._is_player_astral ~= nil and  inst._is_player_astral:value() then
        return false
    end
    if noriding and inst.components.rider ~= nil and inst.components.rider:IsRiding() then
        return false
    end
    if inst.prefab ~= prefab then
        return false
    end
    return true
end

local function Fireeye(inst)
    if not isplayervalid(inst,"monkey_king",nil,true) then
        return 
    end

	if inst.eyecdin then return end

	if GetSandstormLevel(inst) >= TUNING.SANDSTORM_FULL_LEVEL and inst.components.playervision
		and not inst.components.playervision:HasGoggleVision()	 then
			inst.components.talker:Say(STRINGS.MKINSANDSTORM)
		return
	end

	if inst._mknightvision:value() == true then --turn off
		inst._mknightvision:set(false)
		if inst.fireeyetask ~= nil then
			inst.fireeyetask:Cancel()
			inst.fireeyetask = nil
		end
		inst:RemoveTag("fireeye")
	else  --turn on
		inst:SpawnChild("halloween_firepuff_3")
		inst._mknightvision:set(true)
		inst:AddTag("fireeye")
		inst.fireeyetask = inst:DoPeriodicTask(1, hungerdelate, 1)
	end

	inst.eyecdin = true
	inst:DoTaskInTime(0.2, function() inst.eyecdin = false end)
end

local function Changebuild(inst)

    --isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"white_bone",nil,true) then
        return
    end

	if inst.sg:HasStateTag("white_bone_change") then 
        return 
    end

	if not inst._isbeauty then return end
	if not inst._isbeauty:value() then
		inst.sg:GoToState("white_bone_change")
	else
		inst:PushEvent('wb_becomemonster')
	end
end

local function yghungerdelate(inst)
    if (inst.components.health and not inst.components.health:IsDead())
    and (inst.components.hunger and inst.components.hunger.current > 1 )then
		local num  = inst.replica.inventory ~= nil and(
		inst.replica.inventory:EquipHasTag("yangjian_hair") and 0
		or 1)
        inst.components.hunger:DoDelta(-0.8 * num ,true)
    end
end
local function YJSkyeye(inst)
    --isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"yangjian",nil,true) then
        return
    end
	if inst.eyecdin then return end
	if GetSandstormLevel(inst) >= TUNING.SANDSTORM_FULL_LEVEL and inst.components.playervision
		and not inst.components.playervision:HasGoggleVision()	 then
			inst.components.talker:Say(STRINGS.YJINSANDSTORM)
		return
	end
	if inst._skyeye:value() == true then --turn off
		inst._skyeye:set(false)
		if inst.skyeyetask ~= nil then
			inst.skyeyetask:Cancel()
			inst.skyeyetask = nil
		end
		inst:RemoveTag("skyeye")
	else  --turn on
		inst:SpawnChild("yangjian_firebird")
		inst:DoTaskInTime(0.1,function()
			inst._skyeye:set(true)
			inst:AddTag("skyeye")
			inst.skyeyetask = inst:DoPeriodicTask(1, yghungerdelate, 1)
		end)
	end
	inst.eyecdin = true
	inst:DoTaskInTime(0.2, function() inst.eyecdin = false end)
end

local function PigPig(inst)
    --isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"pigsy",true,true,true) then
        return
    end
    if inst.eyecdin then return end
    
    if not inst._canbecomehpig:value() then
        return
    end

    if inst.sg and inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("idle") then
        return
    end
    
    if inst.components.hunger.current < 50 then return end  
    
    if inst._becomehpig:value() == true then --turn off
        inst._becomehpig:set(false)
    else
        if inst.components.inventory then
            local inv = inst.components.inventory
            for k,v in pairs(inv.equipslots) do
                inv:GiveItem(inv:Unequip(k)) 
             end
        end
        inst._becomehpig:set(true)
    end
    inst.eyecdin = true
    inst:DoTaskInTime(0.2, function() inst.eyecdin = false end)
end

local function Yama(inst)
    ----isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"yama_commissioners",nil,true,true) then
        return 
    end
    if inst.eyecdin then return end

    if inst.sg and inst.sg:HasStateTag("yama_transform") then
        return
    end
    inst.sg:GoToState("exchange_commissioner")

    inst.eyecdin = true
    inst:DoTaskInTime(0.2, function() inst.eyecdin = false end)
end

local function Madame(inst)
    ----isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"madameweb",true,true,true) then
        return
    end
    if inst.eyecdin then return end
    if not inst._canbecomespider:value() then
        return 
    end
    if inst.components.mk_silkflyer and inst.components.mk_silkflyer.flying then
        return
    end
    if inst.components.hunger and inst.components.hunger.current <= 0 then
        return
    end
    if inst._beacmespider:value() == true then --turn off
        inst._beacmespider:set(false)
    else
        if inst.components.inventory then
            inst.components.inventory:DropEquipped(true)
        end
        inst.sg:GoToState("madameweb_becamespider")
    end
    inst.eyecdin = true
    inst:DoTaskInTime(0.2, function() inst.eyecdin = false end)
end

local function DingShen(inst)
    ----isplayervalid(inst,prefab,nofly,no_astral,noriding)
    if not isplayervalid(inst,"monkey_king",nil,true,true) then
        return
    end
    if inst.dsfcdin then 
        return 
    end
    if inst.components.sanity and inst.components.sanity.current <= 15 then
        return
    end
    inst.sg:GoToState("mk_dsf")
    inst.dsfcdin = true
    inst:DoTaskInTime(1, function() inst.dsfcdin = false end)
end

local function  Buzzard(inst)
    if not isplayervalid(inst,"yangjian",nil,true) then
        return
    end
    if inst.sg and inst.sg:HasStateTag("whistle_skyhound") then
        return
    end
    SpawnPrefab"yangjian_buzzard_spawn":OnBuiltFn(inst)
end

local function  Skytrack(inst)
    if not isplayervalid(inst,"yangjian",nil,true,true) then
        return
    end
    if inst.sg and inst.sg:HasStateTag("whistle_skyhound") then
        return
    end
    SpawnPrefab"yangjian_track":OnBuiltFn(inst)
end

local function  Daoyao(inst)
    if not isplayervalid(inst,"myth_yutu",nil,true,true) then
        return
    end
    if inst.sg and inst.sg:HasStateTag("myth_daoyao") then
        return
    end
    SpawnPrefab"yt_daoyao":OnBuiltFn(inst)
end

local function Pipa(inst)
    if not isplayervalid(inst,"myth_yutu",nil,true,true) then
        return
    end
    if inst.sg and inst.sg:HasStateTag("playguitar_m") then
        return
    end
    local guitar_jadehare = inst.components.inventory:FindItem(function(item)
        return item.prefab == "guitar_jadehare"
    end)
    if guitar_jadehare and guitar_jadehare.components.myth_use_inventory then
        guitar_jadehare.components.myth_use_inventory.onusefn(guitar_jadehare,inst)
    end
end

local function Bahy(inst)
    if not isplayervalid(inst,"yama_commissioners",nil,true) then
        return
    end
    if inst.components.sanity.current < 15 then 
        return 
    end  
    SpawnPrefab"myth_bahy":OnBuiltFn(inst) 
end

local function myth_transform(inst,num)
    if type(num) =="number" then
        if num == 1 then
            Fireeye(inst)
        elseif num == 2  then
            Changebuild(inst)
        elseif num == 3 then
            YJSkyeye(inst)
        elseif num == 4 then
            PigPig(inst)
        elseif num == 5 then
            Yama(inst)
        elseif num == 6 then
            Madame(inst)
        elseif num == 7 then
            DingShen(inst)
        elseif num == 8 then
            Buzzard(inst)
        elseif num == 9 then
            Skytrack(inst)
        elseif num == 10 then
            Daoyao(inst)
        elseif num == 11 then
            Pipa(inst)
        elseif num == 12 then
            Bahy(inst)
        end
    end
end
AddModRPCHandler("myth_transform", "myth_transform", myth_transform)

--复活的三维 待写！！！
local function onbecamehuman(inst)
    if inst.myth_higanbana_revive_states then
        inst.components.hunger:SetPercent((inst.myth_higanbana_revive_states[1] or 5) / inst.components.hunger.max, true)
        inst.components.sanity:SetPercent((inst.myth_higanbana_revive_states[2] or 30) / inst.components.sanity.max, true)
        inst.components.health:SetCurrentHealth(inst.myth_higanbana_revive_states[3] or 5)
        inst.myth_higanbana_revive_states = nil
    end
end

AddPlayerPostInit(function(inst)
	inst._inwbfog = net_bool(inst.GUID, "player._inwbfog", "inwbfogdirty")


    if not TheWorld.ismastersim then
		return inst
	end
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
end)

FUELTYPE.LOTUS_FLOWER = "LOTUS_FLOWER"

AddPrefabPostInit("lotus_flower", function(inst)
	if not TheWorld.ismastersim then
		return inst
	end
	inst:AddComponent("lotusplant")
    inst:AddComponent("fuel")
    inst.components.fuel.fueltype = FUELTYPE.LOTUS_FLOWER
    inst.components.fuel.fuelvalue = 10
end)

AddPrefabPostInit("world", function(inst)
    inst:AddComponent("white_bone_fog")
    inst:AddComponent("yama_statuemanager")
end)

local function candig(pt)
	if math.abs(pt.z) > 1500 then 
		return false
	end
	return true
end
local MYTHBIANHUAN = Action({ priority=2,under_ground = true})
MYTHBIANHUAN.id = "MYTHBIANHUAN"
MYTHBIANHUAN.strfn = function(act)
    local target = act.invobject or act.target
    if target and target:HasTag("myth_yutu_underground") then
        return "OUT_GROUND"
    else
        return  target and target.BIANHUAN_STR or "USE"
    end
end
MYTHBIANHUAN.fn = function(act)
    if act.doer and act.doer.components.mk_bh  then
		act.doer.components.mk_bh:OnChange()
		return true
	end
	return false
end
AddAction(MYTHBIANHUAN)
AddComponentAction("SCENE", "mk_bh" , function(inst, doer, actions, right)
    if right  and not IsFlying(inst) and inst == doer then
		if doer:HasTag("monkey_king") and not inst:HasTag("mk_miao")
			and inst.replica.inventory and inst.replica.inventory:GetActiveItem() == nil and inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil
			and not (inst.replica.rider ~= nil and inst.replica.rider:IsRiding()) and doer:HasTag("cansuemk_bh") then
			table.insert(actions, ACTIONS.MYTHBIANHUAN)
		elseif doer:HasTag("white_bone")
			and not (inst.replica.rider ~= nil and inst.replica.rider:IsRiding()) and doer:HasTag("cansuemk_bh") then
				table.insert(actions, ACTIONS.MYTHBIANHUAN)
		elseif doer:HasTag("myth_yutu") and not (inst.replica.rider ~= nil and inst.replica.rider:IsRiding()) and inst:IsOnValidGround()then
			local pt = doer:GetPosition()
			if inst:HasTag("myth_yutu_underground") or candig(pt) then
				table.insert(actions, ACTIONS.MYTHBIANHUAN)
			end
        elseif doer:HasTag("pig_zhuzhu") then
            table.insert(actions, ACTIONS.MYTHBIANHUAN)
		end
    end
end) 
AddStategraphActionHandler("wilson",ActionHandler(ACTIONS.MYTHBIANHUAN, function(inst, action)
    if action and action.target ~= nil then
        if action.target:HasTag("white_bone") then
            return "white_bone_frog"
        end
    end
    return "myth_sg_pre"
end))
AddStategraphActionHandler("wilson_client",ActionHandler(ACTIONS.MYTHBIANHUAN, function(inst, action)
    if action and action.target ~= nil then
        if action.target:HasTag("white_bone") then
            return "white_bone_frog"
        end
    end
    return "myth_sg_pre"
end))

STRINGS.ACTIONS.MYTHBIANHUAN = {
    IN_GROUND = STRINGS.YTBIANHUAN,
    OUT_GROUND = STRINGS.YTBIANHUAN_OUT,
    MK_BIANHUAN = STRINGS.MKBIANHUAN,
    USE = STRINGS.MKBIANHUAN,
    WB_BIANHUAN = STRINGS.WBBIANHUAN,
    PIG_BIANHUAN = STRINGS.PIGBIANHUAN,
    MA_BIANHUAN = STRINGS.MADABIANHUAN,
}

local jgbskin = {
    ear = "monkey_king_lunge_ear",
}
local pick = Action({priority = 2,distance=64,})
pick.id = "MYTH_PICKUP_PILLAR"
pick.str = STRINGS.MYTH_PICKUP_PILLAR
pick.fn = function(act)
	local skin = act.target.skin
    if act.target.is_picked then
        return false
    else
        act.target.is_picked = true
    end
    act.target.persists = false
	act.target.AnimState:PlayAnimation("pillar_pick")
    local pt = act.target:GetPosition()
	act.target:ListenForEvent("animover", function(inst)
		SpawnPrefab("collapse_small").Transform:SetPosition(pt.x,0,pt.z)
        local mk_jgb_spin = SpawnPrefab("mk_jgb_spin")
        if mk_jgb_spin then
            mk_jgb_spin.Transform:SetPosition(pt.x,2,pt.z)
            mk_jgb_spin:SetSkin(skin)
            mk_jgb_spin.AnimState:OverrideSymbol("fx_lunge_streak", jgbskin[skin] or "monkey_king_lunge_new", "fx_lunge_streak")
            --mk_jgb_spin:DoTaskInTime(0.5,function()
                if mk_jgb_spin.components.projectile and act.doer and act.doer:IsValid() then
                    if IsFlying(act.doer) then
                        mk_jgb_spin.components.projectile:SetLaunchOffset(Vector3(0, 1.5, 0))
                    end
                    mk_jgb_spin.components.projectile:Throw(mk_jgb_spin, act.doer)
                else
                    mk_jgb_spin:Remove()
                end
            --end)
        end
        inst:Remove()
	end)
	return true
end

AddAction(pick)

AddComponentAction("SCENE", "mk_jgb_pillar", function(inst, doer, actions, right)
	if right and  doer.prefab == 'monkey_king' then
		table.insert(actions, pick)
	end
end)

AddStategraphActionHandler("wilson", ActionHandler(pick, "catchjgb"))
--AddStategraphActionHandler("wilson_client", ActionHandler(pick, "catchjgb"))

AddStategraphState('wilson', State{
    name = "catchjgb",
    tags = {"notalking","busy", "pausepredict"},
	
    onenter = function(inst, data)
        inst.components.locomotor:Stop()
        if not inst.AnimState:IsCurrentAnimation("catch_pre") then
            inst.AnimState:PlayAnimation("catch_pre")
        end
		if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RemotePausePrediction()
        end
	end,
    timeline = {
        TimeEvent(0.2, function(inst)inst:PerformBufferedAction() end)
     },
    events= {
        EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        EventHandler("catchjgb", function(inst) inst.sg:GoToState("idle") end),
    },
	ontimeout = function(inst)
		inst.sg:RemoveStateTag("pausepredict")
		inst.sg:GoToState("idle")
	end,
})

AddStategraphState("wilson", State{
    name = "mk_summonjgb",
    tags ={"idle"},
    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("research")
    end,

    timeline = {
        TimeEvent(1*FRAMES, function(inst)inst:PerformBufferedAction() end)
     },

    events= {
        EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
    },
})

local dssymbols = {
    monkey_king_fire = true,
    monkey_king_wine = true,
}

local function dingshen(inst)
	if inst and inst.prefab == "monkey_king" then 
		local x,y,z = inst.Transform:GetWorldPosition()
        local fx = SpawnPrefab("mk_dsf_sound")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        fx:Play()

		local ents = TheSim:FindEntities(x,y,z, 32,{"_combat","_health"},{"player","INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost"})
		for i,v in ipairs(ents) do
			if v and v.components.health and not v.components.health:IsDead() and 
                (v.components.combat and  v.components.combat.target == inst or v:HasTag("hostile")) then
				if not v.components.mk_hold_animal then
					v:AddComponent("mk_hold_animal")
				end
				v.components.mk_hold_animal:Start()
			end
		end
	end
end

AddStategraphState("wilson", State{
    name = "mk_dsf",
    tags ={"idle","busy","mk_dsf","pausepredict"},
    onenter = function(inst)
        inst.components.locomotor:Stop()
        local build = inst.AnimState:GetBuild()
        local symbol = dssymbols[build] and build or "dshand"
        inst.AnimState:OverrideSymbol("dshand", "my_dshand", symbol)
        inst.AnimState:PlayAnimation("dingshenshu")
        inst.components.sanity:DoDelta(-15)
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RemotePausePrediction()
        end
    end,

    timeline = {
        TimeEvent(0.3, function(inst)
            dingshen(inst) 
        end),
        TimeEvent(0.5, function(inst) 
            inst.sg:RemoveStateTag("busy") 
        end),
    },
    events= {
        EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
    },
	onexit = function(inst)
		--inst.components.health:SetInvincible(false)
	end,
})

AddStategraphState("wilson_client", State{
    name = "mk_summonjgb",
    tags ={"idle"},
    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("research")
    end,

    timeline = {
    	TimeEvent(1*FRAMES, function(inst)inst:PerformPreviewBufferedAction() end),
    	TimeEvent(2, function(inst)inst:ClearBufferedAction()end),
    },

    onupdate = function(inst)
        if inst:HasTag("doing") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle", true)
        end
    end,

    events= {
        EventHandler("animqueneover", function(inst) inst.sg:GoToState("idle") end),
    },
})

AddStategraphState("wilson_client", State{
    name = "mk_dsf",
    tags ={"idle","busy","mk_dsf"},
    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("dingshenshu")
    end,

    timeline = {
    	TimeEvent(1*FRAMES, function(inst)inst:PerformPreviewBufferedAction() end),
    	TimeEvent(2, function(inst)inst:ClearBufferedAction()end),
    },

    onupdate = function(inst)
        if inst:HasTag("doing") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle", true)
        end
    end,

    events= {
        EventHandler("animqueneover", function(inst) inst.sg:GoToState("idle") end),
    },
})

AddStategraphState("wilson", 
    State{
        name = "mk_jgb_teleport_out",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nomorph" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("wortox_portal_jumpout")
        end,
        
        timeline =
        {
            TimeEvent(FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/hop_out") end),
            TimeEvent(7 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }
)

AddStategraphState("wilson_client", 
    State{
        name = "mk_jgb_teleport_out",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nomorph" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("wortox_portal_jumpout")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }
)

AddPrefabPostInit("bird_mutant", function(inst) 
	if inst.components.combat then
		local old = inst.components.combat.StopTrackingTarget
		inst.components.combat.StopTrackingTarget = function(self,...)
			if not self.losetargetcallback then
				return
			end
			old(self,...)
		end
	end
end)

--定身术相关 小豪牛逼
-- 延时任务
AddGlobalClassPostConstruct("scheduler", "Periodic", function(self)
    function self:AddTick()
        if not self.nexttick or not scheduler.attime[self.nexttick] then
            return
        end
        local thislist = scheduler.attime[self.nexttick]
        self.nexttick = self.nexttick + 1
        if not scheduler.attime[self.nexttick] then
            scheduler.attime[self.nexttick] = {}
        end
        local nextlist = scheduler.attime[self.nexttick]
        thislist[self] = nil
        nextlist[self] = true
        self.list = nextlist
    end
end)

-- 状态
AddGlobalClassPostConstruct("stategraph", "StateGraphInstance", function(self)

    local old_gotostate = self.GoToState
    function self:GoToState(newstate, p, ...)
        local state = self.sg.states[newstate]      
        if not state then 
            return old_gotostate(self, newstate, p, ...)
        end
		if self:HasStateTag('white_bone_killed') then 
			return 
		end
		if self.inst:HasTag("myth_dingshenshu") and self.inst.components.mk_hold_animal then
			self.inst.components.mk_hold_animal:Stop()
		end
		return old_gotostate(self, newstate, p, ...)
    end

    local old_update = self.Update
    function self:Update(...)
        local sleep_time = old_update(self, ...)
        if not sleep_time then
            self.myth_nextupdatetick = nil
        else
            local sleep_ticks = sleep_time/GetTickTime()
            sleep_ticks = sleep_ticks == 0 and 1 or sleep_ticks
            self.myth_nextupdatetick = math.floor(sleep_ticks + GetTick())+1
        end

        return sleep_time
    end

    function self:AddMythTick(dt)
        dt = dt or GetTickTime()
        self.statestarttime = self.statestarttime + dt
        if self.myth_nextupdatetick then
            local thislist = SGManager.tickwaiters[self.myth_nextupdatetick]
            if not thislist then return end
            self.myth_nextupdatetick = self.myth_nextupdatetick + 1
            if not SGManager.tickwaiters[self.myth_nextupdatetick]then
                SGManager.tickwaiters[self.myth_nextupdatetick] = {}
            end
            local nextlist = SGManager.tickwaiters[self.myth_nextupdatetick]
            thislist[self] = nil
            nextlist[self] = true
        end
    end
end)

-- AI
AddGlobalClassPostConstruct("behaviourtree", "BT", function(self)
    local old_update = self.Update
    function self:Update(...)
        if self.inst:HasTag("myth_dingshenshu") then
            return
		end
		return old_update(self, ...)
    end
end)

--修复猴子分身嘲讽远古boss的bug
AddPrefabPostInit("atrium_gate",function(inst)
	if inst.IsObjectInAtriumArena then
		local old  = inst.IsObjectInAtriumArena
		inst.IsObjectInAtriumArena = function(inst, obj)
			if not (obj and obj:IsValid()) then
				return false
			end
			return old(inst, obj)
		end
	end
end)