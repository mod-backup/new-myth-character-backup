local assets = {
    Asset("ANIM", "anim/hua_internet_node.zip"),
    Asset("ATLAS", "images/inventoryimages/hua_internet_node_item.xml"),
}

local assets_sea = {
    Asset("ANIM", "anim/hua_internet_node_sea.zip"),
    Asset("ATLAS", "images/inventoryimages/hua_internet_node_sea_item.xml"),
}

local function ondeploy(inst, pt)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
    local tree = SpawnPrefab(inst.deploy_obj)
    if tree ~= nil then
        tree.Transform:SetPosition(pt:Get())
        inst.components.stackable:Get():Remove()
    end
end

local function common(obj)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank(obj)
    inst.AnimState:SetBuild(obj)
    inst.AnimState:PlayAnimation("item")

    inst:AddTag("cattoy")
    inst:AddTag("silkcontaineritem")

    inst.deploy_obj = obj

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

    inst:AddComponent("inspectable")

    MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
    MakeSmallPropagator(inst)
    MakeHauntableLaunchAndIgnite(inst)

    inst:AddComponent("inventoryitem")

    inst:AddComponent("tradable")

    inst:AddComponent("deployable")
    inst.components.deployable.ondeploy = ondeploy

    return inst
end

local function candeploy(inst, pt, mouseover, deployer)
    return TheWorld.Map:IsAboveGroundAtPoint(pt.x, 0, pt.z,false,false)
end

local function fn()
    local inst = common("hua_internet_node")

    inst._custom_candeploy_fn = candeploy

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.deployable:SetDeployMode(DEPLOYMODE.CUSTOM)

    return inst
end

local function candeploy_sea(inst, pt, mouseover, deployer)
    return TheWorld.Map:CanDeployAtPointInWater(pt, inst, mouseover, {
        land = 0.2,
        boat = 0.2,
        radius = 4,
    })
end

local function sea_fn()
    local inst = common("hua_internet_node_sea")

    inst:AddTag("usedeployspacingasoffset")

    inst._custom_candeploy_fn = candeploy_sea

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.deployable:SetDeployMode(DEPLOYMODE.CUSTOM)
    inst.components.deployable:SetDeploySpacing(DEPLOYSPACING.HUA)

    return inst
end

return Prefab("hua_internet_node_item", fn, assets),
Prefab("hua_internet_node_sea_item", sea_fn, assets_sea),
MakePlacer("hua_internet_node_item_placer", "hua_internet_node", "hua_internet_node", "idle_none"),
MakePlacer("hua_internet_node_sea_item_placer", "hua_internet_node_sea", "hua_internet_node_sea", "idle_none")