local prefs = {}

--------------------------------------------------------------------------
--[[ 通用函数 ]]
--------------------------------------------------------------------------

local function GetRandomPos(x, y, z, radius, angle)
    local rad = radius or math.random() * 3
    local ang = angle or math.random() * 2 * PI
    return x + rad * math.cos(ang), y, z - rad * math.sin(ang)
end

local function SpawnLightFx(inst)
	if inst.components.combat ~= nil and inst.components.combat.hiteffectsymbol ~= nil then
		local fx = SpawnPrefab("myth_yutu_heal_fx")
		fx.entity:AddFollower():FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, -50, 0)
		fx:Setup(inst)
	end
end

local function SpawnAroundFx(x, y, z)
	local numfx = 6 + math.random(4)
	for i = 1, numfx, 1 do
		local fx = SpawnPrefab("powder_m_use_fx")
		if fx ~= nil then
			local rad = 1 + math.random()*7
			local ang = math.random() * 2 * PI
			fx.Transform:SetPosition(x + rad * math.cos(ang), y, z - rad * math.sin(ang))
			fx.SetUp(fx, rad, ang, { x = x, y = y, z = z })
		end
	end
end

local function MakePowder(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddNetwork()

			inst.AnimState:SetBank("yutu_powder_myth")
			inst.AnimState:SetBuild("yutu_powder_myth")

			inst.myth_use_needtag = nil
			inst.MYTH_USE_TYPE = nil
			inst.onusesgname = "dolongaction"

			MakeInventoryPhysics(inst)

			if data.floatable ~= nil then
				MakeInventoryFloatable(inst, data.floatable[2], data.floatable[3], data.floatable[4])
				if data.floatable[1] ~= nil then
					local OnLandedClient_old = inst.components.floater.OnLandedClient
					inst.components.floater.OnLandedClient = function(self)
						OnLandedClient_old(self)
						self.inst.AnimState:SetFloatParams(data.floatable[1], 1, self.bob_percent)
					end
				end
			end

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				-- if data.fn_between ~= nil then
				-- 	data.fn_between(inst)
				-- end
				return inst
			end

			inst:AddComponent("inspectable")

			inst:AddComponent("inventoryitem")
			inst.components.inventoryitem.imagename = data.name
			inst.components.inventoryitem.atlasname = "images/inventoryimages/"..data.name..".xml"

			inst:AddComponent("stackable")
			inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

			inst:AddComponent("tradable")

			inst:AddComponent("myth_use_inventory")
			inst.components.myth_use_inventory.canuse = true
			inst.components.myth_use_inventory.canusescene = false --之所以取消，是因为会有多个玩家同时使用1个药粉都生效的问题
			inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
				inst.OnPowderUseFn(inst, doer, true)

				if inst.components.stackable:IsStack() then
					inst.components.stackable:Get():Remove()
				else
					inst:Remove()
				end
				return true
			end)

			MakeHauntableLaunch(inst)

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

--------------------------------------------------------------------------
--[[ 草参药粉 ]]
--------------------------------------------------------------------------

--从singinginspiration.lua中复制
local function HasFriendlyLeader(target, singer, PVP_enabled)
    local target_leader = (target.components.follower ~= nil) and target.components.follower.leader or nil

    if target_leader and target_leader.components.inventoryitem then
        target_leader = target_leader.components.inventoryitem:GetGrandOwner()
        -- Don't attack followers if their follow object has no owner, unless its pvp, then there are no rules!
        if target_leader == nil then
            return not PVP_enabled
        end
    end

    return  (target_leader ~= nil and (target_leader == singer or (not PVP_enabled and target_leader:HasTag("player"))))
			or (not PVP_enabled and target.components.domesticatable and target.components.domesticatable:IsDomesticated())
			or (not PVP_enabled and target.components.saltlicker and target.components.saltlicker.salted)
end

local function HealThisOne(inst, value, cause)
	if inst.components.health ~= nil and not inst.components.health:IsDead() and inst.components.health:IsHurt() then
		inst.components.health:DoDelta(value, false, cause)
	end
end

MakePowder({
	name = "powder_m_hypnoticherb",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_hypnoticherb.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_hypnoticherb.tex"),
	},
	prefabs = nil,
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("hypnoticherb")
	end,
	fn_server = function(inst)
		--tip：可以原地产生一个阵，直接恢复阵内玩家生命值，总共能恢复600血量，用完才消失
		inst.OnPowderUseFn = function(inst, doer, isalone)
			local x, y, z = doer.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(
				x, y, z, 25, nil,
				{ "playerghost", "FX", "DECOR", "INLIMBO" }, { "sleeper", "_health" }
			)
			local canhitplayer = not doer:HasTag("player") or TheNet:GetPVPEnabled() --对其他玩家是否有敌意

			--给自己加血
			HealThisOne(doer, 300, inst.prefab)
			SpawnLightFx(doer)

			for i, v in ipairs(ents) do
				if v ~= doer and v:IsValid() then
					--加血
					if
						not isalone and
						( v:HasTag("player") or HasFriendlyLeader(v, doer, false) )
					then
						HealThisOne(v, 300, inst.prefab)
						SpawnLightFx(v)
					end

					--催眠
					if (canhitplayer or not v:HasTag("player")) then
						local time = TUNING.MANDRAKE_SLEEP_TIME + math.random()
						if
							not (v.components.freezable ~= nil and v.components.freezable:IsFrozen()) and
							not (v.components.pinnable ~= nil and v.components.pinnable:IsStuck()) and
							not (v.components.fossilizable ~= nil and v.components.fossilizable:IsFossilized())
						then
							local mount = v.components.rider ~= nil and v.components.rider:GetMount() or nil
							if mount ~= nil then
								mount:PushEvent("ridersleep", { sleepiness = 7, sleeptime = time })
							end
							if v:HasTag("player") then
								v:PushEvent("yawn", { grogginess = 4, knockoutduration = time })
							elseif v.components.sleeper ~= nil then
								v.components.sleeper:AddSleepiness(7, time)
							elseif v.components.grogginess ~= nil then
								v.components.grogginess:AddGrogginess(4, time)
							else
								v:PushEvent("knockedout")
							end
						end
					end
				end
			end

			if not isalone then
				SpawnAroundFx(x, y, z)
			end
		end
	end
})

--------------------------------------------------------------------------
--[[ 犀茸药粉 ]]
--------------------------------------------------------------------------

local function ResetThisOne(inst)
	if inst.components.health ~= nil and not inst.components.health:IsDead() then
		inst.components.health:SetPercent(1)

		if inst.components.hunger ~= nil then
			inst.components.hunger:SetPercent(1)
		end

		if inst.components.sanity ~= nil then
			inst.components.sanity:SetPercent(1)
		end
	end
end

MakePowder({
	name = "powder_m_lifeelixir",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_lifeelixir.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_lifeelixir.tex"),
	},
	prefabs = nil,
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("lifeelixir")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			--如果doer本来就是鬼魂，说明是作祟产生的效果，那就只能作用于自己
			if doer:HasTag("playerghost") then
				doer:PushEvent("respawnfromghost")
				doer:DoTaskInTime(1, function()
					ResetThisOne(doer)
					SpawnLightFx(doer)
				end)
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			local didit = false
			for _, v in ipairs(AllPlayers) do
				if
					v:HasTag("playerghost") and
					v.entity:IsVisible() and
					v:GetDistanceSqToPoint(x, y, z) < 625 --25范围内
				then
					didit = true

					--先复活
					v:PushEvent("respawnfromghost")
					--再恢复三维
					v:DoTaskInTime(1, function()
						if v:IsValid() then
							ResetThisOne(v)
							SpawnLightFx(v)
						end
					end)

					if isalone then --非捣药时使用，只复活一个玩家
						return
					end
				end
			end

			if not didit then --如果周围没有可复活玩家，就恢复自己的三维
				ResetThisOne(doer)
				SpawnLightFx(doer)
			else
				SpawnAroundFx(x, y, z)
			end
		end

		inst.components.hauntable:SetOnHauntFn(function(inst, haunter)
			if haunter:HasTag("playerghost") then
				inst.components.myth_use_inventory:OnUse(haunter)
			else
				Launch(inst, haunter, TUNING.LAUNCH_SPEED_SMALL)
            	inst.components.hauntable.hauntvalue = TUNING.HAUNT_TINY
			end
			return true
		end)
	end
})

--------------------------------------------------------------------------
--[[ 惊厥药粉 ]]
--------------------------------------------------------------------------

local function TryAddBuff(inst, buffkey, buffname, timername, time, ignore)
	if
		inst.components.debuffable ~= nil and inst.components.debuffable:IsEnabled() and
		not (inst.components.health ~= nil and inst.components.health:IsDead()) and
		not inst:HasTag("playerghost")
	then
		if ignore then
			inst.components.debuffable:AddDebuff(buffkey, buffname)
		else
			local isnewbuff = false
			if not inst.components.debuffable:HasDebuff(buffkey) then --如果无已有buff，就新增一个
				inst.components.debuffable:AddDebuff(buffkey, buffname)
				isnewbuff = true
			end

			local buffnow = inst.components.debuffable:GetDebuff(buffkey)
			if buffnow ~= nil and buffnow.components.timer ~= nil then
				if isnewbuff then --本来没有该buff，则直接设置时间
					buffnow.components.timer:SetTimeLeft(timername, time)
				else --已有buff的剩余时间小于设置的时间，恢复设置时间
					local timeleft = buffnow.components.timer:GetTimeLeft(timername)
					if timeleft ~= nil and timeleft < time then
						buffnow.components.timer:SetTimeLeft(timername, time)
					end
				end
			end
		end

		SpawnLightFx(inst)
	end
end

local function AddBuffMyth(inst, buffname, timekey, time)
	if
		inst.components.debuffable ~= nil and inst.components.debuffable:IsEnabled() and
		not (inst.components.health ~= nil and inst.components.health:IsDead()) and
		not inst:HasTag("playerghost")
	then
		if timekey ~= nil then
			inst[timekey] = { replace_min = time }
		end
		inst.components.debuffable:AddDebuff(buffname, buffname)

		SpawnLightFx(inst)
	end
end

MakePowder({
	name = "powder_m_charged",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_charged.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_charged.tex"),
	},
	prefabs = { "buff_electricattack", "buff_m_atkelec" },
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("charged")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			if isalone then --仅给自己加buff
				AddBuffMyth(doer, "buff_m_atkelec", "time_m_atkelec", 150)
				if doer and doer.components.inventory then
					local items = doer.components.inventory:FindItems(function(...) return true end)
					for _,v in pairs(items) do
						v:PushEvent("onuse_powder_m_charged")
					end
				end
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(
				x, y, z, 25, { "debuffable" },
				{ "playerghost", "FX", "DECOR", "INLIMBO" }, nil
			)
			for i, v in ipairs(ents) do
				if v:IsValid() and ( v == doer or v:HasTag("player") or HasFriendlyLeader(v, doer, false) ) then
					AddBuffMyth(v, "buff_m_atkelec", "time_m_atkelec", 150)
				end
			end
			SpawnAroundFx(x, y, z)
		end
	end
})

--------------------------------------------------------------------------
--[[ 活血药粉 ]]
--------------------------------------------------------------------------

local function MakeBurnable(inst)
	inst:AddComponent("fuel")
	inst.components.fuel.fuelvalue = TUNING.SMALL_FUEL

	MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
	MakeSmallPropagator(inst)
end

MakePowder({
	name = "powder_m_improvehealth",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_improvehealth.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_improvehealth.tex"),
	},
	prefabs = { "buff_m_regenhealth" },
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("improvehealth")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			if isalone then --仅给自己效果
				AddBuffMyth(doer, "buff_m_regenhealth", "time_m_regenhealth", TUNING.JELLYBEAN_DURATION)
				HealThisOne(doer, 60, inst.prefab)
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(
				x, y, z, 25, nil,
				{ "playerghost", "FX", "DECOR", "INLIMBO" }, { "debuffable", "_health" }
			)
			for i, v in ipairs(ents) do
				if v:IsValid() and ( v == doer or v:HasTag("player") or HasFriendlyLeader(v, doer, false) ) then
					AddBuffMyth(v, "buff_m_regenhealth", "time_m_regenhealth", TUNING.JELLYBEAN_DURATION)
					HealThisOne(v, 60, inst.prefab)
				end
			end

			SpawnAroundFx(x, y, z)
		end

		MakeBurnable(inst)
	end
})

--------------------------------------------------------------------------
--[[ 夜明药粉 ]]
--------------------------------------------------------------------------

local function TryAddWormLight(inst)
	if
		(inst.components.health ~= nil and inst.components.health:IsDead())
		or inst:HasTag("playerghost")
	then
		return
	end

	SpawnLightFx(inst)

	--see wormlight.lua for original code
	if inst.wormlight ~= nil then
		if inst.wormlight.prefab == "wormlight_light_greater" then
			inst.wormlight.components.spell.lifetime = 0
			inst.wormlight.components.spell:ResumeSpell()
			return
		else
			inst.wormlight.components.spell:OnFinish()
		end
	end

	--由于wormlight_light_greater本身就是16分钟的发亮时间，设定里也是16分钟，因此不需要改动
	local light = SpawnPrefab("wormlight_light_greater")
	light.components.spell:SetTarget(inst)
	if light:IsValid() then
		if light.components.spell.target == nil then
			light:Remove()
		else
			light.components.spell:StartSpell()
		end
	end
end

MakePowder({
	name = "powder_m_becomestar",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_becomestar.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_becomestar.tex"),
	},
	prefabs = { "wormlight_light_greater", "buff_m_glow" },
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("becomestar")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			if isalone then --仅给自己效果
				if doer:HasTag("player") then
					AddBuffMyth(doer, "buff_m_glow", nil, nil)
				else
					TryAddWormLight(doer)
					SpawnLightFx(doer)
				end
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(
				x, y, z, 25, { "_health" },
				{ "playerghost", "FX", "DECOR", "INLIMBO" }, nil
			)
			for i, v in ipairs(ents) do
				if v:IsValid() and ( v == doer or v:HasTag("player") or HasFriendlyLeader(v, doer, false) ) then
					if v:HasTag("player") then
						AddBuffMyth(v, "buff_m_glow", nil, nil)
					else
						TryAddWormLight(v)
						SpawnLightFx(v)
					end
				end
			end

			SpawnAroundFx(x, y, z)
		end

		MakeBurnable(inst)
	end
})

--------------------------------------------------------------------------
--[[ 排郁药粉 ]]
--------------------------------------------------------------------------

MakePowder({
	name = "powder_m_takeiteasy",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_takeiteasy.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_takeiteasy.tex"),
	},
	prefabs = { "myth_sanity_regenbuff" },
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("takeiteasy")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			if doer.components.sanity ~= nil then
				AddBuffMyth(doer, "myth_sanity_regenbuff", "time_m_sanityregen", 30)
				doer.components.sanity:DoDelta(30)
			end

			if isalone then --仅给自己效果
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			for _, v in ipairs(AllPlayers) do
				if
					v ~= doer and
					v.entity:IsVisible() and
					v:GetDistanceSqToPoint(x, y, z) < 625 --25范围内
				then
					if v.components.sanity ~= nil then
						AddBuffMyth(v, "myth_sanity_regenbuff", "time_m_sanityregen", 30)
						v.components.sanity:DoDelta(30)
					end
				end
			end

			SpawnAroundFx(x, y, z)
		end

		MakeBurnable(inst)
	end
})

--------------------------------------------------------------------------
--[[ 寒眸药粉 ]]
--------------------------------------------------------------------------

MakePowder({
	name = "powder_m_coldeye",
	assets = {
		Asset("ANIM", "anim/yutu_powder_myth.zip"),
		Asset("ATLAS", "images/inventoryimages/powder_m_coldeye.xml"),
		Asset("IMAGE", "images/inventoryimages/powder_m_coldeye.tex"),
	},
	prefabs = { "myth_freezebuff" },
	floatable = {0.01, "med", 0.15, 0.4},
	fn_common = function(inst)
		inst.AnimState:PlayAnimation("coldeye")
	end,
	fn_server = function(inst)
		inst.OnPowderUseFn = function(inst, doer, isalone)
			if isalone then --仅给自己加buff
				TryAddBuff(doer, "myth_freezebuff", "myth_freezebuff", "regenover", 150, false)
				return
			end

			local x, y, z = doer.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(
				x, y, z, 25, { "debuffable" },
				{ "playerghost", "FX", "DECOR", "INLIMBO" }, nil
			)
			for i, v in ipairs(ents) do
				if v:IsValid() and ( v == doer or v:HasTag("player") or HasFriendlyLeader(v, doer, false) ) then
					TryAddBuff(v, "myth_freezebuff", "myth_freezebuff", "regenover", 150, false)
				end
			end

			SpawnAroundFx(x, y, z)
		end
	end
})

--------------------------------------------------------------------------
--[[ 药粉使用时的闪光特效 ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
	"powder_m_use_fx",
	function()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()

		inst.AnimState:SetBank("myth_lifeplant_fx")
		inst.AnimState:SetBuild("myth_lifeplant_fx")
		inst.AnimState:PlayAnimation("single"..math.random(1,3),true)
		inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

		inst:AddTag("FX")

		inst.entity:SetPristine()
		if not TheWorld.ismastersim then
			return inst
		end

		inst.persists = false
		inst.centerpos = nil
		inst.radius = nil
		inst.angle = nil
		inst.angle_step = nil

		inst.SetUp = function(inst, radius, angle, centerpos)
			inst.centerpos = centerpos
			inst.radius = radius
			inst.angle = angle
			inst.angle_step = 2 * PI / 60 / radius

			inst.taskgoaround = inst:DoPeriodicTask(3*FRAMES, function(inst)
				inst.angle = inst.angle + inst.angle_step
				local x2, y2, z2 = GetRandomPos(inst.centerpos.x, 0, inst.centerpos.z, inst.radius, inst.angle)
				inst.Transform:SetPosition(x2, y2, z2)
			end, 0)
		end

		inst:DoTaskInTime(1+math.random()*1, function(inst)
			if inst.taskgoaround ~= nil then
				inst.taskgoaround:Cancel()
			end
			inst:Remove()
		end)

		return inst
	end,
	{
		Asset("ANIM", "anim/myth_lifeplant_fx.zip"),
	},
	nil
))

--------------------------------------------------------------------------
--[[ 琵琶曲子与特效 ]]
--------------------------------------------------------------------------

local songs = require("guitar_songs_myth")

local function IsAlive(inst)
	return inst.components.health ~= nil and not inst.components.health:IsDead() and not inst:HasTag("playerghost")
end

local function MakeFx(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddNetwork()

			inst:AddTag("FX")

			--Dedicated server does not need to spawn the local fx
			if not TheNet:IsDedicated() then
				--Delay one frame so that we are positioned properly before starting the effect
				--or in case we are about to be removed
				inst:DoTaskInTime(0, function(proxy)
					local inst2 = CreateEntity()

					inst2:AddTag("FX")
					inst2:AddTag("NOCLICK")
					--[[Non-networked entity]]

					inst2.entity:AddTransform()
					inst2.entity:AddAnimState()
					inst2.entity:SetCanSleep(false)
					inst2.persists = false

					local parent = proxy.entity:GetParent()
					if parent ~= nil then
						inst2.entity:SetParent(parent.entity)
					end

					inst2.Transform:SetFromProxy(proxy.GUID)

					if data.fn_anim ~= nil then
						data.fn_anim(inst2)
					end

					inst2:ListenForEvent("animover", inst2.Remove)
				end)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				return inst
			end

			inst.persists = false
			inst:DoTaskInTime(3, inst.Remove)

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

for k, v in pairs(songs) do
	table.insert(prefs, Prefab(
		v.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()

			inst:AddTag("CLASSIFIED")

			inst.persists = false

			inst:DoTaskInTime(0, inst.Remove)

			if not TheWorld.ismastersim then
				return inst
			end

			inst.OnBuiltFn = function(inst, builder)
				if builder ~= nil and builder.components.inventory ~= nil and IsAlive(builder) then
					local guitar = builder.components.inventory:FindItem(function(item)
						return item:HasTag("guitar_myth")
					end)
					if guitar ~= nil then
						if builder.components.rider ~= nil and builder.components.rider:IsRiding() then
							builder.components.talker:Say(STRINGS.CANT_USEGUITAR_RIDING)
							return
						end
						builder:DoTaskInTime(0.1,function()
							if IsAlive(builder) then
								guitar.lastsong = k
								builder.sg:GoToState("playguitar_m_pre", guitar)
							end
						end)
					else
						if builder.components.talker ~= nil then
							builder.components.talker:Say(GetString(builder, "DESCRIBE", { "GUITAR_JADEHARE", "NOGUITAR" }))
						end
					end
				end
				inst:Remove()
			end

			return inst
		end,
		nil,
		nil
	))

	if v.fxs.note ~= nil then
		MakeFx({
			name = v.name.."_note_fx",
			assets = {
				Asset("ANIM", "anim/guitar_song_note_fx.zip"),
            	Asset("ANIM", "anim/fx_wathgrithr_buff.zip"), --官方战歌特效动画模板
			},
			prefabs = nil,
			fn_anim = function(inst)
				local anims = {
					"fx_durability",
					"fx_fireresistance",
					"fx_healthgain",
					"fx_sanitygain"
				}
				inst.AnimState:SetBank("fx_wathgrithr_buff")
				inst.AnimState:SetBuild("fx_wathgrithr_buff")
				inst.AnimState:PlayAnimation(anims[math.random(#anims)])
				inst.AnimState:OverrideSymbol("fx_icon", "guitar_song_note_fx", "fx_icon")
				inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
				inst.Transform:SetNoFaced()
				inst.Transform:SetScale(0.5, 0.5, 0.5)
				local color = v.fxs.note
				inst.AnimState:SetMultColour(color.r, color.g, color.b, 1)
			end,
		})
	end

	if v.fxs.wave ~= nil then
		MakeFx({
			name = v.name.."_wave_fx",
			assets = {
				Asset("ANIM", "anim/guitar_song_wave_fx.zip"),
            	Asset("ANIM", "anim/lavaarena_boarrior_fx.zip"), --官方战歌特效动画模板
			},
			prefabs = nil,
			fn_anim = function(inst)
				local anims = {
					"ground_hit_1",
					"ground_hit_2",
					"ground_hit_3"
				}
				inst.AnimState:SetBank("lavaarena_boarrior_fx")
				inst.AnimState:SetBuild("guitar_song_wave_fx")
				inst.AnimState:PlayAnimation(anims[math.random(#anims)])
				inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
				inst.AnimState:SetFinalOffset(1)
				inst.Transform:SetScale(0.5, 0.5, 0.5)
				local color = v.fxs.wave
				inst.AnimState:SetMultColour(color.r, color.g, color.b, 1)
			end,
		})
	end
end

--------------------------------------------------------------------------
--[[ 莹月琵琶 ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
	"guitar_jadehare",
	function()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()
		inst.entity:AddMiniMapEntity()

		inst.MiniMapEntity:SetIcon("guitar_jadehare.tex")

		MakeInventoryPhysics(inst)

		inst.AnimState:SetBank("guitar_jadehare")
		inst.AnimState:SetBuild("guitar_jadehare")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("guitar_myth")
		inst:AddTag("nobundling") --该标签使其不能被放进打包纸

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.character = 'myth_yutu'
		inst.components.myth_itemskin.prefab = 'guitar_jadehare'
		inst.components.myth_itemskin:SetData{
			default = {
				swap = {build = "guitar_jadehare", folder = "swap_guitar"},
				icon = {atlas = "guitar_jadehare.xml", image = "guitar_jadehare"},
				anim = {bank = "guitar_jadehare", build = "guitar_jadehare", anim = "idle"},
			},
			apricot = {
				swap = {build = "guitar_jadehare_apricot", folder = "swap_guitar"},
				icon = {atlas = "guitar_jadehare_apricot.xml", image = "guitar_jadehare_apricot"},
				anim = {bank = "guitar_jadehare", build = "guitar_jadehare_apricot", anim = "idle"},
			},
			laurel = {
				swap = {build = "guitar_jadehare_laurel", folder = "swap_guitar"},
				icon = {atlas = "guitar_jadehare_laurel.xml", image = "guitar_jadehare_laurel"},
				anim = {bank = "guitar_jadehare", build = "guitar_jadehare_laurel", anim = "idle"},
			},
			soprano = {
				swap = {build = "guitar_jadehare_soprano", folder = "swap_guitar"},
				icon = {atlas = "guitar_jadehare_soprano.xml", image = "guitar_jadehare_soprano"},
				anim = {bank = "guitar_jadehare", build = "guitar_jadehare_soprano", anim = "idle"},
			},
		}

		MakeInventoryFloatable(inst, "med", 0.3, 0.6)
		local OnLandedServer_old = inst.components.floater.OnLandedServer
		inst.components.floater.OnLandedServer = function(self) --掉进海里时使用自己的水面动画
			OnLandedServer_old(self)
			inst.AnimState:PlayAnimation(self:IsFloating() and "idle_water" or "idle")
		end
		local OnNoLongerLandedServer_old = inst.components.floater.OnNoLongerLandedServer
		inst.components.floater.OnNoLongerLandedServer = function(self) --非待在海里时使用自己的陆地动画
			OnNoLongerLandedServer_old(self)
			inst.AnimState:PlayAnimation(self:IsFloating() and "idle_water" or "idle")
		end

		inst.myth_use_needtag = "myth_yutu"
    	inst.MYTH_USE_TYPE = "GUITAR"
		inst.onusesgname = "myth_sg_pre"

		inst.entity:SetPristine()
		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("inspectable")

		inst:AddComponent("inventoryitem")
		--inst.components.inventoryitem.imagename = "guitar_jadehare"
		inst.components.inventoryitem.atlasname = "images/inventoryimages/guitar_jadehare.xml"
		inst.components.inventoryitem:SetOnPickupFn(function(inst, pickupguy, src_pos) --玉兔才能捡起该物品
			if pickupguy ~= nil then
				if pickupguy:HasTag("myth_yutu") then
					return
				end

				if
					pickupguy:HasTag("player")
					or pickupguy.prefab == "krampus"
					or pickupguy.prefab == "eyeplant" or pickupguy.prefab == "lureplant"
					or pickupguy.prefab == "monkey"
				then
					if pickupguy.components.inventory ~= nil then
						pickupguy:DoTaskInTime(0, function()
							pickupguy.components.inventory:DropItem(inst, true, true)
						end)
					end
				end
			end
		end)

		inst.lastsong = nil
		inst.OnEquip = function(inst, owner)
			inst.components.myth_itemskin:OverrideSymbol(owner, "swap_guitar")
		end

		inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = true
		inst.components.myth_use_inventory.canusescene = false
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if inst.lastsong ~= nil and songs[inst.lastsong] ~= nil then
				--当前玉兔会这个曲子才能弹奏，防止玉兔B没学习曲子就能弹玉兔A弹过的曲子
				if doer.components.builder ~= nil and doer.components.builder:KnowsRecipe(songs[inst.lastsong].name) then
					doer.sg:GoToState("playguitar_m_pre", inst)
				else
					if doer.components.talker ~= nil then
						doer.components.talker:Say(GetString(doer, "DESCRIBE", { "GUITAR_JADEHARE", "UNKOWN" }))
					end
				end
			else
				if doer.components.talker ~= nil then
					doer.components.talker:Say(GetString(doer, "DESCRIBE", { "GUITAR_JADEHARE", "NOSONG" }))
				end
			end
			return true
		end)

		MakeHauntableLaunch(inst)

		return inst
	end,
	{
		Asset("ANIM", "anim/guitar_jadehare.zip"),
		Asset("ANIM", "anim/guitar_jadehare_apricot.zip"),
		Asset("ANIM", "anim/guitar_jadehare_laurel.zip"),
		Asset("ANIM", "anim/guitar_jadehare_soprano.zip"),
		Asset("ATLAS", "images/inventoryimages/guitar_jadehare.xml"),
		Asset("IMAGE", "images/inventoryimages/guitar_jadehare.tex"),
		Asset("ATLAS", "images/inventoryimages/guitar_jadehare_apricot.xml"),
		Asset("IMAGE", "images/inventoryimages/guitar_jadehare_apricot.tex"),
		Asset("ATLAS", "images/inventoryimages/guitar_jadehare_laurel.xml"),
		Asset("IMAGE", "images/inventoryimages/guitar_jadehare_laurel.tex"),
		Asset("ATLAS", "images/inventoryimages/guitar_jadehare_soprano.xml"),
		Asset("IMAGE", "images/inventoryimages/guitar_jadehare_soprano.tex"),
	},
	nil
))

--------------------------------------------------------------------------
--[[ 可触摸音符 ]]
--------------------------------------------------------------------------

local function Disappear_note(inst)
	inst.AnimState:SetBank("lavaarena_boarrior_fx")
	inst.AnimState:SetBuild("guitar_jadehare_dot")
	inst.AnimState:PlayAnimation("ground_hit_1")
	inst.AnimState:SetScale(0.7, 0.7)
	if inst.touchfxcolor ~= nil then
		inst.AnimState:SetMultColour(inst.touchfxcolor.r, inst.touchfxcolor.g, inst.touchfxcolor.b, 1)
	end

	inst:AddTag("FX")
	inst.components.myth_use_inventory.canusescene = false
	inst:ListenForEvent("animover", function(inst)
        if inst.AnimState:AnimDone() then
            inst:Remove()
        end
    end)
end

table.insert(prefs, Prefab(
	"guitar_jadehare_note",
	function()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddLight()
		inst.entity:AddNetwork()

		inst.Light:SetRadius(.3)
		inst.Light:SetFalloff(1)
		inst.Light:SetIntensity(.6)
		inst.Light:Enable(false)

		inst.AnimState:SetBank("guitar_jadehare_note")

		local builds = {
			"guitar_jadehare_note",
			"guitar_jadehare_noteother1",
			"guitar_jadehare_noteother2",
			"guitar_jadehare_noteother3",
			"guitar_jadehare_noteother4"
		}
		inst.AnimState:SetBuild(builds[math.random(#builds)])

		inst.AnimState:PlayAnimation("idle_pre")
		inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

		inst:AddTag("NOBLOCK")

		inst.myth_use_needtag = nil
    	inst.MYTH_USE_TYPE = "GUITAR_NOTE"
		inst.onusesgname = "give"

		inst.entity:SetPristine()
		if not TheWorld.ismastersim then
			return inst
		end

		inst.persists = false

		inst:DoTaskInTime(0, function()
            inst.AnimState:PushAnimation("idle", true)
        end)

		inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = false
		inst.components.myth_use_inventory.canusescene = true
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if not inst.components.myth_use_inventory.canusescene then
				return false
			end

			if inst.OnMusicTouch ~= nil then
				inst.OnMusicTouch(inst, doer)
			end
			if inst.taskremove ~= nil then
				inst.taskremove:Cancel()
				inst.taskremove = nil
			end
			Disappear_note(inst)

			return true
		end)

		inst.taskremove = inst:DoTaskInTime(9+math.random()*3, function()
            inst.taskremove = nil
			Disappear_note(inst)
        end)

		return inst
	end,
	{
		Asset("ANIM", "anim/lavaarena_boarrior_fx.zip"),    --需要官方的动画模板
		Asset("ANIM", "anim/guitar_jadehare_dot.zip"),
		Asset("ANIM", "anim/guitar_jadehare_note.zip"),
		Asset("ANIM", "anim/guitar_jadehare_noteother1.zip"),
		Asset("ANIM", "anim/guitar_jadehare_noteother2.zip"),
		Asset("ANIM", "anim/guitar_jadehare_noteother3.zip"),
		Asset("ANIM", "anim/guitar_jadehare_noteother4.zip"),
	},
	nil
))

--------------------
--------------------

return unpack(prefs)
