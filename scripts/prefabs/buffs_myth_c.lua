local prefs = {}

--------------------------------------------------------------------------
--[[ 通用函数 ]]
--------------------------------------------------------------------------

local function StartTimer_attach(buff, target, timekey, timedefault)
    --因为是新加buff，不需要考虑buff时间问题
    if timekey == nil or target[timekey] == nil then
        if not buff.components.timer:TimerExists("buffover") then --因为onsave比这里先加载，所以不能替换先加载的
            buff.components.timer:StartTimer("buffover", timedefault)
        end
    else
        if not buff.components.timer:TimerExists("buffover") then
            local times = target[timekey]
            if times.add ~= nil then
                times = times.add
            elseif times.replace ~= nil then
                times = times.replace
            elseif times.replace_min ~= nil then
                times = times.replace_min
            else
                buff:DoTaskInTime(0, function()
                    buff.components.debuff:Stop()
                end)
                target[timekey] = nil
                return
            end
            buff.components.timer:StartTimer("buffover", times)
        end
        target[timekey] = nil
    end
end

local function StartTimer_extend(buff, target, timekey, timedefault)
    --因为是续加buff，需要考虑buff时间的更新方式
    if timekey == nil or target[timekey] == nil then
        buff.components.timer:StopTimer("buffover")
        buff.components.timer:StartTimer("buffover", timedefault)
    else
        local times = target[timekey]
        target[timekey] = nil
        if times.add ~= nil then --增加型：在已有时间上增加，可设置最大时间限制
            local timeleft = buff.components.timer:GetTimeLeft("buffover") or 0
            timeleft = timeleft + times.add

            if times.max ~= nil and timeleft > times.max then
                timeleft = times.max
            end
            buff.components.timer:StopTimer("buffover")
            buff.components.timer:StartTimer("buffover", timeleft)
        elseif times.replace ~= nil then --替换型：不管已有时间，直接设置
            buff.components.timer:StopTimer("buffover")
            buff.components.timer:StartTimer("buffover", times.replace)
        elseif times.replace_min ~= nil then --最小替换型：若已有时间<该时间时才设置新时间（比较建议的类型）
            local timeleft = buff.components.timer:GetTimeLeft("buffover") or 0
            if timeleft < times.replace_min then
                buff.components.timer:StopTimer("buffover")
                buff.components.timer:StartTimer("buffover", times.replace_min)
            end
        end
    end
end

local function InitTimerBuff(inst, data)
    inst.components.debuff:SetAttachedFn(function(inst, target, ...)
        inst.entity:SetParent(target.entity)
        inst.Transform:SetPosition(0, 0, 0) --in case of loading
        inst:ListenForEvent("death", function()
            inst.components.debuff:Stop()
        end, target)

        StartTimer_attach(inst, target, data.time_key, data.time_default)

        if data.fn_start ~= nil then
            data.fn_start(inst, target, ...)
        end
    end)
    inst.components.debuff:SetDetachedFn(function(inst, target)
        if data.fn_end ~= nil then
            data.fn_end(inst, target)
        end
        inst:Remove()
    end)
    inst.components.debuff:SetExtendedFn(function(inst, target, ...)
        StartTimer_extend(inst, target, data.time_key, data.time_default)

        if data.fn_again ~= nil then
            data.fn_again(inst, target, ...)
        end
    end)

    inst:AddComponent("timer")
    inst:ListenForEvent("timerdone", function(inst, data)
        if data.name == "buffover" then
            inst.components.debuff:Stop()
        end
    end)
end

local function InitNoTimerBuff(inst, data)
    inst.components.debuff:SetAttachedFn(function(inst, target, ...)
        inst.entity:SetParent(target.entity)
        inst.Transform:SetPosition(0, 0, 0) --in case of loading
        inst:ListenForEvent("death", function()
            inst.components.debuff:Stop()
        end, target)

        if data.fn_start ~= nil then
            data.fn_start(inst, target, ...)
        end
    end)
    inst.components.debuff:SetDetachedFn(function(inst, target)
        if data.fn_end ~= nil then
            data.fn_end(inst, target)
        end
        inst:Remove()
    end)
    inst.components.debuff:SetExtendedFn(function(inst, target, ...)
        if data.fn_again ~= nil then
            data.fn_again(inst, target, ...)
        end
    end)
end

local function MakeBuff(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
            local inst = CreateEntity()

            inst.entity:AddTransform()
            inst.entity:AddNetwork()
            inst.entity:Hide()
            inst.persists = false

            inst:AddTag("CLASSIFIED")
            inst:AddTag("NOCLICK")
            inst:AddTag("NOBLOCK")

            inst.entity:SetPristine()
            if not TheWorld.ismastersim then
                return inst
            end

            --[[ --这一截是无网络组件实体的写法，但为了buff图标机制，加上网络组件
            if not TheWorld.ismastersim then
                --Not meant for client!
                inst:DoTaskInTime(0, inst.Remove)
                return inst
            end

            inst.entity:AddTransform()

            --Non-networked entity
            --inst.entity:SetCanSleep(false)
            inst.entity:Hide()
            inst.persists = false

            inst:AddTag("CLASSIFIED")
            ]]--

            inst:AddComponent("debuff")
            inst.components.debuff.keepondespawn = true
            if data.notimer then
                InitNoTimerBuff(inst, data)
            else
                InitTimerBuff(inst, data)
            end

            if data.fn_server ~= nil then
                data.fn_server(inst)
            end

            return inst
		end,
		data.assets,
		data.prefabs
	))
end

--------------------------------------------------------------------------
--[[ 缓慢恢复精神 ]]
--------------------------------------------------------------------------

local function OnTick_sanityregen(inst, target)
    if
        target.components.health ~= nil and
        not target.components.health:IsDead() and
        not target:HasTag("playerghost")
    then
		if target.components.sanity ~= nil then
			target.components.sanity:DoDelta(2)
		end
    else
        inst.components.debuff:Stop()
    end
end

MakeBuff({
	name = "myth_sanity_regenbuff",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_sanityregen",
    time_default = TUNING.JELLYBEAN_DURATION,
    notimer = nil,
	fn_start = function(buff, target)
        buff.task = buff:DoPeriodicTask(2, OnTick_sanityregen, nil, target)
	end,
	fn_again = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
        buff.task = buff:DoPeriodicTask(2, OnTick_sanityregen, nil, target)
	end,
    fn_end = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 缓慢恢复生命 ]]
--------------------------------------------------------------------------

local function OnTick_healthregen(inst, target)
    if
        target.components.health ~= nil and
        not target.components.health:IsDead() and
        not target:HasTag("playerghost")
    then
        if target.components.health:IsHurt() then
            target.components.health:DoDelta(2, false, "buff_m_regenhealth")
        end
    else
        inst.components.debuff:Stop()
    end
end

MakeBuff({
	name = "buff_m_regenhealth",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_regenhealth",
    time_default = TUNING.JELLYBEAN_DURATION,
    notimer = nil,
	fn_start = function(buff, target)
        buff.task = buff:DoPeriodicTask(2, OnTick_healthregen, nil, target)
	end,
	fn_again = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
        buff.task = buff:DoPeriodicTask(2, OnTick_healthregen, nil, target)
	end,
    fn_end = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 催眠免疫 ]]
--------------------------------------------------------------------------

local timeDef = 90

MakeBuff({
	name = "buff_m_insomnia",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_insomnia",
    time_default = timeDef,
    notimer = nil,
	fn_start = function(buff, target)
        if target.components.grogginess ~= nil then
            target.components.grogginess:ResetGrogginess()
            target.components.grogginess:AddResistanceSource("buff_m_insomnia", 1000) --蘑菇蛋糕都才10
        end
	end,
	fn_again = function(buff, target)
        if target.components.grogginess ~= nil then
            target.components.grogginess:ResetGrogginess()
        end
	end,
    fn_end = function(buff, target)
        if target.components.grogginess ~= nil then
            target.components.grogginess:RemoveResistanceSource("buff_m_insomnia")
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 火焰免疫 ]]
--------------------------------------------------------------------------

MakeBuff({
	name = "buff_m_fireimmune",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_fireimmune",
    time_default = timeDef,
    notimer = nil,
	fn_start = function(buff, target)
        if target.components.health ~= nil then
            target.components.health.externalfiredamagemultipliers:SetModifier("buff_m_fireimmune", 0)
        end
	end,
	fn_again = nil,
    fn_end = function(buff, target)
        if target.components.health ~= nil then
            target.components.health.externalfiredamagemultipliers:RemoveModifier("buff_m_fireimmune")
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 冰冻免疫 ]]
--------------------------------------------------------------------------

MakeBuff({
	name = "buff_m_iceimmune",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_iceimmune",
    time_default = timeDef,
    notimer = nil,
	fn_start = function(buff, target)
        if target.buff_m_iceimmune == nil then -- 说明没有改造过
            if target.components.freezable ~= nil then
                local AddColdness_old = target.components.freezable.AddColdness
                target.components.freezable.AddColdness = function(self, coldness, ...)
                    if not self.inst.buff_m_iceimmune then
                        AddColdness_old(self, coldness, ...)
                    end
                end
            end
        end
        target.buff_m_iceimmune = true
	end,
	fn_again = nil,
    fn_end = function(buff, target)
        target.buff_m_iceimmune = false
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 受到攻击冰冻敌人(单体) ]]
--------------------------------------------------------------------------

local function OnAttacked_iceshield(inst, data)
    if data and data.attacker and data.attacker.components.freezable ~= nil then
        data.attacker.components.freezable:AddColdness(1.5) --巨鹿是2，冰护符是0.67
        data.attacker.components.freezable:SpawnShatterFX()
    end
end

MakeBuff({
	name = "buff_m_iceshield",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_iceshield",
    time_default = timeDef,
    notimer = nil,
	fn_start = function(buff, target)
        target:ListenForEvent("attacked", OnAttacked_iceshield)
	end,
	fn_again = nil,
    fn_end = function(buff, target)
        target:RemoveEventCallback("attacked", OnAttacked_iceshield)
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 增加工作效率 ]]
--------------------------------------------------------------------------

MakeBuff({
	name = "buff_m_workup",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_workup",
    time_default = timeDef,
    notimer = nil,
	fn_start = function(buff, target)
        if target.components.workmultiplier ~= nil then
            target.components.workmultiplier:AddMultiplier(ACTIONS.CHOP,   TUNING.BUFF_WORKEFFECTIVENESS_MODIFIER, buff)
            target.components.workmultiplier:AddMultiplier(ACTIONS.MINE,   TUNING.BUFF_WORKEFFECTIVENESS_MODIFIER, buff)
            target.components.workmultiplier:AddMultiplier(ACTIONS.HAMMER, TUNING.BUFF_WORKEFFECTIVENESS_MODIFIER, buff)
        end
	end,
	fn_again = nil,
    fn_end = function(buff, target)
        if target.components.workmultiplier ~= nil then
            target.components.workmultiplier:RemoveMultiplier(ACTIONS.CHOP,   buff)
            target.components.workmultiplier:RemoveMultiplier(ACTIONS.MINE,   buff)
            target.components.workmultiplier:RemoveMultiplier(ACTIONS.HAMMER, buff)
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 攻击带电 ]]
--------------------------------------------------------------------------

MakeBuff({
	name = "buff_m_atkelec",
	assets = nil,
	prefabs = { "electrichitsparks", "electricchargedfx" },
    time_key = "time_m_atkelec",
    time_default = 150,
    notimer = nil,
	fn_start = function(buff, target)
        if target.components.electricattacks == nil then
            target:AddComponent("electricattacks")
        end
        target.components.electricattacks:AddSource(buff)

        if buff._onattackother == nil then
            buff._onattackother = function(attacker, data)
                --为了不和官方的攻击带电buff重复作用
                if attacker.components.debuffable ~= nil and attacker.components.debuffable:HasDebuff("buff_electricattack") then
                    return
                end
                if data.weapon ~= nil then
                    if data.projectile == nil then
                        --in combat, this is when we're just launching a projectile, so don't do FX yet
                        if data.weapon.components.projectile ~= nil then
                            return
                        elseif data.weapon.components.complexprojectile ~= nil then
                            return
                        elseif data.weapon.components.weapon:CanRangedAttack() then
                            return
                        end
                    end
                    if data.weapon.components.weapon ~= nil and data.weapon.components.weapon.stimuli == "electric" then
                        --weapon already has electric stimuli, so probably does its own FX
                        return
                    end
                end
                if data.target ~= nil and data.target:IsValid() and attacker:IsValid() then
                    SpawnPrefab("electrichitsparks"):AlignToTarget(data.target, data.projectile ~= nil and data.projectile:IsValid() and data.projectile or attacker, true)
                end
            end
            buff:ListenForEvent("onattackother", buff._onattackother, target)
        end
        SpawnPrefab("electricchargedfx"):SetTarget(target)
	end,
	fn_again = function(buff, target)
        SpawnPrefab("electricchargedfx"):SetTarget(target)
    end,
    fn_end = function(buff, target)
        if target.components.electricattacks ~= nil then
            target.components.electricattacks:RemoveSource(buff)
        end
        if buff._onattackother ~= nil then
            buff:RemoveEventCallback("onattackother", buff._onattackother, target)
            buff._onattackother = nil
        end
	end,
    fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 身体发光 ]]
--------------------------------------------------------------------------

local function TryAddWormLight(inst)
	--see wormlight.lua for original code
	if inst.wormlight ~= nil then
		if inst.wormlight.prefab == "wormlight_light_greater" then
			inst.wormlight.components.spell.lifetime = 0
			inst.wormlight.components.spell:ResumeSpell()
			return
		else
			inst.wormlight.components.spell:OnFinish()
		end
	end

	--由于wormlight_light_greater本身就是16分钟的发亮时间，设定里也是16分钟，因此不需要改动
	local light = SpawnPrefab("wormlight_light_greater")
	light.components.spell:SetTarget(inst)
	if light:IsValid() then
		if light.components.spell.target == nil then
			light:Remove()
		else
			light.components.spell:StartSpell()
		end
	end
end

local function CheckWormLight(buff, target)
    if
        target.components.health ~= nil and
        not target.components.health:IsDead() and
        not target:HasTag("playerghost") and
        target.wormlight ~= nil and target.wormlight.components.spell ~= nil
    then
        local spell = target.wormlight.components.spell
		if spell.lifetime ~= nil and spell.duration ~= nil and spell.lifetime < spell.duration then
            if buff.components.timer:TimerExists("buffover") then
                buff.components.timer:SetTimeLeft("buffover", spell.duration-spell.lifetime)
                if not buff.components.timer:IsPaused("buffover") then --这个timer只是为了让buff图标能识别到
                    buff.components.timer:PauseTimer("buffover")
                end
            else
                buff.components.timer:StartTimer("buffover", spell.duration-spell.lifetime)
                buff.components.timer:PauseTimer("buffover")
            end
            return
        end
    end
    buff.components.debuff:Stop()
end

MakeBuff({
	name = "buff_m_glow",
	assets = nil,
	prefabs = { "wormlight_light_greater" },
    time_key = nil,
    time_default = nil,
    notimer = true,
	fn_start = function(buff, target)
        TryAddWormLight(target)
        buff.task = buff:DoPeriodicTask(1, CheckWormLight, nil, target)
	end,
	fn_again = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
        TryAddWormLight(target)
        buff.task = buff:DoPeriodicTask(1, CheckWormLight, nil, target)
    end,
    fn_end = function(buff, target)
        if buff.task ~= nil then
            buff.task:Cancel()
        end
	end,
    fn_server = function(buff)
        buff:AddComponent("timer")
        buff:ListenForEvent("timerdone", function(inst, data)
            if data.name == "buffover" then
                inst.components.debuff:Stop()
            end
        end)
    end,
})

--------------------------------------------------------------------------
--[[ 捉鬼断罪(对邪恶生物与头目级生物增加25%伤害) ]]
--------------------------------------------------------------------------

local function OnHitOther_ghostbuster(inst, data)
    if
        data.target ~= nil and
        (
            data.target:HasTag("epic") or data.target:HasTag("monster") or data.target:HasTag("hostile")
        ) and
        data.target.components.health ~= nil and not data.target.components.health:IsDead()
    then
        local damage = data.damage or data.damageresolved
        if damage > 0 then
            data.target.components.health:DoDelta(-damage*0.25, nil, inst.nameoverride or inst.prefab, nil, inst)
        end
    end
end


local function KillCircleFires(inst)
    --if inst.components.debuffable:HasDebuff("whip_commissioner_buff") then --后期备用兼容复活buff
    --    return
    --end
    if inst.ghostbusteircles_fires then
        for _,v in pairs(inst.ghostbusteircles_fires) do
            if v:IsValid() then
                v:Remove()
            end
	    end
    end

    inst.ghostbusteircles_fires = {}
    if inst.ghostbuste_fire_task then
        inst.ghostbuste_fire_task:Cancel()
        inst.ghostbuste_fire_task = nil
    end
end
local radius = 1
local function UpdateDeerOffsets(inst)
	local pt = inst:GetPosition()
	local theta = inst.ghostbuste_fire_theta
	inst.ghostbuste_fire_theta = inst.ghostbuste_fire_theta + .1
	local num = 3
	for _,v in pairs(inst.ghostbusteircles_fires) do
        if v:IsValid() then
		    local offset = Vector3(radius * math.cos(theta), 1, -radius * math.sin(theta))
		    v.formationpos = pt + offset
		    v.Transform:SetPosition(v.formationpos:Get())
		    theta = theta - (2*PI/num)
        end
	end
end
local function SpawnFires(inst)
    local pos = inst:GetPosition()
    for k = 0 , 240, 120 do 
        local fire = SpawnPrefab("yama_fire_red")
        fire._light.Light:Enable(false)
        fire.Transform:SetPosition(pos.x + radius * math.cos(math.rad(k)), 1,pos.z + radius * math.sin(math.rad(k)))
        table.insert(inst.ghostbusteircles_fires,fire)
    end
end
local function Spawn_Circle_Fires(inst)
    if inst.ghostbusteircles_fires and next(inst.ghostbusteircles_fires) ~= nil then
        return
    end
    inst.ghostbusteircles_fires = {}
    inst.ghostbuste_fire_theta = 0
	SpawnFires(inst)
	inst.ghostbuste_fire_task = inst:DoPeriodicTask(0, UpdateDeerOffsets)
end

MakeBuff({
	name = "buff_m_ghostbuster",
	assets = nil,
	prefabs = nil,
    time_key = "time_m_ghostbuster",
    time_default = 4,
    notimer = nil,
	fn_start = function(buff, target)
        target:ListenForEvent("onhitother", OnHitOther_ghostbuster)
        Spawn_Circle_Fires(target)
	end,
	fn_again = nil,
    fn_end = function(buff, target)
        target:RemoveEventCallback("onhitother", OnHitOther_ghostbuster)
        KillCircleFires(target)
	end,
    fn_server = nil,
})
--ThePlayer.components.debuffable:AddDebuff("buff_m_ghostbuster","buff_m_ghostbuster")
--------------------
--------------------

return unpack(prefs)
