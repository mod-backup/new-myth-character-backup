
local prefabs =
{

}

local assets =
{
    Asset("ANIM", "anim/madameweb_net.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_net.xml"),

}

local ANIM_DATA =
{
    SMALL =
    {
        hit = "cocoon_small_hit",
        hit_combat = "cocoon_small_hit_combat",
        idle = "cocoon_small",
    },

}

local function MakeSpiderDenFn(den_level)
    return function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddGroundCreepEntity()
        inst.entity:AddSoundEmitter()
        --inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        --MakeObstaclePhysics(inst, .5)

        --inst.MiniMapEntity:SetIcon("spiderden_" .. tostring(den_level) .. ".png")

        inst.AnimState:SetBank("madameweb_net")
        inst.AnimState:SetBuild("madameweb_net")
        inst.AnimState:PlayAnimation("net")
        inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
        inst.AnimState:SetLayer(LAYER_BACKGROUND)
        inst.AnimState:SetSortOrder(3)

        inst:AddTag("hive")
        inst:AddTag("NOBLOCK")

        MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.GroundCreepEntity:SetRadius(TUNING.SPIDERDEN_CREEP_RADIUS[2])
        inst:AddComponent("lootdropper")

        MakeMediumBurnable(inst)

        MakeMediumPropagator(inst)

        inst:AddComponent("inspectable")

        inst:AddComponent("hauntable")
        inst.components.hauntable.cooldown = TUNING.HAUNT_COOLDOWN_MEDIUM

        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.DIG)
        inst.components.workable:SetWorkLeft(1)
        inst.components.workable:SetOnFinishCallback(inst.Remove)

        MakeSnowCovered(inst)

        return inst
    end
end


local function ondeploy(inst, pt)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
    local tree = SpawnPrefab("madameweb_spiderden")
    if tree ~= nil then
        tree.Transform:SetPosition(pt:Get())
        inst.components.stackable:Get():Remove()
    end
end

local function onpickup(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
end

local function itemfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("madameweb_net")
    inst.AnimState:SetBuild("madameweb_net")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("cattoy")
    inst:AddTag("silkcontaineritem")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_LARGEITEM

    inst:AddComponent("inspectable")

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

    MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
    MakeSmallPropagator(inst)
    MakeHauntableLaunchAndIgnite(inst)

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_net.xml"

    inst:AddComponent("tradable")

    inst.components.inventoryitem:SetOnPickupFn(onpickup)

    inst:AddComponent("deployable")
    inst.components.deployable.ondeploy = ondeploy

    return inst
end

local function ontimerdone(inst, data)
    inst:Remove()
end
local function reset(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, 0, z, 12, {"madameweb_groundsilk"})
	for i,ent in ipairs(ents) do
        if ent ~= inst and ent.components.timer then
            ent.components.timer:SetTimeLeft("lifttime", 120)
        end
	end
end

local function silk()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddGroundCreepEntity()
    inst.entity:AddNetwork()

    inst:AddTag("fx")
    inst:AddTag("NOBLOCK")
    inst:AddTag("madameweb_groundsilk")
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.GroundCreepEntity:SetRadius(TUNING.SPIDERDEN_CREEP_RADIUS[2])

    inst:AddComponent("timer")
    inst.components.timer:StartTimer("lifttime", 120)
    inst:ListenForEvent("timerdone", ontimerdone)

    inst.ResetBrother = reset
    return inst
end


return Prefab("madameweb_spiderden", MakeSpiderDenFn(1), assets, prefabs),
    Prefab("madameweb_net", itemfn, assets, prefabs),
    Prefab("madameweb_groundsilk", silk),
    MakePlacer("madameweb_net_placer", "madameweb_net", "madameweb_net", "net")