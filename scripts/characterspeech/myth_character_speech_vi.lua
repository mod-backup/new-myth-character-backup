






STRINGS.CHARACTERS.MONKEY_KING = require "speech_wolfgang"
STRINGS.CHARACTERS.NEZA = require "speech_wathgrithr"
STRINGS.CHARACTERS.WHITE_BONE = require "speech_webber"
STRINGS.CHARACTERS.PIGSY = require "speech_wolfgang"
STRINGS.CHARACTERS.YANGJIAN = require "characterspeech/speech_yangjian_vi"
STRINGS.CHARACTERS.MYTH_YUTU = require "characterspeech/speech_yutu_vi"
STRINGS.CHARACTERS.YAMA_COMMISSIONERS = require "characterspeech/speech_yama_commissioners"
STRINGS.CHARACTERS.MADAMEWEB = require "characterspeech/speech_mademeweb"

STRINGS.CHARACTER_TITLES.monkey_king = "Tề Thiên Đại Thánh"
STRINGS.CHARACTER_NAMES.monkey_king = "Tôn Ngộ Không"
STRINGS.CHARACTER_DESCRIPTIONS.monkey_king = [[
* Mỹ Hầu Vương thần thông quảng đại
* Ngoại trừ kim cô bổng, mọi vũ khí khác đều không thuận tay
* Đại náo thiên cung gây họa cho hoa quả sơn, sinh linh đồ thán
* Ăn chay và thích nhất là ăn đào
* Không giỏi tính nước, không thích lông bị ướt
]]
STRINGS.CHARACTER_QUOTES.monkey_king = [["Ngạo nghễ cười giữa trời, thần tiên đều phế vật"]]

--neza
STRINGS.CHARACTER_TITLES.neza = "Na Tra Tam Thái Tử"
STRINGS.CHARACTER_NAMES.neza = "Na Tra"
STRINGS.CHARACTER_DESCRIPTIONS.neza = [[
* Linh Châu chuyển thế, cậu bé thân sen
* Có rất nhiều thần binh pháp bảo
* Tính cách trẻ con nhưng phải làm quan thiên đình
]]
STRINGS.CHARACTER_QUOTES.neza = [["Lấy Mạng đền mạng, sen máu sáng rực trời"]]
STRINGS.NAMES.NEZA = "Neza"

STRINGS.CHARACTER_TITLES.white_bone = "Bạch Cốt Phu Nhân"
STRINGS.CHARACTER_NAMES.white_bone = "Bạch Cốt Phu Nhân"
STRINGS.CHARACTER_DESCRIPTIONS.white_bone = [[
* Thiên niên thi ma thành tinh. Giỏi khống chế xương cốt.
* Tâm địa tàn độc, thích uống máu ăn tim
* Yêu thuật nham hiểm và sống hai mặt
]]
STRINGS.CHARACTER_QUOTES.white_bone = [["Ngàn năm Bạch Cốt hóa Âm Phong"]]

STRINGS.CHARACTER_TITLES.pigsy = "Thiên Bồng Nguyên Soái"
STRINGS.CHARACTER_NAMES.pigsy = "Trư Bát Giới"
STRINGS.CHARACTER_DESCRIPTIONS.pigsy = [[
* Từng là nguyên soái hạ phàm đầu thai làm heo.
* Lẻo mép, sống với heo rất vui vẻ
* Thọ giới tam huân ngũ yếm nên được đặt tên là Bát Giới
* Ham ăn mê ngủ, da thô thịt dày chịu đòn giỏi
* Tay cầm đinh ba, giỏi làm nông nhưng không giỏi làm công
]]
STRINGS.CHARACTER_QUOTES.pigsy = [["Giải tán thì giải tán."]]

STRINGS.CHARACTER_TITLES.yangjian = "Nhị Lang Thần"
STRINGS.CHARACTER_NAMES.yangjian = "Dương Tiễn"
STRINGS.CHARACTER_DESCRIPTIONS.yangjian = [[
* Trời sinh ba mắt, lý trí cực cao
* Xem thường sức mạnh bóng tối, khinh thường ma pháp
* Biến thành chim ưng, dịch chuyển ngàn dặm
* Tay trái dắt chó, tay phải đỡ diều hâu, làm bạn với thần thú
]]
STRINGS.CHARACTER_QUOTES.yangjian = [["Thiên nhãn mở ra vạn yêu khó thoát"]]

STRINGS.CHARACTER_TITLES.myth_yutu = "Ngọc Thố Tiên Tử"
STRINGS.CHARACTER_NAMES.myth_yutu = "Ngọc Thố"
STRINGS.CHARACTER_DESCRIPTIONS.myth_yutu = [[
* Linh lung tiên thố, giảo thố tam quật
* Trường cư nguyệt cung, đảo dược vi chức
* Nhớ nhà da diết, Nguyệt Nga dẫn lối
]]
STRINGS.CHARACTER_QUOTES.myth_yutu = [["Chuyện nghiền thuốc hay là để mai nói đi."]]

STRINGS.CHARACTER_TITLES.yama_commissioners = "Câu Hồn Âm Sai"
STRINGS.CHARACTER_NAMES.yama_commissioners = "Hắc Bạch Vô Thường"
STRINGS.CHARACTER_DESCRIPTIONS.yama_commissioners = [[
* Hắc bạch song sinh, âm gian u minh
* Âm ti tại thế, mệnh tòng Diêm La
* Nhiếp hồn câu phách, dẫn độ hoàng tuyền
]]
STRINGS.CHARACTER_QUOTES.yama_commissioners = "\"Lệ quỷ câu hồn, vô thường đòi mệnh\""

STRINGS.CHARACTER_TITLES.madameweb = "盘丝娘娘"
STRINGS.CHARACTER_NAMES.madameweb = "盘丝娘娘"
STRINGS.CHARACTER_DESCRIPTIONS.madameweb = "*蜘蛛成精，自立为后\n*织网缠丝，丝薄弱火\n*毒狠网牢，见雀张罗"
STRINGS.CHARACTER_QUOTES.madameweb = "\"不如来我盘丝洞中坐坐？\""

STRINGS.NAMES.MONKEY_KING = "Tôn Ngộ Không"
STRINGS.NAMES.MK_PHANTOM = "Tôn Ngộ Không"
STRINGS.NAMES.YANGJIAN = "Dương Tiễn"
STRINGS.NAMES.NEZA = "Na Tra"
STRINGS.NAMES.WHITE_BONE = "Bạch Cốt Phu Nhân"
STRINGS.NAMES.MYTH_YUTU = "Ngọc Thố"
STRINGS.NAMES.PIGSY = "Trư Bát Giới"
STRINGS.NAMES.YAMA_COMMISSIONERS = "Hắc Bạch Vô Thường"

STRINGS.SKIN_NAMES.pigsy_none = "Trư Bát Giới"
STRINGS.SKIN_NAMES.yangjian_none = "Dương Tiễn"
STRINGS.SKIN_NAMES.myth_yutu_none = "Ngọc Thố"
STRINGS.SKIN_NAMES.yama_commissioners_none = "Hắc Bạch Vô Thường"

STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.NZ_PLANT={
	HASONE = "Không thể trồng được chỗ này.",
										}

STRINGS.NAMES.MK_JGB = "Kim Cô Bổng"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MK_JGB = "Là một thanh vũ khí bảo bối óng ánh!"

STRINGS.NAMES.MK_JGB_PILLAR = "Định Hải Thần Châm"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MK_JGB_PILLAR = "Một bổng này dựng lên chọc trời khuấy nước"

STRINGS.NAMES.LOTUS_FLOWER = "Hoa Sen"
STRINGS.RECIPE_DESC.LOTUS_FLOWER = "Lóc thịt trả mẹ, lóc xương trả cha"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LOTUS_FLOWER = "Một bông hoa xinh đẹp."

STRINGS.NAMES.LOTUS_FLOWER_COOKED ="Hoa Sen Nướng"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LOTUS_FLOWER_COOKED = "Mày điên à mà đi nướng hoa sen?"

STRINGS.NAMES.NZ_LANCE ="Hỏa Tiêm Thương"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_LANCE = "Cây thương này ẩn chứa thần lực"
STRINGS.NAMES.NZ_DAMASK ="Hỗn Thiên Lăng"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_DAMASK = "Dải lụa này thật khá!"
STRINGS.NAMES.NZ_RING ="Càn Khôn Quyện"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_RING = "Cái quyện này không tầm thường chút nào."

STRINGS.NAMES.WB_HEART = "Âm Sâm Chi Tâm"
STRINGS.RECIPE_DESC.WB_HEART = "Khiến ngươi hồn lìa khỏi xác"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_HEART = "Trái tim này tà ác ghê gớm quá!!!"

STRINGS.NAMES.BONE_WAND = "Cốt Trượng"
STRINGS.RECIPE_DESC.BONE_WAND = "Gai xương người dưới đất trồi lên"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_WAND = "Nhìn cây trượng nổi hết da gà da vịt."

STRINGS.NAMES.BONE_BLADE = "Cốt Nhận"
STRINGS.RECIPE_DESC.BONE_BLADE = "Khiến con mồi chảy máu tới chết"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_BLADE = "Nhìn cây kiếm nổi hết cả gai ốc."

STRINGS.NAMES.BONE_WHIP = "Cốt Tiên"
STRINGS.RECIPE_DESC.BONE_WHIP = "Cướp da đoạt thịt"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_WHIP = "Nhìn cây roi mà rùng hết cả mình."

STRINGS.NAMES.BONE_PET = "Bạch Cốt Lâu Lâu"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_PET = "Trông ghê vl!"

STRINGS.PGONEATMEAT = "Phật Tổ ở trong tâm."
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD.NOSLEEPWHENRIDER = "Con trâu điên ồn quá, ta không ngủ được!"
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD.NOSLEEPWHENSTARVE = "Ta đói quá ngủ không nổi!" --Đói không ngủ được sẽ nói gì

STRINGS.NAMES.PIGSY_SLEEPBED = "Đệm Rơm"
STRINGS.RECIPE_DESC.PIGSY_SLEEPBED = "Bụng hướng lên trời, ta lại làm tiên"

STRINGS.NAMES.PIGSY_RAKE = "Cửu Xỉ Đinh Ba"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_RAKE = "Ngũ Đế Lục Giáp tương trợ, đinh ba này được mạ vàng"

STRINGS.PIGSY250 = "Ăn no không hẳn là được ngủ... Làm việc chứ gì?"
STRINGS.PIGSY75 = "Dạ dày đang rỗng tuếch, một ngoạm bốn tô cơm"
STRINGS.PIGSY50 = "Ta đói đến mức da bụng chạm da lưng rồi"

STRINGS.NAMES.PIGSY_SOIL = "Ụ Đất"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_SOIL = "Triệu ngàn lúa gạo lớn từ đây!"

STRINGS.NAMES.PIGSY_HAT = "Mặc Lan Mão"
STRINGS.RECIPE_DESC.PIGSY_HAT = "Ngăn cho não Bát Giới khỏi bị lạnh"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_HAT = "Mũ này ấm thật!"
STRINGS.NOPIGSY_RAKE = "Quân tử hữu lễ, không phải của mình" --Người khác cầm đinh ba sẽ nói

STRINGS.RECIPE_DESC.SKYHOUND_TOOTH = "Vòng cổ của Hao Thiên Khuyển"

STRINGS.NAMES.MK_JGB_REC = "Nhất Trụ Kình Thiên"
STRINGS.RECIPE_DESC.MK_JGB_REC = "Một bổng này đủ cho ngươi tan thành mây khói!"

STRINGS.NAMES.YJ_SPEAR = "Tam Tiêm Lưỡng Nhận Đao"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "Tam Phong Như Nhận Hoa Thiên Ngân"

STRINGS.NAMES.YANGJIAN_HAIR = "Tam Sơn Phi Phụng Quán"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "Tam Sơn chiều đến, Phi Phụng gáy vang"

STRINGS.NAMES.YANGJIAN_ARMOR = "Tỏa Tử Thanh Nguyên Giáp"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "Bảo bối băng hàn đến cực điểm, không biết người sở hữu là ai"

STRINGS.NAMES.SKYHOUND = "Hao Thiên Khuyển"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SKYHOUND = "Nghe nói ngươi nuốt được cả Nhật Nguyệt sao?"

STRINGS.NAMES.YJ_SPEAR_NEDDTHUNDER = "Còn phải làm phiền Lôi Công Điện Mẫu giúp ta một tay."

STRINGS.NAMES.YANGJIAN_TRACK = "Triển Sí Hóa Ưng"
STRINGS.RECIPE_DESC.YANGJIAN_TRACK = "Như diều bay lên chín nghìn dặm"
STRINGS.NAMES.YANGJIAN_TRACK_NOHUNGER = "Bụng rỗng làm sao vượt mây xanh"
STRINGS.NAMES.YANGJIAN_TRACK_INHOUSE = "Không gian quá nhỏ không triển được"


STRINGS.NAMES.YT_RABBITHOLE = STRINGS.NAMES.RABBITHOLE
STRINGS.RECIPE_DESC.RABBITHOLE = "Một cái hang xinh xắn"


STRINGS.NAMES.YT_DAOYAO = "Nghiền thuốc" --捣药配方名字
STRINGS.RECIPE_DESC.YT_DAOYAO = "Nghiền thuốc hỗ trợ" --捣药配方描述

STRINGS.NAMES.YT_MOONBUTTERFLY = "Nguyệt Nga Dẫn Lộ"
STRINGS.RECIPE_DESC.MOONBUTTERFLY = "Hoa thành bướm, hướng Quảng Hàn" -- 月娥配方描述
STRINGS.RECIPE_DESC.YT_MOONBUTTERFLY = "Hoa thành bướm, hướng Quảng Hàn" -- 月娥配方描述

STRINGS.MYTH_NOPESTLE = "Ta phải cầm chày thì mới nghiền thuốc được" --捣药提醒

STRINGS.NAMES.MEDICINE_PESTLE_MYTH = "Chày Nghiền Thuốc" --捣药杵名字
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MEDICINE_PESTLE_MYTH = "Một mẩu Dương Chi Ngọc, mùi thuốc bám ngàn năm" --检查描述

STRINGS.YJFLYTS = [[
Chọn người hoặc điểm đánh dấu để đến,
20 giây không thao tác tự động rơi xuống đất.
]]
STRINGS.MONKEY_COOK = "Luyện chế"
STRINGS.MONKEY_HARVEST = "Thu hoạch"
STRINGS.MJ_JGB = "Nặng quá đi! Đây không phải thứ ta có thể cầm được."
STRINGS.NE_TEDING = "Hắn không nghe ta sai bảo"
STRINGS.MKONEATMEAT = "Rượu thịt cào ruột quá!"
STRINGS.MKINSANDSTORM = "Bão cát mạnh quá!"
STRINGS.YJINSANDSTORM = "Cơn bão cát đáng ghét này!"
STRINGS.MKDROPWEAPON = "Binh khí này nhẹ quá!"
STRINGS.ACTIONS.CASTAOE.MK_JGB = "Thân Ngoại Thân Pháp"
STRINGS.ACTIONS.CASTAOE.NZ_LANCE = "Ba Đầu Sáu Tay"
STRINGS.ACTIONS.CASTAOE.YJ_SPEAR = "Khu Lôi Sách Điện"
STRINGS.ACTIONS.CASTAOE.PENNANT_COMMISSIONER = "Chiêu hồn"

STRINGS.WBRECIPE = "Bạch Cốt"
STRINGS.NZ_PLANT = "Gieo Trồng"
STRINGS.USE_GOURD = "Sử Dụng"
STRINGS.MKBIANHUAN = "Biến Hình"
STRINGS.WBBIANHUAN = "Phun Sương"
STRINGS.PIGBIANHUAN = "Phòng Thủ"
STRINGS.REPAIR_BONE = "Sửa Chữa"
STRINGS.WHITE_BONE_RIGHT = "Lấy Hồn"
STRINGS.WHITE_BONE_LEFT = "Lấy Xác"
STRINGS.BONEPETSFULL = "Tàng tàng năm chân là đủ"

STRINGS.MYTH_PICKUP_PILLAR = "Thu Hồi"
STRINGS.YTBIANHUAN = "Độn Thổ"
STRINGS.YTBIANHUAN_OUT  ="Chui lên"

STRINGS.NAMES.YANGJIAN_BUZZARD_SPAWN  ="Gọi Ưng"
STRINGS.RECIPE_DESC.YANGJIAN_BUZZARD_SPAWN = "Mắt vàng cánh bạc, Vồ bắt hàng đầu"

STRINGS.NAMES.YANGJIAN_BUZZARD  ="Ngạo Thiên Ưng"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YANGJIAN_BUZZARD= "Chẳng lẽ đây là bưu cú của phương đông?"

STRINGS.NAMES.WB_ARMORBLOOD  = "Huyết Sắc Nghê"
STRINGS.RECIPE_DESC.WB_ARMORBLOOD = "Đầy trời màu máu, nhuộm đỏ áo ta"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORBLOOD = "Chim quyên khóc máu, gió xuân chẳng về"

STRINGS.NAMES.WB_ARMORBONE  = "Kiên Cốt Phi"
STRINGS.RECIPE_DESC.WB_ARMORBONE = "Xương trắng nặng nề chắn thân ta"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORBONE = "Áo choàng này rốt cục là đổi bằng bao nhiêu sinh mạng?"

STRINGS.NAMES.WB_ARMORLIGHT  = "Doanh Phong Trừu"
STRINGS.RECIPE_DESC.WB_ARMORLIGHT = "Cánh chim đầy gió, ta đạp mây xanh"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORLIGHT = "Gió mát không nhốt được, lồng lộng giữa đêm trăng"

STRINGS.NAMES.WB_ARMORSTORAGE  = "Ôn Huyền Bào"
STRINGS.RECIPE_DESC.WB_ARMORSTORAGE = "Y bào phần phật, tay áo chứa trời"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORSTORAGE = "Dưới vạt áo có cả Càn Khôn"

STRINGS.NAMES.WB_ARMORSTORAGE_BACK  = STRINGS.NAMES.WB_ARMORSTORAGE

STRINGS.NAMES.WB_ARMORFOG  = "Vụ Ẩn Thường"
STRINGS.RECIPE_DESC.WB_ARMORFOG = "Gió ẩn vội vã, sương ẩn từ từ"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORFOG = "Ở đây sương khói mờ nhân ảnh, mỹ nhân soi gương tiếc thanh xuân"

STRINGS.NAMES.WB_ARMORGREED  = "Bất Phế Y"
STRINGS.RECIPE_DESC.WB_ARMORGREED = "Ta, vĩnh viễn không thỏa mãn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORGREED = "Cái áo kia như một con yêu quái tham lam"

STRINGS.WB_ARMORSTR = {
	WB_ARMORBLOOD = "Mát lạnh, tăng khả năng hút máu của Bạch Cốt",
	WB_ARMORBONE = "Mát lạnh, tăng khả năng phòng ngự, nhưng khiến Bạch Cốt mất năng lực hút máu",
	WB_ARMORLIGHT = "Mát lạnh, tăng cao tốc độ di chuyển",
	WB_ARMORSTORAGE = "Ấm áp, có thể mang theo nhiều vật phẩm, tự động nhặt vật phẩm cùng loại",
	WB_ARMORFOG = "Ấm áp, mở rộng phạm vi sương mù, Bạch Cốt trong sương mù tăng tốc độ di chuyển, tăng một ít giáp và khả năng hút máu",
	WB_ARMORGREED = "Ấm áp, tham lam khiến ngươi nhận thêm nhiều chiến lợi phẩm, tử thi tồn tại lâu hơn",
}

STRINGS.NAMES.BONE_MIRROR  = "Bạch Cốt Yêu Kính"
STRINGS.RECIPE_DESC.BONE_MIRROR = "Gương sáng điểm trang bóng giai nhân"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_MIRROR = {
	MAX = "Lông mày nhợt nhạt đáng xấu hổ, hải đường ngoài kia nở hay chưa?",
	MIDDLE = "Phù dung khó sánh bóng giai nhân",
	SMALL = "Gương thì xấu, mặt thì thô",
}

STRINGS.WB_MIRROR_NOSPACE = "Không có chỗ trống"
STRINGS.WB_MIRROR_NOTENOUGH = "Không đủ nguyên liệu"
STRINGS.WB_MIRROR_HASONE = "Đã có áo choàng cùng loại!"
STRINGS.WB_MIRROR_NEED  = "Gương cấp "
STRINGS.WB_MIRROR_NEED_MAX  = "Cấp bậc tối đa!"
STRINGS.WB_MIRROR_NEED_UP = "Lên cấp cần:"
STRINGS.WB_MIRROR_CURRENT_SKIN = "Đang dùng"
STRINGS.NAMES.MK_DSF  = "Định Thân Pháp"
STRINGS.RECIPE_DESC.MK_DSF = "Định!"
STRINGS.WHITE_BONE_BLINK = "Vụ Ẩn"
STRINGS.PIGSY_TACKLE = "刚鬣横冲"

--无常
STRINGS.NAMES.HAT_COMMISSIONER_BLACK = "Vô Thường Quan Mão"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_COMMISSIONER_BLACK = "Ban ngày sao hiểu được đêm đen"
STRINGS.NAMES.HAT_COMMISSIONER_WHITE = "Vô Thường Quan Mão"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_COMMISSIONER_WHITE = "Ban đêm sao hiểu được sáng ngày"
STRINGS.NAMES.WHIP_COMMISSIONER = "Câu Hồn Tác"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WHIP_COMMISSIONER = "Chuyên câu ác hồn, cũng trừng tà phách"
STRINGS.NAMES.TOKEN_COMMISSIONER = "Trấn Hồn Lệnh"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.TOKEN_COMMISSIONER = {
    GENERIC = "Bảo vật trấn áp tà ma hù dọa người sống",
	SPELL = "Âm giới mượn đường! Người dương mau tránh!",
    NOSOUL = "Không ác không làm được",
	NOHAT = "Không ác mão không làm được",
}
STRINGS.NAMES.PENNANT_COMMISSIONER = "Chiêu Hồn Phiên"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PENNANT_COMMISSIONER = {
    GENERIC = "Chiêu hồn hấp phách, dẫn độ hoàng tuyền",
	SPELL = "Giờ lành dẫn độ! Hồn phách đến mau!",
    NOSOUL = "Không thiện cũng vô dụng",
	NOHAT = "Không thiện mão đành vô dụng",
}
STRINGS.NAMES.BELL_COMMISSIONER = "Nhiếp Hồn Linh"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BELL_COMMISSIONER = {
    GENERIC = "Bảo vật trấn áp tà ma hù dọa người sống",
	SPELL = "Âm giới mượn đường! Nhiếp hồn vào mộng!",
    NOSOUL = "Không thiện cũng vô dụng",
	NOHAT = "Không thiện mão đành vô dụng",
}
STRINGS.NAMES.SOUL_GHAST = "Ác Hồn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOUL_GHAST = "Ngày sống điều ác, không muốn quay đầu, chết thành thế này"
STRINGS.NAMES.SOUL_SPECTER = "Thiện Hồn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOUL_GHAST = "Ngày sống hành thiện, lòng mang hảo ý, hồn sạch như này"
STRINGS.NAMES.GHOST_IRRITATED = "Oán Hồn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOST_IRRITATED = "Ác hồn không bị gò bó, càng trở nên tác oai tác quái"
STRINGS.NAMES.GHOST_CLEMENT = "Ân Hồn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOST_CLEMENT = "Thiện tai, vong hồn báo ân sẽ sớm đầu thai chuyển thế"

STRINGS.YAMARECIPE = "Âm Ti"

STRINGS.NAMES.MYTH_YAMA_STATUE1 = "Pho Tượng Diêm La"
STRINGS.RECIPE_DESC.MYTH_YAMA_STATUE1 = "Hồn xa xăm về đường thăm thẳm"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE1 = "Một pho tượng vô danh"

STRINGS.NAMES.MYTH_YAMA_STATUE2 = "Tượng Thờ Diêm La"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE2 = "Có chút thờ cúng bấy bá"

STRINGS.NAMES.MYTH_YAMA_STATUE3 = "Miếu Thờ Diêm La"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE3 = "Vài viên ngói vừa che mưa nắng"

STRINGS.NAMES.MYTH_YAMA_STATUE4 = "Đền Thờ Diêm Vương"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE4 = "Đứng đầu Thập Điện, vua của địa ngục"

STRINGS.NAMES.MYTH_BAHY = "Bỉ Ngạn Hoàn Dương"
STRINGS.RECIPE_DESC.MYTH_BAHY = "Bỉ ngạn đỏ thắm, mau chóng về trần"

STRINGS.NAMES.MYTH_HIGANBANA_ITEM = "Hoa Bỉ Ngạn"
STRINGS.RECIPE_DESC.MYTH_HIGANBANA_ITEM = "Bỉ ngạn đỏ thắm, mau chóng về trần"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_ITEM = "Bỉ ngạn không lá nở hoa, kiếp sau gặp lại đã là nghìn năm"

STRINGS.NAMES.MYTH_HIGANBANA_REVIVE = "Hoa Bỉ Ngạn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_REVIVE = "Bỉ ngạn không lá nở hoa, kiếp sau gặp lại đã là nghìn năm"

STRINGS.NAMES.MYTH_HIGANBANA_TELE = "Hoa Bỉ Ngạn"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_TELE = "Bỉ ngạn không lá nở hoa, kiếp sau gặp lại đã là nghìn năm"

STRINGS.NAMES.MYTH_CQF = "Xuất Khiếu Phù"
STRINGS.RECIPE_DESC.MYTH_CQF = "Bùa hỗ trợ Nguyên Thần Xuất Khiếu"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_CQF = "Bùa hỗ trợ Nguyên Thần Xuất Khiếu"

STRINGS.YAMA_COMMISSIONERS_ISRAINING = "Trời sắp mưa, không đi được!"
STRINGS.YAMA_COMMISSIONERS_ISRAINING_BLACK = "Trời sắp mưa, chi bằng ở lại!"

STRINGS.YAMA_CANTDIGMOUNT = "Tội đào mộ, xử cực hình!"
STRINGS.YAMA_BLACKDEAD = "Lão Phạm ngươi lại lười biếng rồi"
STRINGS.YAMA_WHITEDEAD = "Lão Tạ ngươi có lẽ cần nghỉ ngơi chút"

STRINGS.NAMES.MYTH_BAMBOO_BASKET = "Trúc Dược Lâu"
STRINGS.RECIPE_DESC.MYTH_BAMBOO_BASKET = "Ngày tàn nắng tắt, hái thuốc chưa ngơi"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_BAMBOO_BASKET = "Hương thuốc liên miên bất tận"
--STRINGS.CHARACTERS.MYHT_YUTU.DESCRIBE.MYTH_BAMBOO_BASKET = "Ta không phải đi hái nấm"

STRINGS.MYTH_NOENOUGHCOIN = "Cần 40 đồng tiền và một sợi thừng"

--------------------------------------------------------------------------
--[[ 玉兔重做tip ]]
--------------------------------------------------------------------------

STRINGS.TAB_JADEHARE = "Thiềm Cung Kỹ Nghệ"

STRINGS.NAMES.POWDER_M_HYPNOTICHERB = "Thảo Tham Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_HYPNOTICHERB = "An thần đại bổ"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_HYPNOTICHERB = "Ngươi có hóa thành bột ta cũng nhận ra. Nhân sâm!"
STRINGS.NAMES.POWDER_M_LIFEELIXIR = "Tê Nhung Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_LIFEELIXIR = "Trường sinh linh dược"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_LIFEELIXIR = "Nghiền thành bột chẳng những đại bổ lại còn thêm phép thuật"
STRINGS.NAMES.POWDER_M_CHARGED = "Kinh Quyết Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_CHARGED = "Điện giật kinh người"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_CHARGED = "Sau khi nghiền thành bột lại dẫn điện kinh người"
STRINGS.NAMES.POWDER_M_IMPROVEHEALTH = "Hoạt Huyết Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_IMPROVEHEALTH = "Lưu thông máu ứ"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_IMPROVEHEALTH = "Mùi rất dịu ngọt, đúng không?"
STRINGS.NAMES.POWDER_M_COLDEYE = "Hàn Mâu Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_COLDEYE = "Tà khách lạnh như thù"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_COLDEYE = "Vốn dĩ định tạo một con tuần lộc mà thất bại"
STRINGS.NAMES.POWDER_M_BECOMESTAR = "Dạ Minh Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_BECOMESTAR = "Bột sáng như trăng"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_BECOMESTAR = "Bất kể bé trai hay bé gái, cũng nên thoa trước khi đi ngủ nha"
STRINGS.NAMES.POWDER_M_TAKEITEASY = "Bài Úc Dược Phấn"
STRINGS.RECIPE_DESC.POWDER_M_TAKEITEASY = "Trừ ưu giải phiền"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_TAKEITEASY = "Thật ra còn có thể dùng pha trà"

--ACTIONS.MYTH_USE_INVENTORY是主题mod里定义的，但本mod后于其加载
STRINGS.ACTIONS.MYTH_USE_INVENTORY.GUITAR = "Biểu diễn"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.GUITAR_NOTE = "Bắt nhạc"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.WHIP = "鬼火缠身"

STRINGS.NAMES.GUITAR_JADEHARE = "Oánh Nguyệt Tỳ Bà"
STRINGS.RECIPE_DESC.GUITAR_JADEHARE = "Bổng trầm thánh thót, lệ tràn bờ mi"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GUITAR_JADEHARE = {
	GENERIC = "Thiềm cung thăm thẳm, vẳng nghe tiếng tỳ bà",
	UNKOWN = "Ta còn chưa học bài này",
	NOSONG = "Biểu diễn bài nào đâu?",
	NOGUITAR = "Bài ca thật tuyệt, nhưng còn thiếu cây đàn.",
	NOCOST = "Ta mệt quá không chơi nổi"
}
STRINGS.CHARACTERS.MYTH_YUTU.DESCRIBE.GUITAR_JADEHARE = {
	GENERIC = "Đây là Hằng Nga tỷ tỷ cho ta mượn, hâm mộ chưa!",
	UNKOWN = "Bài này bổn cô nương chưa biết.",
	NOSONG = "Ta phải tìm một bài để tập đã.",
	NOGUITAR = "Đứt dây rồi! Đùa đấy, đàn còn không có lấy đâu ra dây!",
	NOCOST = "Sức đâu nữa mà gảy đàn!"
}

STRINGS.CANT_USEGUITAR_RIDING = "Ngồi lưng trâu thổi sáo được chứ đàn làm sao"
STRINGS.CANT_USEDAOYAO_RIDING = "Thuốc quý thế này sàng qua sàng lại đổ hết"

STRINGS.NAMES.SONG_M_WORKUP = "《Điền Trung Nhạc》"
STRINGS.RECIPE_DESC.SONG_M_WORKUP = "Sáng đạp bằng ô uế, tối về đội trăng sao"
STRINGS.NAMES.SONG_M_INSOMNIA = "《Xuân Quang Khúc》"
STRINGS.RECIPE_DESC.SONG_M_INSOMNIA = "Tình nhân buồn đêm vắng"
STRINGS.NAMES.SONG_M_FIREIMMUNE = "《Dục Hỏa Tấu》"
STRINGS.RECIPE_DESC.SONG_M_FIREIMMUNE = "Lửa đốt ngày đêm lại bình thường"
STRINGS.NAMES.SONG_M_ICEIMMUNE = "《Hàn Phong Điều》"
STRINGS.RECIPE_DESC.SONG_M_ICEIMMUNE = "Bỗng như một đêm gió xuân về"
STRINGS.NAMES.SONG_M_ICESHIELD = "《Mộng Phi Sương》"
STRINGS.RECIPE_DESC.SONG_M_ICESHIELD = "Sông băng lạnh lẽo đàn ngưng tiếng"

STRINGS.NAMES.SONG_M_NOCURE = "《Oán Triền Thân》"
STRINGS.RECIPE_DESC.SONG_M_NOCURE = "Chẳng trách gió xuân chỉ trách mình"
STRINGS.NAMES.SONG_M_WEAKATTACK = "《Xuân Phong Hóa Vũ》"
STRINGS.RECIPE_DESC.SONG_M_WEAKATTACK = "Chờ sáng ngày mai bán hạnh hoa"
STRINGS.NAMES.SONG_M_WEAKDEFENSE = "《Bá Vương Tá Giáp》"
STRINGS.RECIPE_DESC.SONG_M_WEAKDEFENSE = "Tướng quân mượn giáp nhập cửu trùng"
STRINGS.NAMES.SONG_M_NOLOVE = "《Lưu Thủy Vô Tình》"
STRINGS.RECIPE_DESC.SONG_M_NOLOVE = "Ly ca một khúc oán tà dương"
STRINGS.NAMES.SONG_M_SWEETDREAM = "《Dạ Lan Dao》"
STRINGS.RECIPE_DESC.SONG_M_SWEETDREAM = "Đêm khuya một khắc bi ai trong lòng"


--盘丝的部分
--吊丝
STRINGS.NAMES.MADAMEWEB_FEISHENG = "盘丝吊"
STRINGS.RECIPE_DESC.MADAMEWEB_FEISHENG = "一丝虚空挂"
STRINGS.RECIPE_DESC.SILK = "我腹能得几多丝？"

STRINGS.NAMES.MADAMEWEB_NET = "蛛丝罗网"
STRINGS.RECIPE_DESC.MADAMEWEB_NET = "无处可逃"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_NET = "天罗何在？地网何织？"

STRINGS.NAMES.MADAMEWEB_SPIDERDEN = "蛛丝罗网"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDERDEN = "天罗何在？地网何织？"

STRINGS.NAMES.MADAMEWEB_PITFALL = "蛛网陷阱"
STRINGS.RECIPE_DESC.MADAMEWEB_PITFALL = "百丝杀气生 "
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL = "你要来试试吗？"

STRINGS.NAMES.MADAMEWEB_PITFALL_GROUND = "蛛网陷阱"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFF_GROUND = "你要来试试吗？"

STRINGS.NAMES.MADAMEWEB_PITFALL_TRAP = "蛛网陷阱"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL_TRAP = "里面有什么呢？"

STRINGS.NAMES.MADAMEWEB_PITFALL_COCOON = "困顿之茧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL_COCOON = "它现在动弹不得"

STRINGS.NAMES.MADAMEWEB_STINGER = "盘丝蜂针"
STRINGS.RECIPE_DESC.MADAMEWEB_STINGER = "丝悬针尾"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_STINGER = "蜂针缠丝穿心过"

STRINGS.NAMES.MADAMEWEB_POISONSTINGER = "盘丝毒针"
STRINGS.RECIPE_DESC.MADAMEWEB_POISONSTINGER = "毒缠针头"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_POISONSTINGER = "毒沁针头索命回"

STRINGS.NAMES.MADAMEWEB_BEEMINE = "毒蜂茧"
STRINGS.RECIPE_DESC.MADAMEWEB_BEEMINE = "先去里面呆着"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_BEEMINE = "让我看看里面的宝贝"

STRINGS.NAMES.MADAMEWEB_BEE = "盘丝毒蜂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_BEE = "沾染毒气的蜜蜂"

STRINGS.NAMES.MADAMEWEB_POISONBEEMINE = "盘丝蛛卵"
STRINGS.RECIPE_DESC.MADAMEWEB_POISONBEEMINE = "应时而动，适时而谋"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_POISONBEEMINE = "还不是孵化的时机"

STRINGS.NAMES.MADAMEWEB_SPIDERHOME = "盘丝蛛巢"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDERHOME = "孵化毒蛛的温床"

STRINGS.NAMES.MADAMEWEB_SPIDER_WARRIOR = "盘丝毒蛛"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDER_WARRIOR = "这些可不是好惹的"

STRINGS.NAMES.MADAMEWEB_DETOXIFY = "解毒散"
STRINGS.RECIPE_DESC.MADAMEWEB_DETOXIFY = "以毒攻毒"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_DETOXIFY = "清热解毒的良药"

STRINGS.NAMES.MADAMEWEB_ARMOR = "盘丝披肩"
STRINGS.RECIPE_DESC.MADAMEWEB_ARMOR = "也许可以"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_ARMOR = "肩上霓裳美若珠"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON = "蛛丝茧袋"
STRINGS.RECIPE_DESC.MADAMEWEB_SILKCOCOON = "绵绵絮絮，长存其中"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON = "要来猜猜是什么吗？"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON_PACKED = "盘丝织袋"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON_PACKED = "且待明日，适时二开"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON_HANGED = "盘丝悬茧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON_HANGED = "悬空的蛛丝所结，不知藏有何物"

STRINGS.NAMES.MADAMEWEB_SILKVALUELOW = "再等等，妾身还需要点时间"

STRINGS.NAMES.HUA_INTERNET_NODE = "盘丝标点"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE = "特制的节点，可以承载有韧性的蛛丝"
STRINGS.NAMES.HUA_INTERNET_NODE_SEA = "海丝标点"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_SEA = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_INTERNET_LINK = "蛛丝"

STRINGS.NAMES.HUA_INTERNET_NODE_ITEM = "盘丝滑索"
STRINGS.RECIPE_DESC.HUA_INTERNET_NODE_ITEM = "编织你的蛛网吧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_ITEM = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_INTERNET_NODE_SEA_ITEM = "海丝滑索"
STRINGS.RECIPE_DESC.HUA_INTERNET_NODE_SEA_ITEM = "编织你的蛛网吧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_SEA_ITEM = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_FAKE_SPIDER_SHOE = "步丝履"
STRINGS.RECIPE_DESC.HUA_FAKE_SPIDER_SHOE = "体验蜘蛛的感觉"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_FAKE_SPIDER_SHOE = "等等,这地有点滑"


STRINGS.HUA_INTERNET_BUILDER = {
    FAILED_DIST = "距离太远，无法建造",
    FAILED_EXIST = "已有连线，无法建造",
    FAILED_PREFAB = "缺少 prefab：",
    FAILED_MATERIAL = "材料不足，无法建造",
}
STRINGS.HUA_INTERNET_USE = {
    FAILED_NO_LINK = "附近没有直达目的地的连线",
}

STRINGS.NAMES.MADAMEWEB_CANTSILKFLY = "无处可悬"
STRINGS.ACTIONS.DEPLOY.SILKCOCOON = "悬挂"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.SPIDERDEN = "降级"
STRINGS.MYTH_FEIWEN_STR = "薄丝飞吻"
STRINGS.MYTH_SILKFLY_STR = "落地"
STRINGS.NAMES.MYTHTUMBLE = "摔落"
STRINGS.MADABIANHUAN = "布网"
STRINGS.MURDERED_BY_MADAMEWEB = "吞噬"
STRINGS.MYTH_SILKFLY_DOWN = "盘丝坠"
STRINGS.MYTH_INTERNET_RIGHT = "滑行"
STRINGS.MYTH_INTERNET_LEFT = "连线"

STRINGS.MYTH_SKILL_HY = "火眼金睛"
STRINGS.MYTH_SKILL_YZQT = "一柱擎天"
STRINGS.MYTH_SKILL_WBBS ="美人画皮"
STRINGS.MYTH_SKILL_PIGBS ="刚鬣本相"
STRINGS.MYTH_SKILL_HUAYING ="展翅化鹰"
STRINGS.MYTH_SKILL_TIANYAN ="天眼"
STRINGS.MYTH_SKILL_YMBS ="黑白双生"
STRINGS.MYTH_SKILL_MABS ="蛛皇本相"
STRINGS.MYTH_SKILL_PIPA ="演奏"

--===========这里开始是 一些专属物品的不同的检查用的

local speech = {
	--猴子的
	monkey_king = {
		HEAT_RESISTANT_PILL = "Con tê giác già kia chống nắng chống lửa thật dễ dàng!",  --避火丹
		COLD_RESISTANT_PILL = "Con tê giác già kia thật không sợ mưa gió lạnh lùng!",  --避寒丹
		DUST_RESISTANT_PILL = "Con tê giác già kia thế mà không sợ cát bay đầy trời bụi trần khói độc",  --避尘丹
		FLY_PILL = "Hát dài khinh mặt đất, vạn dặm bước ngạo trời",  --腾云
		BLOODTHIRSTY_PILL = "Cái gì mà điên cuồng như yêu tinh vậy?",  --嗜血
		CONDENSED_PILL = "Cha, hương vị này thật khiến nâng cao tinh thần!",  --凝神
		THORNS_PILL = "Hahaha, còn ai chui hàng rào giỏi hơn Lão Tôn ta?", --荆棘
		ARMOR_PILL = "Lão Tôn ta mình đồng da sắt!", --壮骨
		DETOXIC_PILL = "Bách độc bất xâm!", --化毒

		PEACH = "Cái này là Lão Tôn ta vào vườn Bàn Đào hái được",  --桃子
		BIGPEACH =  "Hay cho một quả Đại Bàn Đào, của Lão Tôn!", --大桃子
		PEACH_COOKED = "Nướng lên bay sạch tiên khí!",  --烤桃子
		PEACH_BANQUET = "Hử, ngẫm lại hậu quả năm đó Vương Mẫu Nương Nương làm tiệc Bàn Đào không mời ta!" , --蟠桃大会
		PEACH_WINE = "Đây là rượu chay của thần tiên, ta cũng không có phá giới!",  --蟠桃素酒
		MK_JGB = "Như Ý Kim Cô Bổng, một vạn ba nghìn năm trăm cân",  --金箍棒
		MK_JGB_PILLAR = 'Bổng tên Như Ý, tùy theo ý ta', --定海神针
		HONEY_PIE = "Chính là lương khô đem đi Tây Thiên",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Hay cho một phần cơm chay thịnh soạn" , --素斋
		CASSOCK = "Adidas Phật...",  --袈裟
		KAM_LAN_CASSOCK = "Bảo bối này Quan Âm ban cho sư phụ.",  --锦襕袈裟

		GOLDEN_HAT_MK = "Năm trăm năm rồi...",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Còn ai dám chống Lão Tôn ta?",  --大圣战甲

		MK_BATTLE_FLAG = "Hehe! Chiến kỳ thật đẹp! Phải viết lên 4 chữ \"Tề Thiên Đại Thánh\"",  --旗子
		MK_BATTLE_FLAG_ITEM = "Hehe! Chiến kỳ thật đẹp! Phải viết lên 4 chữ \"Tề Thiên Đại Thánh\"" , --旗子

		XZHAT_MK  = "Ta không tin Bồ Tát lần nào nữa đâu!",      --行者帽


		NZ_LANCE =  "Cây thương này sao bằng cây bổng của Lão Tôn!" ,     --火尖枪
		NZ_DAMASK =  "Lấy nhu khắc cương",     --混天绫
		NZ_RING =  "Thằng bé này có cái vòng cũng đánh rơi được.",     --乾坤圈

		PIGSY_RAKE = "Là cái bồ cào của Lão Trư", --九齿钉耙
		PIGSY_HAT = "Là cái mũ của thằng ngu", -- 墨兰帽
		GOURD = "Thần tiên tạo ra loại cây này", --葫芦
		GOURD_COOKED = "Ơ, mùi thơm có chút ý tứ" , --烤葫芦
		GOURD_SOUP = "Canh này không tồi" , --葫芦汤
		GOURD_OMELET = "Có vị trứng chim" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Rượu ngon, hồ lô đẹp", --酒葫芦
		PILL_BOTTLE_GOURD = "Đan dược của Lão Quân thường trông giống người đậu phộng", --丹药葫芦
		BANANAFAN = "Giỏi mượn giỏi trả", --芭蕉扇
		LAOZI_SP = "Bùa gọi lão quan nhân?", --急急如律令
		LAOZI = "Hehe! Lão quan nhân! Đã lâu không gặp!", --老子
		WB_HEART =  "Trái tim yêu quái!" , --阴森之心
		BONE_WAND =  "Người chết không yên!" , --骨杖
		BONE_BLADE =  "Cây đao thật đê tiện!" , --骨刃
		BONE_WHIP =  "Yêu tinh đê tiện kéo gân rút xương!" , --骨鞭
		BONE_PET =  "Ăn một gậy của ta đi rồi gáy!" , --小骷髅

		LOTUS_FLOWER = "Trên Hoa Quả Sơn cũng có một cái hồ sen." ,--莲花
		LOTUS_FLOWER_COOKED = "Lão Tôn ta càng thích đồ ăn giòn ngọt", -- 烤莲花

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Lửa thiêu lửa đốt, ta vì tránh li hỏa phải trốn vào Cung Tốn",
			EMPTY = "Haha. Cái này so với lò của Lão Quân cũng không khác bao nhiêu.",
			DONE = "Haha. Không uổng công ngươi trải ba kiếp trong lò.",
		},

		YJ_SPEAR = "Năm đó còn chưa chán! Lại đây đánh thêm ba trăm hiệp đi!",--三尖两刃刀
		YANGJIAN_HAIR = "Hừm. Phụng Sí Tử Kim Quán của ta mới tính là đẹp.",--三山飞凤冠
		YANGJIAN_ARMOR = "Hừm. Còn không bằng ba phần uy phong của Lão Tôn.",--锁子清源甲
		SKYHOUND = "Nhả chân ta ra con chó!",--哮天犬
		SKYHOUND_WARG = "Bất quá chỉ là một con chó đá, Lão Tôn cho một bổng là vỡ ngay!", --月石哮天犬

		MONKEY_KING = "Hả? Ngươi chính là Lục Nhĩ Mi Hầu!", --猴子
		MK_PHANTOM = "Hehe. Ta là Tôn Hành Giả, còn ngươi là Giả Hành Tôn.",
		PIGSY = "Thằng ngu này còn không đứng lên làm việc.", -- 猪猪
		NEZA = "Na Tra tiểu đệ hôm nay có rảnh ghé chơi uống vài chén.", --那咋
		WHITE_BONE = "Ba lần bảy lượt gạt sư phụ ta, ăn của ta một bổng!", --白骨
		YANGJIAN = "Chân Quân sao nay rảnh rỗi hạ phàm tới đây?", --杨戬
		MYTH_YUTU = "Ngươi không ở Quảng Hàn Cung hầu hạ Hằng Nga, lại trốn ra đây chơi?",
		YAMA_COMMISSIONERS = "Khi khác cho Lão Tôn mượn hình dáng của các ngươi biến thân chơi",

		MYTH_INTERIORS_LIGHT = "Biết bao nhiêu năm, mà dầu thắp còn chưa dùng hết.",
		MYTH_INTERIORS_BED = "Sư phụ hiện giờ nghỉ ngơi ở đâu.",
		MYTH_INTERIORS_GZ = "Sư phụ rất thích bức tranh này",
		MYTH_INTERIORS_PF = "Sư phụ rất thích loại cây này",
		MYTH_INTERIORS_XL = "Để ta lần nữa thay sư phụ thắp hương",
		MYTH_INTERIORS_ZZ = "Chính sư phụ đã dạy Lão Tôn biết chữ",
		MYTH_FOOD_ZPD = "Phì. Đưa cho thằng ngu ăn đi.",
		MYTH_FOOD_NX = "Quả ngon kem ngon, vị thật ngon",
		MYTH_FOOD_LXQ = "Lão Tôn đúng là rất thích trái cây",
		MYTH_FOOD_FHY = "Chưa kịp bảo lão Long Vương đãi Lão Tôn ta một bữa.",
		MYTH_FOOD_HYMZ = "Đồ ăn cũng khắc hình được sao",
		MYTH_BANANA_LEAF = "Đợi Lão Tôn kết một cái quần",
		MYTH_BANANA_TREE = "Lá xanh che mất ngập tràn trái cây",
		MYTH_ZONGZI_ITEM1 = "Vẫn là bánh ú ngọt này vị mềm ngon miệng",
		MYTH_ZONGZI_ITEM2 = "Lại bỏ thêm thịt? Ăn không ngon, ăn không ngon!",
		BANANAFAN_BIG = "Giống cái quạt năm đó chị dâu cho ta mượn xài",

	},




	--八戒的
	pigsy = {
		HEAT_RESISTANT_PILL = "Lạnh lạnh mát mát, đặt trước ngực là thích hợp nhất",  --避火丹
		COLD_RESISTANT_PILL = "Đừng để Bật Mã Ôn kia biết là được",  --避寒丹
		DUST_RESISTANT_PILL = "Hehe. Khỏi phải giặt quần áo",  --避尘丹
		FLY_PILL = "Miên Hoa Vân cũng là đằng vân",  --腾云
		BLOODTHIRSTY_PILL = "Đan này hại thân",  --嗜血
		CONDENSED_PILL = "Tập trung sức lực làm hẳn một hồi",  --凝神
		THORNS_PILL = "Mình mặc giáp gai", --荆棘
		ARMOR_PILL = "Ông già à, ta chỉ dùng Tráng Cốt Đan của Lão Quân", --壮骨
		DETOXIC_PILL = "Bách Độc Bất Xâm", --化毒

		PEACH = "Nhường ta một quả",  --桃子
		BIGPEACH =  "Đây chính là tiên phẩm trên tiệc Bàn Đào", --大桃子
		PEACH_COOKED = "Mất đi vài phần tiên khí",  --烤桃子
		PEACH_BANQUET = "Con khỉ kia cũng vì cái này mà đại náo thiên cung long trời lở đất" , --蟠桃大会
		PEACH_WINE = "Đây là rượu chay của thần tiên, ta cũng không có phá giới!",  --蟠桃素酒
		MK_JGB = "Lão Trư ta cũng không thích gặp cây bổng này đâu",  --金箍棒
		MK_JGB_PILLAR = 'Hảo ca ca! Định Hải Thần Châm này thật uy phong! Uy phong!', --定海神针
		HONEY_PIE = "Bánh ngon ngọt quá, thèm chết Lão Trư rồi!",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Nhiều một chút! Ít quá Lão Trư ăn không no!" , --素斋
		CASSOCK = "Cà sa thôi mà",  --袈裟
		KAM_LAN_CASSOCK = "Cái này không phải bảo bối cà sa của sư phụ đâu.",  --锦襕袈裟

		GOLDEN_HAT_MK = "Lông công phấp phới, thật là oai phong.",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Giáp này lại gợi lên một đoạn chuyện cũ năm xưa",  --大圣战甲

		MK_BATTLE_FLAG = "Uy phong lẫm liệt!",  --旗子
		MK_BATTLE_FLAG_ITEM = "Uy phong lẫm liệt!" , --旗子

		XZHAT_MK  = "Lão Trư ta đội cái mũ của con khỉ lên cũng đẹp trai chán" ,     --行者帽


		NZ_LANCE =  "Hay cho một cái trường thương uy phong lẫm liệt" ,     --火尖枪
		NZ_DAMASK =  "So với tơ lụa của phụ nữ còn muốn mềm hơn",     --混天绫
		NZ_RING =  "Ui da, đừng có ném Lão Trư ta nữa",     --乾坤圈

		PIGSY_RAKE = "Thứ này luyện bởi phép thần, công phu mài giũa sáng trưng một màu", --九齿钉耙
		PIGSY_HAT = "Lão Trư ta cũng không muốn bị cảm lạnh", -- 墨兰帽
		GOURD = "Cắt thành sợi, xào sơ một lần", --葫芦
		GOURD_COOKED = "Có mùi quyến rũ" , --烤葫芦
		GOURD_SOUP = "Ăn vào hồi sức" , --葫芦汤
		GOURD_OMELET = "Lão Trư ta một ngụm phải ăn mấy phần này" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Không thể phá giới! <Làm một ngụm>", --酒葫芦
		PILL_BOTTLE_GOURD = "Hồ lô của Lão Quân? Ta thay ông ấy lấy lại", --丹药葫芦
		BANANAFAN = "Mùa hè vừa ngủ vừa quạt, Ngọc Đế cũng không sướng bằng", --芭蕉扇
		LAOZI_SP = "Bùa này gọi được lão quan nhân?", --急急如律令
		LAOZI = "Lão quan nhân sao lại ở đây?", --老子
		WB_HEART =  "Tà ma ngoại đạo" , --阴森之心
		BONE_WAND =  "Tà ma ngoại đạo" , --骨杖
		BONE_BLADE =  "Tà ma ngoại đạo!" , --骨刃
		BONE_WHIP =  "Tà ma ngoại đạo!" , --骨鞭
		BONE_PET =  "Tà ma ngoại đạo!" , --小骷髅

		LOTUS_FLOWER = "Bên trong có đài sen không?" ,--莲花
		LOTUS_FLOWER_COOKED = "Lại có mùi  thơm.", -- 烤莲花

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Phương thốn càn khôn, nhật nguyệt đấu chuyển",
		},
		YJ_SPEAR = "Binh khí này của Nhị Lang, sao sánh được với Cửu Xỉ Đinh Ba của ta",--三尖两刃刀
		YANGJIAN_HAIR = "Cái mũ của chân quân thực sự sang trọng",--三山飞凤冠
		YANGJIAN_ARMOR = "Giáp trụ này quá bé, Lão Trư hình thể to lớn mặc không vừa",--锁子清源甲
		SKYHOUND = "Thịt chó ngon lắm... Quên! Người xuất gia không được nghĩ chuyện sát sinh!",--哮天犬
		SKYHOUND_WARG = "A! Con chó còn to hơn Lão Trư nữa! Thịt chắc cũng dai hơn", --月石哮天犬

		MONKEY_KING = "Đi đâu cũng gặp tên Bật Mã Ôn đáng ghét này!", --猴子
		MK_PHANTOM = "Hầu Ca sao lại đi bán muối thế? Haha.",
		PIGSY = "Ta đang soi gương sao?", -- 猪猪
		NEZA = "Tam Thái Tử theo ai đến đây?", --那咋
		WHITE_BONE = "Nữ bồ tát... Không đúng! Yêu quái!", --白骨
		YANGJIAN = "Chân Quân từ đâu đến vậy?", --杨戬
		MYTH_YUTU = "Tiên tử lần này hạ phàm có đồ ăn gì ngon không?",
		YAMA_COMMISSIONERS = "Hừ, con khỉ thối lại biến hình lừa gạt ta đây mà",

		MYTH_INTERIORS_LIGHT = "Ta cũng muốn ngửi mùi dầu thắp",
		MYTH_INTERIORS_BED = "Giường này cũng bé quá",
		MYTH_INTERIORS_GZ = "Tranh hoạ chẳng hay ho gì",
		MYTH_INTERIORS_PF = "Lão Trư ta thích nhất là ánh trăng xinh đẹp",
		MYTH_INTERIORS_XL = "Lão Trư ta bình thường cũng không thắp hương",
		MYTH_INTERIORS_ZZ = "Nguệch ngoạc cái gì, ta cũng không hiểu được",
		MYTH_FOOD_ZPD = "Cái này chế biến cũng tốn một hồi công phu",
		MYTH_FOOD_NX = "Một ngụm là hết.",
		MYTH_FOOD_LXQ = "Tôm viên này còn không đủ cho ta nhét kẽ răng",
		MYTH_FOOD_FHY = "Cuối cùng cũng có thể ăn no nê",
		MYTH_FOOD_HYMZ = "Khiến cho ta càng ăn càng ghiền",
		MYTH_BANANA_LEAF = "Nhìn qua không ngon lành gì, chẳng qua đem chưng cơm chắc sẽ thơm lắm",
		MYTH_BANANA_TREE = "Mau cho ta xem trái cây nằm đâu",
		MYTH_ZONGZI_ITEM1 = "Ta một lúc có thể ăn 30 cái",
		MYTH_ZONGZI_ITEM2 = "Lại có thịt ở trong. Thôi đừng mách sư phụ nhé!",
		BANANAFAN_BIG = "Gió thổi làm cho ta mùa hè ngủ càng thấy thích",
	},



	--娜扎

	neza= {
		HEAT_RESISTANT_PILL = "Đỡ mất công mình ra sông",  --避火丹
		COLD_RESISTANT_PILL = "Thật là bảo vật!" , --避寒丹
		DUST_RESISTANT_PILL = "Bụi đất trở về bụi đất",  --避尘丹
		FLY_PILL = "Hứ. Na Tra cũng có bảo vật đằng vân giá vũ" , --腾云
		BLOODTHIRSTY_PILL = "Một loại tà vật, là ai luyện?",  --嗜血
		CONDENSED_PILL = "Na Tra sinh ra mạnh sẵn rồi, không cần đan này." , --凝神
		THORNS_PILL = "Na Tra là hoa sen chứ đâu phải hoa hồng!", --荆棘
		ARMOR_PILL = "Hiện tại Na Tra cũng không giòn mỏng lắm đâu.", --壮骨
		DETOXIC_PILL = "Ngũ Độc Giáo, ta tới đây!", --化毒

		PEACH = "Con khỉ ngang ngược, hương vị quả này thế nào?",  --桃子
		BIGPEACH =  "Đào ngon! Phải đề phòng con khỉ ngang ngược háu ăn kia", --大桃子
		PEACH_COOKED = "Đào tiên đem nướng? Ta giận đỏ hết người rồi!",  --烤桃子
		PEACH_BANQUET = "Sư phụ mới có tư cách được mời" , --蟠桃大会
		PEACH_WINE = "Na Tra còn nhỏ, không được uống rượu",  --蟠桃素酒
		MK_JGB = "Đáng đời lão Long Vương",  --金箍棒 123
		MK_JGB_PILLAR = "Thiếu Định Hải Thần Châm này, lật tung đáy biển dễ dàng hơn nhiều", --定海神针
		HONEY_PIE = "Đồ ăn vặt ngon quá!",  --蜂蜜素饼 123
		VEGETARIAN_FOOD = "Không thích cũng không được bỏ đi" , --素斋
		CASSOCK = "Sư phụ ta là đạo sĩ",  --袈裟
		KAM_LAN_CASSOCK = "...Óng ánh ghê!",  --锦襕袈裟

		GOLDEN_HAT_MK = "Uầy! Ngầu!",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Na Tra cũng thích có cái giáp như này!",  --大圣战甲

		MK_BATTLE_FLAG = "Oai phong quá!",  --旗子
		MK_BATTLE_FLAG_ITEM = "Oai phong quá!" , --旗子

		XZHAT_MK  = "Mẹ cũng từng cho Na Tra đội mũ" ,     --行者帽


		NZ_LANCE =  "Bây giờ còn ai đỡ nổi một đòn của ta" ,     --火尖枪
		NZ_DAMASK =  "Thứ gì vướng quá? Chán ghê.",     --混天绫
		NZ_RING =  "May mà mày cũng còn biết đường về",     --乾坤圈

		PIGSY_RAKE = "Đây đâu phải bảo vật Ngọc Đế ban", --九齿钉耙
		PIGSY_HAT = "Mũ đẹp mà người đội xấu quá!", -- 墨兰帽
		GOURD = "Hồ lô nho nhỏ bụng to to", --葫芦
		GOURD_COOKED = "Dãi chảy đầy miệng rồi!" , --烤葫芦
		GOURD_SOUP = "Xin thêm chén nữa!" , --葫芦汤
		GOURD_OMELET = "Sao mà... giống trứng cuộn mẹ làm..." , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Một bầu rượu trong, một giấc mơ.", --酒葫芦
		PILL_BOTTLE_GOURD = "Dùng để đựng đan dược", --丹药葫芦
		BANANAFAN = "Quạt báu này chẳng tầm thường", --芭蕉扇
		LAOZI_SP = "Bùa cung phụng Lão Quân", --急急如律令
		LAOZI = "Bái kiến sư tổ", --老子
		WB_HEART =  "Trái tim yêu quái!!" , --阴森之心
		BONE_WAND =  "Người chết không yên" , --骨杖
		BONE_BLADE =  "Cây đao hèn hạ!", --骨刃
		BONE_WHIP =  "Yêu tinh đê tiện giật gân rút xương!" , --骨鞭
		BONE_PET =  "Ọe. Xác còn chưa lạnh." , --小骷髅

		LOTUS_FLOWER = "Liên hoa hiện hóa thân, Linh Châu xuất phàm trần" ,--莲花 123
		LOTUS_FLOWER_COOKED = "Na Tra cảm giác ngột ngạt nóng bức quá.", -- 烤莲花 那咋也爱吃烤莲花？

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Sư phụ thường dùng lò này",
			EMPTY = "Con khỉ trốn mất rồi rồi",
			DONE = "Con khỉ ngang ngược đã bị luyện hóa xong",
		},
		YJ_SPEAR = "Thần uy rợp trời, phong lôi sắp hiện",--三尖两刃刀
		YANGJIAN_HAIR = "Cái cột tóc đuôi ngựa của sư huynh",--三山飞凤冠
		YANGJIAN_ARMOR = "Dương đại ca có áo giáp oai quá. Na Tra thích!",--锁子清源甲
		SKYHOUND = "Hao Thiên Hao Thiên! Cho Na Tra ôm một cái!",--哮天犬
		SKYHOUND_WARG = "Hao Thiên lớn như này Na Tra ôm không xuể.", --月石哮天犬

		MONKEY_KING = "Đại Thánh đã lâu không gặp.", --猴子
		MK_PHANTOM = "Đại Thánh đã lâu không gặp, sao vừa gặp đã chết rồi?",
		PIGSY = "Thiên Bồng Nguyên Soái sao không bảo hộ sư phụ người đi Tây Thiên?", -- 猪猪
		NEZA = "Sao lại có 2 Na Tra?", --那咋
		WHITE_BONE = "Yêu nghiệt! Tiếp ta một thương!", --白骨
		YANGJIAN = "Sư huynh sao lại ở đây?", --杨戬 123
		MYTH_YUTU = "Ngọc Thố muội muội khỏe không?",
		YAMA_COMMISSIONERS = "Chà, Vô Thường hôm nay đi tuần à?",

		MYTH_INTERIORS_LIGHT = "Giống cái đèn ở đầu giường Na Tra quá!",
		MYTH_INTERIORS_BED = "Để Na Tra nằm nghỉ một lát.",
		MYTH_INTERIORS_GZ = "Cái bình có vẽ vời chút ít.",
		MYTH_INTERIORS_PF = "Mặt trên bức tranh cũng thật tinh xảo.",
		MYTH_INTERIORS_XL = "Có hương này Na Tra tọa thiền càng dễ nhập định.",
		MYTH_INTERIORS_ZZ = "Xem xem Na Tra còn học được gì nè!",
		MYTH_FOOD_ZPD = "Ọe... cứ nghĩ cái đống đó mà cho vào mồm thì...",
		MYTH_FOOD_NX = "Na Tra cũng muốn một ly!",
		MYTH_FOOD_LXQ = "Tôm Hùm Đông Hải cứ để Na Tra ăn hết",
		MYTH_FOOD_FHY = "Hay cho câu chuyện năm đó",
		MYTH_FOOD_HYMZ = "Con thỏ đáng yêu quá! Hệt như con thỏ Na Tra từng nuôi!",
		MYTH_BANANA_LEAF = "So với lá sen còn to hơi mấy phần",
		MYTH_BANANA_TREE = "Ta cũng thích ăn chuối!",
		MYTH_ZONGZI_ITEM1 = "Nhớ tay nghề của mẹ quá!",
		MYTH_ZONGZI_ITEM2 = "Bánh ú mà cũng đặc sắc ghê!",
		BANANAFAN_BIG = "Lục đinh thần hỏa luyện, giữa lò bát quái sinh",
	},





	--白骨的
	white_bone = {
		HEAT_RESISTANT_PILL = "Lạnh thấu xương, không sợ mùa hè",  --避火丹
		COLD_RESISTANT_PILL = "Nóng rừng rực, không sợ mùa đông",  --避寒丹
		DUST_RESISTANT_PILL = "Bụi về đường bụi, đất về đường đất, ai cho ta tái nhập luân hồi",  --避尘丹
		FLY_PILL = "Phép thuật bay bay, đường này nho nhỏ",  --腾云
		BLOODTHIRSTY_PILL = "Ta cực kỳ thích nếm máu của ngươi",  --嗜血
		CONDENSED_PILL = "Lại đi săn một con mồi mới",  --凝神
		THORNS_PILL = "Giáp gai vỡ", --荆棘
		ARMOR_PILL = "Thực sự là quá tốt cho xương ta", --壮骨
		DETOXIC_PILL = "Tống độc khỏi xương", --化毒

		PEACH = "Chỉ một chút linh khí, không đáng kể",  --桃子
		BIGPEACH =  "Dù tìm đỏ mắt không thấy, vẫn có thể khiến công lực ta đại tăng", --大桃子
		PEACH_COOKED = "Mất sạch linh khí",  --烤桃子
		PEACH_BANQUET = "Cái này không tới lượt yêu loại hèn kém như thiếp thân có thể hưởng dùng" , --蟠桃大会
		PEACH_WINE = "Ruộng trên đầy nước, ruộng dưới khô",  --蟠桃素酒
		MK_JGB = "Đừng đánh thiếp!",  --金箍棒
		MK_JGB_PILLAR = 'Hay cho một cây Như Ý Bổng uy phong lẫm liệt', --定海神针
		HONEY_PIE = "Mang đi mang đi, ta không có ăn cái thứ này",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Là phân hồi trước ta biến thành con cóc ị ra phải không?" , --素斋
		CASSOCK = "Cái áo rách của mấy thằng Hòa Thượng",  --袈裟
		KAM_LAN_CASSOCK = "Cà sa này cũng không tầm thường chút nào",  --锦襕袈裟

		GOLDEN_HAT_MK = "Uy phong lẫm liệt!",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Bảo khí tràn trề!",  --大圣战甲

		MK_BATTLE_FLAG = "Xé cái cờ này tiếng kêu thật rất hay",  --旗子
		MK_BATTLE_FLAG_ITEM = "Mang cái này ra khỏi động của ta đi" , --旗子

		XZHAT_MK  = "A! Đây là cái mũ có khẩn cô chú đây mà!" ,     --行者帽


		NZ_LANCE =  "Mũi thương thật sắc bén!" ,     --火尖枪
		NZ_DAMASK =  "Tam Công Tử thế nào lại đeo dây lụa con gái? Hahaha",     --混天绫
		NZ_RING =  "Càn khôn một cú chết ma rồng",     --乾坤圈

		PIGSY_RAKE = "Vốn từ lễ khí, lại thành sát khí", --九齿钉耙
		PIGSY_HAT = "Phàm vật thế này.", -- 墨兰帽
		GOURD = "Phàm vật", --葫芦
		GOURD_COOKED = "Phàm vật bị cháy" , --烤葫芦
		GOURD_SOUP = "Phàm vật nhúng nước" , --葫芦汤
		GOURD_OMELET = "To gan, dám dâng cái này cho ta cho có lệ à?" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Không sợ gió bão bất ngờ, chỉ sợ hồ lô hết rượu", --酒葫芦
		PILL_BOTTLE_GOURD = "Một viên tiên đan trăm năm nhanh chóng", --丹药葫芦
		BANANAFAN = "Hảo bảo bối", --芭蕉扇
		LAOZI_SP = "Hừ! Không lẽ ngươi định lấy cái này phong ấn ta?", --急急如律令
		LAOZI = "Không ổn rồi đại vương ơi, chạy thôi!", --老子
		WB_HEART =  "Màu thẫm tô đỏ môi họa bì" , --阴森之心
		BONE_WAND =  "Pháp trượng kỳ bí, khắc như sừng rồng, mùi như sừng tê, thêm viên ngọc quỷ" , --骨杖
		BONE_BLADE =  "Lấy mạng ngươi, nuôi mạng ta" , --骨刃
		BONE_WHIP =  "Roi vung bốn phía nổi phong vân, đầm đìa máu huyết của địch quân" , --骨鞭
		BONE_PET =  "Ngoan nghe lời ta!" , --小骷髅
		LOTUS_FLOWER = "Hoa như áo mỹ nhân, lá như khúc nghê thường" ,--莲花
		LOTUS_FLOWER_COOKED = "Chẳng lợi cho ta được một phần máu thịt", -- 烤莲花

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Chân hỏa mạnh thật, nhanh nào nhanh nào!",
		},
		YJ_SPEAR = "Trảm yêu trừ ma tam tiêm duệ, hàng long phục hổ lưỡng nhận đao",--三尖两刃刀
		YANGJIAN_HAIR = "Thần tướng đội mũ này vào trông cũng thật đẹp trai",--三山飞凤冠
		YANGJIAN_ARMOR = "Lạnh thấu xương, ta không dám lại gần",--锁子清源甲
		SKYHOUND = "Không phải Hao Thiên Khuyển của Nhị Lang Thần sao? Phải cẩn thận chút.",--哮天犬
		SKYHOUND_WARG = "Con thần khuyển này dĩ nhiên là mặt trăng cũng dám ăn.", --月石哮天犬
		MONKEY_KING = "Không biết cảm giác bị đuổi thế nào?", --猴子
		MK_PHANTOM = "Không biết bị niệm chú đến chết cảm giác ra sao?",
		PIGSY = "Trưởng lão à có ăn cơm chay không?", -- 猪猪
		NEZA = "Gặp được Tam Công Tử thật là may mắn", --那咋
		WHITE_BONE = "Lại thêm một khối ngàn năm xương trắng?", --白骨
		YANGJIAN = "Nhị Lang Thần! Đánh cũng không lại, chạy cũng khó xong.", --杨戬
		MYTH_YUTU = "Người thì không thấy ai đắc đạo, toàn gà chó được lên trời",
		YAMA_COMMISSIONERS = "Ta là âm hồn, không dám chống bọn chúng",


		MYTH_INTERIORS_LIGHT = "A.. ngọn đèn này cũng có pháp lực.",
		MYTH_INTERIORS_BED = "Thôi để bộ xương già này nằm nghỉ một chút",
		MYTH_INTERIORS_GZ = "Chẳng qua vẽ lại ít mây ít núi",
		MYTH_INTERIORS_PF = "Mấy tấm bình phong, mơ hồ mông lung",
		MYTH_INTERIORS_XL = "Hương liệu trước giờ ta dùng toàn từ tinh huyết con người",
		MYTH_INTERIORS_ZZ = "Cái này rõ ràng là tiên nhân lưu lại",
		MYTH_FOOD_ZPD = "Trân phẩm nhân gian, vị bùi và béo",
		MYTH_FOOD_NX = "Đồ ăn dụ con nít à",
		MYTH_FOOD_LXQ = "So với ăn sống thì thú vị hơn chút ít",
		MYTH_FOOD_FHY = "Ẩm thực phong phú, là để bày tiệc",
		MYTH_FOOD_HYMZ = "Nguyệt sắc hoa hương, nghiêng thành một chén",
		MYTH_BANANA_LEAF = "Lá xanh như ngọc",
		MYTH_BANANA_TREE = "Gió thu thổi qua cũng bị nhuốm xanh",
		MYTH_ZONGZI_ITEM1 = "Cái gì ngũ huân tam yếm, ngon lành gì chứ",
		MYTH_ZONGZI_ITEM2 = "Ít nhất cũng thêm chút thịt vào thế này",
		BANANAFAN_BIG = "Hay cho một cây bảo phiến pháp lực cao cường",
	},


	--杨戬
	yangjian = {
		HEAT_RESISTANT_PILL = "Cơ thể ta nào sợ nóng lạnh bụi?",  --避火丹
		COLD_RESISTANT_PILL = "Cơ thể ta nào sợ nóng lạnh bụi",  --避寒丹
		DUST_RESISTANT_PILL = "Cơ thể ta chưa từng sợ nóng lạnh bụi",  --避尘丹
		FLY_PILL = "Mây ở quanh đây, cùng ta bay lên!",  --腾云
		BLOODTHIRSTY_PILL = "Đã lâu chưa được thưởng thức mùi vị máu.",  --嗜血
		CONDENSED_PILL = "Ý chí quyết liệt, thần thông quảng đại!",  --凝神
		THORNS_PILL = "Bụi gai quanh thân như có thêm đao", --荆棘
		ARMOR_PILL = "Sức khỏe giảm sút cũng không chịu thua.", --壮骨
		DETOXIC_PILL = "Cơ thể ta nào sợ độc", --化毒

		PEACH = "Không có gì lạ",  --桃子
		BIGPEACH =  "Thật là hiếm thấy", --大桃子
		PEACH_COOKED = "Nát bét rồi",  --烤桃子
		PEACH_BANQUET = "Vương Mẫu Nương Nương lại không mời ta." , --蟠桃大会
		PEACH_WINE = "Tỉnh táo thoải mái",  --蟠桃素酒
		MK_JGB = "Cái con khỉ ngang ngược đó chắc chắn ở đây.",  --金箍棒
		MK_JGB_PILLAR = 'Bảo vật ở đây, cái con khỉ ngang ngược đó chắc chắn ở gần đây.', --定海神针
		HONEY_PIE = "Lương khô mà thôi",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Có một phong vị khác" , --素斋
		CASSOCK = "Đồ của Phật Môn",  --袈裟
		KAM_LAN_CASSOCK = "Đây không phải là pháp khí của Kim Thiền Tử sao?",  --锦襕袈裟

		GOLDEN_HAT_MK = "Mũ búi tóc của con khỉ ngang ngược đó",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Dám xưng Tề Thiên? Thật nực cười.",  --大圣战甲

		MK_BATTLE_FLAG = "Tăng nhiều sĩ khí quân ta.",  --旗子
		MK_BATTLE_FLAG_ITEM = "Tay cầm giáo mác mình mặc giáp, chiến xa giao tranh xáp lá cà." , --旗子

		XZHAT_MK  = "Thô thiển! Vất đi!" ,     --行者帽


		NZ_LANCE =  "Không bằng Tam Tiêm Đao của ta." ,     --火尖枪
		NZ_DAMASK =  "Bảo vật hộ thân",     --混天绫
		NZ_RING =  "Không biết so với Kim Cương Trạc của Lão Quân thế nào.",     --乾坤圈

		PIGSY_RAKE = "Xin chào quân tử, vị trí nào cũng xứng", --九齿钉耙
		PIGSY_HAT = "Vật phẩm nhân giới", -- 墨兰帽
		GOURD = "Dây leo đung đưa tô điểm sắc thu, hòa vào không khí trong lành ở vườn.", --葫芦
		GOURD_COOKED = "Ăn cho có no thôi" , --烤葫芦
		GOURD_SOUP = "Tuy không đẹp lắm, nhưng có thể giải khát." , --葫芦汤
		GOURD_OMELET = "Tay nghề rất khá" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Rượu dâng lên, không ngừng rót!", --酒葫芦
		PILL_BOTTLE_GOURD = "Lát nữa đưa cho Lão Quân.", --丹药葫芦
		BANANAFAN = "Sao lại làm mất món đồ này?", --芭蕉扇
		LAOZI_SP = "Lão Quân xá lệnh", --急急如律令
		LAOZI = "Bái kiến sư tổ.", --老子
		WB_HEART =  "Tuyệt đối không được lầm đường lạc lối." , --阴森之心
		BONE_WAND =  "Món đồ của yêu quái." , --骨杖
		BONE_BLADE =  "Món đồ của yêu quái." , --骨刃
		BONE_WHIP =  "Món đồ của yêu quái." , --骨鞭
		BONE_PET =  "Món đồ của yêu quái." , --小骷髅

		LOTUS_FLOWER = "Không như hoa lá thế gian, hoa rơi chậu vàng lá chạm sân." ,--莲花
		LOTUS_FLOWER_COOKED = "Món ăn tinh xảo.", -- 烤莲花

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Châm lại lò đan giữ khói chiều",
		},
		YJ_SPEAR = "Tiến thẳng một mạch, trảm yêu trừ ma, sét gió khuấy động, ai dám kháng uy",--三尖两刃刀
		YANGJIAN_HAIR = "Búi tóc đội mũ bạc, trông thật khí phách",--三山飞凤冠
		YANGJIAN_ARMOR = "Ánh trăng bàng bạc, vừa chiến vừa cuồng",--锁子清源甲
		SKYHOUND = "Ra ngoài chơi nhớ cẩn thẩn chút.",--哮天犬
		SKYHOUND_WARG = "Hồ đồ! Thừa lúc ta vắng mặt lại ăn lung tung.", --月石哮天犬
		SKYHOUND_TOOTH = "Hao Thiên ta nhất định sẽ không để ngươi ngủ quá lâu!" ,--天犬幼齿
		MONKEY_KING = "Lại đánh thêm 300 hiệp!", --猴子
		MK_PHANTOM = "Đồ bại tướng! Ngươi đi chết đi!",
		PIGSY = "Sao Thiên Bồng Nguyên Soái không bảo vệ sư phụ đi tây thiên lấy kinh.", -- 猪猪
		NEZA = "Na Tra huynh đệ đang làm gì vậy?", --那咋
		WHITE_BONE = "Yêu nghiệt chạy đi đâu!", --白骨
		YANGJIAN = "Cái con khỉ ngang ngược ngươi, lại biến thành ta đi lừa lọc!", --杨戬
		MYTH_YUTU = "Ngươi lại đi gây chuyện cho Tinh Quân gánh đấy à?",
		YAMA_COMMISSIONERS = "Dương gian địa phủ cũng như nhau",

		MYTH_INTERIORS_LIGHT = "Chắc không phải là con chuột trộm dầu đèn chứ",
		MYTH_INTERIORS_BED = "Nơi này uống trà đàm đạo thật tốt",
		MYTH_INTERIORS_GZ = "Cái vại tuy tầm thường, nhưng hoa văn lại phi thường",
		MYTH_INTERIORS_PF = "Bình phong dừng lại, chỉ vì người",
		MYTH_INTERIORS_XL = "Bảo vật xuất hiện từ tro bụi",
		MYTH_INTERIORS_ZZ = "Nghiêng mình mài mực đi từng nét, tự cổ chí kim được mấy người",
		MYTH_FOOD_ZPD = "Thứ cho Dương Tiễn khó tuân mệnh, mau đem đi đi",
		MYTH_FOOD_NX = "Không biết ai nghĩ ra cách chế biến này",
		MYTH_FOOD_LXQ = "Chích Khẩu Hà Hoàn vào chậu ngọc.",
		MYTH_FOOD_FHY = "Lấp biển dời núi mời khách khứa",
		MYTH_FOOD_HYMZ = "Hoa xuân làm trang sức, trăng thu đầy chén",
		MYTH_BANANA_LEAF = "Trông thật là xanh tươi",
		MYTH_BANANA_TREE = "Ánh trăng in trên lá, niềm vui lan khắp nhà",
		MYTH_ZONGZI_ITEM1 = "Chấm chút mật ong, đó là mĩ vị nhân gian",
		MYTH_ZONGZI_ITEM2 = "Mùi lá bánh bay quanh, có chút ngấy, lại rất nhẹ nhàng",
		BANANAFAN_BIG = "Nghịch cải thiên mệnh, không biết món đồ này là phúc hay họa",
	},

	--玉兔
	myth_yutu = {
		HEAT_RESISTANT_PILL = "Diệt lửa dễ dàng",  --避火丹
		COLD_RESISTANT_PILL = "Không sợ lạnh",  --避寒丹
		DUST_RESISTANT_PILL = "Tẩy mọi bụi trần",  --避尘丹
		FLY_PILL = "Đạp sương cưỡi mây",  --腾云
		BLOODTHIRSTY_PILL = "Hao tâm tổn sức, không sử dụng nhiều",  --嗜血
		CONDENSED_PILL = "Sức tập trung của ta lại càng cao rồi",  --凝神
		THORNS_PILL = "Vòng gai quấn lấy ta", --荆棘
		ARMOR_PILL = "Hiện giờ ta là con thỏ cường tráng nhất!", --壮骨
		DETOXIC_PILL = "Ta không sợ ăn phải nâm độc nữa đâu", --化毒

		PEACH = "Bàn Đào nhỏ à! Phải dâng nhiều hơn nữa",  --桃子
		BIGPEACH =  "Loại Bàn Đào lớn thật hiếm thấy.", --大桃子
		PEACH_COOKED = "Linh khí sao lại tan biến như vậy",  --烤桃子
		PEACH_BANQUET = "Mỗi lần đến hội Bàn Đào thì Hằng Nga tỷ tỷ đều dẫn ta đi." , --蟠桃大会
		PEACH_WINE = "Một bình rượu chay một lần say",  --蟠桃素酒
		MK_JGB = "Con khỉ ngang ngược đó chắc chắn ở gần đây",  --金箍棒
		MK_JGB_PILLAR = 'Tề Thiên Đại Thánh! Uy phong lẫm liệt', --定海神珍
		HONEY_PIE = "Bỏ vào túi, đem về.",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Tuy ta không phải là hòa thượng, nhưng ta rất thích ăn chay" , --素斋
		CASSOCK = "Oh~ Đội lên đầu sao?",  --袈裟
		KAM_LAN_CASSOCK = "Ôi chao, nhiều báu vật như vậy à!",  --锦襕袈裟

		GOLDEN_HAT_MK = "Lại là con khỉ ngang ngược đó",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Cái tên phá hoại chuyện tốt của người khác",  --大圣战甲

		MK_BATTLE_FLAG = "Thỏ Ngọc đến đây chơi",  --旗子
		MK_BATTLE_FLAG_ITEM = "Rất uy phong" , --旗子

		XZHAT_MK  = "Cái con khỉ đó thật quá đáng! Lại lấy lông thỏ làm mũ." ,     --行者帽


		NZ_LANCE =  "Rờ vào nó cảm thấy nóng" ,     --火尖枪
		NZ_DAMASK =  "Một khúc lụa đầy phép thuật",     --混天绫
		NZ_RING =  "Đúng lúc ta thiếu một cái vòng vàng.",     --乾坤圈

		PIGSY_RAKE = "Bồ cào là đồ tốt, người thì...", --九齿钉耙
		PIGSY_HAT = "Rất là bình thường", -- 墨兰帽
		GOURD = "Để ta xem bên trong là gì", --葫芦
		GOURD_COOKED = "Thêm chút thì là, cảm ơn" , --烤葫芦
		GOURD_SOUP = "Thanh lọc giải nhiệt" , --葫芦汤
		GOURD_OMELET = "Tại sao phải thêm trứng gà vậy" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Hi hi, mau đổ đầy cho ta.", --酒葫芦
		PILL_BOTTLE_GOURD = "Bên trong có thuốc uống vào thì có thể bay lên không?", --丹药葫芦
		BANANAFAN = "Quạt một cái gió nổi lên", --芭蕉扇
		LAOZI_SP = "Đây không phải là sắc lệnh của Lão Quân sao?", --急急如律令
		LAOZI = "Bái kiến Lão Quân", --老子
		WB_HEART =  "Hội tụ của tà ác, xấu xa, u tối" , --阴森之心
		BONE_WAND =  "Tà ma ngoại đạo mà thôi" , --骨杖
		BONE_BLADE =  "Tà ma ngoại đạo mà thôi" , --骨刃
		BONE_WHIP =  "Tà ma ngoại đạo mà thôi" , --骨鞭
		BONE_PET =  "Hồn đã đi, sao xương vẫn còn?" , --小骷髅

		LOTUS_FLOWER = "Trời chiều đỏ rực, hương hoa sen thơm ngát" ,--莲花
		LOTUS_FLOWER_COOKED = "Thơm nhẹ mềm ấm", -- 烤莲花

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Ta không biết dùng cái này, ta chỉ biết dùng cối thuốc để giã thuốc",
		},
		YJ_SPEAR = "Vũ khí của Chân Quân sao lại ở đây",--三尖两刃刀
		YANGJIAN_HAIR = "Phượng kêu ríu rít",--三山飞凤冠
		YANGJIAN_ARMOR = "Thanh tĩnh đến lạnh người",--锁子清源甲
		SKYHOUND = "Hao Thiên, muốn ăn cà rốt không",--哮天犬
		SKYHOUND_WARG = "Ngươi thật là to lớn", --月石哮天犬
		SKYHOUND_TOOTH = "Trái tim bất diệt" ,--天犬幼齿
		MONKEY_KING = "Cái tên Bật Mã Ôn xấu xa nhà ngươi", --猴子
		MK_PHANTOM = "Ngươi là thật hay giả vậy?", --猴子分身
		PIGSY = "Mau tránh xa Quảng Hàn Cung ra", -- 猪猪
		NEZA = "Tam Thái Tử muốn đi đâu vậy!", --那咋
		WHITE_BONE = "Hứ, có ta ở đây ngươi đừng hòng lại gần Quảng Hàn Cung", --白骨
		YANGJIAN = "Hằng Nga tỷ tỷ mời ngươi đi uống trà đó.", --杨戬
		MYTH_YUTU = "Ta mới là bảo bối của Hằng Nga tỷ tỷ!",
		YAMA_COMMISSIONERS = "Đừng bắt ta, ta chỉ là du khách thôi!",

		MYTH_INTERIORS_LIGHT = "Sáng hoài không tắt", --房子里面的灯
		MYTH_INTERIORS_BED = "Nơi nghỉ ngơi",--房子里面的床
		MYTH_INTERIORS_GZ = "Một bức họa mà thôi",--房子里面的挂画
		MYTH_INTERIORS_PF = "Mờ mờ, thấp thoáng",--房子里面的屏风
		MYTH_INTERIORS_XL = "Đàn Hương định tâm",--房子里面的香炉
		MYTH_INTERIORS_ZZ = "Để ta thu dọn cho",--房子里面的桌子
		MYTH_FOOD_ZPD = "Cái thứ này là thuốc độc sao?",--猪皮冻
		MYTH_FOOD_NX = "Mùi sữa nồng nặc",--奶昔
		MYTH_FOOD_LXQ = "Vừa lớn vừa no",--龙虾球
		MYTH_FOOD_FHY = "Có thể dùng để mời khách",--覆海宴
		MYTH_FOOD_HYMZ = "Nguyệt Thần rủ rỉ bên tai ngươi",--花语满载
		MYTH_BANANA_LEAF = "Như ngọc như phỉ thúy",--香蕉叶子
		MYTH_BANANA_TREE = "Ta đợi không nổi rồi",--香蕉树
		MYTH_ZONGZI_ITEM1 = "Một cái là bao no",--粽子
		MYTH_ZONGZI_ITEM2 = "Ta không thể ăn bánh ú nhân thịt",--肉粽
		BANANAFAN_BIG = "Muôn vàn mưa gió đến từ đây",--大扇子
	},

	YAMA_COMMISSIONERS = {
		HEAT_RESISTANT_PILL = "Cầm vật này, nóng nực không ngại",  --避火丹
		COLD_RESISTANT_PILL = "Cầm vật này, gió lạnh không lo",  --避寒丹
		DUST_RESISTANT_PILL = "Ngàn dặm bụi trần ta phủi sạch",  --避尘丹
		FLY_PILL = "Nổi gió bắt hồn",  --腾云
		BLOODTHIRSTY_PILL = "Hồn phách mà cũng hút máu được sao?",  --嗜血
		CONDENSED_PILL = "Nín thử ngưng thần, một đòn trí mạng",  --凝神
		THORNS_PILL = "Bụi gai quanh mình, chịu khổ cùng ta", --荆棘
		ARMOR_PILL = "Xương người khác, chắn thân ta", --壮骨
		DETOXIC_PILL = "Có độc có giải", --化毒

		PEACH = "Tiên chủng lại rơi xuống phàm trần",  --桃子
		BIGPEACH =  "Vậy mà vẫn giữ lại tiên khí thuần khiết", --大桃子
		PEACH_COOKED = "Tục khí, tục khí chịu không nổi",  --烤桃子
		PEACH_BANQUET = "Ta cũng từng dự tiệc với Diêm Quân" , --蟠桃大会
		PEACH_WINE = "Trong chén toàn rượu ngon, phải say sưa một bữa",  --蟠桃素酒
		MK_JGB = "Như Ý Kim Cô Bổng, nặng một vạn ba ngàn năm trăm cân",  --金箍棒
		MK_JGB_PILLAR = 'Hắn tốt nhất đừng đến địa phủ thì hơn', --定海神针
		HONEY_PIE = "Khi khác cúng nhiều một chút",  --蜂蜜素饼
		VEGETARIAN_FOOD = "Cái này đưa cho người đi lấy kinh đi" , --素斋
		CASSOCK = "Không biết vị hòa thượng nào bỏ áo lại đây",  --袈裟
		KAM_LAN_CASSOCK = "Trên khảm thất bảo, nước lửa chẳng vào",  --锦襕袈裟

		GOLDEN_HAT_MK = "Đại Thánh đừng ghé thăm âm phủ nữa",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "Đại Thánh đừng ghé thăm âm phủ nữa",  --大圣战甲

		MK_BATTLE_FLAG = "Tinh kỳ phấp phới, thực sự oai phong",  --旗子
		MK_BATTLE_FLAG_ITEM = "Có lẽ cần phải viết gì đó lên" , --旗子

		XZHAT_MK  = "Lão Phạm, ngươi đội thử không?",      --行者帽


		NZ_LANCE =  "Thương nhỏ phóng lửa to, lập tức rõ anh hào" ,     --火尖枪
		NZ_DAMASK =  "Hỗn loạn thiên địa, nhật nguyệt tinh vân",     --混天绫
		NZ_RING =  "Siêu sáng siêu cứng, đánh vật đánh người",     --乾坤圈

		PIGSY_RAKE = "Đây là binh khí của Thiên Bồng Nguyên Soái phải không", --九齿钉耙
		PIGSY_HAT = "Lão Phạm, ngươi đội chắc hợp lắm, ta không hứng thú", -- 墨兰帽
		GOURD = "Bên trong có đứa nhỏ không?", --葫芦
		GOURD_COOKED = "Đúng là không có đứa nhỏ nào trong này" , --烤葫芦
		GOURD_SOUP = "Ngọt! Ngon!" , --葫芦汤
		GOURD_OMELET = "Thêm hai cái trứng đừng bỏ hành" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "Rượu ngon tràn bình", --酒葫芦
		PILL_BOTTLE_GOURD = "Cái này không phải hồ lô của Lão Quân", --丹药葫芦
		BANANAFAN = "Cái này không phải quạt của Lão Quân", --芭蕉扇
		LAOZI_SP = "Bùa này gọi được Lão Quân", --急急如律令
		LAOZI = "Bái kiến Lão Quân", --老子
		WB_HEART =  "Thật là âm trầm, thật là đáng sợ" , --阴森之心
		BONE_WAND =  "Tanh máu, tàn bạo, đáng phạt" , --骨杖
		BONE_BLADE =  "Tanh máu, tàn bạo, đáng phạt" , --骨刃
		BONE_WHIP =  "Tanh máu, tàn bạo, đáng phạt" , --骨鞭
		BONE_PET =  "Chỉ là một chút gia cố lên bộ xương khô" , --小骷髅

		LOTUS_FLOWER = "Hoa sen uyển chuyển ở giữa hồ" ,--莲花
		LOTUS_FLOWER_COOKED = "Ăn nhẹ buổi xế", -- 烤莲子

		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "Lửa nóng bừng bừng, giữa lò tam muội",
			EMPTY = "Chạm trổ tinh xảo, lò thật đẹp!",
			DONE = "Không dày vò sao thành bảo vật!",
		},

		YJ_SPEAR = "Lạnh lẽo lăng lệ, chớp giật sáng ngời",--三尖两刃刀
		YANGJIAN_HAIR = "Phượng gáy!",--三山飞凤冠
		YANGJIAN_ARMOR = "Mùa thu lạnh, chém giặc man",--锁子清源甲
		SKYHOUND = "Hao Thiên mau đuổi theo Chân Quân",--哮天犬
		SKYHOUND_WARG = "Thật là oai phong", --月石哮天犬

		MONKEY_KING = "Đại Thánh khỏe chứ? Sao không thấy Đường Trưởng Lão?", --猴子
		MK_PHANTOM = "Đại Thánh khỏe chứ? Sao không thấy Đường Trưởng Lão?",--假猴子（黑白无常分辨不出）
		PIGSY = "Thiên Bồng Nguyên Soái, vốn riêng của ngài phải giữ thật kỹ đấy", -- 猪猪
		NEZA = "Tam Thái Tử mạnh giỏi", --那咋
		WHITE_BONE = "To gan! Mày lại dám trốn lại nhân gian?", --白骨
		YANGJIAN = "Chân Quân mạnh giỏi. Ở đây được gặp thật là vinh hạnh", --杨戬
		MYTH_YUTU = "Tiên nhân bảo hộ, kết tóc trường sinh",
		YAMA_COMMISSIONERS = "Không biết huynh đài theo phương nào luân hồi đến đây?",

		MYTH_INTERIORS_LIGHT = "Đã bao nhiêu năm, mà dầu thắp vẫn chưa dùng hết",--油灯
		MYTH_INTERIORS_BED = "Chiếc gối này cũng đã trải ít nhiều sinh tử",--床
		MYTH_INTERIORS_GZ = "Giang Nam không chỗ nào có",--花瓶
		MYTH_INTERIORS_PF = "Nối nhau chắn gió, chẳng chắn phong lưu",--屏风
		MYTH_INTERIORS_XL = "Huệ lan đều tàn lụi",--香炉
		MYTH_INTERIORS_ZZ = "Nói bao nhiêu lần, ngoài mặt phải gọi Thủ Quân",--书桌
		MYTH_FOOD_ZPD = "Lão Phạm nếm thử cái này đi",--猪皮冻
		MYTH_FOOD_NX = "Mỹ vị tuôn trào như tơ như suối",--香蕉奶昔
		MYTH_FOOD_LXQ = "Từng viên ngon miệng",--蕉叶龙虾球
		MYTH_FOOD_FHY = "Hải sản quý báu một phần ăn",--覆海宴
		MYTH_FOOD_HYMZ = "Đồ cúng mà cũng trang trí đẹp thế sao",--花月满盏（废稿）
		MYTH_BANANA_LEAF = "Mưa dầm chẳng ướt lá chuối xanh",--芭蕉叶
		MYTH_BANANA_TREE = "Dong dỏng như cái dù",--芭蕉树
		MYTH_ZONGZI_ITEM1 = "Ngọt ngon. Đưa Khuất Linh Quân một phần đội mang về",--甜粽子
		MYTH_ZONGZI_ITEM2 = "Nhất định là nhân lòng đỏ trứng!",--咸粽子
		BANANAFAN_BIG = "Một quạt bão lên",--芭蕉扇
	},
	--盘丝娘娘
	madameweb = {	
		HEAT_RESISTANT_PILL = "诸火辟易",  --避火丹
		COLD_RESISTANT_PILL = "诸寒不侵",  --避寒丹
		DUST_RESISTANT_PILL = "涤垢不染",  --避尘丹
		FLY_PILL = "揽雾登青云",  --腾云
		BLOODTHIRSTY_PILL = "我需要血！更多的血！",  --嗜血
		CONDENSED_PILL = "我的注意力更集中了",  --凝神
		THORNS_PILL = "荆棘庇佑", --荆棘
		ARMOR_PILL = "妾身更强大了！", --壮骨

		PEACH = "这倒是不曾多见",  --桃子
		BIGPEACH =  "这般大的仙桃！。", --大桃子
		PEACH_COOKED = "如此这般便失了灵气",  --烤桃子
		PEACH_BANQUET = "妾身倒是三生有幸了。" , --蟠桃大会
		PEACH_WINE = "灵界甘露，仙宫典藏",  --蟠桃素酒
		MK_JGB = "那贼猴定在附近",  --金箍棒
		MK_JGB_PILLAR = '齐天大圣！好不威风。', --定海神珍
		HONEY_PIE = "妾身可不爱这过家家。",  --蜂蜜素饼
		VEGETARIAN_FOOD = "来点荤的斋饭" , --素斋
		CASSOCK = "哦~披在头上吗？",  --袈裟
		KAM_LAN_CASSOCK = "此物并非凡间所有！",  --锦襕袈裟

		GOLDEN_HAT_MK = "又是那泼猴",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "妾身恨不得把他撕碎",  --大圣战甲

		MK_BATTLE_FLAG = "不若下次妾身来织",  --旗子
		MK_BATTLE_FLAG_ITEM = "倒是蛮威风的" , --旗子

		XZHAT_MK  = "哼，且报窃衣之仇。" ,     --行者帽


		NZ_LANCE =  "三太子，来斗一场" ,     --火尖枪
		NZ_DAMASK =  "让妾身试试你这混天绫",     --混天绫
		NZ_RING =  "妾身还缺了一只金镯子。",     --乾坤圈 

		PIGSY_RAKE = "耙子倒是好东西，人嘛...", --九齿钉耙
		PIGSY_HAT = "脏死了，快拿走", -- 墨兰帽
		GOURD = "让我看看里面是什么", --葫芦
		GOURD_COOKED = "来点虫子夹心，谢谢" , --烤葫芦
		GOURD_SOUP = "清爽解暑" , --葫芦汤
		GOURD_OMELET = "为什么要加鸡蛋呀" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "嗝，妾身没有喝醉。", --酒葫芦
		PILL_BOTTLE_GOURD = "里面可有什么灵丹妙药？", --丹药葫芦
		BANANAFAN = "一扇风起", --芭蕉扇
		LAOZI_SP = "这不是老君的敕令吗？", --急急如律令
		LAOZI = "妾身还是早早遁去吧", --老子
		WB_HEART =  "邪恶，丑陋，阴暗的聚合体" , --阴森之心
		BONE_WAND =  "多么美妙的武器" , --骨杖
		BONE_BLADE =  "多么美妙的武器" , --骨刃
		BONE_WHIP =  "多么美妙的武器" , --骨鞭
		BONE_PET =  "啧啧，是什么支撑着你的躯体？" , --小骷髅

		LOTUS_FLOWER = "落日无边红，一截藕花香" ,--莲花 
		LOTUS_FLOWER_COOKED = "绵香暖糯", -- 烤莲花
		
		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "三昧翻滚，阴阳颠转",
		},	
		YJ_SPEAR = "如此宝器怎么遗失在这",--三尖两刃刀
		YANGJIAN_HAIR = "凤鸣潇潇",--三山飞凤冠
		YANGJIAN_ARMOR = "清寒逼人",--锁子清源甲
		SKYHOUND = "莫要来沾惹本尊",--哮天犬	
		SKYHOUND_WARG = "多了几分本事？", --月石哮天犬
		MONKEY_KING = "你这好色的弼马温，休要狡辩", --猴子
		MK_PHANTOM = "你是真的还是假的呢？", --猴子分身
		PIGSY = "莫要来玷污濯垢泉", -- 猪猪
		NEZA = "三太子要去哪呀！", --那咋
		WHITE_BONE = "你我本同道，何必相决绝", --白骨
		YANGJIAN = "参见真君，妾身有礼了。", --杨戬
		YAMA_COMMISSIONERS = "妾身还没到油尽灯枯的时候",

		MYTH_INTERIORS_LIGHT = "长明不灭", --房子里面的灯
		MYTH_INTERIORS_BED = "休憩的场所",--房子里面的床
		MYTH_INTERIORS_GZ = "一副挂画罢了",--房子里面的挂画
		MYTH_INTERIORS_PF = "朦朦胧胧，隐隐约约",--房子里面的屏风
		MYTH_INTERIORS_XL = "檀香定心",--房子里面的香炉
		MYTH_INTERIORS_ZZ = "让我来整理一下吧",--房子里面的桌子
		MYTH_FOOD_ZPD = "这东西是毒药吗？",--猪皮冻
		MYTH_FOOD_NX = "浓浓的奶香",--奶昔
		MYTH_FOOD_LXQ = "又大又饱满",--龙虾球
		MYTH_FOOD_FHY = "可以用来宴请客人",--覆海宴
		MYTH_BANANA_LEAF = "如玉似翡",--香蕉叶子
		MYTH_BANANA_TREE = "我已经等不及了",--香蕉树
		MYTH_ZONGZI_ITEM1 = "一个就管饱",--粽子
		MYTH_ZONGZI_ITEM2 = "咸鲜合宜",--肉粽
		BANANAFAN_BIG = "万丈风雨由此起",--大扇子
	}
}


local newitems = {
	MEDICINE_PESTLE_MYTH ={ --捣药杵
		monkey_king = "Vũ khí thế này cũng dám ngạnh đấu cùng ta?",
		pigsy = "Cho lão Trư mượn giã cái bánh dẻo đi",
		neza = "Không biết so với Hoả Tiêm Thương của ta thì cái nào lợi hại hơn",
		white_bone = "Hay cho một cái thuý ngọc bảo khí",
		yangjian = "Cái này không phải chày giã thuốc ở Cung Quảng sao?",
		myth_yutu = "Một đoạn dương chi ngọc, mùi thuốc quyện ngàn năm",
		yama_commissioners = "Mùi thuốc dai dẳng không tan",
		madameweb="仙宫造物，灵气不绝",
	},
	MYTH_GHG ={
		monkey_king = "Hằng Nga lại chuyển hộ khẩu đến đây à?",
		pigsy = "Ta một mình đi vào liệu có vô lễ không?",
		neza = "Ngói ngọc lưu ly, tường băng thuý ngọc",
		white_bone = "Chúng sinh lạy trăng, mặt trăng ở đây",
		yangjian = "Quảng Hàn Cung cũng rơi xuống đây à!",
		myth_yutu = "Về đến nhà rồi!",
		yama_commissioners = "Quảng Hàn Cung sao lại nằm đây?",
		madameweb="钟灵秀敏，人间仰望",
	},
	MYTH_CHANG_E ={
		monkey_king = "Tiên tử ở đây làm gì?",
		pigsy = "Hằng... Hằng Nga tiên tử! Tiên tử ở đây thật tốt!",
		neza = "Hằng Nga tỷ tỷ, ta tìm người kiếm bánh trung thu ăn",
		white_bone = "Có nên lãnh giáo tiên tử một chút không?",
		yangjian = "Mong tiên tử mời ta một ly trà!",
		myth_yutu = "Hằng Nga tỷ tỷ, ta về rồi!",
		yama_commissioners = "Tham kiến tiên tử, thân ta dơ bẩn lẽ ra không nên làm ô uế bảo địa",
		madameweb="卑鄙之身，还望海涵",
	},
	MYTH_INTERIORS_GHG_LU ={
		monkey_king = "Xem ra còn tinh thuần hơn cái trong phòng sư phụ",
		pigsy = "Cuộc sống của Hằng Nga thật tinh tế",
		neza = "Mẫu thân cùng từng dùng loại lư hương này",
		white_bone = "Không gió cũng lên, quả thật thơm quá",
		yangjian = "Ngồi thiền đốt trầm khói nghi ngút",
		myth_yutu = "Ta phải đổi trầm mới rồi",
		yama_commissioners = "Hương trong miếu so với nơi này thật thua xa",
		madameweb="清烟如丝起",
	},
	MYTH_INTERIORS_GHG_LIGHT ={
		monkey_king = "Đây là thứ bảo bối gì?",
		pigsy = "Sáng ghê",
		neza = "Ban ngày cũng đốt đèn à",
		white_bone = "Giá đèn thật không tầm thường",
		yangjian = "Trông như nhan sắc của tiên tử",
		myth_yutu = "Ánh sáng hôm nay ôn nhu ghê",
		yama_commissioners = "Đẹp như ánh trăng rũ xuống",
		madameweb="幽幽月明",
	},
	MYTH_INTERIORS_GHG_FLOWER ={
		monkey_king = "<Hít hít>....Hắt xì!!!",
		pigsy = "Mùi thật là thơm!",
		neza = "Na Tra bẻ một cành được không?",
		white_bone = "Đào lý tầm thường khó sánh nổi",
		yangjian = "Nột thất của tiên tử thật sự rất tao nhã",
		myth_yutu = "Nguyệt hoa nở mãi, lâu bền không tàn",
		yama_commissioners = "Lão Phạm ngươi nhỏ giọng lại một chút",
		madameweb="我想让它凋谢....",
	},
	MYTH_INTERIORS_GHG_HE_RIGHT ={
		monkey_king = "Trên Hoa Quả Sơn cũng có một đám tiên hạc",
		pigsy = "Không biết giò tiên hạc ăn có ngon không",
		neza = "Đây chỉ là tượng sao?",
		white_bone = "Linh quang lưu chuyển, không giống vật chết",
		yangjian = "Haha, đây là hai vị hộ vệ Nguyệt Cung sao",
		myth_yutu = "Đừng đứng đó nữa, mau dẫn ta đi chơi.",
		yama_commissioners = "Hai mắt sáng ngời, trong chứa càn khôn",
		madameweb="妾身可看得出你的真身",
	},
	MYTH_INTERIORS_GHG_HE_LEFT ={
		monkey_king = "Trên Hoa Quả Sơn cũng có một đám tiên hạc",
		pigsy = "Không biết giò tiên hạc ăn có ngon không",
		neza = "Đây chỉ là tượng sao?",
		white_bone = "Linh quang lưu chuyển, không giống vật chết",
		yangjian = "Haha, đây là hai vị hộ vệ Nguyệt Cung sao",
		myth_yutu = "Đừng đứng đó nữa, mau dẫn ta đi chơi.",
		yama_commissioners = "Hai mắt sáng ngời, trong chứa càn khôn",
		madameweb="妾身可看得出你的真身",
	},
	MYTH_REDLANTERN ={
		monkey_king = "Lồng đèn một cái, quỷ sứ khỏi chặn đường",
		pigsy = "Ta ở Cao Lão Trang thấy tuần đinh ngày nào cũng cầm",
		neza = "Nhà ta cũng có nhiều đèn giống loại này",
		white_bone = "Nếu ở đây ban đêm quỷ mị không hoành hành, ta còn cần gì đèn lồng",
		yangjian = "Khỏi lửa nhân gian",
		myth_yutu = "Mãu nào đẹp nhất đâu?",
		yama_commissioners = "Đèn hồn vừa khêu, ba hồn xuất hiện",
		madameweb="灯笼薄腊纸，云母含清光",
	},
	MYTH_REDLANTERN_GROUND ={
		monkey_king = "Một gậy của Lão Tôn là vỡ ngay",
		pigsy = "Ta có thể đứng tựa người vào đây không?",
		neza = "Na Tra cũng thích làm đèn này",
		white_bone = "Nguyệt nhược minh đăng, tẩu mã hồng trần",
		yangjian = "Ánh đèn màu hoàng hôn, thật đẹp",
		myth_yutu = "Đúng là cái lồng đèn ưa thích của ta đặt trên đất",
		yama_commissioners = "Bình an này là chính quê hương",
		madameweb="去年元夜时，花市灯如昼。",
	},
	MYTH_RUYI ={
		monkey_king = "Haha, đúng là cái cù nèo năm xưa ở Ngũ Trang Quán",
		pigsy = "Nếu giải tán chia đồ, thì phần ta cái này",
		neza = "Xem ra hình dáng cũng thật kỳ quái",
		white_bone = "Kiện bảo bối này phải về tay ta",
		yangjian = "Tiên linh khí dạt dào",
		myth_yutu = "Đây không phải ngọc như ý của tỷ tỷ sao?",
		yama_commissioners = "Bảo ngọc như ý, như hạc phất phơ",
		madameweb="灵韵内涵，不露清光",
	},

	MYTH_FENCE ={
		monkey_king = "Thủy Liêm Động của Lão Tôn vẫn còn thiếu mấy tấm bình phong như này",
		pigsy = "Lão Trư ta ghét nhất bình phong. Chắn tầm nhìn chả thấy gì.",
		neza = "Nhà Na Tra có nhiều lắm",
		white_bone = "Đằng sau bình phong, xương trắng mọc thịt, vẽ mặt da người, mỹ nhân xuất hiện",
		yangjian = "Che chắn quá thì bất tiện",
		myth_yutu = "Ngọc Thố cũng sẽ vẽ thật nhiều tranh khác nhau nha.",
		yama_commissioners = "Che cái gì giấu cái gì",
		madameweb="人行明镜中，鸟度屏风里。",
	},
	MYTH_FENCE_ITEM = {
		monkey_king = "Thủy Liêm Động của Lão Tôn vẫn còn thiếu mấy tấm bình phong như này",
		pigsy = "Lão Trư ta ghét nhất bình phong. Chắn tầm nhìn chả thấy gì.",
		neza = "Nhà Na Tra có nhiều lắm",
		white_bone = "Đằng sau bình phong, xương trắng mọc thịt, vẽ mặt da người, mỹ nhân xuất hiện",
		yangjian = "Che chắn quá thì bất tiện",
		myth_yutu = "Ngọc Thố cũng sẽ vẽ thật nhiều tranh khác nhau nha.",
		yama_commissioners = "Che cái gì giấu cái gì",
		madameweb="人行明镜中，鸟度屏风里。",
	},

	MYTH_BBN ={
		monkey_king = "Cái túi này thật thần kỳ",
		pigsy = "Ta giấu tiền riêng ở đây, Bật Mã Ôn kia nhất định không biết",
		neza = "Na Tra có thể cất hết pháp bảo vào đây",
		white_bone = "Bảo bối tùy thân của thiếp",
		yangjian = "Lấy ánh trăng tạo ra không gian, hào nhoáng nhưng bất ổn",
		myth_yutu = "Ái chà, Chờ chút nữa lại rót ánh trăng vào",
		yama_commissioners = "Óng ánh trăng hoa rơi giữa túi",
		madameweb="清光落此地，幽幽乱泉声",
	},
	MYTH_YYLP ={
		monkey_king = "Ngọc thạch mà cũng nở hoa được, có nở ra khỉ con được không?",
		pigsy = "Khối phác ngọc thật lớn!",
		neza = "Có thể ngắt một đóa ra cho Na Tra không?",
		white_bone = "Tiên Pháp! Trên mặt ghi toàn thiên cơ!",
		yangjian = "Bảo ngọc để ghi chép",
		myth_yutu = "Để ta xem xem cái này chế tạo thế nào",
		yama_commissioners = "Ngươi muốn lấy thì ta cũng không ý kiến",
		madameweb="清影如璧，内涵万千",
	},
	MYTH_MOONCAKE_ICE ={
		monkey_king = "Băng của Lão Tôn đánh hàn thiềm mà có!",
		pigsy = "Loại bánh trung thu này chỉ có ở Cung Quảng Hàn",
		neza = "Cần để giải nhiệt",
		white_bone = "Có thể giúp ta ngưng tụ thần khí",
		yangjian = "Có thứ này không sợ tẩu hỏa nhập ma",
		myth_yutu = "Lại thêm một cái nữa...",
		yama_commissioners = "Chà, mát lạnh ngon miệng",
		madameweb="可冻死妾身了",
	},
	MYTH_MOONCAKE_NUTS ={
		monkey_king = "Lão Tôn ta chỉ ăn nổi một cái",
		pigsy = "Đồ chơi bé tí này lại khiến lão Trư no bụng",
		neza = "Ăn cái này là cái cuối nha",
		white_bone = "Ăn cái này rồi không cần ăn nữa cũng được",
		yangjian = "Giống ích cốc đan mà ăn ngon hơn",
		myth_yutu = "No quá đi!",
		yama_commissioners = "Ta ăn không hết, lão Phạm chia ngươi phân nửa",
		madameweb="半块薄饼足矣",
	},
	MYTH_MOONCAKE_LOTUS ={
		monkey_king = "Để Lão Tôn nếm thử chút",
		pigsy = "Không có cảm giác gì",
		neza = "Hằng Nga tỷ tỷ cho ta vài cái",
		white_bone = "Lại có thể giúp ta hấp thụ tinh hoa đồ chay",
		yangjian = "Sao cứ phải lo lắng chất lượng đồ ăn thế?",
		myth_yutu = "Bánh trung thu khai vị",
		yama_commissioners = "Đồ ăn ngọt khai vị",
		madameweb="妾身感觉更有食欲了~",
	},

	MYTH_CASH_TREE_GROUND = {
		monkey_king = "Thật tiếc cây không ra quả",
		pigsy = "Lại có thêm tiền",
		neza = "Thật khiến ta động lòng",
		white_bone = "Đúng là hố sâu tham lam",
		yangjian = "Khiến phàm nhân phát rồ cả đám",
		myth_yutu = "Miễn đừng rơi vào đầu ta",
		yama_commissioners = "Cây tham lam, quả dục vọng",
		madameweb="金行灵物，自堕凡俗",
	},

	MYTH_CHANG_E_FURNITURE = {
		monkey_king = "Ngồi thiền xong rồi đi tìm sư phụ vậy",
		pigsy = "Còn không vừa nửa cái mông ta",
		neza = "Nhìn qua thấy thật êm",
		white_bone = "Có thể ngồi lãnh giáo Tiên Tử chuyện tu hành",
		yangjian = "Có thể ngồi nghỉ một chút",
		myth_yutu = "Ngọc Thố mệt quá, phải ngồi một chút",
		yama_commissioners = "Ngồi thiền một phút, tâm tĩnh ngàn giờ",
		madameweb="坐禅？妾身更想躺在那。",
	},
	MYTH_CASH_TREE = {
		monkey_king = "Cây này bón bằng gì?",
		pigsy = "Cái loại hút tiền từ đất này mà cũng cao ghê",
		neza = "Có thể hút tiền kết thành quả",
		white_bone = "Hình dáng ban đầu của tiên bảo",
		yangjian = "Lấy tiền nuôi dưỡng, lại có thể lớn được",
		myth_yutu = "Có khi nó đang đói",
		yama_commissioners = "Cây tham lam dĩ nhiên cần nuôi lớn bằng dục vọng",
		madameweb="",
	},

	MYTH_TREASURE_BOWL = {
		monkey_king = "Có thể biến ra quả đào không?",
		pigsy = "Ta muốn về nhà",
		neza = "Trẻ con không được cá cược đâu",
		white_bone = "Chẳng qua đổi chác thôi",
		yangjian = "Mười đổ chín thua",
		myth_yutu = "Mới săm soi một tí đã bị Hằng Nga tỷ tỷ phát hiện",
		yama_commissioners = "Chớ mê bài bạc, làm hại người nhà",
		madameweb="赌门歪道把人迷",
	},
	MYTH_SMALL_GOLDFROG = {
		monkey_king = "Kiến đông cắn chết voi",
		pigsy = "Mau cút ra",
		neza = "Xem Càn Khôn Quyển của ta!",
		white_bone = "Yêu vật cút đi",
		yangjian = "Thứ tiểu yêu dám mạo phạm Nhị Lang Chân Quân ta!",
		myth_yutu = "Cách xa ta ra!",
		yama_commissioners = "Nhất hồn xuất khiếu",
		madameweb="万丝现，杀气显",
	},
	MYTH_GOLDFROG_BASE = {
		monkey_king = "Chút tài mọn, tưởng sẽ lừa được Lão Tôn?",
		pigsy = "Cái này của Lão Trư ta!",
		neza = "Nơi này sao lại có Đại Nguyên Bảo?",
		white_bone ="Có người đặt bẫy",
		yangjian = "Giấu đầu hở đuôi?",
		myth_yutu = "Cục vàng to quá trời quá đất à!",
		yama_commissioners = "Nguyên bảo khổng lồ sáng rõ",
		madameweb="你想拿这个来得妾身的芳心？",
	},
	MYTH_GOLDFROG = {
		monkey_king = "Để Lão Tôn ta thay ngươi giữ bảo bối",
		pigsy = "Hơi sồ sề, nhưng cũng đủ cho Lão Trư ăn một bữa",
		neza = "Dám vơ vét mồ hôi nước mắt của người dân?",
		white_bone = "Trên ngườ nó có bảo vật ta cần",
		yangjian = "Yêu vật, dám mạo phạm Nhị Lang Hiển Thánh Chân Quân ta!",
		myth_yutu = "Con cóc to ghê quá đi à!",
		yama_commissioners = "Hồn ơi xuất khiếu, phách ơi li thể",
		madameweb="来试试妾身的毒吧",
	},
	MYTH_COIN = {
		monkey_king = "Cái này không dùng được ở Hoa Quả Sơn",
		pigsy = "Đổi thành bạc vụn mới dễ giấu",
		neza = "Tiểu gia không lạ gì cái này",
		white_bone = "Nhân gian khói lửa, thêm lực đúc thành",
		yangjian = "Thế gian hành tẩu, không thể không cần",
		myth_yutu = "Có thể cầm mua củ cải nhé",
		yama_commissioners = "Có tiền có thể sai khiến cả quỷ ma",
		madameweb="哦，红尘滚滚，妾身不愿入内",
	},

	MYTH_YJP = { --玉净瓶
		monkey_king = "Bồ Tát nói bình này cứu được cả cành liễu đã bỏ vào lò đan của Lão Quân",
		pigsy = "Ta còn chưa nếm thử hương vị nước cam lồ",
		neza = "Dùng cái này hút nước bốn bể, chắc chắn sẽ khiến con rồng già tức điên",
		white_bone = "Ta thật đã thấy được trời cao đất rộng",
		yangjian = "Cam lồ màu nhiệm, cây vĩnh viễn xanh",
		myth_yutu = "Có bảo vật này thảo dược của ta sẽ rất xanh tốt",
		yama_commissioners = "Nước thánh ba màu, tưới mát chúng sinh",
		madameweb="身如琉璃，净无瑕秽",
	},

	MYTH_TUDI = { --土地
		monkey_king = "Lão Thổ Địa đã lâu không gặp",
		pigsy = "Thổ Địa, ta tới xin cơm chay",
		neza = "Thổ Địa Công Công cần ta giúp gì?",
		white_bone = "Miếu nhỏ thần bé, Bản Nương không hứng thú",
		yangjian = "Nhờ ngươi khó nhọc bảo vệ vùng đấy này",
		myth_yutu = "Thổ Địa Bà Bà đâu rồi?",
		yama_commissioners = "Thổ địa công công mạnh giỏi. Gần đây có gì lạ không?",
		madameweb="你也要试试本尊的法力？",
	},

	MYTH_TUDI_SHRINES = { --土地庙
		monkey_king = "Lão Thổ Địa có nhà không?",
		pigsy = "Cơm bố thí nếu ăn không hết cứ chừa cho Lão Trư",
		neza = "Thổ Địa Công Công, có nhà không?",
		white_bone = "Ngay cả một tiểu thần pháp lực thấp kém còn được hương khói thờ phụng. Thật bất công.",
		yangjian = "Thổ địa phương này, mau ra diện kiến",
		myth_yutu = "Woa. Tiểu miếu nhỏ xinh.",
		yama_commissioners = "Thổ địa bảo hộ một phương đất ruộng, cúng kiếng cũng tràn đầy",
		madameweb="你这破庙算得上什么",
	},

	BOOKINFO_MYTH = { --土地
		monkey_king = "Để Lão Tôn xem trên này viết cái gì",
		pigsy = "Ra chữ! Ra chữ!",
		neza = "Haha, giờ nó thuộc về Tiểu Gia!",
		white_bone = "Tro tàn ám vào, hiện ra nét chữ",
		yangjian = "Chư thiên ký sự, đều được lưu lại",
		myth_yutu = "Để ta ngó qua một trang xem nào",
		yama_commissioners = "Thiên cơ bất khả lộ",
		madameweb="虽晦涩，心亦喜",
	},

	YANGJIAN_BUZZARD = { --傲天鹰
		monkey_king = "Lão Tôn ta còn chưa quên con trọc điểu mỏ nhọn kia!",
		pigsy = "Haha! Chân gà này chắc chắn là vị ngon khó cưỡng",
		neza = "Ngươi mang cái gì hay ho cho Na Tra vậy?",
		white_bone = "Tên Dương Nhị Lang nhất định đang ở quanh đây, chạy mau thôi",
		yangjian = "Muốn cùng chiến hữu như môi như răng",
		myth_yutu = "Ngươi mang tin gì cho ta hả?",
		madameweb="又是那泼猴来偷衣服吗？",
	},

	MYTH_GRANARY = { --谷仓
		monkey_king = "Không thể để thằng ngu kia phát hiện",
		pigsy = "Đây không phải nhà của Lão Trư ta sao?",
		neza = "Giờ Na Tra chỉ muốn ăn thịt bò",
		white_bone = "Đồ ăn nhân gian, ai mà ghét được",
		yangjian = "Nhà kho lầm lẫm, dân chúng ấm no",
		myth_yutu = "Ta muốn lôi nó ra trang trí!!!",
		madameweb="人间无战祸，何来我乘机",
	},
	MYTH_WELL = { --水池
		monkey_king = "So với nước suối ở Hoa Quả Sơn vẫn thua ba phần",
		pigsy = "Để ta nếm thử xem nước này có ngọt hay không",
		neza = "Na Tra tắm ở đây được không?",
		white_bone = "Gột rửa bụi trần",
		yangjian = "Nước giếng coi như cũng ngọt",
		myth_yutu = "A! Không được hắt nước ta!",
		madameweb="妾身来加点东西？",
	},

	MOVEMOUNTAIN_PILL = { --移山丹
		monkey_king = "Lão Tôn còn cần cái này sao?",
		pigsy = "Ta mới không cần ăn, ăn vào phải làm cu li",
		neza = "Cứng quá! Nắm tay của Na Tra cứng quá!",
		white_bone = "Đan dược tăng lực tạm thời",
		yangjian = "Trò mèo như này",
		myth_yutu = "Ta chỉ cần một chày có thể cho ngươi gặp Diêm Vương",
		madameweb="短时所获，并非所有",
	},


	MYTH_FOOD_SJ = { --水饺
		yangjian = "Năm tháng trôi qua, chỉ mong thường gặp lại",
	},

	MYTH_FOOD_BZ = { --大肉包子
		pigsy = "Để ta nếm thử xem là vị gì, đây là...",
	},
	MYTH_FOOD_HSY = { --麻辣红烧鱼
		neza = "Na Tra lại phải thêm chút ớt",
	},
	MYTH_FOOD_BBF = { --八宝饭
		neza = "Ngọt mà không ngấy, ích lợi thật nhiều",
	},
	MYTH_FOOD_TSJ = { --屠苏酒
		monkey_king = "Cho ta thêm hai vò rượu ngon",
		pigsy = "Đây là phần rượu ngon nhất năm",
		neza = "Na Tra liền nếm một ngụm nhỏ",
		white_bone = "Có chăng cùng thiếp giải sầu, eo thon như liễu môi màu như hoa",
		yangjian = "Giết dê mổ trâu mời chúng bạn, cùng nhau uống cạn bốn trăm chung",
		myth_yutu = "Dưới trăng ta uống say sưa, Hằng Nga tỷ tỷ cũng vừa cạn ly",
	},
	MYTH_FOOD_TR = { --糖人
		monkey_king = "Ta trông thấy thật quen mắt",
		pigsy = "Hừ, xem ta một ngoạm đứt đầu ngươi",
		neza = "Mau xem đồ chơi bằng đường của Na Tra",
		white_bone = "Sao lại là con khỉ ngang ngược?",
		yangjian = "Con nít mới ăn cái này. Ngọt quá!",
		myth_yutu = "Mũi hít hà mồm nhỏ dãi",
	},

	MYTH_TOY_TIGERDOLL = { --布老虎
		neza = "Gừ, lão hổ ta đến đây!",
	},

	MYTH_TOY_FEATHERBUNDLE  = { --毽子
	white_bone = "Nhớ ta ngày xưa đó, tung tay ngón chân hoa",
	},

	MYTH_SIVING_BOSS = { --子圭玄鸟
		monkey_king = "Chim ngu, để Lão Tôn xem ngươi là cái loài chim gì",
		pigsy = "Nhìn sơ không thấy có miếng thịt nào",
		neza = "Phong Hỏa Luân của Na Tra bay cũng không thua ngươi",
		white_bone = "Phải chi ta chiếm được tu vi của ngươi...",
		yangjian = "Thiên mệnh huyền điểu? Ngươi rốt cục đã muốn chấm dứt nhân quả phàm trần chưa?",
		myth_yutu = "Chim nhỏ chim nhỏ, ngươi ở đây chi vậy?",
	},

	SIVING_ROCKS = { --子圭石
		monkey_king = "Chà! Cái này cũng biến lớn như Kim Cô Bổng của ta được",
		pigsy = "Không ăn được, bảo ta dùng làm gì đây",
		neza = "<Chọc chọc> Ngươi làm cái gì đó đi!",
		white_bone = "Một sinh vật kỳ diệu",
		yangjian = "Không thánh không phàm, có sinh cơ mà không linh trí",
		myth_yutu = "Hòn đá nhỏ thật khá",
	},

	ARMORSIVING = { --子圭战甲
		monkey_king = "Thân ta dù chết, chí vẫn không sờn",
		neza = "Cốt nhục thành bùn trả phàm trần",
		white_bone = "Từ bé đã là yêu, còn ngại ngần gì",
		yangjian = "Thiên lôi rõ ràng, thiên uy rực rỡ",
	},

	SIVING_HAT = { --子圭战盔
		monkey_king = "Khiến Lăng Tiêu kia cúi mặt, nhìn Linh Sơn hắn điên cuồng",
		neza = "Ngó vàng sen ngọc lại một hồi",
		white_bone = "Đoạt thiên cải mệnh, ta tự ngông cuồng",
		yangjian = "Hô mưa gọi gió, diệu đạo Thanh Nguyên",
	},

	MYTH_LOTUS_FLOWER = { --莲花
		monkey_king = "Đẹp quá là đẹp",
		neza = "Hoa sen hiện hóa thân, Linh Châu xuất phàm trần",
		white_bone = "Mặt thiếp cũng không đẹp bằng hoa",
		pigsy = "Đài sen to của ta đâu rồi?",
		myth_yutu = "Hằng Nga tỷ tỷ nhất định sẽ thích đóa sen mới nở này",
		yangjian = "Hoa sen Tây Hồ mới nở à",
	},

	LOTUS_ROOT = { --莲藕
		monkey_king = "Không ngọt bằng đào trên Hoa Quả Sơn",
		neza = "Đây là cánh tay, còn chân đâu?",
		pigsy = "Lão Trư ta một ngoạm ăn được cả tám cái",
		myth_yutu = "Để nữ đầu bếp nhỏ nấu biểu diễn món củ sen thịt nguội",
		--yangjian = "西湖的荷花开了吗",
	},

	LOTUS_SEEDS = { --莲子
		neza = "Con ngươi? Tiểu gia thấy ghép lại ngắm được mà?",
		pigsy = "Đừng để nó đến đánh Lão Trư nha.",
		myth_yutu = "Áo hồng ráng sức giã hạt xanh",
		yangjian = "Tim sen dù đắng lại giúp tĩnh khí hoàn hồn",
	},

	MYTH_LOTUSLEAF = { --莲叶
		monkey_king = "Cũng che được mưa",
		neza = "Ta chính là lấy lá sen làm áo",
		white_bone = "Che mưa đậy gió? Ta không dùng được ngươi",
		myth_yutu = "Gió nổi mưa sa tan ngọc châu",
		pigsy = "Thằng nhóc đó đánh sao lại ta",
	},

	MYTH_LOTUSLEAF_HAT = { --莲叶帽子
		monkey_king = "Lớn thêm chút nữa thì tốt rồi",
		neza = "Na Tra đội Na Tra, hahaha",
		white_bone = "Xanh ngọc như dù che đầu thiếp",
		myth_yutu = "Mưa nhỏ rơi trên kêu tí tách",
		pigsy = "Ban ngày ngủ lấy cái này che là đẹp",
		yangjian = "Chớ giỡn mặt ta",
	},

	MYTH_FOOD_ZYOH = { --折月藕盒
		myth_yutu = "Ước gì nó làm đẹp giấc mơ của bạn",
	},

	MYTH_FOOD_HBJ = { --荷包鸡
		monkey_king = "Đương nhiên không được để thằng ngu kia thấy",
		neza = "Nóng quá! Nóng quá!",
		myth_yutu = "Ở Nguyệt Cung chưa từng ăn qua",
		pigsy = "Ta phải ăn trước nhất",
		yangjian = "Có muốn ta cho ngươi một phần không Hao Thiên",
	},

	LAOZI_BELL = { --兜率牛铃
		monkey_king = "Con trâu của Lão Quân chạy đâu rồi?",
		neza = "Đinh đinh đang đang",
		myth_yutu = "Ta đoán đây nhất định là cái chuông đẹp nhất",
		pigsy = "Phiền chết được, không cho ta ngủ yên không được sao?",
		white_bone = "Thế này đến Thần Tài cũng phải giậm chân giận dữ",
		madameweb="啧啧啧，仙神的玩物罢了",
	},

	LAOZI_QINGNIU  = { --青牛
		monkey_king = "Lão Quân à trâu của ông lại chạy lạc rồi.",
		neza = "Chính là ngươi lần trước trộm binh khí của Na Tra?",
		myth_yutu = "Đại Thanh Ngưu mau chở ta đi chơi",
		pigsy = "Mau thay lão Trư ta cày ruộng",
		white_bone = "Cúi đầu hèn mọn, cam làm tọa kỵ?",
		yangjian = "Thanh Ngưu này sao không ở Đâu Suất Cung?",
		madameweb="青牛君安好，可要庇护小妹",
	},

	SADDLE_QINGNIU  = { --兜率牛鞍
		monkey_king = "Ghế ngồi của Lão Quân",
		neza = "Na Tra cũng thích ngồi thử",
		myth_yutu = "Có cái này mông sẽ không đau",
		pigsy = "Không biết nằm trên đây ngủ có ngon không",
		white_bone = "Nhất định là pháp bảo khống chế con trâu điên kia",
		yangjian = "Yên ngồi của Lão Quân",
		madameweb="此等修为，为人坐骑？呸！",
	},

	MYTH_QXJ  = { --七星剑
		monkey_king = "Bảo bối của Lão Quân lại bị đồng tử trộm ra đây",
		neza = "Xem Tiểu Gia một đòn diệt sát toàn bộ yêu quái",
		myth_yutu = "Mặt trên đầy sao thật đẹp",
		pigsy = "Không biết bổ dưa hấu có nhanh hay không",
		white_bone = "Ở đâu ra vật gì gớm thế này?",
		yangjian = "Hà Quang như kiếm, ung dung thanh hàn",
		madameweb="拔剑起，杀意临",
	},

	MYTH_BAMBOO  = { --竹子
		monkey_king = "Đoạn trúc này cầm lên cũng thật tiện tay",
		neza = "Như trúc um tùm, như tùng rậm rạp",
		myth_yutu = "Nghe tiếng gió thổi qua lá trúc",
		pigsy = "Hay cho một chỗ râm mát, ta đang muốn đánh một giấc",
		white_bone = "Che giấu trúc ảnh, thướt tha dịu dàng",
		yangjian = "Không sợ nóng lạnh, vạn cổ vẫn xanh",
		madameweb="竹畔疏帘里，舞罢风掀袂。",
	},

	MYTH_YAMA_STATUE1 = { --阎罗雕像 1级
		monkey_king = "Một cái tượng bể tè le",
		neza = "Là Nguyên THủy Diêm Quân mà",
		myth_yutu = "Hằng Nga Tỷ Tỷ nói Diêm Vương cũng muốn lên đây",
		pigsy = "Tảng đá to vừa hay ta kê đầu ngủ một giấc",
		white_bone = "Không tốt, Diêm Vương muốn trực tiếp tới bắt ta?",
		yangjian = "Diêm Vương cũng muốn vào cõi này sao?",
		yama_commissioners = "Miếu thờ sơ sài, Diêm Quân tha tội",
		madameweb="我来给你填点蛛丝",
	},

	MYTH_YAMA_STATUE2 = { --阎罗雕像 2级
		monkey_king = "Nhìn sơ hình như thờ Diêm Vương",
		neza = "Diêm Vương Bá Bá sao nhợt nhạt thế này",
		myth_yutu = "Một chút bánh hấp, kính ý sơ sài",
		pigsy = "Miếu gì rách nát, chút xôi oẳn cũng không có",
		white_bone = "Đáng chết, ai lại dựng miếu thờ này?",
		yangjian = "Diêm Quân nếu muốn có thể lắng nghe, cũng có thể phản hồi",
		yama_commissioners = "Thờ kính bằng cặp đèn cầy với ba cây nhang",
		madameweb="有些不一样了",
	},
	MYTH_YAMA_STATUE3 = { --阎罗雕像 3级
		monkey_king = "Không nghĩ tới Diêm Vương cũng rảnh rỗi lên đây chơi",
		neza = "Diêm Quân Bá Bá còn chưa tỉnh ngủ à?",
		myth_yutu = "Một chút hương nến, kính ý sơ sài",
		pigsy = "Khi khác trời mưa cho ta mượn chỗ của ngươi dùng tạm",
		white_bone = "Ta phải nghĩ cách cản Diêm Vương tới",
		yangjian = "Diêm Quân chính là từng bước muốn tiến vào thế giới này",
		yama_commissioners = "Miếu cao đã dựng, mưa gió chẳng vào",
		madameweb="他的法力已经倾注其中",
	},
	MYTH_YAMA_STATUE4 = { --阎罗雕像 4级
		monkey_king = "Diêm Vương lão đệ xem thế mà miếu mả khang trang gớm",
		neza = "Diêm Quân Bá Bá mạnh giỏi!",
		myth_yutu = "Bụi cây kia cũng thật gớm!",
		pigsy = "Ui! Hóa ra Diêm Quân Đại Nhân. Có mang bảo bối gì lên không?",
		white_bone = "Không ổn! Không ổn! Chuồn thôi!",
		yangjian = "Chúc mừng Diêm Quân tới cõi này",
		yama_commissioners = "Bỉ ngạn hoa nở, vua hiện bàn thờ",
		madameweb="神祗所在，不可力敌",
	},

	MYTH_HIGANBANA_ITEM = { --彼岸花
		monkey_king = "Hoa này đẹp quá Lão Tôn ta thích",
		neza = "Hoa đẹp nhưng lại khiến Na Tra sợ hãi!",
		myth_yutu = "Nhớ chuyện xưa, người xuống suối vàng còn ai nhớ",
		pigsy = "Không biết so với mẫu đơn thì cái nào ăn ngon hơn",
		white_bone = "Hoa xuân như mặt, mưa thu không ngớt",
		yangjian = "Hoa này cũng mọc theo dòng vong xuyên",
		yama_commissioners = "Hoa nở dẫn độ âm hồn",
		madameweb="花与叶间了无缘",
	},

	MYTH_CQF = { --出窍符咒
		monkey_king = "Lão Tôn còn cần cái này sao?",
		neza = "Cho Na Tra thử  trước đi",
		myth_yutu = "Dùng cái này có thể lén đi ra ngoài chơi",
		pigsy = "Lão Trư nhất định không dùng cái này lén xem gái tắm đâu",
		white_bone = "Thiếp chỉ là một cái tàn hồn, nguyên thần cùng hồn không thể tách",
		yangjian = "Mặt trời chiếu khắp, nhưng tâm an nhiên",
		yama_commissioners = "Ta là thần ở Âm Ti, không cần xuất khiếu",
		wortox = "Linh hồn ly thể? Ngươi bắt muốn bắt hồn ta xuống địa ngục sao?",
		wormwood = "Linh hồn của ta cắm rễ lâu rồi",
		wurt = "Bùa lãng nhách! Phải lấy cả linh hồn lẫn thể xác để cung phụng quốc vương",
		wanda = "Nếu tách rời thân xác, sức mạnh thời gian sẽ xé tan hồn ta",
		madameweb="妾身怎能离开这完美的身躯",
	},
	HAT_COMMISSIONER_BLACK = { --无常官帽黑
		monkey_king = "Ha, đây không phải mũ của tên quan nhỏ da đen sao?",
		neza = "Nhị vị vô thường sao lại đánh rơi pháp khí?",
		myth_yutu = "Để đem trả mũ lại cho tiểu Hắc",
		pigsy = "Lão Trư đội mũ này thì có đẹp trai không?",
		white_bone = "Thiếp chỉ là một cái tàn hồn",
		yangjian = "Không phải mũ của Hắc Vô Thường sao, sao lại đánh rơi ở đây",
		madameweb="噗~这是什么腌臜之物",
	},
	HAT_COMMISSIONER_WHITE = { --无常官帽黑
		monkey_king = "Ha, đây không phải mũ của tên quan nhỏ trắng bệch sao?",
		neza = "Nhị vị vô thường sao lại đánh rơi pháp khí?",
		myth_yutu = "Để đem trả mũ lại cho tiểu Bạch",
		pigsy = "Lão Trư đội mũ này có giống chú rể không?",
		white_bone = "Thiếp chỉ là một cái tàn hồn",
		yangjian = "Không phải mũ của Bạch Vô Thường sao, sao lại đánh rơi ở đây",
		madameweb="噗~这是什么腌臜之物",
	},
	PENNANT_COMMISSIONER  = {--"招魂幡"
		monkey_king = "Lá cờ rách này thì lấy đâu ra khả năng gọi hồn",
		neza = "Phiên kỳ phấp phới, trông ra cũng đẹp",
		myth_yutu = "Là ai trong sương mù đang tới?",
		pigsy = "Giúp ta tìm mấy nữ thí chủ xinh đẹp",
		white_bone = "Bản hồn phách này, lại giúp ta bữa ăn ngon",
		yangjian = "Không thể tùy ý chiêu hồn",
		madameweb="远方魂，于我为奴",
	},
	BELL_COMMISSIONER = { --摄魂铃
		monkey_king = "Rung nhanh đi lão Tôn đang muốn ngủ",
		neza = "Làm Na Tra nhớ bài hát ru của mẹ",
		myth_yutu = "Hằng Nga tỷ tỷ, ta buồn ngủ",
		pigsy = "Oáp... zｚＺ",
		white_bone = "Nhiếp hồn câu phách",
		yangjian = "Pháp khí làm nhiễu loạn thần thức",
		madameweb="为我而活，为我而战",
	},
	WHIP_COMMISSIONER ={ --勾魂锁
		monkey_king = "Ngon thì đến câu Lão Tôn thử xem? Haha",
		neza = "Pháp khí có oán khí hoành hành",
		myth_yutu = "Ta không hiểu cách dùng cái này",
		pigsy = "Haha, tránh Lão Trư xa xa một chút",
		white_bone = "Kia không phải Câu Hồn Tỏa chuyên bắt quỷ hồn sao?",
		yangjian = "Oán khí quá nhiều cũng chỉ vì cái Câu Hồn Tỏa này",
		madameweb="元神？给妾身出来吧",
	},
	TOKEN_COMMISSIONER = { --镇魂铃
		monkey_king = "Âm binh thấy Lão Tôn còn không mau nhường đường",
		neza = "Hay là thôi Na Tra vẫn tránh sang bên đi",
		myth_yutu = "Trăng hoa thái âm, chiếu rọi chư thần",
		pigsy = "Ta ở lại trước",
		white_bone = "Âm khí dày đặc, là vật phiền hà",
		yangjian = "Bầy quỷ trông thấy, ai dám không them",
		madameweb="阴司所过，何人不避？",
	},
	MYTH_HUANHUNDAN = { --还魂丹
		monkey_king = "Khá khen Lão Quân kia, giấu cái này kỹ quá Lão Tôn không thấy",
		neza = "Sư phụ đã vì ta mà đi xin mấy viên này",
		myth_yutu = "So với kẹo hồ lô thì có ngon hơn không?",
		pigsy = "Có thể... cho Lão Trư một viên nữa không?",
		white_bone = "Nhất địch có lợi ích lớn cho nguyên thần của ta",
		yangjian = "Lão Quân rất hiếm khi mở lò luyện đan này",
		yama_commissioners  = "Hồn ơi đến đây",
		madameweb="定神！不可损之",
	},
	MYTH_MOONCAKE_BOX = {
		monkey_king = "Tặng sư phụ một phần, người sẽ rất vui vẻ",
		neza = "Na Tra nhớ nhà, muốn mẹ làm bánh trung thu này",
		myth_yutu = "Ăn ngay một miếng nhỏ",
		pigsy = "Sao phần của ta lại hết rồi? Haiz...",
		white_bone = "Xem ta biến bầy cóc thành đồ chay bỏ đầy hộp",
		yangjian = "Tiên tử thật có tâm",
		madameweb="妾身来尝尝有何好物",	
	},

	MYTH_COIN_BOX = {
		monkey_king = "Mùi tiền dơ bẩn",
		neza = "Một xâu tiền dài! Ở Trần Đường Quan chừng này tiền mua được nhiều kẹo lắm",
		myth_yutu = "Một cây cải củ mấy văn tiền?",
		pigsy = "Lần này ta phải giấu thật kỹ",
		white_bone = "Haha, ta thích bao nhiêu chẳng biến ra được",
		yangjian = "Tục vật nhân gian, nhơ nhớp bụi trần",
		yama_commissioners  = "Sinh ra tay trắng, chết lại trắng tay",
		madameweb ="奴家买东西可不用钱",
	},
	MYTH_BAMBOO_BASKET = {
		myth_yutu = "Ta cũng đâu phải đi hái nấm",
	},
	--蛛网陷阱(种下去的)
	MADAMEWEB_PITFALL_GROUNG = {
		monkey_king = "休想骗俺老孙",
		neza = "小爷我可不是你能骗的了的",
		myth_yutu = "我可不是小兔，不是小白",
		pigsy = "朗格里格朗，太阳真真好",
		white_bone = "呵呵，雕虫小技罢了",
		yangjian = "何人在此设阱",
		yama_commissioners  = "吾等幽魂，何惧缠身",		
		madameweb ="你要来试试吗？",
	},
	--困顿之茧
	MADAMEWEB_PITFALL_COCOON = {
		monkey_king = "傻子上当了",
		neza = "呵呵，有笨蛋上当了",
		myth_yutu = "有小白上当了",
		pigsy = "哎呦，吓死俺老猪了",
		white_bone = "可惜呀~",
		yangjian = "白痴！",
		yama_commissioners  = "既已缠身，随吾长行",		
		madameweb ="动弹不得",
	},
	--困顿之茧
	MADAMEWEB_PITFALL_COCOON = {
		monkey_king = "傻子上当了",
		neza = "呵呵，有笨蛋上当了",
		myth_yutu = "有小白上当了",
		pigsy = "哎呦，吓死俺老猪了",
		white_bone = "可惜呀~",
		yangjian = "白痴！",
		yama_commissioners  = "既已缠身，随吾长行",		
		madameweb ="动弹不得",
	},
	--盘丝蜂针
	MADAMEWEB_STINGER = {	
		madameweb ="何必穿针上彩楼",
	},
	--盘丝蜂针
	MADAMEWEB_POISONSTINGER = {	
		madameweb ="金针玉指弄春丝",
	},
	--毒蜂
	MADAMEWEB_BEE = {
		monkey_king = "好阴狠的手段",
		neza = "混天绫在此，何惧尔等",
		myth_yutu = "哎呀，我先遁地了",
		pigsy = "别蜇俺，快走开",
		white_bone = "好生毒辣",
		yangjian = "毒物不可长留",
		yama_commissioners  = "丝丝毒，附骨疽",		
		madameweb ="沾染毒气的蜜蜂",
	},
	--盘丝蛛卵
	MADAMEWEB_POISONBEEMINE = {
		monkey_king = "嘿嘿，给你孵化又何妨",
		neza = "不可给机会",
		myth_yutu = "决策要成早",
		pigsy = "这比蝉蛹好吃吗",
		white_bone = "不为我所用，不可留",
		yangjian = "斩草需除根",
		yama_commissioners  = "此为生，吾为死",		
		madameweb ="妾身儿郎的产房",
	},
	--盘丝蛛巢
	MADAMEWEB_SPIDERHOME = {
		monkey_king = "吃俺老孙一棒",
		neza = "不可让它发展壮大",
		myth_yutu = "咦！恶心的蛛卵在动",
		pigsy = "什么东西在里面动",
		white_bone = "斯，未出生的蛛卵",
		yangjian = "邪恶含蕴的温床",
		yama_commissioners  = "盘丝缠，游蛛产",		
		madameweb ="蛛丝会保护你们",
	},
	--盘丝毒蛛
	MADAMEWEB_SPIDER_WARRIOR = {
		monkey_king = "哼，俺老孙不打女人，可饶不了你们这群怪物",
		neza = "你可别来招惹小爷",
		myth_yutu = "打不过...还是跑吧...",
		pigsy = "你这小玩意，长的还挺有趣",
		white_bone = "我更喜欢你们，化为白骨",
		yangjian = "助纣为虐，不可轻饶",
		yama_commissioners  = "铃儿摇，蛛儿跑，旗而飘，魂而消",		
		madameweb ="我的儿郎们可不是好惹的",
	},
	--盘丝披肩
	MADAMEWEB_ARMOR = {
		monkey_king = "这有啥好看的",
		neza = "这是女人家家的衣服，哪吒才不要呢",
		myth_yutu = "我才不要穿那个呢",
		pigsy = "嘿嘿嘿嘿，好看",
		white_bone = "奴家闻到了肮脏的蛛丝味",
		yangjian = "天眼所见，妖气横斜",
		yama_commissioners  = "红粉骷髅，白骨皮肉",		
		madameweb ="肩上霓裳美若珠",
	},
	--盘丝织袋
	MADAMEWEB_SILKCOCOON = {
		monkey_king = "这袋子倒是有趣",
		neza = "哪吒的东西会被弄脏的",
		myth_yutu = "黏糊糊的，我才不要用呢",
		pigsy = "让俺看看有没有吃的",
		white_bone = "我那披风，自有乾坤",
		yangjian = "这东西哮天都嫌弃",
		yama_commissioners  = "一个容器罢了",		
		madameweb ="且待明日，适时而开",
	},

	--盘丝标记
	HUA_INTERNET_NODE = {
		monkey_king = "俺老孙一个筋斗就走了",
		neza = "风火轮可比你快多了",
		myth_yutu = "我还是去打个秋千吧",
		pigsy = "俺上去不会踩断吧",
		white_bone = "我行如风，不为所阻",
		yangjian = "一堆玩具罢了",
		yama_commissioners  = "游行阴阳，来来往往",		
		madameweb ="蛛网之上，我自称王",
	},
	

	--盘丝标记水
	HUA_INTERNET_NODE_SEA = {
		monkey_king = "俺老孙一个跟头的事情，居然弄这么麻烦",
		neza = "风火轮在上面滑会不会烧坏丝呢",
		myth_yutu = "昔日洛神舞，今有本兔行，嘿嘿",
		pigsy = "吓死俺了，俺差点掉下去了",
		white_bone = "此物倒是便捷",
		yangjian = " 哦，水面滑行的感觉还不错",
		yama_commissioners  = " 额，我还是离水远一点吧",		
		madameweb ="乖乖的不许动哦",
	},

	--步丝履
	HUA_FAKE_SPIDER_SHOE = {
		monkey_king = "俺老孙的脚都被黏住了",
		neza = "哪吒还是更喜欢风火轮",
		myth_yutu = "呼呼，跑不过我哟！",
		pigsy = "这鞋怎么有点挤脚",
		white_bone = "此物可助我悬丝而行 ",
		yangjian = "休想让我穿上这双鞋",
		yama_commissioners  = "黏糊糊的",		
		madameweb ="此鞋可助你悬丝而行",
	},

	--盘丝悬茧
	MADAMEWEB_SILKCOCOON = {	
		madameweb ="腹丝如注，长悬空中",
	},

	--红包
	MYTH_PLAYERREDPOUCH_SUPER = {
		monkey_king = "嘿嘿，这红包倒是份量足足",
		neza = "这个红包比以往的都要厚！",
		myth_yutu = "嘿嘿，回去找嫦娥姐姐再要一份",
		pigsy = "俺的媳妇本又厚了",
		white_bone = "奴家的是最厚的吗？",
		yangjian = "这红包居然有我的一份",
		yama_commissioners  = "让我看看我的这份",		
		madameweb ="妾身是不是也要准备一些",
	},
	--铸铁大刀
	MYTH_IRON_BROADSWORD = {
		monkey_king = "凡兵俗器，一折就断了",
		neza = "火尖枪一碰就碎了",
		myth_yutu = "我还是不要这个了",
		pigsy = "吓死我了，我还以为是杀猪刀呢",
		white_bone = "啊，这可杀不死我",
		yangjian = "欲将轻骑逐,大雪满弓刀",
		yama_commissioners  = "俺来试试刮胡子",		
		madameweb ="风刀霜剑严相逼",
	},
	--铸铁战甲
	MYTH_IRON_BATTLEGEAR = {
		monkey_king = "俺老孙可不背这破铜烂铁",
		neza = "三面精金甲，妖魔破胆还",
		myth_yutu = "哈哈，现在你们打不过我啦",
		pigsy = "穿上去有点小挤",
		white_bone = "朔气传金柝，寒光照铁衣",
		yangjian = "羽仪映松雪，戈甲带春寒。",
		yama_commissioners  = "也许我并不需要它的保护",		
		madameweb ="要在哪里补一点蛛丝呢",
	},
	--苍竹瓦屋
	MYTH_HOUSE_BAMBOO = {
		monkey_king = "这哪能比得上俺老孙的水帘洞",
		neza = "小爷家可比这金碧辉煌",
		myth_yutu = "哇，好漂亮的小房子呀",
		pigsy = "睡哪里都差不多",
		white_bone = "歇脚之地罢了",
		yangjian = "宁可食无肉,不可居无竹",
		yama_commissioners  = "俺想小憩一会了",		
		madameweb ="让妾身看看哪里适合布上蛛丝",
	},
	--年兽
	MYTH_NIAN = {
		monkey_king = "大胆妖孽，扰乱新年，吃俺老孙一棒",
		neza = "哪吒的年夜饭都被你搅合了",
		myth_yutu = "哼...你快滚开啦",
		pigsy = "俺老猪放个屁就能吓跑你",
		white_bone = "年之皮骨，血肉法力都将归我",
		yangjian = "作乱人间，律令当斩",
		yama_commissioners  = "凡间事本不该管",		
		madameweb ="妾身的宴席都被你吓散了",
	},
	--年兽坐骑
	NIAN_MOUNT = {
		monkey_king = "吃了一棒，倒是乖顺了几分",
		neza = "你快赔哪吒的年夜饭",
		myth_yutu = "驾！跑快点呦",
		pigsy = "俺以后就省去了驾云的力气了",
		white_bone = "罢了，饶你一命",
		yangjian = "知错能改，善莫大焉",
		yama_commissioners  = "怎得，你还想被打一顿不成",		
		madameweb ="你以后就当妾身的脚力吧",
	},
	--年兽铃铛
	NINA_BELL = {
		monkey_king = "晓得俺老孙的厉害了吧",
		neza = "嘿嘿，以后哪吒不想飞的时候你就出来吧",
		myth_yutu = "以后不许胡闹了，知道吗",
		pigsy = "什么！你还想和俺抢吃的",
		white_bone = "蠢钝愚兽，可惜难以炼化",
		yangjian = "孽畜，饶你一命",
		yama_commissioners  = "额，它好像吐了",		
		madameweb ="吃了我的儿郎，就得",
	},
	--串天候
	MINIFLARE_MYTH = {
		monkey_king = "俺老孙飞的可比你高多了",
	},

	--桃木手杖
	CANE_PEACH = {
		monkey_king = "快给俺老孙结几个果子",
		neza = "老人家才用拐杖呢",
		myth_yutu = "小棍子你能变成胡萝卜田吗",
		pigsy = "你都给大师兄了，凭什么不给俺",
		white_bone = "虽有仙气，却已经消散八分",
		yangjian = "夸父也是一个伟人",
		yama_commissioners  = "这旺盛的活力，似有长春不老之意",		
		madameweb ="若是能带回我那花园倒是不错",
	},

	--被推倒的树根
	MYTH_PLANT_INFANTREE_TRUNK = {
		monkey_king = "看俺老孙干嘛，俺老孙可不认识这个",
		neza = "可怜叶落根出土，道人断绝草还丹",
		myth_yutu = "月华浇灌也难回天命",
		pigsy = "俺还想再尝尝人参果那味呢",
		white_bone = "如此仙根！是谁暴殄天物",
		yangjian = "可惜，杨戬法力救不回它",
		yama_commissioners  = "嘘，万寿观那位听见阎王爷也保不住你",		
		madameweb ="了无生机，鸡肋之物",
	},

	--被推倒的树根
	MYTH_PLANT_INFANTREE_TRUNK = {
		monkey_king = "看俺老孙干嘛，俺老孙可不认识这个",
		neza = "可怜叶落根出土，道人断绝草还丹",
		myth_yutu = "月华浇灌也难回天命",
		pigsy = "俺还想再尝尝人参果那味呢",
		white_bone = "如此仙根！是谁暴殄天物",
		yangjian = "可惜，杨戬法力救不回它",
		yama_commissioners  = "嘘，万寿观那位听见阎王爷也保不住你",		
		madameweb ="了无生机，鸡肋之物",
	},
	--复苏仙树
	MYTH_PLANT_INFANTREE_MEDIUM = {
		monkey_king = "俺老孙有的是方子",
		neza = "百折磨难，总算有点效果了",
		myth_yutu = "玉露润泽，春芽冬发",
		pigsy = "俺老猪来给它一泡大肥",
		white_bone = "枝青果出，奴家再来",
		yangjian = "复原如此当是奇迹",
		yama_commissioners  = "仙根岂是俗物",		
		madameweb ="灵气晕染，时间快到了",
	},
	--仙树
	MYTH_PLANT_INFANTREE = {
		monkey_king = "见到俺老孙可不要发抖哦",
		neza = "这比王母娘娘的蟠桃树还要高大",
		myth_yutu = "广寒宫的月桂也没有如此高大",
		pigsy = "再给俺一个果子尝尝味道",
		white_bone = "待到瓜熟蒂落之日",
		yangjian = "若有机会看看可否向大仙讨要一枚果子",
		yama_commissioners  = "这还能活也是怪哉",		
		madameweb ="我这蛛丝也不是五行之属",
	},
	--人生果
	MYTH_INFANT_FRUIT = {
		monkey_king = "俺老孙自有长生之法",
		neza = "哪吒也只曾听闻此等仙果",
		myth_yutu = "还是...等嫦娥姐姐...来要吧",
		pigsy = "再尝一个？",
		white_bone = "长生触手可及",
		yangjian = "若食之，可免调和龙虎，捉坎填离的功夫",
		yama_commissioners  = "俺就是想尝尝味道",		
		madameweb ="草还丹，丹霞缭绕的宝物",
	},
	--金击子
	MYTH_GOLD_STAFF = {
		monkey_king = "俺老孙的金箍棒也是五金之属",
		neza = "哪吒的火尖枪可比这好用多了",
		myth_yutu = "梆梆两下，人参果就下俩了",
		pigsy = "借俺悄悄一用",
		white_bone = "奴家只要一下",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "这十足赤金，人家居然只是拿来摘果子",		
		madameweb ="我这蛛丝不知可否牵下",
	},
	--判官笔
	COMMISSIONER_BOOK = {
		monkey_king = "哟哟，咋查不到俺老孙的名字？",
		white_bone = "死又何妨，天地游荡",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "生死簿可不该出现在这",		
	},
	COMMISSIONER_FLYBOOK = {
		monkey_king = "哟哟，咋查不到俺老孙的名字？",
		white_bone = "死又何妨，天地游荡",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "生死簿可不该出现在这",		
	},
	--孟婆汤
	COMMISSIONER_MPT = {
		monkey_king = "休想骗俺老孙喝下这孟婆汤",
		neza = "有很多美好的记忆不能忘记",
		myth_yutu = "啊，味道怪怪的，我不要喝",
		pigsy = "俺尝尝..嗝，俺再尝尝..嗝，俺...",
		white_bone = "我的苦痛与不甘，岂能被你洗去",
		yangjian = "往日不可追，记忆却不能忘却",
		yama_commissioners  = "老范，你那脑袋瓜子不喝也记不住多少",		
		madameweb ="本座还没到轮回的时候",	
	},
}


for k,v in pairs(speech) do
	for k1,v1 in pairs(v) do
		STRINGS.CHARACTERS[string.upper(k)].DESCRIBE[k1] = v1
	end
end

for k,v in pairs(newitems) do
	for k1,v1 in pairs(v) do
		STRINGS.CHARACTERS[string.upper(k1)].DESCRIBE[k] = v1
	end
end