
global("TheHua")
GLOBAL.TheHua = GLOBAL.TheHua or {}

TheHua.CheckIsMadamWeb = function(_, player)
    return player and (player:HasTag("madameweb")--[[ or player:HasTag("valkyrie")]])
end

TheHua.CheckIsRealSpiderMan = function(_, player)
    return player and (player:HasTag("spiderwhisperer") or TheHua:CheckIsMadamWeb(player))
end

TheHua.CheckIsGeneralSpiderMan = function(_, player, no_real)
    if not player then
        return
    end

    if TheHua:CheckIsRealSpiderMan(player) then
        return not no_real
    end

    if player.components.inventory == nil then
        return player.replica.inventory and player.replica.inventory:HasItemWithTag("hua_fake_spider",1)
    end

    for k, v in pairs(player.components.inventory.itemslots) do
        if v and v:HasTag("hua_fake_spider") then
            return true
        end
    end
end

TheHua.CheckGeneralSpiderManItem = function(_, player) -- 不应在客机出现
    if not player then
        return
    end

    if TheHua:CheckIsRealSpiderMan(player) then
        return
    end

    if player.components.inventory == nil then
        return
    end

    for k, v in pairs(player.components.inventory.itemslots) do
        if v and v:HasTag("hua_fake_spider") then
            return v
        end
    end
end

TheHua.PickOneFromArray = function(_, arr)
    return arr[math.random(#arr)]
end

TheHua.MakeWaterCrackablePhysics = function(_, inst, rad, height, restitution)
    inst:AddTag("blocker")
    local phys = inst.entity:AddPhysics()
    phys:SetMass(0) --Bullet wants 0 mass for static objects
    phys:SetCollisionGroup(COLLISION.OBSTACLES)
    phys:ClearCollisionMask()
    --phys:CollidesWith(COLLISION.ITEMS)
    --phys:CollidesWith(COLLISION.CHARACTERS)
    --phys:CollidesWith(COLLISION.GIANTS)
    phys:CollidesWith(COLLISION.OBSTACLES)
    phys:SetCapsule(rad, height)

    inst:AddComponent("waterphysics")
    inst.components.waterphysics.restitution = restitution

    return phys
end

local aaa = {
    "hua_internet_node",
    "hua_internet_node_item",
    "hua_internet_link",
    "hua_internet_builder",
    "hua_fake_spider_shoe",
}

for _,v in ipairs(aaa) do 
    table.insert(PrefabFiles,v)
end
RegisterInventoryItemAtlas("images/inventoryimages/hua_internet_node_item.xml", "hua_internet_node_item.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hua_internet_node_sea_item.xml", "hua_internet_node_sea_item.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hua_fake_spider_shoe.xml", "hua_fake_spider_shoe.tex")

AddPrefabPostInit("world", function(inst)
    inst:AddComponent("hua_internet_topology")
end)

TUNING.HUA_INTERNET_BUILD_DIST = 40 -- 两点之间可建连线的最大距离
TUNING.HUA_INTERNET_USE_DIST = 1--3 -- 与线的距离达到这个值以下方可传送
TUNING.HUA_INTERNET_COST_UNIT_SILK = 2 -- 基于距离的造价，每格地皮（距离 4）需要的蜘蛛网数量
TUNING.HUA_INTERNET_COST_MIN_SILK = 2 -- 最小基础造价（总不能是 0 吧！）
TUNING.HUA_INTERNET_TRAVEL_SPEED = 20 -- 御丝飞行速度

TUNING.HUA_FAKE_SPIDER_SHOE_USES = 400
TUNING.HUA_FAKE_SPIDER_SHOE_SEW_AWARD = 100 -- 修一次回复耐久
TUNING.HUA_FAKE_SPIDER_SHOE_WALKING_COST = 0.25 -- 我也不知道怎么评价它，嫌快就改小就行，0 也行！
-- 它的判定似乎跑得越快判定得越频繁。也就是说走过相同的路程消耗的耐久大致相等？

-- 摆放距离
DEPLOYSPACING.HUA = 6 -- 这玩意儿以后可能会修改，更不排除完全重写整个 net
DEPLOYSPACING_RADIUS[DEPLOYSPACING.HUA] = 8

local DOER_TO_NODE = {}
local function SetDoerToNode(doer, node)
    DOER_TO_NODE[doer] = node

    if doer.player_classified and doer.player_classified.hua_internet_chosen_node then
        doer.player_classified.hua_internet_chosen_node:set(node)
    end
end
local function GetDoerToNode(doer) -- 强迫症
    return DOER_TO_NODE[doer]
end

AddGlobalClassPostConstruct("bufferedaction", "BufferedAction", function(BA)
    local old_BA_BufferedAction = BA.Do
    BA.Do = function(self)
        if self.doer and (self.action == nil or self.action.id ~= "HUA_INTERNET_BUILD") then
            SetDoerToNode(self.doer)
        end

        return old_BA_BufferedAction(self)
    end
end)

AddAction("HUA_INTERNET_BUILD", STRINGS.MYTH_INTERNET_LEFT, function(act)
    if act.target ~= nil and TheHua:CheckIsMadamWeb(act.doer) then
        local chosen_node = GetDoerToNode(act.doer)
        if chosen_node == nil then
            SetDoerToNode(act.doer, act.target)
            return true
        elseif chosen_node ~= act.target then
            TheWorld.components.hua_internet_topology:BuildLink(act.doer, chosen_node, act.target)
        end

        SetDoerToNode(act.doer)
        return true
    end
end).distance = TUNING.HUA_INTERNET_BUILD_DIST + 4 -- 可以修改

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

AddAction("HUA_INTERNET_USE", STRINGS.MYTH_INTERNET_RIGHT, function(act)
    if act.target ~= nil and TheHua:CheckIsGeneralSpiderMan(act.doer) and not IsFlying(act.doer) then
        TheWorld.components.hua_internet_topology:UseInternet(act.doer, act.target)
        return true
    end
end).distance = TUNING.HUA_INTERNET_BUILD_DIST + 4 -- 不建议修改

AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.HUA_INTERNET_BUILD, "doshortaction"))
AddStategraphActionHandler("wilson_client", ActionHandler(ACTIONS.HUA_INTERNET_BUILD, "doshortaction"))

AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.HUA_INTERNET_USE, "doshortaction"))
AddStategraphActionHandler("wilson_client", ActionHandler(ACTIONS.HUA_INTERNET_USE, "doshortaction"))

local upvaluehelper = require "components/myth_upvaluehelper"
local COMPONENT_ACTIONS = upvaluehelper.Get(EntityScript.CollectActions,"COMPONENT_ACTIONS")
local old_SCENE_inspectable = COMPONENT_ACTIONS.SCENE.inspectable
COMPONENT_ACTIONS.SCENE.inspectable = function(inst, doer, actions, right)
    old_SCENE_inspectable(inst, doer, actions, right)
    if inst:HasTag("hua_internet_node") then
        if right and TheHua:CheckIsGeneralSpiderMan(doer) then
            table.insert(actions, ACTIONS.HUA_INTERNET_USE)
        elseif TheHua:CheckIsMadamWeb(doer) then
            table.insert(actions, ACTIONS.HUA_INTERNET_BUILD)
        end
        return
    end
end

--[[local function ClientGoToTravelling(inst)
    local player = inst._parent
    if player and player.sg then
        player.sg:GoToState("hua_internet_destination_travelling")
    end
end]]

AddPrefabPostInit("player_classified", function(inst)
    inst.hua_internet_chosen_node = net_entity(inst.GUID, "hua_internet_chosen_node")
    --[[inst.hua_internet_goto_travelling = net_uint(inst.GUID, "hua_internet_goto_travelling", "hua_internet_goto_travelling_dirty")

    if TheNet:GetIsClient() then
        inst:ListenForEvent("hua_internet_goto_travelling_dirty", ClientGoToTravelling)
    end]]
end)

-- 绝招了，暴力
local INTERRUPT_CONTROLS = {
    CONTROL_MOVE_UP,
    CONTROL_MOVE_DOWN,
    CONTROL_MOVE_LEFT,
    CONTROL_MOVE_RIGHT,
    CONTROL_CONTROLLER_ATTACK,
}
local PlayerHud = require("screens/playerhud")
local old_PlayerHud_OnUpdate = PlayerHud.OnUpdate
function PlayerHud:OnUpdate(dt)
    for _, v in pairs(INTERRUPT_CONTROLS) do
        if TheInput:IsControlPressed(v) then
            SendModRPCToServer(MOD_RPC.hua_internet.interrupt_travelling)
        end
    end

    if TheSim:GetMouseButtonState(MOUSEBUTTON_LEFT) then
        SendModRPCToServer(MOD_RPC.hua_internet.interrupt_travelling)
    end

    return old_PlayerHud_OnUpdate(self, dt)
end

-- NT Klei
-- 【【【【【【【【【【【【【如果需求的蜘蛛网可能超过 100 个，下处和 builder 要适当增加】】】】】】】】】】】】
for i = 1, 100 do
    if i >= 81 then
        CONSTRUCTION_PLANS["hua_internet_builder_" .. i] = { Ingredient("silk", 40), Ingredient("silk", 40), Ingredient("silk", i - 80) }
    elseif i >= 41 then
        CONSTRUCTION_PLANS["hua_internet_builder_" .. i] = { Ingredient("silk", 40), Ingredient("silk", i - 40) }
    else
        CONSTRUCTION_PLANS["hua_internet_builder_" .. i] = { Ingredient("silk", i) }
    end
end

AddStategraphEvent("wilson", EventHandler("hua_internet_destination_go", function(inst, data)
    if not inst.sg:HasStateTag("dead") then
        inst.sg:GoToState("hua_internet_destination_travelling", data)
    end
end))

local function SetTravellingPhysics(inst, travelling)
    if travelling then
        if inst.components.drownable and inst.components.drownable.enabled ~= false then
            inst.components.drownable.enabled = false
        end
        inst.Physics:ClearCollisionMask()
        inst.Physics:CollidesWith(COLLISION.GROUND)
        inst.Physics:Teleport(inst.Transform:GetWorldPosition())
    else
        if inst.components.drownable then
            inst.components.drownable.enabled = true
        end
        if not inst:HasTag("playerghost") then
            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
            inst.Physics:Teleport(inst.Transform:GetWorldPosition())
        end
    end
end

AddStategraphState("wilson", State({
    name = "hua_internet_destination_travelling",
    tags = {"hua_internet_destination_travelling", "busy", "canrotate"},
    onenter = function(inst, data)
        inst.components.locomotor:Stop()
        inst.components.locomotor:Clear()
        inst:ClearBufferedAction()

        SetTravellingPhysics(inst, true)

        inst.AnimState:PlayAnimation("myth_surf_loop", true)

        inst.hua_internet_destination = data.node
        inst.hua_internet_last_destination = data.last_node

        inst.hua_internet_interrupt_travelling = nil
        inst:AddTag("hua_internet_destination_travelling")

        --[[if inst.player_classified then
            local v = inst.player_classified.hua_internet_goto_travelling:value()
            if v > 1000 then
                v = 10
            end
            inst.player_classified.hua_internet_goto_travelling:set(v + 1)
        end]]
    end,

    onupdate = function(inst, dt)
        if inst.components.locomotor:WantsToMoveForward() or inst.hua_internet_interrupt_travelling then
            inst.sg:GoToState("idle")
            return
        end

        if inst.hua_internet_destination and inst.hua_internet_destination:IsValid() then
            local x, _, z = inst.hua_internet_destination.Transform:GetWorldPosition()
            inst:ForceFacePoint(x, 0, z)
            inst.Physics:SetMotorVel(TUNING.HUA_INTERNET_TRAVEL_SPEED, 0, 0)

            if inst:GetDistanceSqToInst(inst.hua_internet_destination) < TUNING.HUA_INTERNET_TRAVEL_SPEED * TUNING.HUA_INTERNET_TRAVEL_SPEED * dt * dt then
                --inst.Physics:Teleport(x, 0, z) -- 也许没必要非得飞到中心
                --inst.sg:GoToState("idle")
                TheWorld.components.hua_internet_topology:TravelToNextNode(inst, inst.hua_internet_last_destination, inst.hua_internet_destination)
            end
        else
            inst.sg:GoToState("idle")
        end
    end,

    onexit = function(inst)
        inst.hua_internet_interrupt_travelling = nil
        inst:RemoveTag("hua_internet_destination_travelling")
        inst.Physics:SetMotorVel(0, 0, 0)
        inst.components.locomotor:Stop()
        inst.components.locomotor:Clear()
        SetTravellingPhysics(inst)

        inst.hua_internet_last_destination = nil
        inst.hua_internet_destination = nil
    end,
}))

AddStategraphState("wilson", State({
    name = "hua_internet_waiting",
    tags = {"hua_internet_waiting", "working", "canrotate"},
    onenter = function(inst, data)
        inst.components.locomotor:Stop()
        inst.components.locomotor:Clear()
        inst:ClearBufferedAction()

        inst.AnimState:PlayAnimation("idle_loop", true)
    end,
}))

--[[AddStategraphState("wilson_client", State({
    name = "hua_internet_destination_travelling",
    tags = {"hua_internet_destination_travelling", "idle", "canrotate"},
    onenter = function(inst, data)
        inst.components.locomotor:Stop()
        inst.components.locomotor:Clear()
        inst:ClearBufferedAction()

        inst.AnimState:PlayAnimation("idle_wardrobe1_pre")
        inst.AnimState:PushAnimation("idle_wardrobe1_loop", true)
    end,

    onupdate = function(inst)
        if inst.components.locomotor:WantsToMoveForward() then
            SendModRPCToServer(MOD_RPC.hua_internet.interrupt_travelling) -- 实在是没有任何办法，开启了延迟预测的情况下基本无法打断
        end
    end,
}))]]

--[[local PlayerController = require("components/playercontroller")
local old_CanLocomote = PlayerController.CanLocomote
function PlayerController:CanLocomote(...)
    if self.inst:HasTag("hua_internet_destination_travelling") and self.inst.sg then -- 开延迟预测也可以打断
        return true
    end

    return old_CanLocomote(self, ...)
end]]

AddModRPCHandler("hua_internet", "interrupt_travelling", function(player)
    player.hua_internet_interrupt_travelling = true
end)

--[[
local EMPTY_TABLE = {}
local ConstructionSite = require("components/constructionsite")
local old_OnConstruct = ConstructionSite.OnConstruct
function ConstructionSite:OnConstruct(doer, items)
    if self.must_full then -- 傻逼克雷
        self.builder = nil
        local x, y, z = self.inst.Transform:GetWorldPosition()
        for i, v in ipairs(CONSTRUCTION_PLANS[self.inst.prefab] or EMPTY_TABLE) do
            local item = items[i]
            local prefab = item.prefab
            local count = item.components.stackable and item.components.stackable:StackSize() or 1
            if v.type ~= prefab or v.amount > count then -- 不符合要求，全部倒出来

                if doer.components.talker ~= nil then
                    doer.components.talker:Say(STRINGS.HUA_INTERNET_BUILDER.FAILED_MATERIAL)
                end

                for _, it in ipairs(items) do
                    it.components.inventoryitem:RemoveFromOwner(true)
                    it.components.inventoryitem:DoDropPhysics(x, y, z, true)
                end

                return
            end
        end
    end

    return old_OnConstruct(self, doer, items)
end]]

-- 附近不允许建造的
--[[local BANNED_BUILDING_NEARBY = {"hua_no_build_boat"}
local function IsBannedBuildingNearHuaBoat(x, y, z)
    return #TheSim:FindEntities(x, 0, z, 1.7, BANNED_BUILDING_NEARBY) > 0
end

local function ModifyFunc(name)
    local map = getmetatable(TheWorld.Map).__index
    local old = map[name]
    map[name] = function(self, pt, ...)
        if IsBannedBuildingNearHuaBoat(pt:Get()) then
            return false
        end

        return old(self, pt, ...)
    end
end

local REWRITTEN_FUNCS = {
    "CanDeployAtPoint",
    "CanDeployPlantAtPoint",
    "CanDeployWallAtPoint",
    "CanDeployAtPointInWater",
    "CanDeployMastAtPoint",
    "CanDeployRecipeAtPoint",
}
AddPrefabPostInit("world", function(inst)
    for _, v in pairs(REWRITTEN_FUNCS) do
        ModifyFunc(v)
    end
end)]]

-- 扫把变色
AddPrefabPostInit("reskin_tool", function(inst)
    if not inst.components.spellcaster then
        return
    end
    local old_spell = inst.components.spellcaster.spell
    local old_can_cast_fn = inst.components.spellcaster.can_cast_fn

    local function spellCB(tool, target, pos)
        if target and target.components.hua_dye_target then
            target.components.hua_dye_target:ChangeDye()
            return
        end

        return old_spell(tool, target, pos)
    end

    local function can_cast_fn(doer, target, pos)
        if target and target.components.hua_dye_target then
            return true
        end

        return old_can_cast_fn(doer, target, pos)
    end

    inst.components.spellcaster:SetSpellFn(spellCB)
    inst.components.spellcaster:SetCanCastFn(can_cast_fn)
end)

-- 鞋子加速效果
AddComponentPostInit("locomotor", function(self)
    local old = self.UpdateGroundSpeedMultiplier
    function self:UpdateGroundSpeedMultiplier()
        local x, y, z = self.inst.Transform:GetWorldPosition()
        local oncreep = TheWorld.GroundCreep:OnCreep(x, y, z)
        if oncreep and TheHua:CheckIsGeneralSpiderMan(self.inst) and not (self.inst.components.rider and self.inst.components.rider:IsRiding()) then
            self.groundspeedmultiplier = self.fastmultiplier
            return
        end
        return old(self)
    end
end)

--[[ 影响蛛网行走速度除了 FasterOnCreep 之外还有 triggerscreep，因此不得不重写
local LocoMotor = require("components/locomotor")
function LocoMotor:UpdateGroundSpeedMultiplier()
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local oncreep = TheWorld.GroundCreep:OnCreep(x, y, z)

    ---------------------
    local spider_man = TheHua:CheckIsGeneralSpiderMan(self.inst)
    local item = TheHua:CheckGeneralSpiderManItem(self.inst)

    if oncreep and (not spider_man and self.triggerscreep) then
    ---------------------

        -- if this ever needs to happen when self.enablegroundspeedmultiplier is set, need to move the check for self.enablegroundspeedmultiplier above
        if not self.wasoncreep then
            for _, v in ipairs(TheWorld.GroundCreep:GetTriggeredCreepSpawners(x, y, z)) do
                v:PushEvent("creepactivate", { target = self.inst })
            end
            self.wasoncreep = true
        end
        self.groundspeedmultiplier = self.slowmultiplier
    else
        self.wasoncreep = false

        local current_ground_tile = TheWorld.Map:GetTileAtPoint(x, 0, z)
        self.groundspeedmultiplier = (self:IsFasterOnGroundTile(current_ground_tile) or
                (self:FasterOnRoad() and ((RoadManager ~= nil and RoadManager:IsOnRoad(x, 0, z)) or current_ground_tile == GROUND.ROAD)) or
                (self:FasterOnCreep() and oncreep))
                and self.fastmultiplier
                or 1
    end
end]]