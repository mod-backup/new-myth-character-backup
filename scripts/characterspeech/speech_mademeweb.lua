return
{
	ACTIONFAIL =
	{
        APPRAISE =
        {
            NOTNOW = "人家忙着呢。",		--暂无注释
        },
        REPAIR =
        {
            WRONGPIECE = "那不太对吧。",		--化石骨架拼接错误
        },
        BUILD =
        {
            MOUNTED = "现在，可放不下。",		--建造失败（骑乘状态）
            HASPET = "跟着妾身你可活不了多久。",		--建造失败（已经有一个宠物了）
        },
		SHAVE =
		{
			AWAKEBEEFALO = "妾身柔弱，不如你来吧。",		--给醒着的牛刮毛
			GENERIC = "妾身不能给那个剃毛！",		--刮牛毛失败
			NOBITS = "些许琐碎可不值得让妾身出手！",		--给没毛的牛刮毛
            REFUSE = "only_used_by_woodie",		--暂无注释
            SOMEONEELSESBEEFALO = "怎可剽窃！",		--暂无注释
		},
		STORE =
		{
			GENERIC = "这箱匣装不下了",		--存放东西失败
			NOTALLOWED = "这东西不能装进那里。",		--存放东西--不被允许
			INUSE = "妾身还可以再等等",		--别人正在用箱子
            NOTMASTERCHEF = "妾身的厨艺还不够。",		--暂无注释
		},
        CONSTRUCT =
        {
            INUSE = "有人抢先了。",		--建筑正在使用
            NOTALLOWED = "不匹配。",		--建筑不允许使用
            EMPTY = "妾身需要一些建造材料。",		--建筑空了
            MISMATCH = "哎呀，计划错误。",		--升级套件错误（目前用不到）
        },
		RUMMAGE =
		{
			GENERIC = "妾身不能那么做。",		--打开箱子失败
			INUSE = "他们在垃圾里埋头寻找。",		--打开箱子 正在使用
            NOTMASTERCHEF = "妾身的厨艺还不够。",		--暂无注释
		},
		UNLOCK =
        {
        	WRONGKEY = "妾身做不到。",		--暂无注释
        },
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "哎呀！那不对。",		--使用克劳斯钥匙
        	KLAUS = "妾身现在无暇接应！！",		--克劳斯
        },
		ACTIVATE =
		{
			LOCKED_GATE = "大门锁上了。",		--远古钥匙
            HOSTBUSY = "他现在好像有点忙。",		--暂无注释
            CARNIVAL_HOST_HERE = "他就在附近。",		--暂无注释
            NOCARNIVAL = "鸟儿看起来都飞走了。"		--暂无注释
		},
        COOK =
        {
            GENERIC = "妾身现在做不了饭。",		--做饭失败
            INUSE = "好像你跟妾身有一样的想法。",		--做饭失败-别人在用锅
            TOOFAR = "太远了！",		--做饭失败-太远
        },
        START_CARRAT_RACE =
        {
            NO_RACERS = "妾身肯定是遗漏了些什么。",		--暂无注释
        },
		DISMANTLE =
		{
			COOKING = "煮着东西呢，不能这样。",		--暂无注释
			INUSE = "妾身可以再等等。",		--暂无注释
			NOTEMPTY = "妾身再想想办法吧。",		--暂无注释
        },
        FISH_OCEAN =
		{
			TOODEEP = "不是海钓竿，不适合深海海钓。",		--暂无注释
		},
        OCEAN_FISHING_POND =
		{
			WRONGGEAR = "这根钓竿不适合在池塘钓鱼。",		--暂无注释
		},
        READ =
        {
            GENERIC = "only_used_by_wickerbottom",		--暂无注释
            NOBIRDS = "only_used_by_wickerbottom"		--暂无注释
        },
        GIVE =
        {
            GENERIC = "你怎忍拒妾身的心意...",		--给予失败
            DEAD = "也许妾身该留着这个。",		--给予 -目标死亡
            SLEEPING = "不省人事，无心牵挂。",		--给予--目标睡觉
            BUSY = "容妾身过后再试。",		--给予--目标正忙
            ABIGAILHEART = "你不该再牵挂她。",		--给阿比盖尔 救赎之心
            GHOSTHEART = "或许，该把你送走了。",		--给鬼魂 救赎之心
            NOTGEM = "它需要一些闪闪发光的东西。",		--给的不是宝石
            WRONGGEM = "这个宝石在这里不起作用。",		--给错了宝石
            NOTSTAFF = "它的形状有些不对。",		--给月石祭坛的物品不是法杖
            MUSHROOMFARM_NEEDSSHROOM = "一颗蘑菇可能会有更多的用处。",		--蘑菇农场需要蘑菇
            MUSHROOMFARM_NEEDSLOG = "一块活木头可能会有更多的用处。",		--蘑菇农场需要活木
            MUSHROOMFARM_NOMOONALLOWED = "这些蘑菇好像不愿意被种下去！",		--暂无注释
            SLOTFULL = "妾身已经把一些东西放在那里了。",		--已经放了材料后，再给雕像桌子再给一个材料
            DUPLICATE = "妾身已经知道那个了。",		--给雕像桌子已经学习过的图纸
            NOTSCULPTABLE = "妾身觉得这不太对。",		--给陶艺圆盘的东西不对
            NOTATRIUMKEY = "它的形状有些不对。",		--中庭钥匙不对
            CANTSHADOWREVIVE = "它不会复活。",		--中庭仍在CD
            NOMOON = "妾身需要看到月亮才行。",		--洞穴里建造月石科技
			PIGKINGGAME_MESSY = "妾身得先把周围清理干净。",		--猪王旁边有建筑，不能开始抢元宝
			PIGKINGGAME_DANGER = "现在这样太危险了。",		--危险，不能开始抢元宝
			PIGKINGGAME_TOOLATE = "现在已经太晚了。",		--不是白天，不能开始抢元宝
			CARNIVALGAME_INVALID_ITEM = "妾身需要去买一些东西。",		--暂无注释
			CARNIVALGAME_ALREADY_PLAYING = "已经有游戏在进行中了。",		--暂无注释
            SPIDERNOHAT = "已经...满了...",		--暂无注释
        },
        GIVETOPLAYER =
        {
            FULL = "你的口袋太满了！",		--给玩家一个东西 -但是背包满了
            DEAD = "生不带来死不带去。",		--给死亡的玩家一个东西
            SLEEPING = "不省人事，无心牵挂。",		--给睡觉的玩家一个东西
            BUSY = "容妾身过后再试。",		--给忙碌的玩家一个东西
        },
        GIVEALLTOPLAYER =
        {
            FULL = "你的口袋太满了！",		--给人一组东西 但是背包满了
            DEAD = "生不带来死不带去。",		--给于死去的玩家一组物品
            SLEEPING = "不省人事了。",		--给于正在睡觉的玩家一组物品
            BUSY = "容妾身过后再试。",		--给于正在忙碌的玩家一组物品
        },
        WRITE =
        {
            GENERIC = "妾身觉得它现在这样挺好。",		--鞋子失败
            INUSE = "你若无情那便休。",		--写字 正在使用中
        },
        DRAW =
        {
            NOIMAGE = "如果妾身面前有这东西...",		--画图缺乏图像
        },
        CHANGEIN =
        {
            GENERIC = "妾身现在不想换。",		--换装失败 
            BURNING = "妾身的薄纱可经不起这样！",		--换装失败-着火了
            INUSE = "妾身不想与他人共用。",		--衣橱有人占用
            NOTENOUGHHAIR = "皮毛不够用来装扮。",		--暂无注释
            NOOCCUPANT = "得在上面栓点东西。",		--暂无注释
        },
        ATTUNE =
        {
            NOHEALTH = "脱壳之术。",		--制造肉雕像血量不足
        },
        MOUNT =
        {
            TARGETINCOMBAT = "妾身还没笨到去惹一头狂牛！",		--骑乘，牛正在战斗
            INUSE = "有人抢先骑上去了！",		--骑乘（牛被占据）
        },
        SADDLE =
        {
            TARGETINCOMBAT = "妾身身子弱，可受不了。",		--给战斗状态的牛上鞍
        },
        TEACH =
        {
            KNOWN = "妾身已经知道那个了。",		--学习已经知道的蓝图
            CANTLEARN = "妾身无法学习那个。",		--学习无法学习的蓝图
            WRONGWORLD = "这地图在这用不了。",		--学习另外一个世界的地图
			MESSAGEBOTTLEMANAGER_NOT_FOUND = "光线太暗，妾身什么都看不到！",--Likely trying to read messagebottle treasure map in caves		--暂无注释
        },
        WRAPBUNDLE =
        {
            EMPTY = "妾身需要有东西来打包。",		--打包纸是空的
        },
        PICKUP =
        {
			INUSE = "妾身可以再等等。",		--捡起已经打开的容器
            NOTMINE_SPIDER = "only_used_by_webber",		--暂无注释
            NOTMINE_YOTC =
            {
                "你可不是妾身的小萝卜。",		--暂无注释
                "被它咬了一口！",		--暂无注释
            },
			NO_HEAVY_LIFTING = "only_used_by_wanda",		--暂无注释
        },
        SLAUGHTER =
        {
            TOOFAR = "它逃走了。",		--屠杀？？ 因为太远而失败
        },
        REPLATE =
        {
            MISMATCH = "它需要另一种碟子。",		--暴食-换盘子换错了 比如用碗换碟子
            SAMEDISH = "妾身只需要用一个碟子。",		--暴食-换盘子已经换了
        },
        SAIL =
        {
        	REPAIR = "它不需要修理。",		--暂无注释
        },
        ROW_FAIL =
        {
            BAD_TIMING0 = "太快了！",		--暂无注释
            BAD_TIMING1 = "时机不对！",		--暂无注释
            BAD_TIMING2 = "别再犯了！",		--暂无注释
        },
        LOWER_SAIL_FAIL =
        {
            "哎呦！",		--暂无注释
            "根本慢不下来！",		--暂无注释
            "失败只是一时的！",		--暂无注释
        },
        BATHBOMB =
        {
            GLASSED = "不行，这已经不能泡了。",		--暂无注释
            ALREADY_BOMBED = "别浪费沐浴球。",		--暂无注释
        },
		GIVE_TACKLESKETCH =
		{
			DUPLICATE = "不对，不对。",		--暂无注释
		},
		COMPARE_WEIGHABLE =
		{
            FISH_TOO_SMALL = "这家伙太小了。",		--暂无注释
            OVERSIZEDVEGGIES_TOO_SMALL = "还不够沉。",		--暂无注释
		},
        BEGIN_QUEST =
        {
            ONEGHOST = "only_used_by_wendy",		--暂无注释
        },
		TELLSTORY =
		{
			GENERIC = "only_used_by_walter",		--暂无注释
			NOT_NIGHT = "only_used_by_walter",		--暂无注释
			NO_FIRE = "only_used_by_walter",		--暂无注释
		},
        SING_FAIL =
        {
            SAMESONG = "only_used_by_wathgrithr",		--暂无注释
        },
        PLANTREGISTRY_RESEARCH_FAIL =
        {
            GENERIC = "不剩什么需要学的了。",		--暂无注释
            FERTILIZER = "妾身不想知道更多细节了。",		--暂无注释
        },
        FILL_OCEAN =
        {
            UNSUITABLE_FOR_PLANTS = "海水可不能用来浇花。",		--暂无注释
        },
        POUR_WATER =
        {
            OUT_OF_WATER = "糟糕，没水了。",		--暂无注释
        },
        POUR_WATER_GROUNDTILE =
        {
            OUT_OF_WATER = "妾身的浇水壶干了。",		--暂无注释
        },
        USEITEMON =
        {
            BEEF_BELL_INVALID_TARGET = "妾身不可能做到！",		--暂无注释
            BEEF_BELL_ALREADY_USED = "这头弗娄牛已经属于别人了。",		--暂无注释
            BEEF_BELL_HAS_BEEF_ALREADY = "妾身不需要那麽多的蠢牛。",		--暂无注释
        },
        HITCHUP =
        {
            NEEDBEEF = "有铃铛就能奴役他们。",		--暂无注释
            NEEDBEEF_CLOSER = "妾身的坐骑离妾身太远了。",		--暂无注释
            BEEF_HITCHED = "妾身的坐骑已经拴好了。",		--暂无注释
            INMOOD = "妾身的新坐骑还不错。",		--暂无注释
        },
        MARK =
        {
            ALREADY_MARKED = "妾身已经选好了。",		--暂无注释
            NOT_PARTICIPANT = "妾身没有手下参赛。",		--暂无注释
        },
        YOTB_STARTCONTEST =
        {
            DOESNTWORK = "看来这里并不支持艺术。",		--暂无注释
            ALREADYACTIVE = "他可能忙着比别的比赛呢。",		--暂无注释
        },
        YOTB_UNLOCKSKIN =
        {
            ALREADYKNOWN = "这个妾身已经知晓了！",		--暂无注释
        },
        CARNIVALGAME_FEED =
        {
            TOO_LATE = "妾身得快一点！",		--暂无注释
        },
        HERD_FOLLOWERS =
        {
            WEBBERONLY = "你们安敢欺负妾身。",		--暂无注释
        },
        BEDAZZLE =
        {
            BURNING = STRINGS.CHARACTERS.WEBBER.ACTIONFAIL.BEDAZZLE.BURNING,		--暂无注释
            BURNT = STRINGS.CHARACTERS.WEBBER.ACTIONFAIL.BEDAZZLE.BURNT,		--暂无注释
            FROZEN = STRINGS.CHARACTERS.WEBBER.ACTIONFAIL.BEDAZZLE.FROZEN,		--暂无注释
            ALREADY_BEDAZZLED = STRINGS.CHARACTERS.WEBBER.ACTIONFAIL.BEDAZZLE.ALREADY_BEDAZZLED,		--暂无注释
        },
        UPGRADE = 
        {
            BEDAZZLED = STRINGS.CHARACTERS.WEBBER.ACTIONFAIL.UPGRADE.BEDAZZLED,		--暂无注释
        },
		CAST_POCKETWATCH = 
		{
			GENERIC = "only_used_by_wanda",		--暂无注释
			REVIVE_FAILED = "only_used_by_wanda",		--暂无注释
			WARP_NO_POINTS_LEFT = "only_used_by_wanda",		--暂无注释
			SHARD_UNAVAILABLE = "only_used_by_wanda",		--暂无注释
		},
        DISMANTLE_POCKETWATCH =
        {
            ONCOOLDOWN = "only_used_by_wanda",		--暂无注释
        },
    },
	ACTIONFAIL_GENERIC = "妾身做不到呀。",		--动作失败
	ANNOUNCE_BOAT_LEAK = "水越来越多了。",		--暂无注释
	ANNOUNCE_BOAT_SINK = "妾身浑身上下都要湿了！",		--暂无注释
	ANNOUNCE_DIG_DISEASE_WARNING = "它看起来已经好多了。", --removed		--挖起生病的植物
	ANNOUNCE_PICK_DISEASE_WARNING = "嗯啊，本来就是那味道吗？", --removed		--（植物生病）
    ANNOUNCE_MOUNT_LOWHEALTH = "这长毛畜生似乎受伤了。",		--牛血量过低
    ANNOUNCE_TOOMANYBIRDS = "only_used_by_waxwell_and_wicker",		--暂无注释
    ANNOUNCE_WAYTOOMANYBIRDS = "only_used_by_waxwell_and_wicker",		--暂无注释
    ANNOUNCE_NORMALTOMIGHTY = "only_used_by_wolfang",		--暂无注释
    ANNOUNCE_NORMALTOWIMPY = "only_used_by_wolfang",		--暂无注释
    ANNOUNCE_WIMPYTONORMAL = "only_used_by_wolfang",		--暂无注释
    ANNOUNCE_MIGHTYTONORMAL = "only_used_by_wolfang",		--暂无注释
	ANNOUNCE_BEES = "蜜蜂！！！！",		--戴养蜂帽被蜜蜂蛰
	ANNOUNCE_BOOMERANG = "噢！妾身应该尽量接住它！",		--回旋镖
	ANNOUNCE_CHARLIE = "不知是哪位？何不出来一见？",		--查理即将攻击
	ANNOUNCE_CHARLIE_ATTACK = "大胆！我看你是敬酒不吃吃罚酒。",		--被查理攻击
	ANNOUNCE_CHARLIE_MISSED = "only_used_by_winona", --winona specific		--暂无注释
	ANNOUNCE_COLD = "好冷！",		--过冷
	ANNOUNCE_HOT = "热得要烧起来了！",		--过热
	ANNOUNCE_DEERCLOPS = "便欲经营网众虫。",		--boss来袭
	ANNOUNCE_CAVEIN = "天花板在晃动！",		--要地震了？？？
	ANNOUNCE_ANTLION_SINKHOLE =
	{
		"地面在晃动！",		--蚁狮地震
		"地震！",		--蚁狮地震
		"可怕的震动！",		--蚁狮地震
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "待本座恢复，你就等死吧。",		--向蚁狮致敬
        "妾身能屈能伸，不在乎这些。",		--给蚁狮上供
        "你先给本座安静...",		--给蚁狮上供
	},
	ANNOUNCE_SACREDCHEST_YES = "妾身认为妾身值得。",		--远古宝箱物品正确给出蓝图
	ANNOUNCE_SACREDCHEST_NO = "它不喜欢那个。",		--远古宝箱
    ANNOUNCE_DUSK = "桑榆莫晚，霞光满天。",		--进入黄昏
    ANNOUNCE_CHARGE = "only_used_by_wx78",		--暂无注释
	ANNOUNCE_DISCHARGE = "only_used_by_wx78",		--暂无注释
	ANNOUNCE_EAT =
	{
		GENERIC = "也许来点虫子夹心会更好！",		--吃东西
		PAINFUL = "虽然味道有点怪，但并不妨碍填饱肚子。",		--吃怪物肉
		SPOILED = "呸！妾身还不至于吃这些个。",		--吃腐烂食物
		STALE = "下次莫要再给妾身吃这个了。",		--吃黄色食物
		INVALID = "妾身不吃那个！",		--拒吃
        YUCKY = "莫要来恶心妾身！",		--吃红色食物
		COOKED = "only_used_by_warly",		--暂无注释
		DRIED = "only_used_by_warly",		--暂无注释
        PREPARED = "only_used_by_warly",		--暂无注释
        RAW = "only_used_by_warly",		--暂无注释
		SAME_OLD_1 = "only_used_by_warly",		--暂无注释
		SAME_OLD_2 = "only_used_by_warly",		--暂无注释
		SAME_OLD_3 = "only_used_by_warly",		--暂无注释
		SAME_OLD_4 = "only_used_by_warly",		--暂无注释
        SAME_OLD_5 = "only_used_by_warly",		--暂无注释
		TASTY = "only_used_by_warly",		--暂无注释
    },
    ANNOUNCE_ENCUMBERED =
    {
        "气喘...吁吁...",		--搬运雕像、可疑的大理石
        "妾身应该...找些帮手...",		--搬运雕像、可疑的大理石
        "抬不起来...谁来帮帮妾身...",		--搬运雕像、可疑的大理石
        "这不是...妾身该做的...工作...",		--搬运雕像、可疑的大理石
        "为了...！",		--搬运雕像、可疑的大理石
        "这东西...正把妾身头发弄得一团糟吗？",		--搬运雕像、可疑的大理石
        "哼...！",		--搬运雕像、可疑的大理石
        "气喘...吁吁...",		--搬运雕像、可疑的大理石
        "这是最糟糕的...",		--搬运雕像、可疑的大理石
    },
    ANNOUNCE_ATRIUM_DESTABILIZING =
    {
		"妾身该走了！",		--中庭击杀boss后即将刷新
		"那是什么？！",		--中庭震动
		"这里不安全。",		--中庭击杀boss后即将刷新
	},
    ANNOUNCE_RUINS_RESET = "所有的怪兽都回来了！",		--地下重置
    ANNOUNCE_SNARED = "尖锐！尖锐的骨头！！",		--远古嘤嘤怪的骨笼
    ANNOUNCE_SNARED_IVY = "啊！好疼！",		--暂无注释
    ANNOUNCE_REPELLED = "它有保护！",		--嘤嘤怪保护状态
	ANNOUNCE_ENTER_DARK = "幽夜难！猛虎伺其中。",		--进入黑暗
	ANNOUNCE_ENTER_LIGHT = "斜阳欲坠，仍留余痕。",		--进入光源
	ANNOUNCE_HOUNDS = "你们这群畜生倒是大胆。",		--猎犬将至
	ANNOUNCE_WORMS = "你感觉到了吗？",		--蠕虫袭击
	ANNOUNCE_HUNGRY = "百虫为食，腹常苦饥！",		--饥饿
	ANNOUNCE_HUNT_BEAST_NEARBY = "新鲜的足迹，那只野兽肯定就在附近。",		--即将找到野兽
	ANNOUNCE_HUNT_LOST_TRAIL = "野兽的足迹到这里就没了。",		--猎物（大象脚印丢失）
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "这种湿土留不下脚印。",		--大猎物丢失踪迹
	ANNOUNCE_INV_FULL = "妾身拿不了更多的东西了！",		--身上的物品满了
	ANNOUNCE_KNOCKEDOUT = "妾身的头好晕！",		--被催眠
    ANNOUNCE_NOWARDROBEONFIRE = "它着火了，妾身无法靠近！",		--橱柜着火
    ANNOUNCE_NODANGERGIFT = "怪物在四周，现在可不是时候！",		--周围有危险的情况下打开礼物
    ANNOUNCE_NOMOUNTEDGIFT = "妾身得先从坐骑身上下来。",		--骑牛的时候打开礼物
	ANNOUNCE_NODANGERSLEEP = "一觉是要睡到永眠！",		--危险，不能睡觉
	ANNOUNCE_NODAYSLEEP = "光线太亮了。",		--白天睡帐篷
	ANNOUNCE_NODAYSLEEP_CAVE = "妾身不累。",		--洞穴里白天睡帐篷
	ANNOUNCE_NOHUNGERSLEEP = "妾身太饿了，不如先去找些吃的。",		--饿了无法睡觉
	ANNOUNCE_NOSLEEPONFIRE = "你想要烧死妾身吗？",		--营帐着火无法睡觉
	ANNOUNCE_NODANGERSIESTA = "现在午休太危险了！",		--危险，不能午睡
	ANNOUNCE_NONIGHTSIESTA = "晚上该睡觉，而不是午休。",		--夜晚睡凉棚
	ANNOUNCE_NONIGHTSIESTA_CAVE = "妾身不认为这里能真正放松下来。",		--在洞穴里夜晚睡凉棚
	ANNOUNCE_NOHUNGERSIESTA = "妾身太饿了，需要来一点血食！",		--饱度不足无法午睡
	ANNOUNCE_PECKED = "噢！停下！",		--被小高鸟啄
	ANNOUNCE_QUAKE = "听起来不妙。",		--地震
	ANNOUNCE_SHELTER = "大树底下好遮荫！",		--下雨天躲树下
	ANNOUNCE_THORNS = "噢！",		--玫瑰、仙人掌、荆棘扎手
	ANNOUNCE_BURNT = "呀！好烫！",		--烧完了
	ANNOUNCE_TORCH_OUT = "妾身需要一个灯笼！",		--火把用完了
	ANNOUNCE_THURIBLE_OUT = "香消耗殆尽了。",		--香炉燃尽
	ANNOUNCE_FAN_OUT = "乐随风消而逝。",		--小风车停了
    ANNOUNCE_COMPASS_OUT = "这个指南针再也指不了方向了。",		--指南针用完了
	ANNOUNCE_TRAP_WENT_OFF = "哎呀。",		--触发陷阱（例如冬季陷阱）
	ANNOUNCE_WORMHOLE = "头脑正常的人可不会干那事。",		--跳虫洞
	ANNOUNCE_TOWNPORTALTELEPORT = "些许微末法术。",		--豪华传送
	ANNOUNCE_CANFIX = "\n妾身觉得妾身能修好这个！",		--墙壁可以修理
	ANNOUNCE_INSUFFICIENTFERTILIZER = "再来点肥料吧",		--土地肥力不足
	ANNOUNCE_TOOL_SLIP = "哦，那工具太滑手了！",		--工具滑出
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "天雷也休想毁我修为！",		--规避闪电
	ANNOUNCE_TOADESCAPING = "癞蛤蟆想跑！。",		--蟾蜍正在逃跑
	ANNOUNCE_TOADESCAPED = "癞蛤蟆逃走了。",		--蟾蜍逃走了
	ANNOUNCE_DAMP = "织网凝水珠，疾风骤雨来。",		--潮湿（1级）
	ANNOUNCE_WET = "雾随风作雨，湿人庭前衣。",		--潮湿（2级）
	ANNOUNCE_WETTER = "雨落酥胸白似银，香肩入水粉还羞！",		--潮湿（3级）
	ANNOUNCE_SOAKED = "湿处冰肌滑，荡涤玉体新。",		--潮湿（4级）
	ANNOUNCE_WASHED_ASHORE = "不过是跃浪翻波，负水顽耍罢了。",		--暂无注释
	ANNOUNCE_BECOMEGHOST = "本座还有机会！！",		--变成鬼魂
	ANNOUNCE_GHOSTDRAIN = "死了便早早的投胎去，休要惹人清烦...",		--队友死亡掉san
	ANNOUNCE_PETRIFED_TREES = "妾身刚刚听到树在尖叫吗？",		--石化树
	ANNOUNCE_KLAUS_ENRAGE = "不要与他正面硬拼！！",		--克劳斯被激怒（杀死了鹿）
	ANNOUNCE_KLAUS_UNCHAINED = "它的链条松开了！",		--克劳斯-未上锁的  猜测是死亡准备变身？
	ANNOUNCE_KLAUS_CALLFORHELP = "它开始求救了！",		--克劳斯召唤小偷
	ANNOUNCE_MOONALTAR_MINE =
	{
		GLASS_MED = "里面有东西。",		--暂无注释
		GLASS_LOW = "快挖出来了。",		--暂无注释
		GLASS_REVEAL = "你自由了！",		--暂无注释
		IDOL_MED = "里面有东西。",		--暂无注释
		IDOL_LOW = "快挖出来了。",		--暂无注释
		IDOL_REVEAL = "你自由了！",		--暂无注释
		SEED_MED = "里面有东西。",		--暂无注释
		SEED_LOW = "快挖出来了。",		--暂无注释
		SEED_REVEAL = "你自由了！",		--暂无注释
	},
	{
		"only_used_by_willow",		--暂无注释
    },
    ANNOUNCE_HUNGRY_SLOWBUILD =
    {
	    "only_used_by_winona",		--暂无注释
    },
    ANNOUNCE_HUNGRY_FASTBUILD =
    {
	    "only_used_by_winona",		--暂无注释
    },
    ANNOUNCE_KILLEDPLANT =
    {
        "only_used_by_wormwood",		--暂无注释
    },
    ANNOUNCE_GROWPLANT =
    {
        "only_used_by_wormwood",		--暂无注释
    },
    ANNOUNCE_BLOOMING =
    {
        "only_used_by_wormwood",		--暂无注释
    },
    ANNOUNCE_SOUL_EMPTY =
    {
        "only_used_by_wortox",		--暂无注释
    },
    ANNOUNCE_SOUL_FEW =
    {
        "only_used_by_wortox",		--暂无注释
    },
    ANNOUNCE_SOUL_MANY =
    {
        "only_used_by_wortox",		--暂无注释
    },
    ANNOUNCE_SOUL_OVERLOAD =
    {
        "only_used_by_wortox",		--暂无注释
    },
	ANNOUNCE_SLINGHSOT_OUT_OF_AMMO =
	{
		"only_used_by_walter",		--暂无注释
		"only_used_by_walter",		--暂无注释
	},
	ANNOUNCE_STORYTELLING_ABORT_FIREWENTOUT =
	{
        "only_used_by_walter",		--暂无注释
	},
	ANNOUNCE_STORYTELLING_ABORT_NOT_NIGHT =
	{
        "only_used_by_walter",		--暂无注释
	},
    ANNOUNCE_ROYALTY =
    {
        "你也配叫陛下。",		--向带着蜂王帽的队友鞠躬
        "戴个帽子就想装皇帝？。",		--向带着蜂王帽的队友鞠躬
        "你的样子可真可笑！",		--向带着蜂王帽的队友鞠躬
    },
    ANNOUNCE_ATTACH_BUFF_ELECTRICATTACK    = "这果冻酥酥麻麻的！",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_ATTACK            = "妾身现在心狠手“辣”！",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_PLAYERABSORPTION  = "妾身的甲又变厚了！",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_WORKEFFECTIVENESS = "法力澎湃！",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_MOISTUREIMMUNITY  = "啧啧，干巴巴的！",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_SLEEPRESISTANCE   = "某种蘑菇？也没什么",		--暂无注释
    ANNOUNCE_DETACH_BUFF_ELECTRICATTACK    = "身上没有酥麻的感觉了。",		--暂无注释
    ANNOUNCE_DETACH_BUFF_ATTACK            = "妾身不喜欢打架。",		--暂无注释
    ANNOUNCE_DETACH_BUFF_PLAYERABSORPTION  = "虽然时间不长但是还不错。",		--暂无注释
    ANNOUNCE_DETACH_BUFF_WORKEFFECTIVENESS = "妾身只想睡睡觉",		--暂无注释
    ANNOUNCE_DETACH_BUFF_MOISTUREIMMUNITY  = "妾身又不想干活了。",		--暂无注释
    ANNOUNCE_DETACH_BUFF_SLEEPRESISTANCE   = "妾身……（哈欠）要睡了……",		--暂无注释
	ANNOUNCE_OCEANFISHING_LINESNAP = "应该用妾身编织的丝线。",		--暂无注释
	ANNOUNCE_OCEANFISHING_LINETOOLOOSE = "收线也许管用。",		--暂无注释
	ANNOUNCE_OCEANFISHING_GOTAWAY = "它逃走了。",		--暂无注释
	ANNOUNCE_OCEANFISHING_BADCAST = "妾身的钓鱼技艺有待提高……",		--暂无注释
	ANNOUNCE_OCEANFISHING_IDLE_QUOTE =
	{
		"鱼呢？",		--暂无注释
		"也许该换个鱼多的地方钓。",		--暂无注释
		"妾身还以为海里到处都是鱼呢！",		--暂无注释
		"妾身想休息了……",		--暂无注释
	},
	ANNOUNCE_WEIGHT = "重量：{weight}",		--暂无注释
	ANNOUNCE_WEIGHT_HEAVY  = "重量: {weight}\n妾身也是钓鱼的好手!",		--暂无注释
	ANNOUNCE_WINCH_CLAW_MISS = "好像没对准。",		--暂无注释
	ANNOUNCE_WINCH_CLAW_NO_ITEM = "两手空空。",		--暂无注释
    ANNOUNCE_KINGCREATED = "only_used_by_wurt",		--暂无注释
    ANNOUNCE_KINGDESTROYED = "only_used_by_wurt",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_THRONE = "only_used_by_wurt",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_HOUSE = "only_used_by_wurt",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_WATCHTOWER = "only_used_by_wurt",		--暂无注释
    ANNOUNCE_READ_BOOK =
    {
        BOOK_SLEEP = "only_used_by_wurt",		--暂无注释
        BOOK_BIRDS = "only_used_by_wurt",		--暂无注释
        BOOK_TENTACLES =  "only_used_by_wurt",		--暂无注释
        BOOK_BRIMSTONE = "only_used_by_wurt",		--暂无注释
        BOOK_GARDENING = "only_used_by_wurt",		--暂无注释
		BOOK_SILVICULTURE = "only_used_by_wurt",		--暂无注释
		BOOK_HORTICULTURE = "only_used_by_wurt",		--暂无注释
    },
    ANNOUNCE_WEAK_RAT = "这只胡萝卜鼠不在训练的状态。",		--暂无注释
    ANNOUNCE_CARRAT_START_RACE = "比赛开始！",		--暂无注释
    ANNOUNCE_CARRAT_ERROR_WRONG_WAY = 
    {
        "不，不！方向错了！",		--暂无注释
        "掉头，你什么眼神啊！",		--暂无注释
    },
    ANNOUNCE_CARRAT_ERROR_FELL_ASLEEP = "你敢给妾身睡！蠢货，妾身要赢比赛！",		--暂无注释
    ANNOUNCE_CARRAT_ERROR_WALKING = "不要用走，跑起来，笨蛋！",		--暂无注释
    ANNOUNCE_CARRAT_ERROR_STUNNED = "你这刁奴站起来！冲，冲！",		--暂无注释
    ANNOUNCE_GHOST_QUEST = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_GHOST_HINT = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_GHOST_TOY_NEAR = 
    {
        "only_used_by_wendy",		--暂无注释
    },
	ANNOUNCE_SISTURN_FULL = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_ABIGAIL_DEATH = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_ABIGAIL_RETRIEVE = "only_used_by_wendy",		--暂无注释
	ANNOUNCE_ABIGAIL_LOW_HEALTH = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_ABIGAIL_SUMMON =
	{
		LEVEL1 = "only_used_by_wendy",		--暂无注释
		LEVEL2 = "only_used_by_wendy",		--暂无注释
		LEVEL3 = "only_used_by_wendy",		--暂无注释
	},
    ANNOUNCE_GHOSTLYBOND_LEVELUP =
	{
		LEVEL2 = "only_used_by_wendy",		--暂无注释
		LEVEL3 = "only_used_by_wendy",		--暂无注释
	},
    ANNOUNCE_NOINSPIRATION = "only_used_by_wathgrithr",		--暂无注释
    ANNOUNCE_BATTLESONG_INSTANT_TAUNT_BUFF = "only_used_by_wathgrithr",		--暂无注释
    ANNOUNCE_BATTLESONG_INSTANT_PANIC_BUFF = "only_used_by_wathgrithr",		--暂无注释
    ANNOUNCE_WANDA_YOUNGTONORMAL = "only_used_by_wanda",		--暂无注释
    ANNOUNCE_WANDA_NORMALTOOLD = "only_used_by_wanda",		--暂无注释
    ANNOUNCE_WANDA_OLDTONORMAL = "only_used_by_wanda",		--暂无注释
    ANNOUNCE_WANDA_NORMALTOYOUNG = "only_used_by_wanda",		--暂无注释
	ANNOUNCE_POCKETWATCH_PORTAL = "没有人告诉妾身时间旅行完这么痛苦……",		--暂无注释
	ANNOUNCE_POCKETWATCH_MARK = "only_used_by_wanda",		--暂无注释
	ANNOUNCE_POCKETWATCH_RECALL = "only_used_by_wanda",		--暂无注释
	ANNOUNCE_POCKETWATCH_OPEN_PORTAL = "only_used_by_wanda",		--暂无注释
	ANNOUNCE_POCKETWATCH_OPEN_PORTAL_DIFFERENTSHARD = "only_used_by_wanda",		--暂无注释
    ANNOUNCE_ARCHIVE_NEW_KNOWLEDGE = "来自远古传承拓宽了妾身的思路！",		--暂无注释
    ANNOUNCE_ARCHIVE_OLD_KNOWLEDGE = "妾身早就知道了。",		--暂无注释
    ANNOUNCE_ARCHIVE_NO_POWER = "得给它供点能。",		--暂无注释
    ANNOUNCE_PLANT_RESEARCHED =
    {
        "妾身现在更会照顾这些小苗了！",		--暂无注释
    },
    ANNOUNCE_PLANT_RANDOMSEED = "不知道会长成什么。",		--暂无注释
    ANNOUNCE_FERTILIZER_RESEARCHED = "妾身从未想过如此。",		--暂无注释
	ANNOUNCE_FIRENETTLE_TOXIN =
	{
		"妾身现在想脱下几件衣裳！",		--暂无注释
		"哎哟，太热了！",		--暂无注释
	},
	ANNOUNCE_FIRENETTLE_TOXIN_DONE = "你烫坏了妾身的染甲。",		--暂无注释
	ANNOUNCE_TALK_TO_PLANTS =
	{
        "若是耽误时辰，便把你们一个个都拔了！",		--暂无注释
        "你这样的在我那花园里算得上什么。",		--暂无注释
		"妾身可以和你说说我那木香亭的古树！",		--暂无注释
        "一柱普普通通的草木。",		--暂无注释
        "聆听呢喃细语。",		--暂无注释
	},
    ANNOUNCE_CALL_BEEF = "你给妾身过来！",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_YOTB_POST = "这里妾身很难取胜。",		--暂无注释
    ANNOUNCE_YOTB_LEARN_NEW_PATTERN =  "妾身从来都不缺创意！",		--暂无注释
	BATTLECRY =
	{
		GENERIC = "织设罗网，待杀众生！",		--战斗
		PIG = "到这儿来，愚笨的小猪！",		--战斗 猪人
		PREY = "妾身只需动动手指。",		--战斗 猎物？？大象？
		SPIDER = "些许微末道行都无，也敢于本座面前放肆！",		--战斗 蜘蛛
		SPIDER_WARRIOR = "竟敢冒犯本尊！",		--战斗 蜘蛛战士
		DEER = "与我融为一体吧！",		--战斗 无眼鹿
	},
	COMBAT_QUIT =
	{
		GENERIC = "现在不是时候！",		--攻击目标被卡住，无法攻击
		PIG = "这次姑且放过他。",		--退出战斗-猪人
		PREY = "你这脚底倒是滑溜！",		--退出战斗 猎物？？大象？
		SPIDER = "你同妾身本一族，且放你一马。",		-- 退出战斗 蜘蛛
		SPIDER_WARRIOR = "不若来替妾身搏斗！",		--退出战斗 蜘蛛战士
	},
	DESCRIBE =
	{
		MULTIPLAYER_PORTAL = "如此瑰丽，如此...",		-- 物品名:"绚丽之门"
        MULTIPLAYER_PORTAL_MOONROCK = "承载着月华之灵的大门。",		-- 物品名:"天体传送门"
        MOONROCKIDOL = "妾身只崇拜无边无际的法力。",		-- 物品名:"月岩雕像" 制造描述:"重要人物。"
        CONSTRUCTION_PLANS = "小玩意！",		-- 物品名:未找到
        ANTLION =
        {
            GENERIC = "妾身身上有它想要的东西。",		-- 物品名:"蚁狮"->默认
            VERYHAPPY = "你要来做本座的仆人吗？",		-- 物品名:"蚁狮"
            UNHAPPY = "看起来很生气。",		-- 物品名:"蚁狮"
        },
        ANTLIONTRINKET = "有人可能对此感兴趣。",		-- 物品名:"沙滩玩具"
        SANDSPIKE = "一段流沙聚集罢了！",		-- 物品名:"沙刺"
        SANDBLOCK = "真是坚持不懈！",		-- 物品名:"沙堡"
        GLASSSPIKE = "这可划不破妾身的网。",		-- 物品名:"玻璃尖刺"
        GLASSBLOCK = "这份礼物送给你。",		-- 物品名:"玻璃城堡"
        ABIGAIL_FLOWER =
        {
            GENERIC ="莫要眷恋尘世。",		-- 物品名:"阿比盖尔之花"->默认 制造描述:"一个神奇的纪念品。"
			LEVEL1 = "你想一个人待着吗？",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
			LEVEL2 = "你该走了。",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
			LEVEL3 = "破损的魂魄，不该久居人间！",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            LONG = "你应该可以修补妾身的灵魂伤。",		-- 物品名:"阿比盖尔之花"->还需要很久 制造描述:"一个神奇的纪念品。"
            MEDIUM = "这让妾身毛骨悚然。",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            SOON = "那朵花有情况！",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            HAUNTED_POCKET = "妾身该放下它了。",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            HAUNTED_GROUND = "不若我来送你走吧。",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
        },
        BALLOONS_EMPTY = "看起来像小丑货币。",		-- 物品名:"一堆气球" 制造描述:"要是有更简单的方法该多好。"
        BALLOON = "他们怎么漂浮的？",		-- 物品名:"气球" 制造描述:"谁不喜欢气球呢？"
		BALLOONPARTY = "他怎么把小气球放进去的？",		-- 物品名:"派对气球" 制造描述:"散播一点欢笑。"
		BALLOONSPEED =
        {
            DEFLATED = "所有气球都一样。",		-- 物品名:"迅捷气球" 制造描述:"让你的脚步变得轻盈。"
            GENERIC = "多来几个能让妾身飞起来吗？",		-- 物品名:"迅捷气球"->默认 制造描述:"让你的脚步变得轻盈。"
        },
        BERNIE_INACTIVE =
        {
            BROKEN = "它最终土崩瓦解。",		-- 物品名:"伯尼" 制造描述:"这个疯狂的世界总有你熟悉的人。"
            GENERIC = "它全烧焦了。",		-- 物品名:"伯尼"->默认 制造描述:"这个疯狂的世界总有你熟悉的人。"
        },
        BERNIE_ACTIVE = "这算一件有器灵的法器吗？",		-- 物品名:"伯尼"
        BERNIE_BIG = "哦，不要靠近妾身。",		-- 物品名:"伯尼！"
        BOOK_BIRDS = "一本画的不错的图册。",		-- 物品名:"世界鸟类大全" 制造描述:"涵盖1000个物种：习性、栖息地及叫声。"
        BOOK_TENTACLES = "有人会被拖着去读这本书。",		-- 物品名:"触手的召唤" 制造描述:"让妾身来了解一下地下的朋友们！"
        BOOK_GARDENING = "还不如看齐民要术来得方便。",		-- 物品名:"应用园艺学" 制造描述:"讲述培育和照料植物的相关知识。"
		BOOK_SILVICULTURE = "植树造林可不是妾身该做的事情。",		-- 物品名:"应用造林学" 制造描述:"分支管理的指引。"
		BOOK_HORTICULTURE = "还不如看齐民要术来得方便。",		-- 物品名:"应用园艺学，简编" 制造描述:"讲述培育和照料植物的相关知识。"
        BOOK_SLEEP = "太无趣了。",		-- 物品名:"睡前故事" 制造描述:"送你入梦的睡前故事。"
        BOOK_BRIMSTONE = "若我法力滔天，何惧万劫生灭。",		-- 物品名:"末日将至！" 制造描述:"世界将在火焰和灾难中终结！"
        PLAYER =
        {
            GENERIC = "你好，%s！",		-- 物品名:未找到
            ATTACKER = "%s 看着很善变...",		-- 物品名:未找到
            MURDERER = "谋杀犯！",		-- 物品名:未找到
            REVIVER = "%s，该回来了。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s 你想活吗？",		-- 物品名:"幽灵"
            FIRESTARTER = "烧掉这个并不好，%s。",		-- 物品名:未找到
        },
        WILSON =
        {
            GENERIC = "你好，大科学家是吗？",		-- 物品名:"威尔逊"->默认
            ATTACKER = "妾身看起来很吓人吗？",		-- 物品名:"威尔逊"
            MURDERER = "你的存在触犯了本座的威严，%s！",		-- 物品名:"威尔逊"
            REVIVER = "%s现在是时候了。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你把你的肉身丢在哪里了？",		-- 物品名:"幽灵"
            FIRESTARTER = "烧掉这个并不好，%s。",		-- 物品名:"威尔逊"
        },
        WOLFGANG =
        {
            GENERIC = "壮士，%s！",		-- 物品名:"沃尔夫冈"->默认
            ATTACKER = "不要妄图冒犯本尊...",		-- 物品名:"沃尔夫冈"
            MURDERER = "本尊要杀了你！",		-- 物品名:"沃尔夫冈"
            REVIVER = "%s只是一只虚弱巨熊。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "妾身跟你说过，不要乱来。",		-- 物品名:"幽灵"
            FIRESTARTER = "你是没法打倒火的，%s！",		-- 物品名:"沃尔夫冈"
        },
        WAXWELL =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"麦斯威尔"->默认
            ATTACKER = "你似乎想与我来一场。",		-- 物品名:"麦斯威尔"
            MURDERER = "妾身要教什么是礼貌！",		-- 物品名:"麦斯威尔"
            REVIVER = "%s做我的仆人怎么样。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "不要那样看妾身，%s！",		-- 物品名:"幽灵"
            FIRESTARTER = "%s只求火烤。",		-- 物品名:"麦斯威尔"
        },
        WX78 =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"WX-78"->默认
            ATTACKER = "%s，妾身想并不需要你...",		-- 物品名:"WX-78"
            MURDERER = "%s！你现在要变成一堆废铁了！",		-- 物品名:"WX-78"
            REVIVER = "妾身猜这可以让你动起来。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "妾身一直认为你该长点心。",		-- 物品名:"幽灵"
            FIRESTARTER = "%s！你的胳膊快融化了。",		-- 物品名:"WX-78"
        },
        WILLOW =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"薇洛"->默认
            ATTACKER = "%s紧紧抓住希望...",		-- 物品名:"薇洛"
            MURDERER = "纵火犯！离妾身远一点",		-- 物品名:"薇洛"
            REVIVER = "%s，火焰的朋友。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s，妾身敢肯定你渴望有一颗心。",		-- 物品名:"幽灵"
            FIRESTARTER = "再来？",		-- 物品名:"薇洛"
        },
        WENDY =
        {
            GENERIC = "你好，%s！",		-- 物品名:"温蒂"->默认
            ATTACKER = "%s没有尖锐的东西，有吗？",		-- 物品名:"温蒂"
            MURDERER = "快把你那晦气姐妹带走！",		-- 物品名:"温蒂"
            REVIVER = "%s视鬼魂为家人。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你还是早点送走你的姐妹吧。",		-- 物品名:"幽灵"
            FIRESTARTER = "妾身知道是你点的那些火焰，%s。",		-- 物品名:"温蒂"
        },
        WOODIE =
        {
            GENERIC = "你好，%s！",		-- 物品名:"伍迪"->默认
            ATTACKER = "%s最近有点活力...",		-- 物品名:"伍迪"
            MURDERER = "本座要让你赔命！",		-- 物品名:"伍迪"
            REVIVER = "%s救下了。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s，“宇宙”包括虚无吗？",		-- 物品名:"幽灵"
            BEAVER = "%s在疯狂的砍树。",		-- 物品名:"伍迪"
            BEAVERGHOST = "%s，如果妾身复活你，来当我的仆人怎么样？",		-- 物品名:"伍迪"
            MOOSE = "老天爷啊，这是一头鹿！",		-- 物品名:"伍迪"
            MOOSEGHOST = "那一定很不舒服吧。",		-- 物品名:"伍迪"
            GOOSE = "瞧瞧这玩意！",		-- 物品名:"伍迪"
            GOOSEGHOST = "你这头蠢鹅！滚开。",		-- 物品名:"伍迪"
            FIRESTARTER = "%s，别把自己烧了。",		-- 物品名:"伍迪"
        },
        WICKERBOTTOM =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"薇克巴顿"->默认
            ATTACKER = "你看起来干巴巴的不好吃。",		-- 物品名:"薇克巴顿"
            MURDERER = "哈哈！您还是早点回家吧。",		-- 物品名:"薇克巴顿"
            REVIVER = "哦。多么美妙。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "这似乎不太妙，，%s？",		-- 物品名:"幽灵"
            FIRESTARTER = "妾身相信您一定是有原因的。",		-- 物品名:"薇克巴顿"
        },
        WES =
        {
            GENERIC = "你好，%s！",		-- 物品名:"韦斯"->默认
            ATTACKER = "%s死寂般沉默...",		-- 物品名:"韦斯"
            MURDERER = "快走吧，妾身不爱看哑剧！",		-- 物品名:"韦斯"
            REVIVER = "不过是固有的模式。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你要什么？大声说出来。",		-- 物品名:"幽灵"
            FIRESTARTER = "等等，火是你点的？",		-- 物品名:"韦斯"
        },
        WEBBER =
        {
            GENERIC = "小可爱，来当我的儿子如何~",		-- 物品名:"韦伯"->默认
            ATTACKER = "妾身希望你能多活一会。",		-- 物品名:"韦伯"
            MURDERER = "你在挑衅本尊。",		-- 物品名:"韦伯"
            REVIVER = "%s和其他人打成一片。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s，妾身可以把你拉回来。",		-- 物品名:"幽灵"
            FIRESTARTER = "妾身得防一下你了。",		-- 物品名:"韦伯"
        },
        WATHGRITHR =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"薇格弗德"->默认
            ATTACKER = "妾身很害怕%s的拳头。",		-- 物品名:"薇格弗德"
            MURDERER = "%s变得狂暴！",		-- 物品名:"薇格弗德"
            REVIVER = "%s精神饱满。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你怎么不去你的英灵殿呢，%s。",		-- 物品名:"幽灵"
            FIRESTARTER = "%s是个放火好手。",		-- 物品名:"薇格弗德"
        },
        WINONA =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"薇诺娜"->默认
            ATTACKER = "%s是安全隐患.",		-- 物品名:"薇诺娜"
            MURDERER = "到此为止了，%s！",		-- 物品名:"薇诺娜"
            REVIVER = "你可真是方便好用啊，%s。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "好像有人在偷袭你。",		-- 物品名:"幽灵"
            FIRESTARTER = "你的工厂烧起来了。",		-- 物品名:"薇诺娜"
        },
        WORTOX =
        {
            GENERIC = "你好，%s！",		-- 物品名:"沃拓克斯"->默认
            ATTACKER = "妾身就知道%s不可信！",		-- 物品名:"沃拓克斯"
            MURDERER = "恶魔！妖精？谁又比谁好呢？",		-- 物品名:"沃拓克斯"
            REVIVER = "多谢你的援助之手%s。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "妾身并不是很想与恶魔交易。",		-- 物品名:"幽灵"
            FIRESTARTER = "%s你现在是我们的负担。",		-- 物品名:"沃拓克斯"
        },
        WORMWOOD =
        {
            GENERIC = "你好，%s！",		-- 物品名:"沃姆伍德"->默认
            ATTACKER = "%s今天似乎比平时更多刺。",		-- 物品名:"沃姆伍德"
            MURDERER = "准备被本尊除去吧，小杂草。",		-- 物品名:"沃姆伍德"
            REVIVER = "%s从来不会放弃他的朋友。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "需要一些帮助吧，小伙伴？",		-- 物品名:"幽灵"
            FIRESTARTER = "妾身以为你讨厌火，%s.",		-- 物品名:"沃姆伍德"
        },
        WARLY =
        {
            GENERIC = "你好，%s！",		-- 物品名:"沃利"->默认
            ATTACKER = "酝酿着灾难。",		-- 物品名:"沃利"
            MURDERER = "别打妾身的主意！",		-- 物品名:"沃利"
            REVIVER = "总是可以指望%s来做一个计划。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你现在可以去给无常鬼做饭了。",		-- 物品名:"幽灵"
            FIRESTARTER = "他会把这个地方都烧了！",		-- 物品名:"沃利"
        },
        WURT =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"沃特"->默认
            ATTACKER = "%s今天一副凶神恶煞的样子……",		-- 物品名:"沃特"
            MURDERER = "妾身认识的鱼精都比你漂亮！",		-- 物品名:"沃特"
            REVIVER = "为什么要谢你，%s！",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s你现在怎么不喘气了。",		-- 物品名:"幽灵"
            FIRESTARTER = "就没人教你别玩火吗？！",		-- 物品名:"沃特"
        },
        WALTER =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"沃尔特"->默认
            ATTACKER = "这是你该做的事吗？",		-- 物品名:"沃尔特"
            MURDERER = "你的故事说完了吗，%s？",		-- 物品名:"沃尔特"
            REVIVER = "%s永远靠谱。",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "妾身知道你玩的很开心，可惜丢了小命。",		-- 物品名:"幽灵"
            FIRESTARTER = "那个看起来可不像是营火，%s。",		-- 物品名:"沃尔特"
        },
        WANDA =
        {
            GENERIC = "久仰，%s！",		-- 物品名:"旺达"->默认
            ATTACKER = "不要靠经时间，%s！",		-- 物品名:"旺达"
            MURDERER = "妾身绝不会给你第二次机会的！",		-- 物品名:"旺达"
            REVIVER = "要不是%s，妾身就死了！",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "你为什么不去试试时间倒流。",		-- 物品名:"幽灵"
            FIRESTARTER = "时间不是你能掌握的。",		-- 物品名:"旺达"
        },
        MIGRATION_PORTAL =
        {
        },
        GLOMMER =
        {
            GENERIC = "恶趣味，确又不失可爱。",		-- 物品名:"格罗姆"->默认
            SLEEPING = "这虫子过的真不错。",		-- 物品名:"格罗姆"->睡着了
        },
        GLOMMERFLOWER =
        {
            GENERIC = "花瓣微微发亮。",		-- 物品名:"格罗姆花"->默认
            DEAD = "花瓣枯萎，往日不在。",		-- 物品名:"格罗姆花"->死了
        },
        GLOMMERWINGS = "这些装在头盔上好看极了！",		-- 物品名:"格罗姆翅膀"
        GLOMMERFUEL = "这团糊糊恶臭无比。",		-- 物品名:"格罗姆的黏液"
        BELL = "叮呤呤。",		-- 物品名:"远古铃铛" 制造描述:"这可不是普通的铃铛。"
        STATUEGLOMMER =
        {
            GENERIC = "妾身不确定那是什么东西。",		-- 物品名:"格罗姆雕像"->默认
            EMPTY = "打碎了，你还想要妾身赔偿吗？",		-- 物品名:"格罗姆雕像"
        },
        LAVA_POND_ROCK = "这地不赖啊。",		-- 物品名:"岩石"
		WEBBERSKULL = "可怜的小家伙",		-- 物品名:"韦伯的头骨"
		WORMLIGHT = "看起来很好吃。",		-- 物品名:"发光浆果"
		WORMLIGHT_LESSER = "有点皱巴巴的。",		-- 物品名:"小发光浆果"
		WORM =
		{
		    PLANT = "小小伪装罢了。",		-- 物品名:"洞穴蠕虫"
		    DIRT = "看起来就像一堆土。",		-- 物品名:"洞穴蠕虫"
		    WORM = "它是一只蠕虫！",		-- 物品名:"洞穴蠕虫"
		},
        WORMLIGHT_PLANT = "小小伪装罢了。",		-- 物品名:"神秘植物"
		MOLE =
		{
			HELD = "没什么可挖的了，小土地。",		-- 物品名:"鼹鼠"->拿在手里
			UNDERGROUND = "下面的家伙，别挖洞了。",		-- 物品名:"鼹鼠"
			ABOVEGROUND = "一碰就晕了...",		-- 物品名:"鼹鼠"
		},
		MOLEHILL = "多么美好舒适的地洞啊！",		-- 物品名:"鼹鼠洞"
		MOLEHAT = "夜晚，是狩猎的绝佳时机。",		-- 物品名:"鼹鼠帽" 制造描述:"为穿戴者提供夜视能力。"
		EEL = "这能做一道好菜。",		-- 物品名:"鳗鱼"
		EEL_COOKED = "闻起来真香！",		-- 物品名:"烤鳗鱼"
		UNAGI = "希望你不会让妾身失望！",		-- 物品名:"鳗鱼料理"
		EYETURRET = "希望它不会看向妾身。",		-- 物品名:"眼睛炮塔"
		EYETURRET_ITEM = "妾身想它在睡觉。",		-- 物品名:"眼睛炮塔" 制造描述:"要远离讨厌的东西，就得杀了它们。"
		MINOTAURHORN = "妾身可受不了这个大角！",		-- 物品名:"守护者之角"
		MINOTAURCHEST = "让妾身看看有没有什么好东西。",		-- 物品名:"大号华丽箱子"
		THULECITE_PIECES = "更小块的铥矿。",		-- 物品名:"铥矿碎片"
		POND_ALGAE = "池塘边的水藻。",		-- 物品名:"水藻"
		GREENSTAFF = "这东西迟早派得上用场。",		-- 物品名:"拆解魔杖" 制造描述:"干净而有效的摧毁。"
		GIFT = "那是要送给妾身的吗？",		-- 物品名:"礼物"
        GIFTWRAP = "好棒！",		-- 物品名:"礼物包装" 制造描述:"把东西打包起来，好看又可爱！"
		POTTEDFERN = "盆里的蕨类植物。",		-- 物品名:"蕨类盆栽" 制造描述:"做个花盆，里面栽上蕨类植物。"
        SUCCULENT_POTTED = "盆里的多肉植物。",		-- 物品名:"多肉盆栽" 制造描述:"塞进陶盆的漂亮多肉植物。"
		SUCCULENT_PLANT = "那里有芦荟。",		-- 物品名:"多肉植物"
		SUCCULENT_PICKED = "妾身能吃那个，但妾身不想吃。",		-- 物品名:"多肉植物"
		SENTRYWARD = "啊~妾身可不想在沐浴时见到你。",		-- 物品名:"月眼守卫" 制造描述:"绘图者最有价值的武器。"
        TOWNPORTAL =
        {
			GENERIC = "这个金字塔控制着沙子。",		-- 物品名:"强征传送塔"->默认 制造描述:"用沙子的力量聚集你的朋友们。"
			ACTIVE = "准备好来一次说走就走的旅行。",		-- 物品名:"强征传送塔"->激活了 制造描述:"用沙子的力量聚集你的朋友们。"
		},
        TOWNPORTALTALISMAN =
        {
			GENERIC = "受到召唤的石头。",		-- 物品名:"沙之石"->默认
			ACTIVE = "未知的法力在驱动着。",		-- 物品名:"沙之石"->激活了
		},
        WETPAPER = "妾身希望它快点干。",		-- 物品名:"纸张"
        WETPOUCH = "这个包裹只能勉强绑在一起。",		-- 物品名:"起皱的包裹"
        MOONROCK_PIECES = "天狗漏下来的碎渣？",		-- 物品名:"月亮石碎块"
        MOONBASE =
        {
            GENERIC = "中间有个洞，好像可以扔东西进去。",		-- 物品名:"月亮石"->默认
            BROKEN = "都搞砸了。",		-- 物品名:"月亮石"
            STAFFED = "然后呢？",		-- 物品名:"月亮石"
            WRONGSTAFF = "妾身有一种感觉，这明显不对。",		-- 物品名:"月亮石"->插错法杖
            MOONSTAFF = "石头神奇的点燃了它。",		-- 物品名:"月亮石"->已经插了法杖的（月杖）
        },
        MOONDIAL =
        {
			GENERIC = "水承载月，月倒映了水。",		-- 物品名:"月晷"->默认 制造描述:"追踪月相！"
			NIGHT_NEW = "是新月。",		-- 物品名:"月晷"->新月 制造描述:"追踪月相！"
			NIGHT_WAX = "月亮慢慢在变圆。",		-- 物品名:"月晷"->上弦月 制造描述:"追踪月相！"
			NIGHT_FULL = "是满月。",		-- 物品名:"月晷"->满月 制造描述:"追踪月相！"
			NIGHT_WANE = "月亮正在变小。",		-- 物品名:"月晷"->下弦月 制造描述:"追踪月相！"
			CAVE = "这下面没有月亮来测量。",		-- 物品名:"月晷"->洞穴 制造描述:"追踪月相！"
			WEREBEAVER = "only_used_by_woodie", --woodie specific		-- 物品名:"月晷" 制造描述:"追踪月相！"
			GLASSED = "妾身有种奇怪的感觉，好像有人在盯着妾身。",		-- 物品名:"月晷" 制造描述:"追踪月相！"
        },
		THULECITE = "妾身想知道这是哪来的？",		-- 物品名:"铥矿" 制造描述:"将小碎片合成一大块。"
		ARMORRUINS = "它轻得古怪。",		-- 物品名:"铥矿甲" 制造描述:"炫目并且能提供保护。"
		ARMORSKELETON = "铮铮白骨",		-- 物品名:"骨头盔甲"
		SKELETONHAT = "它给妾身带来可怕的幻象。",		-- 物品名:"骨头头盔"
		RUINS_BAT = "份量相当重。",		-- 物品名:"铥矿棒" 制造描述:"尖钉让一切变得更好。"
		RUINSHAT = "妾身的头发怎么样？",		-- 物品名:"铥矿皇冠" 制造描述:"附有远古力场！"
		NIGHTMARE_TIMEPIECE =
		{
            CALM = "一切都好了。",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            WARN = "感测到这里有相当强的混沌之灵。",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            WAXING = "变得越来越密集了！",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            STEADY = "好像保持稳定了。",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            WANING = "感觉法力正在逐渐减弱。",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            DAWN = "噩梦就要结束了！",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
            NOMAGIC = "这里没有灵气。",		-- 物品名:"铥矿徽章" 制造描述:"跟踪周围魔力水平的流动。"
		},
		BISHOP_NIGHTMARE = "它在分崩离析！",		-- 物品名:"损坏的发条主教"
		ROOK_NIGHTMARE = "真可怕！",		-- 物品名:"损坏的发条战车"
		KNIGHT_NIGHTMARE = "一个残暴的发条骑士！",		-- 物品名:"损坏的发条骑士"
		MINOTAUR = "那家伙看起来不太高兴。",		-- 物品名:"远古守护者"
		SPIDER_DROPPER = "来我怀里睡一觉吗。",		-- 物品名:"穴居悬蛛"
		NIGHTMARELIGHT = "死后的残渣，依旧能点亮。",		-- 物品名:"梦魇灯座"
		NIGHTSTICK = "雷音冥冥，呼啸其中！",		-- 物品名:"晨星锤" 制造描述:"用于夜间战斗的晨光。"
		GREENGEM = "生命的法则再闪烁。",		-- 物品名:"绿宝石"
		MULTITOOL_AXE_PICKAXE = "太高明了！",		-- 物品名:"多用斧镐" 制造描述:"加倍实用。"
		ORANGESTAFF = "这比走路好。",		-- 物品名:"懒人魔杖" 制造描述:"适合那些不喜欢走路的人。"
		YELLOWAMULET = "手感温暖。",		-- 物品名:"魔光护符" 制造描述:"从天堂汲取力量。"
		GREENAMULET = "事半功倍！",		-- 物品名:"建造护符" 制造描述:"用更少的材料合成物品！"
		SLURPERPELT = "死了的样子也没什么不同。",		-- 物品名:"铥矿徽章"->啜食者皮 制造描述:"跟踪周围魔力水平的流动。"
		SLURPER = "好多毛！",		-- 物品名:"啜食者"
		SLURPER_PELT = "死了的样子也没什么不同。",		-- 物品名:"啜食者皮"
		ARMORSLURPER = "一根湿漉漉的腰带。",		-- 物品名:"饥饿腰带" 制造描述:"保持肚子不饿。"
		ORANGEAMULET = "瞬移的用处可大了。",		-- 物品名:"懒人护符" 制造描述:"适合那些不喜欢捡东西的人。"
		YELLOWSTAFF = "太阳之灵，加持吾身。",		-- 物品名:"唤星者魔杖" 制造描述:"召唤一个小星星。"
		YELLOWGEM = "这个宝石是黄色的。",		-- 物品名:"黄宝石"
		ORANGEGEM = "一颗橙宝石。",		-- 物品名:"橙宝石"
        OPALSTAFF = "法力！这正是本座追求的力量！",		-- 物品名:"唤月者魔杖"
        OPALPRECIOUSGEM = "瑰丽混沌，融为一体。",		-- 物品名:"彩虹宝石"
        TELEBASE =
		{
			VALID = "它准备好了。",		-- 物品名:"传送焦点"->有效 制造描述:"装上宝石试试。"
			GEMS = "它需要更多紫宝石。",		-- 物品名:"传送焦点"->需要宝石 制造描述:"装上宝石试试。"
		},
		GEMSOCKET =
		{
			VALID = "看起来准备就绪了。",		-- 物品名:"宝石底座"->有效
			GEMS = "它需要一颗宝石。",		-- 物品名:"宝石底座"->需要宝石
		},
		STAFFLIGHT = "那似乎非常危险。",		-- 物品名:"矮星"
        STAFFCOLDLIGHT = "呃！令人寒心。",		-- 物品名:"极光"
        ANCIENT_ALTAR = "古老而神秘的建筑。",		-- 物品名:"远古伪科学站"
        ANCIENT_ALTAR_BROKEN = "这个好像破损了。",		-- 物品名:"损坏的远古伪科学站"
        ANCIENT_STATUE = "它好像在与世界格格不入地震动。",		-- 物品名:"远古雕像"
        LICHEN = "妾身还是更喜欢肉食。",		-- 物品名:"洞穴苔藓"
		CUTLICHEN = "有营养吗，妾身还有小宝宝要喂养呢。",		-- 物品名:"苔藓"
		CAVE_BANANA = "软塌塌的，看起来很恶心。",		-- 物品名:"洞穴香蕉"
		CAVE_BANANA_COOKED = "快快拿走！",		-- 物品名:"烤香蕉"
		CAVE_BANANA_TREE = "我怀疑你因何而生。",		-- 物品名:"洞穴香蕉树"
		ROCKY = "它的钳子里看上去没多少肉。",		-- 物品名:"石虾"
		COMPASS =
		{
			GENERIC="妾身面朝哪个方向？",		-- 物品名:"指南针"->默认 制造描述:"指向北方。"
			N = "北。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			S = "南。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			E = "东。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			W = "西。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			NE = "东北。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			SE = "东南。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			NW = "西北。",		-- 物品名:"指南针" 制造描述:"指向北方。"
			SW = "西南。",		-- 物品名:"指南针" 制造描述:"指向北方。"
		},
        HOUNDSTOOTH = "真锋利！",		-- 物品名:"犬牙"
        ARMORSNURTLESHELL = "不可能，绝对不可能。",	-- 物品名:"蜗壳护甲"
        BAT = "啊！真可怕！",		-- 物品名:"洞穴蝙蝠"
        BATBAT = "妾身敢打赌，它的味道会不错。",		-- 物品名:"蝙蝠棒" 制造描述:"所有科技都如此...耗费精神。"
        BATWING = "那些东西即使死了都很讨厌。",		-- 物品名:"洞穴蝙蝠翅膀"
        BATWING_COOKED = "至少它不会再活过来了。",		-- 物品名:"烤蝙蝠翅膀"
        BATCAVE = "妾身可不想吵醒它们。",		-- 物品名:"蝙蝠洞"
        BEDROLL_FURRY = "真是又暖和又舒服。",		-- 物品名:"毛皮铺盖" 制造描述:"舒适地一觉睡到天亮！"
        BUNNYMAN = "化形失败的兔子精？",		-- 物品名:"兔人"
        FLOWER_CAVE = "可以拿来点缀妾身的家。",		-- 物品名:"荧光花"
        GUANO = "你可别想玷污妾身的发髻。",		-- 物品名:"鸟粪"
        LANTERN = "更好的灯具。",		-- 物品名:"提灯" 制造描述:"可加燃料、明亮、便携！"
        LIGHTBULB = "奇怪，看起来就很好吃。",		-- 物品名:"荧光果"
        MANRABBIT_TAIL = "来一副兔绒手套，冬天会更好。",		-- 物品名:"兔绒"
        MUSHROOMHAT = "蹩脚的伪装。",		-- 物品名:"蘑菇帽"
        MUSHROOM_LIGHT2 =
        {
            ON = "他看起来冰冰凉凉的。",		-- 物品名:"菌伞灯"->开启 制造描述:"受到火山岩浆灯饰学问的激发。"
            OFF = "为我点灯。",		-- 物品名:"菌伞灯"->关闭 制造描述:"受到火山岩浆灯饰学问的激发。"
            BURNT = "妾身没有让它发霉。",		-- 物品名:"菌伞灯"->烧焦的 制造描述:"受到火山岩浆灯饰学问的激发。"
        },
        MUSHROOM_LIGHT =
        {
            ON = "又把它点亮了。",		-- 物品名:"蘑菇灯"->开启 制造描述:"任何蘑菇的完美添加物。"
            OFF = "它是发光的大蘑菇。",		-- 物品名:"蘑菇灯"->关闭 制造描述:"任何蘑菇的完美添加物。"
            BURNT = "完全烧焦。",		-- 物品名:"蘑菇灯"->烧焦的 制造描述:"任何蘑菇的完美添加物。"
        },
        SLEEPBOMB = "一扔就让妾身犯困。",		-- 物品名:"睡袋" 制造描述:"可以扔掉的袋子睡意沉沉。"
        MUSHROOMBOMB = "一朵蘑菇云正在生成！",		-- 物品名:"炸弹蘑菇"
        SHROOM_SKIN = "疙疙瘩瘩的！",		-- 物品名:"蘑菇皮"
        TOADSTOOL_CAP =
        {
            EMPTY = "只是地上的一个洞。",		-- 物品名:"毒菌蟾蜍"
            INGROUND = "有东西伸出来了。",		-- 物品名:"毒菌蟾蜍"->在地里面
            GENERIC = "那毒菌只是想被砍掉。",		-- 物品名:"毒菌蟾蜍"->默认
        },
        TOADSTOOL =
        {
            GENERIC = "咦嘘！散发着臭气的癞蛤蟆！",		-- 物品名:"毒菌蟾蜍"->默认
            RAGE = "它狂跳不止！",		-- 物品名:"毒菌蟾蜍"->愤怒
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "带上去会不会变成傻子！",		-- 物品名:"孢子帽"->默认
            BURNT = "多么不道德！",		-- 物品名:"孢子帽"->烧焦的
        },
        MUSHTREE_TALL =
        {
            GENERIC = "那个蘑菇长得太大了。",		-- 物品名:"蓝蘑菇树"->默认
            BLOOM = "闻之，令人作呕。",		-- 物品名:"蓝蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "那个蘑菇长得太大了。",		-- 物品名:"红蘑菇树"->默认
            BLOOM = "闻之，令人作呕。",		-- 物品名:"红蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "那个蘑菇长得太大了。",		-- 物品名:"绿蘑菇树"->默认
            BLOOM = "闻之，令人作呕。",		-- 物品名:"绿蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_TALL_WEBBED = "你的审美挺不错。",		-- 物品名:"蛛网蓝蘑菇树"
        SPORE_TALL =
        {
            GENERIC = "它四处飘荡。",		-- 物品名:"蓝色孢子"->默认
            HELD = "妾身要在口袋里装一点光。",		-- 物品名:"蓝色孢子"->拿在手里
        },
        SPORE_MEDIUM =
        {
            GENERIC = "在这世上已了无牵挂。",		-- 物品名:"红色孢子"->默认
            HELD = "妾身要在口袋里装一点光。",		-- 物品名:"红色孢子"->拿在手里
        },
        SPORE_SMALL =
        {
            GENERIC = "那是孢子的眼睛看到的景象。",		-- 物品名:"绿色孢子"->默认
            HELD = "妾身要在口袋里装一点光。",		-- 物品名:"绿色孢子"->拿在手里
        },
        RABBITHOUSE =
        {
            GENERIC = "那可不是真的胡萝卜。",		-- 物品名:"兔屋"->默认 制造描述:"可容纳一只巨大的兔子及其所有物品。"
            BURNT = "那可不是真的烤胡萝卜。",		-- 物品名:"兔屋"->烧焦的 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        },
        SLURTLE = "呕...恶心...",		-- 物品名:"蛞蝓龟"
        SLURTLE_SHELLPIECES = "拼不好的拼图。",		-- 物品名:"壳碎片"
        SLURTLEHAT = "那会弄乱妾身的头发。",		-- 物品名:"背壳头盔"
        SLURTLEHOLE = "“恶心的”洞穴。",		-- 物品名:"蛞蝓龟窝"
        SLURTLESLIME = "要不是有用的话，妾身才不会去碰呢。",		-- 物品名:"蛞蝓龟黏液"
        SNURTLE = "他不算太恶心，但还是恶心。",		-- 物品名:"蜗牛龟"
        SPIDER_HIDER = "哇！妾身喜欢你坚硬的身体！",		-- 物品名:"洞穴蜘蛛"
        SPIDER_SPITTER = "妾身讨厌你胡乱喷射的蜘蛛液体！",		-- 物品名:"喷射蜘蛛"
        SPIDERHOLE = "它外面盖满了蛛网。",		-- 物品名:"蛛网岩"
        SPIDERHOLE_ROCK = "它外面盖满了蛛网。",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        STALAGMITE = "妾身看它就是块石头。",		-- 物品名:"石笋"
        STALAGMITE_TALL = "石头，石头，石头，石头...",		-- 物品名:"石笋"
        TURF_CARPETFLOOR = "非常扎人。",		-- 物品名:"地毯地板" 制造描述:"超级柔软。闻着就像皮弗娄牛。"
        TURF_CHECKERFLOOR = "这些非常华丽。",		-- 物品名:"棋盘地板" 制造描述:"精心制作成棋盘状的大理石地砖。"
        TURF_DIRT = "一块地皮。",		-- 物品名:"泥土地皮"
        TURF_FOREST = "一块地皮。",		-- 物品名:"森林地皮" 制造描述:"一块森林地皮。"
        TURF_GRASS = "一块地皮。",		-- 物品名:"长草地皮" 制造描述:"一片草地。"
        TURF_MARSH = "一块地皮。",		-- 物品名:"沼泽地皮" 制造描述:"沼泽在哪，家就在哪！"
        TURF_METEOR = "一块月球地面。",		-- 物品名:"月球环形山地皮" 制造描述:"月球表面的月坑。"
        TURF_PEBBLEBEACH = "一块海滩。",		-- 物品名:"岩石海滩地皮" 制造描述:"一块鹅卵石海滩地皮。"
        TURF_ROAD = "草草铺砌的石头。",		-- 物品名:"卵石路" 制造描述:"修建你自己的道路，通往任何地方。"
        TURF_ROCKY = "一块地皮。",		-- 物品名:"岩石地皮" 制造描述:"一块石头地皮。"
        TURF_SAVANNA = "一块地皮。",		-- 物品名:"热带草原地皮" 制造描述:"一块热带草原地皮。"
        TURF_WOODFLOOR = "这些是木地板。",		-- 物品名:"木地板" 制造描述:"优质复合地板。"
		TURF_CAVE="又一种地皮类型。",		-- 物品名:"鸟粪地皮" 制造描述:"洞穴地面冰冷的岩石。"
		TURF_FUNGUS="又一种地皮类型。",		-- 物品名:"菌类地皮" 制造描述:"一块长满了真菌的洞穴地皮。"
		TURF_FUNGUS_MOON = "又一种地皮类型。",		-- 物品名:"变异菌类地皮" 制造描述:"一块长满了变异真菌的洞穴地皮。"
		TURF_ARCHIVE = "又一种地皮类型。",		-- 物品名:"远古石刻"
		TURF_SINKHOLE="又一种地皮类型。",		-- 物品名:"黏滑地皮" 制造描述:"一块潮湿、沾满泥巴的草地地皮。"
		TURF_UNDERROCK="又一种地皮类型。",		-- 物品名:"洞穴岩石地皮" 制造描述:"一块乱石嶙峋的洞穴地皮。"
		TURF_MUD="又一种地皮类型。",		-- 物品名:"泥泞地皮" 制造描述:"一块泥地地皮。"
		TURF_DECIDUOUS = "又一种地皮类型。",		-- 物品名:"桦树地皮" 制造描述:"一块桦树森林地皮。"
		TURF_SANDY = "又一种地皮类型。",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_BADLANDS = "又一种地皮类型。",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_DESERTDIRT = "一块地皮。",		-- 物品名:"沙漠地皮" 制造描述:"一片干燥的沙子。"
		TURF_FUNGUS_GREEN = "一块地皮。",		-- 物品名:"菌类地皮" 制造描述:"一块爬满绿菌的洞穴地皮。"
		TURF_FUNGUS_RED = "一块地皮。",		-- 物品名:"菌类地皮" 制造描述:"一块爬满红菌的洞穴地皮。"
		TURF_DRAGONFLY = "你想证明它能防火吗？",		-- 物品名:"龙鳞地板" 制造描述:"消除火灾蔓延速度。"
        TURF_SHELLBEACH = "一块海滩。",		-- 物品名:"贝壳海滩地皮" 制造描述:"一块贝壳海岸。"
		POWCAKE = "来人帮帮妾身吧。",		-- 物品名:"芝士蛋糕"
        CAVE_ENTRANCE = "妾身在想那岩石能不能移开。",		-- 物品名:"被堵住的洞穴"
        CAVE_ENTRANCE_RUINS = "它有可能在隐瞒什么事情。",		-- 物品名:"被堵住的陷洞"->单机 洞二入口
       	CAVE_ENTRANCE_OPEN =
        {
            GENERIC = "它在排斥妾身！",		-- 物品名:"洞穴"->默认
            OPEN = "妾身敢打赌在那下面肯定能发现各种各样的东西。",		-- 物品名:"洞穴"->打开
            FULL = "妾身会等到有人离开了再进入。",		-- 物品名:"洞穴"->满了
        },
        CAVE_EXIT =
        {
            GENERIC = "妾身想妾身该待在这下面。",		-- 物品名:"楼梯"->默认
            OPEN = "妾身暂时不想再探险了。",		-- 物品名:"楼梯"->打开
            FULL = "上面太拥挤！",		-- 物品名:"楼梯"->满了
        },
		MAXWELLPHONOGRAPH = "音乐原来是从那来的。",--single player		-- 物品名:"麦斯威尔的留声机"->单机 麦斯威尔留声机
		BOOMERANG = "妾身还是更喜欢飞针！",		-- 物品名:"回旋镖" 制造描述:"来自澳洲土著。"
		PIGGUARD = "他看起来不那么友善。",		-- 物品名:"猪人守卫"
		ABIGAIL =
		{
            LEVEL1 =
            {
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
            },
            LEVEL2 =
            {
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
            },
            LEVEL3 =
            {
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
                "噢，她有一个可爱的小蝴蝶结。",		-- 物品名:未找到 制造描述:未找到
            },
		},
		ADVENTURE_PORTAL = "妾身不想再一次上当了。",		-- 物品名:"麦斯威尔之门"->单机 麦斯威尔之门
		AMULET = "轮回...重生...不再害怕。",		-- 物品名:"重生护符" 制造描述:"逃离死神的魔爪。"
		ANIMAL_TRACK = "妾身指的是...一只动物。",		-- 物品名:"动物足迹"
		ARMORGRASS = "还不如用蛛丝。",		-- 物品名:"草甲" 制造描述:"提供少许防护。"
		ARMORMARBLE = "那看起来真的很沉。",		-- 物品名:"大理石甲" 制造描述:"它很重，但能够保护你。"
		ARMORWOOD = "那是一件非常合适的衣服。",		-- 物品名:"木甲" 制造描述:"为你抵御部分伤害。"
		ARMOR_SANITY = "灵魂的残渣披在身上，倒也不错。",		-- 物品名:"魂甲" 制造描述:"保护你的躯体，但无法保护你的心智。"
		ASH =
		{
			GENERIC = "妾身讨厌看到灰烬，总是想起不好的事情。",		-- 物品名:"灰烬"->默认
		},
		AXE = "奴家举不动这个的。",		-- 物品名:"斧头" 制造描述:"砍倒树木！"
		BABYBEEFALO =
		{
			GENERIC = "啊。太可爱了！",		-- 物品名:"小皮弗娄牛"->默认
		    SLEEPING = "做个好梦，臭家伙。",		-- 物品名:"小皮弗娄牛"->睡着了
        },
        BUNDLE = "妾身的物资都在那里面！",		-- 物品名:"捆绑物资"
        BUNDLEWRAP = "把东西打包起来应该会容易搬一点。",		-- 物品名:"捆绑包装" 制造描述:"打包你的东西的部分和袋子。"
		BACKPACK = "妾身想要一个更轻便的行囊。",		-- 物品名:"背包" 制造描述:"携带更多物品。"
		BACONEGGS = "意外的美味。",		-- 物品名:"培根煎蛋"
		BANDAGE = "冰冰凉凉的，敷在伤口上。",		-- 物品名:"蜂蜜药膏" 制造描述:"愈合小伤口。"
		BASALT = "太硬了，敲不碎！", --removed		-- 物品名:"玄武岩"
		BEARDHAIR = "黑色卷卷的毛。",		-- 物品名:"胡子"
		BEARGER = "熊一样大的獾。",		-- 物品名:"熊獾"
		BEARGERVEST = "妾身想要一个熊皮大氅！",		-- 物品名:"熊皮背心" 制造描述:"熊皮背心。"
		ICEPACK = "毛皮使包内有保温效果。",		-- 物品名:"保鲜背包" 制造描述:"容量虽小，但能保持东西新鲜。"
		BEARGER_FUR = "一块厚毛皮。",		-- 物品名:"熊皮" 制造描述:"毛皮再生。"
		BEDROLL_STRAW = "休想让本尊睡这个。",		-- 物品名:"草席卷" 制造描述:"一觉睡到天亮。"
		BEEQUEEN = "滚开，不然把你的刺拔下来！",		-- 物品名:"蜂王"
		BEEQUEENHIVE =
		{
			GENERIC = "太黏了，没法走下去了。",		-- 物品名:"蜂蜜地块"->默认
			GROWING = "那东西以前在那里吗？",		-- 物品名:"蜂蜜地块"->正在生长
		},
        BEEQUEENHIVEGROWN = "它是怎么长到这么大的？！",		-- 物品名:"巨大蜂窝"
        BEEGUARD = "它正在守卫女王。",		-- 物品名:"嗡嗡蜜蜂"
        HIVEHAT = "可真是滑稽的小帽子。",		-- 物品名:"蜂王冠"
        MINISIGN =
        {
            GENERIC = "妾身可以画得比那好！",		-- 物品名:"小木牌"->默认
            UNDRAWN = "妾身应该在那上面画些东西。",		-- 物品名:"小木牌"->没有画画
        },
        MINISIGN_ITEM = "像这样没有太多用处。妾身应该把它放下。",		-- 物品名:"小木牌" 制造描述:"用羽毛笔在这些上面画画。"
		BEE =
		{
			GENERIC = "你们可真可爱。",		-- 物品名:"蜜蜂"->默认
			HELD = "你要来当我的干儿子吗？",		-- 物品名:"蜜蜂"->拿在手里
		},
		BEEBOX =
		{
			READY = "它里面装满了蜂蜜。",		-- 物品名:"蜂箱"->准备好的 满的 制造描述:"贮存你自己的蜜蜂。"
			FULLHONEY = "蜜和蜜蜂可以兼得。",		-- 物品名:"蜂箱"->蜂蜜满了 制造描述:"贮存你自己的蜜蜂。"
			GENERIC = "我的晚餐在里面。",		-- 物品名:"蜂箱"->默认 制造描述:"贮存你自己的蜜蜂。"
			NOHONEY = "不会产蜜的蜜蜂也不需要再留了。",		-- 物品名:"蜂箱"->没有蜂蜜 制造描述:"贮存你自己的蜜蜂。"
			SOMEHONEY = "需要等一会。",		-- 物品名:"蜂箱"->有一些蜂蜜 制造描述:"贮存你自己的蜜蜂。"
			BURNT = "怎么被烧掉的？！！",		-- 物品名:"蜂箱"->烧焦的 制造描述:"贮存你自己的蜜蜂。"
		},
		MUSHROOM_FARM =
		{
			STUFFED = "那真是许许多多的蘑菇！",		-- 物品名:"蘑菇农场"->塞，满了？？ 制造描述:"种蘑菇。"
			LOTS = "木头上长满了蘑菇。",		-- 物品名:"蘑菇农场"->很多 制造描述:"种蘑菇。"
			SOME = "现在它应该继续生长。",		-- 物品名:"蘑菇农场"->有一些 制造描述:"种蘑菇。"
			EMPTY = "它可以使用一个孢子。或是一个蘑菇移植植物。",		-- 物品名:"蘑菇农场" 制造描述:"种蘑菇。"
			ROTTEN = "木头死亡了。妾身应该用一个活的来替换它。",		-- 物品名:"蘑菇农场"->腐烂的--需要活木 制造描述:"种蘑菇。"
			BURNT = "自然的力量催生了它。",		-- 物品名:"蘑菇农场"->烧焦的 制造描述:"种蘑菇。"
			SNOWCOVERED = "妾身认为它在这种寒冷的天气里没办法生长。",		-- 物品名:"蘑菇农场"->被雪覆盖 制造描述:"种蘑菇。"
		},
		BEEFALO =
		{
			FOLLOWER = "妾身的仆人在静静地跟着。",		-- 物品名:"皮弗娄牛"->追随者
			GENERIC = "那是一只皮弗娄牛！",		-- 物品名:"皮弗娄牛"->默认
			NAKED = "呃，他很伤心。",		-- 物品名:"皮弗娄牛"->牛毛被刮干净了
			SLEEPING = "这些家伙睡得真死。",		-- 物品名:"皮弗娄牛"->睡着了
            DOMESTICATED = "能当本座的坐骑是你的荣幸。",		-- 物品名:"皮弗娄牛"->驯化牛
            ORNERY = "它看起来非常生气。",		-- 物品名:"皮弗娄牛"->战斗牛
            RIDER = "这家伙看起来挺好骑的。",		-- 物品名:"皮弗娄牛"->骑行牛
            PUDGY = "嗯，它可能吃太多东西了。",		-- 物品名:"皮弗娄牛"->胖牛
            MYPARTNER = "永远都是朋友。",		-- 物品名:"皮弗娄牛"
		},
		BEEFALOHAT = "妾身不要被弄乱秀发。",		-- 物品名:"牛角帽" 制造描述:"成为牛群中的一员！连气味也变得一样。"
		BEEFALOWOOL = "闻起来不太妙。",		-- 物品名:"牛毛"
		BEEHAT = "我可不怕什么虫子。",		-- 物品名:"养蜂帽" 制造描述:"防止被愤怒的蜜蜂蜇伤。"
        BEESWAX = "蜂蜡可以防腐！",		-- 物品名:"蜂蜡" 制造描述:"一种有用的防腐蜂蜡。"
		BEEHIVE = "我的晚餐嗡嗡忙不停。",		-- 物品名:"蜂窝"
		BEEMINE = "它晃动时就会嗡嗡叫。",		-- 物品名:"蜜蜂地雷" 制造描述:"变成武器的蜜蜂。会出什么问题？"
		BEEMINE_MAXWELL = "被装在地雷里的狂暴蚊子！",--removed		-- 物品名:"麦斯威尔的蚊子陷阱"->单机 麦斯威尔的蚊子陷阱
		BERRIES = "若是...还是来点解解腻吧。",		-- 物品名:"浆果"
		BERRIES_COOKED = "妾身认为加热并没有让它们变得更好。",		-- 物品名:"烤浆果"
        BERRIES_JUICY = "美味稍纵即逝。",		-- 物品名:"多汁浆果"
        BERRIES_JUICY_COOKED = "时间会很快带走它！",		-- 物品名:"烤多汁浆果"
		BERRYBUSH =
		{
			BARREN = "妾身想它需要施肥。",		-- 物品名:"浆果丛"
			WITHERED = "这炎炎夏日，唉。",		-- 物品名:"浆果丛"->枯萎了
			GENERIC = "妾身觉得这些应该能吃。",		-- 物品名:"浆果丛"->默认
			PICKED = "或许它们会再长出来？",		-- 物品名:"浆果丛"->被采完了
			DISEASED = "看上去病的很重。",--removed		-- 物品名:"浆果丛"->生病了
			DISEASING = "呃...有些地方不对劲。",--removed		-- 物品名:"浆果丛"->正在生病？？
			BURNING = "它大部分都着火了。",		-- 物品名:"浆果丛"->正在燃烧
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "若不施肥，它长不出浆果。",		-- 物品名:"多汁浆果丛"
			WITHERED = "高温将多汁的浆果脱水！",		-- 物品名:"多汁浆果丛"->枯萎了
			GENERIC = "妾身最近吃腻了。",		-- 物品名:"多汁浆果丛"->默认
			PICKED = "下一批丛林在努力生长。",		-- 物品名:"多汁浆果丛"->被采完了
			DISEASED = "它看上去很不舒服。",--removed		-- 物品名:"多汁浆果丛"->生病了
			DISEASING = "呃...有些地方不对劲。",--removed		-- 物品名:"多汁浆果丛"->正在生病？？
			BURNING = "它大部分都着火了。",		-- 物品名:"多汁浆果丛"->正在燃烧
		},
		BIRDCAGE =
		{
			GENERIC = "笼中雀，该抓谁好呢。",		-- 物品名:"鸟笼"->默认 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			OCCUPIED = "位置不够可怎么办？",		-- 物品名:"鸟笼"->被占领 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			SLEEPING = "嘘，他睡着了。",		-- 物品名:"鸟笼"->睡着了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			HUNGRY = "它看起来有点饿。",		-- 物品名:"鸟笼"->有点饿了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			STARVING = "饿的都瘦了。",		-- 物品名:"鸟笼"->挨饿 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			DEAD = "是饿死的。",		-- 物品名:"鸟笼"->死了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			SKELETON = "只是一具普通的尸骨？",		-- 物品名:"骷髅"
		},
		BIRDTRAP = "经纬智何多，网张犹设罗。",		-- 物品名:"捕鸟陷阱" 制造描述:"捕捉会飞的动物。"
		CAVE_BANANA_BURNT = "不是妾身的错！",		-- 物品名:"鸟笼"->烧焦的洞穴香蕉树 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BIRD_EGG = "一颗小小的鸟蛋。",		-- 物品名:"鸟蛋"
		BIRD_EGG_COOKED = "真好吃！",		-- 物品名:"煎蛋"
		BISHOP = "退后，你这该死的怪物！",		-- 物品名:"发条主教"
		BLOWDART_FIRE = "妾身的毒针要更好用。",		-- 物品名:"火焰吹箭" 制造描述:"向你的敌人喷火。"
		BLOWDART_SLEEP = "妾身的毒针要更好用。",		-- 物品名:"催眠吹箭" 制造描述:"催眠你的敌人。"
		BLOWDART_PIPE = "妾身的毒针要更好用！",		-- 物品名:"吹箭" 制造描述:"向你的敌人喷射利齿。"
		BLOWDART_YELLOW = "妾身的毒针要更好用。",		-- 物品名:"雷电吹箭" 制造描述:"朝你的敌人放闪电。"
		BLUEAMULET = "酷暑的天气多给本座送一些来！",		-- 物品名:"寒冰护符" 制造描述:"多么冰冷酷炫的护符。"
		BLUEGEM = "寒冰的力量凝聚其中。",		-- 物品名:"蓝宝石"
		BLUEPRINT =
		{
            COMMON = "记录知识的图纸！",		-- 物品名:"蓝图"
            RARE = "太精妙了！",		-- 物品名:"蓝图"->罕见的
        },
        SKETCH = "一张雕像的图片。",		-- 物品名:"{item}草图"
		BLUE_CAP = "它很古怪，还黏黏的。",		-- 物品名:"采摘的蓝蘑菇"
		BLUE_CAP_COOKED = "这回不一样了…",		-- 物品名:"烤蓝蘑菇"
		BLUE_MUSHROOM =
		{
			GENERIC = "是蘑菇。",		-- 物品名:"蓝蘑菇"->默认
			INGROUND = "它在睡觉。",		-- 物品名:"蓝蘑菇"->在地里面
			PICKED = "不知道它会不会长回来？",		-- 物品名:"蓝蘑菇"->被采完了
		},
		BOARDS = "木板。",		-- 物品名:"木板" 制造描述:"更平直的木头。"
		BONESHARD = "一小片骨头。",		-- 物品名:"骨头碎片"
		BONESTEW = "东北大锅炖。",		-- 物品名:"炖肉汤"
		BUGNET = "我需要这个吗？你简直是在开玩笑。",		-- 物品名:"捕虫网" 制造描述:"抓虫子用的。"
		BUSHHAT = "看着有点扎人。",		-- 物品名:"灌木丛帽" 制造描述:"很好的伪装！"
		BUTTER = "让妾身尝尝西洋口味！",		-- 物品名:"黄油"
		BUTTERFLY =
		{
			GENERIC = "忆江南蝶，斜日纷飞一双双。",		-- 物品名:"蝴蝶"->默认
			HELD = "你只能在妾身的掌心飞舞！",		-- 物品名:"蝴蝶"->拿在手里
		},
		BUTTERFLYMUFFIN = "有只蝴蝶落在了妾身的盘子上。",		-- 物品名:"蝴蝶松饼"
		BUTTERFLYWINGS = "你不用再为花忙了。",		-- 物品名:"蝴蝶翅膀"
		BUZZARD = "多么丑陋的一只秃鹫！",		-- 物品名:"秃鹫"
		SHADOWDIGGER = "思绪随着美梦飞舞。",		-- 物品名:"蝴蝶"
		CACTUS =
		{
			GENERIC = "快拿开。",		-- 物品名:"仙人掌"->默认
			PICKED = "再过几天吧。",		-- 物品名:"仙人掌"->被采完了
		},
		CACTUS_MEAT_COOKED = "来自沙漠的美食。",		-- 物品名:"烤仙人掌肉"
		CACTUS_MEAT = "啧啧，你不会想让妾身吃那个吧。",		-- 物品名:"仙人掌肉"
		CACTUS_FLOWER = "沙漠之地的美丽花朵。",		-- 物品名:"仙人掌花"
		COLDFIRE =
		{
			EMBERS = "得加燃料了，或者就要灭了。",		-- 物品名:"吸热营火"->即将熄灭 制造描述:"这火是越烤越冷的冰火。"
			GENERIC = "肯定能驱走黑暗。",		-- 物品名:"吸热营火"->默认 制造描述:"这火是越烤越冷的冰火。"
			HIGH = "火要失控了！",		-- 物品名:"吸热营火"->高 制造描述:"这火是越烤越冷的冰火。"
			LOW = "火变得有点小了。",		-- 物品名:"吸热营火"->低？ 制造描述:"这火是越烤越冷的冰火。"
			NORMAL = "妾身可不能考的太近。",		-- 物品名:"吸热营火"->普通 制造描述:"这火是越烤越冷的冰火。"
			OUT = "哦，火灭了。",		-- 物品名:"吸热营火"->出去？外面？ 制造描述:"这火是越烤越冷的冰火。"
		},
		CAMPFIRE =
		{
			EMBERS = "得加燃料了，不然火就要灭了。",		-- 物品名:"营火"->即将熄灭 制造描述:"燃烧时发出光亮。"
			GENERIC = "肯定能驱走黑暗。",		-- 物品名:"营火"->默认 制造描述:"燃烧时发出光亮。"
			HIGH = "火要失控了！",		-- 物品名:"营火"->高 制造描述:"燃烧时发出光亮。"
			LOW = "火变得有点小了。",		-- 物品名:"营火"->低？ 制造描述:"燃烧时发出光亮。"
			NORMAL = "妾身可不能靠的太近。",		-- 物品名:"营火"->普通 制造描述:"燃烧时发出光亮。"
			OUT = "哦，火灭了。",		-- 物品名:"营火"->出去？外面？ 制造描述:"燃烧时发出光亮。"
		},
		CANE = "我还没老到那个地步。",		-- 物品名:"步行手杖" 制造描述:"泰然自若地快步走。"
		CATCOON = "顽皮的小东西。",		-- 物品名:"浣猫"
		CATCOONDEN =
		{
			GENERIC = "树桩里的窝。",		-- 物品名:"空心树桩"->默认
			EMPTY = "它的主人没命了。",		-- 物品名:"空心树桩"
		},
		CATCOONHAT = "遮住耳朵的帽子！",		-- 物品名:"猫帽" 制造描述:"适合那些重视温暖甚于朋友的人。"
		COONTAIL = "妾身觉得它还在摆动。",		-- 物品名:"猫尾"
		CARROT = "再熟一点就炸了。",		-- 物品名:"胡萝卜"
		CARROT_COOKED = "软塌塌的。",		-- 物品名:"烤胡萝卜"
		CARROT_PLANTED = "你该起床喽。",		-- 物品名:"胡萝卜"
		CARROT_SEEDS = "这是一颗种子。",		-- 物品名:"椭圆形种子"
		CARTOGRAPHYDESK =
		{
			GENERIC = "妾身要大显身手了！",		-- 物品名:"制图桌"->默认 制造描述:"准确地告诉别人你去过哪里。"
			BURNING = "那件事到此为止。",		-- 物品名:"制图桌"->正在燃烧 制造描述:"准确地告诉别人你去过哪里。"
			BURNT = "现在就只有灰烬了。",		-- 物品名:"制图桌"->烧焦的 制造描述:"准确地告诉别人你去过哪里。"
		},
		WATERMELON_SEEDS = "这是一颗种子。",		-- 物品名:"方形种子"
		CAVE_FERN = "它是一种蕨类植物。",		-- 物品名:"蕨类植物"
		CHARCOAL = "又小又黑，闻起来像烧焦的木头。",		-- 物品名:"木炭"
        CHESSPIECE_PAWN = "妾身有同感。",		-- 物品名:"卒子雕塑"
        CHESSPIECE_ROOK =
        {
            GENERIC = "它比看上去的更重。",		-- 物品名:"战车雕塑"->默认
            STRUGGLE = "棋子们自己在动！",		-- 物品名:"战车雕塑"->三基佬棋子晃动
        },
        CHESSPIECE_KNIGHT =
        {
            GENERIC = "它是匹马，当然。",		-- 物品名:"骑士雕塑"->默认
            STRUGGLE = "棋子们自己在动！",		-- 物品名:"骑士雕塑"->三基佬棋子晃动
        },
        CHESSPIECE_BISHOP =
        {
            GENERIC = "是个石头传教士。",		-- 物品名:"主教雕塑"->默认
            STRUGGLE = "棋子们自己在动！",		-- 物品名:"主教雕塑"->三基佬棋子晃动
        },
        CHESSPIECE_MUSE = "嗯...是我的雕像吗。",		-- 物品名:"女王雕塑"
        CHESSPIECE_FORMAL = "对妾身来说，看起来没有非常“高贵”。",		-- 物品名:"国王雕塑"
        CHESSPIECE_HORNUCOPIA = "仅仅看着它就让妾身想到了丰收。",		-- 物品名:"丰饶角雕塑"
        CHESSPIECE_PIPE = "那从来不是妾身的菜。",		-- 物品名:"泡泡烟斗雕塑"
        CHESSPIECE_DEERCLOPS = "感觉它的目光在跟着你。",		-- 物品名:"独眼巨鹿雕塑"
        CHESSPIECE_BEARGER = "靠近后它大得多了。",		-- 物品名:"熊獾雕塑"
        CHESSPIECE_MOOSEGOOSE =
        {
            "呃。这太逼真了。",		-- 物品名:"麋鹿鹅雕塑" 制造描述:未找到
        },
        CHESSPIECE_DRAGONFLY = "啊，这让妾身想起一些事情。不好的记忆。",		-- 物品名:"龙蝇雕塑"
		CHESSPIECE_MINOTAUR = "现在轮到你被吓得一动不动了！",		-- 物品名:"远古守护者雕塑"
        CHESSJUNK1 = "一堆烂发条装置。",		-- 物品名:"损坏的发条装置"
        CHESSJUNK2 = "另一堆烂发条装置。",		-- 物品名:"损坏的发条装置"
        CHESSJUNK3 = "更多的烂发条装置。",		-- 物品名:"损坏的发条装置"
		CHESTER = "哦哦，我的小仆人，到妾身怀里来。",		-- 物品名:"切斯特"
		CHESTER_EYEBONE =
		{
			GENERIC = "它在看着妾身。",		-- 物品名:"眼骨"->默认
			WAITING = "它睡着了。",		-- 物品名:"眼骨"->需要等待
		},
		COOKEDMANDRAKE = "可怜的小家伙。",		-- 物品名:"烤曼德拉草"
		COOKEDMEAT = "碳烤熟透。",		-- 物品名:"烤大肉"
		COOKEDMONSTERMEAT = "还不如生食呢。",		-- 物品名:"烤怪物肉"
		COOKEDSMALLMEAT = "啧啧，还是给孩儿们吃吧！",		-- 物品名:"烤小肉"
		COOKPOT =
		{
			COOKING_LONG = "这还需要一点时间。",		-- 物品名:"烹饪锅"->饭还需要很久 制造描述:"制作更好的食物。"
			COOKING_SHORT = "就快好了！",		-- 物品名:"烹饪锅"->饭快做好了 制造描述:"制作更好的食物。"
			DONE = "嗯！可以吃了！",		-- 物品名:"烹饪锅"->完成了 制造描述:"制作更好的食物。"
			EMPTY = "看着都觉得饿。",		-- 物品名:"烹饪锅" 制造描述:"制作更好的食物。"
			BURNT = "锅给烧没了。",		-- 物品名:"烹饪锅"->烧焦的 制造描述:"制作更好的食物。"
		},
		CORN = "多好的玉米棒子！",		-- 物品名:"玉米"
		CORN_COOKED = "别有一番风味！",		-- 物品名:"爆米花"
		CORN_SEEDS = "这是一颗种子。",		-- 物品名:"簇状种子"
        CANARY =
		{
			GENERIC = "黄鹂是你的亲戚吗？",		-- 物品名:"金丝雀"->默认
			HELD = "顿网轻摇试有禽，不是吗？",		-- 物品名:"金丝雀"->拿在手里
		},
        CANARY_POISONED = "它可能是好的。",		-- 物品名:"金丝雀（中毒）"
		CRITTERLAB = "那里面有什么东西吗？",		-- 物品名:"岩石巢穴"
        CRITTER_GLOMLING = "多么可爱的小绒球！",		-- 物品名:"小格罗姆"
        CRITTER_DRAGONLING = "它爬进了妾身的心里。",		-- 物品名:"小龙蝇"
		CRITTER_LAMB = "在长大一点，烤羊腿有希望了。",		-- 物品名:"小钢羊"
        CRITTER_PUPPY = "对一只小怪兽来说真是太可爱了！",		-- 物品名:"小座狼"
        CRITTER_KITTEN = "你会成为一位很棒的手下。",		-- 物品名:"小浣猫"
        CRITTER_PERDLING = "妾身明天想吃两个鸡蛋，没有的话...",		-- 物品名:"小火鸡"
		CRITTER_LUNARMOTHLING = "留着她是因为她有价值。",		-- 物品名:"小蛾子"
		CROW =
		{
			GENERIC = "倦倚阑干小院东，织网惊雀堕虚空！",		-- 物品名:"乌鸦"->默认
			HELD = "你落在我的手中了。",		-- 物品名:"乌鸦"->拿在手里
		},
		CUTGRASS = "妾身最擅长编织了。",		-- 物品名:"采下的草"
		CUTREEDS = "穿丝引线。",		-- 物品名:"采下的芦苇"
		CUTSTONE = "光滑诱人。",		-- 物品名:"石砖" 制造描述:"切成正方形的石头。"
		DEER =
		{
			GENERIC = "它是不是盯着妾身？",		-- 物品名:"无眼鹿"->默认
			ANTLER = "多么引人注目的一个鹿角啊！",		-- 物品名:"无眼鹿"
		},
        DEER_ANTLER = "掉下来正常吗？",		-- 物品名:"鹿角"
        DEER_GEMMED = "它被那头野兽控制着！",		-- 物品名:"无眼鹿"
		DEERCLOPS = "它体型真大！！",		-- 物品名:"独眼巨鹿"
		DEERCLOPS_EYEBALL = "这真的很恶心。",		-- 物品名:"独眼巨鹿眼球"
		EYEBRELLAHAT =	"它会守护佩戴人。",		-- 物品名:"眼球伞" 制造描述:"面向天空的眼球让你保持干燥。"
		DEPLETED_GRASS =
		{
			GENERIC = "还没我的丝韧呢。",		-- 物品名:"草"->默认
		},
        GOGGLESHAT = "这对护目镜可真时髦。",		-- 物品名:"时髦的护目镜" 制造描述:"你可以瞪大眼睛看装饰性护目镜。"
        DESERTHAT = "优质护目用具。",		-- 物品名:"沙漠护目镜" 制造描述:"从你的眼睛里把沙子揉出来。"
		DEVTOOL = "闻起来像培根！",		-- 物品名:"开发工具"
		DEVTOOL_NODEV = "妾身可以采下来做点东西。",		-- 物品名:"草"
		DIRTPILE = "这是一堆土...还是？",		-- 物品名:"可疑的土堆"
		DIVININGROD =
		{
			COLD = "信号很弱。", --singleplayer		-- 物品名:"冻伤"->冷了
			GENERIC = "它是某种自动引导装置。", --singleplayer		-- 物品名:"探测杖"->默认 制造描述:"肯定有方法可以离开这里..."
			HOT = "这东西发疯了！", --singleplayer		-- 物品名:"中暑"->热了
			WARM = "妾身在正确的方向上。", --singleplayer		-- 物品名:"探测杖"->温暖 制造描述:"肯定有方法可以离开这里..."
			WARMER = "肯定很接近了。", --singleplayer		-- 物品名:"探测杖" 制造描述:"肯定有方法可以离开这里..."
		},
		DIVININGRODBASE =
		{
			GENERIC = "妾身想知道它有什么用。", --singleplayer		-- 物品名:"探测杖底座"->默认
			READY = "看起来它需要一把大钥匙。", --singleplayer		-- 物品名:"探测杖底座"->准备好的 满的
			UNLOCKED = "现在机器可以工作了！", --singleplayer		-- 物品名:"探测杖底座"->解锁了
		},
		DIVININGRODSTART = "那根手杖看起来很有用！", --singleplayer		-- 物品名:"探测杖底座"->单机探测杖底座
		DRAGONFLY = "那是一只龙蝇！是龙还是蝇？",		-- 物品名:"龙蝇"
		ARMORDRAGONFLY = "热呼呼的盔甲！",		-- 物品名:"鳞甲" 制造描述:"脾气火爆的盔甲。"
		DRAGON_SCALES = "还是热的。",		-- 物品名:"鳞片"
		DRAGONFLYCHEST = "仅次于保险箱！",		-- 物品名:"龙鳞宝箱" 制造描述:"一种结实且防火的容器。"
		DRAGONFLYFURNACE =
		{
			HAMMERED = "看起来不太对啊。",		-- 物品名:"龙鳞火炉"->被锤了 制造描述:"给自己建造一个苍蝇暖炉。"
			GENERIC = "产生了大量的热量，但亮光却不多。", --no gems		-- 物品名:"龙鳞火炉"->默认 制造描述:"给自己建造一个苍蝇暖炉。"
			NORMAL = "它是在朝妾身眨眼吗？", --one gem		-- 物品名:"龙鳞火炉"->普通 制造描述:"给自己建造一个苍蝇暖炉。"
			HIGH = "滚烫！", --two gems		-- 物品名:"龙鳞火炉"->高 制造描述:"给自己建造一个苍蝇暖炉。"
		},
        HUTCH = "哈奇蛋戈鱼，P.I.",		-- 物品名:"哈奇"
        HUTCH_FISHBOWL =
        {
            GENERIC = "妾身一直想要这个。",		-- 物品名:"星空"->默认
            WAITING = "或许他需要点时间？",		-- 物品名:"星空"->需要等待
        },
		LAVASPIT =
		{
			HOT = "好烫的口水！",		-- 物品名:"中暑"->热了
			COOL = "妾身可不喜欢你。",		-- 物品名:"龙蝇唾液"
		},
		LAVA_POND = "壮观！",		-- 物品名:"岩浆池"
		LAVAE = "太烫了，没办法吃。",		-- 物品名:"岩浆虫"
		LAVAE_COCOON = "冷却下来，平静下来。",		-- 物品名:"冷冻虫卵"
		LAVAE_PET =
		{
			STARVING = "可怜的东西一定很饿。",		-- 物品名:"超级可爱岩浆虫"->挨饿
			HUNGRY = "妾身听到它的小肚子在咕咕叫。",		-- 物品名:"超级可爱岩浆虫"->有点饿了
			CONTENT = "它似乎很快乐。",		-- 物品名:"超级可爱岩浆虫"->内容？？？、
			GENERIC = "啊。越看越饿。",		-- 物品名:"超级可爱岩浆虫"->默认
		},
		LAVAE_EGG =
		{
			GENERIC = "里面传来微弱的暖意。",		-- 物品名:"岩浆虫卵"->默认
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "妾身觉得蛋不够热。",		-- 物品名:"冻伤"->冷了
			COMFY = "妾身从未想过妾身会见到一只高兴的蛋。",		-- 物品名:"岩浆虫卵"->舒服的
		},
		LAVAE_TOOTH = "一颗蛋牙！",		-- 物品名:"岩浆虫尖牙"
		DRAGONFRUIT = "好奇怪的水果。",		-- 物品名:"火龙果"
		DRAGONFRUIT_COOKED = "这水果还是很怪。",		-- 物品名:"烤火龙果"
		DRAGONFRUIT_SEEDS = "这是一颗种子。",		-- 物品名:"球茎状种子"
		DRAGONPIE = "那个火龙果非常耐饱。",		-- 物品名:"火龙果派"
		DRUMSTICK = "准备好狼吞虎咽了。",		-- 物品名:"鸟腿"
		DRUMSTICK_COOKED = "比狼吞虎咽还更好！",		-- 物品名:"炸鸟腿"
		DUG_BERRYBUSH = "现在可以把它带到任何地方了。",		-- 物品名:"浆果丛"
		DUG_BERRYBUSH_JUICY = "这树可让妾身的小花园多姿多彩。",		-- 物品名:"多汁浆果丛"
		DUG_GRASS = "哪里都可以种这种植物。",		-- 物品名:"草丛"
		DUG_MARSH_BUSH = "这需要种植。",		-- 物品名:"尖刺灌木"
		DUG_SAPLING = "这需要种下去。",		-- 物品名:"树苗"
		DURIAN = "呃，真臭！",		-- 物品名:"榴莲"
		DURIAN_COOKED = "现在它闻起来更臭了！",		-- 物品名:"超臭榴莲"
		DURIAN_SEEDS = "这是一颗种子。",		-- 物品名:"脆籽荚"
		EARMUFFSHAT = "温暖你内外。",		-- 物品名:"兔耳罩" 制造描述:"毛茸茸的保暖物品。"
		EGGPLANT = "它看起来不像一颗蛋。",		-- 物品名:"茄子"
		EGGPLANT_COOKED = "现在的它更不像蛋了。",		-- 物品名:"烤茄子"
		EGGPLANT_SEEDS = "这是一颗种子。",		-- 物品名:"漩涡种子"
		ENDTABLE =
		{
			BURNT = "一个放在烧焦的桌子上的烧焦的花瓶。",		-- 物品名:"茶几"->烧焦的 制造描述:"一张装饰桌。"
			GENERIC = "桌上花瓶里的一朵花。",		-- 物品名:"茶几"->默认 制造描述:"一张装饰桌。"
			EMPTY = "妾身应该把一些东西放进那里。",		-- 物品名:"茶几" 制造描述:"一张装饰桌。"
			WILTED = "看起来不太新鲜。",		-- 物品名:"茶几"->枯萎的 制造描述:"一张装饰桌。"
			FRESHLIGHT = "有一点亮光真是好。",		-- 物品名:"茶几"->茶几-新的发光的 制造描述:"一张装饰桌。"
			OLDLIGHT = "妾身没忘记换新鲜的花吧？", -- will be wilted soon, light radius will be very small at this point		-- 物品名:"茶几"->茶几-旧的发光的 制造描述:"一张装饰桌。"
		},
		DECIDUOUSTREE =
		{
			BURNING = "真是浪费木材。",		-- 物品名:"桦栗树"->正在燃烧
			BURNT = "火焰总是无情。",		-- 物品名:"桦栗树"->烧焦的
			CHOPPED = "斩草须除根！",		-- 物品名:"桦栗树"->被砍了
			POISON = "看来偷桦栗果惹火了它！",		-- 物品名:"桦栗树"->毒化的
			GENERIC = "阔叶纷纷，枝丫满尘。",		-- 物品名:"桦栗树"->默认
		},
		ACORN = "生命在其中跳动。",		-- 物品名:"桦栗果"
        ACORN_SAPLING = "它很快将变成参天大树！",		-- 物品名:"桦栗树树苗"
		ACORN_COOKED = "倒是别有一番风味。",		-- 物品名:"烤桦栗果"
		BIRCHNUTDRAKE = "疯狂的小坚果。",		-- 物品名:"桦栗果精"
		EVERGREEN =
		{
			BURNING = "真是浪费木材。",		-- 物品名:"常青树"->正在燃烧
			BURNT = "火焰总是无情。",		-- 物品名:"常青树"->烧焦的
			CHOPPED = "斩草须除根！",		-- 物品名:"常青树"->被砍了
			GENERIC = "为草当作兰，为木当作松。",		-- 物品名:"常青树"->默认
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "真是浪费木材。",		-- 物品名:"粗壮常青树"->正在燃烧
			BURNT = "火焰总是无情。",		-- 物品名:"粗壮常青树"->烧焦的
			CHOPPED = "斩草须除根！",		-- 物品名:"粗壮常青树"->被砍了
			GENERIC = "松香青古淡雅人。",		-- 物品名:"粗壮常青树"->默认
		},
		TWIGGYTREE =
		{
			BURNING = "真是浪费木材。",		-- 物品名:"多枝树"->正在燃烧
			BURNT = "火焰总是无情。",		-- 物品名:"多枝树"->烧焦的
			CHOPPED = "斩草须除根！",		-- 物品名:"多枝树"->被砍了
			GENERIC = "满是树枝。",		-- 物品名:"多枝树"->默认
			DISEASED = "它看起来很糟糕。", --unimplemented		-- 物品名:"多枝树"->生病了
		},
		TWIGGY_NUT_SAPLING = "它生长不需要任何助力。",		-- 物品名:"多枝树苗"
        TWIGGY_OLD = "那棵树看起来很弱。",		-- 物品名:"多枝树"
		TWIGGY_NUT = "它里面有一棵想要出去的多枝树。",		-- 物品名:"多枝树种"->多枝树果
		EYEPLANT = "妾身会觉得羞涩的。",		-- 物品名:"眼球草"
		INSPECTSELF = "妾身没缺胳膊少腿吧？",		-- 物品名:"多枝树"->检查自己
		FARMPLOT =
		{
			GENERIC = "妾身应该试着种一些庄稼。",		-- 物品名:未找到
			GROWING = "长啊，植物，长啊！",		-- 物品名:未找到
			NEEDSFERTILIZER = "应该需要施肥。",		-- 物品名:未找到
			BURNT = "灰烬中长不出庄稼。",		-- 物品名:未找到
		},
		FEATHERHAT = "变成鸟！",		-- 物品名:"羽毛帽" 制造描述:"头上的装饰。"
		FEATHER_CROW = "黑鸟的羽毛。",		-- 物品名:"黑色羽毛"
		FEATHER_ROBIN = "红雀的羽毛。",		-- 物品名:"红色羽毛"
		FEATHER_ROBIN_WINTER = "雪雀的羽毛。",		-- 物品名:"蔚蓝羽毛"
		FEATHER_CANARY = "金丝雀的羽毛。",		-- 物品名:"黄色羽毛"
		FEATHERPENCIL = "也许毛笔更适合我。",		-- 物品名:"羽毛笔" 制造描述:"是的，羽毛是必须的。"
        COOKBOOK = "妾身可不会洗手做汤羹。",		-- 物品名:"烹饪指南" 制造描述:"查看你收集的食谱。"
		FEM_PUPPET = "她被困住了！", --single player		-- 物品名:未找到
		FIREFLIES =
		{
			GENERIC = "妾身的家里还缺一盏明灯！",		-- 物品名:"萤火虫"->默认
			HELD = "亮堂堂的屋子就靠你了！",		-- 物品名:"萤火虫"->拿在手里
		},
		FIREHOUND = "那东西在发红光。",		-- 物品名:"红色猎犬"
		FIREPIT =
		{
			EMBERS = "在火熄灭之前妾身得加点燃料。",		-- 物品名:"火坑"->即将熄灭 制造描述:"一种更安全、更高效的营火。"
			GENERIC = "肯定能驱走黑暗。",		-- 物品名:"火坑"->默认 制造描述:"一种更安全、更高效的营火。"
			HIGH = "好在火被围起来了！",		-- 物品名:"火坑"->高 制造描述:"一种更安全、更高效的营火。"
			LOW = "火变得有点小了。",		-- 物品名:"火坑"->低？ 制造描述:"一种更安全、更高效的营火。"
			NORMAL = "妾身可不能考的太近。",		-- 物品名:"火坑"->普通 制造描述:"一种更安全、更高效的营火。"
			OUT = "至少妾身能让它再烧起来。",		-- 物品名:"火坑"->出去？外面？ 制造描述:"一种更安全、更高效的营火。"
		},
		COLDFIREPIT =
		{
			EMBERS = "在火熄灭之前妾身得加点燃料。",		-- 物品名:"吸热火坑"->即将熄灭 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			GENERIC = "肯定能驱走黑暗。",		-- 物品名:"吸热火坑"->默认 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			HIGH = "好在火被围起来了！",		-- 物品名:"吸热火坑"->高 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			LOW = "火变得有点小了。",		-- 物品名:"吸热火坑"->低？ 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			NORMAL = "妾身可不能考的太近。",		-- 物品名:"吸热火坑"->普通 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			OUT = "至少妾身能让它再烧起来。",		-- 物品名:"吸热火坑"->出去？外面？ 制造描述:"燃烧效率更高，但仍然越烤越冷。"
		},
		FIRESTAFF = "火焰在我手中翻腾。",		-- 物品名:"火魔杖" 制造描述:"利用火焰的力量！"
		FIRESUPPRESSOR =
		{
			ON = "开始灭火！",		-- 物品名:"雪球发射器"->开启 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
			OFF = "一切都平息了。",		-- 物品名:"雪球发射器"->关闭 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
			LOWFUEL = "燃料有点不足了。",		-- 物品名:"雪球发射器"->燃料不足 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		},
		FISH = "为了它妾身耽误了好久。",		-- 物品名:"鱼"
		FISHINGROD = "钩子、鱼线和棍子！",		-- 物品名:"钓竿" 制造描述:"去钓鱼。为了鱼。"
		FISHSTICKS = "这可以填饱肚子。",		-- 物品名:"炸鱼排"
		FISHTACOS = "松脆而美味！",		-- 物品名:"鱼肉玉米卷"
		FISH_COOKED = "烤得恰到好处。",		-- 物品名:"烤鱼"
		FLINT = "莫要划坏了奴家的染甲。",		-- 物品名:"燧石"
		FLOWER =
		{
            GENERIC = "碧空露重彩盘湿，花上乞得蜘蛛丝。。",		-- 物品名:"花"->默认
            ROSE = "妖红生彩黛，云衣薄妆新。",		-- 物品名:"花"->玫瑰
        },
        FLOWER_WITHERED = "美丽易逝，妾容长青。",		-- 物品名:"枯萎的花"
		FLOWERHAT = "闻亦美矣。",		-- 物品名:"花环" 制造描述:"放松神经的东西。"
		FLOWER_EVIL = "啊！竟如此欢欣！",		-- 物品名:"邪恶花"
		FOLIAGE = "应怜屐齿印苍苔,小口柴扉久不开。",		-- 物品名:"蕨叶"
		FOOTBALLHAT = "妾身并不喜欢运动。",		-- 物品名:"橄榄球头盔" 制造描述:"保护你的脑壳。"
        FOSSIL_PIECE = "妾身可不想搭积木。",		-- 物品名:"化石碎片"
        FOSSIL_STALKER =
        {
			GENERIC = "还有些碎片没找到。",		-- 物品名:"奇异的骨架"->默认
			FUNNY = "直觉告诉妾身这有点不对。",		-- 物品名:"奇异的骨架"->趣味？？
			COMPLETE = "它活着！哦等等，不，它没有。",		-- 物品名:"奇异的骨架"->准备好了
        },
        STALKER = "残缺的躯体和破碎的灵魂！",		-- 物品名:"复活的骨架"
        STALKER_ATRIUM = "为什么它必须这么大呢？",		-- 物品名:"远古织影者"
        STALKER_MINION = "丝线在本尊手中！",		-- 物品名:"编织暗影"
        THURIBLE = "闻起来像一个香炉。",		-- 物品名:"暗影香炉"
        ATRIUM_OVERGROWTH = "这些符号妾身一个都不认得。",		-- 物品名:"远古方尖碑"
		FROG =
		{
			DEAD = "他死了。",		-- 物品名:"青蛙"->死了
			GENERIC = "他真可爱！",		-- 物品名:"青蛙"->默认
			SLEEPING = "你该回池塘了！",		-- 物品名:"青蛙"->睡着了
		},
		FROGGLEBUNWICH = "美腿三文治。",		-- 物品名:"蛙腿三明治"
		FROGLEGS = "它可是一道美味。",		-- 物品名:"蛙腿"
		FROGLEGS_COOKED = "它叫田鸡，是因为味道像鸡肉吗？",		-- 物品名:"烤蛙腿"
		FRUITMEDLEY = "果香扑鼻。",		-- 物品名:"水果圣代"
		FURTUFT = "黑白毛皮。",		-- 物品名:"毛丛"
		GEARS = "一堆机械零件。",		-- 物品名:"齿轮"
		GHOST = "遇上妾身可能就是灰飞烟灭了。",		-- 物品名:"幽灵"
		GOLDENAXE = "挺花哨的一把斧头。",		-- 物品名:"黄金斧头" 制造描述:"砍树也要有格调！"
		GOLDENPICKAXE = "金子不是软的吗？",		-- 物品名:"黄金鹤嘴锄" 制造描述:"像大Boss一样砸碎岩石。"
		GOLDENPITCHFORK = "精美的叉子，还是叉子。",		-- 物品名:"黄金干草叉" 制造描述:"重新布置整个世界。"
		GOLDENSHOVEL = "等不及要挖洞了。",		-- 物品名:"黄金铲子" 制造描述:"挖掘作用相同，但使用寿命更长。"
		GOLDNUGGET = "闪闪发光的不一定都是金子。",		-- 物品名:"金块"
		GRASS =
		{
			BARREN = "它需要一点点滋养。",		-- 物品名:"草"
			WITHERED = "天气这么热，它不会长回来。",		-- 物品名:"草"->枯萎了
			BURNING = "烧得好快！",		-- 物品名:"草"->正在燃烧
			GENERIC = "来一点血食会让它更茂盛。",		-- 物品名:"草"->默认
			PICKED = "它被采走了。",		-- 物品名:"草"->被采完了
			DISEASED = "它看上去很不舒服。", --unimplemented		-- 物品名:"草"->生病了
			DISEASING = "呃...有些地方不对劲。", --unimplemented		-- 物品名:"草"->正在生病？？
		},
		GRASSGEKKO =
		{
			GENERIC = "这是一只壁虎吗？",		-- 物品名:"草壁虎"->默认
			DISEASED = "它看上去真的很不舒服。", --unimplemented		-- 物品名:"草壁虎"->生病了
		},
		GREEN_CAP = "它看起来很正常。",		-- 物品名:"采摘的绿蘑菇"
		GREEN_CAP_COOKED = "不同了...",		-- 物品名:"烤绿蘑菇"
		GREEN_MUSHROOM =
		{
			GENERIC = "是蘑菇。",		-- 物品名:"绿蘑菇"->默认
			INGROUND = "在睡觉。",		-- 物品名:"绿蘑菇"->在地里面
			PICKED = "不知道它会不会长回来？",		-- 物品名:"绿蘑菇"->被采完了
		},
		GUNPOWDER = "看起来像胡椒粉，你要来一口有吗。",		-- 物品名:"火药" 制造描述:"一把火药。"
		HAMBAT = "这好像不卫生。",		-- 物品名:"火腿棒" 制造描述:"舍不得火腿套不着狼。"
		HAMMER = "停下！",		-- 物品名:"锤子" 制造描述:"敲碎各种东西。"
		HEALINGSALVE = "有点儿刺痛感就说明它起作用了。",		-- 物品名:"治疗药膏" 制造描述:"对割伤和擦伤进行消毒杀菌。"
		HEATROCK =
		{
			FROZEN = "它比冰还冷。",		-- 物品名:"暖石"->冰冻 制造描述:"储存热能供旅行途中使用。"
			COLD = "一块冰凉的石头。",		-- 物品名:"冻伤"->冷了
			GENERIC = "妾身可以控制它的温度。",		-- 物品名:"暖石"->默认 制造描述:"储存热能供旅行途中使用。"
			WARM = "虽然是块石头，但却相当温暖，让人想抱在怀里！",		-- 物品名:"暖石"->温暖 制造描述:"储存热能供旅行途中使用。"
			HOT = "很热很舒服！",		-- 物品名:"中暑"->热了
		},
		HOMESIGN =
		{
			GENERIC = "上面写着“你在这里”。",		-- 物品名:"路牌"->默认 制造描述:"在世界中留下你的标记。"
            UNWRITTEN = "这块牌子现在是空白的。",		-- 物品名:"路牌"->没有写字 制造描述:"在世界中留下你的标记。"
			BURNT = "“不要玩火柴。”",		-- 物品名:"路牌"->烧焦的 制造描述:"在世界中留下你的标记。"
		},
		ARROWSIGN_POST =
		{
			GENERIC = "它说“那个方向”。",		-- 物品名:"指路标志"->默认 制造描述:"对这个世界指指点点。或许只是打下手势。"
            UNWRITTEN = "这块牌子现在是空白的。",		-- 物品名:"指路标志"->没有写字 制造描述:"对这个世界指指点点。或许只是打下手势。"
			BURNT = "“不要玩火柴。”",		-- 物品名:"指路标志"->烧焦的 制造描述:"对这个世界指指点点。或许只是打下手势。"
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "它说“那个方向”。",		-- 物品名:"指路标志"->默认
            UNWRITTEN = "这块牌子现在是空白的。",		-- 物品名:"指路标志"->没有写字
			BURNT = "“不要玩火柴。”",		-- 物品名:"指路标志"->烧焦的
		},
		HONEY = "看起来很好吃！",		-- 物品名:"蜂蜜"
		HONEYCOMB = "蜜蜂曾住在这里。",		-- 物品名:"蜂巢"
		HONEYHAM = "香甜可口。",		-- 物品名:"蜜汁火腿"
		HONEYNUGGETS = "味道像鸡肉，但妾身觉得不是鸡肉。",		-- 物品名:"蜜汁卤肉"
		HORN = "听起来好像那里面有一个皮弗娄牛牧场。",		-- 物品名:"牛角"
		HOUND = "滚开，你只是条畜生！",		-- 物品名:"猎犬"
		HOUNDCORPSE =
		{
			GENERIC = "哈哈哈，来追妾身呀。",		-- 物品名:"猎犬"->默认
			BURNING = "莫要沾惹到妾身。",		-- 物品名:"猎犬"->正在燃烧
			REVIVING = "你也配与我一战？",		-- 物品名:"猎犬"
		},
		HOUNDBONE = "吓坏人家了，哈哈真可笑。",		-- 物品名:"犬骨"
		HOUNDMOUND = "你或许该走了。",		-- 物品名:"猎犬丘"
		ICEBOX = "妾身掌控了寒冷的力量！",		-- 物品名:"冰箱" 制造描述:"延缓食物变质。"
		ICEHAT = "保持冷静，孩子。",		-- 物品名:"冰帽" 制造描述:"用科学技术合成的冰帽。"
		ICEHOUND = "也许夏天我会更喜欢你？",		-- 物品名:"蓝色猎犬"
		INSANITYROCK =
		{
			ACTIVE = "这是活的？",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "小尖石头。",		-- 物品名:"方尖碑"->没有激活
		},
		JAMMYPRESERVES = "或许该做个罐子。",		-- 物品名:"果酱"
		KABOBS = "串在棍子上的午餐。",		-- 物品名:"肉串"
		KILLERBEE =
		{
			GENERIC = "哦，不！愤怒的小孩子！",		-- 物品名:"杀人蜂"->默认
			HELD = "这东西好像很危险。",		-- 物品名:"杀人蜂"->拿在手里
		},
		KNIGHT = "看！",		-- 物品名:"发条骑士"
		KOALEFANT_SUMMER = "吃起来一定很美味。",		-- 物品名:"考拉象"
		KOALEFANT_WINTER = "它看起来又暖和肉又多。",		-- 物品名:"考拉象"
		KRAMPUS = "他要偷妾身的东西！",		-- 物品名:"坎普斯"
		KRAMPUS_SACK = "呕...上面全是恶心的黏液。",		-- 物品名:"坎普斯背包"
		LEIF = "他真巨大！",		-- 物品名:"树精守卫"
		LEIF_SPARSE = "他真巨大！",		-- 物品名:"树精守卫"
		LIGHTER  = "她的幸运打火机。",		-- 物品名:"薇洛的打火机" 制造描述:"火焰在雨中彻夜燃烧。"
		LIGHTNING_ROD =
		{
			CHARGED = "力量在妾身手中！",		-- 物品名:"避雷针" 制造描述:"防雷劈。"
			GENERIC = "主宰天命！",		-- 物品名:"避雷针"->默认 制造描述:"防雷劈。"
		},
		LIGHTNINGGOAT =
		{
			GENERIC = "咩！哈哈哈...",		-- 物品名:"伏特羊"->默认
			CHARGED = "妾身不认为它喜欢被雷劈。",		-- 物品名:"伏特羊"
		},
		LIGHTNINGGOATHORN = "它像一根迷你避雷针。",		-- 物品名:"伏特羊角"
		GOATMILK = "美味，还滋滋作响！",		-- 物品名:"带电的羊奶"
		LITTLE_WALRUS = "等他长大了，就能多几两肉了。",		-- 物品名:"小海象"
		LIVINGLOG = "它看着有点着急。",		-- 物品名:"活木头" 制造描述:"用你的身体来代替\n你该干的活吧。"
		LOG =
		{
			BURNING = "妾身不想靠近！",		-- 物品名:"木头"->正在燃烧
			GENERIC = "又大又重的木头。",		-- 物品名:"木头"->默认
		},
		LUCY = "这斧子倒是好玩。",		-- 物品名:"露西斧"
		LUREPLANT = "它如此迷人。",		-- 物品名:"食人花"
		LUREPLANTBULB = "妾身的花园里还缺一个垃圾桶。",		-- 物品名:"食人花种子"
		MALE_PUPPET = "他被困住了！", --single player		-- 物品名:"木头"
		MANDRAKE_ACTIVE = "切断它！",		-- 物品名:"曼德拉草"
		MANDRAKE_PLANTED = "妾身听说过关于它的奇怪传言。",		-- 物品名:"曼德拉草"
		MANDRAKE = "曼德拉草根拥有奇怪的属性。",		-- 物品名:"曼德拉草"
        MANDRAKESOUP = "唔，它再也不会醒来了。",		-- 物品名:"曼德拉草汤"
        MANDRAKE_COOKED = "它看起来没有那么奇怪了。",		-- 物品名:"木头"
        MAPSCROLL = "一张空白的地图。看起来没什么用。",		-- 物品名:"地图卷轴" 制造描述:"向别人展示你看到什么！"
        MARBLE = "多么美妙的纹理！",		-- 物品名:"大理石"
        MARBLEBEAN = "奇怪纹理的豆子。",		-- 物品名:"大理石豌豆" 制造描述:"种植一片大理石森林。"
        MARBLEBEAN_SAPLING = "看起来刻了点什么。",		-- 物品名:"大理石芽"
        MARBLESHRUB = "好像有点道理。",		-- 物品名:"大理石灌木"
        MARBLEPILLAR = "妾身想妾身能用得上它。",		-- 物品名:"大理石柱"
        MARBLETREE = "多么宏伟的石头树。",		-- 物品名:"大理石树"
        MARSH_BUSH =
        {
			BURNT = "少了一片需要担心的荆棘。",		-- 物品名:"尖刺灌木"->烧焦的
            BURNING = "烧得好快！",		-- 物品名:"尖刺灌木"->正在燃烧
            GENERIC = "它看起来很多刺。",		-- 物品名:"尖刺灌木"->默认
            PICKED = "哎哟！",		-- 物品名:"尖刺灌木"->被采完了
        },
        BURNT_MARSH_BUSH = "被烧得精光。",		-- 物品名:"尖刺灌木"
        MARSH_PLANT = "这是一株植物。",		-- 物品名:"植物"
        MARSH_TREE =
        {
            BURNING = "尖刺！火！",		-- 物品名:"针刺树"->正在燃烧
            BURNT = "现在它是一棵被烧焦了的针刺树。",		-- 物品名:"针刺树"->烧焦的
            CHOPPED = "现在没那么多刺了！",		-- 物品名:"针刺树"->被砍了
            GENERIC = "那些尖刺看起来很锋利！",		-- 物品名:"针刺树"->默认
        },
        MEAT = "美味的腥气，带着三分的血丝。",		-- 物品名:"肉"
        MEATBALLS = "平平凡凡的肉丸罢了。",		-- 物品名:"肉丸"
        MEATRACK =
        {
            DONE = "肉干可以吃了！",		-- 物品名:"晾肉架"->完成了 制造描述:"晾肉的架子。"
            DRYING = "肉风干需要一些时间。",		-- 物品名:"晾肉架"->正在风干 制造描述:"晾肉的架子。"
            DRYINGINRAIN = "雨天晾干肉需要更长时间。",		-- 物品名:"晾肉架"->雨天风干 制造描述:"晾肉的架子。"
            GENERIC = "妾身该晾干一些肉。",		-- 物品名:"晾肉架"->默认 制造描述:"晾肉的架子。"
            BURNT = "晾肉架烧掉了。",		-- 物品名:"晾肉架"->烧焦的 制造描述:"晾肉的架子。"
            DONE_NOTMEAT = "妾身的网也可以用来风干。",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子。"
            DRYING_NOTMEAT = "风干可算不上什么大本事。",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子。"
            DRYINGINRAIN_NOTMEAT = "你这雨还是改日再来吧。",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子。"
        },
        MEAT_DRIED = "妾身还是喜欢新鲜。",		-- 物品名:"风干肉"
        MERM = "腥味十足呀！",		-- 物品名:"鱼人"
        MERMHEAD =
        {
            GENERIC = "整天都会闻到，真是臭死了。",		-- 物品名:"鱼人头"->默认
            BURNT = "烧过的鱼人肉闻起来更糟糕了。",		-- 物品名:"鱼人头"->烧焦的
        },
        MERMHOUSE =
        {
            GENERIC = "谁会住在这种地方？",		-- 物品名:"漏雨的小屋"->默认
            BURNT = "现在没地住了。",		-- 物品名:"漏雨的小屋"->烧焦的
        },
        MINERHAT = "免持照明装置。",		-- 物品名:"矿工帽" 制造描述:"用你脑袋上的灯照亮夜晚。"
        MONKEY = "好奇的小家伙。",		-- 物品名:"穴居猴"
        MONKEYBARREL = "那东西刚刚是不是动了？",		-- 物品名:"穴居猴桶"
        MONSTERLASAGNA = "这是对本座冒犯。",		-- 物品名:"怪物千层饼"
        FLOWERSALAD = "一碗绿叶子。",		-- 物品名:"花沙拉"
        ICECREAM = "妾身太想吃冰淇淋了！",		-- 物品名:"冰淇淋"
        WATERMELONICLE = "冷冻西瓜。",		-- 物品名:"西瓜冰棍"
        TRAILMIX = "健康、天然的小点心。",		-- 物品名:"什锦干果"
        HOTCHILI = "辣到喷火！",		-- 物品名:"辣椒炖肉"
        GUACAMOLE = "额，好奇怪的名字。",		-- 物品名:"鳄梨酱"
        MONSTERMEAT = "呃，看起来味道不怎么样。",		-- 物品名:"怪物肉"
        MONSTERMEAT_DRIED = "气味怪怪的肉干。",		-- 物品名:"怪物肉干"
        MOOSE = "妾身不清楚那东西是什么。",		-- 物品名:"漏雨的小屋"
        MOOSE_NESTING_GROUND = "屋漏偏逢连夜雨。",		-- 物品名:"漏雨的小屋"
        MOOSEEGG = "屋漏偏逢连夜雨。",		-- 物品名:"漏雨的小屋"
        MOSSLING = "一路火花带闪电！",		-- 物品名:"麋鹿鹅幼崽"
        FEATHERFAN = "降温用的。",		-- 物品名:"羽毛扇" 制造描述:"超柔软、超大的扇子。"
        MINIFAN = "不知道怎么回事，风以两倍的速度从后面吹来。",		-- 物品名:"旋转的风扇" 制造描述:"你得跑起来，才能带来风！"
        GOOSE_FEATHER = "毛茸茸的！",		-- 物品名:"麋鹿鹅羽毛"
        STAFF_TORNADO = "旋转的毁灭之力。",		-- 物品名:"天气风向标" 制造描述:"把你的敌人吹走。"
        MOSQUITO =
        {
            GENERIC = "你该落在我的手上。",		-- 物品名:"蚊子"->默认
            HELD = "为他人作嫁衣？",		-- 物品名:"蚊子"->拿在手里
        },
        MOSQUITOSACK = "这也许是别人的血...",		-- 物品名:"蚊子血囊"
        MOUND =
        {
            DUG = "或许，这是他应得的。",		-- 物品名:"坟墓"->被挖了
            GENERIC = "妾身还是不要打扰他们为好。",		-- 物品名:"坟墓"->默认
        },
        NIGHTLIGHT = "它发出阴森可怕的光。",		-- 物品名:"魂灯" 制造描述:"用你的噩梦点亮夜晚。"
        NIGHTMAREFUEL = "这东西太古怪了！",		-- 物品名:"噩梦燃料" 制造描述:"傻子和疯子使用的邪恶残渣。"
        NIGHTSWORD = "为什么会有人做这个？太可怕了。",		-- 物品名:"暗夜剑" 制造描述:"造成噩梦般的伤害。"
        NITRE = "也许炎炎夏日会有用。",		-- 物品名:"硝石"
        ONEMANBAND = "西洋乐器。",		-- 物品名:"独奏乐器" 制造描述:"疯子音乐家也有粉丝。"
        OASISLAKE =
		{
			GENERIC = "这是海市蜃楼吗？",		-- 物品名:"湖泊"->默认
			EMPTY = "干的像一块骨头。",		-- 物品名:"湖泊"
		},
        PANDORASCHEST = "里面可能有很好的东西！也可能是可怕的东西。",		-- 物品名:"华丽箱子"
        PANFLUTE = "为动物们唱小夜曲。",		-- 物品名:"排箫" 制造描述:"抚慰凶猛野兽的音乐。"
        PAPYRUS = "一些纸。",		-- 物品名:"莎草纸" 制造描述:"用于书写。"
        WAXPAPER = "一些蜡纸。",		-- 物品名:"蜡纸" 制造描述:"用于打包东西。"
        PENGUIN = "肯定到繁殖季节了。",		-- 物品名:"企鸥"
        PERD = "蠢鸟！离那些浆果远点！",		-- 物品名:"火鸡"
        PEROGIES = "这些东西味道相当不错。",		-- 物品名:"波兰水饺"
        PETALS = "这下花儿们知道谁才是老大了！",		-- 物品名:"花瓣"
        PETALS_EVIL = "可以不拿些东西吗？",		-- 物品名:"恶魔花瓣"
        PHLEGM = "恶心，非常恶心。",		-- 物品名:"脓鼻涕"
        PICKAXE = "很形象，不是吗？",		-- 物品名:"鹤嘴锄" 制造描述:"凿碎岩石。"
        PIGGYBACK = "臭臭的背包。",		-- 物品名:"小猪包" 制造描述:"能装许多东西，但会减慢你的速度。"
        PIGHEAD =
        {
            GENERIC = "看起来像是给野兽的供品。",		-- 物品名:"猪头"->默认
            BURNT = "外酥里嫩。",		-- 物品名:"猪头"->烧焦的
        },
        PIGHOUSE =
        {
            FULL = "这猪圈可真豪华。",		-- 物品名:"猪屋"->满了 制造描述:"可以住一只猪。"
            GENERIC = "这些猪圈真不赖。",		-- 物品名:"猪屋"->默认 制造描述:"可以住一只猪。"
            LIGHTSOUT = "出来！妾身知道你在家！",		-- 物品名:"猪屋"->关灯了 制造描述:"可以住一只猪。"
            BURNT = "哎呀，是谁这么坏呀！",		-- 物品名:"猪屋"->烧焦的 制造描述:"可以住一只猪。"
        },
        PIGKING = "妾身是蛛，你休想占便宜",		-- 物品名:"猪王"
        PIGMAN =
        {
            DEAD = "应该找人通知它的家人。",		-- 物品名:"猪人"->死了
            FOLLOWER = "你是妾身的仆人之一。",		-- 物品名:"猪人"->追随者
            GENERIC = "他们有点吓人。",		-- 物品名:"猪人"->默认
            GUARD = "看起来很严肃。",		-- 物品名:"猪人"->警戒
            WEREPIG = "来试试本座的蛛网吧！！",		-- 物品名:"猪人"->疯猪
        },
        PIGSKIN = "上面还带着小尾巴。",		-- 物品名:"猪皮"
        PIGTENT = "你这蠢货，离本座远一点。",		-- 物品名:"猪人"
        PIGTORCH = "看上去挺惬意。",		-- 物品名:"猪火炬"
        PINECONE = "来给妾身拨一点松子。",		-- 物品名:"松果"
        PINECONE_SAPLING = "它很快将长成一棵大树！",		-- 物品名:"常青树苗"
        LUMPY_SAPLING = "这棵树是怎么繁殖的？",		-- 物品名:"有疙瘩的树苗"
        PITCHFORK = "现在妾身只需要一群暴民的加入。",		-- 物品名:"干草叉" 制造描述:"铲地用具。"
        PLANTMEAT = "看着不咋样。",		-- 物品名:"叶肉"
        PLANTMEAT_COOKED = "至少它现在是热的。",		-- 物品名:"烤叶肉"
        PLANT_NORMAL =
        {
            GENERIC = "枝繁叶茂！",		-- 物品名:"农作物"->默认
            GROWING = "唉，长得可真慢！",		-- 物品名:"农作物"->正在生长
            READY = "嗯。可以采摘了。",		-- 物品名:"农作物"->准备好的 满的
            WITHERED = "它是热死的。",		-- 物品名:"农作物"->枯萎了
        },
        POMEGRANATE = "它的果实看起来就像琉璃宝石一样。",		-- 物品名:"石榴"
        POMEGRANATE_COOKED = "高级料理！",		-- 物品名:"切片熟石榴"
        POMEGRANATE_SEEDS = "这是一颗种子。",		-- 物品名:"风刮来的种子"
        POND = "妾身看不到底！",		-- 物品名:"池塘"
        POOP = "妾身的纤纤细手可不是干这个的！",		-- 物品名:"粪肥"
        FERTILIZER = "一桶粪便。",		-- 物品名:"便便桶" 制造描述:"少拉点在手上，多拉点在庄稼上。"
        PUMPKIN = "比妾身的腰还要粗的多！",		-- 物品名:"南瓜"
        PUMPKINCOOKIE = "美味的南瓜饼！",		-- 物品名:"南瓜饼干"
        PUMPKIN_COOKED = "它为什么没变成馅饼？",		-- 物品名:"烤南瓜"
        PUMPKIN_LANTERN = "真可怕！",		-- 物品名:"南瓜灯" 制造描述:"长着鬼脸的照明用具。"
        PUMPKIN_SEEDS = "这是一颗种子。",		-- 物品名:"尖种子"
        PURPLEAMULET = "它在向妾身低语。",		-- 物品名:"梦魇护符" 制造描述:"引起精神错乱。"
        PURPLEGEM = "尊贵的紫色。",		-- 物品名:"紫宝石" 制造描述:"结合你们的颜色！"
        RABBIT =
        {
            GENERIC = "可惜我不喜欢你毛茸茸的口感。",		-- 物品名:"兔子"->默认
            HELD = "乖乖的，就能多活一会。",		-- 物品名:"兔子"->拿在手里
        },
        RABBITHOLE =
        {
            GENERIC = "哈哈，一个毛茸茸的家。",		-- 物品名:"兔洞"->默认
            SPRING = "你可别想喂我闭门羹。",		-- 物品名:"兔洞"->春天 or 潮湿
        },
        RAINOMETER =
        {
            GENERIC = "它能测出是否有雨。",		-- 物品名:"雨量计"->默认 制造描述:"观测降雨机率。"
            BURNT = "测量部分化成一缕青烟了。",		-- 物品名:"雨量计"->烧焦的 制造描述:"观测降雨机率。"
        },
        RAINCOAT = "让雨水待在它应该在的地方。",		-- 物品名:"雨衣" 制造描述:"让你保持干燥的防水外套。"
        RAINHAT = "头发乱了...可真令人烦躁。",		-- 物品名:"雨帽" 制造描述:"手感柔软，防雨必备。"
        RATATOUILLE = "今天可以换换口味了。",		-- 物品名:"蔬菜杂烩"
        RAZOR = "要出家的和尚才用这个。",		-- 物品名:"剃刀" 制造描述:"剃掉你脏脏的山羊胡子。"
        REDGEM = "它闪着温暖的光。",		-- 物品名:"红宝石"
        RED_CAP = "闻起来怪怪的。",		-- 物品名:"采摘的红蘑菇"
        RED_CAP_COOKED = "现在跟生蘑菇不同了...",		-- 物品名:"烤红蘑菇"
        RED_MUSHROOM =
        {
            GENERIC = "是蘑菇。",		-- 物品名:"红蘑菇"->默认
            INGROUND = "它在睡觉。",		-- 物品名:"红蘑菇"->在地里面
            PICKED = "不知道它会不会长回来？",		-- 物品名:"红蘑菇"->被采完了
        },
        REEDS =
        {
            BURNING = "烧的真旺！",		-- 物品名:"芦苇"->正在燃烧
            GENERIC = "一簇芦苇。",		-- 物品名:"芦苇"->默认
            PICKED = "所有有用的芦苇都已采摘下来了。",		-- 物品名:"芦苇"->被采完了
        },
        RELIC = "远古家居用品。",		-- 物品名:"遗物"
        RUINS_RUBBLE = "这个可以修复。",		-- 物品名:"损毁的废墟"
        RUBBLE = "只是些碎石块。",		-- 物品名:"碎石"
        RESEARCHLAB =
        {
            GENERIC = "凡俗巧技。",		-- 物品名:"科学机器"->默认 制造描述:"解锁新的合成配方！"
            BURNT = "火焰之下，无所遁形。",		-- 物品名:"科学机器"->烧焦的 制造描述:"解锁新的合成配方！"
        },
        RESEARCHLAB2 =
        {
            GENERIC = "它比那个更强一些罢了！",		-- 物品名:"炼金引擎"->默认 制造描述:"解锁更多合成配方！"
            BURNT = "再多的技巧也不能使它幸免于难。",		-- 物品名:"炼金引擎"->烧焦的 制造描述:"解锁更多合成配方！"
        },
        RESEARCHLAB3 =
        {
            GENERIC = "妾身创造了什么？",		-- 物品名:"暗影操控器"->默认 制造描述:"这还是科学吗？"
            BURNT = "不管它原来是什么，现在已经烧掉了。",		-- 物品名:"暗影操控器"->烧焦的 制造描述:"这还是科学吗？"
        },
        RESEARCHLAB4 =
        {
            GENERIC = "谁会起个这样的名字？",		-- 物品名:"灵子分解器"->默认 制造描述:"增强高礼帽的魔力。"
            BURNT = "现在不用纠结了...",		-- 物品名:"灵子分解器"->烧焦的 制造描述:"增强高礼帽的魔力。"
        },
        RESURRECTIONSTATUE =
        {
            GENERIC = "血肉所制成的身外身！",		-- 物品名:"肉块雕像"->默认 制造描述:"以肉的力量让你重生。"
            BURNT = "再也没用了。",		-- 物品名:"肉块雕像"->烧焦的 制造描述:"以肉的力量让你重生。"
        },
        RESURRECTIONSTONE = "佑我轮回不绝。",		-- 物品名:"试金石"
        ROBIN =
        {
            GENERIC = "那是否意味着冬季已经结束了？",		-- 物品名:"红雀"->默认
            HELD = "他喜欢妾身的口袋。",		-- 物品名:"红雀"->拿在手里
        },
        ROBIN_WINTER =
        {
            GENERIC = "寒荒之地的生命。",		-- 物品名:"雪雀"->默认
            HELD = "它如此柔软。",		-- 物品名:"雪雀"->拿在手里
        },
        ROBOT_PUPPET = "被困住了！", --single player		-- 物品名:"雪雀"
        ROCK_LIGHT =
        {
            GENERIC = "裹了石皮的熔岩坑。",--removed		-- 物品名:"熔岩坑"->默认
            OUT = "看起来很易碎。",--removed		-- 物品名:"熔岩坑"->出去？外面？
            LOW = "那块熔岩正在裹上石皮。",--removed		-- 物品名:"熔岩坑"->低？
            NORMAL = "妾身可不能考的太近。",--removed		-- 物品名:"熔岩坑"->普通
        },
        CAVEIN_BOULDER =
        {
            GENERIC = "你莫不是觉得妾身可以举起这个？",		-- 物品名:"岩石"->默认
            RAISED = "太远了，妾身够不着。",		-- 物品名:"岩石"->提高了？
        },
        ROCK = "妾身口袋里可装不下它。",		-- 物品名:"岩石"
        PETRIFIED_TREE = "看来它被吓僵住了。",		-- 物品名:"岩石"
        ROCK_PETRIFIED_TREE = "生命已经长逝。",		-- 物品名:"石化树"
        ROCK_PETRIFIED_TREE_OLD = "生命已经长逝。",		-- 物品名:"岩石"
        ROCK_ICE =
        {
            GENERIC = "妾身觉得有点凉了。",		-- 物品名:"迷你冰川"->默认
            MELTED = "凛冬的风，会再次吹拂你。",		-- 物品名:"迷你冰川"->融化了
        },
        ROCK_ICE_MELTED = "凛冬的风，会再次吹拂你。",		-- 物品名:"融化的迷你冰川"
        ICE = "妾身觉得有点凉了。",		-- 物品名:"冰"
        ROCKS = "这般坚硬之物，倒是有些许用处。",		-- 物品名:"石头"
        ROOK = "攻占城堡！",		-- 物品名:"发条战车"
        ROPE = "一种短绳。",		-- 物品名:"绳子" 制造描述:"紧密编成的草绳，非常有用。"
        ROTTENEGG = "呕！臭死了！",		-- 物品名:"腐烂鸟蛋"
        ROYAL_JELLY = "那只小蜜蜂倒是会享受！",		-- 物品名:"蜂王浆"
        JELLYBEAN = "一部分是果冻，一部分是豌豆。",		-- 物品名:"彩虹糖豆"
        SADDLE_BASIC = "有了它就可以骑臭动物了。",		-- 物品名:"鞍具" 制造描述:"你坐在动物身上。仅仅是理论上。"
        SADDLE_RACE = "这鞍具真的在飞！",		-- 物品名:"闪亮鞍具" 制造描述:"抵消掉完成目标所花费的时间。或许。"
        SADDLE_WAR = "唯一的问题是会长鞍疮。",		-- 物品名:"战争鞍具" 制造描述:"战场首领的王位。"
        SADDLEHORN = "这能够卸下鞍具。",		-- 物品名:"鞍角" 制造描述:"把鞍具撬开。"
        SALTLICK = "从粗糙到光滑。",		-- 物品名:"舔盐块" 制造描述:"好好喂养你家的牲畜。"
        BRUSH = "皮弗娄牛肯定很喜欢这个。",		-- 物品名:"刷子" 制造描述:"减缓皮弗娄牛毛发的生长速度。"
		SANITYROCK =
		{
			ACTIVE = "那石头看起来真疯狂！",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "它的其余部分去哪儿了？",		-- 物品名:"方尖碑"->没有激活
		},
		SAPLING =
		{
			BURNING = "烧得好快！",		-- 物品名:"树苗"->正在燃烧
			WITHERED = "如果能给它降降温，也许它能好起来。",		-- 物品名:"树苗"->枯萎了
			GENERIC = "小树苗真可爱！",		-- 物品名:"树苗"->默认
			PICKED = "它尝到教训了。",		-- 物品名:"树苗"->被采完了
			DISEASED = "它看上去很不舒服。", --removed		-- 物品名:"树苗"->生病了
			DISEASING = "呃...有些地方不对劲。", --removed		-- 物品名:"树苗"->正在生病？？
		},
   		SCARECROW =
   		{
			GENERIC = "打扮好稻草人就不会有乌鸦了。",		-- 物品名:"友善的稻草人"->默认 制造描述:"模仿最新的秋季时尚。"
			BURNING = "妾身也无能为力。",		-- 物品名:"友善的稻草人"->正在燃烧 制造描述:"模仿最新的秋季时尚。"
			BURNT = "被烧死了！",		-- 物品名:"友善的稻草人"->烧焦的 制造描述:"模仿最新的秋季时尚。"
   		},
   		SCULPTINGTABLE=
   		{
			EMPTY = "妾身可以用这东西来做个石雕。",		-- 物品名:"陶轮" 制造描述:"大理石在你手里将像黏土一样！"
			BLOCK = "准备雕刻。",		-- 物品名:"陶轮"->锁住了 制造描述:"大理石在你手里将像黏土一样！"
			SCULPTURE = "一个杰作！",		-- 物品名:"陶轮"->雕像做好了 制造描述:"大理石在你手里将像黏土一样！"
			BURNT = "完全烧焦。",		-- 物品名:"陶轮"->烧焦的 制造描述:"大理石在你手里将像黏土一样！"
   		},
        SCULPTURE_KNIGHTHEAD = "其余部分在哪里？",		-- 物品名:"可疑的大理石"
		SCULPTURE_KNIGHTBODY =
		{
			COVERED = "是一个古怪的大理石雕像。",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "妾身猜他在压力之下崩溃了。",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "终于又弄到一起了。",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "里面有东西在动。",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        SCULPTURE_BISHOPHEAD = "那是一颗头吗？",		-- 物品名:"可疑的大理石"
		SCULPTURE_BISHOPBODY =
		{
			COVERED = "看起来很老，但感觉挺新。",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "有一个大的碎片没找到。",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "然后呢？",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "里面有东西在动。",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        SCULPTURE_ROOKNOSE = "这是哪儿来的？",		-- 物品名:"可疑的大理石"
		SCULPTURE_ROOKBODY =
		{
			COVERED = "一种大理石雕像。",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "它不在最佳状态。",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "所有都修补好了。",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "里面有东西在动。",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        GARGOYLE_HOUND = "妾身不喜欢它看妾身的眼神。",		-- 物品名:"可疑的月岩"
        GARGOYLE_WEREPIG = "它看起来栩栩如生。",		-- 物品名:"可疑的月岩"
		SEEDS = "每一颗都是一个小小的谜。",		-- 物品名:"种子"
		SEEDS_COOKED = "生的叫种子，熟了叫食物！",		-- 物品名:"烤种子"
		SEWING_KIT = "以丝为线。",		-- 物品名:"针线包" 制造描述:"缝补受损的衣物。"
		SEWING_TAPE = "修补得不错。",		-- 物品名:"可靠的胶布" 制造描述:"缝补受损的衣物。"
		SHOVEL = "地下隐藏着很多秘密。",		-- 物品名:"铲子" 制造描述:"挖掘各种各样的东西。"
		SILK = "蚕吐春丝，济世功绝。妾织罗网，飞虫聚血。",		-- 物品名:"蜘蛛丝"
		SKELETON = "你死好过妾身死。",		-- 物品名:"骷髅"
		SCORCHED_SKELETON = "真可怕。",		-- 物品名:"易碎骨骼"
		SKULLCHEST = "不确定要不要打开。", --removed		-- 物品名:"骷髅箱"
		SMALLBIRD =
		{
			GENERIC = "好小的一只鸟。",		-- 物品名:"小鸟"->默认
			HUNGRY = "它看起来饿了。",		-- 物品名:"小鸟"->有点饿了
			STARVING = "它一定很饿。",		-- 物品名:"小鸟"->挨饿
			SLEEPING = "它都不偷偷睁眼看一下。",		-- 物品名:"小鸟"->睡着了
		},
		SMALLMEAT = "一小块动物尸体。",		-- 物品名:"小肉"
		SMALLMEAT_DRIED = "一小块肉干。",		-- 物品名:"小风干肉"
		SPAT = "看起来脾气暴躁的动物。",		-- 物品名:"钢羊"
		SPEAR = "好尖的一根棍子。",		-- 物品名:"长矛" 制造描述:"使用尖的那一端。"
		SPEAR_WATHGRITHR = "可真是奢侈。",		-- 物品名:"战斗长矛" 制造描述:"黄金使它更锋利。"
		WATHGRITHRHAT = "那顶帽子好奇特。",		-- 物品名:"战斗头盔" 制造描述:"独角兽是你的保护神。"
		SPIDER =
		{
			DEAD = "妾身的儿郎，不曾归乡",		-- 物品名:"蜘蛛"->死了
			GENERIC = "小乖乖，快来给妾身捶捶背。",		-- 物品名:"蜘蛛"->默认
			SLEEPING = "千万不要，长眠不醒。",		-- 物品名:"蜘蛛"->睡着了
		},
		SPIDERDEN = "舒适而温暖！",		-- 物品名:"蜘蛛巢"
		SPIDEREGGSACK = "妾身的孩子，快快出来吧。",		-- 物品名:"蜘蛛卵" 制造描述:"跟你的朋友寻求点帮助。"
		SPIDERGLAND = "这是哪位儿郎于妾身的贡品。",		-- 物品名:"蜘蛛腺"
		SPIDERHAT = "愚笨之人所织造。",		-- 物品名:"蜘蛛帽" 制造描述:"蜘蛛们会喊你\"妈妈\"。"
		SPIDERQUEEN = "你倒是还有几分本事，可惜遇到了本座。",		-- 物品名:"蜘蛛女王"
		SPIDER_WARRIOR =
		{
			DEAD = "将军百战死，壮士十年归。",		-- 物品名:"蜘蛛战士"->死了
			GENERIC = "你看起来有几分凶恶。",		-- 物品名:"蜘蛛战士"->默认
			SLEEPING = "战士要保持警惕。",		-- 物品名:"蜘蛛战士"->睡着了
		},
		SPOILED_FOOD = "那是长满毛毛的腐烂食物。",		-- 物品名:"腐烂物"
        STAGEHAND =
        {
			AWAKE = "总之不要伸出你的手，好吗？",		-- 物品名:"舞台之手"->醒了
			HIDING = "这里有些古怪，但妾身说不出所以然。",		-- 物品名:"舞台之手"->藏起来了
        },
        STATUE_MARBLE =
        {
            GENERIC = "高级的大理石雕像。",		-- 物品名:"大理石雕像"->默认
            TYPE1 = "不要失去理智！",		-- 物品名:"大理石雕像"->类型1
            TYPE2 = "雕像般的。",		-- 物品名:"大理石雕像"->类型2
            TYPE3 = "妾身想知道是哪位大师的作品。", --bird bath type statue		-- 物品名:"大理石雕像"
        },
		STATUEHARP = "他的头怎么了？",		-- 物品名:"竖琴雕像"
		STEELWOOL = "粗糙的金属纤维。",		-- 物品名:"钢丝绵"
		STINGER = "看起来很尖锐！",		-- 物品名:"蜂刺"
		STRAWHAT = "帽子总是会弄乱妾身的发型。",		-- 物品名:"草帽" 制造描述:"帮助你保持清凉干爽。"
		STUFFEDEGGPLANT = "真的塞满了！",		-- 物品名:"酿茄子"
		SWEATERVEST = "这件背心十分体面。",		-- 物品名:"犬牙背心" 制造描述:"粗糙，但挺时尚。"
		REFLECTIVEVEST = "走开，愚蠢的金乌！",		-- 物品名:"清凉夏装" 制造描述:"穿上这件时尚的背心，让你神清气爽。"
		HAWAIIANSHIRT = "穿着它可不安全！",		-- 物品名:"花衬衫" 制造描述:"适合夏日穿着，或在周五便装日穿着。"
		TAFFY = "甜腻腻的，莫要吃坏了妾身的牙齿。",		-- 物品名:"太妃糖"
		TALLBIRD = "好高的一只鸟！",		-- 物品名:"高脚鸟"
		TALLBIRDEGG = "它会孵化吗？",		-- 物品名:"高脚鸟蛋"
		TALLBIRDEGG_COOKED = "美味又营养。",		-- 物品名:"煎高脚鸟蛋"
		TALLBIRDEGG_CRACKED =
		{
			COLD = "它是在哆嗦吗？",		-- 物品名:"冻伤"->冷了
			GENERIC = "看起来它正在孵化！",		-- 物品名:"孵化中的高脚鸟蛋"->默认
			HOT = "蛋也会出汗吗？",		-- 物品名:"中暑"->热了
			LONG = "妾身感觉这需要一些时间...",		-- 物品名:"孵化中的高脚鸟蛋"->还需要很久
			SHORT = "它现在随时要孵出来了。",		-- 物品名:"孵化中的高脚鸟蛋"->很快了
		},
		TALLBIRDNEST =
		{
			GENERIC = "那真是个不一般的蛋！",		-- 物品名:"高脚鸟巢"->默认
			PICKED = "巢是空的。",		-- 物品名:"高脚鸟巢"->被采完了
		},
		TEENBIRD =
		{
			GENERIC = "不是很高的鸟。",		-- 物品名:"小高脚鸟"->默认
			HUNGRY = "你想吃东西想的不行了？",		-- 物品名:"小高脚鸟"->有点饿了
			STARVING = "它目露凶光。",		-- 物品名:"小高脚鸟"->挨饿
			SLEEPING = "它在眯眼休息。",		-- 物品名:"小高脚鸟"->睡着了
		},
		TELESTAFF = "乾坤挪移。",		-- 物品名:"传送魔杖" 制造描述:"穿越空间的法杖！时间要另外收费。"
		TENT =
		{
			GENERIC = "妾身该睡觉了。",		-- 物品名:"帐篷"->默认 制造描述:"回复理智值，但要花费时间并导致饥饿。"
			BURNT = "不剩什么睡觉的东西了。",		-- 物品名:"帐篷"->烧焦的 制造描述:"回复理智值，但要花费时间并导致饥饿。"
		},
		SIESTAHUT =
		{
			GENERIC = "午后安全避暑休息的好地方。",		-- 物品名:"遮阳篷"->默认 制造描述:"躲避烈日，回复理智值。"
			BURNT = "它现在遮不了多少阳了。",		-- 物品名:"遮阳篷"->烧焦的 制造描述:"躲避烈日，回复理智值。"
		},
		TENTACLE = "那个看起来很危险。",		-- 物品名:"触手"
		TENTACLESPIKE = "它又尖又黏又恶心。",		-- 物品名:"触手尖刺"
		TENTACLESPOTS = "有韧性的皮。",		-- 物品名:"触手皮"
		TENTACLE_PILLAR = "黏滑的触手。",		-- 物品名:"大触手"
        TENTACLE_PILLAR_HOLE = "似乎很臭，但值得一探。",		-- 物品名:"硕大的泥坑"
		TENTACLE_PILLAR_ARM = "滑溜溜的小触手。",		-- 物品名:"小触手"
		TENTACLE_GARDEN = "又一种黏滑的触手。",		-- 物品名:"大触手"
		TOPHAT = "多好的帽子。",		-- 物品名:"高礼帽" 制造描述:"最经典的帽子款式。"
		TORCH = "驱赶黑暗的东西。",		-- 物品名:"火炬" 制造描述:"可携带的光源。"
		TRANSISTOR = "雷电让它呼呼作响。",		-- 物品名:"电子元件" 制造描述:"科学至上！滋滋滋！"
		TRAP = "妾身把它编得很紧密。",		-- 物品名:"陷阱" 制造描述:"捕捉小型生物。"
		TRAP_TEETH = "真是糟糕的意外。",		-- 物品名:"犬牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西。"
		TRAP_TEETH_MAXWELL = "妾身可不想踩在那上面！", --single player		-- 物品名:"麦斯威尔的犬牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西。"
		TREASURECHEST =
		{
			GENERIC = "一个箱罢了！",		-- 物品名:"箱子"->默认 制造描述:"一种结实的容器。"
			BURNT = "那个箱子被烧坏了。",		-- 物品名:"箱子"->烧焦的 制造描述:"一种结实的容器。"
		},
		TREASURECHEST_TRAP = "真方便！",		-- 物品名:"宝箱"
		SACRED_CHEST =
		{
			GENERIC = "妾身听到低语了。它想要什么东西。",		-- 物品名:"远古宝箱"->默认
			LOCKED = "它正在做出判断。",		-- 物品名:"远古宝箱"->锁住了
		},
		TREECLUMP = "是不是有人想把妾身困在这里。", --removed		-- 物品名:"远古宝箱"
		TRINKET_1 = "融化了。或许薇洛和他们相处愉快？", --Melted Marbles		-- 物品名:"熔化的弹珠"
		TRINKET_2 = "你跟卡祖有什么关系？", --Fake Kazoo		-- 物品名:"假卡祖笛"
		TRINKET_3 = "那是个死结。永远打不开的结。", --Gord's Knot		-- 物品名:"戈尔迪之结"
		TRINKET_4 = "有点像土地，但它不是。", --Gnome		-- 物品名:"地精爷爷"
		TRINKET_5 = "不幸的是，它太小了，妾身没法乘坐它逃离。", --Toy Rocketship		-- 物品名:"迷你火箭"
		TRINKET_6 = "它们输送电力的日子结束了。", --Frazzled Wires		-- 物品名:"烂电线"
		TRINKET_7 = "没有时间享乐和游戏！", --Ball and Cup		-- 物品名:"杯子和球"
		TRINKET_8 = "太好了。堵浴缸的塞子有了。", --Rubber Bung		-- 物品名:"硬化橡胶塞"
		TRINKET_9 = "妾身本人更喜欢拉链。", --Mismatched Buttons		-- 物品名:"不搭的纽扣"
		TRINKET_10 = "它们很快成为韦斯喜欢的道具了。", --Dentures		-- 物品名:"二手假牙"
		TRINKET_11 = "哈尔向妾身轻诉美丽的谎言。", --Lying Robot		-- 物品名:"机器人玩偶"
		TRINKET_12 = "来份火锅就更好了。", --Dessicated Tentacle		-- 物品名:"干瘪的触手"
		TRINKET_13 = "一定是某种宗教物品。", --Gnomette		-- 物品名:"地精奶奶"
		TRINKET_14 = "现在要是能喝点茶，该多好...", --Leaky Teacup		-- 物品名:"漏水的茶杯"
		TRINKET_15 = "...麦斯威尔又落下了他的行头。", --Pawn		-- 物品名:"白色主教"
		TRINKET_16 = "...麦斯威尔又落下了他的行头。", --Pawn		-- 物品名:"黑色主教"
		TRINKET_17 = "你要给妾身来份血糊糊的肉排吗？。", --Bent Spork		-- 物品名:"弯曲的叉子"
		TRINKET_18 = "妾身想知道它在隐藏什么？", --Trojan Horse		-- 物品名:"玩具木马"
		TRINKET_19 = "结网不是很顺利。", --Unbalanced Top		-- 物品名:"失衡陀螺"
		TRINKET_20 = "什么东西打妾身？！", --Backscratcher		-- 物品名:"痒痒挠"
		TRINKET_21 = "这个搅蛋器整个都弯曲变形了。", --Egg Beater		-- 物品名:"破搅拌器"
		TRINKET_22 = "关于这个绳子，妾身有几个理论。", --Frayed Yarn		-- 物品名:"磨损的纱线"
		TRINKET_23 = "妾身可以自己穿鞋，谢谢。", --Shoehorn		-- 物品名:"鞋拔子"
		TRINKET_24 = "妾身想薇克巴顿应该有只猫。", --Lucky Cat Jar		-- 物品名:"幸运猫扎尔"
		TRINKET_25 = "闻起来有点不新鲜。", --Air Unfreshener		-- 物品名:"臭气制造器"
		TRINKET_26 = "食物和杯子！终极生存容器。", --Potato Cup		-- 物品名:"土豆杯"
		TRINKET_27 = "如果你解开它，你可以从远处刺到别人。", --Coat Hanger		-- 物品名:"钢丝衣架"
		TRINKET_28 = "简直就是阴谋。", --Rook		-- 物品名:"白色战车"
        TRINKET_29 = "简直就是阴谋。", --Rook		-- 物品名:"黑色战车"
        TRINKET_30 = "他怎么到处乱丢呢。", --Knight		-- 物品名:"白色骑士"
        TRINKET_31 = "他怎么到处乱丢呢。", --Knight		-- 物品名:"黑色骑士"
        TRINKET_32 = "妾身认识喜欢这个的人！", --Cubic Zirconia Ball		-- 物品名:"立方氧化锆球"
        TRINKET_33 = "来，为妾身戴上。", --Spider Ring		-- 物品名:"蜘蛛指环"
        TRINKET_34 = "你指望这个东西带来好运？", --Monkey Paw		-- 物品名:"猴爪"
        TRINKET_35 = "很难在这附近找到一个好的烧瓶。", --Empty Elixir		-- 物品名:"空的长生不老药"
        TRINKET_36 = "在吃完所有的糖果后妾身可能会需要这些东西。", --Faux fangs		-- 物品名:"人造尖牙"
        TRINKET_37 = "妾身不相信超自然现象。", --Broken Stake		-- 物品名:"断桩"
        TRINKET_38 = "妾身想它来自另外一个世界，一个满是欺诈的世界。", -- Binoculars Griftlands trinket		-- 物品名:"双筒望远镜"
        TRINKET_39 = "妾身想知道另一只在哪里？", -- Lone Glove Griftlands trinket		-- 物品名:"单只手套"
        TRINKET_40 = "拿着它让妾身觉得在赶集。", -- Snail Scale Griftlands trinket		-- 物品名:"蜗牛秤"
        TRINKET_41 = "摸起来有点温热。", -- Goop Canister Hot Lava trinket		-- 物品名:"黏液罐"
        TRINKET_42 = "它充满了某人的童年记忆。", -- Toy Cobra Hot Lava trinket		-- 物品名:"玩具眼镜蛇"
        TRINKET_43= "它不太擅长跳跃。", -- Crocodile Toy Hot Lava trinket		-- 物品名:"鳄鱼玩具"
        TRINKET_44 = "它是某种植物标本。", -- Broken Terrarium ONI trinket		-- 物品名:"破碎的玻璃罐"
        TRINKET_45 = "它正在接收另一个世界的频率。", -- Odd Radio ONI trinket		-- 物品名:"奇怪的收音机"
        TRINKET_46 = "也许是测试空气动力学的工具？", -- Hairdryer ONI trinket		-- 物品名:"损坏的吹风机"
        LOST_TOY_1  = "糊弄小孩子的玩具罢了。",		-- 物品名:"遗失的玻璃球"
        LOST_TOY_2  = "糊弄小孩子的玩具罢了。",		-- 物品名:"多愁善感的卡祖笛"
        LOST_TOY_7  = "糊弄小孩子的玩具罢了。",		-- 物品名:"珍视的抽线陀螺"
        LOST_TOY_10 = "糊弄小孩子的玩具罢了。",		-- 物品名:"缺少的牙齿"
        LOST_TOY_11 = "糊弄小孩子的玩具罢了。",		-- 物品名:"珍贵的玩具机器人"
        LOST_TOY_14 = "糊弄小孩子的玩具罢了。",		-- 物品名:"妈妈最爱的茶杯"
        LOST_TOY_18 = "糊弄小孩子的玩具罢了。",		-- 物品名:"宝贵的玩具木马"
        LOST_TOY_19 = "糊弄小孩子的玩具罢了。",		-- 物品名:"最爱的陀螺"
        LOST_TOY_42 = "糊弄小孩子的玩具罢了。",		-- 物品名:"装错的玩具眼镜蛇"
        LOST_TOY_43 = "糊弄小孩子的玩具罢了。",		-- 物品名:"宠爱的鳄鱼玩具"
        HALLOWEENCANDY_1 = "就算蛀牙也值得了，对吧？",		-- 物品名:"糖果苹果"
        HALLOWEENCANDY_2 = "妾身可不太能吃甜食？",		-- 物品名:"糖果玉米"
        HALLOWEENCANDY_3 = "是…玉米。",		-- 物品名:"不太甜的玉米"
        HALLOWEENCANDY_4 = "快去洗一洗干净吧。",		-- 物品名:"粘液蜘蛛"
        HALLOWEENCANDY_5 = "明天妾身的牙齿可能会对此发表意见。",		-- 物品名:"浣猫糖果"
        HALLOWEENCANDY_6 = "妾身…不认为妾身会吃那些东西。",		-- 物品名:"\"葡萄干\""
        HALLOWEENCANDY_7 = "每个人遇到这些东西都会激动的。",		-- 物品名:"葡萄干"
        HALLOWEENCANDY_8 = "只有傻瓜才不会爱上这东西。",		-- 物品名:"鬼魂棒棒糖"
        HALLOWEENCANDY_9 = "粘牙。",		-- 物品名:"果冻虫"
        HALLOWEENCANDY_10 = "只有傻瓜才不会爱上这东西。",		-- 物品名:"触须棒棒糖"
        HALLOWEENCANDY_11 = "比真的东西尝起来味道好多了。",		-- 物品名:"巧克力猪"
        HALLOWEENCANDY_12 = "那块糖果刚动了一下吗？", --ONI meal lice candy		-- 物品名:"糖果虱"
        HALLOWEENCANDY_13 = "哎呀，妾身可怜的下巴。", --Griftlands themed candy		-- 物品名:"无敌硬糖"
        HALLOWEENCANDY_14 = "妾身吃不了辣。", --Hot Lava pepper candy		-- 物品名:"熔岩椒"
        CANDYBAG = "它是某种美味的小小的甜点。",		-- 物品名:"糖果袋" 制造描述:"只带万圣夜好吃的东西。"
		HALLOWEEN_ORNAMENT_1 = "一个可以挂在树上的装饰物。",		-- 物品名:"幽灵装饰"
		HALLOWEEN_ORNAMENT_2 = "古怪的装饰。",		-- 物品名:"蝙蝠装饰"
		HALLOWEEN_ORNAMENT_3 = "妾身也可以吊起来，或许大了一些。",		-- 物品名:"蜘蛛装饰"
		HALLOWEEN_ORNAMENT_4 = "触触逼真。",		-- 物品名:"触手装饰"
		HALLOWEEN_ORNAMENT_5 = "可爱的装饰。",		-- 物品名:"悬垂蜘蛛装饰"
		HALLOWEEN_ORNAMENT_6 = "最近大家都在热烈讨论树的装饰。",		-- 物品名:"乌鸦装饰"
		HALLOWEENPOTION_DRINKS_WEAK = "原以为会更大呢。",		-- 物品名:"远古宝箱"
		HALLOWEENPOTION_DRINKS_POTENT = "强力药水。",		-- 物品名:"远古宝箱"
        HALLOWEENPOTION_BRAVERY = "满满的勇气。",		-- 物品名:"远古宝箱"
		HALLOWEENPOTION_MOON = "注入了变异的力量。",		-- 物品名:"月亮精华液"
		HALLOWEENPOTION_FIRE_FX = "烈火结晶。",		-- 物品名:"远古宝箱"
		MADSCIENCE_LAB = "妾身可不会失心疯！",		-- 物品名:"疯狂科学家实验室" 制造描述:"疯狂实验无极限。唯独神智有极限。"
		LIVINGTREE_ROOT = "你应该被彻底根除。",		-- 物品名:"完全正常的树根"
		LIVINGTREE_SAPLING = "它会长得大到吓人。",		-- 物品名:"完全正常的树苗"
        DRAGONHEADHAT = "所以谁要在前面？",		-- 物品名:"幸运兽脑袋" 制造描述:"野兽装束的前端。"
        DRAGONBODYHAT = "中间的部分让妾身有点犹豫。",		-- 物品名:"幸运兽躯体" 制造描述:"野兽装束的中间部分。"
        DRAGONTAILHAT = "神龙摆尾的洋气帽子。",		-- 物品名:"幸运兽尾巴" 制造描述:"野兽装束的尾端。"
        PERDSHRINE =
        {
            GENERIC = "感觉它想要什么东西。",		-- 物品名:"火鸡神龛"->默认 制造描述:"供奉庄严之火鸡。"
            EMPTY = "妾身要在那里种些东西。",		-- 物品名:"火鸡神龛" 制造描述:"供奉庄严之火鸡。"
            BURNT = "完全没用了。",		-- 物品名:"火鸡神龛"->烧焦的 制造描述:"供奉庄严之火鸡。"
        },
        REDLANTERN = "这个灯笼感觉比其他灯笼特别。",		-- 物品名:"红灯笼" 制造描述:"照亮你的路的幸运灯笼。"
        LUCKY_GOLDNUGGET = "多么幸运的发现！",		-- 物品名:"幸运黄金"
        FIRECRACKERS = "可别把火星弄到妾身的裙子上。",		-- 物品名:"红鞭炮" 制造描述:"用重击来庆祝！"
        PERDFAN = "非常大。",		-- 物品名:"幸运扇" 制造描述:"额外的运气，超级多。"
        REDPOUCH = "里面有什么东西吗？",		-- 物品名:"红袋子"
        WARGSHRINE =
        {
            GENERIC = "妾身得做点好玩的东西。",		-- 物品名:"座狼神龛"->默认 制造描述:"供奉陶土之座狼。"
            EMPTY = "妾身需要在里面放支火把。",		-- 物品名:"座狼神龛" 制造描述:"供奉陶土之座狼。"
            BURNING = "妾身得做点好玩的东西。", --for willow to override		-- 物品名:"座狼神龛"->正在燃烧 制造描述:"供奉陶土之座狼。"
            BURNT = "它烧毁了。",		-- 物品名:"座狼神龛"->烧焦的 制造描述:"供奉陶土之座狼。"
        },
        CLAYWARG =
        {
        	GENERIC = "黏土怪物！",		-- 物品名:"黏土座狼"->默认
        	STATUE = "它刚刚是不是动了？",		-- 物品名:"黏土座狼"->雕像
        },
        CLAYHOUND =
        {
        	GENERIC = "它脱离束缚了！",		-- 物品名:"黏土猎犬"->默认
        	STATUE = "它看起来好逼真。",		-- 物品名:"黏土猎犬"->雕像
        },
        HOUNDWHISTLE = "这个能阻挡狗的脚步。",		-- 物品名:"幸运哨子" 制造描述:"对野猎犬吹哨。"
        CHESSPIECE_CLAYHOUND = "反正栓着了，妾身才不担心呢。",		-- 物品名:"猎犬雕塑"
        CHESSPIECE_CLAYWARG = "妾身竟然没被吃掉！",		-- 物品名:"座狼雕塑"
		PIGSHRINE =
		{
            GENERIC = "我当是什么呢，竟敢在这吃香火。",		-- 物品名:"猪神龛"->默认 制造描述:"供奉富饶之猪人。"
            EMPTY = "它想要肉。",		-- 物品名:"猪神龛" 制造描述:"供奉富饶之猪人。"
            BURNT = "烧焦了。",		-- 物品名:"猪神龛"->烧焦的 制造描述:"供奉富饶之猪人。"
		},
		PIG_TOKEN = "这个看起来很重要。",		-- 物品名:"金色腰带"
		PIG_COIN = "是纯金的吗？",		-- 物品名:"猪鼻铸币"
		YOTP_FOOD1 = "一场适合妾身的盛宴。",		-- 物品名:"致敬烤肉" 制造描述:"向猪王敬肉。"
		YOTP_FOOD2 = "只有野兽才会喜欢的食物。",		-- 物品名:"八宝泥馅饼" 制造描述:"那里隐藏着什么？"
		YOTP_FOOD3 = "没什么精致的。",		-- 物品名:"鱼头串" 制造描述:"棍子上的荣华富贵。"
		PIGELITE1 = "看什么呢？", --BLUE		-- 物品名:"韦德"
		PIGELITE2 = "他得了淘金热！", --RED		-- 物品名:"伊格内修斯"
		PIGELITE3 = "你眼里有泥！", --WHITE		-- 物品名:"德米特里"
		PIGELITE4 = "难道你不想打其他人吗？", --GREEN		-- 物品名:"索耶"
		PIGELITEFIGHTER1 = "看什么看？", --BLUE		-- 物品名:"韦德"
		PIGELITEFIGHTER2 = "他得了淘金热！", --RED		-- 物品名:"伊格内修斯"
		PIGELITEFIGHTER3 = "你眼里有泥！", --WHITE		-- 物品名:"德米特里"
		PIGELITEFIGHTER4 = "难道你不想打其他人吗？", --GREEN		-- 物品名:"索耶"
		CARRAT_GHOSTRACER = "真令人不安啊。",		-- 物品名:"查理的胡萝卜鼠"
        YOTC_CARRAT_RACE_START = "不错的起点。",		-- 物品名:"起点" 制造描述:"冲向比赛！"
        YOTC_CARRAT_RACE_CHECKPOINT = "关键的一点。",		-- 物品名:"检查点" 制造描述:"通往光荣之路上的一站。"
        YOTC_CARRAT_RACE_FINISH =
        {
            GENERIC = "与其说是终点线，不如说是终点圈。",		-- 物品名:"终点线"->默认 制造描述:"你走投无路了。"
            BURNT = "一把火烧的干干净净！",		-- 物品名:"终点线"->烧焦的 制造描述:"你走投无路了。"
            I_WON = "哈哈！本座胜出了！",		-- 物品名:"终点线" 制造描述:"你走投无路了。"
            SOMEONE_ELSE_WON = "哎……恭喜了，{winner}。",		-- 物品名:"终点线" 制造描述:"你走投无路了。"
        },
		YOTC_CARRAT_RACE_START_ITEM = "好吧，算是个开始。",		-- 物品名:"起点套装" 制造描述:"冲向比赛！"
        YOTC_CARRAT_RACE_CHECKPOINT_ITEM = "总会抵达终点。",		-- 物品名:"检查点套装" 制造描述:"通往光荣之路上的一站。"
		YOTC_CARRAT_RACE_FINISH_ITEM = "大限将至。",		-- 物品名:"终点线套装" 制造描述:"你走投无路了。"
		YOTC_SEEDPACKET = "如果你问妾身，妾身会说看起来很多籽。",		-- 物品名:"种子包" 制造描述:"一包普通的混合种子。"
		YOTC_SEEDPACKET_RARE = "哟，还是稀罕货啊！",		-- 物品名:"高级种子包" 制造描述:"一包高质量的种子。"
		MINIBOATLANTERN = "真亮！",		-- 物品名:"漂浮灯笼" 制造描述:"闪着暖暖的光亮。"
        YOTC_CARRATSHRINE =
        {
            GENERIC = "做什么呢……",		-- 物品名:"胡萝卜鼠神龛"->默认 制造描述:"供奉灵巧之胡萝卜鼠。"
            EMPTY = "嗯……胡萝卜鼠喜欢吃什么呢？",		-- 物品名:"胡萝卜鼠神龛" 制造描述:"供奉灵巧之胡萝卜鼠。"
            BURNT = "烤胡萝卜的气味。",		-- 物品名:"胡萝卜鼠神龛"->烧焦的 制造描述:"供奉灵巧之胡萝卜鼠。"
        },
        YOTC_CARRAT_GYM_DIRECTION =
        {
            GENERIC = "为训练指明方向。",		-- 物品名:"方向健身房"->默认
            RAT = "你能当一只优秀的小白鼠。",		-- 物品名:"方向健身房"
            BURNT = "妾身的训练计划灰飞烟灭。",		-- 物品名:"方向健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_SPEED =
        {
            GENERIC = "妾身需要提高胡萝卜鼠的速度。",		-- 物品名:"速度健身房"->默认
            RAT = "快点……快点！",		-- 物品名:"速度健身房"
            BURNT = "妾身可能放太多燃料了。",		-- 物品名:"速度健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_REACTION =
        {
            GENERIC = "妾身来训练胡萝卜鼠的反应速度！",		-- 物品名:"反应健身房"->默认
            RAT = "对象的反应时间正在稳步提高！",		-- 物品名:"反应健身房"
            BURNT = "追求力量的过程中付出的微小代价。",		-- 物品名:"反应健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_STAMINA =
        {
            GENERIC = "变得更加强壮了！",		-- 物品名:"耐力健身房"->默认
            RAT = "这只胡萝卜鼠……将无人能挡！！",		-- 物品名:"耐力健身房"
            BURNT = "谁都阻挡不了进步！但这个会推迟它……",		-- 物品名:"耐力健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_DIRECTION_ITEM = "妾身也该练练了！",		-- 物品名:"方向健身房套装" 制造描述:"提高你的胡萝卜鼠的方向感。"
        YOTC_CARRAT_GYM_SPEED_ITEM = "妾身最好把这个组装起来。",		-- 物品名:"速度健身房套装" 制造描述:"增加你的胡萝卜鼠的速度。"
        YOTC_CARRAT_GYM_STAMINA_ITEM = "这个会改善胡萝卜鼠的耐力",		-- 物品名:"耐力健身房套装" 制造描述:"增强你的胡萝卜鼠的耐力。"
        YOTC_CARRAT_GYM_REACTION_ITEM = "这个应该能显著提高胡萝卜鼠的反应时间。",		-- 物品名:"反应健身房套装" 制造描述:"加快你的胡萝卜鼠的反应时间。"
        YOTC_CARRAT_SCALE_ITEM = "可以秤妾身的胡萝卜鼠。",		-- 物品名:"胡萝卜鼠秤套装" 制造描述:"看看你的胡萝卜鼠的情况。"
        YOTC_CARRAT_SCALE =
        {
            GENERIC = "希望天平向妾身倾斜。",		-- 物品名:"胡萝卜鼠秤"->默认
            CARRAT = "它终究只是一个有知觉的蔬菜。",		-- 物品名:"胡萝卜鼠" 制造描述:"灵巧机敏，富含胡萝卜素。"
            CARRAT_GOOD = "胡萝卜鼠熟到可以赛跑了！",		-- 物品名:"胡萝卜鼠秤"
            BURNT = "真是一团糟",		-- 物品名:"胡萝卜鼠秤"->烧焦的
        },
        YOTB_BEEFALOSHRINE =
        {
            GENERIC = "做什么呢……",		-- 物品名:"皮弗娄牛神龛"->默认 制造描述:"供奉坚毅的皮弗娄牛。"
            EMPTY = "嗯……皮弗娄牛是什么做的呢？",		-- 物品名:"皮弗娄牛神龛" 制造描述:"供奉坚毅的皮弗娄牛。"
            BURNT = "闻起来是烤肉的味道。",		-- 物品名:"皮弗娄牛神龛"->烧焦的 制造描述:"供奉坚毅的皮弗娄牛。"
        },
        BEEFALO_GROOMER =
        {
            GENERIC = "没有皮弗娄牛让妾身打扮。",		-- 物品名:"皮弗娄牛美妆台"->默认 制造描述:"美牛原型机。"
            OCCUPIED = "让妾身来美化一下这头皮弗娄牛！",		-- 物品名:"皮弗娄牛美妆台"->被占领 制造描述:"美牛原型机。"
            BURNT = "妾身用最火的风格打扮了妾身的皮弗娄牛……然而也付出了代价。",		-- 物品名:"皮弗娄牛美妆台"->烧焦的 制造描述:"美牛原型机。"
        },
        BEEFALO_GROOMER_ITEM = "妾身还是换个地方把它装起来吧。",		-- 物品名:"美妆台套装" 制造描述:"美牛原型机。"
		BISHOP_CHARGE_HIT = "噢！",		-- 物品名:"皮弗娄牛美妆台"->被主教攻击 制造描述:"美牛原型机。"
		TRUNKVEST_SUMMER = "荒野休闲装。",		-- 物品名:"透气背心" 制造描述:"暖和，但算不上非常暖和。"
		TRUNKVEST_WINTER = "冬季生存装备。",		-- 物品名:"松软背心" 制造描述:"足以抵御冬季暴雪的保暖背心。"
		TRUNK_COOKED = "不知怎么回事比以前更像鼻子了。",		-- 物品名:"象鼻排"
		TRUNK_SUMMER = "一根轻快通风的象鼻。",		-- 物品名:"象鼻"
		TRUNK_WINTER = "一根肥厚多毛的象鼻。",		-- 物品名:"冬象鼻"
		TUMBLEWEED = "谁知道那个风滚草里裹着什么。",		-- 物品名:"风滚草"
		TURKEYDINNER = "啧~",		-- 物品名:"火鸡大餐"
		TWIGS = "一堆小树枝。",		-- 物品名:"树枝"
		UMBRELLA = "妾身讨厌弄湿妾身的头发。",		-- 物品名:"雨伞" 制造描述:"遮阳挡雨！"
		GRASS_UMBRELLA = "妾身的头发湿的时候很好看...问题出在干的时候。",		-- 物品名:"花伞" 制造描述:"漂亮轻便的保护伞。"
		UNIMPLEMENTED = "看起来还没有完工！可能有危险。",		-- 物品名:"皮弗娄牛美妆台" 制造描述:"美牛原型机。"
		WAFFLES = "西洋甜品，香甜可口。",		-- 物品名:"华夫饼"
		WALL_HAY =
		{
			GENERIC = "呃。妾身想只能将就着用了。",		-- 物品名:"草墙"->默认
			BURNT = "完全没用了。",		-- 物品名:"草墙"->烧焦的
		},
		WALL_HAY_ITEM = "这不像是个好主意。",		-- 物品名:"草墙" 制造描述:"草墙墙体。不太结实。"
		WALL_STONE = "不错的墙。",		-- 物品名:"石墙"
		WALL_STONE_ITEM = "它们让妾身有安全感。",		-- 物品名:"石墙" 制造描述:"石墙墙体。"
		WALL_RUINS = "一段古老的墙。",		-- 物品名:"铥矿墙"
		WALL_RUINS_ITEM = "一段坚固的历史。",		-- 物品名:"铥矿墙" 制造描述:"这些墙可以承受相当多的打击。"
		WALL_WOOD =
		{
			GENERIC = "尖的！",		-- 物品名:"木墙"->默认
			BURNT = "烧焦了！",		-- 物品名:"木墙"->烧焦的
		},
		WALL_WOOD_ITEM = "木桩！",		-- 物品名:"木墙" 制造描述:"木墙墙体。"
		WALL_MOONROCK = "空灵而光滑！",		-- 物品名:"月岩墙"
		WALL_MOONROCK_ITEM = "非常轻盈，但出人意料地坚硬。",		-- 物品名:"月岩墙" 制造描述:"月球疯子之墙。"
		FENCE = "不过是个木头栅栏。",		-- 物品名:"木栅栏"
        FENCE_ITEM = "有了它就能造出一个美丽牢固的栅栏。",		-- 物品名:"木栅栏" 制造描述:"木栅栏部分。"
        FENCE_GATE = "有时开，有时关。",		-- 物品名:"木门"
        FENCE_GATE_ITEM = "有了它就能造出一个美丽牢固的大门。",		-- 物品名:"木门" 制造描述:"木栅栏的门。"
		WALRUS = "海象是天生的捕食者，但蜘蛛也是。",		-- 物品名:"海象"
		WALRUSHAT = "上面盖了一层海象毛。",		-- 物品名:"贝雷帽"
		WALRUS_CAMP =
		{
			EMPTY = "看起来有人在这里扎营。",		-- 物品名:"海象营地"
			GENERIC = "我能进去看看吗。",		-- 物品名:"海象营地"->默认
		},
		WALRUS_TUSK = "谢谢你珍贵的礼物。",		-- 物品名:"海象牙"
		WARDROBE =
		{
			GENERIC = "且看妾身，云鬓蓬松宝髻偏...",		-- 物品名:"衣柜"->默认 制造描述:"随心变换面容。"
            BURNING = "烧得好快！",		-- 物品名:"衣柜"->正在燃烧 制造描述:"随心变换面容。"
			BURNT = "毫无风格可言。",		-- 物品名:"衣柜"->烧焦的 制造描述:"随心变换面容。"
		},
		WARG = "你好像不太好对付啊，来试试吧。",		-- 物品名:"座狼"
		WASPHIVE = "妾身看那些孩子们都发疯了。",		-- 物品名:"杀人蜂蜂窝"
		WATERBALLOON = "哎呀，妾身要被弄湿了！",		-- 物品名:"水球" 制造描述:"球体灭火。"
		WATERMELON = "又黏又甜。",		-- 物品名:"西瓜"
		WATERMELON_COOKED = "多汁而温热。",		-- 物品名:"烤西瓜"
		WATERMELONHAT = "让果汁从你的脸上流下，黏黏的感觉。",		-- 物品名:"西瓜帽" 制造描述:"提神醒脑，但感觉黏黏的。"
		WAXWELLJOURNAL = "真可怕。",		-- 物品名:"暗影秘典" 制造描述:"这将让你大吃一惊。"
		WETGOOP = "它尝起来没什么味道。",		-- 物品名:"失败料理"
        WHIP = "没什么比大响声更能带来宁静的了。",		-- 物品名:"皮鞭" 制造描述:"提出有建设性的反馈意见。"
		WINTERHAT = "冬天来的时候，它能派上大用场。",		-- 物品名:"冬帽" 制造描述:"保持脑袋温暖。"
		WINTEROMETER =
		{
			GENERIC = "水银的。",		-- 物品名:"温度测量仪"->默认 制造描述:"测量环境气温。"
			BURNT = "再也不能测量了。",		-- 物品名:"温度测量仪"->烧焦的 制造描述:"测量环境气温。"
		},
        WINTER_TREE =
        {
            BURNT = "节日气氛受了影响。",		-- 物品名:"冬季圣诞树"->烧焦的
            BURNING = "妾身认为那是个错误。",		-- 物品名:"冬季圣诞树"->正在燃烧
            CANDECORATE = "冬季盛宴快乐！",		-- 物品名:"冬季圣诞树"->烛台？？？
            YOUNG = "马上就到冬季盛宴了！",		-- 物品名:"冬季圣诞树"->还年轻
        },
		WINTER_TREESTAND =
		{
			GENERIC = "妾身需要一点自己的空间。",		-- 物品名:"圣诞树墩"->默认 制造描述:"种植并装饰一棵冬季圣诞树！"
            BURNT = "节日气氛好像变得有点奇妙。",		-- 物品名:"圣诞树墩"->烧焦的 制造描述:"种植并装饰一棵冬季圣诞树！"
		},
        WINTER_ORNAMENT = "我会好好欣赏玩意。",		-- 物品名:"圣诞小玩意"
        WINTER_ORNAMENTLIGHT = "像星星一样闪光。",		-- 物品名:"圣诞灯"
        WINTER_ORNAMENTBOSS = "这一个尤其令人印象深刻。",		-- 物品名:"华丽的装饰"
		WINTER_ORNAMENTGORGE = "不知道为什么，这让妾身觉得很饿。",		-- 物品名:"暴食装饰"
        WINTER_FOOD1 = "像个小娃娃似的。", --gingerbread cookie		-- 物品名:"小姜饼"
        WINTER_FOOD2 = "妾身胃口小，可吃不下这么多。", --sugar cookie		-- 物品名:"糖曲奇饼"
        WINTER_FOOD3 = "圣诞节期间的牙疼马上就要发生了。", --candy cane		-- 物品名:"拐杖糖"
        WINTER_FOOD4 = "那个可能有点儿不道德。", --fruitcake		-- 物品名:"永远的水果蛋糕"
        WINTER_FOOD5 = "能有一次吃浆果以外的东西真是好。", --yule log cake		-- 物品名:"巧克力树洞蛋糕"
        WINTER_FOOD6 = "直接放进嘴里！", --plum pudding		-- 物品名:"李子布丁"
        WINTER_FOOD7 = "充满了美味汁液的空心苹果。", --apple cider		-- 物品名:"苹果酒"
        WINTER_FOOD8 = "它是怎么保持温暖的？", --hot cocoa		-- 物品名:"热可可"
        WINTER_FOOD9 = "美味的小甜心？", --eggnog		-- 物品名:"美味的蛋酒"
		WINTERSFEASTOVEN =
		{
			GENERIC = "喜庆的炉子，用来做火烤的食物！",		-- 物品名:"砖砌烤炉"->默认 制造描述:"燃起了喜庆的火焰。"
			COOKING = "烹饪果然是门学问。",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰。"
			ALMOST_DONE_COOKING = "美食就快完成了！",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰。"
			DISH_READY = "妾身已经能问到香味了。",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰。"
		},
		BERRYSAUCE = "浆果果酱。",		-- 物品名:"快乐浆果酱"
		BIBINGKA = "软乎乎的。",		-- 物品名:"比宾卡米糕"
		CABBAGEROLLS = "肉躲在白菜里面，但被妾身发现了。",		-- 物品名:"白菜卷"
		FESTIVEFISH = "品尝品尝时令海鲜。",		-- 物品名:"节庆鱼料理"
		GRAVY = "全是肉汁。",		-- 物品名:"好肉汁"
		LATKES = "妾身能吃好多个。",		-- 物品名:"土豆饼"
		LUTEFISK = "那有喇叭鱼吗？",		-- 物品名:"苏打鱼"
		MULLEDDRINK = "这杯潘趣酒劲很足。",		-- 物品名:"香料潘趣酒"
		PANETTONE = "这个仲冬节的面包真是应景。",		-- 物品名:"托尼甜面包"
		PAVLOVA = "真棒！",		-- 物品名:"巴甫洛娃蛋糕"
		PICKLEDHERRING = "无可挑剔的美味。",		-- 物品名:"腌鲱鱼"
		POLISHCOOKIE = "妾身要来一次光碟行动！",		-- 物品名:"波兰饼干"
		PUMPKINPIE = "来尝尝妾身的厨艺。",		-- 物品名:"南瓜派"
		ROASTTURKEY = "肥美多汁的鸡腿，专门为你做的。",		-- 物品名:"烤火鸡"
		STUFFING = "都是好料！",		-- 物品名:"烤火鸡面包馅"
		SWEETPOTATO = "也许单纯的烤红薯更美妙。",		-- 物品名:"红薯焗饭"
		TAMALES = "妾身要是再多吃一些，也许会变得健壮了。",		-- 物品名:"塔马利"
		TOURTIERE = "妾身怎么会像饕餮一样进食呢！",		-- 物品名:"饕餮馅饼"
		TABLE_WINTERS_FEAST =
		{
			GENERIC = "节庆餐桌。",		-- 物品名:"冬季盛宴餐桌"->默认 制造描述:"一起来享用冬季盛宴吧。"
			HAS_FOOD = "吃饭时间到了！",		-- 物品名:"冬季盛宴餐桌" 制造描述:"一起来享用冬季盛宴吧。"
			WRONG_TYPE = "不是吃这个的季节。",		-- 物品名:"冬季盛宴餐桌" 制造描述:"一起来享用冬季盛宴吧。"
			BURNT = "谁会做这种事？",		-- 物品名:"冬季盛宴餐桌"->烧焦的 制造描述:"一起来享用冬季盛宴吧。"
		},
		GINGERBREADWARG = "呀，让本座捏碎你吧。",		-- 物品名:"姜饼座狼"
		GINGERBREADHOUSE = "管吃管住。",		-- 物品名:"姜饼猪屋"
		GINGERBREADPIG = "妾身最好跟着这个小东西。",		-- 物品名:"姜饼猪"
		CRUMBS = "走一路掉一路。",		-- 物品名:"饼干屑"
		WINTERSFEASTFUEL = "冬季精神!",		-- 物品名:"节日欢愉"
        KLAUS = "那究竟是什么东西！",		-- 物品名:"克劳斯"
        KLAUS_SACK = "妾身绝对应该打开那东西。",		-- 物品名:"赃物袋"
		KLAUSSACKKEY = "对一个鹿角来说这真是太花俏了。",		-- 物品名:"麋鹿茸"
		WORMHOLE =
		{
			GENERIC = "一个贪婪的巨口，还有令人厌恶的口水。",		-- 物品名:"虫洞"->默认
			OPEN = "哦，你在呼唤我吗。",		-- 物品名:"虫洞"->打开
		},
		WORMHOLE_LIMITED = "啊，那玩意看着不太对啊。",		-- 物品名:"生病的虫洞"->一次性虫洞 单机
		ACCOMPLISHMENT_SHRINE = "妾身想用一下它，妾身想让全世界都知道妾身的所作所为。", --single player		-- 物品名:"奖杯" 制造描述:"证明你作为一个人的价值。"
		LIVINGTREE = "它在观察妾身吗？",		-- 物品名:"完全正常的树"
		ICESTAFF = "摸上去冷冰冰的。",		-- 物品名:"冰魔杖" 制造描述:"把敌人冰冻在原地。"
		REVIVER = "丑陋之心在跳动，将鬼魂复活了！",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
		SHADOWHEART = "跳动的心房...现在归我了",		-- 物品名:"暗影心房"
        ATRIUM_RUBBLE =
        {
			LINE_1 = "它描绘了一个古老的文明。人们看起来又饿又怕。",		-- 物品名:"远古壁画"->第一行
			LINE_2 = "这块石板已经无法识别了。",		-- 物品名:"远古壁画"->第二行
			LINE_3 = "有黑暗的东西悄悄逼近这座城市和城里的人。",		-- 物品名:"远古壁画"->第三行
			LINE_4 = "人们的皮肤在脱落。他们的表里截然不同。",		-- 物品名:"远古壁画"->第四行
			LINE_5 = "这里描绘了一座宏伟城市。",		-- 物品名:"远古壁画"->第五行
		},
        ATRIUM_STATUE = "这看起来并不全是真的。",		-- 物品名:"远古雕像"
        ATRIUM_LIGHT =
        {
			ON = "非常令人不安的光。",		-- 物品名:"远古灯柱"->开启
			OFF = "它必须有动力来源。",		-- 物品名:"远古灯柱"->关闭
		},
        ATRIUM_GATE =
        {
			ON = "回到正常运转状态。",		-- 物品名:"远古大门"->开启
			OFF = "主要部分依然完好无损。",		-- 物品名:"远古大门"->关闭
			CHARGING = "它正在获得能量。",		-- 物品名:"远古大门"->充能中
			DESTABILIZING = "大门在晃动。",		-- 物品名:"远古大门"->不稳定
			COOLDOWN = "它需要时间恢复。妾身也是。",		-- 物品名:"远古大门"->冷却中
        },
        ATRIUM_KEY = "这是从它里面发射出来的能量。",		-- 物品名:"远古钥匙"
		LIFEINJECTOR = "它可以修补肉体的损伤！",		-- 物品名:"强心针" 制造描述:"提高下你那日渐衰退的最大健康值吧。"
		SKELETON_PLAYER =
		{
			MALE = "%s一定是在和%s进行搏斗时死的。",		-- 物品名:"骷髅"->男性
			FEMALE = "%s一定是在和%s进行试搏斗死的。",		-- 物品名:"骷髅"->女性
			ROBOT = "%s一定是在和%s进行搏斗时死的。",		-- 物品名:"骷髅"->机器人
			DEFAULT = "%s一定是在和%s进行搏斗时死的。",		-- 物品名:"物品栏物品"->默认
		},
		HUMANMEAT = "哼？这肉是万万吃不得的。",		-- 物品名:"长猪"
		HUMANMEAT_COOKED = "煮的粉嫩，你要来一口吗。",		-- 物品名:"煮熟的长猪"
		HUMANMEAT_DRIED = "风干就行了，对吧？",		-- 物品名:"长猪肉干"
		ROCK_MOON = "那块石头来自月亮。",		-- 物品名:"岩石"
		MOONROCKNUGGET = "那块石头来自月亮。",		-- 物品名:"月岩"
		MOONROCKCRATER = "妾身应该把闪亮的东西粘在它里面。应该吧。",		-- 物品名:"带孔月岩" 制造描述:"用于划定地盘的岩石。"
		MOONROCKSEED = "太阴，月正当天！",		-- 物品名:"天体宝球"
        REDMOONEYE = "它能看几公里，也能在几公里外被看到。",		-- 物品名:"红色月眼"
        PURPLEMOONEYE = "是个很好的标记物，但妾身希望它不要再那样看妾身。",		-- 物品名:"紫色月眼"
        GREENMOONEYE = "那东西会密切关注这地方。",		-- 物品名:"绿色月眼"
        ORANGEMOONEYE = "有那东西帮忙观望，没有人会迷路。",		-- 物品名:"橘色月眼"
        YELLOWMOONEYE = "那应该给每个人指路。",		-- 物品名:"黄色月眼"
        BLUEMOONEYE = "留个心眼总是比较明智的。",		-- 物品名:"蓝色月眼"
        BOARON = "本座可以应对他！",		-- 物品名:"战猪"
        PEGHOOK = "那家伙喷出来的东西有腐蚀性！",		-- 物品名:"蝎子"
        TRAILS = "他的胳膊真壮啊。",		-- 物品名:"野猪猩"
        TURTILLUS = "它的壳有尖刺！",		-- 物品名:"坦克龟"
        SNAPPER = "这家伙会咬人。",		-- 物品名:"鳄鱼指挥官"
		RHINODRILL = "他的鼻子很适合干这个活。",		-- 物品名:"后扣帽犀牛兄弟"
		BEETLETAUR = "妾身在这里都能闻到他的气味！",		-- 物品名:"地狱独眼巨猪"
        LAVAARENA_PORTAL =
        {
            ON = "妾身要走了。",		-- 物品名:"远古大门"->开启
            GENERIC = "能过来，应该也能回去吧？",		-- 物品名:"远古大门"->默认
        },
        HEALINGSTAFF = "释放再生的力量。",		-- 物品名:"生命魔杖"
        FIREBALLSTAFF = "召唤流星从天而降。",		-- 物品名:"地狱魔杖"
        HAMMER_MJOLNIR = "这把敲东西的锤子可真重。",		-- 物品名:"锻锤"
        SPEAR_GUNGNIR = "妾身可以用它快速充电。",		-- 物品名:"尖齿矛"
        BLOWDART_LAVA = "妾身可以在范围内用这个武器。",		-- 物品名:"吹箭"
        BLOWDART_LAVA2 = "它利用一股强气流来推动抛射物。",		-- 物品名:"熔化吹箭"
        WEBBER_SPIDER_MINION = "宝宝们快回家，不要出来哦。",		-- 物品名:"蜘蛛宝宝"
        BOOK_FOSSIL = "这样能把那些怪物拖住一阵子。",		-- 物品名:"石化之书"
		SPEAR_LANCE = "它直击要害。",		-- 物品名:"螺旋矛"
		BOOK_ELEMENTAL = "妾身看不清这些字。",		-- 物品名:"召唤之书"
        QUAGMIRE_ALTAR =
        {
        	GENERIC = "妾身最好开始煮些祭品。",		-- 物品名:"饕餮祭坛"->默认
        	FULL = "它正在消化。",		-- 物品名:"饕餮祭坛"->满了
    	},
		QUAGMIRE_SUGARWOODTREE =
		{
			GENERIC = "它含有大量美味可口的树液。",		-- 物品名:"糖木树"->默认
		},
		QUAGMIRE_SPOTSPICE_SHRUB =
		{
			GENERIC = "它让妾身想起来那些带触手的怪物。",		-- 物品名:"带斑点的小灌木"->默认
			PICKED = "从那丛灌木中采集不到更多果实了。",		-- 物品名:"带斑点的小灌木"->被采完了
		},
		QUAGMIRE_SALT_RACK =
		{
			READY = "盐积聚在绳子上了。",		-- 物品名:"盐架"->准备好的 满的
			GENERIC = "这是需要时间的。",		-- 物品名:"盐架"->默认
		},
		QUAGMIRE_SAFE =
		{
			GENERIC = "这是保险箱，用来保护物品安全的。",		-- 物品名:"保险箱"->默认
			LOCKED = "没有钥匙就打不开。",		-- 物品名:"保险箱"->锁住了
		},
		QUAGMIRE_MUSHROOMSTUMP =
		{
			GENERIC = "那些是蘑菇吗？这真是一桩难题。",		-- 物品名:"蘑菇"->默认
			PICKED = "应该不会长回来了。",		-- 物品名:"蘑菇"->被采完了
		},
        QUAGMIRE_PARK_GATE =
        {
            GENERIC = "没钥匙不行。",		-- 物品名:"铁门"->默认
            LOCKED = "锁得牢牢的。",		-- 物品名:"铁门"->锁住了
        },
        QUAGMIRE_PIGEON =
        {
            DEAD = "死了。",		-- 物品名:"鸽子"->死了 制造描述:"这是一只完整的活鸽。"
            GENERIC = "羽翼丰满。",		-- 物品名:"鸽子"->默认 制造描述:"这是一只完整的活鸽。"
            SLEEPING = "暂时睡着了。",		-- 物品名:"鸽子"->睡着了 制造描述:"这是一只完整的活鸽。"
        },
        WINONA_CATAPULT =
        {
        	GENERIC = "聚沙成塔。",		-- 物品名:"薇诺娜的投石机"->默认 制造描述:"向敌人投掷大石块"
        	OFF = "没有动力。",		-- 物品名:"薇诺娜的投石机"->关闭 制造描述:"向敌人投掷大石块"
        	BURNING = "熊熊烈火！",		-- 物品名:"薇诺娜的投石机"->正在燃烧 制造描述:"向敌人投掷大石块"
        	BURNT = "妾身也不能拯救它。",		-- 物品名:"薇诺娜的投石机"->烧焦的 制造描述:"向敌人投掷大石块"
        },
        WINONA_SPOTLIGHT =
        {
        	GENERIC = "好棒的主意！",		-- 物品名:"薇诺娜的聚光灯"->默认 制造描述:"白天夜里都发光"
        	OFF = "没有动力。",		-- 物品名:"薇诺娜的聚光灯"->关闭 制造描述:"白天夜里都发光"
        	BURNING = "熊熊烈火！",		-- 物品名:"薇诺娜的聚光灯"->正在燃烧 制造描述:"白天夜里都发光"
        	BURNT = "妾身也不能拯救它。",		-- 物品名:"薇诺娜的聚光灯"->烧焦的 制造描述:"白天夜里都发光"
        },
        WINONA_BATTERY_LOW =
        {
        	GENERIC = "弱雷阵？",		-- 物品名:"薇诺娜的发电机"->默认 制造描述:"要确保电力供应充足"
        	LOWPOWER = "好像快维持不住了。",		-- 物品名:"薇诺娜的发电机"->没电了 制造描述:"要确保电力供应充足"
        	OFF = "妾身也搞不明白这个。",		-- 物品名:"薇诺娜的发电机"->关闭 制造描述:"要确保电力供应充足"
        	BURNING = "熊熊烈火！",		-- 物品名:"薇诺娜的发电机"->正在燃烧 制造描述:"要确保电力供应充足"
        	BURNT = "妾身也不能拯救它。",		-- 物品名:"薇诺娜的发电机"->烧焦的 制造描述:"要确保电力供应充足"
        },
        WINONA_BATTERY_HIGH =
        {
        	GENERIC = "嘿！可真聪明！",		-- 物品名:"薇诺娜的宝石发电机"->默认 制造描述:"这玩意烧宝石，所以肯定不会差。"
        	LOWPOWER = "好像快维持不住了。",		-- 物品名:"薇诺娜的宝石发电机"->没电了 制造描述:"这玩意烧宝石，所以肯定不会差。"
        	OFF = "法力涌动着。",		-- 物品名:"薇诺娜的宝石发电机"->关闭 制造描述:"这玩意烧宝石，所以肯定不会差。"
        	BURNING = "熊熊烈火！",		-- 物品名:"薇诺娜的宝石发电机"->正在燃烧 制造描述:"这玩意烧宝石，所以肯定不会差。"
        	BURNT = "妾身也不能拯救它。",		-- 物品名:"薇诺娜的宝石发电机"->烧焦的 制造描述:"这玩意烧宝石，所以肯定不会差。"
        },
        COMPOSTWRAP = "妾身可不需要这个。",		-- 物品名:"肥料包" 制造描述:"\"草本\"的疗法。"
        ARMOR_BRAMBLE = "以逸待劳。",		-- 物品名:"荆棘外壳" 制造描述:"让大自然告诉你什么叫\"滚开\"。"
        TRAP_BRAMBLE = "谁要是踩上去肯定会被戳到。",		-- 物品名:"荆棘陷阱" 制造描述:"都有机会中招的干\n扰陷阱。"
        BOATFRAGMENT03 = "就剩下了这一点。",		-- 物品名:"船碎片"
        BOATFRAGMENT04 = "就剩下了这一点。",		-- 物品名:"船碎片"
        BOATFRAGMENT05 = "就剩下了这一点。",		-- 物品名:"船碎片"
		BOAT_LEAK = "要补漏，不然会沉。",		-- 物品名:"漏洞"
        MAST = "且慢！桅杆！",		-- 物品名:"桅杆" 制造描述:"乘风破浪会有时。"
        SEASTACK = "这是一块石头。",		-- 物品名:"浮堆"
        FISHINGNET = "经纬智何多，网张犹设罗。", --unimplemented		-- 物品名:"渔网" 制造描述:"就是一张网。"
        ANTCHOVIES = "啊,妾身能丢回去吗？", --unimplemented		-- 物品名:"蚁头凤尾鱼"
        STEERINGWHEEL = "来世要做一名水手。",		-- 物品名:"方向舵" 制造描述:"航海必备道具。"
        ANCHOR = "妾身可不想让小船飘走。",		-- 物品名:"锚" 制造描述:"船用刹车"
        BOATPATCH = "以防不测。",		-- 物品名:"船补丁" 制造描述:"打补丁永远不晚。"
        DRIFTWOOD_TREE =
        {
            BURNING = "浮木在燃烧！",		-- 物品名:"浮木"->正在燃烧
            BURNT = "看起来没什么用了。",		-- 物品名:"浮木"->烧焦的
            CHOPPED = "可能还有东西值得挖一挖。",		-- 物品名:"浮木"->被砍了
            GENERIC = "被冲到岸上的一颗枯树。",		-- 物品名:"浮木"->默认
        },
        DRIFTWOOD_LOG = "能浮在水上。",		-- 物品名:"浮木桩"
        MOON_TREE =
        {
            BURNING = "树在燃烧。",		-- 物品名:"月树"->正在燃烧
            BURNT = "树烧尽了。",		-- 物品名:"月树"->烧焦的
            CHOPPED = "是吴刚干的吗。",		-- 物品名:"月树"->被砍了
            GENERIC = "这好像也不是桂树。",		-- 物品名:"月树"->默认
        },
		MOON_TREE_BLOSSOM = "从月亮树上掉下来的。",		-- 物品名:"月树花"
        MOONBUTTERFLY =
        {
        	GENERIC = "月光在飞舞。",		-- 物品名:"月蛾"->默认
        	HELD = "月光在我掌心飞舞。",		-- 物品名:"月蛾"->拿在手里
        },
		MOONBUTTERFLYWINGS = "美丽的蝴蝶翅膀。",		-- 物品名:"月蛾翅膀"
        MOONBUTTERFLY_SAPLING = "蛾子变树？疯狂的月光！",		-- 物品名:"月树苗"
        ROCK_AVOCADO_FRUIT = "会把妾身的牙都咬碎。",		-- 物品名:"石果"
        ROCK_AVOCADO_FRUIT_RIPE = "没煮熟的核果最糟了。",		-- 物品名:"成熟石果"
        ROCK_AVOCADO_FRUIT_RIPE_COOKED = "够软了，可以吃了。",		-- 物品名:"熟石果"
        ROCK_AVOCADO_FRUIT_SPROUT = "它正在生长。",		-- 物品名:"发芽的石果"
        ROCK_AVOCADO_BUSH =
        {
        	BARREN = "它结果的日子结束了。",		-- 物品名:"石果灌木丛"
			WITHERED = "被热坏了。",		-- 物品名:"石果灌木丛"->枯萎了
			GENERIC = "是月亮上来的灌木！",		-- 物品名:"石果灌木丛"->默认
			PICKED = "得过些时间才能结出果实。",		-- 物品名:"石果灌木丛"->被采完了
			DISEASED = "看上去病的很重。", --unimplemented		-- 物品名:"石果灌木丛"->生病了
            DISEASING = "呃...有些地方不对劲。", --unimplemented		-- 物品名:"石果灌木丛"->正在生病？？
			BURNING = "它在燃烧。",		-- 物品名:"石果灌木丛"->正在燃烧
		},
        DEAD_SEA_BONES = "上了岸，结果落得如此下场。",		-- 物品名:"海骨"
        HOTSPRING =
        {
        	GENERIC = "可能比得了濯垢泉十一。",		-- 物品名:"温泉"->默认
        	BOMBED = "温度还挺高。",		-- 物品名:"温泉"
        	GLASS = "月华凝结。",		-- 物品名:"温泉"
			EMPTY = "那妾身就等等它再变满吧。",		-- 物品名:"温泉"
        },
        MOONGLASS = "它非常锋利。",		-- 物品名:"月亮碎片"
        MOONGLASS_CHARGED = "磅礴的法力！",		-- 物品名:"注能月亮碎片"
        MOONGLASS_ROCK = "妾身都能看到自己的倒影。",		-- 物品名:"月光玻璃"
        BATHBOMB = "给妾身放些花瓣就好。",		-- 物品名:"沐浴球" 制造描述:"春天里来百花香？这点子把地都炸碎了"
        TRAP_STARFISH =
        {
            GENERIC = "噢，多可爱的一只小海星！",		-- 物品名:"海星"->默认
            CLOSED = "它差点吞了妾身！",		-- 物品名:"海星"
        },
        DUG_TRAP_STARFISH = "它再也骗不了人了。",		-- 物品名:"海星陷阱"
        SPIDER_MOON =
        {
        	GENERIC = "不是你占据了月光，是月光侵蚀了你。",		-- 物品名:"破碎蜘蛛"->默认
        	SLEEPING = "月光之下，无所遁形。",		-- 物品名:"破碎蜘蛛"->睡着了
        	DEAD = "你的月光抛弃了你？",		-- 物品名:"破碎蜘蛛"->死了
        },
        MOONSPIDERDEN = "一股阴气侵蚀了这个蛛巢。",		-- 物品名:"破碎蜘蛛洞"
		FRUITDRAGON =
		{
			GENERIC = "挺可爱的，不过还没熟。",		-- 物品名:"沙拉蝾螈"->默认
			RIPE = "现在应该熟了。",		-- 物品名:"沙拉蝾螈"
			SLEEPING = "在打盹呢。",		-- 物品名:"沙拉蝾螈"->睡着了
		},
        PUFFIN =
        {
            GENERIC = "奇怪的鸟儿，是以前没见过的呢。",		-- 物品名:"海鹦鹉"->默认
            HELD = "抓到一只，吹嘘一下也不为过。",		-- 物品名:"海鹦鹉"->拿在手里
            SLEEPING = "呼呼的安睡。",		-- 物品名:"海鹦鹉"->睡着了
        },
		MOONGLASSAXE = "妾身把它做的非常好用。",		-- 物品名:"月光玻璃斧" 制造描述:"脆弱而有效。"
		GLASSCUTTER = "妾身真的不是打架的料。",		-- 物品名:"玻璃刀" 制造描述:"尖端武器。"
        ICEBERG =
        {
            GENERIC = "一定要避开那东西。", --unimplemented		-- 物品名:"迷你冰山"->默认
            MELTED = "完全融化了。", --unimplemented		-- 物品名:"迷你冰山"->融化了
        },
        ICEBERG_MELTED = "完全融化了。", --unimplemented		-- 物品名:"融化的迷你冰山"
        MINIFLARE = "妾身可以点一发，让所有人都知道妾身在这。",		-- 物品名:"信号弹" 制造描述:"为你信任的朋友照亮前路。"
		MOON_FISSURE =
		{
			GENERIC = "平静和恐惧同时在妾身的脑中回响。",		-- 物品名:"天体裂隙"->默认
			NOLIGHT = "这个地方的裂隙越来越明显。",		-- 物品名:"天体裂隙"
		},
        MOON_ALTAR =
        {
            MOON_ALTAR_WIP = "它想被完成。",		-- 物品名:"组装一半的祭坛"
            GENERIC = "唔？你说什么？",		-- 物品名:"天体祭坛"->默认
        },
        MOON_ALTAR_IDOL = "妾身有种强烈的感觉想把它搬走。",		-- 物品名:"天体祭坛雕像"
        MOON_ALTAR_GLASS = "它不想待在地上。",		-- 物品名:"天体祭坛底座"
        MOON_ALTAR_SEED = "它想让妾身给它一个家。",		-- 物品名:"天体祭坛宝球"
        MOON_ALTAR_ROCK_IDOL = "里面有东西。",		-- 物品名:"在呼唤我"
        MOON_ALTAR_ROCK_GLASS = "里面有东西。",		-- 物品名:"在呼唤我"
        MOON_ALTAR_ROCK_SEED = "里面有东西。",		-- 物品名:"在呼唤我"
        MOON_ALTAR_CROWN = "妾身钓上来的，现在去找个裂隙吧！",		-- 物品名:"未激活天体贡品"
        MOON_ALTAR_COSMIC = "感觉它在等什么事情发生。",		-- 物品名:"天体贡品"
        MOON_ALTAR_ASTRAL = "它似乎是一个更大机制的一部分。",		-- 物品名:"天体圣殿"
        MOON_ALTAR_ICON = "妾身知道把你放哪了。",		-- 物品名:"天体圣殿象征"
        MOON_ALTAR_WARD = "它需要和其他的那些放在一起。",		-- 物品名:"天体圣殿卫戍"
        SEAFARING_PROTOTYPER =
        {
            GENERIC = "是不是该谢谢妾身了。",		-- 物品名:"智囊团"->默认 制造描述:"海上科学。"
            BURNT = "大海丢失了智慧。",		-- 物品名:"智囊团"->烧焦的 制造描述:"海上科学。"
        },
        BOAT_ITEM = "在水上做些挺不错的。",		-- 物品名:"船套装" 制造描述:"让大海成为你的领地。"
        STEERINGWHEEL_ITEM = "它能做成方向舵。",		-- 物品名:"方向舵套装" 制造描述:"航海必备道具。"
        ANCHOR_ITEM = "现在妾身能造出锚了。",		-- 物品名:"锚套装" 制造描述:"船用刹车"
        MAST_ITEM = "现在妾身能造出桅杆了。",		-- 物品名:"桅杆套装" 制造描述:"乘风破浪会有时。"
        MUTATEDHOUND =
        {
        	DEAD = "不过尔尔。",		-- 物品名:"恐怖猎犬"->死了
        	GENERIC = "救救妾身！妾身非常的害怕~",		-- 物品名:"恐怖猎犬"->默认
        	SLEEPING = "你的灵魂在哪里。",		-- 物品名:"恐怖猎犬"->睡着了
        },
        MUTATED_PENGUIN =
        {
			DEAD = "它走到了尽头。",		-- 物品名:"月岩企鸥"->死了
			GENERIC = "那东西真可怕！",		-- 物品名:"月岩企鸥"->默认
			SLEEPING = "谢天谢地，它在睡觉。",		-- 物品名:"月岩企鸥"->睡着了
		},
        CARRAT =
        {
        	DEAD = "它走到了尽头。",		-- 物品名:"胡萝卜鼠"->死了 制造描述:"灵巧机敏，富含胡萝卜素。"
        	GENERIC = "胡萝卜怎么还长腿了？",		-- 物品名:"胡萝卜鼠"->默认 制造描述:"灵巧机敏，富含胡萝卜素。"
        	HELD = "近看真是丑。",		-- 物品名:"胡萝卜鼠"->拿在手里 制造描述:"灵巧机敏，富含胡萝卜素。"
        	SLEEPING = "有点可爱了。",		-- 物品名:"胡萝卜鼠"->睡着了 制造描述:"灵巧机敏，富含胡萝卜素。"
        },
		BULLKELP_PLANT =
        {
            GENERIC = "不赖嘛，咸咸的海带。",		-- 物品名:"公牛海带"->默认
            PICKED = "海带的味道还不错。",		-- 物品名:"公牛海带"->被采完了
        },
		BULLKELP_ROOT = "可以种在深海里。",		-- 物品名:"公牛海带茎"
        KELPHAT = "有时为了好受点，必须先难受一下。",		-- 物品名:"海花冠" 制造描述:"让人神经焦虑的东西。"
		KELP = "把妾身的口袋全弄湿了，好恶心。",		-- 物品名:"海带叶"
		KELP_COOKED = "熟了以后有点稀，黏黏的。",		-- 物品名:"熟海带叶"
		KELP_DRIED = "好咸，上面全是盐。",		-- 物品名:"干海带叶"
		GESTALT = "好像能给妾身...知识。",		-- 物品名:"虚影"
        GESTALT_GUARD = "它跟妾身保证……。",		-- 物品名:"大虚影"
		COOKIECUTTER = "妾身不喜欢它盯着船看的眼神……",		-- 物品名:"饼干切割机"
		COOKIECUTTERSHELL = "曾经尖牙利齿，如今只剩空壳。",		-- 物品名:"饼干切割机壳"
		COOKIECUTTERHAT = "至少不会打湿妾身的头发。",		-- 物品名:"饼干切割机帽子" 制造描述:"穿着必须犀利。"
		SALTSTACK =
		{
			GENERIC = "是天然形成的吗？",		-- 物品名:"盐堆"->默认
			MINED_OUT = "挖过了……全挖过了！",		-- 物品名:"盐堆"
			GROWING = "妾身猜它自己就是会长成这样。",		-- 物品名:"盐堆"->正在生长
		},
		SALTROCK = "妾身只吃研磨过的细盐。",		-- 物品名:"盐晶"
		SALTBOX = "防止食物变质的灵药！",		-- 物品名:"盐盒" 制造描述:"用盐来储存食物。"
		TACKLESTATION = "是时候解决海钓的问题了。",		-- 物品名:"钓具容器" 制造描述:"传统的用饵钓鱼。"
		TACKLESKETCH = "某种钓具的图片。妾身说不定能做个更好的……",		-- 物品名:"{item}广告"
        MALBATROSS = "禽鸟巨兽！",		-- 物品名:"邪天翁"
        MALBATROSS_FEATHER = "妾身想要用这个编制一件披风。",		-- 物品名:"邪天翁羽毛"
        MALBATROSS_BEAK = "闻起来有鱼腥味。",		-- 物品名:"邪天翁喙"
        MAST_MALBATROSS_ITEM = "它比看上去还轻。",		-- 物品名:"飞翼风帆套装" 制造描述:"像海鸟一样航向深蓝。"
        MAST_MALBATROSS = "展翅远航！",		-- 物品名:"飞翼风帆" 制造描述:"像海鸟一样航向深蓝。"
		MALBATROSS_FEATHERED_WEAVE = "妾身在织羽毛被！",		-- 物品名:"羽毛帆布" 制造描述:"精美的羽毛布料。"
        GNARWAIL =
        {
            GENERIC = "天啊，你的角真大。",		-- 物品名:"一角鲸"->默认
            BROKENHORN = "你的鼻子没了！",		-- 物品名:"一角鲸"
            FOLLOWER = "鲸鱼，挺好的。",		-- 物品名:"一角鲸"->追随者
            BROKENHORN_FOLLOWER = "这就是你到处伸鼻子的下场！",		-- 物品名:"一角鲸"
        },
        GNARWAIL_HORN = "不可思议！",		-- 物品名:"一角鲸的角"
        WALKINGPLANK = "妾身就不能造一艘救生船吗？",		-- 物品名:"木板"
        OAR = "手动加速。",		-- 物品名:"桨" 制造描述:"划，划，划小船。"
		OAR_DRIFTWOOD = "手动加速船只。",		-- 物品名:"浮木桨" 制造描述:"小桨要用浮木造？"
		OCEANFISHINGROD = "这才是货真价实的海钓竿！",		-- 物品名:"海钓竿" 制造描述:"像职业选手一样钓鱼吧。"
		OCEANFISHINGBOBBER_NONE = "可以用浮标提高准度。",		-- 物品名:"鱼钩"
        OCEANFISHINGBOBBER_BALL = "鱼群会喜欢这个球的。",		-- 物品名:"木球浮标" 制造描述:"经典设计，初学者和职业钓手两相宜。"
        OCEANFISHINGBOBBER_OVAL = "那些鱼这次休想逃出妾身的手掌心！",		-- 物品名:"硬物浮标" 制造描述:"在经典浮标的基础上融入了时尚设计。"
		OCEANFISHINGBOBBER_CROW = "妾身吃鱼，不吃乌鸦。",		-- 物品名:"黑羽浮标" 制造描述:"深受职业钓手的喜爱。"
		OCEANFISHINGBOBBER_ROBIN = "希望别引来红鲱鱼。",		-- 物品名:"红羽浮标" 制造描述:"深受职业钓手的喜爱。"
		OCEANFISHINGBOBBER_ROBIN_WINTER = "雪鸟的羽毛十分高冷。",		-- 物品名:"蔚蓝羽浮标" 制造描述:"深受职业钓手的喜爱。"
		OCEANFISHINGBOBBER_CANARY = "快来跟妾身的小伙伴问好！",		-- 物品名:"黄羽浮标" 制造描述:"深受职业钓手的喜爱。"
		OCEANFISHINGBOBBER_GOOSE = "你要完了，鱼！",		-- 物品名:"鹅羽浮标" 制造描述:"高级羽绒浮标。"
		OCEANFISHINGBOBBER_MALBATROSS = "这么漂亮的羽毛，见过没。",		-- 物品名:"邪天翁羽浮标" 制造描述:"高级巨鸟浮标。"
		OCEANFISHINGLURE_SPINNER_RED = "也许有鱼会上当！",		-- 物品名:"日出旋转亮片" 制造描述:"早起的鱼儿有虫吃。"
		OCEANFISHINGLURE_SPINNER_GREEN = "也许有鱼会上当！",		-- 物品名:"黄昏旋转亮片" 制造描述:"低光照环境里效果最好。"
		OCEANFISHINGLURE_SPINNER_BLUE = "也许有鱼会上当！",		-- 物品名:"夜间旋转亮片" 制造描述:"适用于夜间垂钓。"
		OCEANFISHINGLURE_SPOON_RED = "也许有小鱼会上当！",		-- 物品名:"日出匙型假饵" 制造描述:"早起的鱼儿有虫吃。"
		OCEANFISHINGLURE_SPOON_GREEN = "也许有小鱼会上当！",		-- 物品名:"黄昏匙型假饵" 制造描述:"在夕阳中继续垂钓。"
		OCEANFISHINGLURE_SPOON_BLUE = "也许有小鱼会上当！",		-- 物品名:"夜间匙型假饵" 制造描述:"适用于夜间垂钓。"
		OCEANFISHINGLURE_HERMIT_RAIN = "把自己弄湿能帮助妾身像鱼一样思考……",		-- 物品名:"雨天鱼饵" 制造描述:"留着雨天用。"
		OCEANFISHINGLURE_HERMIT_SNOW = "鱼儿都不会知道是谁下的手！",		-- 物品名:"雪天鱼饵" 制造描述:"雪天适合用它钓鱼。"
		OCEANFISHINGLURE_HERMIT_DROWSY = "上面涂了没有杀伤力的毒！",		-- 物品名:"麻醉鱼饵" 制造描述:"把鱼闷住就抓住了一半。"
		OCEANFISHINGLURE_HERMIT_HEAVY = "感觉有些沉重。",		-- 物品名:"重量级鱼饵" 制造描述:"钓一条大鱼！"
		OCEANFISH_SMALL_1 = "看起来是个小家伙。",		-- 物品名:"小孔雀鱼"
		OCEANFISH_SMALL_2 = "这条不会给妾身带来吹嘘的资本。",		-- 物品名:"针鼻喷墨鱼"
		OCEANFISH_SMALL_3 = "还没长开。",		-- 物品名:"小饵鱼"
		OCEANFISH_SMALL_4 = "小鱼小虾，成不了大气候。",		-- 物品名:"三文鱼苗"
		OCEANFISH_SMALL_5 = "妾身等不及一口吞了它！",		-- 物品名:"爆米花鱼"
		OCEANFISH_SMALL_6 = "“叶”见为食啊。",		-- 物品名:"落叶比目鱼"
		OCEANFISH_SMALL_7 = "总算给妾身抓到开花的鱼了！",		-- 物品名:"花朵金枪鱼"
		OCEANFISH_SMALL_8 = "烫得很呢！",		-- 物品名:"炽热太阳鱼"
        OCEANFISH_SMALL_9 = "只是随便说说，不过妾身应该能让你派上用场……",		-- 物品名:"口水鱼"
		OCEANFISH_MEDIUM_1 = "看着丑，希望味道好吃点吧。",		-- 物品名:"泥鱼"
		OCEANFISH_MEDIUM_2 = "花大力气钓的。",		-- 物品名:"斑鱼"
		OCEANFISH_MEDIUM_3 = "妾身钓鱼的技术可不是盖的！",		-- 物品名:"浮夸狮子鱼"
		OCEANFISH_MEDIUM_4 = "不知道这个会不会带来厄运。",		-- 物品名:"黑鲶鱼"
		OCEANFISH_MEDIUM_5 = "这条鱼看着有些俗气。",		-- 物品名:"玉米鳕鱼"
		OCEANFISH_MEDIUM_6 = "好一条大肥鱼！",		-- 物品名:"花锦鲤"
		OCEANFISH_MEDIUM_7 = "好一条大肥鱼！",		-- 物品名:"金锦鲤"
		OCEANFISH_MEDIUM_8 = "冰鲷鱼。",		-- 物品名:"冰鲷鱼"
        OCEANFISH_MEDIUM_9 = "成功钓鱼之旅的甜蜜味道。",		-- 物品名:"甜味鱼"
		PONDFISH = "妾身现在要吃上一整天。",		-- 物品名:"淡水鱼"
		PONDEEL = "这能做一道好菜。",		-- 物品名:"活鳗鱼"
        FISHMEAT = "一块鱼肉。",		-- 物品名:"生鱼肉"
        FISHMEAT_COOKED = "烤得恰到好处。",		-- 物品名:"鱼排"
        FISHMEAT_SMALL = "一点点鱼肉。",		-- 物品名:"小鱼块"
        FISHMEAT_SMALL_COOKED = "一点点烤鱼肉。",		-- 物品名:"烤小鱼块"
		SPOILED_FISH = "妾身不太想知道那种气味是怎么来的。",		-- 物品名:"变质的鱼"
		FISH_BOX = "它们像沙丁鱼一样塞得满满的！",		-- 物品名:"锡鱼罐" 制造描述:"保持鱼与网捕之日一样新鲜。"
        POCKET_SCALE = "称称看孩儿们长大了没有。",		-- 物品名:"弹簧秤" 制造描述:"随时称鱼的重量！"
		TACKLECONTAINER = "这件额外的储存工具钩住了妾身的注意力。",		-- 物品名:"钓具箱" 制造描述:"整齐收纳你的钓具。"
		SUPERTACKLECONTAINER = "妾身费了九牛二虎之力才弄到手的。",		-- 物品名:"超级钓具箱" 制造描述:"更多收纳钓具的空间？妾身上钩了！"
		TROPHYSCALE_FISH =
		{
			GENERIC = "来瞻仰妾身今天的海钓成绩吧！",		-- 物品名:"鱼类计重器"->默认 制造描述:"炫耀你的斩获。"
			HAS_ITEM = "重量: {weight}\n捕获人: {owner}",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获。"
			HAS_ITEM_HEAVY = "重量: {weight}\n捕获人: {owner}\n所获颇丰!",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获。"
			BURNING = "如果要给火爆程度打分……全烧起来了。",		-- 物品名:"鱼类计重器"->正在燃烧 制造描述:"炫耀你的斩获。"
			BURNT = "妾身所有吹嘘的资本全都给烧没了！",		-- 物品名:"鱼类计重器"->烧焦的 制造描述:"炫耀你的斩获。"
			OWNER = "完全没有炫技的意思，大家就随便看看……\n重量: {weight}\n捕获人: {owner}",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获。"
			OWNER_HEAVY = "重量: {weight}\n捕获人: {owner}\n还真抓了条大肥鱼！",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获。"
		},
		OCEANFISHABLEFLOTSAM = "长了些泥巴的草。",		-- 物品名:"海洋残骸"
		CALIFORNIAROLL = "但妾身没有筷子。",		-- 物品名:"加州卷"
		SEAFOODGUMBO = "一大碗海鲜浓汤。",		-- 物品名:"海鲜浓汤"
		SURFNTURF = "太完美了！",		-- 物品名:"海鲜牛排"
        WOBSTER_SHELLER = "好一只调皮的龙虾。",		-- 物品名:"龙虾"
        WOBSTER_DEN = "一块石头，里面有龙虾。",		-- 物品名:"龙虾窝"
        WOBSTER_SHELLER_DEAD = "这烹饪出来应该挺不错的。",		-- 物品名:"死龙虾"
        WOBSTER_SHELLER_DEAD_COOKED = "妾身等不及要吃了你。",		-- 物品名:"美味的龙虾"
        LOBSTERBISQUE = "可以再加点盐，但是不关妾身的事。",		-- 物品名:"龙虾汤"
        LOBSTERDINNER = "如果妾身早上吃它，它还算正餐吗？",		-- 物品名:"龙虾正餐"
        WOBSTER_MOONGLASS = "好一只调皮的月光龙虾。",		-- 物品名:"月光龙虾"
        MOONGLASS_WOBSTER_DEN = "一块月光玻璃，里面有月光龙虾。",		-- 物品名:"月光玻璃窝"
		TRIDENT = "等着轰轰轰吧！",		-- 物品名:"刺耳三叉戟" 制造描述:"在汹涌波涛中引领潮流吧！"
		WINCH =
		{
			GENERIC = "这个在必要时能派上用场。",		-- 物品名:"夹夹绞盘"->默认 制造描述:"让它帮你捞起重的东西吧。"
			RETRIEVING_ITEM = "重活就交给它干吧。",		-- 物品名:"夹夹绞盘" 制造描述:"让它帮你捞起重的东西吧。"
			HOLDING_ITEM = "看看这里是什么东西？",		-- 物品名:"夹夹绞盘" 制造描述:"让它帮你捞起重的东西吧。"
		},
        HERMITHOUSE = 
        {
            GENERIC = "这是一座空壳房子。",		-- 物品名:"隐士之家"->默认
            BUILTUP = "它只需要一点点的爱。",		-- 物品名:"隐士之家"
        },
        SHELL_CLUSTER = "妾身打赌里面有不少漂亮贝壳。",		-- 物品名:"贝壳堆"
		SINGINGSHELL_OCTAVE3 =
		{
			GENERIC = "调有点低。",		-- 物品名:"低音贝壳钟"->默认
		},
		SINGINGSHELL_OCTAVE4 =
		{
			GENERIC = "这就是大海的声音吗？",		-- 物品名:"中音贝壳钟"->默认
		},
		SINGINGSHELL_OCTAVE5 =
		{
			GENERIC = "它可以发出高音C。",		-- 物品名:"高音贝壳钟"->默认
        },
        CHUM = "鱼儿的美味佳肴！",		-- 物品名:"鱼食" 制造描述:"鱼儿的美食。"
        SUNKENCHEST =
        {
            GENERIC = "老龙王的宝贝？",		-- 物品名:"沉底宝箱"->默认
            LOCKED = "贝壳闭上了！",		-- 物品名:"沉底宝箱"->锁住了
        },
        HERMIT_BUNDLE = "这些东西她有的是。",		-- 物品名:"一包谢意"
        HERMIT_BUNDLE_SHELLS = "她确实是卖海贝壳的！",		-- 物品名:"贝壳钟包" 制造描述:"她卖的贝壳。"
        RESKIN_TOOL = "妾身喜欢干干净净的家！",		-- 物品名:"清洁扫把" 制造描述:"把装潢的东西清扫的干干净净。"
        MOON_FISSURE_PLUGGED = "虽然不太妙……但是很有效。",		-- 物品名:"堵住的裂隙"
        WOBYBIG =
        {
            "你给她喂了什么？",		-- 物品名:"沃比" 制造描述:未找到
            "你给她喂了什么？",		-- 物品名:"沃比" 制造描述:未找到
        },
        WOBYSMALL =
        {
            "喂养我的小零食。",		-- 物品名:"沃比" 制造描述:未找到
            "喂养我的小零食。",		-- 物品名:"沃比" 制造描述:未找到
        },
		WALTERHAT = "妾身年轻的时候也没喜欢过“户外活动”。",		-- 物品名:"松树先锋队帽子" 制造描述:"形式和功能高于时尚。"
		SLINGSHOT = "所有玻璃的噩梦。",		-- 物品名:"可靠的弹弓" 制造描述:"不带它千万别去冒险！"
		SLINGSHOTAMMO_ROCK = "即将打出的弹药。",		-- 物品名:"鹅卵石" 制造描述:"简单，低效。"
		SLINGSHOTAMMO_MARBLE = "即将打出的弹药。",		-- 物品名:"大理石弹" 制造描述:"可别把它们弄丢了！"
		SLINGSHOTAMMO_THULECITE = "即将打出的弹药。",		-- 物品名:"诅咒弹药" 制造描述:"会出什么问题？"
        SLINGSHOTAMMO_GOLD = "即将打出的弹药。",		-- 物品名:"黄金弹药" 制造描述:"因为高级，所以有效。"
        SLINGSHOTAMMO_SLOW = "即将打出的弹药。",		-- 物品名:"减速弹药" 制造描述:"什么是“物理定律”？"
        SLINGSHOTAMMO_FREEZE = "即将打出的弹药。",		-- 物品名:"冰冻弹药" 制造描述:"把敌人冰冻在原地。"
		SLINGSHOTAMMO_POOP = "粪便弹道。",		-- 物品名:"便便弹" 制造描述:"恶心，但是能让敌人分心。"
        PORTABLETENT = "妾身感觉已经几辈子没睡过好觉了！",		-- 物品名:"宿营帐篷" 制造描述:"便捷的保护设备，让你免受风吹雨打。"
        PORTABLETENT_ITEM = "有了帐篷可不要膨胀。",		-- 物品名:"帐篷卷" 制造描述:"便捷的保护设备，让你免受风吹雨打。"
        BATTLESONG_DURABILITY = "戏剧让妾身坐立不安。",		-- 物品名:"武器化的颤音" 制造描述:"让武器有更多的时间成为焦点。"
        BATTLESONG_HEALTHGAIN = "戏剧让妾身坐立不安。",		-- 物品名:"心碎歌谣" 制造描述:"一首偷心的颂歌。"
        BATTLESONG_SANITYGAIN = "戏剧让妾身坐立不安。",		-- 物品名:"醍醐灌顶华彩" 制造描述:"用歌声慰藉你的心灵。"
        BATTLESONG_SANITYAURA = "戏剧让妾身坐立不安。",		-- 物品名:"英勇美声颂" 制造描述:"无所畏惧！"
        BATTLESONG_FIRERESISTANCE = "妾身还是不想靠火太近。",		-- 物品名:"防火假声" 制造描述:"抵御火辣的戏评人。"
        BATTLESONG_INSTANT_TAUNT = "恐怕妾身不是一个诗人。",		-- 物品名:"粗鲁插曲" 制造描述:"用言语扔一个番茄。"
        BATTLESONG_INSTANT_PANIC = "恐怕妾身不是一个诗人。",		-- 物品名:"惊心独白" 制造描述:"如此出色的表演，就问你怕不怕。"
        MUTATOR_WARRIOR = "哦，哇，这看起来嗯……很好吃，韦伯!",		-- 物品名:"战士变身涂鸦" 制造描述:"做最可爱的小保镖。"
        MUTATOR_DROPPER = "呃，惊喜的小玩意？",		-- 物品名:"悬蛛变身涂鸦" 制造描述:"味道也许会让你惊喜。"
        MUTATOR_HIDER = "哦，哇，这看起来嗯……很好吃，韦伯!",		-- 物品名:"洞穴变身涂鸦" 制造描述:"外酥里嫩。"
        MUTATOR_SPITTER = "多么精巧的小玩意？",		-- 物品名:"喷吐变身涂鸦" 制造描述:"喷薄而出的蜘蛛形象。"
        MUTATOR_MOON = "哦，哇，这看起来嗯……很好吃，韦伯!",		-- 物品名:"破碎变身涂鸦" 制造描述:"吃到它的人会快乐到月球去！"
        MUTATOR_HEALER = "来给妾身按摩按摩？",		-- 物品名:"护士变身涂鸦" 制造描述:"特别多的谷物，所以必须是健康的！"
        SPIDER_WHISTLE = "是你在呼唤妾身吗？",		-- 物品名:"韦伯口哨" 制造描述:"呼叫可怕的爬行动物朋友吧。"
        SPIDERDEN_BEDAZZLER = "看起来某人的手艺提高了不少啊。",		-- 物品名:"蛛巢装饰套装" 制造描述:"一个好的家能培育出好的性格。"
        SPIDER_HEALER = "哦，太好了，你来伺候奴家吧。",		-- 物品名:"护士蜘蛛"
        SPIDER_REPELLENT = "它让奴家感到恶心。",		-- 物品名:"驱赶盒子" 制造描述:"让你的朋友知道你需要一些空间。"
        SPIDER_HEALER_ITEM = "多么美味，那和尚居然觉得恶心。",		-- 物品名:"治疗黏团" 制造描述:"恶心，黏糊糊的，但对你有好处！"
		GHOSTLYELIXIR_SLOWREGEN = "非常奇妙的产物",		-- 物品名:"亡者补药" 制造描述:"时间会抚平所有伤口。"
		GHOSTLYELIXIR_FASTREGEN = "非常奇妙的产物",		-- 物品名:"灵魂万灵药" 制造描述:"治疗重伤的强力药剂。"
		GHOSTLYELIXIR_SHIELD = "非常奇妙的产物",		-- 物品名:"不屈药剂" 制造描述:"保护你的姐妹不受伤害。"
		GHOSTLYELIXIR_ATTACK = "非常奇妙的产物",		-- 物品名:"夜影万金油" 制造描述:"召唤黑暗的力量。"
		GHOSTLYELIXIR_SPEED = "非常奇妙的产物",		-- 物品名:"强健精油" 制造描述:"给你的灵魂来一剂强心针。"
		GHOSTLYELIXIR_RETALIATION = "非常奇妙的产物",		-- 物品名:"蒸馏复仇" 制造描述:"对敌人以牙还牙。"
		SISTURN =
		{
			GENERIC = "弄点花会显得更有生机。",		-- 物品名:"姐妹骨灰罐"->默认 制造描述:"让你疲倦的灵魂休息的地方。"
			SOME_FLOWERS = "多拿些花来应该就可以了。",		-- 物品名:"姐妹骨灰罐" 制造描述:"让你疲倦的灵魂休息的地方。"
			LOTS_OF_FLOWERS = "好美的花束！",		-- 物品名:"姐妹骨灰罐" 制造描述:"让你疲倦的灵魂休息的地方。"
		},
        WORTOX_SOUL = "only_used_by_wortox", --only wortox can inspect souls		-- 物品名:"灵魂"
        PORTABLECOOKPOT_ITEM =
        {
            GENERIC = "这才是烹饪！",		-- 物品名:"便携烹饪锅"->默认 制造描述:"随时随地为美食家服务。"
            DONE = "行，做完饭了！",		-- 物品名:"便携烹饪锅"->完成了 制造描述:"随时随地为美食家服务。"
			COOKING_LONG = "这顿饭需要一点时间了。",		-- 物品名:"便携烹饪锅"->饭还需要很久 制造描述:"随时随地为美食家服务。"
			COOKING_SHORT = "马上就能好！",		-- 物品名:"便携烹饪锅"->饭快做好了 制造描述:"随时随地为美食家服务。"
			EMPTY = "妾身打赌里面什么都没有。",		-- 物品名:"便携烹饪锅" 制造描述:"随时随地为美食家服务。"
        },
        PORTABLEBLENDER_ITEM = "把所有食物混合起来。",		-- 物品名:"便携研磨器" 制造描述:"把原料研磨成粉状调味品。"
        PORTABLESPICER_ITEM =
        {
            GENERIC = "它可以给食物增添辛香风味。",		-- 物品名:"便携香料站"->默认 制造描述:"调味让饭菜更可口。"
            DONE = "应该能让饭菜更可口。",		-- 物品名:"便携香料站"->完成了 制造描述:"调味让饭菜更可口。"
        },
        SPICEPACK = "妾身可不是要穿厨娘装！",		-- 物品名:"厨师袋" 制造描述:"使你的食物保持新鲜。"
        SPICE_GARLIC = "威力强大的粉末。",		-- 物品名:"蒜粉" 制造描述:"用口臭防守是最好的进攻。"
        SPICE_SUGAR = "甜！真甜！",		-- 物品名:"蜂蜜水晶" 制造描述:"令人心平气和的甜美。"
        SPICE_CHILI = "一壶液体火焰。",		-- 物品名:"辣椒面" 制造描述:"刺激十足的粉末。"
        SPICE_SALT = "适量的钠对心脏有好处。",		-- 物品名:"调味盐" 制造描述:"为你的饭菜加点咸味。"
        MONSTERTARTARE = "这儿一定还有别的什么能吃的。",		-- 物品名:"怪物鞑靼"
        FRESHFRUITCREPES = "甜水果！营养均衡的早餐的一部分。",		-- 物品名:"鲜果可丽饼"
        FROGFISHBOWL = "这是鱼里面塞了只青蛙吗？",		-- 物品名:"蓝带鱼排"
        POTATOTORNADO = "啊，多么奇妙的组合！",		-- 物品名:"花式回旋块茎"
        DRAGONCHILISALAD = "希望能承受住辛辣的程度。",		-- 物品名:"辣龙椒沙拉"
        GLOWBERRYMOUSSE = "沃利烧的一手好菜。",		-- 物品名:"发光浆果慕斯"
        VOLTGOATJELLY = "惊人的美味。",		-- 物品名:"伏特羊肉冻"
        NIGHTMAREPIE = "有点吓人啊。",		-- 物品名:"恐怖国王饼"
        BONESOUP = "沃利骨子里是名好厨师。",		-- 物品名:"骨头汤"
        MASHEDPOTATOES = "妾身也应该试试。",		-- 物品名:"奶油土豆泥"
        POTATOSOUFFLE = "妾身都忘记了可口的饭菜是什么味道！",		-- 物品名:"蓬松土豆蛋奶酥"
        MOQUECA = "浓浓心意，尽在其中。",		-- 物品名:"海鲜杂烩"
        GAZPACHO = "爽口冷菜？",		-- 物品名:"芦笋冷汤"
        ASPARAGUSSOUP = "闻起来很好吃。",		-- 物品名:"芦笋汤"
        VEGSTINGER = "你能把芹菜当吸管用吗？",		-- 物品名:"蔬菜鸡尾酒"
        BANANAPOP = "吃得太快了！妾身有点头痛。",		-- 物品名:"香蕉冻"
        CEVICHE = "能弄一只大点的碗吗？这只看起来有点小。",		-- 物品名:"酸橘汁腌鱼"
        SALSA = "真...辣...！",		-- 物品名:"生鲜萨尔萨酱"
        PEPPERPOPPER = "这么一大口！",		-- 物品名:"爆炒填馅辣椒"
        TURNIP = "是个生萝卜。",		-- 物品名:"大萝卜"
        TURNIP_COOKED = "先这样，再那样，然后就好了。",		-- 物品名:"烤大萝卜"
        TURNIP_SEEDS = "一把古怪的种子。",		-- 物品名:"圆形种子"
        GARLIC = "好臭！奇怪的臭味。",		-- 物品名:"大蒜"
        GARLIC_COOKED = "完美的焦黄。",		-- 物品名:"烤大蒜"
        GARLIC_SEEDS = "一把古怪的种子。",		-- 物品名:"种子荚"
        ONION = "看起来很脆。",		-- 物品名:"洋葱"
        ONION_COOKED = "一次成功的化学反应。",		-- 物品名:"烤洋葱"
        ONION_SEEDS = "一把古怪的种子。",		-- 物品名:"尖形种子"
        POTATO = "埋在地下的苹果。",		-- 物品名:"土豆"
        POTATO_COOKED = "一个烤洋芋，啧啧。",		-- 物品名:"烤土豆"
        POTATO_SEEDS = "一把古怪的种子。",		-- 物品名:"毛茸茸的种子"
        TOMATO = "红色的，里面是鲜血吗？",		-- 物品名:"番茄"
        TOMATO_COOKED = "做饭还是挺简单的。",		-- 物品名:"烤番茄"
        TOMATO_SEEDS = "一把古怪的种子。",		-- 物品名:"带刺的种子"
        ASPARAGUS = "一种蔬菜。",		-- 物品名:"芦笋"
        ASPARAGUS_COOKED = "这对妾身有好处。",		-- 物品名:"烤芦笋"
        ASPARAGUS_SEEDS = "就是些种子。",		-- 物品名:"筒状种子"
        PEPPER = "好一个火辣辣的辣椒。",		-- 物品名:"辣椒"
        PEPPER_COOKED = "本来就是辣的。",		-- 物品名:"烤辣椒"
        PEPPER_SEEDS = "一把种子。",		-- 物品名:"块状种子"
        WEREITEM_BEAVER = "妾身猜别的地方有不同的奏效方法。",		-- 物品名:"俗气海狸像" 制造描述:"唤醒海狸人的诅咒"
        WEREITEM_GOOSE = "它让妾身浑身起鸡皮疙瘩。",		-- 物品名:"俗气鹅像" 制造描述:"唤醒鹅人的诅咒"
        WEREITEM_MOOSE = "一头再正常不过的受诅咒的鹿。",		-- 物品名:"俗气鹿像" 制造描述:"唤醒鹿人的诅咒"
        MERMHAT = "一股腥味。",		-- 物品名:"聪明的伪装" 制造描述:"鱼人化你的朋友。"
        MERMTHRONE =
        {
            GENERIC = "配得上沼泽之王的地位！",		-- 物品名:"皇家地毯"->默认
            BURNT = "本来那王座就一股鱼腥味。",		-- 物品名:"皇家地毯"->烧焦的
        },
        MERMTHRONE_CONSTRUCTION =
        {
            GENERIC = "她究竟在盘算什么？",		-- 物品名:"皇家手工套装"->默认 制造描述:"建立一个新的鱼人王朝。"
            BURNT = "永远都不会这道它原来是什么了。",		-- 物品名:"皇家手工套装"->烧焦的 制造描述:"建立一个新的鱼人王朝。"
        },
        MERMHOUSE_CRAFTED =
        {
            GENERIC = "样子还挺可爱的。",		-- 物品名:"鱼人工艺屋"->默认 制造描述:"适合鱼人的家。"
            BURNT = "啊，刺鼻的味道！",		-- 物品名:"鱼人工艺屋"->烧焦的 制造描述:"适合鱼人的家。"
        },
        MERMWATCHTOWER_REGULAR = "他们找到了国王后高兴了起来。",		-- 物品名:"鱼人工艺屋" 制造描述:"适合鱼人的家。"
        MERMWATCHTOWER_NOKING = "士无将可守。",		-- 物品名:"鱼人工艺屋" 制造描述:"适合鱼人的家。"
        MERMKING = "国王陛下！",		-- 物品名:"鱼人之王"
        MERMGUARD = "在他们周围感觉受到了护卫……",		-- 物品名:"忠诚鱼人守卫"
        MERM_PRINCE = "他们遵循先到先得的规矩。",		-- 物品名:"过程中的皇室"
        SQUID = "妾身有预感会派上用场。",		-- 物品名:"鱿鱼"
		GHOSTFLOWER = "妾身不觉的这是荣耀。",		-- 物品名:"哀悼荣耀"
        SMALLGHOST = "嗷，你是不是个小可爱啊？",		-- 物品名:"小惊吓"
        CRABKING =
        {
            GENERIC = "咦！妾身想尝尝大螃蟹了。",		-- 物品名:"帝王蟹"->默认
            INERT = "你的家需要重新装修一下。",		-- 物品名:"帝王蟹"
        },
		CRABKING_CLAW = "警惕！",		-- 物品名:"巨钳"
		MESSAGEBOTTLE = "不知道是不是送给妾身的！",		-- 物品名:"瓶中信"
		MESSAGEBOTTLEEMPTY = "空无一物。",		-- 物品名:"空瓶子"
        BEEBOX_HERMIT =
        {
            READY = "它里面装满了蜂蜜。",		-- 物品名:"蜂箱"->准备好的 满的
            FULLHONEY = "甜蜜蜜在等着妾身。",		-- 物品名:"蜂箱"->蜂蜜满了
            GENERIC = "不知道里面有没有些许甜蜜。",		-- 物品名:"蜂箱"->默认
            NOHONEY = "它是空的。",		-- 物品名:"蜂箱"->没有蜂蜜
            SOMEHONEY = "需要等一会。",		-- 物品名:"蜂箱"->有一些蜂蜜
            BURNT = "怎么被烧掉的？！！",		-- 物品名:"蜂箱"->烧焦的
        },
        HERMITCRAB = "一个人住多少会寂寞吧。",		-- 物品名:"寄居蟹隐士"
        HERMIT_PEARL = "妾身会照顾好它。",		-- 物品名:"珍珠的珍珠"
        HERMIT_CRACKED_PEARL = "妾身……没有照顾好它。",		-- 物品名:"开裂珍珠"
        WATERPLANT = "只要妾身不拿它们的藤壶，它们就会跟妾身做朋友。",		-- 物品名:"海草"
        WATERPLANT_BOMB = "是种子啊！",		-- 物品名:"种壳"
        WATERPLANT_BABY = "还在萌芽阶段。",		-- 物品名:"海芽"
        WATERPLANT_PLANTER = "它们在大海中的岩石上长的最好。",		-- 物品名:"海芽插穗"
        SHARK = "妾身可能需要一艘更大的船……",		-- 物品名:"岩石大白鲨"
        MASTUPGRADE_LAMP_ITEM = "妾身有的都是闪闪发光的好主意。",		-- 物品名:"甲板照明灯" 制造描述:"桅杆照明附件。"
        MASTUPGRADE_LIGHTNINGROD_ITEM = "吸收天雷的力量！",		-- 物品名:"避雷导线" 制造描述:"为你的桅杆带来过电般的刺激。"
        WATERPUMP = "可以非常有效的灭火。",		-- 物品名:"消防泵" 制造描述:"水水水，到处都是水！"
        BATNOSEHAT = "有了它喝奶都不用动手了。",		-- 物品名:"牛奶帽"
        MUSHGNOME = "它也许会周期性的发起攻击。",		-- 物品名:"蘑菇地精"
        SPORE_MOON = "妾身要尽量离这些孢子远一点。",		-- 物品名:"月亮孢子"
        MOON_CAP = "看起来不怎么好吃。",		-- 物品名:"月亮蘑菇"
        MOON_CAP_COOKED = "更糟糕了……",		-- 物品名:"熟月亮蘑菇"
        MUSHTREE_MOON = "这颗蘑菇树明显跟其他的有异。",		-- 物品名:"月亮蘑菇树"
        LIGHTFLIER = "真是奇怪，装在妾身的口袋里居然还能发光！",		-- 物品名:"球状光虫"
        GROTTO_POOL_BIG = "月华凝聚。",		-- 物品名:"玻璃绿洲"
        GROTTO_POOL_SMALL = "月华凝聚。",		-- 物品名:"小玻璃绿洲"
        DUSTMOTH = "真是些整齐的小家伙，不是吗？",		-- 物品名:"尘蛾"
        DUSTMOTHDEN = "它们在里面过得很舒服。",		-- 物品名:"整洁洞穴"
        ARCHIVE_LOCKBOX = "妾身怎么把知识取出来呢？",		-- 物品名:"蒸馏的知识"
        ARCHIVE_CENTIPEDE = "你休想阻止妾身进步！",		-- 物品名:"远古哨兵蜈蚣"
        ARCHIVE_CENTIPEDE_HUSK = "一堆破铜烂铁。",		-- 物品名:"哨兵蜈蚣壳"
        ARCHIVE_COOKPOT =
        {
            COOKING_LONG = "这还需要一点时间。",		-- 物品名:"远古窑"->饭还需要很久
            COOKING_SHORT = "就快好了！",		-- 物品名:"远古窑"->饭快做好了
            DONE = "嗯！可以吃了！",		-- 物品名:"远古窑"->完成了
            EMPTY = "把上面的灰尘先抖干净，怎么样？",		-- 物品名:"远古窑"
            BURNT = "锅给烧没了。",		-- 物品名:"远古窑"->烧焦的
        },
        ARCHIVE_MOON_STATUE = "这些宏伟的月亮雕像让妾身充满了诗意。",		-- 物品名:"远古月亮雕像"
        ARCHIVE_RUNE_STATUE =
        {
            LINE_1 = "如此多的知识，妾身要是能看懂就好了！",		-- 物品名:"远古月亮符文石"->第一行
            LINE_2 = "这些标记看起来与其他废墟中的标记不同。",		-- 物品名:"远古月亮符文石"->第二行
            LINE_3 = "如此多的知识，妾身要是能看懂就好了！",		-- 物品名:"远古月亮符文石"->第三行
            LINE_4 = "这些标记看起来与其他废墟中的标记不同。",		-- 物品名:"远古月亮符文石"->第四行
            LINE_5 = "如此多的知识，妾身要是能看懂就好了！",		-- 物品名:"远古月亮符文石"->第五行
        },
        ARCHIVE_RESONATOR = 
        {
            GENERIC = "它在找什么？",		-- 物品名:"天体探测仪"->默认
            IDLE = "找到了吗？",		-- 物品名:"天体探测仪"
        },
        ARCHIVE_RESONATOR_ITEM = "啊哈！妾身……这是造出来了个什么？",		-- 物品名:"天体探测仪" 制造描述:"它会出土什么秘密呢？"
        ARCHIVE_LOCKBOX_DISPENCER = 
        {
          POWEROFF = "要是有办法让它重新运作就好了……",		-- 物品名:"知识饮水机"
          GENERIC =  "不出水，那要你何用？",		-- 物品名:"知识饮水机"->默认
        },
        ARCHIVE_SECURITY_DESK = 
        {
            POWEROFF = "它已经失去了从前的作用。",		-- 物品名:"远古哨所"
            GENERIC = "它想亲近妾身。",		-- 物品名:"远古哨所"->默认
        },
        ARCHIVE_SECURITY_PULSE = "你这是要上哪去？是个有意思的地方吗？",		-- 物品名:"远古哨所"
        ARCHIVE_SWITCH = 
        {
            VALID = "它看起来是由这些宝石供能……。",		-- 物品名:"华丽基座"->有效
            GEMS = "这个插槽是空的。",		-- 物品名:"华丽基座"->需要宝石
        },
        ARCHIVE_PORTAL = 
        {
            POWEROFF = "一点反应都没。",		-- 物品名:"封印的传送门"
            GENERIC = "奇怪，它完全没反应。",		-- 物品名:"封印的传送门"->默认
        },
        WALL_STONE_2 = "不错的墙。",		-- 物品名:"档案馆石墙"
        WALL_RUINS_2 = "一段古老的墙。",		-- 物品名:"档案馆铥墙"
        REFINED_DUST = "啊——嚏！",		-- 物品名:"尘土块" 制造描述:"远古甜品的关键原料。"
        DUSTMERINGUE = "这玩意谁会愿意吃？",		-- 物品名:"琥珀美食"
        SHROOMCAKE = "果然名副其实。",		-- 物品名:"蘑菇蛋糕"
        NIGHTMAREGROWTH = "这些晶体值得引起妾身的警惕。",		-- 物品名:"梦魇城墙"
        TURFCRAFTINGSTATION = "只需要一点点技巧！",		-- 物品名:"土地夯实器" 制造描述:"一点点的改变世界。"
        MOON_ALTAR_LINK = "似乎在酝酿什么。",		-- 物品名:"神秘能量"
        COMPOSTINGBIN =
        {
            GENERIC = "妾身真是有点受不了这个味道。",		-- 物品名:"堆肥桶"->默认 制造描述:"能让土壤变得臭烘烘和肥沃。"
            WET = "看起来太湿了。",		-- 物品名:"堆肥桶" 制造描述:"能让土壤变得臭烘烘和肥沃。"
            DRY = "嗯……太干了。",		-- 物品名:"堆肥桶" 制造描述:"能让土壤变得臭烘烘和肥沃。"
            BALANCED = "恰到好处！",		-- 物品名:"堆肥桶" 制造描述:"能让土壤变得臭烘烘和肥沃。"
            BURNT = "妾身真没想到它还能发出更糟糕的味道……",		-- 物品名:"堆肥桶"->烧焦的 制造描述:"能让土壤变得臭烘烘和肥沃。"
        },
        COMPOST = "植物的食物，派不上别的用场。",		-- 物品名:"堆肥"
        SOIL_AMENDER =
		{
			GENERIC = "现在妾身就等着它的成长。",		-- 物品名:"催长剂起子"->默认 制造描述:"海里来的瓶装养分。"
			STALE = "它在制造养分!",		-- 物品名:"催长剂起子"->过期了 制造描述:"海里来的瓶装养分。"
			SPOILED = "那股令人作呕的味道！",		-- 物品名:"催长剂起子"->腐烂了 制造描述:"海里来的瓶装养分。"
		},
		SOIL_AMENDER_FERMENTED = "这可是很强的力量！",		-- 物品名:"超级催长剂"
        WATERINGCAN =
        {
            GENERIC = "妾身可以用这个给植物浇水。",		-- 物品名:"空浇水壶"->默认 制造描述:"让植物保持快乐和水分。"
            EMPTY = "也许应该先打一口井...",		-- 物品名:"空浇水壶" 制造描述:"让植物保持快乐和水分。"
        },
        PREMIUMWATERINGCAN =
        {
            GENERIC = "满壶的水！",		-- 物品名:"空鸟嘴壶"->默认 制造描述:"灌溉方面的创新!"
            EMPTY = "没有水就没用了。",		-- 物品名:"空鸟嘴壶" 制造描述:"灌溉方面的创新!"
        },
		FARM_PLOW = "方便的地块设备。",		-- 物品名:"耕地机"
		FARM_PLOW_ITEM = "妾身得先找个好的种地的园子才能使用它。",		-- 物品名:"耕地机" 制造描述:"为你的植物犁一块地。"
		FARM_HOE = "妾身得把地弄的让种子更舒服一些。",		-- 物品名:"园艺锄" 制造描述:"翻耕，为撒播农作物做准备。"
		GOLDEN_FARM_HOE = "妾身真的需要这么花哨的东西来搬土吗？",		-- 物品名:"黄金园艺锄" 制造描述:"优雅地耕地。"
		NUTRIENTSGOGGLESHAT = "它将帮助妾身看到所有藏在泥土里的秘密。",		-- 物品名:"高级耕作先驱帽" 制造描述:"让你看到一个成功的花园。"
		PLANTREGISTRYHAT = "要了解植物，就必须戴上植物。",		-- 物品名:"耕作先驱帽" 制造描述:"让你的园艺专业知识得到增长。"
        FARM_SOIL_DEBRIS = "它把妾身的园子弄得一团糟。",		-- 物品名:"农田杂物"
		FIRENETTLES = "如果你受不了热，就不要在花园里呆着。",		-- 物品名:"火荨麻叶"
		FORGETMELOTS = "嗯，妾身不记得妾身想说什么了。",		-- 物品名:"必忘妾身"
		SWEETTEA = "一杯好茶，让妾身忘记所有的问题。",		-- 物品名:"舒缓茶"
		TILLWEED = "你给妾身滚出妾身的园子！",		-- 物品名:"犁地草"
		TILLWEEDSALVE = "妾身的救赎。",		-- 物品名:"犁地草膏" 制造描述:"慢慢去处病痛。"
        WEED_IVY = "嘿，你这讨厌的杂草！",		-- 物品名:"刺针旋花"
        IVY_SNARE = "真是没礼貌。",		-- 物品名:"缠绕根须"
		TROPHYSCALE_OVERSIZEDVEGGIES =
		{
			GENERIC = "妾身可以测算出收获的薄厚。",		-- 物品名:"农产品秤"->默认 制造描述:"称量你珍贵的水果和蔬菜。"
			HAS_ITEM = "重量: {weight}\n收获日: {day}\n不赖。",		-- 物品名:"农产品秤" 制造描述:"称量你珍贵的水果和蔬菜。"
			HAS_ITEM_HEAVY = "重量: {weight}\n收获日: {day}\n谁能想到会长这么大？",		-- 物品名:"农产品秤" 制造描述:"称量你珍贵的水果和蔬菜。"
            HAS_ITEM_LIGHT = "它太一般了，秤都懒得告诉妾身它的重量。",		-- 物品名:"农产品秤" 制造描述:"称量你珍贵的水果和蔬菜。"
			BURNING = "这是烧什么菜啊？",		-- 物品名:"农产品秤"->正在燃烧 制造描述:"称量你珍贵的水果和蔬菜。"
			BURNT = "妾身想这不是最好的烹饪方式。",		-- 物品名:"农产品秤"->烧焦的 制造描述:"称量你珍贵的水果和蔬菜。"
        },
        CARROT_OVERSIZED = "千万别让兔子精看见!",		-- 物品名:"巨型胡萝卜"
        CORN_OVERSIZED = "好大的玉米棒！爆米花又要来了。",		-- 物品名:"巨型玉米"
        PUMPKIN_OVERSIZED = "好膨胀的南瓜啊。",		-- 物品名:"巨型南瓜"
        EGGPLANT_OVERSIZED = "太大了一点...",		-- 物品名:"巨型茄子"
        DURIAN_OVERSIZED = "妾身很确定它臭味更大。",		-- 物品名:"巨型榴莲"
        POMEGRANATE_OVERSIZED = "这可能是妾身见过的最大的石榴。",		-- 物品名:"巨型石榴"
        DRAGONFRUIT_OVERSIZED = "你会飞起来吗？",		-- 物品名:"巨型火龙果"
        WATERMELON_OVERSIZED = "又大又多汁的西瓜。",		-- 物品名:"巨型西瓜"
        TOMATO_OVERSIZED = "一颗比例惊人的西红柿。",		-- 物品名:"巨型番茄"
        POTATO_OVERSIZED = "这土豆真大。",		-- 物品名:"巨型土豆"
        ASPARAGUS_OVERSIZED = "给妾身来一份冷盘……",		-- 物品名:"巨型芦笋"
        ONION_OVERSIZED = "它们这么快就长这么大了",		-- 物品名:"巨型洋葱"
        GARLIC_OVERSIZED = "大蒜！呕...",		-- 物品名:"巨型大蒜"
        PEPPER_OVERSIZED = "一种大小相当不寻常的辣椒。",		-- 物品名:"巨型辣椒"
        VEGGIE_OVERSIZED_ROTTEN = "腐烂的霉运。",		-- 物品名:"农产品秤" 制造描述:"称量你珍贵的水果和蔬菜。"
		FARM_PLANT =
		{
			GENERIC = "那是一株植物！",		-- 物品名:未找到
			SEED = "现在等着就好了。",		-- 物品名:未找到
			GROWING = "生长吧我的花儿们，生长吧！",		-- 物品名:未找到
			FULL = "是时候收获成果了。",		-- 物品名:未找到
			ROTTEN = "可惜啊！要是在成熟的时候收割就好了!",		-- 物品名:未找到
			FULL_OVERSIZED = "妾身用一点点力量!",		-- 物品名:未找到
			ROTTEN_OVERSIZED = "腐烂的霉运。",		-- 物品名:未找到
			FULL_WEED = "妾身早就想到了斩草除根！",		-- 物品名:未找到
			BURNING = "这对植物来说不是好事……",		-- 物品名:未找到
		},
        FRUITFLY = "到别的地方嗡嗡去！",		-- 物品名:"果蝇"
        LORDFRUITFLY = "嘿，别再来妾身的花园！",		-- 物品名:"果蝇王"
        FRIENDLYFRUITFLY = "有它在，花园似乎更开心了。",		-- 物品名:"友好果蝇"
        FRUITFLYFRUIT = "现在妾身收下你这个仆人了！",		-- 物品名:"友好果蝇果"
        SEEDPOUCH = "妾身早想要一个了。",		-- 物品名:"种子袋" 制造描述:"妥善保管好种子。"
		CARNIVAL_HOST = "是个怪人。",		-- 物品名:"良羽鸦"
		CARNIVAL_CROWKID = "日安，小鸟人。",		-- 物品名:"小乌鸦"
		CARNIVAL_GAMETOKEN = "一枚闪亮的代币。",		-- 物品名:"鸦年华代币" 制造描述:"购买代币，玩游戏，赢取奖品！"
		CARNIVAL_PRIZETICKET =
		{
			GENERIC = "就是那张奖票！",		-- 物品名:"奖票"->默认
			GENERIC_SMALLSTACK = "就是那些奖票！",		-- 物品名:"奖票"
			GENERIC_LARGESTACK = "好多的奖票啊！",		-- 物品名:"奖票"
		},
		CARNIVALGAME_FEEDCHICKS_NEST = "这是一扇小活门。",		-- 物品名:"饥饿乌鸦"
		CARNIVALGAME_FEEDCHICKS_STATION =
		{
			GENERIC = "它要妾身给它一些闪亮的东西才让妾身玩。",		-- 物品名:"鸟鸟吃虫虫"->默认
			PLAYING = "这个看起来有趣喔！",		-- 物品名:"鸟鸟吃虫虫"
		},
		CARNIVALGAME_FEEDCHICKS_KIT = "这还真是个快闪嘉年华。",		-- 物品名:"鸟鸟吃虫虫套装" 制造描述:"小鸟吃虫！"
		CARNIVALGAME_FEEDCHICKS_FOOD = "妾身不需要先把它们嚼碎，对吧？",		-- 物品名:"蛴螬"
		CARNIVALGAME_MEMORY_KIT = "这还真是个快闪嘉年华。",		-- 物品名:"篮中蛋套装" 制造描述:"在你的蛋孵化前数清楚数量。"
		CARNIVALGAME_MEMORY_STATION =
		{
			GENERIC = "它要妾身给它一些闪亮的东西才让妾身玩。",		-- 物品名:"篮中蛋"->默认
			PLAYING = "不是妾身吹牛，妾身是这方面的行家。",		-- 物品名:"篮中蛋"
		},
		CARNIVALGAME_MEMORY_CARD =
		{
			GENERIC = "这是一扇小活门。",		-- 物品名:"蛋篮"->默认
			PLAYING = "是它吗？",		-- 物品名:"蛋篮"
		},
		CARNIVALGAME_HERDING_KIT = "这还真是个快闪嘉年华。",		-- 物品名:"追蛋套裝" 制造描述:"追回失控的蛋。"
		CARNIVALGAME_HERDING_STATION =
		{
			GENERIC = "它要妾身给它一些闪亮的东西才让妾身玩。",		-- 物品名:"追蛋"->默认
			PLAYING = "这些鸡蛋看起来想跑。",		-- 物品名:"追蛋"
		},
		CARNIVALGAME_HERDING_CHICK = "回来这！",		-- 物品名:"追蛋"
		CARNIVAL_PRIZEBOOTH_KIT = "真正的奖品是妾身制作摊位的过程。",		-- 物品名:"奖品摊位套装" 制造描述:"看看有什么奖品。"
		CARNIVAL_PRIZEBOOTH =
		{
			GENERIC = "妾身已想要那边那个！",		-- 物品名:"奖品摊位"->默认
		},
		CARNIVALCANNON_KIT = "你这有什么好东西。",		-- 物品名:"奖品摊位"
		CARNIVALCANNON =
		{
			GENERIC = "爆炸，多么完美！",		-- 物品名:未找到
			COOLDOWN = "太炸了！",		-- 物品名:未找到
		},
		CARNIVAL_PLAZA_KIT = "鸟儿喜欢这棵树。",		-- 物品名:"鸦年华树苗" 制造描述:"鸦年华不可或缺的中间物件。"
		CARNIVAL_PLAZA =
		{
			GENERIC = "现在看没“鸦年华”的样子，对不对？",		-- 物品名:"鸦年华树"->默认
			LEVEL_2 = "一只小鸟告诉妾身，这里要多放点装饰品。",		-- 物品名:"鸦年华树"
			LEVEL_3 = "这事一颗庆祝之树！",		-- 物品名:"鸦年华树"
		},
		CARNIVALDECOR_EGGRIDE_KIT = "希望奖品就只是这些。",		-- 物品名:"鸦年华树"
		CARNIVALDECOR_EGGRIDE = "妾身可以几个小时一直盯着它看。",		-- 物品名:"鸦年华树"
		CARNIVALDECOR_LAMP_KIT = "光剩下一点工作了。",		-- 物品名:"盛夏夜灯套装" 制造描述:"夏夜的梦幻之光。"
		CARNIVALDECOR_LAMP = "它的能量来自于奇思妙想。",		-- 物品名:"盛夏夜灯"
		CARNIVALDECOR_PLANT_KIT = "也许这是一棵黄杨？",		-- 物品名:"微型树套装" 制造描述:"一小块鸦年华。"
		CARNIVALDECOR_PLANT = "不是它小，就是妾身大。",		-- 物品名:"微型树"
		CARNIVALDECOR_FIGURE =
		{
			RARE = "看到没？这就是证据！",		-- 物品名:未找到
			UNCOMMON = "这种设计不是很常见。",		-- 物品名:未找到
			GENERIC = "妾身怎么弄到这么多的这些东西……",		-- 物品名:未找到
		},
		CARNIVALDECOR_FIGURE_KIT = "探索的刺激！",		-- 物品名:"神秘盒子" 制造描述:"里面会是什么？"
        CARNIVAL_BALL = "天才般的极简设计。", --unimplemented		-- 物品名:"发光红球" 制造描述:"保证你有球必应！"
		CARNIVAL_SEEDPACKET = "妾身觉得有点饿了。",		-- 物品名:"种子包" 制造描述:"鸦年华最爱的香脆小吃。"
		CARNIVALFOOD_CORNTEA = "这种食物本该这么松脆吗？",		-- 物品名:"玉米泥" 制造描述:"出乎意料的清爽！"
        CARNIVAL_VEST_A = "这让妾身看起来很有冒险精神。",		-- 物品名:"叽叽喳喳围巾" 制造描述:"用捡来的树叶做成的异想天开的围巾。"
        CARNIVAL_VEST_B = "就像穿上了自己专属的树荫。",		-- 物品名:"叽叽喳喳斗篷" 制造描述:"这东西确实值得称道。"
        CARNIVAL_VEST_C = "希望这里头没虫子……",		-- 物品名:"叽叽喳喳小披肩" 制造描述:"鸦年华游客的必备小披肩。"
        YOTB_SEWINGMACHINE = "缝纫能有多难……？",		-- 物品名:"缝纫机"
        YOTB_SEWINGMACHINE_ITEM = "看来需要组装一下。",		-- 物品名:"缝纫机套装" 制造描述:"做出完美的皮弗娄牛礼服吧。"
        YOTB_STAGE = "奇怪啊，妾身没见到他登台和离开……",		-- 物品名:"裁判席"
        YOTB_POST =  "这场比赛将会顺利进行！",		-- 物品名:"皮弗娄牛舞台"
        YOTB_STAGE_ITEM = "看起来要搭建一下才行。",		-- 物品名:"裁判席" 制造描述:"邀请专家出席。"
        YOTB_POST_ITEM =  "妾身最好先装好它。",		-- 物品名:"皮弗娄牛舞台" 制造描述:"让你的皮弗娄牛登上舞台中央。"
        YOTB_PATTERN_FRAGMENT_1 = "妾身当然能做出一件皮弗娄牛礼服。",		-- 物品名:"恐怖款式碎片" 制造描述:"来一点恐怖的灵感。"
        YOTB_PATTERN_FRAGMENT_2 = "妾身当然能做出一件皮弗娄牛礼服。",		-- 物品名:"正式款式碎片" 制造描述:"来一点正式的灵感。"
        YOTB_PATTERN_FRAGMENT_3 = "妾身当然能做出一件皮弗娄牛礼服。",		-- 物品名:"喜庆款式碎片" 制造描述:"来一点喜庆的灵感。"
        YOTB_BEEFALO_DOLL_WAR = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"战士皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"战士皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_DOLL = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"娃娃皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"娃娃皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_FESTIVE = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"喜庆皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"喜庆皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_NATURE = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"花朵皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"花朵皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_ROBOT = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"铁甲皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"铁甲皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_ICE = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"寒霜皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"寒霜皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_FORMAL = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"正式皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"正式皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_VICTORIAN = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"维多利亚皮弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"维多利亚皮弗娄牛玩偶"
        },
        YOTB_BEEFALO_DOLL_BEAST = 
        {
            GENERIC = "多么可爱的小牛玩偶。",		-- 物品名:"幸运野兽弗娄牛玩偶"->默认
            YOTB = "不知道他们会怎么说？",		-- 物品名:"幸运野兽弗娄牛玩偶"
        },
        WAR_BLUEPRINT = "多么威猛！",		-- 物品名:"战士礼服款式"
        DOLL_BLUEPRINT = "妾身的坐骑一定会可爱动人！",		-- 物品名:"娃娃礼服款式"
        FESTIVE_BLUEPRINT = "是时候来点节庆活动了！",		-- 物品名:"节日盛装款式"
        ROBOT_BLUEPRINT = "小意思罢了。",		-- 物品名:"铁甲礼服款式"
        NATURE_BLUEPRINT = "花朵真是百搭。",		-- 物品名:"花朵礼服款式"
        FORMAL_BLUEPRINT = "这是贵族的礼服。",		-- 物品名:"正式礼服款式"
        VICTORIAN_BLUEPRINT = "妾身可没见过衣服。",		-- 物品名:"维多利亚礼服款式"
        ICE_BLUEPRINT = "妾身喜欢新鲜的牛肉。",		-- 物品名:"寒霜礼服款式"
        BEAST_BLUEPRINT = "妾身觉得要走运了！",		-- 物品名:"幸运野兽礼服款式"
        BEEF_BELL = "它会让皮弗娄牛变得友好。",		-- 物品名:"皮弗娄牛铃" 制造描述:"与皮弗娄牛交朋友。"
        ALTERGUARDIAN_PHASE1 = 
        {
            GENERIC = "你会为破坏付出代价！",		-- 物品名:"天体英雄"->默认
            DEAD = "有了！",		-- 物品名:"天体英雄"->死了
        },
        ALTERGUARDIAN_PHASE2 = 
        {
            GENERIC = "妾身想妾身激怒它了……",		-- 物品名:"天体英雄"->默认
            DEAD = "本尊轻易就能收拾了它。",		-- 物品名:"天体英雄"->死了
        },
        ALTERGUARDIAN_PHASE2SPIKE = "你已经表明了你的观点！",		-- 物品名:"月光玻璃尖刺"
        ALTERGUARDIAN_PHASE3 = "它现在一定生气了！",		-- 物品名:"天体英雄"
        ALTERGUARDIAN_PHASE3TRAP = "小心危险。",		-- 物品名:"启迪陷阱"
        ALTERGUARDIAN_PHASE3DEADORB = "它死了吗？那股神秘的力量好像还在它附近游荡。",		-- 物品名:"天体英雄"
        ALTERGUARDIAN_PHASE3DEAD = "应该找个人过去戳一下……这样才能确定。",		-- 物品名:"被击败的天体英雄"
        ALTERGUARDIANHAT = "它让妾身看到了无限可能……",		-- 物品名:"启迪之冠"
        ALTERGUARDIANHATSHARD = "连部件都如此光彩夺目！",		-- 物品名:"启迪之冠碎片"
        MOONSTORM_GLASS = 
        {
            GENERIC = "呈现玻璃状。",		-- 物品名:"充能玻璃石"->默认
            INFUSED = "它焕发着月光。"		-- 物品名:"充能玻璃石"
        },
        MOONSTORM_STATIC = "能量在其中激荡！",		-- 物品名:"能量静电"
        MOONSTORM_STATIC_ITEM = "嗨，不要弄坏妾身的头发。",		-- 物品名:"约束静电"
        MOONSTORM_SPARK = "妾身不太认识这个",		-- 物品名:"月熠"
        BIRD_MUTANT = "妾身觉得那个以前应该是只乌鸦。",		-- 物品名:"月盲乌鸦"
        BIRD_MUTANT_SPITTER = "妾身不喜欢它盯着妾身的眼神……",		-- 物品名:"奇形鸟"
        WAGSTAFF_NPC = "奇怪的传输！",		-- 物品名:"颗粒状传输"
        ALTERGUARDIAN_CONTAINED = "它正在耗尽那个怪物的能量！",		-- 物品名:"月亮精华提取器"
        WAGSTAFF_TOOL_1 = "那正是妾身苦苦寻找的工具！",		-- 物品名:"网状缓冲器"
        WAGSTAFF_TOOL_2 = "妾身当然知道是怎么回事！",		-- 物品名:"装置除垢器"
        WAGSTAFF_TOOL_3 = "这明显是一个十分厉害的工具！",		-- 物品名:"垫圈开槽器"
        WAGSTAFF_TOOL_4 = "这就是妾身一直在寻找的工具！",		-- 物品名:"概念刷洗器"
        WAGSTAFF_TOOL_5 = "妾身知道它能做什么。",		-- 物品名:"校准观察机"
        MOONSTORM_GOGGLESHAT = "对啊！",		-- 物品名:"天文护目镜" 制造描述:"利用土豆之眼来看清风暴。"
        MOON_DEVICE =
         {
            GENERIC = "它捕捉到能量了！当然，妾身一早就知道会这样。",		-- 物品名:"月亮虹吸器"->默认
            CONSTRUCTION1 = "才刚刚开始。",		-- 物品名:"月亮虹吸器"
            CONSTRUCTION2 = "看起来妙多了！",		-- 物品名:"月亮虹吸器"
        },
        POCKETWATCH_HEAL = 
        {
			GENERIC = "时光荏苒，你能逃几何？。",		-- 物品名:"不老表"->默认 制造描述:"你觉得自己是几岁，你就是几岁。"
			RECHARGING = "时光匆匆，从不停留。",		-- 物品名:"不老表" 制造描述:"你觉得自己是几岁，你就是几岁。"
		},
        POCKETWATCH_REVIVE = 
        {
			GENERIC = "时光荏苒，你能逃几何？。",		-- 物品名:"第二次机会表"->默认 制造描述:"挽回一个朋友的不幸结局。"
			RECHARGING = "时光匆匆，从不停留。",		-- 物品名:"第二次机会表" 制造描述:"挽回一个朋友的不幸结局。"
		},
        POCKETWATCH_WARP = 
        {
			GENERIC = "时光荏苒，你能逃几何？。",		-- 物品名:"倒走表"->默认 制造描述:"重走你的最后几步。"
			RECHARGING = "时光匆匆，从不停留。",		-- 物品名:"倒走表" 制造描述:"重走你的最后几步。"
		},
        POCKETWATCH_RECALL = 
        {
			GENERIC = "时光荏苒，你能逃几何？。",		-- 物品名:"溯源表"->默认 制造描述:"返回到一个遥远的时间点。"
			RECHARGING = "时光匆匆，从不停留。",		-- 物品名:"溯源表" 制造描述:"返回到一个遥远的时间点。"
			UNMARKED = "only_used_by_wanda",		-- 物品名:"溯源表" 制造描述:"返回到一个遥远的时间点。"
			MARKED_SAMESHARD = "only_used_by_wanda",		-- 物品名:"溯源表" 制造描述:"返回到一个遥远的时间点。"
			MARKED_DIFFERENTSHARD = "only_used_by_wanda",		-- 物品名:"溯源表" 制造描述:"返回到一个遥远的时间点。"
		},
        POCKETWATCH_PORTAL = 
        {
			GENERIC = "时光荏苒，你能逃几何？。",		-- 物品名:"裂缝表"->默认 制造描述:"和朋友一起穿越时间会更好。"
			RECHARGING = "时光匆匆，从不停留。",		-- 物品名:"裂缝表" 制造描述:"和朋友一起穿越时间会更好。"
			UNMARKED = "only_used_by_wanda unmarked",		-- 物品名:"裂缝表" 制造描述:"和朋友一起穿越时间会更好。"
			MARKED_SAMESHARD = "only_used_by_wanda other shard",		-- 物品名:"裂缝表" 制造描述:"和朋友一起穿越时间会更好。"
			MARKED_DIFFERENTSHARD = "only_used_by_wanda other shard",		-- 物品名:"裂缝表" 制造描述:"和朋友一起穿越时间会更好。"
		},
        POCKETWATCH_WEAPON =
         {
			GENERIC = "坏事发生只是时间问题。",		-- 物品名:"警告表"->默认 制造描述:"这只钟敲的就是你。"
			DEPLETED = "only_used_by_wanda",		-- 物品名:"警告表" 制造描述:"这只钟敲的就是你。"
		},
        POCKETWATCH_PARTS = "你不该触碰！",		-- 物品名:"时间碎片" 制造描述:"计时必备零件。"
        POCKETWATCH_DISMANTLER = "妾身可不是是什么钟表匠。",		-- 物品名:"钟表匠工具" 制造描述:"修补计时装置。"
        POCKETWATCH_PORTAL_ENTRANCE = 
		{
			GENERIC = "前方一片混沌！",		-- 物品名:"时间裂缝"->默认
			DIFFERENTSHARD = "前方一片混沌！",		-- 物品名:"时间裂缝"
		},
        POCKETWATCH_PORTAL_EXIT = "前方一片混沌。",		-- 物品名:"时间裂缝"
        WATERTREE_PILLAR = "好大一棵树！",		-- 物品名:"大树干"
        OCEANTREE = "这些树长满了大疙瘩，可真恶心。",		-- 物品名:"疙瘩树"
        OCEANTREENUT = "这么大的果子，妾身需要一定帮手。",		-- 物品名:"疙瘩树果"
        WATERTREE_ROOT = "交织盘错。",		-- 物品名:"大树根"
        OCEANTREE_PILLAR = "虽然不如原来那个那么好，但还是挺不错的。",		-- 物品名:"高出平均值的树干"
        OCEANVINE = "多来几根给妾身做一席帘子。",		-- 物品名:"苔藓藤条"
        FIG = "有点像小石榴。",		-- 物品名:"无花果"
        FIG_COOKED = "哦，妾身并不是很喜欢这个味道。",		-- 物品名:"做熟的无花果"
        SPIDER_WATER = "盘丝洞旁的池塘里倒有许多。",		-- 物品名:"海黾"
        MUTATOR_WATER = "哦，哇，这看起来嗯……!",		-- 物品名:"海黾变身涂鸦" 制造描述:"光是看就让人流口水！"
        OCEANVINE_COCOON = "轻轻戳它一下会怎么样？",		-- 物品名:"海黾巢穴"
        OCEANVINE_COCOON_BURNT = "闻到了一些烧焦的味道。",		-- 物品名:"海黾巢穴"
        GRASSGATOR = "要来与本尊一战吗？",		-- 物品名:"草鳄鱼"
        TREEGROWTHSOLUTION = "嗯，肥力很大的样子！",		-- 物品名:"树果酱" 制造描述:"鼓励树木到达新的高度。"
        FIGATONI = "味道出奇的不错！",		-- 物品名:"无花果意面"
        FIGKABAB = "来点荤烤串更好，不是吗？",		-- 物品名:"无花果烤串"
        KOALEFIG_TRUNK = "额...奇怪的组合。",		-- 物品名:"无花果酿树干"
        FROGNEWTON = "芬芳与浓郁的焦香。",		-- 物品名:"无花果蛙腿三明治"
    },
    DESCRIBE_GENERIC = "妾身愚笨，不识此为何物。",		--检查物品描述的缺省值
    DESCRIBE_TOODARK = "太黑了，而且有东西在靠近！",		--天太黑
    DESCRIBE_SMOLDERING = "那东西快要着火了。",		--冒烟
    DESCRIBE_PLANTHAPPY = "多么快乐的植物啊!",		--暂无注释
    DESCRIBE_PLANTVERYSTRESSED = "这株植物似乎承受了很大的压力。",		--暂无注释
    DESCRIBE_PLANTSTRESSED = "它有点暴躁。",		--暂无注释
    DESCRIBE_PLANTSTRESSORKILLJOYS = "妾身可能要除一下草……",		--暂无注释
    DESCRIBE_PLANTSTRESSORFAMILY = "种豆南山下，草盛豆苗稀。",		--暂无注释
    DESCRIBE_PLANTSTRESSOROVERCROWDING = "这地方太小了。",		--暂无注释
    DESCRIBE_PLANTSTRESSORSEASON = "不识天时，难得地物。",		--暂无注释
    DESCRIBE_PLANTSTRESSORMOISTURE = "干旱太久了。",		--暂无注释
    DESCRIBE_PLANTSTRESSORNUTRIENTS = "这颗小苗需要一些农家肥!",		--暂无注释
    DESCRIBE_PLANTSTRESSORHAPPINESS = "它想找人好好聊聊。",		--暂无注释
    EAT_FOOD =
    {
        TALLBIRDEGG_CRACKED = "嗯...是鸡骨头吗。",		--吃孵化的高脚鸟蛋
		WINTERSFEASTFUEL = "它尝起来是节日的味道。",		--暂无注释
    },
}
