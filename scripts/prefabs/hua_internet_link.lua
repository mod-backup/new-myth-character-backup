local assets = {
    Asset("ANIM", "anim/hua_internet_node.zip"),
}

local LENGTH_SCALE = 600 / (710 * 4)

local function OnDig(inst)
    local collapse_fx = SpawnPrefab("collapse_small")
    collapse_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    collapse_fx:SetMaterial("wood")

    inst:Remove()
end

local function RemoveLink(inst)
    TheWorld.components.hua_internet_topology:RemoveLink(inst)
end

local function RegisterLink(inst)
    TheWorld.components.hua_internet_topology:RegisterLink(inst)
end

local function OnSave(inst, data)
    data.link_id = inst.link_id
    data.node1_id = inst.node1_id
    data.node2_id = inst.node2_id
    data.surface = inst.surface
end

local function OnLoad(inst, data)
    if data == nil then
        return
    end

    inst.link_id = data.link_id
    inst.node1_id = data.node1_id
    inst.node2_id = data.node2_id

    if data.surface then
        inst:SetSurface(data.surface[1], data.surface[2])
    end
    inst:DoTaskInTime(FRAMES * 10, RegisterLink)
end

local function SetSurface(inst, angle, length)
    inst.surface = {angle, length}
    inst.Transform:SetRotation(angle)
    inst.AnimState:SetScale(length * LENGTH_SCALE, 1)

    inst.net_length:set(length)

    inst.components.shaveable:SetPrize("silk", math.clamp(math.floor(TUNING.HUA_INTERNET_COST_MIN_SILK * 0.5 + TUNING.HUA_INTERNET_COST_UNIT_SILK * length * 0.125), 1, 100))
end

local function SetNodes(inst, node1, node2)
    inst.node1_id = node1.node_id
    inst.node2_id = node2.node_id
end

local function can_shave(inst, shaver, shave_item) -- 【【【【【【【【【【【【判断可刮人物】】】】】】】】】】
    return true
end

local function on_shaved(inst, shaver, shave_item)
    inst:Remove()
end

local function UpdateForColourChange(inst)
    if inst.my_link == nil then
        inst:Remove()
        return
    end

    local r, g, b, a = inst.my_link.AnimState:GetMultColour()
    inst.AnimState:SetMultColour(r, g, b, a)
end

local function CreateClientLink()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("hua_internet_node")
    inst.AnimState:SetBuild("hua_internet_node")
    inst.AnimState:PlayAnimation("link")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst:AddTag("CLASSIFIED")
    inst:AddTag("NOCLICK")

    inst:DoPeriodicTask(0, UpdateForColourChange)

    return inst
end

local function DrawClientReserveLink(inst)
    local length = inst.net_length:value()
    local reverse_link = inst.reverse_link or CreateClientLink()
    inst.reverse_link = reverse_link

    reverse_link.Transform:SetRotation(180)
    reverse_link.Transform:SetPosition(length, 0, 0)
    reverse_link.AnimState:SetScale(length * LENGTH_SCALE, 1)
    reverse_link.my_link = inst

    inst:AddChild(reverse_link)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("hua_internet_node")
    inst.AnimState:SetBuild("hua_internet_node")
    inst.AnimState:PlayAnimation("link")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    -- 要在客户端画一条反方向上的连线，以免图形被画面边缘卡掉
    -- 这个线只能看，不能选，实际上就算原本的线还在，足够长的情况下，也不是整根都能选中的
    inst.net_length = net_float(inst.GUID, "net_length", "net_client_link_dirty")
    if TheNet:GetIsClient() then
        inst:ListenForEvent("net_client_link_dirty", DrawClientReserveLink)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumBurnable(inst)
    MakeMediumPropagator(inst)

    --inst:AddComponent("inspectable") 不需要整天被选得到

    inst:AddComponent("hua_dye_target")

    inst:AddComponent("shaveable")
    inst.components.shaveable:SetPrize("silk", 1)
    inst.components.shaveable.can_shave_test = can_shave
    inst.components.shaveable.on_shaved = on_shaved

    inst:ListenForEvent("onremove", RemoveLink)

    inst.SetSurface = SetSurface
    inst.SetNodes = SetNodes
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("hua_internet_link", fn, assets)