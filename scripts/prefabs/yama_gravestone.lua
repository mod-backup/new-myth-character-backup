local assets =
{
    Asset("ANIM", "anim/gravestones.zip"),
    Asset("MINIMAP_IMAGE", "gravestones"),
}

local prefabs =
{
    "smallghost",
    "mound",
}

local function on_child_mound_dug(mound, data)
end

local function OnSleep(inst)
    inst:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, .25)

    inst.MiniMapEntity:SetIcon("gravestones.png")

    inst:AddTag("grave")

    inst.AnimState:SetBank("gravestone")
    inst.AnimState:SetBuild("gravestones")

    inst:SetPrefabNameOverride("gravestone")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.AnimState:PlayAnimation("grave"..tostring(math.random(4)))

    inst:AddComponent("inspectable")
    inst.components.inspectable:SetDescription(STRINGS.EPITAPHS[math.random(#STRINGS.EPITAPHS)])

    inst.mound = inst:SpawnChild("yama_mound")
    inst.mound.ghost_of_a_chance = 0.0
    inst:ListenForEvent("worked", on_child_mound_dug, inst.mound)
    inst.mound.Transform:SetPosition((TheCamera:GetDownVec()*.5):Get())

    inst.persists = false
    --inst:AddComponent("hauntable")
    --inst.components.hauntable:SetOnHauntFn(OnHaunt)

    inst.OnEntitySleep = OnSleep

    return inst
end

return Prefab("yama_gravestone", fn, assets, prefabs)
