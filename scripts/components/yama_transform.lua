
local function _GetBuild(inst, iswhite)
	local base = inst.components.skinner and inst.components.skinner:GetClothing().base
	if base == 'yama_commissioners_none' then
		return iswhite and 'yama_commissioners' or 'yama_commissioners_black'
	elseif base == 'yama_commissioners_lotus' then
		return iswhite and 'yama_commissioners_lotus' or 'yama_commissioners_lotus_black'
	else
		return iswhite and "yama_commissioners" or "yama_commissioners_black"
	end
end
local function oncurrent(self,current)
	if current == "white" then
		self.inst:AddTag("commissioner_white")
		self.inst:RemoveTag("commissioner_black")
		self.inst.AnimState:SetBuild(_GetBuild(self.inst, true))
		self.inst.components.combat.damagemultiplier = TUNING.WENDY_DAMAGE_MULT
		self.inst.components.locomotor:SetExternalSpeedMultiplier(self.inst, "yama_commissioners_white", 1.08)

		--speech
		STRINGS.CHARACTERS.YAMA_COMMISSIONERS = require "characterspeech/speech_yama_commissioners"
	else
		self.inst:RemoveTag("commissioner_white")
		self.inst:AddTag("commissioner_black")
		self.inst.AnimState:SetBuild(_GetBuild(self.inst, false))

		self.inst.components.combat.damagemultiplier = 1
		self.inst.components.locomotor:RemoveExternalSpeedMultiplier(self.inst, "yama_commissioners_white")

		--speech
		STRINGS.CHARACTERS.YAMA_COMMISSIONERS = require "characterspeech/speech_yama_commissioners_black"
	end
	if  self.inst._commissioner_balck then
		self.inst._commissioner_balck:set(current == "black")
	end
end

local function onsoul(self,soul)
	if self.inst.yama_status_soul then
		self.inst.yama_status_soul:set(soul)
	end
end

local function OnTaskTick(inst, self, period)
    self:DoDec(period)
end

local  yama_transform = Class(function(self, inst)
	self.inst = inst
	self.states = {
		white = {
			current = {
				currenthealth = TUNING.YAMA_COMMISSIONERS_HEALTH ,
				hunger = TUNING.YAMA_COMMISSIONERS_HUNGER ,
				sanity = TUNING.YAMA_COMMISSIONERS_SANITY ,				
			},
			isdead = false,
		},
		black = {
			current = {
				currenthealth = TUNING.YAMA_COMMISSIONERS_BLACK_HEALTH ,
				hunger = TUNING.YAMA_COMMISSIONERS_BLACK_HUNGER ,
				sanity = TUNING.YAMA_COMMISSIONERS_BLACK_SANITY ,			
			},
			isdead = false,
		},
	}
	self.sanwei = {
		white = {
			max = {
				maxhealth = TUNING.YAMA_COMMISSIONERS_HEALTH ,
				hunger = TUNING.YAMA_COMMISSIONERS_HUNGER ,
				sanity = TUNING.YAMA_COMMISSIONERS_SANITY ,
			},
			maxmax = {
				maxhealth = 175 ,
				hunger = 150 ,
				sanity = 150 ,
			},
		},
		black = {
			max = {
				maxhealth = TUNING.YAMA_COMMISSIONERS_BLACK_HEALTH ,
				hunger = TUNING.YAMA_COMMISSIONERS_BLACK_HUNGER ,
				sanity = TUNING.YAMA_COMMISSIONERS_BLACK_SANITY ,
			},
			maxmax = {
				maxhealth = 250 ,
				hunger = 175 ,
				sanity = 120 ,
			},
		},
	}
	self.current = "white"
	self.soul = 0
	self.task = {}
    self.targettime = {}
	self.startregensoul  = false

	--inst:DoTaskInTime(0,)
    local period = 1
    self.inst:DoPeriodicTask(period, OnTaskTick, nil, self, period)
end,
nil,
{
	current = oncurrent,
	soul = onsoul,
}
)

function yama_transform:IsWhite()
	return self.current == "white"
end

function yama_transform:IsBlack()
	return self.current == "black"
end

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

function yama_transform:Change(mandatory) --给我变！
	local changed = false
	local inst  =self.inst
	for k, v in pairs(self.states) do
		if k ~= self.current and not v.isdead then
			changed = true
            local changedata = {}
            local x, y, z = inst.Transform:GetWorldPosition()
            local fx = nil
            if k == "black" then
                --inst.AnimState:SetBuild("commissioner_black")
                fx = SpawnPrefab("soul_ghast_do_fx")
                SpawnPrefab("token_commissioner_dot_fx").Transform:SetPosition(x, y, z)
                changedata = {
					hat_commissioner_white = true,
					pennant_commissioner = true,
					bell_commissioner = true,
				}
            else
                --inst.AnimState:SetBuild("commissioner_white")
                fx = SpawnPrefab("soul_specter_do_fx")
                SpawnPrefab("pennant_commissioner_dot_fx").Transform:SetPosition(x, y, z)
                changedata = {
					hat_commissioner_black = true,
					whip_commissioner = true,
					token_commissioner = true,
				}
            end
            if fx ~= nil then
                fx.entity:AddFollower():FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, -50, 0)
                fx:Setup(inst)
            end
			self:SetNewState(k)
			if IsFlying(inst) and inst.Spawn_Flyerfx then
				inst:Spawn_Flyerfx()
			end

			inst:PushEvent("yama_transform", {current = self.current})
			
            local inventory = inst.components.inventory
            local items = inventory:FindItems(function(item)
				if item:HasTag("commissioneritem") and changedata[item.prefab] then
					if inventory:IsItemEquipped(item) then
						item.isinequipslot = true
					end
					return true
				end
                return false
            end)
            for k2, v2 in pairs(items) do
                inventory:RemoveItem(v2, true)
                inventory:GiveItem(v2, nil, nil) --第三个参数写了的话，获取新物品时就会出现捡起的物品移动到物品栏的动画
            end

            inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/heal") --目前用的恶魔人灵魂治疗的声音
			break
		end
	end
	if not changed  and not mandatory then
		self.inst.components.talker:Say(self.current == "white" and STRINGS.YAMA_BLACKDEAD or  STRINGS.YAMA_WHITEDEAD)
	end
end

function yama_transform:CanTransform()
	local other  = self:GetOtherState()
	return self.states[other] and not self.states[other].isdead
end

function yama_transform:DoTransform()
	local other  = self:GetOtherState()
	local inst = self.inst
	local changedata = {}
	local x, y, z = inst.Transform:GetWorldPosition()
	local fx = nil
	self:DoDeath(self.current)
	if other == "black" then
		--inst.AnimState:SetBuild("commissioner_black")
		fx = SpawnPrefab("soul_ghast_do_fx")
		SpawnPrefab("token_commissioner_dot_fx").Transform:SetPosition(x, y, z)
		changedata = {
			hat_commissioner_white = true,
			pennant_commissioner = true,
			bell_commissioner = true,
		}
	else
		--inst.AnimState:SetBuild("commissioner_white")
		fx = SpawnPrefab("soul_specter_do_fx")
		SpawnPrefab("pennant_commissioner_dot_fx").Transform:SetPosition(x, y, z)
		changedata = {
			hat_commissioner_black = true,
			whip_commissioner = true,
			token_commissioner = true,
		}
	end
	if fx ~= nil then
		fx.entity:AddFollower():FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, -50, 0)
		fx:Setup(inst)
	end
	self:SetHealth(other)
	self.current = other
	if IsFlying(inst) and inst.Spawn_Flyerfx then
		inst:Spawn_Flyerfx()
	end

	inst:PushEvent("yama_transform", {current = self.current})

	--self:SetNewState(other)

	local inventory = inst.components.inventory
	local items = inventory:FindItems(function(item)
		if item:HasTag("commissioneritem") and changedata[item.prefab] then
			if inventory:IsItemEquipped(item) then
				item.isinequipslot = true
			end
			return true
		end
		return false
	end)
	for k2, v2 in pairs(items) do
		inventory:RemoveItem(v2, true)
		inventory:GiveItem(v2, nil, nil)
	end

	inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/heal")
end

function yama_transform:DoDeath(state)
	self.states[state].current.hunger = 0
	self.states[state].current.sanity = 0
	self.states[state].current.currenthealth = 0
	self.states[state].isdead = true
end

function yama_transform:GetOtherState()
	return self.current == "white" and "black" or "white"
end

function yama_transform:SetHealth(state)
	local inst  = self.inst
	local state = state or self.current

	if self.states[state].isdead then
		return
	end

	local max = TheWorld.components.yama_statuemanager:IsMaxLevel() and "maxmax" or "max"
    inst.components.hunger.max = self.sanwei[state][max].hunger
    inst.components.health.maxhealth = self.sanwei[state][max].maxhealth
    inst.components.sanity.max = self.sanwei[state][max].sanity

	inst.components.health:SetCurrentHealth(self.states[state].current.currenthealth)
	inst.components.sanity.current = self.states[state].current.sanity
	inst.components.hunger.current = self.states[state].current.hunger

	if inst.components.hunger.current > 0 then
		inst:PushEvent("stopstarving")
	end
	inst.components.hunger:DoDelta(0.1)
	inst.components.health:DoDelta(0)

    local ignoresanity = inst.components.sanity.ignore
    inst.components.sanity.ignore = false
	inst.components.sanity:DoDelta(0)
    inst.components.sanity.ignore = ignoresanity	
end

function yama_transform:Upgrade(state)
	local inst  = self.inst
	local state = self.current

	if self.states[state].isdead then
		return
	end

	local max = TheWorld.components.yama_statuemanager:IsMaxLevel() and "maxmax" or "max"
    inst.components.hunger.max = self.sanwei[state][max].hunger
    inst.components.health.maxhealth = self.sanwei[state][max].maxhealth
    inst.components.sanity.max = self.sanwei[state][max].sanity

	if inst.components.hunger.current > 0 then
		inst:PushEvent("stopstarving")
	end
	inst.components.hunger:DoDelta(0.1)
	inst.components.health:DoDelta(0)

    local ignoresanity = inst.components.sanity.ignore
    inst.components.sanity.ignore = false
	inst.components.sanity:DoDelta(0)
    inst.components.sanity.ignore = ignoresanity	
end

function yama_transform:SetNewState(state)
	local inst  = self.inst
	if state == self.current then
		return
	end
	self.states[self.current].current.currenthealth = inst.components.health.currenthealth
	self.states[self.current].current.sanity = inst.components.sanity.current
	self.states[self.current].current.hunger = inst.components.hunger.current

	self:SetHealth(state)
	self.current = state
end

--print(ThePlayer.components.yama_transform.soul)

function yama_transform:DoResurrection()
	local max = TheWorld.components.yama_statuemanager:GetLevel() > 2  and true
	for k, v in pairs(self.states) do
		self.states[k].current.currenthealth = max and 50 or 30
		self.states[k].current.sanity = max and 100 or 50
		self.states[k].current.hunger = max and 100 or 20
		self.states[k].isdead = false
	end
	self.current = "white"
	self:SetHealth()
end

--0.04

local ResurrectionTime = {
	120,90,60,60
}

local healvalue = {
	0.2,0.2,0.4,0.4
}

local soulrate = {
	120,90,60,60
}

function yama_transform:StartRegenSoul()
	self.startregensoul = true
end

function yama_transform:StopRegenSoul()
	self.startregensoul = false
end

local function spawanfxs(x,y,z,prefab,rad,need,max,owner)
	local max = max or 50
	local num = 0
	local map = TheWorld.Map
	for k = 1,max do
		local offset = FindValidPositionByFan(
			math.random() * 2 * PI,
			math.random(rad),
			20,
			function(offset)
				local pt = Vector3(x + offset.x, 0, z + offset.z)
				return map:IsPassableAtPoint(pt.x, 0, pt.z,false,true)
					and not map:IsPointNearHole(pt)
			end
		)
		if offset ~= nil then
			local fx = SpawnPrefab(prefab)
			fx.Transform:SetPosition(x + offset.x, 0, z + offset.z)
			fx:SetOwner(owner)
			num = num + 1
		end
		if num >= need then
			break
		end
	end
end

local flying  = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying() end

function yama_transform:DoDec()

	if  self.startregensoul  then
		self.soul = math.clamp(self.soul + 100/soulrate[TheWorld.components.yama_statuemanager:GetLevel()] ,0,100)
		if not self.lasetspawnflowertime then
			self.lasetspawnflowertime = GetTime()
		end
		if self.soul >= 100 and  (GetTime() - self.lasetspawnflowertime) > 9 then
			local x,y,z = self.inst.Transform:GetWorldPosition()
			spawanfxs(x,y,z,"myth_higanbana_revive",4,1,10,self.inst)
			self.lasetspawnflowertime = GetTime()
		end
		return
	end
	--开启无敌就不会发生改变 很科学
	if self.inst.components.health.invincible then
		return
	end
	--幽冥身不会饥饿
	if flying(self.inst) then
		return
	end

	local other = self:GetOtherState()
	local starve = false
	local delta = 0
	local max = TheWorld.components.yama_statuemanager:IsMaxLevel() and "maxmax" or "max"
	if self.states[other] then

		if self.states[other].current.hunger > 0 and not self.states[other].isdead then
			local oldpercent = self.states[other].current.hunger/self.sanwei[other][max].hunger
			self.states[other].current.hunger =  math.clamp(self.states[other].current.hunger - 0.04,0,self.sanwei[other][max].hunger)
			local new = self.states[other].current.hunger/self.sanwei[other][max].hunger
            if new <= TUNING.HUNGRY_THRESH and oldpercent > TUNING.HUNGRY_THRESH then
				local player  = other == "black" and "yama_commissioners_black"  or  "yama_commissioners"
                self.inst.components.talker:Say(GetString(player, "ANNOUNCE_HUNGRY"))
            end
		else
			starve = true
		end
		if starve and not self.states[other].isdead  then
			delta = delta - 1
		end
		if TheWorld.state.isnight then
			delta = delta + healvalue[TheWorld.components.yama_statuemanager:GetLevel()] or 0
		end
		if delta ~= 0 then
			local old = self.states[other].current.currenthealth
			--print("掉落多少呢？",delta,other,self.states[other].current.currenthealth)
			self.states[other].current.currenthealth = math.clamp(self.states[other].current.currenthealth + delta,0,self.sanwei[other][max].maxhealth)
			--print(self.states[other].current.currenthealth)

			if self.states[other].current.currenthealth >= 1 then
				self.states[other].isdead = false
				if old < 1 then
					self.inst:SpawnChild(other == "black" and "soul_ghast_do_fx" or "soul_specter_do_fx")
					self.states[other].current.hunger = 10
					self.states[other].current.sanity = 5
				end
			else
				self.states[other].isdead = true
			end
		end
	end
end

function yama_transform:OnSave(time)
	self.states[self.current].current.currenthealth = self.inst.components.health.currenthealth
	self.states[self.current].current.sanity = self.inst.components.sanity.current
	self.states[self.current].current.hunger = self.inst.components.hunger.current

    local data = {}
	data.states = self.states
	data.soul = self.soul
	data.current = self.current
	
    return data
end

function yama_transform:OnLoad(data)
    if data then
		if data.states then
			self.states = data.states
		end
		if data.soul then
			self.soul = data.soul
		end
		self.inst:DoTaskInTime(0, function()
			if self.current == "black" and not self.states[self.current].isdead then
				self.inst.AnimState:SetBuild(_GetBuild(self.inst, false))
			end
			--self:SetHealth()
		end)
    end
end

return yama_transform


