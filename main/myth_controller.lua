local function GetUpvalueHelper(fn, name)
    local i = 1
    while _G.debug.getupvalue(fn, i) and _G.debug.getupvalue(fn, i) ~= name do
        i = i + 1
    end
    local name, value = _G.debug.getupvalue(fn, i)
    return value, i
end

local GetUpvalue = function(fn, ...)
    local prv, i, prv_var = nil, nil, "(the starting point)"
    for j, var in ipairs({...}) do
        _G.assert(type(fn) == "function", "We were looking for " .. var .. ", but the value before it, "
                .. prv_var .. ", wasn't a function (it was a " .. type(fn)
                .. "). Here's the full chain: " .. table.concat({"(the starting point)", ...}, ", "))
        prv = fn
        prv_var = var
        fn, i = GetUpvalueHelper(fn, var)
    end
    return fn, i, prv
end

local RPC_HANDLERS = GetUpvalue(HandleRPC, "RPC_HANDLERS")
local IsPointInRange = GetUpvalue(RPC_HANDLERS[RPC.RightClick], "IsPointInRange")
local ConvertPlatformRelativePositionToAbsolutePosition = GetUpvalue(RPC_HANDLERS[RPC.RightClick], "ConvertPlatformRelativePositionToAbsolutePosition")
local printinvalid = GetUpvalue(RPC_HANDLERS[RPC.RightClick], "printinvalid")
local printinvalidplatform = GetUpvalue(RPC_HANDLERS[RPC.RightClick], "printinvalidplatform")

RPC_HANDLERS[RPC.LeftClick] = function(player, action, x, z, target, isreleased, controlmods, noforce, mod_name, platform, platform_relative, skill_name)
    if not (checknumber(action) and
            checknumber(x) and
            checknumber(z) and
            optentity(target) and
            optbool(isreleased) and
            optnumber(controlmods) and
            optbool(noforce) and
            optstring(mod_name) and
            optentity(platform) and
            checkbool(platform_relative)) and
            optstring(skill_name) then
        printinvalid("LeftClick", player)
        return
    end
    local playercontroller = player.components.playercontroller
    if playercontroller ~= nil then
        printinvalidplatform("LeftClick", player, action, x, z, platform, platform_relative)
        x, z = ConvertPlatformRelativePositionToAbsolutePosition(x, z, platform, platform_relative)
        if x ~= nil then
            if IsPointInRange(player, x, z) then
                playercontroller:OnRemoteLeftClick(action, Vector3(x, 0, z), target, isreleased, controlmods, noforce, mod_name, skill_name)
            else
                print("Remote left click out of range")
            end
        end
    end
end

local PlayerController = require("components/playercontroller")
function PlayerController:OnRemoteLeftClick(actioncode, position, target, isreleased, controlmodscode, noforce, mod_name, skill_name)
    if self.ismastersim and self:IsEnabled() and self.handler == nil then
        self.inst.components.combat:SetTarget(nil)

        self.remote_controls[CONTROL_PRIMARY] = 0
        self:DecodeControlMods(controlmodscode)
        SetClientRequestedAction(actioncode, mod_name)
        local lmb, rmb = self.inst.components.playeractionpicker:DoGetMouseActions(position, target)
        ClearClientRequestedAction()
        if isreleased then
            self.remote_controls[CONTROL_PRIMARY] = nil
        end
        self:ClearControlMods()

        if skill_name and actioncode == ACTIONS.CASTAOE.code and mod_name == nil then -- 施法
            lmb = BufferedAction(self.inst, target, ACTIONS.CASTAOE, self.inst --[[熔炉物品包没有判空导致要这么做]], position, skill_name, HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[skill_name].range)
        else
            --Default fallback lmb action is WALKTO
            --Possible for lmb action to switch to rmb after autoequip
            lmb = (lmb == nil and
                    actioncode == ACTIONS.WALKTO.code and
                    mod_name == nil and
                    BufferedAction(self.inst, nil, ACTIONS.WALKTO, nil, position))
                    or (lmb ~= nil and
                    lmb.action.code == actioncode and
                    lmb.action.mod_name == mod_name and
                    lmb)
                    or (rmb ~= nil and
                    rmb.action.code == actioncode and
                    rmb.action.mod_name == mod_name and
                    rmb)
                    or nil
        end

        if lmb ~= nil then
            if lmb.action.canforce and not noforce then
                lmb:SetActionPoint(self:GetRemotePredictPosition() or self.inst:GetPosition())
                lmb.forced = true
            end
            self:DoAction(lmb)
            --elseif mod_name ~= nil then
            --print("Remote left click action failed: "..tostring(ACTION_MOD_IDS[mod_name][actioncode]))
            --else
            --print("Remote left click action failed: "..tostring(ACTION_IDS[actioncode]))
        end
    end
end

function PlayerController:OnLeftClick(down)
    if not self:UsingMouse() then
        return
    elseif not down then
        self:OnLeftUp()
        return
    end

    self:ClearActionHold()

    self.startdragtime = nil

    if not self:IsEnabled() then
        return
    elseif TheInput:GetHUDEntityUnderMouse() ~= nil then
        self:CancelPlacement()
        return
    elseif self.placer_recipe ~= nil and self.placer ~= nil then

        --do the placement
        if self.placer.components.placer.can_build then

            if self.inst.replica.builder ~= nil and not self.inst.replica.builder:IsBusy() then
                self.inst.replica.builder:MakeRecipeAtPoint(self.placer_recipe, TheInput:GetWorldPosition(), self.placer:GetRotation(), self.placer_recipe_skin)
                self:CancelPlacement()
            end

        elseif self.placer.components.placer.onfailedplacement ~= nil then
            self.placer.components.placer.onfailedplacement(self.inst, self.placer)
        end

        return
    end

    self.actionholdtime = GetTime()

    local act
    local hua_skill
    local pointedtarget, ispointed
    if self:IsAOETargeting() then
        if self:IsBusy() then
            TheFocalPoint.SoundEmitter:PlaySound("dontstarve/HUD/click_negative", nil, .4)
            self.reticule:Blip()
            return
        end

        hua_skill = self.inst.hua_skill
        if hua_skill then
            act = BufferedAction(self.inst, nil, ACTIONS.CASTAOE, self.inst--[[熔炉物品包没有判空导致要这么做]], self:GetAOETargetingPos(), hua_skill, self.reticule.range) -- 技能选定期间，直接指定动作
            act.distance = self.reticule.range
        else
            act = self:GetRightMouseAction()
            if act == nil or act.action ~= ACTIONS.CASTAOE then
                return
            end
        end

        self.reticule:PingReticuleAt(act:GetActionPoint(), pointedtarget)
        self:CancelAOETargeting()
    elseif act == nil then
        act = self:GetLeftMouseAction() or BufferedAction(self.inst, nil, ACTIONS.WALKTO, nil, TheInput:GetWorldPosition())
    end

    if act.action == ACTIONS.WALKTO then
        local entity_under_mouse = TheInput:GetWorldEntityUnderMouse()
        if act.target == nil and (entity_under_mouse == nil or entity_under_mouse:HasTag("walkableplatform")) then
            self.startdragtime = GetTime()
        end
    elseif act.action == ACTIONS.ATTACK then
        if self.inst.sg ~= nil then
            if self.inst.sg:HasStateTag("attack") and act.target == self.inst.replica.combat:GetTarget() then
                return
            end
        elseif self.inst:HasTag("attack") and act.target == self.inst.replica.combat:GetTarget() then
            return
        end
    elseif act.action == ACTIONS.LOOKAT and act.target ~= nil and self.inst.HUD ~= nil then
        if act.target.components.playeravatardata ~= nil then
            local client_obj = act.target.components.playeravatardata:GetData()
            if client_obj ~= nil then
                client_obj.inst = act.target
                self.inst.HUD:TogglePlayerAvatarPopup(client_obj.name, client_obj, true)
            end
        elseif act.target.quagmire_shoptab ~= nil then
            self.inst:PushEvent("quagmire_shoptab", act.target.quagmire_shoptab)
        end
    end

    if self.ismastersim then
        self.inst.components.combat:SetTarget(nil)
    else
        local mouseover, platform, pos_x, pos_z
        if act.action == ACTIONS.CASTAOE then
            platform = act.pos.walkable_platform
            pos_x = act.pos.local_pt.x
            pos_z = act.pos.local_pt.z

            if pointedtarget and ispointed then -- 单体目标技能，将目标代入
                mouseover = pointedtarget
            end
        else
            local position = TheInput:GetWorldPosition()
            platform, pos_x, pos_z = self:GetPlatformRelativePosition(position.x, position.z)
            mouseover = act.action ~= ACTIONS.DROP and TheInput:GetWorldEntityUnderMouse() or nil
        end

        local controlmods = self:EncodeControlMods()
        if self.locomotor == nil then
            self.remote_controls[CONTROL_PRIMARY] = 0
            SendRPCToServer(RPC.LeftClick, act.action.code, pos_x, pos_z, mouseover, nil, controlmods, act.action.canforce, act.action.mod_name, platform, platform ~= nil, hua_skill)
        elseif act.action ~= ACTIONS.WALKTO and self:CanLocomote() then
            act.preview_cb = function()
                self.remote_controls[CONTROL_PRIMARY] = 0
                local isreleased = not TheInput:IsControlPressed(CONTROL_PRIMARY)
                SendRPCToServer(RPC.LeftClick, act.action.code, pos_x, pos_z, mouseover, isreleased, controlmods, nil, act.action.mod_name, platform, platform ~= nil, hua_skill)
            end
        end
    end

    self:DoAction(act)
end

-- 修改技能指示触发器
function PlayerController:IsAOETargeting()
    return self.reticule ~= nil and (self.reticule.inst.components.aoetargeting ~= nil or self.reticule.hua_reticule ~= nil)
end

--[[function PlayerController:HasAOETargeting()
    return false -- 防止右键动作被无效化
end]]

--[[function PlayerController:TryAOETargeting() -- 不再使用内置的触发器

end]]

local old_CancelAOETargeting = PlayerController.CancelAOETargeting
function PlayerController:CancelAOETargeting()
    if self.reticule ~= nil then
        self.inst:HuaStopTargeting()
    end

    old_CancelAOETargeting(self)
end

function PlayerController:RefreshReticule()
    if self.reticule ~= nil then
        self.reticule:DestroyReticule()
    end
    if self.inst.hua_skill then
        self.reticule = self.inst.components.reticule
        if self.reticule ~= nil and self.reticule.reticule == nil and (self.reticule.mouseenabled or TheInput:ControllerAttached()) then
            self.reticule.hua_reticule = true
            self.reticule:CreateReticule()
            if self.reticule.reticule ~= nil and not self:IsEnabled() then
                self.reticule.reticule:Hide()
            end
        end
    else
        local item = self.inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        self.reticule = item ~= nil and item.components.reticule or self.inst.components.reticule
        if self.reticule ~= nil and self.reticule.reticule == nil and (self.reticule.mouseenabled or TheInput:ControllerAttached()) then
            self.reticule:CreateReticule()
            if self.reticule.reticule ~= nil and not self:IsEnabled() then
                self.reticule.reticule:Hide()
            end
        end
    end
end