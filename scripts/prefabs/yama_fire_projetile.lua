local assets =
{
    Asset("ANIM", "anim/staff_projectile.zip"),
    Asset("ANIM", "anim/yama_fire_projetile.zip"),
}

local ice_prefabs =
{
    "shatter",
}

local function OnHitIce(inst, owner, target)
    if target:IsValid() and not target:HasTag("freezable") then
        local fx = SpawnPrefab("shatter")
        fx.Transform:SetPosition(target.Transform:GetWorldPosition())
        fx.components.shatterfx:SetLevel(2)
    end

    inst:Remove()
end

local function common(anim, bloom)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)

    inst.AnimState:SetBank("projectile")
    inst.AnimState:SetBuild("yama_fire_projetile")
    inst.AnimState:PlayAnimation(anim, true)
    if bloom ~= nil then
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    end

    --projectile (from projectile component) added to pristine state for optimization
    inst:AddTag("projectile")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("projectile")
    inst.components.projectile:SetSpeed(20)
    inst.components.projectile:SetOnHitFn(inst.Remove)
    inst.components.projectile:SetOnMissFn(inst.Remove)

    return inst
end

local function fire()
    return common("fire_spin_loop", "shaders/anim.ksh")
end

return Prefab("yama_fire_projetile", fire, assets)
