
modimport("main/myth_controller.lua")
modimport("main/myth_players_skills.lua")

for _, v in pairs({"wilson", "wilson_client"}) do
    AddStategraphPostInit(v, function(sg)
        local old_deststate = sg.actionhandlers[ACTIONS.CASTAOE].deststate
        sg.actionhandlers[ACTIONS.CASTAOE].deststate = function(inst, action)
            local skill = action.recipe
            if skill then
                local data = HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[skill]
                if data.nosg then
                    if TheWorld.ismastersim then
                        inst:PerformBufferedAction()
                    else
                        inst:PerformPreviewBufferedAction()
                    end
                    return "idle"
                end
                return data and data.sg or "quickcastspell"
            end
            return old_deststate(inst, action)
        end
    end)

    --[[ 可以取消后摇
    AddStategraphPostInit(v, function(sg)
        local timeline = sg.states["quickcastspell"].timeline
        timeline[#timeline + 1] = TimeEvent(6 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("doing")
        end)
        -- 重新排一下
        local function pred(a,b)
            return a.time < b.time
        end
        table.sort(timeline, pred)
    end)]]
end

local function StartTargeting(inst, skill, quick)
    if HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[skill] == nil then
        return
    end

    if HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[skill].noreticule then -- 无需瞄准，直接发射
        local act = BufferedAction(inst, nil, ACTIONS.CASTAOE, nil, inst:GetPosition(), skill, 999) -- 指定动作
        local mouseover, platform, pos_x, pos_z
        platform = act.pos.walkable_platform
        pos_x = act.pos.local_pt.x
        pos_z = act.pos.local_pt.z

        local controller = inst.components.playercontroller
        local controlmods = controller:EncodeControlMods()
        if controller.locomotor == nil then
            controller.remote_controls[CONTROL_PRIMARY] = 0
            SendRPCToServer(RPC.LeftClick, act.action.code, pos_x, pos_z, mouseover, nil, controlmods, act.action.canforce, act.action.mod_name, platform, platform ~= nil, skill)
        elseif controller:CanLocomote() then
            act.preview_cb = function()
                controller.remote_controls[CONTROL_PRIMARY] = 0
                local isreleased = not TheInput:IsControlPressed(CONTROL_PRIMARY)
                SendRPCToServer(RPC.LeftClick, act.action.code, pos_x, pos_z, mouseover, isreleased, controlmods, nil, act.action.mod_name, platform, platform ~= nil, skill)
            end
        end

        controller:DoAction(act)
        return
    end

    inst.hua_skill = skill

    if inst.components.reticule == nil then
        inst:AddComponent("reticule")
        for k, v in pairs(HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[skill]) do
            inst.components.reticule[k] = v
        end

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RefreshReticule()
        end
    end
end

local function StopTargeting(inst)
    inst.hua_skill = nil

    if inst.components.reticule ~= nil then
        inst:RemoveComponent("reticule")

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RefreshReticule()
        end
    end
end

AddPlayerPostInit(function(inst)
    inst.HuaStartTargeting = StartTargeting
    inst.HuaStopTargeting = StopTargeting

    if TheWorld.ismastersim then
        inst:AddComponent("myth_skill_caster")
    end
end)

local function GetActiveScreenName()
    local screen = TheFrontEnd:GetActiveScreen()
    return screen and screen.name or ""
end
local function IsDefaultScreen()
    return GetActiveScreenName():find("HUD") ~= nil
end

--[[测试技能
TheInput:AddKeyDownHandler(KEY_R, function()
    if not IsDefaultScreen() then
        return
    end
    if ThePlayer._is_player_astral ~= nil and  ThePlayer._is_player_astral:value() then
        return
    end
    --判定是否有金箍棒 脑残是否够 是不是在房子里面
    ThePlayer:HuaStartTargeting("mk_yzqt")
end)

TheInput:AddKeyDownHandler(KEY_C, function()
    if not IsDefaultScreen() then
        return
    end
    SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],7)
    --ThePlayer:HuaStartTargeting("hua_beautiful")
end)

TheInput:AddKeyDownHandler(KEY_Z, function()
    if not IsDefaultScreen() then
        return
    end
    SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],8)
    --ThePlayer:HuaStartTargeting("hua_beautiful")
end)]]

--SG得部分！
AddStategraphState('wilson', State{
    name = "mk_yzqt",
    tags = {"notalking","doing", "busy", "nointerrupt", "nomorph" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        local js = inst.components.inventory:FindItems(function(item) return item.prefab == "mk_jgb" end)
        if #js < 1 then
            inst.sg:GoToState("idle")
            inst:ClearBufferedAction()
            return
        elseif  inst.components.sanity.current < 25 
            or not inst._mkyzqt:value()
            or (inst._inmythcamea and inst._inmythcamea:value() ~= nil)
            or (inst._inhuacamea and inst._inhuacamea:value() ~= nil)
            or (inst._is_player_astral ~= nil and  inst._is_player_astral:value()) then
                inst.sg:GoToState("idle")
                inst:ClearBufferedAction()
                return
        else
            inst.sg.statemem.yzqtjgb = js[1]
            if not js[1].components.equippable:IsEquipped() then
                inst.components.inventory:Equip(js[1])
            end
        end
        inst.components.sanity:DoDelta(-25)
        inst.AnimState:PlayAnimation("yizhuqingtian")
        inst.sg:SetTimeout(1.8)
    end,
    timeline =
    {
        TimeEvent(0.3, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/common/twirl", nil, nil, true)
        end),
        TimeEvent(0.62, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/common/twirl", nil, nil, true)
            if inst.sg.statemem.yzqtjgb and inst.sg.statemem.yzqtjgb:IsValid() then
                inst.sg.statemem.yzqtjgb:Remove()
                inst.AnimState:Show("ARM_carry")
                inst.AnimState:Hide("ARM_normal")
            end
        end),
        TimeEvent(1.2, function(inst)
            inst:PerformBufferedAction()
        end),
    },
    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },
    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle")
    end,
    onexit = function(inst)
        if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
            inst.AnimState:Show("ARM_carry")
            inst.AnimState:Hide("ARM_normal")
        else
            inst.AnimState:ClearOverrideSymbol("swap_object")
            inst.AnimState:Hide("ARM_carry")
            inst.AnimState:Show("ARM_normal")           
        end
    end,
})

AddStategraphState('wilson_client', State{
    name = "mk_yzqt",
    tags = { "doing", "busy", },
    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("yizhuqingtian")
        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(3)
    end,

    onupdate = function(inst)
        if inst:HasTag("busy") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle", true)
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle", true)
    end,
})