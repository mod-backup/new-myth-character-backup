L = MK_MOD_LANGUAGE_SETTING ~= nil and MK_MOD_LANGUAGE_SETTING == "ENGLISH" and true or false
V = MK_MOD_LANGUAGE_SETTING ~= nil and MK_MOD_LANGUAGE_SETTING == "VI" and true or false

--mk
-----------------
SKIN_RARITY_COLORS["MONKEY_KING_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["MONKEY_KING_SEA"] = {0.2, 0.8, 0.2, 1}
SKIN_RARITY_COLORS["MONKEY_KING_HORSE"] = {0.8, 0.8, 0.1, 1}
SKIN_RARITY_COLORS["MONKEY_KING_FIRE"] = {1, 0.3, 0, 1}
SKIN_RARITY_COLORS["MONKEY_KING_EAR"] = {0.36, 0.35, 0.34, 1}
SKIN_RARITY_COLORS["MONKEY_KING_WINE"] = {83/255, 113/255, 117/255, 1}

STRINGS.SKIN_NAMES.monkey_king_none = L and 'Monkey King' or V and 'Tôn Ngộ Không' or '孙悟空'
STRINGS.SKIN_NAMES.monkey_king_sea = L and 'Seafarer' or V and 'Xuất Hải Học Nghệ' or '出海学艺'
STRINGS.SKIN_NAMES.monkey_king_horse = L and 'Horse Supervisor' or V and 'Bật Mã Ôn' or '弼马温'
STRINGS.SKIN_NAMES.monkey_king_fire = L and 'Fiery Golden Eyes' or V and 'Hỏa Nhãn Kim Tinh' or '火眼金睛'
STRINGS.SKIN_NAMES.monkey_king_opera = L and 'Drama Prince' or V and 'Hí Trung Hành Giả' or "戏中行者"
STRINGS.SKIN_NAMES.monkey_king_ear = L and 'Curious Ear' or V and 'Lục Nhĩ Mi Hầu' or "六耳猕猴"
STRINGS.SKIN_NAMES.monkey_king_wine =  L and 'Drunkard Ape' or V and '白面醉翁' or "白面醉翁"

STRINGS.UI.RARITY['MONKEY_KING_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['MONKEY_KING_SEA'] = L and 'Reminiscence' or V and '回忆往昔' or '回忆往昔'
STRINGS.UI.RARITY['MONKEY_KING_HORSE'] = L and 'Reminiscence' or V and '回忆往昔' or '回忆往昔'
STRINGS.UI.RARITY['MONKEY_KING_FIRE'] = L and 'Reminiscence' or V and '回忆往昔' or '回忆往昔'
STRINGS.UI.RARITY['MONKEY_KING_OPERA'] = L and 'Quintessence' or V and '国粹戏曲' or "国粹戏曲"
STRINGS.UI.RARITY['MONKEY_KING_EAR'] = L and '81 Calamities' or V and '回忆往昔' or "八十一难"
STRINGS.UI.RARITY['MONKEY_KING_WINE'] = L and 'Omnifarious' or V and '千姿百态' or "千姿百态"

STRINGS.SKIN_QUOTES['monkey_king_none'] = L and '"Though I beat their arses, I might twist their arms later on..."' or V and '"Ngạo nghễ cười nhìn trời, thần tiên đều vô dụng"' or '\"睥睨意笑满天仙神皆无用\"'
STRINGS.SKIN_QUOTES['monkey_king_sea'] = L and '"Craft my raft, and off we go!"' or V and '"Bè gỗ lại mượn Đông Hải một trận gió"' or '\"折木编筏再借东海一阵风\"'
STRINGS.SKIN_QUOTES['monkey_king_horse'] = L and '"Jadie you know what! Your horses are dead!"' or V and '"Ngọc Đế Lão Nhi, ngựa của ngươi chết rồi"' or '\"玉帝老儿, 你马没了\"'
STRINGS.SKIN_QUOTES['monkey_king_fire'] = L and '"La petite Alchemy Furnance? What can you do to me?"' or V and '"Lò Bát Quái sao khó dễ được ta"' or '\"区区八卦炉能奈我何？\"'
STRINGS.SKIN_QUOTES['monkey_king_opera'] = L and '"A while on the stage comes from 10 years of practice off the stage..."' or V and '"Trên đài một chốc lát, dưới đài tập mười năm"' or '\"台上片刻钟，台下十年功\"'
STRINGS.SKIN_QUOTES['monkey_king_ear'] = L and '"Magic so variable, real or fake no longer distinguishable."' or V and '"Thần thông giỏi biến hóa, Thật giả khó phân chia"' or '\"神通多变化，真假两相平\"'
STRINGS.SKIN_QUOTES['monkey_king_wine'] = L and '"Drinking to forget my worry, yet my heart just grow more weary..."' or V and '"举杯消愁愁更愁"' or '\"举杯消愁愁更愁\"'


STRINGS.SKIN_DESCRIPTIONS['monkey_king_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_sea'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_horse'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_fire'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_opera'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_ear'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['monkey_king_wine'] = L and '' or ''
--nz
-----------------
SKIN_RARITY_COLORS["NEZA_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["NEZA_GREEN"] = {0.2, 0.8, 0.2, 1}
SKIN_RARITY_COLORS["NEZA_FIRE"] = {1, 0.3, 0, 1}
SKIN_RARITY_COLORS["NEZA_PINK"] = {1, 174/255, 201/255, 1}
SKIN_RARITY_COLORS["NEZA_RAP"] = {233/255, 78/255, 64/255, 1}

STRINGS.SKIN_NAMES.neza_none = L and 'Nezha' or V and 'Na Tra' or '哪吒'
STRINGS.SKIN_NAMES.neza_green = L and 'Lotus' or V and 'Thanh Liên Bạch Ngẫu' or '青莲白藕'
STRINGS.SKIN_NAMES.neza_fire =  L and 'Red Boy' or V and 'Thánh Anh Đại Vương' or "圣婴大王"
STRINGS.SKIN_NAMES.neza_pink = L and 'Lancer Lad' or V and 'Trì Anh Thiếu Niên' or '持缨少年'
STRINGS.SKIN_NAMES.neza_pink = L and 'Lancer Lad' or V and 'Trì Anh Thiếu Niên' or '持缨少年'
STRINGS.SKIN_NAMES.neza_rap = L and 'Lancer Lad' or V and 'Trì Anh Thiếu Niên' or '风火唱将'

STRINGS.UI.RARITY['NEZA_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['NEZA_GREEN'] = L and 'Omnifarious' or V and '千姿百态' or '千姿百态'
STRINGS.UI.RARITY['NEZA_FIRE'] = L and '81 Calamities' or V and '八十一难' or " 八十一难"
STRINGS.UI.RARITY['NEZA_PINK'] = L and 'Quintessence' or V and '国粹戏曲' or '国粹戏曲'
STRINGS.UI.RARITY['NEZA_RAP'] = L and '错位时光' or V and '错位时光' or '错位时光'

STRINGS.SKIN_QUOTES['neza_none'] = L and '"A life for a life, and in the bloody heaven pool I rise!"' or V and '"Lấy mạng đền mạng, sen máu sáng rực trời"' or '\"以命偿命，灼灼天池血莲生\"'
STRINGS.SKIN_QUOTES['neza_green'] = L and '"Lotus remains undefiled, despite growing as a muddy flower-child."' or V and '"Gần bùn mà chẳng hôi tanh mùi bùn"' or '\"出淤泥而不染\"'
STRINGS.SKIN_QUOTES['neza_fire'] = L and '"Wukong! Dare to face my Samadhi Real Fire or not!"' or V and '"Tôn Ngộ Không tới đây ta cho một mồi lửa"' or '\"让你见识一下我的三味真火\"'
STRINGS.SKIN_QUOTES['neza_pink'] = L and '"Dressed up and my Lance in hand, young as I am but I proudly stand!"' or V and '"Cầm lụa ra trận, ít tuổi thì có làm sao?"' or '\"持缨上阵，不足弱冠又如何？\"'
STRINGS.SKIN_QUOTES['neza_rap'] = L and '"Dressed up and my Lance in hand, young as I am but I proudly stand!"' or V and '"Cầm lụa ra trận, ít tuổi thì có làm sao?"' or '\"烈火烹歌起，八分风来助\"'

STRINGS.SKIN_DESCRIPTIONS['neza_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['neza_green'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['neza_fire'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['neza_pink'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['neza_rap'] = L and '' or ''
--wb
-----------------
SKIN_RARITY_COLORS["WHITE_BONE_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["WHITE_BONE_WHITE"] = {1, 1, 1, 1}
SKIN_RARITY_COLORS["WHITE_BONE_OPERA"] = {0.6, 0.6, 1, 1}
SKIN_RARITY_COLORS["WHITE_BONE_QUEEN"] = {190/255, 153/255, 75/255, 1}

STRINGS.SKIN_NAMES.white_bone_none = L and 'White Bone' or V and 'Bạch Cốt Phu Nhân' or '白骨夫人'
STRINGS.SKIN_NAMES.white_bone_white = L and 'Pale Mistress' or V and 'Thương Bạch Phu Nhân' or '苍白夫人'
STRINGS.SKIN_NAMES.white_bone_opera = L and 'Makeup Skin' or V and 'Lê Viên Họa Bì' or '梨园画皮'
STRINGS.SKIN_NAMES.white_bone_queen = L and '西梁女王' or V and '西梁女王' or '西梁女王'

STRINGS.UI.RARITY['WHITE_BONE_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['WHITE_BONE_WHITE'] = L and 'Omnifarious' or V and '千姿百态' or '千姿百态'
STRINGS.UI.RARITY['WHITE_BONE_OPERA'] = L and 'Quintessence' or V and '国粹戏曲' or '国粹戏曲'
STRINGS.UI.RARITY['WHITE_BONE_QUEEN'] = L and '81 Calamities' or V and '八十一难' or '八十一难'

STRINGS.SKIN_QUOTES['white_bone_none'] = L and '"White Bone of a millenium drifts in air, tracing the smell of your blood..."' or V and '"Ngàn năm bạch cốt hóa âm phong"' or '\"千年白骨化阴风\"'
STRINGS.SKIN_QUOTES['white_bone_white'] = L and '"Watch your mouth in front of your Dominatrix!"' or V and '"Trước mặt phu nhân chớ làm càn"' or '\"夫人面前休得放肆!\"'
STRINGS.SKIN_QUOTES['white_bone_opera'] = L and '"Lady turns on her charm not with countenance, but with her coquettish figure."' or V and '"Bốn bề nổi gió âm, dáng xinh hai mươi tuổi"' or '\"四面阴风起，双十年华美容仪\"'
STRINGS.SKIN_QUOTES['white_bone_queen'] = L and '"御弟哥哥，这一别怕是再难相见."' or V and '"御弟哥哥，这一别怕是再难相见"' or '\"御弟哥哥，这一别怕是再难相见\"'

STRINGS.SKIN_DESCRIPTIONS['white_bone_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['white_bone_white'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['white_bone_opera'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['white_bone_queen'] = L and '' or ''
--pg 
-----------------
SKIN_RARITY_COLORS["PIGSY_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["PIGSY_MARRY"] = {1, 0.1, 0.1, 1}
SKIN_RARITY_COLORS["PIGSY_WHITE"] = {124/255, 160/255, 173/255, 1}
SKIN_RARITY_COLORS["PIGSY_CONSTELLATION"] = {178/255, 54/255, 39/255, 1}

STRINGS.SKIN_NAMES.pigsy_none = STRINGS.CHARACTER_NAMES['pigsy']
STRINGS.SKIN_NAMES.pigsy_marry = L and "The Fiancé" or V and 'Cao Gia nữ tế' or '高家女婿'
STRINGS.SKIN_NAMES.pigsy_white = L and "Elephantine" or V and 'Hoàng Nha Lão Tượng' or '黄牙老象'
STRINGS.SKIN_NAMES.pigsy_constellation = L and "室火星宿" or V and '室火星宿' or '室火星宿'

STRINGS.UI.RARITY['PIGSY_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['PIGSY_MARRY'] = L and "Reminiscence" or V and '回忆往昔' or '回忆往昔'
STRINGS.UI.RARITY['PIGSY_WHITE'] = L and "81 Calamities" or V and '八十一难' or '八十一难'
STRINGS.UI.RARITY['PIGSY_CONSTELLATION'] = L and "位列仙班" or V and '位列仙班' or '位列仙班'

STRINGS.SKIN_QUOTES['pigsy_none'] = STRINGS.CHARACTER_QUOTES['pigsy']
STRINGS.SKIN_QUOTES['pigsy_marry'] = L and '"Cuilan, my fiancée! Where ya at?"' or V and '"Haha! Thúy Lan ơi ta tới đây!"' or '\"嘿嘿！翠兰俺来了！\"'
STRINGS.SKIN_QUOTES['pigsy_white'] = L and '"Bold monkey! Get your ass down here and let\'s do this!"' or  V and '"Con khỉ to gan! Không mau lại đấu với ta một trận!"' or '\"大胆猴头 还不下来与我一战！\"'
STRINGS.SKIN_QUOTES['pigsy_constellation'] = L and '"辰从官，季神也，营室星神主之。"' or  V and '"辰从官，季神也，营室星神主之。"' or '\"辰从官，季神也，营室星神主之。！\"'

STRINGS.SKIN_DESCRIPTIONS['pigsy_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['pigsy_marry'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['pigsy_white'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['pigsy_constellation'] = L and '' or ''
--yj
-----------------
SKIN_RARITY_COLORS["YANGJIAN_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["YANGJIAN_BLACK"] = {0.8, 1, 0.9, 1}
SKIN_RARITY_COLORS["YANGJIAN_DAO"] = {0.8, 1, 0.9, 1}
SKIN_RARITY_COLORS["YANGJIAN_GOLD"] = {0.8, 0.8, 0.1, 1}
SKIN_RARITY_COLORS["YANGJIAN_HAWK"] = {0.8, 0.8, 0.1, 1}

STRINGS.SKIN_NAMES.yangjian_none = STRINGS.CHARACTER_NAMES['yangjian']
STRINGS.SKIN_NAMES.yangjian_black = L and "Inky Esquire" or V and 'Mặc Ảnh Tố Tấn' or '墨影素鬓'
STRINGS.SKIN_NAMES.yangjian_dao = L and "Taoist" or V and 'Diệu Đạo Thanh Nguyên' or "妙道清源"
STRINGS.SKIN_NAMES.yangjian_gold = L and "Golden Iliad" or V and 'Lưu Kim Hổ Tướng' or "鎏金虎将"
STRINGS.SKIN_NAMES.yangjian_hawk = L and "金翅大鹏" or V and '金翅大鹏' or "金翅大鹏"

STRINGS.UI.RARITY['YANGJIAN_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['YANGJIAN_BLACK'] = L and "Omnifarious" or V and '千姿百态' or '千姿百态'
STRINGS.UI.RARITY["YANGJIAN_DAO"] = L and "Omnifarious" or V and '千姿百态' or "千姿百态"
STRINGS.UI.RARITY["YANGJIAN_GOLD"] = L and "Quintessence" or V and '国粹戏曲' or "国粹戏曲"
STRINGS.UI.RARITY["YANGJIAN_HAWK"] = L and "81 Calamities" or V and '八十一难' or '八十一难'

STRINGS.SKIN_QUOTES['yangjian_none'] = STRINGS.CHARACTER_QUOTES['yangjian']
STRINGS.SKIN_QUOTES['yangjian_black'] = L and '"And they say Chilvary is dead, dead, dead..."' or V and '"Nuốt sáng đuổi ảnh, quân tử phiêu diêu"' or '\"蚀明坠影，君子潇潇\"'
STRINGS.SKIN_QUOTES['yangjian_dao'] = L and '"They say Chivalry is dead, dead, dead…"' or V and '"Diệu"' or '\"妙道清源，二郎显圣\"'
STRINGS.SKIN_QUOTES['yangjian_gold'] = L and '"He whosoever is worthy shall hath its power..."' or V and '"Phụng Vũ mây biến sắc, Thiên Nhãn chớp điện quang"' or '\"凤羽倒竖云色变,天眼圆睁起电光\"'
STRINGS.SKIN_QUOTES['yangjian_hawk'] = L and '"我于阎浮提，一日食百龙"' or V and '"我于阎浮提，一日食百龙"' or '\"我于阎浮提，一日食百龙\"'

STRINGS.SKIN_DESCRIPTIONS['yangjian_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['yangjian_black'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['yangjian_dao'] = L and "" or ""
STRINGS.SKIN_DESCRIPTIONS['yangjian_gold'] = L and "" or ""
STRINGS.SKIN_DESCRIPTIONS['yangjian_hawk'] = L and "" or ""

--yt
------------------
SKIN_RARITY_COLORS["MYTH_YUTU_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["MYTH_YUTU_FROG"] = {0.2, 0.8, 0.2, 1}
SKIN_RARITY_COLORS["MYTH_YUTU_WINTER"] = {0.2, 0.2, 0.8, 1}
SKIN_RARITY_COLORS["MYTH_YUTU_APRICOT"] = {249/255, 157/255, 144/255, 1}
SKIN_RARITY_COLORS["MYTH_YUTU_LAUREL"] = {204/255, 160/255, 73/255, 1}
SKIN_RARITY_COLORS["MYTH_YUTU_SOPRANO"] = {237/255, 126/255, 138/255, 1}

STRINGS.SKIN_NAMES.myth_yutu_none = STRINGS.CHARACTER_NAMES['myth_yutu']
STRINGS.SKIN_NAMES.myth_yutu_frog = L and '"Chief Cook"' or V and 'Thiềm Cung Ngọc Thiện' or"蟾宫玉膳"
STRINGS.SKIN_NAMES.myth_yutu_winter = L and 'Cozy Winter' or V and 'Hàn Nguyệt Noãn Đông' or"寒月暖冬"
STRINGS.SKIN_NAMES.myth_yutu_apricot = L and 'Apricot Fairy' or V and 'Hạnh Hoa Tiên Tử' or"杏花仙子"
STRINGS.SKIN_NAMES.myth_yutu_laurel = L and 'Fragrant Laurel' or V and '月桂酒香' or"月桂酒香"
STRINGS.SKIN_NAMES.myth_yutu_soprano = L and '悦耳甜音' or V and '悦耳甜音' or"悦耳甜音"

STRINGS.UI.RARITY['MYTH_YUTU_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['MYTH_YUTU_FROG'] = V and 'Omnifarious' or "千姿百态"
STRINGS.UI.RARITY['MYTH_YUTU_WINTER'] = L and 'Omnifarious' or V and '千姿百态' or"千姿百态"
STRINGS.UI.RARITY['MYTH_YUTU_APRICOT'] = L and '81 Calamities' or V and '八十一难' or"八十一难"
STRINGS.UI.RARITY['MYTH_YUTU_LAUREL'] = L and 'Omnifarious' or V and '千姿百态' or"千姿百态"
STRINGS.UI.RARITY['MYTH_YUTU_SOPRANO'] = L and L and '错位时光' or V and '错位时光' or '错位时光'

STRINGS.SKIN_QUOTES['myth_yutu_none'] = STRINGS.CHARACTER_QUOTES['myth_yutu']
STRINGS.SKIN_QUOTES['myth_yutu_frog'] = L and '"Yutu\'s talents are not limited to pounding."' or V and '"Thỏ Ngọc không chỉ biết giã thuốc thôi đâu"' or '\"玉兔也不是只会捣药的哦\"'
STRINGS.SKIN_QUOTES['myth_yutu_winter'] = L and '"You take this lamp, and I\'ll just keep my Sugarcoat Haws~"' or V and '"Cho ngươi lồng đèn, ta bây giờ chỉ thèm kẹo hồ lô"' or '\"灯笼给你，我这儿没有糖葫芦。\"'
STRINGS.SKIN_QUOTES['myth_yutu_apricot'] = L and '"Blossoms of peach or plum, in front me they can\'t help but succumb."' or V and '"Đào mận thơm sao bằng em mưa thấm đỏ cánh mềm"' or '\"桃李芬芳怎敌我雨润红姿娇。\"'
STRINGS.SKIN_QUOTES['myth_yutu_laurel'] = L and '"Three bits of laurel I sigh, seven shreds of moonlight to wine."' or V and '' or '\"三分桂香入酒，七缕月光出梦。\"'
STRINGS.SKIN_QUOTES['myth_yutu_soprano'] = L and '"这直指人心的旋律."' or V and '这直指人心的旋律' or '\"这直指人心的旋律。\"'

STRINGS.SKIN_DESCRIPTIONS['myth_yutu_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['myth_yutu_frog'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['myth_yutu_winter'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['myth_yutu_apricot'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['myth_yutu_laurel'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['myth_yutu_soprano'] = L and '' or ''

--yama_commissioners
SKIN_RARITY_COLORS["YAMA_COMMISSIONERS_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["YAMA_COMMISSIONERS_LOTUS"] = {64/255, 107/255, 117/255, 1}

STRINGS.SKIN_NAMES.yama_commissioners_none = STRINGS.CHARACTER_NAMES['yama_commissioners']
STRINGS.SKIN_NAMES.yama_commissioners_lotus = L and 'Gold Horn & Silver Horn' or V and '莲花洞主' or"莲花洞主"

STRINGS.UI.RARITY['YAMA_COMMISSIONERS_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['YAMA_COMMISSIONERS_LOTUS'] = L and '81 Calamities' or V and '八十一难' or"八十一难"

STRINGS.SKIN_QUOTES['yama_commissioners_none'] = STRINGS.CHARACTER_QUOTES['yama_commissioners']
STRINGS.SKIN_QUOTES['yama_commissioners_lotus'] = L and '"I call you by your name. Dare you answer me?"' or V and '"我叫你一声，你敢答应吗？"' or '\"我叫你一声，你敢答应吗？\"'

STRINGS.SKIN_DESCRIPTIONS['yama_commissioners_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['yama_commissioners_lotus'] = L and '' or ''

--madameweb
SKIN_RARITY_COLORS["MADAMEWEB_NONE"] = SKIN_RARITY_COLORS.Character
SKIN_RARITY_COLORS["MADAMEWEB_NONE_RAT"] = {64/255, 107/255, 117/255, 1}

STRINGS.SKIN_NAMES.madameweb_none = STRINGS.CHARACTER_NAMES['madameweb']
STRINGS.SKIN_NAMES.madameweb_rat = L and 'Gold Horn & Silver Horn' or V and '烛香白鼠' or"烛香白鼠"

STRINGS.UI.RARITY['MADAMEWEB_NONE'] = STRINGS.UI.RARITY.Character
STRINGS.UI.RARITY['MADAMEWEB_RAT'] = L and '81 Calamities' or V and '八十一难' or"八十一难"

STRINGS.SKIN_QUOTES['madameweb_none'] = STRINGS.CHARACTER_QUOTES['madameweb']
STRINGS.SKIN_QUOTES['madameweb_rat'] = L and '"你有看到妾身的鞋子吗?"' or V and '"你有看到妾身的鞋子吗?"' or '\"你有看到妾身的鞋子吗？\"'

STRINGS.SKIN_DESCRIPTIONS['madameweb_none'] = L and '' or ''
STRINGS.SKIN_DESCRIPTIONS['madameweb_rat'] = L and '' or ''
----
local SkinGifts = require("skin_gifts")
STRINGS.THANKS_POPUP.MYTH_SKINITEM = L and 'Thanks for supporting Myth Words' or V and 'Cảm ơn bạn đã ủng hộ Tây Du Ký' or '感谢支持神话书说'
SkinGifts.popupdata.MYTH_SKINITEM = 	{
	atlas = "images/thankyou_myth_gift.xml",
	image = "gift.tex",
	titleoffset = {0, -20, 0},
}
local ex_fns = require "prefabs/player_common_extensions"
local GivePlayerStartingItems = ex_fns.GivePlayerStartingItems
ex_fns.GivePlayerStartingItems = function(inst, ...)
    GivePlayerStartingItems(inst, ...)
    if inst and inst.components.inventory then
        for k, v in pairs(inst.components.inventory.itemslots) do
            if v and v.components.myth_itemskin then
                v.components.myth_itemskin:OnStart(inst)
            end
        end
    end
end
--API
do
    local prefn = function(self, pf, character, ...)
        if isskinchar(character) then 
            table.insert(DST_CHARACTERLIST, character)
        end
    end
    local postfn = function(self, pf, character, ...)
        if isskinchar(character) then
            table.removearrayvalue(DST_CHARACTERLIST, character)
        end
    end

    local LoadOutSelect = require('widgets/redux/loadoutselect')
    local constructor = LoadOutSelect._ctor
    LoadOutSelect._ctor = function (self, ...)
        prefn(self, ...)
        constructor(self, ...)
        postfn(self, ...)
    end
    local mt = getmetatable(LoadOutSelect)
    mt.__call = function(class_tbl, ...)
        local obj = {}
        setmetatable(obj, LoadOutSelect)
        if LoadOutSelect._ctor then
            LoadOutSelect._ctor(obj, ...)
        end
        return obj
    end
end

local old_GetItemCollectionName = GetItemCollectionName
function GLOBAL.GetItemCollectionName(key, ...)
    if isskin(key) and Myth_SkinExtra[key] ~= nil then
        return L and "This skin contains:" or V and "Kèm theo:" or "还包含:"
    else
        return old_GetItemCollectionName(key, ...)
    end
end

local ITEM_STRING = {
    weapon = L and "Weapon skin" or V and 'Giao diện vũ khí' or "专属武器皮肤",
    armor = L and "Armor skin" or V and 'Giao diện giáp' or "专属护甲皮肤",
    wb_beauty = L and "Beauty mode skin" or V and 'Mỹ nhân diện' or "美人形态皮肤",
    yj_pet = L and "Pet skin" or V and 'Giao diện Hao Thiên Khuyển' or "啸天犬皮肤",
    yj_eagle = L and "Eagle skin" or V and 'Giao diện Ưng Hình' or "鹰形态皮肤",
    flycloud = L and "Cloud skin" or V and 'Giao diện Đằng Vân' or "腾云皮肤",
    miao = L and "Transformation skin" or V and 'Giao diện biến hình' or "变身形态皮肤",
    guitar = L and "Guitar skin" or V and 'Giao diện tỳ bà' or "琵琶皮肤",
    pet = L and "Pet skin" or V and 'Giao diện tỳ bà' or "宠物皮肤",
}

AddClassPostConstruct('widgets/redux/itemexplorer', function(self)
    local old_fn = self._CreateWidgetDataListForItems
    function self:_CreateWidgetDataListForItems(...)
        local t = old_fn(self, ...)
        for _, data in ipairs(t)do
            if type(data.item_key) == 'string' and isskin(data.item_key) then
                data.is_owned = hasskin(data.item_key)
                data.owned_count = data.is_owned and 1 or 0
            end
        end
        return t
    end

    --无视过滤
    local old_refresh = self.RefreshItems
    function self:RefreshItems(fn, ...)
        if self.primary_item_type == 'base' then
            local parent = self.parent
            if parent and parent.owner and isskinchar(parent.owner.currentcharacter) then
                fn = function() return true end
            end
        end
        return old_refresh(self, fn, ...)
    end

    local old_app = self._ApplyDataToDescription
    function self:_ApplyDataToDescription(data, ...)
        old_app(self, data, ...)
        local item = data and data.item_key
        self.myth_skinitem = item
        if isskin(item) or isskinnone(item) then
            local s = ""
            for _,v in ipairs(Myth_SkinExtra[item] or {})do
                s = s .. ITEM_STRING[v] .. "\n"
            end
            self.description:SetString(s)
            self.action_info:SetString("")
            if self.market_button then
                self.market_button:Hide()
            end
            if self.commerce then
                self.commerce:Hide()
            end
            self:_Myth_ShowUnlockUI(not hasskin(item))
        else 
            if self.market_button then
                self.market_button:Show()
            end
            if self.commerce then 
                self.commerce:Show()
            end
            self:_Myth_ShowUnlockUI(false)
        end
    end

    --新ui
    local foot = self.footer
    local Text = require 'widgets/text'
    local Button = require "widgets/button"
    local TextButton = require 'widgets/textbutton'
    local Sc = require "screens/myth_skin_unlock"
    -- package.loaded['screens/myth_skin_unlock'] = nil
    local width = self.action_info:GetRegionSize()
    local unlock = self.action_info:AddChild(TextButton('unlock'))

    unlock:SetPosition(0, 0)
    unlock:SetFont(HEADERFONT)
    unlock.text:SetRegionSize(width, 30)
    unlock.text:SetHAlign(ANCHOR_LEFT)
    unlock:SetTextSize(30)
    unlock:SetText(L and 'Unlock' or V and 'Mở Khóa' or '解锁')
    unlock:Hide()
    unlock:SetOnClick(function() TheFrontEnd:PushScreen(Sc(self.myth_skinitem)) end)
    self._myth_unlock = unlock

    self._Myth_ShowUnlockUI = function(self, val)
        if val then
            self._myth_unlock:Show()
        else
            self._myth_unlock:Hide()
        end
    end
end)

local old_fn = CompareRarities
function GLOBAL.CompareRarities(k1,k2)
    local w1, w2
    if isskin(k1) or isskin(k2) then
        return true
    end
    return old_fn(k1,k2)
end

table.insert(Assets, Asset("ATLAS", "images/myth_skin_unlock.xml"))
