local assets =
{
    Asset("ANIM", "anim/nz_lance.zip"),
    Asset("ATLAS", "images/inventoryimages/nz_lance.xml"),
}


local function onequip(inst, owner)
	inst.components.myth_itemskin:OverrideSymbol(owner, "swap_object")
	owner.AnimState:Show("ARM_carry")
	owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
	owner.AnimState:ClearOverrideSymbol("swap_object")
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end
local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    for r = 5, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function Lunge(inst, doer, pos)
	doer:PushEvent("nzcombat_superjump", {targetpos = pos, weapon = inst})
	inst.components.rechargeable:StartRecharging()
	local fx = SpawnPrefab("abigail_retaliation")
	if fx then
		fx.Transform:SetPosition(doer.Transform:GetWorldPosition())
	end
end

local function onattack(inst, owner, target)
    local skin  = inst.components.myth_itemskin.skin:value()
    if skin == "rap"  and target and target:IsValid() then
        local pos = target:GetPosition()
        for k = 1 ,math.random(1,2) do
            SpawnPrefab("nz_lance_rapfx"):Start(pos)
        end
    end
end

local function leapfn(inst,doer, pos, targetpos)
	local x, y, z = doer.Transform:GetWorldPosition()
    SpawnPrefab("fx_boat_pop").Transform:SetPosition(x, y, z)
    SpawnPrefab("winona_battery_high_shatterfx").Transform:SetPosition(x, y, z)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, 4, {"_combat", }, { "playerghost", "INLIMBO", "player","companion","wall" })) do
        if v and v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) then
            if doer ~= nil and doer:IsValid() and doer.components.combat and doer.components.combat:CanTarget(v) then
				doer:PushEvent("onareaattackother", { target = v, weapon = inst or nil, stimuli = "nz_lance" })
				local damage = 70
				if doer.components.debuffable and doer.components.debuffable:HasDebuff("condensed_pill_buff") then
					damage = 100
				end
				v.components.combat:GetAttacked(doer, damage, inst, "nz_lance")
				if v.sg ~= nil and v.sg:HasState("hit")
					and v.components.health ~= nil and not v.components.health:IsDead()
					and not v.sg:HasStateTag("transform")
					and not v.sg:HasStateTag("nointerrupt")
					and not v.sg:HasStateTag("frozen")
					then
					if v.components.sleeper ~= nil then
						v.components.sleeper:WakeUp()
					end
					v.sg:GoToState("hit")
				end
			end
        end
    end	
end

local function aoespellsg(inst,doer,action)
    return "nzcombat_superjump_start"
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
	inst.entity:AddNetwork()
	
    inst.MiniMapEntity:SetIcon("nz_lance.tex")

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("nz_lance")
    inst.AnimState:SetBuild("nz_lance")
    inst.AnimState:PlayAnimation("idle_none")

    inst:AddTag("sharp")
    inst:AddTag("pointy")

    inst:AddTag("rechargeable")
	
	inst:AddTag("nzaoeweapon_leap")
	
    inst:AddTag("superjump")
	
    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetRange(30)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoesmall"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoesmallping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true	

    inst:AddComponent("myth_itemskin")
    inst.components.myth_itemskin.character = 'neza'
    inst.components.myth_itemskin.prefab = 'nz_lance'
    inst.components.myth_itemskin:SetDefaultData{"green", "fire", "pink", "rap"}
    -- inst.components.myth_itemskin:SetData{
    --     green = {
    --         swap = {build = "nz_lance", folder = "swap_green"},
    --         icon = {atlas = "nz_lance.xml", image = "nz_lance_green"},
    --         anim = {bank = "nz_lance", build = "nz_lance", anim = "idle_green"},
    --     },
    --     default = {
    --         swap = {build = "nz_lance", folder = "swap_none"},
    --         icon = {atlas = "nz_lance.xml", image = "nz_lance_none"},
    --         anim = {bank = "nz_lance", build = "nz_lance", anim = "idle_none"},
    --     },
    -- }
	
    inst.myth_aoespellsg = aoespellsg
    
    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(90)
    inst.components.weapon:SetRange(1.3, 1.3)
    inst.components.weapon.onattack = onattack

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "nz_lance"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/nz_lance.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

	inst:AddComponent("myth_aoespell")
    inst.components.aoespell = inst.components.myth_aoespell
	inst.components.aoespell:SetSpellFn(Lunge)
	inst:RegisterComponentActions("aoespell")
	
	inst:AddComponent("myth_aoeweapon_leap")
	inst.components.aoeweapon_leap = inst.components.myth_aoeweapon_leap
	inst.components.aoeweapon_leap:SetLeapfn(leapfn)
	inst:RegisterComponentActions("aoeweapon_leap")

	inst:AddComponent("myth_rechargeable")
    inst.components.rechargeable = inst.components.myth_rechargeable
	inst.components.rechargeable:SetRechargeTime(12)
	inst:RegisterComponentActions("rechargeable")

	MakeHauntableLaunch(inst)
	
    return inst
end

local function fxfn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst:AddTag("FX")
	inst:AddTag("NOCLICK")

    inst.AnimState:SetBank('wilson')
    inst.AnimState:SetBuild('neza')
    inst.AnimState:SetMultColour(1,1,1,.8)
    inst.AnimState:PlayAnimation('superjump_land')
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst.persists = false
	inst:ListenForEvent('animover',inst.Remove)
	return inst
end


local function GetBezierPos(p0,p1,p2,t)
	return p0*(1-t)*(1-t)+p1*2*t*(1-t)+p2*t*t
end

local function dorapmove(inst)
	inst.time = inst.time + FRAMES
	local pos = GetBezierPos(inst.startpos,inst.midpos,inst.endpos,inst.time/inst.maxtime)
	inst.Transform:SetPosition(pos:Get())
	if inst.time >= inst.maxtime  then
		inst.movetask:Cancel()
		inst.AnimState:SetBank("lavaarena_boarrior_fx")
		inst.AnimState:SetBuild("guitar_jadehare_dot")
		inst.AnimState:PlayAnimation("ground_hit_1")
		--inst.AnimState:SetScale(0.7, 0.7)
		inst:ListenForEvent("animover", inst.Remove)
	end
end

local function onstart(inst,pos)
    inst.Transform:SetPosition(pos:Get())
    inst.startpos = pos
	local facing_angle = math.random(360) * DEGREES
	local midrange = GetRandomMinMax(1, 2.5)
	local maxrange = GetRandomMinMax(2.5,4.5)
	inst.midpos = Vector3(pos.x + midrange * math.cos(facing_angle), pos.y +GetRandomMinMax(1, 4), pos.z - midrange * math.sin(facing_angle))
	inst.endpos = Vector3(pos.x + maxrange * math.cos(facing_angle), 0, pos.z - maxrange * math.sin(facing_angle))
	inst.maxtime = GetRandomMinMax(1,2) 
	inst.time = 0
	inst.movetask = inst:DoPeriodicTask(0,dorapmove)
end

local builds = {
	"guitar_jadehare_note",
	"guitar_jadehare_noteother1",
	"guitar_jadehare_noteother2",
	"guitar_jadehare_noteother3",
	"guitar_jadehare_noteother4"
}
local colors = {
	{255/255,64/255,14/255,1},
	{255/255,182/255,0/255,1},
	{255/255,0/255,31/255,1},
	{255/255,234/255,96/255,1},
}

local function rapfn()
	local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.AnimState:SetBank("guitar_jadehare_note")
	inst.AnimState:SetBuild(builds[math.random(#builds)])
	inst.AnimState:SetMultColour(unpack(colors[math.random(#colors)]))
	inst.AnimState:PlayAnimation("idle_pre")
	inst.AnimState:PushAnimation("idle")

	local sca = GetRandomMinMax(0.6, 1)
	inst.Transform:SetScale(sca, sca, sca)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.entity:SetCanSleep(false)
    inst.persists = false
    inst.Start = onstart
	return inst
end

return Prefab("nz_lance", fn, assets),
    Prefab("nz_lance_rapfx", rapfn),
	Prefab("nz_lance_fx", fxfn)
		