local assets =
{
    Asset("ANIM", "anim/myth_cqf.zip"),
    Asset("ATLAS", "images/inventoryimages/myth_cqf.xml"),
}


local bannedchar = {
    yama_commissioners = true,
    white_bone = true,
    wortox = true,
    wormwood = true,
    wurt = true,
    wanda = true,
}

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

local function onuse(inst,doer)
    if not bannedchar[doer.prefab] then
        if doer and doer._is_player_astral ~= nil  and doer._is_player_astral:value() then
            return false
        end

        if doer.components.rider ~= nil and doer.components.rider:IsRiding() then
            return false
        end
        if IsFlying(doer) then
            return false
        end
        local body  = SpawnPrefab("yama_resurrectionstatue")
        body.Transform:SetPosition(doer:GetPosition():Get())
        body:SetOwner(doer)

        SpawnPrefab("white_bone_changefx" ).Transform:SetPosition(body.Transform:GetWorldPosition())
        if doer.components.player_astral then
            doer.components.player_astral:SetAstral(true)
            doer.components.player_astral.astral_body = body
        end
        if doer._is_be_ghosted_players ~= nil then
            for k,v in pairs(doer._is_be_ghosted_players) do 
                if k and k.components.player_astral then
                    k.components.player_astral:StopPlayer()
                end
            end
        end
        inst.components.stackable:Get():Remove()
        return true
    end
end

--DebugSpawn"wilson".components.player_astral:SetPlayer(ThePlayer)

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_cqf")
    inst.AnimState:SetBuild("myth_cqf")
    inst.AnimState:PlayAnimation("idle")

    inst.onusesgname = "dolongaction"


    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("tradable")

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_cqf.xml"

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = true
    inst.components.myth_use_inventory:SetOnUseFn(onuse)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("myth_cqf", fn, assets)
