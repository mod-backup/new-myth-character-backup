local assets = {
    Asset("ANIM", "anim/hua_fake_spider_shoe.zip"),
    Asset("ATLAS", "images/inventoryimages/hua_fake_spider_shoe.xml"),
}

local function MakeEffect(inst)
    local grandowner = inst.components.inventoryitem.owner
    if grandowner and grandowner:HasTag("player") and grandowner.components.locomotor.wantstomoveforward then
        local x, y, z = grandowner.Transform:GetWorldPosition()
        local facing_angle = (grandowner.Transform:GetRotation() +  math.random(-20,20))  * DEGREES 
        x = x - 0.5 * math.cos(facing_angle)
        z = z + 0.5 * math.sin(facing_angle)
        local jiaoyin = SpawnPrefab("hua_fake_spider_shoefx")
		jiaoyin.Transform:SetPosition(x, y, z)

        local oncreep = TheWorld.GroundCreep:OnCreep(x, y, z)
        if oncreep and not grandowner:HasTag("spiderwhisperer") then
            inst.components.fueled:DoDelta(-0.1)
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("hua_fake_spider_shoe")
    inst.AnimState:SetBuild("hua_fake_spider_shoe")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("hua_fake_spider")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("fueled")
    inst.components.fueled.fueltype = FUELTYPE.USAGE
    inst.components.fueled:InitializeFuelLevel(300)
    inst.components.fueled:SetDepletedFn(inst.Remove)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hua_fake_spider_shoe.xml"

    inst:AddComponent("tradable")

    inst:DoPeriodicTask(0.1, MakeEffect)

    return inst
end

local function fxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
    inst:AddTag("FX")
    inst.entity:SetCanSleep(false)

    inst.AnimState:SetFinalOffset(-1)

    inst.AnimState:SetBank("hua_fake_spider_shoe")
    inst.AnimState:SetBuild("hua_fake_spider_shoe")
    inst.AnimState:PlayAnimation("fx")

    inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
    inst.AnimState:SetLayer( LAYER_BACKGROUND )
    inst.AnimState:SetSortOrder( 3 )
	
    local scale = math.random(50,80)*0.01
	inst.Transform:SetScale(scale, scale, scale) --大小
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false

	inst:DoTaskInTime(0.08, ErodeAway)

    return inst
end

return Prefab("hua_fake_spider_shoe", fn, assets),
    Prefab("hua_fake_spider_shoefx", fxfn)