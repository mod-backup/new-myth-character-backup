local ex_fns = require "prefabs/player_common_extensions"
local function Empty()
end
local function GetHopDistance(inst, speed_mult)
	return speed_mult < 0.8 and TUNING.WILSON_HOP_DISTANCE_SHORT
			or speed_mult >= 1.2 and TUNING.WILSON_HOP_DISTANCE_FAR
			or TUNING.WILSON_HOP_DISTANCE
end

local function OnStarve(inst,data)
    if inst.components.mk_silkflyer and inst.components.mk_silkflyer.flying and not inst.components.health:IsDead()
        and not inst.sg:HasStateTag("silkfly_down") then
		inst.sg:GoToState("myth_silkfly_down")
	end
end
local SHADECANOPY_MUST_TAGS = {"shadecanopy"}
local SHADECANOPY_SMALL_MUST_TAGS = {"shadecanopysmall"}
local function canflyfn(inst, pos)
	local canopy = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE, SHADECANOPY_MUST_TAGS)
	local canopy_small = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE_SMALL, SHADECANOPY_SMALL_MUST_TAGS)
	if #canopy > 0 or #canopy_small > 0 then
		return true
	end
	return false
end

local function onmove(inst)
	local pos = inst:GetPosition()
	if not canflyfn(inst, pos) and not inst.components.health:IsDead() and not inst.sg:HasStateTag("silkfly_down") then
		inst.sg:GoToState("myth_silkfly_down")
	end
end
--------------------================================================
local Flyer = Class(function(self, inst)
    self.inst = inst
	self.flying = false

	self.quake = function(world)
		if self.flying and not self.startquake  then
			self.startquake = true
			self.inst:DoTaskInTime(2,function()
				self.startquake = false
				if self.inst.TumbleFn then
					self.inst:TumbleFn()
				end
			end)
		end
	end
end)

function Flyer:SetFlying(val,load)
    local inst = self.inst
	if val then
		if self.flying then
			return
		end

		if inst.components.inventory then
			inst.components.inventory:Close()
		end

		if inst.components.catcher ~= nil then
			inst.components.catcher:SetEnabled(false)
		end

		if self.flyfn then
			self.flyfn(self, self.inst, true)
		end

		inst:AddTag("NOHIGHLIGHT")
		inst:AddTag("NOTARGET")
		inst:AddTag("INVISIBLE")

		if inst.Physics then
			RemovePhysicsColliders(inst)
		end

        if inst.components.hunger ~= nil then
            inst.components.hunger.burnratemodifiers:SetModifier("silkfly", 4)
        end

		if inst.components.drownable then
			inst.components.drownable.enabled = false
		end
		self.build  = self.inst.AnimState:GetBuild()
		self.inst.AnimState:SetBuild("madameweb_kissbuild")
		self.flying = true
		if load then
			self.inst:DoTaskInTime(0.2,function()
				if self.flying then
					self.inst.AnimState:SetBuild("madameweb_kissbuild")
				end
				inst:Hide()
				inst.AnimState:SetMultColour(0,0,0,0)
			end)
		end
		self.inst.components.health:SetInvincible(true)
		self.inst._silkfly:set(true)
		inst:ListenForEvent("startstarving", OnStarve)	
		inst:ListenForEvent("startquake", self.quake, TheWorld)
		inst:ListenForEvent("startquake", self.quake, TheWorld.net)

		if not TheWorld:HasTag("cave") then
			inst:ListenForEvent("locomote",onmove)
		end
		inst.AnimState:SetMultColour(0,0,0,0)
		inst:Hide()
		--TheWorld:PushEvent("startquake")
	else
		if inst.components.inventory then
			inst.components.inventory:Open()
		end
		if self.unflyfn then
			self.unflyfn(self, self.inst, false)
		end
		inst:RemoveTag("NOHIGHLIGHT")
		inst:RemoveTag("NOTARGET")
		inst:RemoveTag("INVISIBLE")

		if inst.Physics then
			ChangeToCharacterPhysics(inst)
		end

        if inst.components.hunger ~= nil then
            inst.components.hunger.burnratemodifiers:RemoveModifier("silkfly")
        end

		if inst.components.drownable then
			inst.components.drownable.enabled = true
		end
		self.flying = false
		self.inst._silkfly:set(false)
		self.inst.AnimState:SetBuild(self.build or self.inst.prefab)
		self.inst.components.health:SetInvincible(false)
		inst:RemoveEventCallback("startstarving", OnStarve)	
		inst:RemoveEventCallback("startquake", self.quake, TheWorld.net)	
		inst:RemoveEventCallback("startquake", self.quake, TheWorld)
		if not TheWorld:HasTag("cave") then
			inst:RemoveEventCallback("locomote",onmove)
		end
	end
end

function Flyer:OnSave()
	return {isflying = self.flying}
end

function Flyer:OnLoad(data)
	if data.isflying then
		self:SetFlying(true,true)
	end
end

return Flyer