



--===========这里开始是 一些专属物品的不同的检查用的
STRINGS.CHARACTERS.MONKEY_KING = require "characterspeech/speech_monkeyking_ch"
STRINGS.CHARACTERS.NEZA = require "characterspeech/speech_neza"
STRINGS.CHARACTERS.WHITE_BONE = require "characterspeech/speech_white_bone_jp"
STRINGS.CHARACTERS.PIGSY = require "characterspeech/speech_pigsy_jp"
STRINGS.CHARACTERS.YANGJIAN = require "characterspeech/speech_yangjian_jp"
STRINGS.CHARACTERS.MYTH_YUTU = require "characterspeech/speech_yutu_jp"
STRINGS.CHARACTERS.YAMA_COMMISSIONERS = require "characterspeech/speech_yama_commissioners" --to do
STRINGS.CHARACTERS.MADAMEWEB = require "characterspeech/speech_mademeweb"

STRINGS.CHARACTER_TITLES.monkey_king = "齐天大圣"
STRINGS.CHARACTER_NAMES.monkey_king = "孙悟空"
STRINGS.CHARACTER_DESCRIPTIONS.monkey_king = "*神通广大的美猴王\n*除了金箍棒，其他兵器不称手\n*抗天兵落得花果山生灵涂炭\n*厌荤食素且最爱瓜果\n*水性不好，不喜毛发沾湿"
STRINGS.CHARACTER_QUOTES.monkey_king = "\"睥睨意笑满天仙神皆无用\""

--neza
STRINGS.CHARACTER_TITLES.neza = "哪吒三太子"
STRINGS.CHARACTER_NAMES.neza = "哪吒"
STRINGS.CHARACTER_DESCRIPTIONS.neza = "*灵珠转世，藕身翩翩少年\n*多件神兵法宝在他手\n*心如顽童却受神职所缚"
STRINGS.CHARACTER_QUOTES.neza = "\"以命偿命，灼灼天池血莲生\""

STRINGS.CHARACTER_TITLES.white_bone = "「白骨夫人」"
STRINGS.CHARACTER_NAMES.white_bone = "「白骨夫人」"
STRINGS.CHARACTER_DESCRIPTIONS.white_bone = "*尸魔成精、尸骨を操ることができる。\n*精血が好きで、陰険で狡猾だ。\n*神出鬼没，二つの顔。"
STRINGS.CHARACTER_QUOTES.white_bone = "\"千年の白骨は陰風と変わる\""

STRINGS.CHARACTER_TITLES.pigsy = "「天蓬元帅」"
STRINGS.CHARACTER_NAMES.pigsy = "「猪八戒」"
STRINGS.CHARACTER_DESCRIPTIONS.pigsy = "*左遷されて転生した。豚人と友好的である。\n*食いしん坊で寝つきがよく，皮が粗く肉が厚い。\n*農作業が得意で、制作が苦手で、恨みを惹きつけやすい体質。"
STRINGS.CHARACTER_QUOTES.pigsy = "\"せいぜいに解散だ\""

STRINGS.CHARACTER_TITLES.yangjian = "「二郎真君」"
STRINGS.CHARACTER_NAMES.yangjian = "「杨戬」"
STRINGS.CHARACTER_DESCRIPTIONS.yangjian = "三つ目は神眼、万里でも追跡できる\n「灌口」の神、闇を洞察する\n神犬【哮天】は、左右に相伴っている"
STRINGS.CHARACTER_QUOTES.yangjian = "\"【天眼】開って、万妖は逃げられない\""

STRINGS.CHARACTER_TITLES.myth_yutu = "玉兔仙子"
STRINGS.CHARACTER_NAMES.myth_yutu = "玉兔"
STRINGS.CHARACTER_DESCRIPTIONS.myth_yutu = "*玲珑仙兔，狡兔三窟\n*长居月宫，捣药为职\n*思乡之情，月蛾引路"
STRINGS.CHARACTER_QUOTES.myth_yutu = "\"捣药的事情明天再说\""

STRINGS.CHARACTER_TITLES.yama_commissioners = "勾魂阴差"
STRINGS.CHARACTER_NAMES.yama_commissioners = "黑白无常"
STRINGS.CHARACTER_DESCRIPTIONS.yama_commissioners = "*黑白双生，阴间幽冥\n*阴司在世，命从阎罗\n*摄魂勾魄，引渡黄泉"
STRINGS.CHARACTER_QUOTES.yama_commissioners = "\"厉鬼勾魂，无常索命\""

STRINGS.CHARACTER_TITLES.madameweb = "盘丝娘娘"
STRINGS.CHARACTER_NAMES.madameweb = "盘丝娘娘"
STRINGS.CHARACTER_DESCRIPTIONS.madameweb = "*蜘蛛成精，自立为后\n*织网缠丝，丝薄弱火\n*毒狠网牢，见雀张罗"
STRINGS.CHARACTER_QUOTES.madameweb = "\"不如来我盘丝洞中坐坐？\""

STRINGS.NAMES.WHITE_BONE = "「白骨夫人」"
STRINGS.NAMES.MONKEY_KING = "「齐天大圣」"
STRINGS.NAMES.MK_PHANTOM = "「齐天大圣」"
STRINGS.NAMES.NEZA = "「哪吒三太子」"
STRINGS.NAMES.YANGJIAN = "「二郎真君」"
STRINGS.NAMES.PIGSY = "「天蓬元帅」"
STRINGS.NAMES.MYTH_YUTU = "「玉兔」"

STRINGS.SKIN_NAMES.pigsy_none = "「天蓬元帅」"
STRINGS.SKIN_NAMES.yangjian_none = "「二郎真君」"
STRINGS.SKIN_NAMES.myth_yutu_none = "「玉兔」"

STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.NZ_PLANT={
	HASONE = "ここには植えられない。",
										}		
							
STRINGS.NAMES.MK_JGB = "「金箍棒」" 
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MK_JGB = "どのような強力な武器！" 

STRINGS.NAMES.MK_JGB_PILLAR = '「金箍棒」'
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MK_JGB = "どのような強力な武器！"

STRINGS.NAMES.LOTUS_FLOWER = "蓮の花"
STRINGS.RECIPE_DESC.LOTUS_FLOWER = "命をもって償う。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LOTUS_FLOWER = "いくつかの美しい装飾"

STRINGS.NAMES.LOTUS_FLOWER_COOKED ="焼き蓮根"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LOTUS_FLOWER_COOKED = "おいしい料理。"

STRINGS.NAMES.NZ_LANCE ="「火尖枪」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_LANCE = "この矛には魔力がある。"	
STRINGS.NAMES.NZ_DAMASK ="「混天绫」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_DAMASK = "なんときれいなシルク!"	
STRINGS.NAMES.NZ_RING ="「乾坤圈」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.NZ_RING = "これは普通の輪ではない。"

STRINGS.NAMES.WB_HEART = "陰気な心."
STRINGS.RECIPE_DESC.WB_HEART = "あなたの魂をあなたの体から離れさせる。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_HEART = "邪悪の心!"

STRINGS.NAMES.BONE_WAND = "白骨の杖"
STRINGS.RECIPE_DESC.BONE_WAND = "鋭い骨刺！"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_WAND = "陰気なにおいがします！"

STRINGS.NAMES.BONE_BLADE = "骨の剣"
STRINGS.RECIPE_DESC.BONE_BLADE = "敵人に血を流せる。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_BLADE = "陰気なにおいがします！"

STRINGS.NAMES.BONE_WHIP = "骨の鞭"
STRINGS.RECIPE_DESC.BONE_WHIP = "新鮮な肉を剥ぐ。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_WHIP = "陰気なにおいがします！"

STRINGS.NAMES.BONE_PET = "白骨の付き人"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_PET = "怖い！"

STRINGS.NAMES.MK_JGB_REC = "「金箍棒」"
STRINGS.RECIPE_DESC.MK_JGB_REC = "「金箍棒」を召喚する!"


STRINGS.PGONEATMEAT = "仏は心に留め置く。"
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD.NOSLEEPWHENRIDER = "この牛は揺れるので、眠れません！"
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD.NOSLEEPWHENSTARVE = "お腹が空いて眠れません" --没饥饿做不了睡觉会说啥

STRINGS.NAMES.PIGSY_SLEEPBED = "「草垛」" --草席的名字
STRINGS.RECIPE_DESC.PIGSY_SLEEPBED = "お腹が空に向かって横になると、神仙よりも快適です。" --草席配方描述

STRINGS.NAMES.PIGSY_RAKE = "「九齿钉耙」" --钉耙的名字
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_RAKE = "「五方五帝」と「六丁六甲」の助けを得て、この熊手を作り出しことができました！" --钉耙的检查

STRINGS.PIGSY250 = "お腹がいっぱいになったら寝るべき...仕事ですよね!"
STRINGS.PIGSY75 = "お腹の中がとても広く，1食は3，5斗ぐらい。"
STRINGS.PIGSY50 = "胸が背中にくっつくほどお腹が空きました。"

STRINGS.NAMES.PIGSY_SOIL = "土の山"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_SOIL = "万粒の食糧でもここからが始まります。" --土堆的检查

STRINGS.NAMES.PIGSY_HAT = "「墨兰帽」" 
STRINGS.RECIPE_DESC.PIGSY_HAT = "「八戒」の大脳門に風邪を引くことを防ぐ。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGSY_HAT = "暖かい帽子！"
STRINGS.NOPIGSY_RAKE = "君子は礼儀正しく，位でなければつり合わない。"

STRINGS.RECIPE_DESC.SKYHOUND_TOOTH = "忠心的な「哮天犬」を弔う。"

STRINGS.NAMES.YJ_SPEAR = "「三尖两刃刀」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "三つ鋒は刃のように天の上に痕跡を刻める"

STRINGS.NAMES.YANGJIAN_HAIR = "「三山飞凤冠」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "三山は暮れば遠く，飛鳳清鳴。"

STRINGS.NAMES.YANGJIAN_ARMOR = "「锁子清源甲」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YJ_SPEAR = "このような非常に冷たい宝物は、誰のものかわからない。"

STRINGS.NAMES.SKYHOUND = "「哮天犬」"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SKYHOUND = "君が太陽と月を食べられることと聞きました、本当か？"

STRINGS.NAMES.YJ_SPEAR_NEDDTHUNDER = "また「雷公电母」が「杨戬」を助けてくれて頼む！"

STRINGS.NAMES.YANGJIAN_TRACK = "「展翅化鹰」"
STRINGS.RECIPE_DESC.YANGJIAN_TRACK = "風を乗って、九万里の空へ飛んで行け！"
STRINGS.NAMES.YANGJIAN_TRACK_NOHUNGER = "お腹が空いたら、空を飛ぶ力がありますか？"
STRINGS.NAMES.YANGJIAN_TRACK_INHOUSE = "狭いスペースは発揮するにくいです。"

STRINGS.NAMES.YT_RABBITHOLE = STRINGS.NAMES.RABBITHOLE--不用写
STRINGS.RECIPE_DESC.RABBITHOLE = "幸せな家" -- --兔子洞的配方描述

STRINGS.NAMES.YT_DAOYAO = "「捣药」" --捣药配方名字
STRINGS.RECIPE_DESC.YT_DAOYAO = "薬をつく." --捣药配方描述

STRINGS.NAMES.YT_MOONBUTTERFLY = "月蛾を道案内"
STRINGS.RECIPE_DESC.MOONBUTTERFLY = "花で蝶を呼び，月へひらひらと舞う" -- 月娥配方描述
STRINGS.RECIPE_DESC.YT_MOONBUTTERFLY = "花で蝶を呼び，月へひらひらと舞う" -- 月娥配方描述

STRINGS.MYTH_NOPESTLE = "私は薬棒が必要です。" --捣药提醒


STRINGS.NAMES.MEDICINE_PESTLE_MYTH = "「捣药杵」" --捣药杵名字
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MEDICINE_PESTLE_MYTH = "羊脂の玉の一段に，薬の香りが千年を巻き付ける" --检查描述

STRINGS.YJFLYTS = "右クリックで鷹が開いているリング内または左クリックでプレイヤーのアイコンを追跡できます。操作なしで20秒後に自動的に地面に落ちます。"
STRINGS.MJ_JGB = "重すぎる！これは持てません！!"
STRINGS.NE_TEDING = "それは使えません！"
STRINGS.MKONEATMEAT = "OoO"
STRINGS.MKINSANDSTORM = "OoO"
STRINGS.YJINSANDSTORM = "この嫌い砂ぼこり。"
STRINGS.MKDROPWEAPON = "OoO"
STRINGS.ACTIONS.CASTAOE.MK_JGB = "「身外身法」"
STRINGS.ACTIONS.CASTAOE.NZ_LANCE = "「三头六臂」"
STRINGS.ACTIONS.CASTAOE.YJ_SPEAR = "「驱雷策电」"
STRINGS.ACTIONS.CASTAOE.PENNANT_COMMISSIONER = "「招魂」"
STRINGS.WBRECIPE = "骨"
STRINGS.NZ_PLANT = "植物"
STRINGS.USE_GOURD = "使う"
STRINGS.MKBIANHUAN = "変化する"
STRINGS.WBBIANHUAN = "霧"
STRINGS.PIGBIANHUAN = "嘲讽"
STRINGS.REPAIR_BONE = "修理する"
STRINGS.WHITE_BONE_RIGHT = "溶融する"
STRINGS.WHITE_BONE_LEFT = "解剖する"
STRINGS.BONEPETSFULL = "随従の数はもう十分です。"
STRINGS.MONKEY_COOK = "調理する"
STRINGS.MONKEY_HARVEST = "収穫する"

---新加
STRINGS.MYTH_PICKUP_PILLAR = "回収"
STRINGS.YTBIANHUAN = "遁地"
STRINGS.YTBIANHUAN_OUT  ="帰る"

STRINGS.NAMES.YANGJIAN_BUZZARD_SPAWN  ="唤鹰"
STRINGS.RECIPE_DESC.YANGJIAN_BUZZARD_SPAWN = "金眼银翅，擒拿无双"

STRINGS.NAMES.YANGJIAN_BUZZARD  ="傲天鹰"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.YANGJIAN_BUZZARD= "这难道是东方的猫头鹰吗？"

STRINGS.NAMES.WB_ARMORBLOOD  = "血色霓"
STRINGS.RECIPE_DESC.WB_ARMORBLOOD = "漫天血色，尽染霓虹"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORBLOOD = "杜鹃泣血，春风不归"

STRINGS.NAMES.WB_ARMORBONE  = "坚骨披"
STRINGS.RECIPE_DESC.WB_ARMORBONE = "重重白骨护我身"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORBONE = "这披风到底害死了多少条命"

STRINGS.NAMES.WB_ARMORLIGHT  = "盈风绸"
STRINGS.RECIPE_DESC.WB_ARMORLIGHT = "羽翼盈风，我登青云"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORLIGHT = "清风锁不住，盈羽月夜归"

STRINGS.NAMES.WB_ARMORSTORAGE  = "蕴玄袍"
STRINGS.RECIPE_DESC.WB_ARMORSTORAGE = "衣袍猎猎，袖里乾坤"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORSTORAGE = "衣带之下，别有乾坤"

STRINGS.NAMES.WB_ARMORSTORAGE_BACK  = STRINGS.NAMES.WB_ARMORSTORAGE

STRINGS.NAMES.WB_ARMORFOG  = "雾隐裳"
STRINGS.RECIPE_DESC.WB_ARMORFOG = "风隐阵阵，雾隐迟迟"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORFOG = "霜云映月雾朦胧，美人似坐于镜中"

STRINGS.NAMES.WB_ARMORGREED  = "不餍衣"
STRINGS.RECIPE_DESC.WB_ARMORGREED = "我，永不餍足"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WB_ARMORGREED = "那衣物好似一只不知餍足的怪物"

STRINGS.WB_ARMORSTR = {
	WB_ARMORBLOOD = "清凉,获得极其强力的白骨吸血能力",
	WB_ARMORBONE = "清凉,获得较高的护甲,但会失去白骨吸血能力",
	WB_ARMORLIGHT = "清凉,风起,大幅提升移动速度",
	WB_ARMORSTORAGE = "暖和,可以携带更多物品,自动吸取同类物品",
	WB_ARMORFOG = "暖和,扩大妖雾范围,要妖雾中巨幅提升移速\n获得少量的护甲和强力吸血能力",
	WB_ARMORGREED = "暖和,贪婪会使你获得更多的战利品\n召唤喽喽存在时间延长",
}

STRINGS.NAMES.BONE_MIRROR  = "白骨妖镜"
STRINGS.RECIPE_DESC.BONE_MIRROR = "明镜妆里影自怜"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BONE_MIRROR = {
	MAX = "眉淡秋山羞镜台，海棠开未开？",
	MIDDLE = "芙蓉不及佳人妆",
	SMALL = "鸾镜碎，朱颜毁",
}

STRINGS.WB_MIRROR_NOSPACE = "没有空额"
STRINGS.WB_MIRROR_NOTENOUGH = "没有足够的材料"
STRINGS.WB_MIRROR_HASONE = "已解锁过同类物品！"
STRINGS.WB_MIRROR_NEED  = "镜子等级"
STRINGS.WB_MIRROR_NEED_MAX  = "已满级!"
STRINGS.WB_MIRROR_NEED_UP = "升级需要:"
STRINGS.WB_MIRROR_CURRENT_SKIN = "当前"

STRINGS.NAMES.MK_DSF  = "定身法"
STRINGS.RECIPE_DESC.MK_DSF = "定！"
STRINGS.WHITE_BONE_BLINK = "雾隐"
STRINGS.PIGSY_TACKLE = "刚鬣横冲"


--无常
STRINGS.NAMES.HAT_COMMISSIONER_BLACK  = "无常官帽"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_COMMISSIONER_BLACK = "白天不懂夜的黑。"
STRINGS.NAMES.HAT_COMMISSIONER_WHITE  = "无常官帽"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_COMMISSIONER_WHITE = "夜晚不懂昼的白。"
STRINGS.NAMES.WHIP_COMMISSIONER  = "勾魂索"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.WHIP_COMMISSIONER = "专勾恶魂，亦惩邪魄。"
STRINGS.NAMES.TOKEN_COMMISSIONER  = "镇魂令"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.TOKEN_COMMISSIONER = {
    GENERIC = "此乃镇死物惊生人之宝物。",
	SPELL = "阴差借道！生人回避！",
    NOSOUL = "无恶，不作为。",
	NOHAT = "无纳恶之顶，不作为。",
}
STRINGS.NAMES.PENNANT_COMMISSIONER  = "招魂幡"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PENNANT_COMMISSIONER = {
    GENERIC = "招魂引魄，牵渡黄泉。",
	SPELL = "引渡吉时！魂魄速来！",
    NOSOUL = "无善，无用处。",
	NOHAT = "无纳善之冠，无用处。",
}
STRINGS.NAMES.BELL_COMMISSIONER  = "摄魂铃"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BELL_COMMISSIONER = {
    GENERIC = "此乃安死物抚生人之宝物。",
	SPELL = "阴差借道！摄魂入梦！",
    NOSOUL = "无善，无用处。",
	NOHAT = "无纳善之冠，无用处。",
}
STRINGS.NAMES.SOUL_GHAST = "恶魂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOUL_GHAST = "生前作恶下落，心怀不轨，死后如斯。"
STRINGS.NAMES.SOUL_SPECTER = "善魂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOUL_SPECTER = "生前行善积德，心怀好意，逝后这般。"
STRINGS.NAMES.GHOST_IRRITATED = "怨魂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOST_IRRITATED = "恶魂失去束缚，变得更加暴躁邪恶。"
STRINGS.NAMES.GHOST_CLEMENT = "恩魂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOST_CLEMENT = "善哉，亡魂报完恩筹便会投胎转世。"

STRINGS.YAMARECIPE = "阴司栏"

STRINGS.NAMES.MYTH_YAMA_STATUE1 = "阎罗雕像"
STRINGS.RECIPE_DESC.MYTH_YAMA_STATUE1 = "魂迢迢兮路遥遥"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE1 = "一个不知名的石头雕像"

STRINGS.NAMES.MYTH_YAMA_STATUE2 = "阎罗神像"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE2 = "些许供奉，色彩斑驳"

STRINGS.NAMES.MYTH_YAMA_STATUE3 = "阎罗神龛"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE3 = "瓦砾初起"

STRINGS.NAMES.MYTH_YAMA_STATUE4 = "阎王神龛"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_YAMA_STATUE4 = "十殿之首，狱众之王"

STRINGS.NAMES.MYTH_BAHY = "彼岸还阳"
STRINGS.RECIPE_DESC.MYTH_BAHY = "花红不可见，速速返人间"

STRINGS.NAMES.MYTH_HIGANBANA_ITEM = "彼岸花"
STRINGS.RECIPE_DESC.MYTH_HIGANBANA_ITEM = "花红彼岸，捻灰作尘"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_ITEM = "花开不见叶，相逢一千年"

STRINGS.NAMES.MYTH_HIGANBANA_REVIVE = "彼岸花"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_REVIVE = "花开不见叶，相逢一千年"

STRINGS.NAMES.MYTH_HIGANBANA_TELE = "彼岸花"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_HIGANBANA_TELE = "花开不见叶，相逢一千年"

STRINGS.NAMES.MYTH_CQF = "出窍符"
STRINGS.RECIPE_DESC.MYTH_CQF = "辅助元神出窍的符咒"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_CQF = "辅助元神出窍的符咒"

STRINGS.YAMA_COMMISSIONERS_ISRAINING = "天欲雨，不可离去"
STRINGS.YAMA_COMMISSIONERS_ISRAINING_BLACK = "天欲雨，于此等君"

STRINGS.YAMA_CANTDIGMOUNT = "盗发冢，处极刑"
STRINGS.YAMA_BLACKDEAD = "老范你又在偷懒了"
STRINGS.YAMA_WHITEDEAD = "老谢也许还需要休息一下"

STRINGS.NAMES.MYTH_BAMBOO_BASKET = "竹药篓"
STRINGS.RECIPE_DESC.MYTH_BAMBOO_BASKET = "霞光日晚，采药方还"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MYTH_BAMBOO_BASKET = "一段绵绵不绝的药香传来"
--STRINGS.CHARACTERS.MYHT_YUTU.DESCRIBE.MYTH_BAMBOO_BASKET = "我可不是来采蘑菇的"
--------------------------------------------------------------------------
--[[ 玉兔重做tip ]]
--------------------------------------------------------------------------

STRINGS.TAB_JADEHARE = "蟾宫技艺"

STRINGS.NAMES.POWDER_M_HYPNOTICHERB  = "草参药粉"
STRINGS.RECIPE_DESC.POWDER_M_HYPNOTICHERB = "大补心力，使人嗜睡。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_HYPNOTICHERB = "即使被磨成粉了我还是认得你，曼德拉草！"
STRINGS.NAMES.POWDER_M_LIFEELIXIR  = "犀茸药粉"
STRINGS.RECIPE_DESC.POWDER_M_LIFEELIXIR = "君王难逑，长生灵药。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_LIFEELIXIR = "被磨成粉后不仅大补，而且还有仁慈的魔法。"
STRINGS.NAMES.POWDER_M_CHARGED  = "惊厥药粉"
STRINGS.RECIPE_DESC.POWDER_M_CHARGED = "电光石火，惊厥逼人。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_CHARGED = "被磨成粉后，导电能力具有了挥发性。"
STRINGS.NAMES.POWDER_M_IMPROVEHEALTH  = "活血药粉"
STRINGS.RECIPE_DESC.POWDER_M_IMPROVEHEALTH = "活血化瘀，疏络心脉。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_IMPROVEHEALTH = "闻起来非常香甜，是吧。"
STRINGS.NAMES.POWDER_M_COLDEYE  = "寒眸药粉"
STRINGS.RECIPE_DESC.POWDER_M_COLDEYE = "寒邪客于敌。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_COLDEYE = "本想创造一头巨鹿，但失败了。"
STRINGS.NAMES.POWDER_M_BECOMESTAR  = "夜明药粉"
STRINGS.RECIPE_DESC.POWDER_M_BECOMESTAR = "肤如凝脂，粉光若腻。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_BECOMESTAR = "不管男孩子还是女孩子，都推荐睡前涂一涂哦。"
STRINGS.NAMES.POWDER_M_TAKEITEASY  = "排郁药粉"
STRINGS.RECIPE_DESC.POWDER_M_TAKEITEASY = "除忧解虑，安然自得。"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.POWDER_M_TAKEITEASY = "其实还可以拿来泡茶。"

STRINGS.MYTH_NOENOUGHCOIN = "我需要绳子和足量的铜币"

--ACTIONS.MYTH_USE_INVENTORY是主题mod里定义的，但本mod后于其加载
STRINGS.ACTIONS.MYTH_USE_INVENTORY.GUITAR = "弹奏"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.GUITAR_NOTE = "触摸"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.WHIP = "鬼火缠身"

STRINGS.NAMES.GUITAR_JADEHARE  = "莹月琵琶"
STRINGS.RECIPE_DESC.GUITAR_JADEHARE = "轻拢慢捻，泪珠盈睫"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GUITAR_JADEHARE = {
	GENERIC = "蟾宫幽恨，都向曲中传。",
	UNKOWN = "我还不会这首曲子。",
	NOSONG = "弹首什么曲子好呢？",
	NOGUITAR = "离美妙的音乐还差个琵琶！",
	NOCOST = "我没力气弹了。"
}
STRINGS.CHARACTERS.MYTH_YUTU.DESCRIBE.GUITAR_JADEHARE = {
	GENERIC = "这是嫦娥姐姐借给我的，羡慕吧！",
	UNKOWN = "这曲子本姑娘不会。",
	NOSONG = "我得找首曲子练练手。",
	NOGUITAR = "弦断了！...骗你的，连琵琶都还没有呢！",
	NOCOST = "没精力继续弹了。"
}
STRINGS.CANT_USEGUITAR_RIDING = "在牛背上我可没有雅兴如此"
STRINGS.CANT_USEDAOYAO_RIDING = "此等灵药当以十分尊重待之"

STRINGS.NAMES.SONG_M_WORKUP  = "《田中乐》"
STRINGS.RECIPE_DESC.SONG_M_WORKUP = "晨兴理荒秽，带月荷锄归。"
STRINGS.NAMES.SONG_M_INSOMNIA  = "《春光曲》"
STRINGS.RECIPE_DESC.SONG_M_INSOMNIA = "情人怨遥夜，竟夕起相思。"
STRINGS.NAMES.SONG_M_FIREIMMUNE  = "《浴火奏》"
STRINGS.RECIPE_DESC.SONG_M_FIREIMMUNE = "千锤万凿出深山，烈火焚烧若等闲。"
STRINGS.NAMES.SONG_M_ICEIMMUNE  = "《寒风调》"
STRINGS.RECIPE_DESC.SONG_M_ICEIMMUNE = "忽如一夜春风来，千树万树梨花开。"
STRINGS.NAMES.SONG_M_ICESHIELD  = "《梦飞霜》"
STRINGS.RECIPE_DESC.SONG_M_ICESHIELD = "冰泉冷涩弦凝绝，凝绝不通声暂歇。"

STRINGS.NAMES.SONG_M_NOCURE  = "《怨缠身》"
STRINGS.RECIPE_DESC.SONG_M_NOCURE = "红颜胜人多薄命，莫怨春风当自嗟。"
STRINGS.NAMES.SONG_M_WEAKATTACK  = "《春风化雨》"
STRINGS.RECIPE_DESC.SONG_M_WEAKATTACK = "小楼一夜听春雨，深巷明朝卖杏花。"
STRINGS.NAMES.SONG_M_WEAKDEFENSE  = "《霸王卸甲》"
STRINGS.RECIPE_DESC.SONG_M_WEAKDEFENSE = "鼓衰矢竭谁收功，将军卸甲入九重。"
STRINGS.NAMES.SONG_M_NOLOVE  = "《流水无情》"
STRINGS.RECIPE_DESC.SONG_M_NOLOVE = "潮到空城头尽白，离歌一曲怨残阳。"
STRINGS.NAMES.SONG_M_SWEETDREAM  = "《夜阑谣》"
STRINGS.RECIPE_DESC.SONG_M_SWEETDREAM = "山高地阔兮，更深夜阑兮。"

--盘丝的部分
--吊丝
STRINGS.NAMES.MADAMEWEB_FEISHENG = "盘丝吊"
STRINGS.RECIPE_DESC.MADAMEWEB_FEISHENG = "一丝虚空挂"
STRINGS.RECIPE_DESC.SILK = "我腹能得几多丝？"

STRINGS.NAMES.MADAMEWEB_NET = "蛛丝罗网"
STRINGS.RECIPE_DESC.MADAMEWEB_NET = "无处可逃"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_NET = "天罗何在？地网何织？"

STRINGS.NAMES.MADAMEWEB_SPIDERDEN = "蛛丝罗网"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDERDEN = "天罗何在？地网何织？"

STRINGS.NAMES.MADAMEWEB_PITFALL = "蛛网陷阱"
STRINGS.RECIPE_DESC.MADAMEWEB_PITFALL = "百丝杀气生 "
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL = "你要来试试吗？"

STRINGS.NAMES.MADAMEWEB_PITFALL_GROUND = "蛛网陷阱"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFF_GROUND = "你要来试试吗？"

STRINGS.NAMES.MADAMEWEB_PITFALL_TRAP = "蛛网陷阱"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL_TRAP = "里面有什么呢？"

STRINGS.NAMES.MADAMEWEB_PITFALL_COCOON = "困顿之茧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_PITFALL_COCOON = "它现在动弹不得"

STRINGS.NAMES.MADAMEWEB_STINGER = "盘丝蜂针"
STRINGS.RECIPE_DESC.MADAMEWEB_STINGER = "丝悬针尾"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_STINGER = "蜂针缠丝穿心过"

STRINGS.NAMES.MADAMEWEB_POISONSTINGER = "盘丝毒针"
STRINGS.RECIPE_DESC.MADAMEWEB_POISONSTINGER = "毒缠针头"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_POISONSTINGER = "毒沁针头索命回"

STRINGS.NAMES.MADAMEWEB_BEEMINE = "毒蜂茧"
STRINGS.RECIPE_DESC.MADAMEWEB_BEEMINE = "先去里面呆着"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_BEEMINE = "让我看看里面的宝贝"

STRINGS.NAMES.MADAMEWEB_BEE = "盘丝毒蜂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_BEE = "沾染毒气的蜜蜂"

STRINGS.NAMES.MADAMEWEB_POISONBEEMINE = "盘丝蛛卵"
STRINGS.RECIPE_DESC.MADAMEWEB_POISONBEEMINE = "应时而动，适时而谋"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_POISONBEEMINE = "还不是孵化的时机"

STRINGS.NAMES.MADAMEWEB_SPIDERHOME = "盘丝蛛巢"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDERHOME = "孵化毒蛛的温床"

STRINGS.NAMES.MADAMEWEB_SPIDER_WARRIOR = "盘丝毒蛛"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SPIDER_WARRIOR = "这些可不是好惹的"

STRINGS.NAMES.MADAMEWEB_DETOXIFY = "解毒散"
STRINGS.RECIPE_DESC.MADAMEWEB_DETOXIFY = "以毒攻毒"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_DETOXIFY = "清热解毒的良药"

STRINGS.NAMES.MADAMEWEB_ARMOR = "盘丝披肩"
STRINGS.RECIPE_DESC.MADAMEWEB_ARMOR = "也许可以"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_ARMOR = "肩上霓裳美若珠"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON = "蛛丝茧袋"
STRINGS.RECIPE_DESC.MADAMEWEB_SILKCOCOON = "绵绵絮絮，长存其中"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON = "要来猜猜是什么吗？"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON_PACKED = "盘丝织袋"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON_PACKED = "且待明日，适时二开"

STRINGS.NAMES.MADAMEWEB_SILKCOCOON_HANGED = "盘丝悬茧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MADAMEWEB_SILKCOCOON_HANGED = "悬空的蛛丝所结，不知藏有何物"

STRINGS.NAMES.MADAMEWEB_SILKVALUELOW = "再等等，妾身还需要点时间"

STRINGS.NAMES.HUA_INTERNET_NODE = "盘丝标点"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE = "特制的节点，可以承载有韧性的蛛丝"
STRINGS.NAMES.HUA_INTERNET_NODE_SEA = "海丝标点"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_SEA = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_INTERNET_LINK = "蛛丝"

STRINGS.NAMES.HUA_INTERNET_NODE_ITEM = "盘丝滑索"
STRINGS.RECIPE_DESC.HUA_INTERNET_NODE_ITEM = "编织你的蛛网吧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_ITEM = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_INTERNET_NODE_SEA_ITEM = "海丝滑索"
STRINGS.RECIPE_DESC.HUA_INTERNET_NODE_SEA_ITEM = "编织你的蛛网吧"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_INTERNET_NODE_SEA_ITEM = "特制的节点，可以承载有韧性的蛛丝"

STRINGS.NAMES.HUA_FAKE_SPIDER_SHOE = "步丝履"
STRINGS.RECIPE_DESC.HUA_FAKE_SPIDER_SHOE = "体验蜘蛛的感觉"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HUA_FAKE_SPIDER_SHOE = "等等,这地有点滑"

STRINGS.HUA_INTERNET_BUILDER = {
    FAILED_DIST = "距离太远，无法建造",
    FAILED_EXIST = "已有连线，无法建造",
    FAILED_PREFAB = "缺少 prefab：",
    FAILED_MATERIAL = "材料不足，无法建造",
}
STRINGS.HUA_INTERNET_USE = {
    FAILED_NO_LINK = "附近没有直达目的地的连线",
}

STRINGS.NAMES.MADAMEWEB_CANTSILKFLY = "无处可悬"
STRINGS.ACTIONS.DEPLOY.SILKCOCOON = "悬挂"
STRINGS.ACTIONS.MYTH_USE_INVENTORY.SPIDERDEN = "降级"
STRINGS.MYTH_FEIWEN_STR = "薄丝飞吻"
STRINGS.MYTH_SILKFLY_STR = "落地"
STRINGS.NAMES.MYTHTUMBLE = "摔落"
STRINGS.MADABIANHUAN = "布网"
STRINGS.MURDERED_BY_MADAMEWEB = "吞噬"
STRINGS.MYTH_SILKFLY_DOWN = "盘丝坠"
STRINGS.MYTH_INTERNET_RIGHT = "滑行"
STRINGS.MYTH_INTERNET_LEFT = "连线"

STRINGS.MYTH_SKILL_HY = "火眼金睛"
STRINGS.MYTH_SKILL_YZQT = "一柱擎天"
STRINGS.MYTH_SKILL_WBBS ="美人画皮"
STRINGS.MYTH_SKILL_PIGBS ="刚鬣本相"
STRINGS.MYTH_SKILL_HUAYING ="展翅化鹰"
STRINGS.MYTH_SKILL_TIANYAN ="天眼"
STRINGS.MYTH_SKILL_YMBS ="黑白双生"
STRINGS.MYTH_SKILL_MABS ="蛛皇本相"
STRINGS.MYTH_SKILL_PIPA ="演奏"

local speech = {
    --猴子的
    monkey_king = {
        HEAT_RESISTANT_PILL = "あのくさサイはあくどい烈日と滔天の火炎を抵抗するとおりにするんだ！", --避火丹
        COLD_RESISTANT_PILL = "あのくさサイは強い寒風とどしゃ降りを抵抗するとおりにするんだ！", --避寒丹
        DUST_RESISTANT_PILL = "あのくさサイは黄風飛砂と粉塵毒霧を抵抗するとおりにするんだ！", --避尘丹
        FLY_PILL = "長歌であわただしいを蔑視して、これは最も自在で、最も豪気万丈だな！", --腾云
        BLOODTHIRSTY_PILL = "方式を換えて妖精のように狂おう！", --嗜血
        CONDENSED_PILL = "うん、この眠気覚ましの味！", --凝神
        THORNS_PILL = "イーヒヒヒ、俺と比べて、誰かより刺せるか？", --荆棘
        ARMOR_PILL = "俺は銅のような頭、鉄のような腕、鋼のような腱、鉄のような骨であるんだ！", --壮骨
        DETOXIC_PILL = "百毒無用であるんだ！", --化毒
        PEACH = "これは蟠桃園からとった仙桃だ！", --桃子
        BIGPEACH = "なんと大きな蟠桃が俺のものだ！", --大桃子
        PEACH_COOKED = "焼いただけど、もうその仙境の匂いがしあないんだ！", --烤桃子
        PEACH_BANQUET = "へん、あの時、王母の蟠桃大会は俺を招かないことの結果を考えてみよ！", --蟠桃大会
        PEACH_WINE = "これは神仙素酒だ！破戒はないぞ！", --蟠桃素酒
        MK_JGB = "「如意金箍棒」は，一万三千五百斤。", --金箍棒
        MK_JGB_PILLAR = "棒の名は「如意」、俺の心を従う。", --定海神针
        HONEY_PIE = "西行の乾燥食品として便利だな。", --蜂蜜素饼
        VEGETARIAN_FOOD = "すばらしい精進料理ですね。", --素斋
        CASSOCK = "阿弥陀仏~", --袈裟
        KAM_LAN_CASSOCK = "観音が師匠に授けた宝物だ！", --锦襕袈裟
        GOLDEN_HAT_MK = "五百年たっだ。。。", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "まだ誰が俺に思い切って邪魔したいか？", --大圣战甲
        MK_BATTLE_FLAG = "へへ！美しい戦旗だな、また「齐天大圣」と書く上げべきだ！", --旗子
        MK_BATTLE_FLAG_ITEM = "へへ！美しい戦旗だな、また「齐天大圣」と書く上げべきだ！", --旗子
        XZHAT_MK = "俺はもう菩薩にだまされません。", --行者帽
        NZ_LANCE = "この槍は俺の棒にはかなわない。", --火尖枪
        NZ_DAMASK = "柔をもって剛を制する。", --混天绫
        NZ_RING = "このガキはただこの輪はうまく保管できない。", --乾坤圈
        PIGSY_RAKE = "「八戒」の耕作地のための熊手だ", --九齿钉耙
        PIGSY_HAT = "これを着るは愚かに見えただ。", -- 墨兰帽
        GOURD = "神様はこの種の作物が好きだ。", --葫芦
        GOURD_COOKED = "うん、ちょっと面白い匂いだな。", --烤葫芦
        GOURD_SOUP = "このスープはいいね！", --葫芦汤
        GOURD_OMELET = "鳥の卵のにおいがいる。", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "いい酒、いい瓢箪だな！", --酒葫芦
        PILL_BOTTLE_GOURD = "老君の丹薬はピーナッツのような味をしている。", --丹药葫芦
        BANANAFAN = "よく借りて、よく返す~", --芭蕉扇
        LAOZI_SP = "「老君」がいる？", --急急如律令
        LAOZI = "へへ、「老君」！お久しぶりだな~", --老子
        WB_HEART = "妖魔の心！！", --阴森之心
        BONE_WAND = "亡者は眠れない！", --骨杖
        BONE_BLADE = "卑劣な刃物！", --骨刃
        BONE_WHIP = "卑劣な妖精は筋肉を抜いて骨を抜く！", --骨鞭
        BONE_PET = "一撃でバラバラにさせる！", --小骷髅
        LOTUS_FLOWER = "花果山にも蓮池があります。",
         --莲花
        LOTUS_FLOWER_COOKED = "生のほうが甘いのが好きです。", -- 烤莲花
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "焼き，焼き，火を避けるために巽宮に入る！",
            EMPTY = "へ、「老君」の「炼丹炉」とほとんど同じだ！",
            DONE = "へへ！中でこのような三つ災難を経験したのはむだではない！"
        },
        YJ_SPEAR = "その年は思う存分がないんだ！あと三百回で戦お！",
         --三尖两刃刀
        YANGJIAN_HAIR = "チェ、俺の「紫金冠」の「花翎」ほど美しくない！",
         --三山飞凤冠
        YANGJIAN_ARMOR = "チェ、おれの三割威風には及びませんよ！",
         --锁子清源甲
        SKYHOUND = "俺のふくらはぎをかむな。",
         --哮天犬
        SKYHOUND_WARG = "石を一重に羽織っただけだ、私は棒で一击、叩き潰すことができます。", --月石哮天犬
        MONKEY_KING = "ダイ！この「六耳猕猴」！", --猴子
        MK_PHANTOM = "へへ、俺は「孙行者」，君は「者行孙」。",
        PIGSY = "くそボケ、まだ起きない？早く働け！", -- 猪猪
        NEZA = "「哪吒」兄弟は今日数杯を小酌に来ましょう。", --那咋
        WHITE_BONE = "何度も変化して俺の師匠をからかって、この一撃をくらう！", --白骨
        YANGJIAN = "真君はどうして今日暇があって、下界に下ってここに来ますか？", --杨戬
        MYTH_INTERIORS_LIGHT = "何年も経っているんだ、この灯油はまだ使い果たしていない。",
        MYTH_INTERIORS_BED = "先生は今どこで休んでいますか？",
        MYTH_INTERIORS_GZ = "先生は絵を塗るのが好きです。",
        MYTH_INTERIORS_PF = "先生はこのいくつかの植物についてよく好きです。",
        MYTH_INTERIORS_XL = "先生の代わりに、この香炉を新たに焚き直しましょう。",
        MYTH_INTERIORS_ZZ = "先生が前に字の読み書きを教えてくれました。",
        MYTH_FOOD_ZPD = "ヘッ！ぼけに食わせ！",
        MYTH_FOOD_NX = "よい果実といい乳といい味！",
        MYTH_FOOD_LXQ = "俺はやはり果物が好きだ。",
        MYTH_FOOD_FHY = "老龍王が俺を招待したごちそうほどよくありません。",
        MYTH_FOOD_HYMZ = "柔らかいものの上に形を作る、彫刻の技術はとてもいいです。",
        MYTH_BANANA_LEAF = "スカートを編ませて。",
        MYTH_BANANA_TREE = "緑の葉の覆いの間に果物が隠れているようです。",
        MYTH_ZONGZI_ITEM1 = "やはりこの甘いちまきはもちもちして美味しいです。",
        MYTH_ZONGZI_ITEM2 = "ベーコンを入れましたが、美味しくない！",
        BANANAFAN_BIG = "兄嫁が当時貸してくれたらしいです。"
    },
    --八戒的
    pigsy = {
        HEAT_RESISTANT_PILL = "冷たいのは胸の上に置くのがちょうどいいです。", --避火丹
        COLD_RESISTANT_PILL = "あの「弼马温」に見かけさせることはやめて。", --避寒丹
        DUST_RESISTANT_PILL = "へへ、洗濯しなくてもいいんだ。", --避尘丹
        FLY_PILL = "綿雲も雲に乗る術です。", --腾云
        BLOODTHIRSTY_PILL = "この丹は身を傷つけます。", --嗜血
        CONDENSED_PILL = "精神を集中してしっかりと打つ。", --凝神
        THORNS_PILL = "いばらのよろいを身にまとう。", --荆棘
        ARMOR_PILL = "リウマチは老君牌で作った壮骨丹だけを使う。", --壮骨
        DETOXIC_PILL = "百毒無用であった！", --化毒
        PEACH = "一つ嘗めさせて", --桃子
        BIGPEACH = "これは蟠桃宴の上の圧巻のだ！", --大桃子
        PEACH_COOKED = "仙境の匂いが少しうしなった。", --烤桃子
        PEACH_BANQUET = "あの猿はこれだけのために天宮をひっくり返した。", --蟠桃大会
        PEACH_WINE = "これは神仙素酒だ！破戒はないぞ！", --蟠桃素酒
        MK_JGB = "この棒には打ちられたくない。", --金箍棒
        MK_JGB_PILLAR = "お兄さん。この「定海神珍」は本当に威光だな！威光！", --定海神针
        HONEY_PIE = "このお餅は甘くて、食べたくてたまらないんだ！", --蜂蜜素饼
        VEGETARIAN_FOOD = "もっと来て！お腹が空いて倒れそうだよ！", --素斋
        CASSOCK = "袈裟だけだ。", --袈裟
        KAM_LAN_CASSOCK = "これは師匠の宝物の袈裟じゃないか？", --锦襕袈裟
        GOLDEN_HAT_MK = "「花翎」が飛んでいて、威風だな！", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "この戦甲は昔のことをたくさん思い出させました。", --大圣战甲
        MK_BATTLE_FLAG = "威風堂々!", --旗子
        MK_BATTLE_FLAG_ITEM = "威風堂々!", --旗子
        XZHAT_MK = "猿のこの帽子は俺の美しさには及ばないよ。", --行者帽
        NZ_LANCE = "威風堂々の槍だな。", --火尖枪
        NZ_DAMASK = "女の子たちの絹織物よりも柔らかいです。", --混天绫
        NZ_RING = "あら、俺を命中しないで。", --乾坤圈
        PIGSY_RAKE = "君子は礼儀正しく，位でなければつり合わない。", --九齿钉耙
        PIGSY_HAT = "風邪を引いたくない。", -- 墨兰帽
        GOURD = "千切りにして軽く炒め，ˉ﹃ˉ", --葫芦
        GOURD_COOKED = "ちょっと焦げ香りです。", --烤葫芦
        GOURD_SOUP = "疲れをとった~", --葫芦汤
        GOURD_OMELET = "一口でたくさん食べられる！", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "破戒できない...一口だけ飲む。", --酒葫芦
        PILL_BOTTLE_GOURD = "「老君」のヒョウタン？彼のために保管しておきます。", --丹药葫芦
        BANANAFAN = "夏に寝ている時に吹いたら、玉帝よりもっと快適だな！", --芭蕉扇
        LAOZI_SP = "「老君」がいる？", --急急如律令
        LAOZI = "「老君」は今日、何でここにいる？", --老子
        WB_HEART = "邪悪の道", --阴森之心
        BONE_WAND = "邪悪の道", --骨杖
        BONE_BLADE = "邪悪の道！", --骨刃
        BONE_WHIP = "邪悪の道！", --骨鞭
        BONE_PET = "邪悪の道！", --小骷髅
        LOTUS_FLOWER = "中には蓮の花托があるよね？",
         --莲花
        LOTUS_FLOWER_COOKED = "うまい！もう一つお願いします！", -- 烤莲花
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "方寸乾坤、日月斗転"
        },
        YJ_SPEAR = "二郎神のこの兵器は、俺の熊手より六つの歯が欠けているぞ。",
         --三尖两刃刀
        YANGJIAN_HAIR = "真君のこの冠は本当に凝っているな。",
         --三山飞凤冠
        YANGJIAN_ARMOR = "この甲冑は小さすぎて、俺の壮大な体型はとても着られません。",
         --锁子清源甲
        SKYHOUND = "犬の肉の煮込み~いや！出家者が戒めを破るものか。",
         --哮天犬
        SKYHOUND_WARG = "あ！どうして俺より大きいですか？肉もざらざらになりました。", --月石哮天犬
        MONKEY_KING = "どこに行ってもこの嫌な弼馬温に会えます！", --猴子
        MK_PHANTOM = "どこに行ってもこの嫌な弼馬温に会えます！",
        PIGSY = "鏡ですか？", -- 猪猪
        NEZA = "三若様はどこから来ましたか？", --那咋
        WHITE_BONE = "女の菩薩…違う！妖怪だ！", --白骨
        YANGJIAN = "「真君」はどこに行きますか？", --杨戬
        MYTH_INTERIORS_LIGHT = "俺も灯油の味を食べてみたい。",
        MYTH_INTERIORS_BED = "このベッドもあまりにも小さいだ。",
        MYTH_INTERIORS_GZ = "何のいいものも入っていない古い空き缶。",
        MYTH_INTERIORS_PF = "俺はおぼろの美しさが大好きです。",
        MYTH_INTERIORS_XL = "俺は普段に焼香しません。",
        MYTH_INTERIORS_ZZ = "これ何の字ですか？俺は分からん。",
        MYTH_FOOD_ZPD = "これはずいぶん時間がかかって作りました。",
        MYTH_FOOD_NX = "一口でなくなった。",
        MYTH_FOOD_LXQ = "こんなの小海老は歯のすき間に詰めことも足りません。",
        MYTH_FOOD_FHY = "やっと飽食できました。",
        MYTH_FOOD_HYMZ = "ちょっと食べさせてみて。少しだけ、お願いんだ。",
        MYTH_BANANA_LEAF = "これはあまり美味しそうではないですが、蒸しご飯は十分な香りがするはずです。",
        MYTH_BANANA_TREE = "果物がどこにあるか見てみましょう。",
        MYTH_ZONGZI_ITEM1 = "俺は一回に30個食べられる。",
        MYTH_ZONGZI_ITEM2 = "なんと肉がその中にあるとは，师傅に見えさせないんだ！",
        BANANAFAN_BIG = "風が吹いて、夏はもっと快適に寝られます。"
    },
    --娜扎

    neza = {
        HEAT_RESISTANT_PILL = "水辺まで歩く力を省きました。", --避火丹
        COLD_RESISTANT_PILL = "これはいいものだぞ！", --避寒丹
        DUST_RESISTANT_PILL = "ほこりが土に返る。", --避尘丹
        FLY_PILL = "チェ、「哪吒」にも雲霧に乗る宝物があります。", --腾云
        BLOODTHIRSTY_PILL = "邪物のようなもの，誰が精錬したものか？", --嗜血
        CONDENSED_PILL = "...「哪吒」は生まれつきの神力で、この丹を必要としません。", --凝神
        THORNS_PILL = "「哪吒」は蓮です。バラじゃない！", --荆棘
        ARMOR_PILL = "今の「哪吒」は脆いものではないよ。", --壮骨
        DETOXIC_PILL = "", --化毒
        PEACH = "クソ猿、この味はどうですか？", --桃子
        BIGPEACH = "良い桃！きっと食い意地が悪くて、あの猿が。", --大桃子
        PEACH_COOKED = "粗末にするだな。", --烤桃子
        PEACH_BANQUET = "師匠にはその資格があるだ。", --蟠桃大会
        PEACH_WINE = "「哪吒」はまだ小さいので、お酒を飲んではいけません。", --蟠桃素酒
        MK_JGB = "あの龍王は生きざまだ。", --金箍棒 123
        MK_JGB_PILLAR = "この「定海神珍」がなくなったら、海に沸騰させるほうがおっとりしているだ！", --定海神针
        HONEY_PIE = "美味しい通行料だ。", --蜂蜜素饼 123
        VEGETARIAN_FOOD = "たとえ気に入らなくても捨てることもいけません。", --素斋
        CASSOCK = "師匠は道士です。", --袈裟
        KAM_LAN_CASSOCK = "..立派だな！", --锦襕袈裟
        GOLDEN_HAT_MK = "精巧で美しい。", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "「哪吒」もこんなの宝物が欲しいです。", --大圣战甲
        MK_BATTLE_FLAG = "威風だな!", --旗子
        MK_BATTLE_FLAG_ITEM = "威風だな。", --旗子
        XZHAT_MK = "母も「哪吒」に帽子を作ることがある。", --行者帽
        NZ_LANCE = "この世には耐えられる人が少ない。", --火尖枪
        NZ_DAMASK = "巻きつくのものはなんだ？運命だ。", --混天绫
        NZ_RING = "まだ何があるんだ、全て来いよ！", --乾坤圈
        PIGSY_RAKE = "これは玉帝の賜物ではない。", --九齿钉耙
        PIGSY_HAT = "小さい帽子は主人の体形に合わない。", -- 墨兰帽
        GOURD = "小さなヒョウタン、大きな腹。", --葫芦
        GOURD_COOKED = "コークスの香り。", --烤葫芦
        GOURD_SOUP = "もう一杯！", --葫芦汤
        GOURD_OMELET = "これは...母が作ったお餅のようです。", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "一本の清酒で、一回の夢。", --酒葫芦
        PILL_BOTTLE_GOURD = "丹を入れるためのヒョウタンです。", --丹药葫芦
        BANANAFAN = "この扇子は普通ではない。", --芭蕉扇
        LAOZI_SP = "老君を祭る符です。", --急急如律令
        LAOZI = "祖師に謁見します。", --老子
        WB_HEART = "妖魔の心！！", --阴森之心
        BONE_WAND = "亡者は眠れない！", --骨杖
        BONE_BLADE = "卑劣な刃物", --骨刃
        BONE_WHIP = "卑劣な妖精は筋肉を抜いて骨を抜く！", --骨鞭
        BONE_PET = "ウェ、死んでまだ間もない。", --小骷髅
        LOTUS_FLOWER = "蓮花は化身を現わし、霊珠は俗塵を出す。",
         --莲花 123
        LOTUS_FLOWER_COOKED = "「哪吒」の尊厳はまた冒されたような気がします。", -- 烤莲花 那咋也爱吃烤莲花？
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "師匠はこのストーブをよく使います。",
            EMPTY = "行こう、あの猿が捕まえてきて、練ってくれます。",
            DONE = "クソ猿はもう青煙と化した。"
        },
        YJ_SPEAR = "神威天をおおい，風雷来ようとしている。",
         --三尖两刃刀
        YANGJIAN_HAIR = "兄弟子がおさげをして天を突く。",
         --三山飞凤冠
        YANGJIAN_ARMOR = "楊さんのこの銀甲はとても立派だな。「哪吒」は羨ましいな。",
         --锁子清源甲
        SKYHOUND = "「啸天」、「啸天」、「哪吒」を抱っこさせて！",
         --哮天犬
        SKYHOUND_WARG = "こんなの「哮天」は「哪吒」を抱けなくなった。", --月石哮天犬
        MONKEY_KING = "「大圣」はその後お変わりありませんか。", --猴子
        MK_PHANTOM = "「大圣」はその後お変わりありませんか。",
        PIGSY = "「天蓬元帅」はどうしてあなたの師匠を守って西天に行ってお経を取りに行きませんか？", -- 猪猪
        NEZA = "どうして「哪吒」が二ついますか？", --那咋
        WHITE_BONE = "不吉な魔物，この一撃をくらう！", --白骨
        YANGJIAN = "兄弟子は何でここにいますか？", --杨戬 123
        MYTH_INTERIORS_LIGHT = "「哪吒」のまくらみたい。",
        MYTH_INTERIORS_BED = "「哪吒」をちょっと休ませて。",
        MYTH_INTERIORS_GZ = "瓶に絵が入っています。",
        MYTH_INTERIORS_PF = "上の絵は本当に凝っています。",
        MYTH_INTERIORS_XL = "香を燃やして座禅を組むと、「哪吒」はより禅定に入りやすいです。",
        MYTH_INTERIORS_ZZ = "「哪吒」を見てまた何を学ぶことができますか。",
        MYTH_FOOD_ZPD = "ウェ，これを「哪吒」の口の中に入れるな。",
        MYTH_FOOD_NX = "「哪吒」はもう一杯お願いします。",
        MYTH_FOOD_LXQ = "東海のロブスターは、「哪吒」に全部食べましょう。",
        MYTH_FOOD_FHY = "当時の様子のようだな。",
        MYTH_FOOD_HYMZ = "とても可愛いウサギだ。「哪吒」が飼ったあのウサギに似ています。",
        MYTH_BANANA_LEAF = "蓮葉より上の方が幾らか大きい。",
        MYTH_BANANA_TREE = "俺もバナナが好きだ。",
        MYTH_ZONGZI_ITEM1 = "母の料理を思い出しました。",
        MYTH_ZONGZI_ITEM2 = "咸粽もなかなか特別な味があります。",
        BANANAFAN_BIG = "六丁神火で練って、八掛炉の中に出てくる。"
    },
    --白骨的
    white_bone = {
        HEAT_RESISTANT_PILL = "骨だけだから，寒暑に恐れるものか。", --避火丹
        COLD_RESISTANT_PILL = "骨だけだから，寒暑に恐れるものか。", --避寒丹
        DUST_RESISTANT_PILL = "尘は尘に帰り、土は土に帰り。輪廻転生ができないことが誰が私のために決めてくれますか？", --避尘丹
        FLY_PILL = "飛翔の術，手口だけだ。", --腾云
        BLOODTHIRSTY_PILL = "あなたの血を喜んで味わいます。", --嗜血
        CONDENSED_PILL = "新しい獲物を狩る。", --凝神
        THORNS_PILL = "いばらのよろい，", --荆棘
        ARMOR_PILL = "わたくしの筋骨をしっかりと補う。", --壮骨
        DETOXIC_PILL = "骨の毒を払う。", --化毒
        PEACH = "ほんのわずかな仙気だけで，惜しむに足りない。", --桃子
        BIGPEACH = "たとえ十が一に足りなくても、私の力は大いに増える！", --大桃子
        PEACH_COOKED = "霊妙な気質をすっかり失った。", --烤桃子
        PEACH_BANQUET = "これはわたくしのような顔のない妖精には資格がないぞ。", --蟠桃大会
        PEACH_WINE = "歌声が休んで，玉の杯が空いて，酒が興が尽きた。", --蟠桃素酒
        MK_JGB = "わたしくを打たないて。", --金箍棒
        MK_JGB_PILLAR = "威風堂々な如意棒だな。", --定海神针
        HONEY_PIE = "持ち行いて、わたくしがこれを食べたくない。", --蜂蜜素饼
        VEGETARIAN_FOOD = "私がガマで変えたのあれですか？", --素斋
        CASSOCK = "坊さんのおんぼろ。", --袈裟
        KAM_LAN_CASSOCK = "これは一般の袈裟ではない。", --锦襕袈裟
        GOLDEN_HAT_MK = "威風堂々。", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "宝の気が満ちあふれている。", --大圣战甲
        MK_BATTLE_FLAG = "裂く音はきっといいですわ。", --旗子
        MK_BATTLE_FLAG_ITEM = "引っ越して帰って私の洞窟の中に置いている。", --旗子
        XZHAT_MK = "あ~これはあの「紧箍咒」を缔めた帽子だ。", --行者帽
        NZ_LANCE = "鋭利な刃。", --火尖枪
        NZ_DAMASK = "三公子はどうやって女の子の家の腹巻をかぶっていますか？ハハハ～", --混天绫
        NZ_RING = "乾坤一擲、妖龍を滅ぼす。", --乾坤圈
        PIGSY_RAKE = "礼器なのに殺し物になった。", --九齿钉耙
        PIGSY_HAT = "ただの俗物だけだ。", -- 墨兰帽
        GOURD = "俗物。", --葫芦
        GOURD_COOKED = "俗物。", --烤葫芦
        GOURD_SOUP = "俗物。", --葫芦汤
        GOURD_OMELET = "すごい度胸ですね，こんなものばかりで私を敷衍している！", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "風や雨がひどくなるのを恐れず，酒の残りを煮る，この風雨の後の花を見ている。", --酒葫芦
        PILL_BOTTLE_GOURD = "仙丹の一粒は百年の修行に勝る。", --丹药葫芦
        BANANAFAN = "良い宝物だな！", --芭蕉扇
        LAOZI_SP = "ふん！これで私を封印するつもりですか？", --急急如律令
        LAOZI = "ヤバい、速く逃げろ！", --老子
        WB_HEART = "紫が絵の皮の唇をドット染めています。", --阴森之心
        BONE_WAND = "法杖は不思議で、像紋が彫られています。竜型の瑠璃が入っています。犀文玉で取っ手を飾ります。", --骨杖
        BONE_BLADE = "汝の命を奪い、我が身を養う。", --骨刃
        BONE_WHIP = "鞭は四方の野から風雲が起こり，敵を攻撃して完膚なきまでに血が滴らした。", --骨鞭
        BONE_PET = "私に従お！", --小骷髅
        LOTUS_FLOWER = "花は美しい人の装いのようで、葉は青々と酔う霖裳。",
         --莲花
        LOTUS_FLOWER_COOKED = "わたくしに血と肉をくれてはどう？", -- 烤莲花
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "強い真火だ！速く逃げろ！"
        },
        YJ_SPEAR = "妖斬魔除のトライデント、降竜伏虎の両刃刀！",
         --三尖两刃刀
        YANGJIAN_HAIR = "この神将の冠を飾ってなおハンサムだな。",
         --三山飞凤冠
        YANGJIAN_ARMOR = "清寒の至りで，むやみに近寄る勇気がない。",
         --锁子清源甲
        SKYHOUND = "これは二郎神の哮天犬ではないですか？気をつけなければならないだな。",
         --哮天犬
        SKYHOUND_WARG = "この神犬はよくも月霊までも飲み込むこともできたものだ。", --月石哮天犬
        MONKEY_KING = "駆逐される味はどうでしょうか？", --猴子
        MK_PHANTOM = "駆逐される味はどうでしょうか？",
        PIGSY = "精進料理を一杯が欲しいですか？", -- 猪猪
        NEZA = "三若様に出会えて、本当に幸せです。", --那咋
        WHITE_BONE = "また一体の千年の白骨ですか？", --白骨
        YANGJIAN = "「二郎神」！なかなか勝てないだ、見つけられないてほうがいい。", --杨戬
        MYTH_INTERIORS_LIGHT = "あ～法力のあるそうなランプが一つあります。",
        MYTH_INTERIORS_BED = "ごつごつと重なった白い骨に横になって休ませてほどではない。",
        MYTH_INTERIORS_GZ = "ただ山水画だ。",
        MYTH_INTERIORS_PF = "いくつかの屏風がほのかにかすんでいる。",
        MYTH_INTERIORS_XL = "私の以前使った香料は全部人血で精製されています。",
        MYTH_INTERIORS_ZZ = "これはきっと仙人が残した遺稿だ。",
        MYTH_FOOD_ZPD = "この世の珍品だ，まるでエジアオのようだ。",
        MYTH_FOOD_NX = "子供にあげるお菓子ですか？",
        MYTH_FOOD_LXQ = "生食よりも面白いだな。",
        MYTH_FOOD_FHY = "世の中いろいろなものがみな宴会を楽しむ。",
        MYTH_FOOD_HYMZ = "月の光も花の香りも酒と一緒にすることができます。",
        MYTH_BANANA_LEAF = "翠葉が玉のようだ。",
        MYTH_BANANA_TREE = "秋風が吹いても青に染まる。",
        MYTH_ZONGZI_ITEM1 = "生臭物が付着していない、何の美味しいがある？",
        MYTH_ZONGZI_ITEM2 = "まだ凡俗のレベルを超えていないです。少しの肉の粉だけです。",
        BANANAFAN_BIG = "法力が高い宝の扇ですね。"
    },
    --杨戬
    yangjian = {
        HEAT_RESISTANT_PILL = "神の体はどうして寒塵や火を恐れることがあろうか？", --避火丹
        COLD_RESISTANT_PILL = "神の体はどうして寒塵や火を恐れることがあろうか？", --避寒丹
        DUST_RESISTANT_PILL = "神の体はどうして寒塵や火を恐れることがあろうか？", --避尘丹
        FLY_PILL = "八方仙雲、私に飛ばせように上げましょう！", --腾云
        BLOODTHIRSTY_PILL = "血を味わうことではありませんでした。", --嗜血
        CONDENSED_PILL = "千分の戦意、百般の神通！", --凝神
        THORNS_PILL = "いばらが身を着る、刃が手にいる。", --荆棘
        ARMOR_PILL = "骨が折れて体が折れても敗れることと言わない。", --壮骨
        DETOXIC_PILL = "", --化毒
        PEACH = "珍しいことではないです。", --桃子
        BIGPEACH = "これは珍しいですね。", --大桃子
        PEACH_COOKED = "台無しになった。", --烤桃子
        PEACH_BANQUET = "王母は僕を招待することが少なくないよ。", --蟠桃大会
        PEACH_WINE = "さっぱりしていて、目が覚める。", --蟠桃素酒
        MK_JGB = "あのクソ猿はきっとここにいます。", --金箍棒
        MK_JGB_PILLAR = "宝物はここにいる、あのクソ猿はきっとこの近くにいます。", --定海神针
        HONEY_PIE = "乾燥食品だけだ。", --蜂蜜素饼
        VEGETARIAN_FOOD = "一味違っている。", --素斋
        CASSOCK = "仏門の物。", --袈裟
        KAM_LAN_CASSOCK = "これは「金蝉子」の法具ではないでしょうか？", --锦襕袈裟
        GOLDEN_HAT_MK = "あのクソ猿の「束髪冠」。", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "肝っ玉が天と並ぶ？可笑しい。", --大圣战甲
        MK_BATTLE_FLAG = "わが軍の士気を高めることができます。", --旗子
        MK_BATTLE_FLAG_ITEM = "干戈を手にしたり、犀皮甲を身につけたり、戦車が交錯したり、刀と剣で斬り合う。", --旗子
        XZHAT_MK = "粗野、なくしましょう。", --行者帽
        NZ_LANCE = "これは「三尖刀」と比べものにできる。", --火尖枪
        NZ_DAMASK = "護身の宝物。", --混天绫
        NZ_RING = "「老君」の「金刚镯」と比べたらどうですか。", --乾坤圈
        PIGSY_RAKE = "君子は礼儀正しく，位でなければつり合わない。", --九齿钉耙
        PIGSY_HAT = "人間界の俗物", -- 墨兰帽
        GOURD = "藤蔓が秋の色を散らして描いて、農地の清気は服の長衣を染めます。", --葫芦
        GOURD_COOKED = "腹を満たすもの。", --烤葫芦
        GOURD_SOUP = "あまりきれいではないが、喉の渇きをいやす。", --葫芦汤
        GOURD_OMELET = "腕がいいね。", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "早くお酒を飲んで、止めないで！", --酒葫芦
        PILL_BOTTLE_GOURD = "後で老君に持って帰ります。", --丹药葫芦
        BANANAFAN = "これをどうして残しましたか？", --芭蕉扇
        LAOZI_SP = "「老君赦令」！", --急急如律令
        LAOZI = "祖師に謁見します。", --老子
        WB_HEART = "決して邪道を行ってはいけません。", --阴森之心
        BONE_WAND = "妖俗物。", --骨杖
        BONE_BLADE = "妖俗物。", --骨刃
        BONE_WHIP = "妖俗物。", --骨鞭
        BONE_PET = "妖俗物。", --小骷髅
        LOTUS_FLOWER = "世の中の人たちは花と葉の言い方が違って、きれいな金の鉢に花を植えるが、花の葉にかかわらず土の中に落ちて埃になる。",
         --莲花
        LOTUS_FLOWER_COOKED = "美味しいおかずだな。", -- 烤莲花
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "再び丹炉を燃やし，晩年の煙をロックする。"
        },
        YJ_SPEAR = "長駆してまっすぐに入って、妖怪を殺して悪魔を除く！\n風雷は一斉に動きます。悪魔の妖怪のは大胆に私の神威を犯します？",
         --三尖两刃刀
        YANGJIAN_HAIR = "銀冠を髪に束ね，堂々たる風采を示す。",
         --三山飞凤冠
        YANGJIAN_ARMOR = "月の影は澄んでいて，戦かつすれば狂暴するほど。",
         --锁子清源甲
        SKYHOUND = "遊びに行ったら気をつけてください。",
         --哮天犬
        SKYHOUND_WARG = "「哮天」！私がいない間に、食べ物をむやみに食べます。", --月石哮天犬
        SKYHOUND_TOOTH = "「哮天」、私はきっとあなたを長く眠らせません！",
         --天犬幼齿
        MONKEY_KING = "もう300ラウンド！", --猴子
        MK_PHANTOM = "もう300ラウンド！",
        PIGSY = "「天蓬元帅」、どうしてあなたの師匠を守って西天に行ってお経を取りに行きませんか？", -- 猪猪
        NEZA = "「哪吒」兄弟はここで何をしますか？", --那咋
        WHITE_BONE = "不吉な魔物は何処に隠れたいですか！", --白骨
        YANGJIAN = "このクソ猿はよくも私の姿になって、ここでだまします！", --杨戬
        MYTH_INTERIORS_LIGHT = "灯油を盗むネズミはいないだろうか。",
        MYTH_INTERIORS_BED = "お茶と論道のいい場合。",
        MYTH_INTERIORS_GZ = "缶は凡物だが，すばらしいものは絵だ。",
        MYTH_INTERIORS_PF = "屏風に止められるのは君子です。",
        MYTH_INTERIORS_XL = "ほこりから抜け出した逸品。",
        MYTH_INTERIORS_ZZ = "机にうつぶせになって書道を研究しています。昔から今までもう何代もの先輩がいました。",
        MYTH_FOOD_ZPD = "「杨戬」はご指示に従いかねますので、早く持って行ってください。",
        MYTH_FOOD_NX = "どの料理人が考えたのか分かりません。",
        MYTH_FOOD_LXQ = "熱い口のエビの丸が玉の皿に入る。",
        MYTH_FOOD_FHY = "山を越え海を覆って客をもてなす。",
        MYTH_FOOD_HYMZ = "春の花を飾りとして，秋の月がいっぱいある。",
        MYTH_BANANA_LEAF = "これはただ青々として滴るばかりだ。",
        MYTH_BANANA_TREE = "庭の百花が休んでいます。月光の光が葉を照らしています。",
        MYTH_ZONGZI_ITEM1 = "蜂蜜を少しつけたら、ただこの世の美味しいものです。",
        MYTH_ZONGZI_ITEM2 = "粽の葉は香りが漂っています。少し脂っこいを減らした、味が多くて爽やかです。",
        BANANAFAN_BIG = "なんと勝手に天意を改正できる、これが福かどうか、災いかどうか、わかりません。"
    },
    --玉兔
    myth_yutu = {
        EAT_RESISTANT_PILL = "諸火辟易", --避火丹
        COLD_RESISTANT_PILL = "諸寒侵さず", --避寒丹
        DUST_RESISTANT_PILL = "汚れが染まらない", --避尘丹
        FLY_PILL = "霧に乗って雲をあける", --腾云
        BLOODTHIRSTY_PILL = "使いすぎると、精神をもみすぎた", --嗜血
        CONDENSED_PILL = "注意力はもっと集中しました。", --凝神
        THORNS_PILL = "茨が私を折り返している", --荆棘
        ARMOR_PILL = "今、私は一番強いウサギだ！", --壮骨
        DETOXIC_PILL = "", --化毒
        PEACH = "小さい「蟠桃」だね！大会では多いだよ。", --桃子
        BIGPEACH = "こんなの大きさの「蟠桃」は珍しいですね。", --大桃子
        PEACH_COOKED = "そうすれば霊妙な気質をすっかり失った。", --烤桃子
        PEACH_BANQUET = "毎回の「蟠桃大会」、嫦娥お姉さんが連れて行ってくれるわ。", --蟠桃大会
        PEACH_WINE = "つぼの中の「素酒」は波がきらきらと輝き，まるで湖の水が中に流れ落ちているようだ。", --蟠桃素酒
        MK_JGB = "そのクソ猿はきっと近くにいます。", --金箍棒
        MK_JGB_PILLAR = "斉天大聖！威風堂々！", --定海神珍
        HONEY_PIE = "カバンに入れて持って帰ります。", --蜂蜜素饼
        VEGETARIAN_FOOD = "私はお坊さんじゃないが、でもこれは私が精進料理が好きなことに邪魔にならない。", --素斋
        CASSOCK = "お~頭にかけるのか？", --袈裟
        KAM_LAN_CASSOCK = "あら、こんなに宝物が多いだ！", --锦襕袈裟
        GOLDEN_HAT_MK = "またあのクソ猿！", --凤翅紫金冠
        GOLDEN_ARMOR_MK = "他人の縁結びを破るやつ！", --大圣战甲
        MK_BATTLE_FLAG = "「玉兔」は来たことがある（書く）", --旗子
        MK_BATTLE_FLAG_ITEM = "なかなか威風がいい", --旗子
        XZHAT_MK = "あの猿はひどすぎたよ！ウサギの毛で帽子を作ったて！", --行者帽
        NZ_LANCE = "それは触ると暖かい", --火尖枪
        NZ_DAMASK = "法力に満ちた", --混天绫
        NZ_RING = "ちょうど私は金の腕輪に欠けていた。", --乾坤圈
        PIGSY_RAKE = "熊手はいいものだが、人だな...", --九齿钉耙
        PIGSY_HAT = "平凡で何の変哲もない", -- 墨兰帽
        GOURD = "中身の物を見せて", --葫芦
        GOURD_COOKED = "もうクミンをちょっとお願い、ありがとう", --烤葫芦
        GOURD_SOUP = "暑さを払う", --葫芦汤
        GOURD_OMELET = "なぜ卵を入れるの？", --葫芦鸡蛋饼
        WINE_BOTTLE_GOURD = "ヒヒ、早くいっぱいくれよ！", --酒葫芦
        PILL_BOTTLE_GOURD = "中には飲んで飛び上げる丹薬がある？", --丹药葫芦
        BANANAFAN = "扇で風が生じる", --芭蕉扇
        LAOZI_SP = "これは「老君」の「勅令」じゃないか？", --急急如律令
        LAOZI = "「老君」に謁見します。", --老子
        WB_HEART = "悪、醜、陰の重合体", --阴森之心
        BONE_WAND = "邪道だけだ", --骨杖
        BONE_BLADE = "邪道だけだ", --骨刃
        BONE_WHIP = "邪道だけだ", --骨鞭
        BONE_PET = "魂はもう帰ったが，骨はなぜ長く残される！？", --小骷髅
        LOTUS_FLOWER = "落日には限りなく赤い，あたり一面の蓮花の香り。",
         --莲花
        LOTUS_FLOWER_COOKED = "モチモチ", -- 烤莲花
        ALCHMY_FUR = {
            --炼丹炉
            GENERIC = "これは使う方が分からない。私は臼で薬をつく方法が知ってるだけです。"
        },
        YJ_SPEAR = "「真君」の武器はどうやってここに？",
         --三尖两刃刀
        YANGJIAN_HAIR = "鳳鳴ひゅうひゅう",
         --三山飞凤冠
        YANGJIAN_ARMOR = "寒さが身に迫る",
         --锁子清源甲
        SKYHOUND = "「哮天」、にんじんを食べてみたい？",
         --哮天犬
        SKYHOUND_WARG = "大きくなったわ！", --月石哮天犬
        SKYHOUND_TOOTH = "消えない心！",
         --天犬幼齿
        MONKEY_KING = "お前は他の人の良い事を破壊するクソ猿！", --猴子
        MK_PHANTOM = "君は本物？それとも偽物？", --猴子分身
        PIGSY = "早く「广寒宫」から離れて", -- 猪猪
        NEZA = "三太子はどこに行くかな？", --那咋
        WHITE_BONE = "へん、私がいる限り、広寒宮に近づくことが思うな！", --白骨
        YANGJIAN = "「嫦娥」姉さんはあなたを招待してお茶を飲みに行きますよ。", --杨戬
        MYTH_INTERIORS_LIGHT = "長明不滅", --房子里面的灯
        MYTH_INTERIORS_BED = "休憩の場所",
         --房子里面的床
        MYTH_INTERIORS_GZ = "絵一つだけだ",
         --房子里面的挂画
        MYTH_INTERIORS_PF = "ほのぼのとした美しさがある。",
         --房子里面的屏风
        MYTH_INTERIORS_XL = "白檀の燃える香りは心の安定を助けます。",
         --房子里面的香炉
        MYTH_INTERIORS_ZZ = "私に整理させてみましょう。",
         --房子里面的桌子
        MYTH_FOOD_ZPD = "これは毒物ですか？！",
         --猪皮冻
        MYTH_FOOD_NX = "濃厚なミルクの香り",
         --奶昔
        MYTH_FOOD_LXQ = "大きくてふっくら",
         --龙虾球
        MYTH_FOOD_FHY = "お客さんを招待するために利用できます。",
         --覆海宴
        MYTH_FOOD_HYMZ = "月の神様があなたの耳元でささやいている。",
         --花语满载
        MYTH_BANANA_LEAF = "翡翠のよう！",
         --香蕉叶子
        MYTH_BANANA_TREE = "もう待ちきれない。",
         --香蕉树
        MYTH_ZONGZI_ITEM1 = "一つでお腹がいっぱいになれる。",
         --粽子
        MYTH_ZONGZI_ITEM2 = "肉やちまきは食べませんよ！",
         --肉粽
        BANANAFAN_BIG = "万丈の風雨はここから起こる。"
     --大扇子
    },
	--盘丝娘娘
	madameweb = {	
		HEAT_RESISTANT_PILL = "诸火辟易",  --避火丹
		COLD_RESISTANT_PILL = "诸寒不侵",  --避寒丹
		DUST_RESISTANT_PILL = "涤垢不染",  --避尘丹
		FLY_PILL = "揽雾登青云",  --腾云
		BLOODTHIRSTY_PILL = "我需要血！更多的血！",  --嗜血
		CONDENSED_PILL = "我的注意力更集中了",  --凝神
		THORNS_PILL = "荆棘庇佑", --荆棘
		ARMOR_PILL = "妾身更强大了！", --壮骨

		PEACH = "这倒是不曾多见",  --桃子
		BIGPEACH =  "这般大的仙桃！。", --大桃子
		PEACH_COOKED = "如此这般便失了灵气",  --烤桃子
		PEACH_BANQUET = "妾身倒是三生有幸了。" , --蟠桃大会
		PEACH_WINE = "灵界甘露，仙宫典藏",  --蟠桃素酒
		MK_JGB = "那贼猴定在附近",  --金箍棒
		MK_JGB_PILLAR = '齐天大圣！好不威风。', --定海神珍
		HONEY_PIE = "妾身可不爱这过家家。",  --蜂蜜素饼
		VEGETARIAN_FOOD = "来点荤的斋饭" , --素斋
		CASSOCK = "哦~披在头上吗？",  --袈裟
		KAM_LAN_CASSOCK = "此物并非凡间所有！",  --锦襕袈裟

		GOLDEN_HAT_MK = "又是那泼猴",  --凤翅紫金冠
		GOLDEN_ARMOR_MK = "妾身恨不得把他撕碎",  --大圣战甲

		MK_BATTLE_FLAG = "不若下次妾身来织",  --旗子
		MK_BATTLE_FLAG_ITEM = "倒是蛮威风的" , --旗子

		XZHAT_MK  = "哼，且报窃衣之仇。" ,     --行者帽


		NZ_LANCE =  "三太子，来斗一场" ,     --火尖枪
		NZ_DAMASK =  "让妾身试试你这混天绫",     --混天绫
		NZ_RING =  "妾身还缺了一只金镯子。",     --乾坤圈 

		PIGSY_RAKE = "耙子倒是好东西，人嘛...", --九齿钉耙
		PIGSY_HAT = "脏死了，快拿走", -- 墨兰帽
		GOURD = "让我看看里面是什么", --葫芦
		GOURD_COOKED = "来点虫子夹心，谢谢" , --烤葫芦
		GOURD_SOUP = "清爽解暑" , --葫芦汤
		GOURD_OMELET = "为什么要加鸡蛋呀" , --葫芦鸡蛋饼
		WINE_BOTTLE_GOURD = "嗝，妾身没有喝醉。", --酒葫芦
		PILL_BOTTLE_GOURD = "里面可有什么灵丹妙药？", --丹药葫芦
		BANANAFAN = "一扇风起", --芭蕉扇
		LAOZI_SP = "这不是老君的敕令吗？", --急急如律令
		LAOZI = "妾身还是早早遁去吧", --老子
		WB_HEART =  "邪恶，丑陋，阴暗的聚合体" , --阴森之心
		BONE_WAND =  "多么美妙的武器" , --骨杖
		BONE_BLADE =  "多么美妙的武器" , --骨刃
		BONE_WHIP =  "多么美妙的武器" , --骨鞭
		BONE_PET =  "啧啧，是什么支撑着你的躯体？" , --小骷髅

		LOTUS_FLOWER = "落日无边红，一截藕花香" ,--莲花 
		LOTUS_FLOWER_COOKED = "绵香暖糯", -- 烤莲花
		
		ALCHMY_FUR =  {   --炼丹炉
			GENERIC = "三昧翻滚，阴阳颠转",
		},	
		YJ_SPEAR = "如此宝器怎么遗失在这",--三尖两刃刀
		YANGJIAN_HAIR = "凤鸣潇潇",--三山飞凤冠
		YANGJIAN_ARMOR = "清寒逼人",--锁子清源甲
		SKYHOUND = "莫要来沾惹本尊",--哮天犬	
		SKYHOUND_WARG = "多了几分本事？", --月石哮天犬
		MONKEY_KING = "你这好色的弼马温，休要狡辩", --猴子
		MK_PHANTOM = "你是真的还是假的呢？", --猴子分身
		PIGSY = "莫要来玷污濯垢泉", -- 猪猪
		NEZA = "三太子要去哪呀！", --那咋
		WHITE_BONE = "你我本同道，何必相决绝", --白骨
		YANGJIAN = "参见真君，妾身有礼了。", --杨戬
		YAMA_COMMISSIONERS = "妾身还没到油尽灯枯的时候",

		MYTH_INTERIORS_LIGHT = "长明不灭", --房子里面的灯
		MYTH_INTERIORS_BED = "休憩的场所",--房子里面的床
		MYTH_INTERIORS_GZ = "一副挂画罢了",--房子里面的挂画
		MYTH_INTERIORS_PF = "朦朦胧胧，隐隐约约",--房子里面的屏风
		MYTH_INTERIORS_XL = "檀香定心",--房子里面的香炉
		MYTH_INTERIORS_ZZ = "让我来整理一下吧",--房子里面的桌子
		MYTH_FOOD_ZPD = "这东西是毒药吗？",--猪皮冻
		MYTH_FOOD_NX = "浓浓的奶香",--奶昔
		MYTH_FOOD_LXQ = "又大又饱满",--龙虾球
		MYTH_FOOD_FHY = "可以用来宴请客人",--覆海宴
		MYTH_BANANA_LEAF = "如玉似翡",--香蕉叶子
		MYTH_BANANA_TREE = "我已经等不及了",--香蕉树
		MYTH_ZONGZI_ITEM1 = "一个就管饱",--粽子
		MYTH_ZONGZI_ITEM2 = "咸鲜合宜",--肉粽
		BANANAFAN_BIG = "万丈风雨由此起",--大扇子
	}
}

local newitems = {
    MEDICINE_PESTLE_MYTH = {
        --捣药杵
        monkey_king = "このような兵器は大胆に俺と敵に当たる？",
        pigsy = "俺に貸してもちをつくのに使う~",
        neza = "私の「火尖枪」と比べると、どれのほうが強い？",
        white_bone = "ずいぶんきらきらのような宝物の器だ",
        yangjian = "これは「广寒宫」の中の薬杵ではないでしょうか？",
        myth_yutu = "羊脂の玉の一段に，薬の香りが千年を巻き付ける。",
        madameweb="仙宫造物，灵气不绝",
    },
    MYTH_GHG = {
        monkey_king = "「嫦娥」はここに引っ越したか？",
        pigsy = "俺は一人で入るのはあまりよくないでしょう。",
        neza = "碧瓦瑠璃，寒氷翠玉。",
        white_bone = "衆生は月を拝み，太陰の所にいる。",
        yangjian = "「广寒宫」もこの世界に降臨した。",
        myth_yutu = "ただいま!",
        madameweb="钟灵秀敏，人间仰望",
    },
    MYTH_CHANG_E = {
        monkey_king = "「仙子」は何をしていますか？",
        pigsy = "「嫦...嫦娥仙子」、「仙子」は最近はいいかな。",
        neza = "「嫦娥」姉さん、私はまた月餅を食いに来ました！",
        white_bone = "「仙子」に修行を教えてもらうことができますか？",
        yangjian = "「仙子」がお茶を一杯くれますように。",
        myth_yutu = "「嫦娥」姉さん、ただいま！",
        madameweb="卑鄙之身，还望海涵",
    },
    MYTH_INTERIORS_GHG_LU = {
        monkey_king = "先生の部屋の中のそれよりもっと精致だな。",
        pigsy = "「仙子」の生活は本当に精致だね。",
        neza = "お母さんは家でもよくこんなのストーブを使います。",
        white_bone = "風がないけと起きる、確かにいい香だ。",
        yangjian = " 静かに座って、香炉の中で雲煙が起伏するのを見てる。",
        myth_yutu = "今日は何の香が燃えてるの？",
        madameweb="清烟如丝起",
    },
    MYTH_INTERIORS_GHG_LIGHT = {
        monkey_king = "これは何の宝物？",
        pigsy = "明るすぎだ。",
        neza = "昼も点灯しますか？",
        white_bone = "杯皿灯碗、すべてのものも非凡である。",
        yangjian = "「仙子」のように可愛い。",
        myth_yutu = "今日は光がもっと優しくなったようだ。",
        madameweb="幽幽月明",
    },
    MYTH_INTERIORS_GHG_FLOWER = {
        monkey_king = "(クンクン)....ハクション！",
        pigsy = "香りがいいですね。",
        neza = "「哪吒」は一本の枝を折ることができますか？",
        white_bone = "桃李凡花と混同しない。",
        yangjian = "仙子の好みはなかなか上品だな。",
        myth_yutu = "月宮の白梅は，長盛衰えない。",
        madameweb="我想让它凋谢....",
    },
    MYTH_INTERIORS_GHG_HE_RIGHT = {
        monkey_king = "花果の山にも鶴の群れがいる。",
        pigsy = "鶴の足が美味しいかどうかな？",
        neza = "ただの彫像ですか？",
        white_bone = "霊光が流転して，死物ではない。",
        yangjian = "ハハ、お二人はこの月宮の護衛ですか？",
        myth_yutu = "怠けないで、早く遊びに連れて行ってよ。",
        madameweb="妾身可看得出你的真身",
    },
    MYTH_INTERIORS_GHG_HE_LEFT = {
        monkey_king = "花果の山にも鶴の群れがいる。",
        pigsy = "鶴の足が美味しいかどうかな？",
        neza = "ただの彫像ですか？",
        white_bone = "霊光が流転して，死物ではない。",
        yangjian = "ハハ、お二人はこの月宮の護衛ですか？",
        myth_yutu = "怠けないで、早く遊びに連れて行ってよ。",
        madameweb="妾身可看得出你的真身",
    },
    MYTH_REDLANTERN = {
        monkey_king = "明かりを一つ提げて，小僧が止めるな。",
        pigsy = "俺は「高老庄」で畑を回る時にいつでもそれを持っています。",
        neza = "私の家にもこのランプがたくさんあります。",
        white_bone = "月は明灯のように、紅塵の中に入る。",
        yangjian = "この世の花火。",
        myth_yutu = "どんなスタイルが一番いいの？",
        madameweb="灯笼薄腊纸，云母含清光",
    },
    MYTH_REDLANTERN_GROUND = {
        monkey_king = "俺は触ったら壊れます。",
        pigsy = "これに寄りかかってもいい？",
        neza = "「哪吒」もこれをつくれる。",
        white_bone = "提灯置き場。",
        yangjian = "夕闇に灯を挙げる、美しい。",
        myth_yutu = "私の一番好きな提灯を上に掛けます。",
		yama_commissioners = "此心安处即吾乡",
		madameweb="去年元夜时，花市灯如昼。",
    },
    MYTH_RUYI = {
        monkey_king = "へイ、あの「五庄观」の「金击子」みたいですね。",
        pigsy = "解散するなら，これは俺の所有だね。",
        neza = "壊れやすいように見えます。",
        white_bone = "この宝物は私のものだ。",
        yangjian = "仙霊の気がみなぎる。",
        myth_yutu = "これは姉さんの玉如意じゃ！",
		yama_commissioners = "闲弄如意，若鹤翻飞",
		madameweb="灵韵内涵，不露清光",

    },
    MYTH_FENCE = {
        monkey_king = "俺の「水帘洞」はまだ屏風一つが欠けているね。",
        pigsy = "俺は屏風が嫌いだ。あれは何も見えなくならせた！",
        neza = "「哪吒」の家にはたくさんあります。",
        white_bone = "屏風の後、白骨の上から血肉が生く、描いた皮袋をそっと羽織る、美人は出かけられた。",
        yangjian = "多くの不便を遮った。",
        myth_yutu = "「玉兔」もいろんなデザインが描けますよ。",
		yama_commissioners = "遮蔽了什么，掩藏了什么",
		madameweb="人行明镜中，鸟度屏风里。",
    },
    MYTH_FENCE_ITEM = {
        monkey_king = "俺の「水帘洞」はまだ屏風一つが欠けているね。",
        pigsy = "俺は屏風が嫌いだ。あれは何も見えなくならせた！",
        neza = "「哪吒」の家にはたくさんあります。",
        white_bone = "屏風の後、白骨の上から血肉が生く、描いた皮袋をそっと羽織る、美人は出かけられた。",
        yangjian = "多くの不便を遮った。",
        myth_yutu = "「玉兔」もいろんなデザインが描けますよ。",
		yama_commissioners = "遮蔽了什么，掩藏了什么",
		madameweb="人行明镜中，鸟度屏风里。",
    },
    MYTH_BBN = {
        monkey_king = "この袋は不思議だね。",
        pigsy = "俺が俺のへそくりをここに隠したら、「弼马温」はきっと見えない。",
        neza = "「哪吒」は全部の法宝をここに置くことができます。",
        white_bone = "肌身の宝物。",
        yangjian = "月華が駆動するスペースバッグは、あまり安定していないようだ。",
        myth_yutu = "アィヤ、もうすぐまた月華を注がねばならない。",
    },
    MYTH_YYLP = {
        monkey_king = "玉にはまだ花が咲いていますが、猿が飛び出すことができますか？",
        pigsy = "大きな自然の玉！",
        neza = "花を一つ摘み取って「哪吒」をくれてもいい？",
        white_bone = "仙術！上には仙界の知識が記録されています！",
        yangjian = "品物の透明な玉を記録する。",
        myth_yutu = "それらはどうやって作ったのか見せなさい。",
    },
    MYTH_MOONCAKE_ICE = {
        monkey_king = "涼しい！",
        pigsy = "この月餅は「广寒宫」にしかありません。",
        neza = "暑気を払うには必ず必要である。",
        white_bone = "精神力を結集するのを手伝ってくれます。",
        yangjian = "これがあれば「走火入魔」を恐れなかった。",
        myth_yutu = "もう一つお願いします~",
    },
    MYTH_MOONCAKE_NUTS = {
        monkey_king = "俺は次のものしか食べられませんでした。",
        pigsy = "このつまらないものは俺を腹いっぱい食べさせることができるのだ。",
        neza = "これは最後に食べます。",
        white_bone = "力のために食べ物を探すの必要はなかった。",
        yangjian = "辟谷丹より美味しい。",
        myth_yutu = "お腹がいっぱいだ。",
    },
    MYTH_MOONCAKE_LOTUS = {
        monkey_king = "食べさせてみて。",
        pigsy = "あまり感じないようだ。",
        neza = "「嫦娥」姉さんはいくつもくれましたぞう！",
        white_bone = "食べ物の中の不利な要素を排除してくれます。",
        yangjian = "食べ物の品質を心配する必要がなかった。",
        myth_yutu = "前菜入りの月餅。",
    },
    MYTH_CASH_TREE_GROUND = {
        monkey_king = "果物が落ちれないのが残念です。",
        pigsy = "もっと多く落ちて。",
        neza = "揺れさせてみて。",
        white_bone = "貪欲のどん底。",
        yangjian = "凡人を魅了する。",
        myth_yutu = "私に当てないで。",
    },
    MYTH_CHANG_E_FURNITURE = {
        monkey_king = "座禅はやはり師匠に行きましょう。",
        pigsy = "お尻の半分ぐらいだけだ。",
        neza = "ふわふわに見える。",
        white_bone = "仙子に修行を教えてもらうことができます。",
        yangjian = "少し休憩できます。",
        myth_yutu = "その上に座って話を聞きたい。",
    },
    MYTH_CASH_TREE = {
        monkey_king = "何を使って肥料をやりますか？",
        pigsy = "土に植えたら背が高くなれますか？",
        neza = "お金を上に掛けることができる。",
        white_bone = "仙宝の雛形",
        yangjian = "貨幣で供養してこそ，初めて成長することができます。",
        myth_yutu = "お腹が空いたそう。",
    },
    MYTH_TREASURE_BOWL = {
        monkey_king = "桃を吐き出せるか？",
        pigsy = "それを家に運びたい。",
        neza = "子供はギャンブルをしてはいけません。",
        white_bone = "ただの取引です。",
        yangjian = "十賭九敗。",
        myth_yutu = "ちょっと遊ぶだけ、「嫦娥」姉さんに見つけられない！",
    },
    MYTH_SMALL_GOLDFROG = {
        monkey_king = "蟻が多くなら、象も殺せる。",
        pigsy = "立ち去れ！",
        neza = "「乾坤圈」来い！",
        white_bone = "妖怪は逃げるな！",
        yangjian = "区区の小妖、よくも俺「二郎显圣真君」を怒らせる！",
        myth_yutu = "来ないで！！",
    },
    MYTH_GOLDFROG_BASE = {
        monkey_king = "彫虫小技、俺をだますことはできないぞう。",
        pigsy = "あれは俺の！",
        neza = "ここにどうしてそんなに大きな元宝がありますか？",
        white_bone = "罠が仕掛けられていた。",
        yangjian = "「ここには300両の銀がない」のこと？",
        myth_yutu = "大きい金元宝だね！",
    },
    MYTH_GOLDFROG = {
        monkey_king = "俺に宝物をお預かりさせましょう。",
        pigsy = "ちょっとなにを落ちたら、たくさんのいいものを買って食べることがもう十分だ。",
        neza = "どこからか盗んできたの人民の膏血！",
        white_bone = "あれの身に必ず私に必要な宝物があります！",
        yangjian = "区区の妖怪、よくも俺「二郎显圣真君」を怒らせる！",
        myth_yutu = "恐ろしい大ガマ！",
    },
    MYTH_COIN = {
        monkey_king = "花果山ではこれは使えない。",
        pigsy = "小さい銀に両替しないと隠しにくい。",
        neza = "俺はこれを珍しくないですよ。",
        white_bone = "俗塵による願力をこれに加持する。",
        yangjian = "世の中を生活すれば、これが欠かせない。",
        myth_yutu = "これで人参を買えるね！",
    },
    MYTH_YJP = {
        monkey_king = "菩薩によると、あれを使って、「老君」の「炼丹炉」で焼いた柳の枝でも、全部救うことができるそうだ。",
        pigsy = "俺はまだ甘露の味を味わったことがないね。",
        neza = "それを使って四海の水を取って行いたら、きっとあの老竜王を怒らせましょう！",
        white_bone = "私は本当にとても凄い造化を得た！",
        yangjian = "甘露妙法、樹永長春。",
        myth_yutu = "この宝物があったら、私の薬田がもっと手入れしやすいでした。",
    },
    MYTH_TUDI = {
        monkey_king = "「土地老儿」、お久しぶりだね。",
        pigsy = "「土地」、俺は精進料理を一つ乞いに来た（改）",
        neza = "「土地公公」、「哪吒」に何か手伝いがありますか？",
        white_bone = "お寺も小さいし、この神も小さい，私は珍しくない",
        yangjian = "この土地は面倒してお願いだ。",
        myth_yutu = "「土地おばあさん」はどこに行きましたか？",
    },
    MYTH_TUDI_SHRINES = {
        monkey_king = "「土地老儿」はいますか？",
        pigsy = "食べ終れないお布施は俺にくれるよ",
        neza = "「土地公公」、家にいますか？",
        white_bone = "法の力の弱いじじでさえ線香の火が供えられている。なんと不公平なことか。",
        yangjian = "こちの「土地」で速く会って来い！",
        myth_yutu = "ワ！きれいなお寺ですね！ ",
    },
    BOOKINFO_MYTH = {
        monkey_king = "上に何が書いてあるかを俺に見てもらおう",
        pigsy = "出った！字が出った！",
        neza = "ヒヒ、今はおれのものんだ！",
        white_bone = "闇に燃えた灰の中に、本の香りがかすかに現れた。",
        yangjian = "諸天のこまごました事については，すべて記録がある。",
        myth_yutu = "私はどのページですか？",
    },
    YANGJIAN_BUZZARD = {
        --傲天鹰
        monkey_king = "あのハゲ鳥の尖ったくちばしは死ぬまで忘れない！",
        pigsy = "この手羽先はきっとすごく美味しい！",
        neza = "「哪吒」に何かいいものを持ってきたか？",
        white_bone = "楊二郎はきっと近くにいます。速く逃げろ！",
        yangjian = "戦友に寄り添うべきだ。",
        myth_yutu = "私に手紙を送ってくれますか？",
    },
    MYTH_YUTU = {
        --玉兔
        monkey_king = "ダイ！この妖精はまた勝手に下界にやって来た。",
        pigsy = "もう二度と酔って暴れたり暴れたりしません。",
        neza = "仙子はどこに行くつもりか？",
        white_bone = "いくら言ってもただの妖怪だ。",
        yangjian = "主人は何の依頼がありますか？",
        myth_yutu = "あれ？これは鏡を見ていますか？",
    },
    MYTH_GRANARY = {
        --谷仓
        monkey_king = "これはあのアホにバレるわけにはいかない",
        pigsy = "え、これは俺の故郷じゃないか！",
        neza = "今、「哪吒」は魚が食べたい",
        white_bone = "五穀を食べるなら，誰が病気が出ないか？",
        yangjian = "米倉ともに豊かで，庶民も和楽である",
        myth_yutu = "いっぱいにさせよう！",
    },
    MYTH_WELL = {
        --水池
        monkey_king = "俺の「花果山」の湧き水よりも少し悪いだね",
        pigsy = "この水は甘くかを食べさせてみましょう。",
        neza = "「哪吒」は風呂に入ることができますか？",
        white_bone = "ちりや垢を洗う",
        yangjian = "井戸水はまあまあ",
        myth_yutu = "あ！水を私にぶちまけるな！",
    },
    MOVEMOUNTAIN_PILL = {
        --移山丹
        monkey_king = "俺はこれが必要か？ ",
        pigsy = "食べたくないで、食べたら俺につらい仕事をさせるでしょう。",
        neza = "堅くなった。「哪吒」のこぶしが硬くなった。",
        white_bone = "一時的に力を増す丹薬。",
        yangjian = "ちょっとしたトリックだけだ。",
        myth_yutu = "私は今一撃だけであなたを閻魔に会わせるよ。",
    },
    MYTH_FOOD_SJ = {
        --水饺
        yangjian = "毎月とか毎年とか、いつでもお会いたい",
    },
    MYTH_FOOD_BZ = {
        --大肉包子
        pigsy = " 何か味を食べさせて、うん。。。",
    },
    MYTH_FOOD_HSY = {
        --麻辣红烧鱼
        neza = "「哪吒」をもう一杯お願いします。",
    },
    MYTH_FOOD_BBF = {
        --八宝饭
        neza = "甜而不腻，多有裨益"
    },
    MYTH_FOOD_TSJ = {
        --屠苏酒
        monkey_king = " いいお酒をもっと、もっと！",
        pigsy = "長年大切にしてきたいいお酒ですね。",
        neza = "一口飲ませて、一口だけ...",
        white_bone = "私とお酒を一杯飲みませんか？",
        yangjian = "羊と牛の料理を楽しみたいなら、あと300杯のお酒を飲みましょう！",
        myth_yutu = "月に誘われて酒に酔って、嫦娥は私をかばって寝ます。 "
    },
    MYTH_FOOD_TR = {
        --糖人
        monkey_king = "見覚えがある",
        pigsy = " ふん、一口でお前を食い尽くしてみよう。",
        neza = "「哪吒」の小糖人を見に来て！",
        white_bone = "またあのいまいましいクソ猿だ。",
        yangjian = "子供が食べるものです。ちょっと甘すぎ……",
        myth_yutu = "香りが甘くて口に合う"
    },
    MYTH_TOY_TIGERDOLL = {
        --布老虎
        neza = " アォウ、虎ちゃんが来た！"
    },
    MYTH_TOY_FEATHERBUNDLE = {
        --毽子
        white_bone = "私の昔のことを思い出してみると、つま先に花のようにそれをに乗せたこともある！"
    },

	MYTH_SIVING_BOSS = { --子圭玄鸟
		monkey_king = "アホ鳥め、お前の口の中にあるものを見せてみろ。",
		pigsy = "肉があるようには全然見えないのようだね。",
		neza = "「哪吒」の「风火轮」はあなたより速く飛べるぞ。",
		white_bone = "彼女の修為を奪ったら...",
		yangjian = "天命玄鸟？ 俗世の因果もう終えたか？",
		myth_yutu = "鳥ちゃん、何してるの？",
	},

	SIVING_ROCKS = { --子圭石
		monkey_king = "あれ！俺の金箍棒と同じように大きくに変われる？",
		pigsy = "食べられない、持っていても意味がない。",
		neza = "（刺す）動けるのかな？",
		white_bone = "素晴らしく活気に満ちた香り",
		yangjian = "非霊的、非俗的な、生命力はあるが知恵がない",
		myth_yutu = "なんて可愛い石なんだ！",
	},

	ARMORSIVING = { --子圭战甲
		monkey_king = "体は滅びても、心は決して滅びない！",
		neza = "血と骨は泥とか、埃とかになって、消えていた。",
		white_bone = "妖に生まれついては，何の妨げにもなる？",
		yangjian = "天の雷鳴は澄んでいる、天の力は冴えている",
	},

	SIVING_HAT = { --子圭战盔
		monkey_king = "あの諸神に頭を下げさせて、まだ諸佛がどうのようが暴れられるか見てみよう！",
		neza = "金レンコンと青蓮でもう一回",
		white_bone = "天を奪い命を変える",
		yangjian = "司風命雨、清源妙道",
	},

	MYTH_LOTUS_FLOWER = { --莲花
		monkey_king = "如何にも美しい",
		neza = "ハスの花が化身し，霊珠が俗塵を現す。",
		white_bone = "私の顔は花のようによくない。",
		pigsy = "俺の大蓮ポンは？",
		myth_yutu = "嫦娥姉さんはきっとこの出水芙蓉が好きです。",
		yangjian = "西湖のハスの花は咲きましたか？",
	},

	LOTUS_ROOT = { --莲藕
		monkey_king = "花果山に及ばない桃",
		neza = "これは腕？それとも足か？",
		pigsy = "俺は一口で8個食べられる！",
		myth_yutu = "私が蓮根の盛り合わせを見せてあげましょう。",
	},

	LOTUS_SEEDS = { --莲子
		neza = "目？俺はパズルをしていますか？",
		pigsy = "そんなことで私をおざなりをするなよ",
		myth_yutu = "赤い皮をむいて、霜のような白い蓮の実を潰す。",
		yangjian = "蓮の芯は苦いが，それでも気を静める。",
	},

	MYTH_LOTUSLEAF = { --莲叶
		monkey_king = "雨を遮ることができて、日よけを防ぐことができる。",
		neza = "私はハスの葉で作ったこの服です。",
		white_bone = "雨露をしのぐ？あなたには使わないよ",
		myth_yutu = "雨はこの上に玉のように散っています。",
		pigsy = "それは俺をさえぎることができないぞ。",
	},

	MYTH_LOTUSLEAF_HAT = { --莲叶帽子
		monkey_king = "もうちょっと大きいといい",
		neza = "「哪吒」は頭の上に「哪吒」を支えている，ハハハ",
		white_bone = "翠のカーテンのようだ、私の顔を隠す。",
		myth_yutu = "小雨がしとしとと降るのはちょうどよい",
		pigsy = "昼寝はちょうど太陽を遮るために使う。",
		yangjian = "私をからかうな",
	},

	MYTH_FOOD_ZYOH = { --折月藕盒
		myth_yutu = "あなたの夢を彩りますように",
	},

	MYTH_FOOD_HBJ = { --荷包鸡
		monkey_king = "これはきっとあのばかには見せられない",
		neza = "あつ！あつい！",
		myth_yutu = "月宮ではこれまで味わったことがない。",
		pigsy = "まず食べてみます。",
		yangjian = "哮天」は一つを食べてみたいか？",
	},

	LAOZI_BELL = { --兜率牛铃
		monkey_king = "「老倌」の牛はどこに行った？",
		neza = "ディンリンリンリンリン",
		myth_yutu = "もっと綺麗な鈴がほしいな。",
		pigsy = "うるさくて、寝られない！",
		white_bone = "そんな神のような素材なのにもったいないだな！",
		yangjian = "哮天」、あなたもこれが欲しい？",
	},

	LAOZI_QINGNIU  = { --青牛
		monkey_king = "「老倌」、あなたの牛がまた飛び出しましたよ。",
		neza = "あなたが前回私の兵器を盗んだか？",
		myth_yutu = "大青牛は私を乗せて。",
		pigsy = "早く耕して来て！",
		white_bone = "頭を下げて卑屈になり，喜んで乗りに乗る。",
		yangjian = "青牛はなぜ兜率宮にいないですか？",
	},

	SADDLE_QINGNIU  = { --兜率牛鞍
		monkey_king = "「老倌」の尻当て。",
		neza = "哪吒も遊びに行きたい。",
		myth_yutu = "これがあればお尻が痛くないね。",
		pigsy = "上ではよく眠れるかな？",
		white_bone = "これはきっとあの蛮牛をコントロールする宝物です。",
		yangjian = "老君の椅子。",
	},

	MYTH_QXJ  = { --七星剑
		monkey_king = "老倌の宝はまた童子に盗まれた。",
		neza = "俺様が魑魅魍魎を殺されたの瞬間を見てくれ！",
		myth_yutu = "その上の星はきれいね。",
		pigsy = "スイカを切るのが速いかどうかわからないね。",
		white_bone = "どうしてこんなものが世に現われるものか！",
		yangjian = "霞の光がきらきらと光って，悠々として冷たい。",
	},

	MYTH_BAMBOO  = { --竹子
		monkey_king = "この竹竿は、武器としても良いでしょう",
		neza = "竹の芽が続く、松の青々としたように",
		myth_yutu = "笹の葉を撫でる風の音に耳を傾ける",
		pigsy = "木陰がある、ちょうど少しでも寝ようと思ってだ！",
		white_bone = "木陰がある、ちょうど少しでも寝ようと思ってだ！",
		yangjian = "暑さにも寒さにも負けず、永遠に青々としている",
	},

	MYTH_YAMA_STATUE1 = { --阎罗雕像 1级
		monkey_king = "破破烂烂一块砖",
		neza = "原始阎君在此",
		myth_yutu = "嫦娥姐姐说阎王也要步临此地了",
		pigsy = "大石头借俺靠着睡一觉",
		white_bone = "不好，阎王想把手伸入此地",
		yangjian = "阎君也开始进入此地了吗",
		yama_commissioners = "初临此地，阎君海涵",
	},

	MYTH_YAMA_STATUE2 = { --阎罗雕像 2级
		monkey_king = "看起来像是在供奉那阎王",
		neza = "些许炊饼，聊表敬意",
		myth_yutu = "阎王伯伯这是掉色了吗",
		pigsy = "什么破庙，连点吃的都没有",
		white_bone = "该死，是谁搭建的庙宇",
		yangjian = "阎君已经可以倾听并回应了",
		yama_commissioners = "敬奉二烛三香",
	},
	MYTH_YAMA_STATUE3 = { --阎罗雕像 3级
		monkey_king = "没想到阎王也能管到这儿来",
		neza = "阎君伯伯还没睡醒吗？",
		myth_yutu = "些许香烛，聊表敬意",
		pigsy = "下次下雨俺来借你宝地一用",
		white_bone = "我必须想些办法阻止阎王降临",
		yangjian = "阎君正一步步深入这个世界",
		yama_commissioners = "高台已立，风雨不侵",
	},
	MYTH_YAMA_STATUE4 = { --阎罗雕像 4级
		monkey_king = "阎罗小儿你这庙宇倒是宽敞",
		neza = "阎君伯伯安好！",
		myth_yutu = "这些灌木可真恐怖",
		pigsy = "哟！原来是阎君大人，可曾带些什么宝贝过来？",
		white_bone = "避之！避之！速速遁去！",
		yangjian = "恭喜阎君剥离此地束缚",
		yama_commissioners = "彼岸花开，君临供台",
	},

	MYTH_HIGANBANA_ITEM = { --彼岸花
		monkey_king = "这花艳的俺老孙喜欢",
		neza = "这花妖艳的让哪吒害怕！",
		myth_yutu = "忆往昔，此去黄泉谁相忆",
		pigsy = "不知道和牡丹比起来哪个好吃点",
		white_bone = "春花如面，秋雨不绝",
		yangjian = "忘川一河与花见",
		yama_commissioners = "花开，命魂引渡",
	},

	MYTH_CQF = { --出窍符咒
		monkey_king = "俺老孙还需要这玩意？",
		neza = "让哪吒先来试试",
		myth_yutu = "有了它就能偷偷出去玩了",
		pigsy = "俺老猪绝不会出去偷看那些仙子洗澡",
		white_bone = "妾本一孤魄，元神与魂连",
		yangjian = "日应万物，其心寂然",
		yama_commissioners = "吾乃阴司正神，何须出窍",
		wortox = "灵魂离体？你想让我现在就去地狱吗？",
		wormwood = "我的灵魂已深深扎根",
		wurt = "浮浪噗！要以纯真的身心侍奉我们的国王！",
		wanda = "离开躯体，时间的伟力量就会撕碎我都魂魄",
	},
	HAT_COMMISSIONER_BLACK = { --无常官帽黑
		monkey_king = "嘿，这不是那黑小官的帽子吗？",
		neza = "二位无常怎会丢失法器？",
		myth_yutu = "得把帽子还给小黑",
		pigsy = "俺带着这帽子俊俏不？",
		white_bone = "妾本一孤魄，元神与魂连",
		yangjian = "这不是黑无常的帽子吗，怎会遗失在此？",
	},
	HAT_COMMISSIONER_WHITE = { --无常官帽黑
		monkey_king = "嘿，这不是那白小官的帽子吗",
		neza = "二位无常怎会丢失法器？",
		myth_yutu = "得把帽子还给小白",
		pigsy = "俺带着像新郎官不？",
		white_bone = "妾本一孤魄，元神与魂连",
		yangjian = "这不是黑无常的帽子吗，怎会遗失在此？",
	},
	PENNANT_COMMISSIONER  = {--"招魂幡"
		monkey_king = "这破旗子能招来什么歪瓜裂枣",
		neza = "幡旗招展，倒是好看",
		myth_yutu = "是谁，在迷雾中前来",
		pigsy = "帮俺找几个漂亮的女施主",
		white_bone = "这诸般魂魄，便是我的美餐",
		yangjian = "不可随意招魂",
	},
	BELL_COMMISSIONER = { --摄魂铃
		monkey_king = "听的俺老孙都快睡着了",
		neza = "让哪吒想起了妈妈的摇篮曲",
		myth_yutu = "嫦娥姐姐，我想睡了",
		pigsy = "呼呼zｚＺ",
		white_bone = "摄魂取魄！",
		yangjian = "扰乱神识的法器",
	},
	WHIP_COMMISSIONER ={ --勾魂锁
		monkey_king = "来对俺老孙试试，嘿嘿！",
		neza = "怨气横行的法器",
		myth_yutu = "我不会用上它的",
		pigsy = "嘿嘿，离俺老猪远点",
		white_bone = "那莫不是专拿鬼魂的勾魂锁",
		yangjian = "多大的怨气都逃不过这勾魂锁",
	},
	TOKEN_COMMISSIONER = { --镇魂铃
		monkey_king = "阴兵看到俺老孙还不得让道？",
		neza = "哪吒还是靠边走走吧",
		myth_yutu = "太阴月华，照耀诸天",
		pigsy = "俺先溜了",
		white_bone = "阴气森森，是非之物",
		yangjian = "群鬼见之，谁敢不从",
	},
	MYTH_HUANHUNDAN = { --还魂丹
		monkey_king = "老君那多着呢，就是藏着不让俺老孙看着",
		neza = "师傅倒是为我讨要过几粒",
		myth_yutu = "比糖豆好吃吗",
		pigsy = "能再给俺一粒吗？",
		white_bone = "必能极大裨益我的元神",
		yangjian = "老君很少开炉炼制此丹药",
		yama_commissioners  = "魂兮来兮",
	},
	MYTH_MOONCAKE_BOX = {
		monkey_king = "送师傅一份，他定然开心",
		neza = "哪吒想家了，想妈妈做的月饼",
		myth_yutu = "就吃一小块！",
		pigsy = "俺的为啥又空了？嗝~~",
		white_bone = "看我用癞蛤蟆变点素斋填满它",
		yangjian = "仙子倒是有心了",	
	},

	MYTH_COIN_BOX = {
		monkey_king = "腌臜的铜臭味",
		neza = "一大串铜钱！在陈塘关可以买好多糖葫芦",
		myth_yutu = "一个萝卜几文钱呢？",
		pigsy = "这次俺可得藏好了",
		white_bone = "呵呵，奴家随手可变",
		yangjian = "人间俗物，沾惹红尘",
		yama_commissioners  = "生不带来，死不带去",		
	},
	MYTH_BAMBOO_BASKET = {
		myth_yutu = "我可不是来采蘑菇的",
	},
	--蛛网陷阱(种下去的)
	MADAMEWEB_PITFALL_GROUNG = {
		monkey_king = "休想骗俺老孙",
		neza = "小爷我可不是你能骗的了的",
		myth_yutu = "我可不是小兔，不是小白",
		pigsy = "朗格里格朗，太阳真真好",
		white_bone = "呵呵，雕虫小技罢了",
		yangjian = "何人在此设阱",
		yama_commissioners  = "吾等幽魂，何惧缠身",		
		madameweb ="你要来试试吗？",
	},
	--困顿之茧
	MADAMEWEB_PITFALL_COCOON = {
		monkey_king = "傻子上当了",
		neza = "呵呵，有笨蛋上当了",
		myth_yutu = "有小白上当了",
		pigsy = "哎呦，吓死俺老猪了",
		white_bone = "可惜呀~",
		yangjian = "白痴！",
		yama_commissioners  = "既已缠身，随吾长行",		
		madameweb ="动弹不得",
	},
	--困顿之茧
	MADAMEWEB_PITFALL_COCOON = {
		monkey_king = "傻子上当了",
		neza = "呵呵，有笨蛋上当了",
		myth_yutu = "有小白上当了",
		pigsy = "哎呦，吓死俺老猪了",
		white_bone = "可惜呀~",
		yangjian = "白痴！",
		yama_commissioners  = "既已缠身，随吾长行",		
		madameweb ="动弹不得",
	},
	--盘丝蜂针
	MADAMEWEB_STINGER = {	
		madameweb ="何必穿针上彩楼",
	},
	--盘丝蜂针
	MADAMEWEB_POISONSTINGER = {	
		madameweb ="金针玉指弄春丝",
	},
	--毒蜂
	MADAMEWEB_BEE = {
		monkey_king = "好阴狠的手段",
		neza = "混天绫在此，何惧尔等",
		myth_yutu = "哎呀，我先遁地了",
		pigsy = "别蜇俺，快走开",
		white_bone = "好生毒辣",
		yangjian = "毒物不可长留",
		yama_commissioners  = "丝丝毒，附骨疽",		
		madameweb ="沾染毒气的蜜蜂",
	},
	--盘丝蛛卵
	MADAMEWEB_POISONBEEMINE = {
		monkey_king = "嘿嘿，给你孵化又何妨",
		neza = "不可给机会",
		myth_yutu = "决策要成早",
		pigsy = "这比蝉蛹好吃吗",
		white_bone = "不为我所用，不可留",
		yangjian = "斩草需除根",
		yama_commissioners  = "此为生，吾为死",		
		madameweb ="妾身儿郎的产房",
	},
	--盘丝蛛巢
	MADAMEWEB_SPIDERHOME = {
		monkey_king = "吃俺老孙一棒",
		neza = "不可让它发展壮大",
		myth_yutu = "咦！恶心的蛛卵在动",
		pigsy = "什么东西在里面动",
		white_bone = "斯，未出生的蛛卵",
		yangjian = "邪恶含蕴的温床",
		yama_commissioners  = "盘丝缠，游蛛产",		
		madameweb ="蛛丝会保护你们",
	},
	--盘丝毒蛛
	MADAMEWEB_SPIDER_WARRIOR = {
		monkey_king = "哼，俺老孙不打女人，可饶不了你们这群怪物",
		neza = "你可别来招惹小爷",
		myth_yutu = "打不过...还是跑吧...",
		pigsy = "你这小玩意，长的还挺有趣",
		white_bone = "我更喜欢你们，化为白骨",
		yangjian = "助纣为虐，不可轻饶",
		yama_commissioners  = "铃儿摇，蛛儿跑，旗而飘，魂而消",		
		madameweb ="我的儿郎们可不是好惹的",
	},
	--盘丝披肩
	MADAMEWEB_ARMOR = {
		monkey_king = "这有啥好看的",
		neza = "这是女人家家的衣服，哪吒才不要呢",
		myth_yutu = "我才不要穿那个呢",
		pigsy = "嘿嘿嘿嘿，好看",
		white_bone = "奴家闻到了肮脏的蛛丝味",
		yangjian = "天眼所见，妖气横斜",
		yama_commissioners  = "红粉骷髅，白骨皮肉",		
		madameweb ="肩上霓裳美若珠",
	},
	--盘丝织袋
	MADAMEWEB_SILKCOCOON = {
		monkey_king = "这袋子倒是有趣",
		neza = "哪吒的东西会被弄脏的",
		myth_yutu = "黏糊糊的，我才不要用呢",
		pigsy = "让俺看看有没有吃的",
		white_bone = "我那披风，自有乾坤",
		yangjian = "这东西哮天都嫌弃",
		yama_commissioners  = "一个容器罢了",		
		madameweb ="且待明日，适时而开",
	},

	--盘丝标记
	HUA_INTERNET_NODE = {
		monkey_king = "俺老孙一个筋斗就走了",
		neza = "风火轮可比你快多了",
		myth_yutu = "我还是去打个秋千吧",
		pigsy = "俺上去不会踩断吧",
		white_bone = "我行如风，不为所阻",
		yangjian = "一堆玩具罢了",
		yama_commissioners  = "游行阴阳，来来往往",		
		madameweb ="蛛网之上，我自称王",
	},
	

	--盘丝标记水
	HUA_INTERNET_NODE_SEA = {
		monkey_king = "俺老孙一个跟头的事情，居然弄这么麻烦",
		neza = "风火轮在上面滑会不会烧坏丝呢",
		myth_yutu = "昔日洛神舞，今有本兔行，嘿嘿",
		pigsy = "吓死俺了，俺差点掉下去了",
		white_bone = "此物倒是便捷",
		yangjian = " 哦，水面滑行的感觉还不错",
		yama_commissioners  = " 额，我还是离水远一点吧",		
		madameweb ="乖乖的不许动哦",
	},

	--步丝履
	HUA_FAKE_SPIDER_SHOE = {
		monkey_king = "俺老孙的脚都被黏住了",
		neza = "哪吒还是更喜欢风火轮",
		myth_yutu = "呼呼，跑不过我哟！",
		pigsy = "这鞋怎么有点挤脚",
		white_bone = "此物可助我悬丝而行 ",
		yangjian = "休想让我穿上这双鞋",
		yama_commissioners  = "黏糊糊的",		
		madameweb ="此鞋可助你悬丝而行",
	},

	--盘丝悬茧
	MADAMEWEB_SILKCOCOON = {	
		madameweb ="腹丝如注，长悬空中",
	},

	--红包
	MYTH_PLAYERREDPOUCH_SUPER = {
		monkey_king = "嘿嘿，这红包倒是份量足足",
		neza = "这个红包比以往的都要厚！",
		myth_yutu = "嘿嘿，回去找嫦娥姐姐再要一份",
		pigsy = "俺的媳妇本又厚了",
		white_bone = "奴家的是最厚的吗？",
		yangjian = "这红包居然有我的一份",
		yama_commissioners  = "让我看看我的这份",		
		madameweb ="妾身是不是也要准备一些",
	},
	--铸铁大刀
	MYTH_IRON_BROADSWORD = {
		monkey_king = "凡兵俗器，一折就断了",
		neza = "火尖枪一碰就碎了",
		myth_yutu = "我还是不要这个了",
		pigsy = "吓死我了，我还以为是杀猪刀呢",
		white_bone = "啊，这可杀不死我",
		yangjian = "欲将轻骑逐,大雪满弓刀",
		yama_commissioners  = "俺来试试刮胡子",		
		madameweb ="风刀霜剑严相逼",
	},
	--铸铁战甲
	MYTH_IRON_BATTLEGEAR = {
		monkey_king = "俺老孙可不背这破铜烂铁",
		neza = "三面精金甲，妖魔破胆还",
		myth_yutu = "哈哈，现在你们打不过我啦",
		pigsy = "穿上去有点小挤",
		white_bone = "朔气传金柝，寒光照铁衣",
		yangjian = "羽仪映松雪，戈甲带春寒。",
		yama_commissioners  = "也许我并不需要它的保护",		
		madameweb ="要在哪里补一点蛛丝呢",
	},
	--苍竹瓦屋
	MYTH_HOUSE_BAMBOO = {
		monkey_king = "这哪能比得上俺老孙的水帘洞",
		neza = "小爷家可比这金碧辉煌",
		myth_yutu = "哇，好漂亮的小房子呀",
		pigsy = "睡哪里都差不多",
		white_bone = "歇脚之地罢了",
		yangjian = "宁可食无肉,不可居无竹",
		yama_commissioners  = "俺想小憩一会了",		
		madameweb ="让妾身看看哪里适合布上蛛丝",
	},
	--年兽
	MYTH_NIAN = {
		monkey_king = "大胆妖孽，扰乱新年，吃俺老孙一棒",
		neza = "哪吒的年夜饭都被你搅合了",
		myth_yutu = "哼...你快滚开啦",
		pigsy = "俺老猪放个屁就能吓跑你",
		white_bone = "年之皮骨，血肉法力都将归我",
		yangjian = "作乱人间，律令当斩",
		yama_commissioners  = "凡间事本不该管",		
		madameweb ="妾身的宴席都被你吓散了",
	},
	--年兽坐骑
	NIAN_MOUNT = {
		monkey_king = "吃了一棒，倒是乖顺了几分",
		neza = "你快赔哪吒的年夜饭",
		myth_yutu = "驾！跑快点呦",
		pigsy = "俺以后就省去了驾云的力气了",
		white_bone = "罢了，饶你一命",
		yangjian = "知错能改，善莫大焉",
		yama_commissioners  = "怎得，你还想被打一顿不成",		
		madameweb ="你以后就当妾身的脚力吧",
	},
	--年兽铃铛
	NINA_BELL = {
		monkey_king = "晓得俺老孙的厉害了吧",
		neza = "嘿嘿，以后哪吒不想飞的时候你就出来吧",
		myth_yutu = "以后不许胡闹了，知道吗",
		pigsy = "什么！你还想和俺抢吃的",
		white_bone = "蠢钝愚兽，可惜难以炼化",
		yangjian = "孽畜，饶你一命",
		yama_commissioners  = "额，它好像吐了",		
		madameweb ="吃了我的儿郎，就得",
	},
	--串天候
	MINIFLARE_MYTH = {
		monkey_king = "俺老孙飞的可比你高多了",
	},

	--桃木手杖
	CANE_PEACH = {
		monkey_king = "快给俺老孙结几个果子",
		neza = "老人家才用拐杖呢",
		myth_yutu = "小棍子你能变成胡萝卜田吗",
		pigsy = "你都给大师兄了，凭什么不给俺",
		white_bone = "虽有仙气，却已经消散八分",
		yangjian = "夸父也是一个伟人",
		yama_commissioners  = "这旺盛的活力，似有长春不老之意",		
		madameweb ="若是能带回我那花园倒是不错",
	},

	--被推倒的树根
	MYTH_PLANT_INFANTREE_TRUNK = {
		monkey_king = "看俺老孙干嘛，俺老孙可不认识这个",
		neza = "可怜叶落根出土，道人断绝草还丹",
		myth_yutu = "月华浇灌也难回天命",
		pigsy = "俺还想再尝尝人参果那味呢",
		white_bone = "如此仙根！是谁暴殄天物",
		yangjian = "可惜，杨戬法力救不回它",
		yama_commissioners  = "嘘，万寿观那位听见阎王爷也保不住你",		
		madameweb ="了无生机，鸡肋之物",
	},

	--被推倒的树根
	MYTH_PLANT_INFANTREE_TRUNK = {
		monkey_king = "看俺老孙干嘛，俺老孙可不认识这个",
		neza = "可怜叶落根出土，道人断绝草还丹",
		myth_yutu = "月华浇灌也难回天命",
		pigsy = "俺还想再尝尝人参果那味呢",
		white_bone = "如此仙根！是谁暴殄天物",
		yangjian = "可惜，杨戬法力救不回它",
		yama_commissioners  = "嘘，万寿观那位听见阎王爷也保不住你",		
		madameweb ="了无生机，鸡肋之物",
	},
	--复苏仙树
	MYTH_PLANT_INFANTREE_MEDIUM = {
		monkey_king = "俺老孙有的是方子",
		neza = "百折磨难，总算有点效果了",
		myth_yutu = "玉露润泽，春芽冬发",
		pigsy = "俺老猪来给它一泡大肥",
		white_bone = "枝青果出，奴家再来",
		yangjian = "复原如此当是奇迹",
		yama_commissioners  = "仙根岂是俗物",		
		madameweb ="灵气晕染，时间快到了",
	},
	--仙树
	MYTH_PLANT_INFANTREE = {
		monkey_king = "见到俺老孙可不要发抖哦",
		neza = "这比王母娘娘的蟠桃树还要高大",
		myth_yutu = "广寒宫的月桂也没有如此高大",
		pigsy = "再给俺一个果子尝尝味道",
		white_bone = "待到瓜熟蒂落之日",
		yangjian = "若有机会看看可否向大仙讨要一枚果子",
		yama_commissioners  = "这还能活也是怪哉",		
		madameweb ="我这蛛丝也不是五行之属",
	},
	--人生果
	MYTH_INFANT_FRUIT = {
		monkey_king = "俺老孙自有长生之法",
		neza = "哪吒也只曾听闻此等仙果",
		myth_yutu = "还是...等嫦娥姐姐...来要吧",
		pigsy = "再尝一个？",
		white_bone = "长生触手可及",
		yangjian = "若食之，可免调和龙虎，捉坎填离的功夫",
		yama_commissioners  = "俺就是想尝尝味道",		
		madameweb ="草还丹，丹霞缭绕的宝物",
	},
	--金击子
	MYTH_GOLD_STAFF = {
		monkey_king = "俺老孙的金箍棒也是五金之属",
		neza = "哪吒的火尖枪可比这好用多了",
		myth_yutu = "梆梆两下，人参果就下俩了",
		pigsy = "借俺悄悄一用",
		white_bone = "奴家只要一下",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "这十足赤金，人家居然只是拿来摘果子",		
		madameweb ="我这蛛丝不知可否牵下",
	},
	--判官笔
	COMMISSIONER_BOOK = {
		monkey_king = "哟哟，咋查不到俺老孙的名字？",
		white_bone = "死又何妨，天地游荡",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "生死簿可不该出现在这",		
	},
	COMMISSIONER_FLYBOOK = {
		monkey_king = "哟哟，咋查不到俺老孙的名字？",
		white_bone = "死又何妨，天地游荡",
		yangjian = "未经允许，不可窃之",
		yama_commissioners  = "生死簿可不该出现在这",		
	},
	--孟婆汤
	COMMISSIONER_MPT = {
		monkey_king = "休想骗俺老孙喝下这孟婆汤",
		neza = "有很多美好的记忆不能忘记",
		myth_yutu = "啊，味道怪怪的，我不要喝",
		pigsy = "俺尝尝..嗝，俺再尝尝..嗝，俺...",
		white_bone = "我的苦痛与不甘，岂能被你洗去",
		yangjian = "往日不可追，记忆却不能忘却",
		yama_commissioners  = "老范，你那脑袋瓜子不喝也记不住多少",		
		madameweb ="本座还没到轮回的时候",	
	},
}

for k, v in pairs(speech) do
    for k1, v1 in pairs(v) do
        STRINGS.CHARACTERS[string.upper(k)].DESCRIBE[k1] = v1
    end
end

for k, v in pairs(newitems) do
    for k1, v1 in pairs(v) do
        STRINGS.CHARACTERS[string.upper(k1)].DESCRIBE[k] = v1
    end
end
