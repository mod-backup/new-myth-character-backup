
--替换血量显示
AddClassPostConstruct("widgets/healthbadge", function(self)
	if self.owner and self.owner.prefab == "yama_commissioners" then
        self.owner:DoTaskInTime(0.1,function()
            if self.circleframe then
                self.circleframe:GetAnimState():OverrideSymbol("icon", "yama_status_health", "icon")
            end
            if self.anim then
                self.anim:GetAnimState():SetMultColour(75 / 255,139 / 255,138 / 255,1)
            end
        end)
    end
end)

--升级材料
GLOBAL.CONSTRUCTION_PLANS["myth_yama_statue1"] = { Ingredient("boards", 3), Ingredient("charcoal", 6), Ingredient("beeswax", 2), Ingredient("goldnugget", 10) }
GLOBAL.CONSTRUCTION_PLANS["myth_yama_statue2"] = { Ingredient("boards", 6), Ingredient("livinglog", 12), Ingredient("marble", 4), Ingredient("driftwood_log", 4) }
GLOBAL.CONSTRUCTION_PLANS["myth_yama_statue3"] = { Ingredient("foliage", 16), Ingredient("dug_marsh_bush", 6), Ingredient("soul_ghast", 20), Ingredient("soul_specter", 20) }


--靠近幽灵不掉san
local ghosts = {"ghost"}
for _,v in ipairs(ghosts) do
    AddPrefabPostInit(v,function(inst)
        if inst.components.sanityaura then
            local old = inst.components.sanityaura.GetAura
            inst.components.sanityaura.GetAura = function(self, observer)
                if observer and observer.prefab == "yama_commissioners" then
                    return 0
                end
                return old(self, observer)
            end
        end
    end)
end

--可以作祟物品
local function DefaultRangeCheck(doer, target)
    if target == nil then
        return
    end
    local target_x, target_y, target_z = target.Transform:GetWorldPosition()
    local doer_x, doer_y, doer_z = doer.Transform:GetWorldPosition()
    local dst = distsq(target_x, target_z, doer_x, doer_z)
    return dst <= 16
end
local YAMAHAUNT = Action({priority = 2,distance = 6})
YAMAHAUNT.id = "YAMAHAUNT"
YAMAHAUNT.strfn = function(act)
    return act.target ~= nil and act.target:HasTag("pass_commissioner") and "PASS" or "HAUNT"
end
--
YAMAHAUNT.fn = function(act)
    if act.target ~= nil and act.target:HasTag("pass_commissioner") then
        local exchangedata = {
            pass_commissioner_ylw = "pass_commissioner_wz",
            pass_commissioner_wz = "pass_commissioner_zk",
            pass_commissioner_zk = "pass_commissioner_cj",
            pass_commissioner_cj = "pass_commissioner_lzd",
            pass_commissioner_lzd = "pass_commissioner_mp",
            pass_commissioner_mp = "pass_commissioner_ylw",
        }
        local newpass = exchangedata[act.target.prefab] or "pass_commissioner_ylw"
        local isactived = act.target.isactived
        newpass = ReplacePrefab(act.target, newpass)
        if newpass ~= nil then
            if isactived then --如果之前是激活状态，继续激活状态
                newpass:DoTaskInTime(0.5,function()
                    newpass.fn_mythuse(newpass, nil, true)
                end)
            end
            newpass:SpawnChild("commissioner_teleport_small")
        end
        return true
    end
    return ACTIONS.HAUNT.fn(act)
end
AddAction(YAMAHAUNT)
AddComponentAction("SCENE", "hauntable" , function(inst, doer, actions)
    if doer and doer.replica.inventory and doer.replica.inventory:EquipHasTag("pennant_commissioner") and not (inst:HasTag("haunted") or inst:HasTag("catchable")) then
        table.insert(actions, ACTIONS.YAMAHAUNT)
    end
end)
AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.YAMAHAUNT, "use_pennant_commissioner"))
AddStategraphActionHandler("wilson_client", ActionHandler(ACTIONS.YAMAHAUNT, "use_pennant_commissioner"))

STRINGS.ACTIONS.YAMAHAUNT = {
    PASS = STRINGS.ACTIONS.CYCLE.GENERIC,
    HAUNT = STRINGS.ACTIONS.HAUNT,
}


AddComponentAction("SCENE", "hauntable" , function(inst, doer, actions)
    if doer and doer.replica.inventory and doer.replica.inventory:EquipHasTag("pennant_commissioner") and not (inst:HasTag("haunted") or inst:HasTag("catchable")) then
        table.insert(actions, ACTIONS.YAMAHAUNT)
    end
end)

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

AddPrefabPostInit("wasphive",function(inst)
    if inst.components.playerprox then
        local old_onnear = inst.components.playerprox.onnear
        inst.components.playerprox.onnear = function(inst,target)
            if target and target.prefab == "yama_commissioners" and IsFlying(target) then
                return 
            end
            if old_onnear then
                old_onnear(inst,target)
            end
        end
    end
end)

--可以掩埋骨架
local skeletons = {"skeleton","skeleton_player"}
local function onuse(inst,doer)
    if inst and inst:IsValid() and doer then
        local fx = SpawnPrefab("collapse_small")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        SpawnPrefab("yama_gravestone").Transform:SetPosition(inst.Transform:GetWorldPosition())
        if doer.Give_Yama_Souls then
            doer:Give_Yama_Souls(5,doer:GetPosition(),inst,true)
        end
        inst:Remove()
        return true
    end
end

--招魂幡作祟几率增加
local new_chance = {
    HAUNT_CHANCE_OFTEN = .95,
    HAUNT_CHANCE_HALF = .75,
    HAUNT_CHANCE_OCCASIONAL = .5,
    HAUNT_CHANCE_RARE = .2,
    HAUNT_CHANCE_VERYRARE = .01,
    HAUNT_CHANCE_SUPERRARE = .005,    
}
AddComponentPostInit("hauntable", function(self)
	local oldDoHaunt = self.DoHaunt
	function self:DoHaunt(doer)
        local change = {}
        if doer and doer.components.inventory and doer.components.inventory:EquipHasTag("pennant_commissioner") then
            for k,v in pairs(new_chance) do 
                change[k] = TUNING[k] --记录旧的
                TUNING[k] = v --赋值新的
            end
        end
        local old = oldDoHaunt(self,doer)
        if next(change) ~= nil then
            for k,v in pairs(change) do 
                TUNING[k] = v --还原旧的
            end       
        end
        return old
    end
end)

for _,v in ipairs(skeletons) do
    AddPrefabPostInit(v, function(inst)
        inst.MYTH_USE_TYPE = "SKELETON"
        inst.myth_use_needtag = "yama_commissioners"
        inst.onusesgname = "dolongaction"
        if not TheWorld.ismastersim then
            return inst
        end
        inst.yama_soul = "soul_specter_spawn"
        inst:AddComponent("myth_use_inventory")
        inst.components.myth_use_inventory.canusescene = true
        inst.components.myth_use_inventory:SetOnUseFn(onuse)
    end)
end

--无法挖掘坟墓
local mounds = {"mound","yama_mound"}
for _,v in ipairs(mounds) do
    AddPrefabPostInit(v, function(inst)
        if inst.components.workable then
            local old_work = inst.components.workable.WorkedBy
            inst.components.workable.WorkedBy = function(self,worker, numworks)
                if worker and worker.prefab == "yama_commissioners" then
                    worker.components.talker:Say(STRINGS.YAMA_CANTDIGMOUNT)
                    return
                end
                return old_work(self,worker, numworks)
            end
        end
    end)
end
--------------------------------------------------------------------------
--[[ 镇魂令 sg ]]
--------------------------------------------------------------------------

AddStategraphState("wilson", State{
    name = "use_token_commissioner",
    tags = { "busy", "doing", "nodangle" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        local build = 'token_commissioner'
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.invobject or nil
        if target ~= nil then
            build = target.AnimState:GetBuild()
        end
        inst.AnimState:OverrideSymbol("spider_repellent", build, "spider_repellent")

        inst.AnimState:PlayAnimation("useitem_pre")
        inst.AnimState:PushAnimation("spider_repellent", false)
        inst.AnimState:PushAnimation("useitem_pst", false)

        inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_attack_LP", "usetoken")
    end,

    timeline =
    {
        TimeEvent(3*FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
        TimeEvent(17*FRAMES, function(inst)
            -- inst:PerformBufferedAction()
            inst.SoundEmitter:KillSound("usetoken")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        --复原使用驱赶盒子时动作的贴图（虽然无常并不能用驱赶盒子）
        inst.AnimState:OverrideSymbol("spider_repellent", "player_spider_repellent", "spider_repellent")

        inst.SoundEmitter:KillSound("usetoken")
    end,
})

AddStategraphState("wilson_client", State{
    name = "use_token_commissioner",
    tags = { "doing", "playing" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("useitem_pre")
        inst.AnimState:PushAnimation("useitem_lag", false)

        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(2)
    end,

    onupdate = function(inst)
        if inst:HasTag("doing") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle")
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle")
    end,
})

--------------------------------------------------------------------------
--[[ 招魂幡 sg ]]
--------------------------------------------------------------------------

AddStategraphState("wilson", State{
    name = "use_pennant_commissioner",
    tags = { "busy", "doing", "nodangle" },

    onenter = function(inst)
        if inst.components.rider:IsRiding() then --骑牛无法使用招魂幡
            inst.sg:GoToState("idle")
            return
        end
        inst.components.locomotor:Stop()

        local build = 'pennant_commissioner'
        local buffaction = inst:GetBufferedAction()
        local hand = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        local target = buffaction ~= nil and buffaction.invobject or nil
        if target ~= nil then
            build = target.AnimState:GetBuild()
        elseif hand ~= nil then
            build = hand.AnimState:GetBuild()
        end

        inst.AnimState:OverrideSymbol("spider_repellent", build, "spider_repellent")

        inst.AnimState:PlayAnimation("useitem_pre")
        inst.AnimState:PushAnimation("spider_repellent", false)
        inst.AnimState:PushAnimation("useitem_pst", false)

        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/chain_foley")
        inst.SoundEmitter:PlaySound("moonstorm/creatures/boss/alterguardian2/spell_cast")
    end,

    timeline =
    {
        TimeEvent(17 * FRAMES, function(inst)
            inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/chain_foley")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        --复原使用驱赶盒子时动作的贴图（虽然无常并不能用驱赶盒子）
        inst.AnimState:OverrideSymbol("spider_repellent", "player_spider_repellent", "spider_repellent")
    end,
})

AddStategraphState("wilson_client", State{
    name = "use_pennant_commissioner",
    tags = { "doing", "playing" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("useitem_pre")
        inst.AnimState:PushAnimation("useitem_lag", false)

        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(2)
    end,

    onupdate = function(inst)
        if inst:HasTag("doing") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle")
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle")
    end,
})

--------------------------------------------------------------------------
--[[ 摄魂铃 sg ]]
--------------------------------------------------------------------------

local function PlayBellSound(inst)
    -- inst.SoundEmitter:PlaySound("yotb_2021/common/cow_bell")
    if not inst.sg.statemem.use_bell_lotus then
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/chain_foley")
        inst.SoundEmitter:PlaySound("dontstarve/common/together/celestial_orb/active", nil, 0.3)
    end
end

local function PlayBellSound2(inst)
    if not inst.sg.statemem.use_bell_lotus then
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/chain_foley")
    end
end

AddStategraphState("wilson", State{
    name = "use_bell_commissioner",
    tags = { "doing", "playing", "busy" },

    onenter = function(inst)
        if inst.components.rider:IsRiding() then --骑牛无法使用招魂幡
            inst.sg:GoToState("idle")
            return
        end
        inst.components.locomotor:Stop()

        local build = "bell_commissioner"
        local hand = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.invobject or nil
        if target ~= nil then
            build = target.AnimState:GetBuild()
        elseif hand ~= nil then
            build = hand.AnimState:GetBuild()
        end
        if build == "bell_commissioner" then
            inst.AnimState:PlayAnimation("action_uniqueitem_pre")
            inst.AnimState:PushAnimation("cowbell", false)

            inst.AnimState:OverrideSymbol("cbell", build, "cbell")
            inst.AnimState:Show("ARM_normal")
        else
            inst.AnimState:OverrideSymbol("use_bell_lotus", "swap_bell_commissioner_lotus", "swap_object")
            inst.AnimState:PlayAnimation("use_bell_lotus_pre")
            inst.AnimState:PushAnimation("use_bell_lotus", false)
            inst.AnimState:Show("ARM_carry")
            inst.sg.statemem.use_bell_lotus = true 
            local colour ={ 74/255, 165/255, 255/255 }
            inst.sg.statemem.stafffx = SpawnPrefab("staffcastfx")
            inst.sg.statemem.stafffx.entity:SetParent(inst.entity)
            inst.sg.statemem.stafffx:SetUp(colour)

            inst.sg.statemem.stafflight = SpawnPrefab("staff_castinglight")
            inst.sg.statemem.stafflight.Transform:SetPosition(inst.Transform:GetWorldPosition())
            inst.sg.statemem.stafflight:SetUp(colour, 1.9, .33)
            --加速
            inst.sg.statemem.stafffx.AnimState:SetDeltaTimeMultiplier(2)
            inst.AnimState:SetDeltaTimeMultiplier(2)
        end
    end,

    timeline =
    {
        TimeEvent(1*FRAMES, PlayBellSound2),
        TimeEvent(3*FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
        TimeEvent(10*FRAMES, PlayBellSound2),
        TimeEvent(13*FRAMES, function(inst)
            if inst.sg.statemem.use_bell_lotus then
                inst.SoundEmitter:PlaySound( "dontstarve/wilson/use_gemstaff")
            end
        end),
        TimeEvent(16*FRAMES, function(inst) --摇第一下铃铛：中部
            PlayBellSound(inst)
            --if not inst.sg.statemem.use_bell_lotus then
                -- inst:PerformBufferedAction()
                inst.sg.statemem.stafffx = nil --Can't be cancelled anymore
                inst.sg.statemem.stafflight = nil --Can't be cancelled anymore
            --end
        end),

        TimeEvent(30*FRAMES, PlayBellSound2),
        TimeEvent(35*FRAMES, PlayBellSound), --摇第二下铃铛：中部
        TimeEvent(38*FRAMES, function(inst) --摇第二下铃铛：底部--直接跳到第三下摇铃铛底部的时间去
            if not inst.sg.statemem.use_bell_lotus then
                inst.AnimState:SetTime(1.967)
            end
        end),
        TimeEvent(58*FRAMES, PlayBellSound2),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() and not inst.sg.statemem.use_bell_lotus then
                inst.sg:GoToState("idle")
            end
        end),
        EventHandler("animqueueover", function(inst)
            if inst.AnimState:AnimDone() and inst.sg.statemem.use_bell_lotus then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        inst.AnimState:SetDeltaTimeMultiplier(1)
        if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) ~= nil then
            inst.AnimState:Show("ARM_carry")
            inst.AnimState:Hide("ARM_normal")
        elseif inst.sg.statemem.use_bell_lotus then
            inst.AnimState:Hide("ARM_carry")
            inst.AnimState:Show("ARM_normal")
        end
    end,
})

AddStategraphState("wilson_client", State{
    name = "use_bell_commissioner",
    tags = { "doing", "playing" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        local build = "bell_commissioner"
        local hand = inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.invobject or nil
        if target ~= nil then
            build = target.AnimState:GetBuild()
        elseif hand ~= nil then
            build = hand.AnimState:GetBuild()
        end
        if build == "bell_commissioner" then
            inst.AnimState:PlayAnimation("action_uniqueitem_pre")
            inst.AnimState:PushAnimation("action_uniqueitem_lag", false)
        else
            inst.AnimState:PlayAnimation("use_bell_lotus_pre")
            inst.AnimState:PushAnimation("use_bell_lotus", false)
        end

        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(2)
    end,

    onupdate = function(inst)
        if inst:HasTag("doing") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle")
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle")
    end,
})

--------------------------------------------------------------------------
--[[ 黑白无常互相转换 sg ]]
--------------------------------------------------------------------------

AddStategraphState("wilson", State{
    name = "exchange_commissioner",
    tags = { "doing","yama_transform"},

    onenter = function(inst)
        if inst.components.inventory:IsHeavyLifting() then
            inst.components.inventory:DropItem(
                inst.components.inventory:Unequip(EQUIPSLOTS.BODY),
                true,
                true
            )
        end
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation( inst:HasTag("commissioner_white") and "switch_white_myth" or "switch_balck_myth")
    end,

    timeline =
    {
        TimeEvent(15*FRAMES, function(inst)
            inst.sg:AddStateTag("busy")
            inst.sg:AddStateTag("nointerrupt")
            inst.sg:AddStateTag("nomorph")
        end),
        TimeEvent(20*FRAMES, function(inst)
            if inst.components.yama_transform ~= nil then
                inst.components.yama_transform:Change()
            end
        end),
        TimeEvent(28*FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nointerrupt")
            inst.sg:RemoveStateTag("nomorph")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    -- onexit = function(inst) end,
})

AddStategraphState("wilson_client", State{
    name = "exchange_commissioner",
    tags = { "doing" },

    onenter = function(inst)
        inst.components.locomotor:Stop()

        inst.AnimState:PlayAnimation(inst:HasTag("commissioner_white") and "switch_white_myth" or "switch_balck_myth")
    end,

    timeline =
    {
        TimeEvent(15*FRAMES, function(inst)
            inst.sg:AddStateTag("busy")
            inst.sg:AddStateTag("nointerrupt")
            inst.sg:AddStateTag("nomorph")
        end),
        TimeEvent(20*FRAMES, function(inst)
            inst:ClearBufferedAction()
        end),
        TimeEvent(28*FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nointerrupt")
            inst.sg:RemoveStateTag("nomorph")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    -- onexit = function(inst) end,
})

--[[test
TheInput:AddKeyUpHandler(KEY_V, function()
    local inst = ThePlayer
    if
        inst.sg:HasStateTag("busy") or
        inst:HasTag("busy") or inst:HasTag("playerghost") or
        (inst.replica.rider ~= nil and inst.replica.rider:IsRiding())
    then
        return
    end
    inst.sg:GoToState("exchange_commissioner")
end)]]

--------------------------------------------------------------------------
--[[ 让玩家无法主动丢弃善恶魂 ]]
--------------------------------------------------------------------------

local dropfn_old = ACTIONS.DROP.fn
ACTIONS.DROP.fn = function(act)
    if act.invobject ~= nil and act.invobject:HasTag("soul_lost") then
        return nil
    end

    return dropfn_old(act)
end

STRINGS.ACTIONS.DROP.SOULLOST = " "
local dropstrfn_old = ACTIONS.DROP.strfn
ACTIONS.DROP.strfn = function(act)
    if act.invobject ~= nil and act.invobject:HasTag("soul_lost") then
        return "SOULLOST"
    end

    return dropstrfn_old(act)
end