local assets =
{
    Asset("ANIM", "anim/madameweb_spider_warrior.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_spider_warrior.xml"),
}

local beeassets =
{
    Asset("ANIM", "anim/madameweb_bee.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_bee.xml"),
}


--小小小蜘蛛？
local brain = require "brains/madameweb_spiderbrain"

local function ShouldAcceptItem(inst, item, giver)
    return giver:HasTag("madameweb") and 
        (inst.components.eater:CanEat(item) or 
        (item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD))
end

local function OnGetItemFromPlayer(inst, giver, item)
    if inst.components.eater:CanEat(item) then
        inst.components.eater:Eat(item)
        if inst.components.inventoryitem.owner ~= nil then
            inst.sg:GoToState("idle")
        else
            inst.sg:GoToState("eat", true)
        end
        if inst.components.combat.target == giver then
            inst.components.combat:SetTarget(nil)
        elseif giver.components.leader ~= nil and
            inst.components.follower ~= nil and  inst.components.follower.leader == nil  then
            if giver.components.minigame_participator == nil then
                giver:PushEvent("makefriend")
                giver.components.leader:AddFollower(inst)
            end
        end
    elseif item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD then
        local current = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if current ~= nil then
            inst.components.inventory:DropItem(current)
        end
        inst.components.inventory:Equip(item)
        inst.AnimState:Show("hat")
    end
end

local function OnRefuseItem(inst, item)
    inst.sg:GoToState("taunt")
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function HasFriendlyLeader(inst, target)
    local leader = inst.components.follower.leader
    local target_leader = (target.components.follower ~= nil) and target.components.follower.leader or nil
    
    if leader ~= nil and target_leader ~= nil then

        if target_leader.components.inventoryitem then
            target_leader = target_leader.components.inventoryitem:GetGrandOwner()
            -- Don't attack followers if their follow object has no owner
            if target_leader == nil then
                return true
            end
        end

        local PVP_enabled = TheNet:GetPVPEnabled()
        return leader == target or (target_leader ~= nil 
                and (target_leader == leader or (target_leader:HasTag("player") 
                and not PVP_enabled))) or
                (target.components.domesticatable and target.components.domesticatable:IsDomesticated() 
                and not PVP_enabled) or
                (target.components.saltlicker and target.components.saltlicker.salted
                and not PVP_enabled)
    
    elseif target_leader ~= nil and target_leader.components.inventoryitem then
        -- Don't attack webber's chester
        target_leader = target_leader.components.inventoryitem:GetGrandOwner()
        return target_leader ~= nil and target_leader:HasTag("madameweb")
    end

    return false
end
local TARGET_MUST_TAGS = { "_combat", "character" }
local TARGET_CANT_TAGS = { "madameweb", "madameweb_baby", "INLIMBO" }
local function FindTarget(inst, radius)
    if not inst.no_targeting then
        return FindEntity(
            inst,
            SpringCombatMod(radius),
            function(guy)
                return (not inst.bedazzled and (not guy:HasTag("monster") or guy:HasTag("player")))
                    and inst.components.combat:CanTarget(guy)
                    and not (inst.components.follower ~= nil and inst.components.follower.leader == guy)
                    and not HasFriendlyLeader(inst, guy)
                    and not (inst.components.follower.leader ~= nil and inst.components.follower.leader:HasTag("player") 
                        and guy:HasTag("player") and not TheNet:GetPVPEnabled())
            end,
            TARGET_MUST_TAGS,
            TARGET_CANT_TAGS
        )
    end
end

local function keeptargetfn(inst, target)
    return target ~= nil
         and target.components.combat ~= nil
         and target.components.health ~= nil
         and not target.components.health:IsDead()
         and not (inst.components.follower ~= nil and
                 (inst.components.follower.leader == target or inst.components.follower:IsLeaderSame(target)))
end

local function BasicWakeCheck(inst)
    return inst.components.combat:HasTarget()
        or (inst.components.homeseeker ~= nil and inst.components.homeseeker:HasHome())
        or inst.components.burnable:IsBurning()
        or inst.components.freezable:IsFrozen()
        or inst.components.health.takingfiredamage
        or inst.components.follower:GetLeader() ~= nil
        or inst.summoned
end

local function ShouldSleep(inst)
    return TheWorld.state.iscaveday and not BasicWakeCheck(inst)
end

local function ShouldWake(inst)
    return not TheWorld.state.iscaveday
        or BasicWakeCheck(inst)
        or FindTarget(inst, TUNING.SPIDER_WARRIOR_WAKE_RADIUS) ~= nil
end

local function CalcSanityAura(inst, observer)
    if observer:HasTag("madameweb") or 
    (inst.components.follower.leader ~= nil and inst.components.follower.leader:HasTag("madameweb")) then
        return 0
    end
    return inst.components.sanityaura.aura
end


local function OnAttacked(inst, data)
    if inst.components.follower.leader ~= nil and inst.components.follower.leader == data.attacker then
        return
    end
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude)
        return dude.components.health and
            not dude.components.health:IsDead()
            and dude:HasTag("madameweb_baby")
            and dude.components.follower ~= nil
            and dude.components.follower.leader == inst.components.follower.leader
    end, 10)
end

local function OnTrapped(inst, data)
    inst.components.inventory:DropEverything()
end

local function OnDropped(inst, data)
    if ShouldWake(inst) then
        inst.sg:GoToState("idle")
    elseif ShouldSleep(inst) then
        inst.sg:GoToState("sleep")
    end
end

local function SetHappyFace(inst, is_happy)
    inst.AnimState:ClearOverrideSymbol("face")
end

local DIET = { FOODTYPE.MEAT }
local BASE_PATHCAPS = { ignorecreep = true }
local function create_common(bank, build, tag, common_init, extra_data)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(1.5, .5)
    inst.Transform:SetFourFaced()

    inst:AddTag("cavedweller")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("scarytoprey")
    inst:AddTag("canbetrapped")
    inst:AddTag("smallcreature")
    inst:AddTag("notraptrigger")
    inst:AddTag("spider") --不想被别人控制 不要这个了
    inst:AddTag("drop_inventory_onpickup")
    inst:AddTag("drop_inventory_onmurder")
    
    if tag ~= nil then
        inst:AddTag(tag)
    end

    --trader (from trader component) added to pristine state for optimization
    inst:AddTag("trader")

    inst.AnimState:SetBank(bank)
    inst.AnimState:SetBuild(build)
    inst.AnimState:PlayAnimation("idle")

    MakeFeedableSmallLivestockPristine(inst)
    
    if common_init ~= nil then
        common_init(inst)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    -- locomotor must be constructed before the stategraph!
    inst:AddComponent("locomotor")
    inst.components.locomotor:SetSlowMultiplier( 1 )
    inst.components.locomotor:SetTriggersCreep(false)
    inst.components.locomotor.pathcaps = (extra_data and extra_data.pathcaps) or BASE_PATHCAPS
    -- boat hopping setup
    inst.components.locomotor:SetAllowPlatformHopping(true)
    
    inst:AddComponent("embarker")
    inst:AddComponent("drownable")

    inst:SetStateGraph((extra_data and extra_data.sg) or "SGmadameweb_spider_warrior")

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:AddRandomLoot("spidergland", 1)
    inst.components.lootdropper.numrandomloot = 1

    ---------------------
    MakeMediumBurnableCharacter(inst, "body")
    MakeMediumFreezableCharacter(inst, "body")
    inst.components.burnable.flammability = TUNING.SPIDER_FLAMMABILITY
    ---------------------

    inst:AddComponent("health")
    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "body"
    inst.components.combat:SetKeepTargetFunction(keeptargetfn)
    --inst.components.combat:SetOnHit(SummonFriends)

    inst:AddComponent("follower")

    inst:AddComponent("sleeper")
    inst.components.sleeper.watchlight = true
    inst.components.sleeper:SetResistance(2)
    inst.components.sleeper:SetSleepTest(ShouldSleep)
    inst.components.sleeper:SetWakeTest(ShouldWake)
    ------------------

    inst:AddComponent("knownlocations")

    ------------------

    inst:AddComponent("eater")
    inst.components.eater:SetDiet(DIET, DIET)
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
    inst.components.eater:SetCanEatRawMeat(true)

    ------------------

    inst:AddComponent("inspectable")

    ------------------
    inst:AddComponent("tradable")

    inst:AddComponent("inventory")

    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    inst.components.trader:SetAbleToAcceptTest(ShouldAcceptItem)
    inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.onrefuse = OnRefuseItem
    inst.components.trader.deleteitemonaccept = false

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.nobounce = true
    inst.components.inventoryitem.canbepickedup = false
    inst.components.inventoryitem.canbepickedupalive = true
    inst.components.inventoryitem:SetSinks(true)

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura
    
    MakeFeedableSmallLivestock(inst, TUNING.SPIDER_PERISH_TIME)
    MakeHauntablePanic(inst)

    inst:SetBrain((extra_data and extra_data.brain) or brain)

    inst:ListenForEvent("attacked", OnAttacked)
    
    inst:ListenForEvent("ontrapped", OnTrapped)

    inst:ListenForEvent("ondropped", OnDropped)

    --inst:ListenForEvent("gotosleep", OnGoToSleep)
    --inst:ListenForEvent("onwakeup", OnWakeUp)

    --inst:ListenForEvent("onpickup", OnPickup)

    --inst:WatchWorldState("iscaveday", OnIsCaveDay)
    --OnIsCaveDay(inst, TheWorld.state.iscaveday)

    inst.build = build
    
    inst.SetHappyFace = (extra_data and extra_data.SetHappyFaceFn) or SetHappyFace

    return inst
end

local function WarriorRetarget(inst)
    return FindTarget(inst, TUNING.SPIDER_WARRIOR_TARGET_DIST)
end

local function create_warrior()
    local inst = create_common("spider", "madameweb_spider_warrior", "madameweb_baby")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH)

    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_WARRIOR_DAMAGE)
    inst.components.combat:SetAttackPeriod(0.5*TUNING.SPIDER_WARRIOR_ATTACK_PERIOD + math.random() * 2)
    inst.components.combat:SetRange(TUNING.SPIDER_WARRIOR_ATTACK_RANGE, TUNING.SPIDER_WARRIOR_HIT_RANGE)
    inst.components.combat:SetRetargetFunction(2, WarriorRetarget)

    inst.components.locomotor.walkspeed = TUNING.SPIDER_WARRIOR_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.SPIDER_WARRIOR_RUN_SPEED

    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_spider_warrior.xml"

    Myth_MakePoisonable(inst)
    --无法攻击盘丝和同伴
    local old = inst.components.combat.IsValidTarget
    inst.components.combat.IsValidTarget = function(self,target)
        if target and (target:HasTag("madameweb")  or target:HasTag("madameweb_baby") ) then
            return false
         end
        return old(self,target)
    end

    return inst
end

local bee = Prefabs.bee.fn
local function worker_OnIsSpring(inst, isspring)
    inst:DoTaskInTime(0,function()
        inst.AnimState:SetBuild("madameweb_bee")
        inst.components.inventoryitem:ChangeImageName("madameweb_bee")
    end)
end
local function beefn()
    
    local inst = bee()

    inst.AnimState:SetBuild("madameweb_bee")

    inst:AddTag("madameweb_baby")

    if not TheWorld.ismastersim then
        return inst
    end

    Myth_MakePoisonable(inst)

    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_bee.xml"
    --无法攻击盘丝和同伴
    local old = inst.components.combat.IsValidTarget
    inst.components.combat.IsValidTarget = function(self,target)
        if target and (target:HasTag("madameweb")  or target:HasTag("madameweb_baby") ) then
            return false
         end
        return old(self,target)
    end

    inst:WatchWorldState("isspring", worker_OnIsSpring)
    if TheWorld.state.isspring then
        worker_OnIsSpring(inst, true)
    end

    return inst
end

return Prefab("madameweb_spider_warrior", create_warrior, assets),Prefab("madameweb_bee", beefn, beeassets)
