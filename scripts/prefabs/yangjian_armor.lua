


local function lantern_onremovefx(fx)
    fx._lantern._lit_fx_inst = nil
end
local function off(inst)
    local fx = inst._lit_fx_inst
    if fx ~= nil then
        if fx.KillFX ~= nil then
            inst._lit_fx_inst = nil
            inst:RemoveEventCallback("onremove", lantern_onremovefx, fx)
            --fx:RemoveEventCallback("enterlimbo", lantern_enterlimbo, inst)
            fx._lastpos = fx._lastpos or fx:GetPosition()
            fx.entity:SetParent(nil)
            if fx.Follower ~= nil then
                fx.Follower:FollowSymbol(0, "", 0, 0, 0)
            end
            fx.Transform:SetPosition(fx._lastpos:Get())
            fx:KillFX()
        else
            fx:Remove()
        end
    end
end
local function onequip(inst, owner)
	if owner.components.freezable then
		owner.components.freezable.qingyuan = true
	end
    if  inst.components.myth_itemskin.skin:value() == "hawk" then
		if inst._lit_fx_inst == nil then
			inst._lit_fx_inst = SpawnPrefab("hawk_flower_fx_held")
			inst._lit_fx_inst._lantern = inst
			inst._lit_fx_inst.entity:AddFollower()
			inst:ListenForEvent("onremove", lantern_onremovefx, inst._lit_fx_inst)
		end
		inst._lit_fx_inst.entity:SetParent(owner.entity)
		inst._lit_fx_inst.Follower:FollowSymbol(owner.GUID, "swap_body", 0, -20, 0)
    end
	inst.components.myth_itemskin:OverrideSymbol(owner, "swap_body")
end

local function onunequip(inst, owner) 
    --if  inst.components.myth_itemskin.skin:value() == "hawk" then
    --    owner.AnimState:ClearOverrideSymbol("swap_body_tall")
    --else
        owner.AnimState:ClearOverrideSymbol("swap_body")
    --end
	off(inst)
	if owner.components.freezable then
		owner.components.freezable.qingyuan = false
	end
end

local assets =
{
	Asset("ANIM", "anim/yangjian_armor.zip"),
	Asset( "ATLAS", "images/inventoryimages/yangjian_armor.xml" ),
	Asset( "IMAGE", "images/inventoryimages/yangjian_armor.tex" ),
}

local function TemperatureChange(inst, data)
	local ambient_temp = TheWorld.state.temperature

	if ambient_temp >0 then
		inst.components.equippable.walkspeedmult = 1.1
	else
		inst.components.equippable.walkspeedmult =  1.25
	end
end

local function onpercent(inst,data)
	local percent =  data and data.percent or inst.components.armor.condition
	if percent <= 0 then 
		inst.components.armor.absorb_percent = 0 
	else
		inst.components.armor.absorb_percent = 0.935
	end
end

local function fn()
	local inst = CreateEntity()

                inst:AddTag("hide_percentage")
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddMiniMapEntity()

	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("yangjian_armor")
	inst.AnimState:SetBuild("yangjian_armor")
	inst.AnimState:PlayAnimation("idle_none")

	inst.MiniMapEntity:SetIcon("yangjian_armor.tex")
	
	MakeInventoryFloatable(inst, "small", 0.2, 0.80)

	inst:AddComponent("myth_itemskin")
    inst.components.myth_itemskin.character = 'yangjian'
    inst.components.myth_itemskin.prefab = 'yangjian_armor'
	inst.components.myth_itemskin:SetDefaultData{"black", "gold" ,"hawk"}

	inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("tradable")
	
	inst:AddComponent("inspectable")

	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = 'yangjian_armor'
	inst.components.inventoryitem.atlasname = "images/inventoryimages/yangjian_armor.xml"
	
	inst:AddComponent("equippable")
	inst.components.equippable.equipslot = EQUIPSLOTS.BODY
	inst.components.equippable:SetOnEquip(onequip)
	inst.components.equippable:SetOnUnequip(onunequip)
	inst.components.equippable.walkspeedmult = 1.25
                inst.components.equippable.dapperness = 5.5/60


	inst:AddComponent("armor")
	inst.components.armor:InitCondition(4000, 0.935)
                inst.components.armor.indestructible = true
		inst.components.armor.SetCondition =  function(self,amount)	
			if self.indestructible then
				return
			end
			self.condition = math.max(math.min(amount, self.maxcondition),0)
			self.inst:PushEvent("percentusedchange", { percent = self:GetPercent() })
		end	
		
	inst:AddComponent("waterproofer")
	inst.components.waterproofer:SetEffectiveness(TUNING.WATERPROOFNESS_ABSOLUTE)

	inst:WatchWorldState("temperature", TemperatureChange)
	TemperatureChange(inst)
	
	inst:ListenForEvent("percentusedchange", onpercent)
	onpercent(inst)	
		
	MakeHauntableLaunch(inst)

	return inst
end

return Prefab('yangjian_armor', fn, assets)