return
{
	ACTIONFAIL =
	{
        REPAIR =
        {
            WRONGPIECE = "Có vẻ cái này không đúng",		--化石骨架拼接错误
        },
        BUILD =
        {
            MOUNTED = "Huhu... tay ngắn quá không làm được!",		--建造失败（骑乘状态）
            HASPET = "Một cái đủ rồi",		--建造失败（已经有一个宠物了）
        },
		SHAVE =
		{
			AWAKEBEEFALO = "Ta không muốn đọ sức với bò đâu",		--给醒着的牛刮毛
			GENERIC = "Ôi chao! Sai rồi",		--刮牛毛失败
			NOBITS = "Làm thỏ không được tham lam",		--给没毛的牛刮毛
		},
		STORE =
		{
			GENERIC = "Sao không bỏ vào được?",		--存放东西失败
			NOTALLOWED = "Người ta không cho ta sử dụng",		--存放东西--不被允许
			INUSE = "Luôn phải có người xếp hàng, đúng không!",		--别人正在用箱子
            NOTMASTERCHEF = "Để đầu bếp làm đi",		--暂无注释
		},
        CONSTRUCT =
        {
            INUSE = "Phải đợi thôi",		--建筑正在使用
            NOTALLOWED = "Hiện không thể dùng",		--建筑不允许使用
            EMPTY = "Cần nguyên liệu chế tạo",		--建筑空了
            MISMATCH = "Không đúng?",		--升级套件错误（目前用不到）
        },
		RUMMAGE =
		{
			GENERIC = "Không được",		--打开箱子失败
			INUSE = "Tìm được món đồ gì ngon không?",		--打开箱子 正在使用
            NOTMASTERCHEF = "Để đầu bếp nấu đi",		--暂无注释
		},
		UNLOCK =
        {
        },
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "Chìa khóa này không đúng!",		--使用克劳斯钥匙
        	KLAUS = "Thời gian không đúng!",		--克劳斯
        },
		ACTIVATE =
		{
			LOCKED_GATE = "Ta cũng muốn vào đó!",		--远古钥匙
		},
        COOK =
        {
            GENERIC = "Ta không vui",		--做饭失败
            INUSE = "Ôi... thật là thơm",		--做饭失败-别人在用锅
            TOOFAR = "Ngươi muốn ta nấu ăn bằng mắt sao!",		--做饭失败-太远
        },
        START_CARRAT_RACE =
        {
            NO_RACERS = "Không bắt đầu được! Cần ít rau tươi!",		--暂无注释
        },
        FISH_OCEAN =
		{
			TOODEEP = "Lại gần đây, cá ơi!",		--暂无注释
		},
        OCEAN_FISHING_POND =
		{
			WRONGGEAR = "Cái ao không cần thứ này",		--暂无注释
		},
        GIVE =
        {
            GENERIC = "Hắn không cần",		--给予失败
            DEAD = "Hồn phách không dùng được",		--给予 -目标死亡
            SLEEPING = "Dậy đi! Ta có quà tặng ngươi!",		--给予--目标睡觉
            BUSY = "Bận đến không có thời gian nhận quà?",		--给予--目标正忙
            ABIGAILHEART = "Ôi... xin lỗi, ngươi đi bảo vệ muội muội của ngươi đi",		--给阿比盖尔 救赎之心
            GHOSTHEART = "Ơ... Ta không nghĩ nó có tác dụng",		--给鬼魂 救赎之心
            NOTGEM = "Không được",		--给的不是宝石
            WRONGGEM = "Râu ông nọ cắm cằm bà kia",		--给错了宝石
            NOTSTAFF = "Cần một món pháp khí mới có thể kích hoạt sức mạnh ánh trăng",		--给月石祭坛的物品不是法杖
            MUSHROOMFARM_NEEDSSHROOM = "Cần phải tìm được nấm sặc sỡ!",		--蘑菇农场需要蘑菇
            MUSHROOMFARM_NEEDSLOG = "Cần khúc gỗ sống",		--蘑菇农场需要活木
            SLOTFULL = "Đầy rồi",		--已经放了材料后，再给雕像桌子再给一个材料
            FOODFULL = "Có đồ đang nấu",		--没调用
            NOTDISH = "Bùm!!",		--没调用
            DUPLICATE = "Ô! Sớm đã lường trước rồi!",		--给雕像桌子已经学习过的图纸
            NOTSCULPTABLE = "Không thể nặn!",		--给陶艺圆盘的东西不对
            NOTATRIUMKEY = "Trông không đúng lắm",		--中庭钥匙不对
            CANTSHADOWREVIVE = "Không được",		--中庭仍在CD
            WRONGSHADOWFORM = "Trông cái đó không đúng...",		--没调用
            NOMOON = "Không có tác dụng, thiếu sức mạnh mặt trăng",		--洞穴里建造月石科技
			PIGKINGGAME_MESSY = "Phải xử lý trước...",		--猪王旁边有建筑，不能开始抢元宝
			PIGKINGGAME_DANGER = "Giờ không phải lúc chơi đùa!",		--危险，不能开始抢元宝
			PIGKINGGAME_TOOLATE = "Ôi... trời tối rồi",		--不是白天，不能开始抢元宝
        },
        GIVETOPLAYER =
        {
            FULL = "Hắn đã có nhiều rồi!",		--给玩家一个东西 -但是背包满了
            DEAD = "Người chết không dùng được",		--给死亡的玩家一个东西
            SLEEPING = "Dậy đi! Ta có đồ tặng ngươi nè!",		--给睡觉的玩家一个东西
            BUSY = "Cầm lấy đi! Cầm lấy đi!!",		--给忙碌的玩家一个东西
        },
        GIVEALLTOPLAYER =
        {
            FULL = "Bọn họ có nhiều rồi!",		--给人一组东西 但是背包满了
            DEAD = "Người chết không dùng được",		--给于死去的玩家一组物品
            SLEEPING = "Dậy đi! Ta có đồ tặng ngươi nè!",		--给于正在睡觉的玩家一组物品
            BUSY = "Cầm lấy đi! Cầm lấy đi!!",		--给于正在忙碌的玩家一组物品
        },
        WRITE =
        {
            GENERIC = "Viết chữ như thế nào vậy?",		--鞋子失败
            INUSE = "Để ta! Để ta!",		--写字 正在使用中
        },
        DRAW =
        {
            NOIMAGE = "Ta quên phải vẽ gì rồi, cà rốt sao?",		--画图缺乏图像
        },
        CHANGEIN =
        {
            GENERIC = "Không vui",		--换装失败
            BURNING = "Mau cứu lửa đi!",		--换装失败-着火了
            INUSE = "Mỗi người trong Quảng Hàn Cung đều có tủ áo riêng...",		--衣橱有人占用
        },
        ATTUNE =
        {
            NOHEALTH = "Máu yếu... hồi chút đi",		--制造肉雕像血量不足
        },
        MOUNT =
        {
            TARGETINCOMBAT = "Trông tụi nó có vẻ rất tức giận...",		--骑乘，牛正在战斗
            INUSE = "Bé thỏ muốn cưỡi bò!",		--骑乘（牛被占据）
        },
        SADDLE =
        {
            TARGETINCOMBAT = "Nó đang tức giận",		--给战斗状态的牛上鞍
        },
        TEACH =
        {
            KNOWN = "Biết rồi!",		--学习已经知道的蓝图
            CANTLEARN = "Khó quá, bé thỏ đau đầu quá",		--学习无法学习的蓝图
            WRONGWORLD = "Bản đồ không khớp với vị trí...",		--学习另外一个世界的地图
        },
        WRAPBUNDLE =
        {
            EMPTY = "Trống rỗng",		--打包纸是空的
        },
        PICKUP =
        {
			RESTRICTION = "Không cần",		--熔炉模式下捡起错误的武器
			INUSE = "Phải đợi thôi",		--捡起已经打开的容器
            NOTMINE_YOTC =
            {
                "Nó không muốn lại đây",		--暂无注释
                "Nó đã có chủ rồi",		--暂无注释
            },
        },
        SLAUGHTER =
        {
            TOOFAR = "Mau quay lại đây!",		--屠杀？？ 因为太远而失败
        },
        REPLATE =
        {
            MISMATCH = "Không đúng món", 		--暴食-换盘子换错了 比如用碗换碟子
            SAMEDISH = "Đã có rồi!", 		--暴食-换盘子已经换了
        },
        SAIL =
        {
        	REPAIR = "Ổn rồi",		--暂无注释
        },
        ROW_FAIL =
        {
            BAD_TIMING0 = "Tại sao không thể đằng vân!",		--暂无注释
            BAD_TIMING1 = "Mỏi tay quá",		--暂无注释
            BAD_TIMING2 = "Tốt xấu gì ta cũng là một con thỏ tiên...",		--暂无注释
        },
        LOWER_SAIL_FAIL =
        {
            "Ôi trời!",		--暂无注释
            "Xuống!",		--暂无注释
            "Không được...",		--暂无注释
        },
        BATHBOMB =
        {
            GLASSED = "Nước cứng quá",		--暂无注释
            ALREADY_BOMBED = "Nước nóng rồi",		--暂无注释
        },
		GIVE_TACKLESKETCH =
		{
			DUPLICATE = "He he! Sớm đã lường trước rồi!",		--暂无注释
		},
		COMPARE_WEIGHABLE =
		{
			TOO_SMALL = "Con cá này nhỏ quá!",		--暂无注释
		},
        BEGIN_QUEST =
        {
            ONEGHOST = "only_used_by_wendy",		--暂无注释
        },
	},
	ACTIONFAIL_GENERIC = "Không làm được",		--动作失败
	ANNOUNCE_BOAT_LEAK = "Đừng! Thuyền ướt quá rồi!",		--暂无注释
	ANNOUNCE_BOAT_SINK = "... có lẽ thuyền hơi ướt quá rồi",		--暂无注释
	ANNOUNCE_DIG_DISEASE_WARNING = "Xem đi đỡ chút rồi?",		--挖起生病的植物
	ANNOUNCE_PICK_DISEASE_WARNING = "Thối quá!",		--（植物生病）
	ANNOUNCE_ADVENTUREFAIL = "Đập nào!",		--没调用（废案）
    ANNOUNCE_MOUNT_LOWHEALTH = "Hình như con bò lông nhung bị thương rồi...",		--牛血量过低
	ANNOUNCE_BEES = "Ta muốn ăn chút mật ong!",		--戴养蜂帽被蜜蜂蛰
	ANNOUNCE_BOOMERANG = "Vo ve~ Bay đến rồi",		--回旋镖
	ANNOUNCE_CHARLIE = "Hằng Nga tỷ tỷ mau cứu tôi!",		--查理即将攻击
	ANNOUNCE_CHARLIE_ATTACK = "Hứ! Cũng do mặt trăng hôm nay âm u.....",		--被查理攻击
	ANNOUNCE_COLD = "Ta không nên thích mát mà không mặc áo lông",		--过冷
	ANNOUNCE_HOT = "Ta không muốn thành thịt thỏ phơi nắng!",		--过热
	ANNOUNCE_CRAFTING_FAIL = "Không được, cần nhiều đồ hơn",		--没调用
	ANNOUNCE_DEERCLOPS = "Có yêu khí gần đây...",		--boss来袭
	ANNOUNCE_CAVEIN = "Đá rơi kìa!!",		--要地震了？？？
	ANNOUNCE_ANTLION_SINKHOLE =
	{
		"Thổ địa ở đây lại lười biếng rồi",		--蚁狮地震
		"Tại sao đất đang rung lắc?!",		--蚁狮地震
		"Ta phải tìm ra nguyên nhân!",		--蚁狮地震
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "Cầm lấy đi, đừng rung nữa",		--向蚁狮致敬
        "Tặng ngươi cái này",		--给蚁狮上供
        "Yêu thần sớm muộn sẽ diệt ngươi",		--给蚁狮上供
	},
	ANNOUNCE_SACREDCHEST_YES = "Yeah!",		--远古宝箱物品正确给出蓝图
	ANNOUNCE_SACREDCHEST_NO = "Cái rương đáng ghét",		--远古宝箱
    ANNOUNCE_DUSK = "Ông mặt trời sắp ngủ rồi",		--进入黄昏
	ANNOUNCE_EAT =
	{
		GENERIC = "Ngồm ngoàm - ngoàm ngoàm!",		--吃东西
		PAINFUL = "Đau bụng...",		--吃怪物肉
		SPOILED = "Sao ngươi lại cho ta ăn cái này! Hu hu~",		--吃腐烂食物
		STALE = "Mùi lạ lạ",		--吃黄色食物
		INVALID = "Bản tiên không thể ăn cái này",		--拒吃
        YUCKY = "Hằng Nga tỷ tỷ trước giờ chưa từng cho ta ăn cái này",		--吃红色食物
    },
    ANNOUNCE_ENCUMBERED =
    {
        "Ha...",		--搬运雕像、可疑的大理石
        "Mệt rồi...",		--搬运雕像、可疑的大理石
        "Không cần sao!",		--搬运雕像、可疑的大理石
        "Tiên nữ... không thích hợp... dọn đồ nặng!",		--搬运雕像、可疑的大理石
        "Không... thích... như vậy...",		--搬运雕像、可疑的大理石
        "（Lẩm bẩm）",		--搬运雕像、可疑的大理石
        "/(ㄒoㄒ)/~~",		--搬运雕像、可疑的大理石
        "Có... người to khỏe nào... giúp không...?",		--搬运雕像、可疑的大理石
        "Ôi chao! Cái eo nhỏ của ta!",		--搬运雕像、可疑的大理石
    },
    ANNOUNCE_ATRIUM_DESTABILIZING =
    {
		"Trời đất đang rung chuyển!",		--中庭击杀boss后即将刷新
		"Xảy ra chuyện rồi!",		--中庭震动
		"Chuồn đi!",		--中庭击杀boss后即将刷新
	},
    ANNOUNCE_RUINS_RESET = "Tuần hoàn không ngừng!",		--地下重置
    ANNOUNCE_SNARED = "Đừng nhốt ta",		--远古嘤嘤怪的骨笼
    ANNOUNCE_REPELLED = "Con rùa to?!",		--嘤嘤怪保护状态
	ANNOUNCE_ENTER_DARK = "Màn đêm giống như đôi tay che lấy ta，",		--进入黑暗
	ANNOUNCE_ENTER_LIGHT = "Trong Nguyệt Cung luôn sáng trưng",		--进入光源
	ANNOUNCE_FREEDOM = "Hẹn gặp lại!",		--没调用（废案）
	ANNOUNCE_HIGHRESEARCH = "Cảm giác đầu rất thông minh!",		--没调用（废案）
	ANNOUNCE_HOUNDS = "Ta... ta cần một cây gậy đánh đuổi đám chó...",		--猎犬将至
	ANNOUNCE_WORMS = "Có con sâu to lớn hung ác sắp chui ra",		--蠕虫袭击
	ANNOUNCE_HUNGRY = "Bây giờ ta có thể một đớp ăn được tám trăm củ cà rốt to!",		--饥饿
	ANNOUNCE_HUNT_BEAST_NEARBY = "Có hy vọng?",		--即将找到野兽
	ANNOUNCE_HUNT_LOST_TRAIL = "Bay vào hoa cải khó tìm được",		--猎物（大象脚印丢失）
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "Đất ẩm ướt không giữ được dấu chân",		--大猎物丢失踪迹
	ANNOUNCE_INV_FULL = "Túi của ta không đủ chỗ",		--身上的物品满了
	ANNOUNCE_KNOCKEDOUT = "Đau đầu...",		--被催眠
	ANNOUNCE_LOWRESEARCH = "Khó hiểu...",		--没调用（废案）
	ANNOUNCE_MOSQUITOS = "Tránh ra! Đám côn trùng kia!",		--没调用
    ANNOUNCE_NOWARDROBEONFIRE = "Không ổn rồi!",		--橱柜着火
    ANNOUNCE_NODANGERGIFT = "Muốn mở quà... nhưng không an toàn",		--周围有危险的情况下打开礼物
    ANNOUNCE_NOMOUNTEDGIFT = "Phải xuống bò",		--骑牛的时候打开礼物
	ANNOUNCE_NODANGERSLEEP = "Giờ không phải là lúc ngủ!",		--危险，不能睡觉
	ANNOUNCE_NODAYSLEEP = "Ban ngày ngủ cái gì!",		--白天睡帐篷
	ANNOUNCE_NODAYSLEEP_CAVE = "Bé thỏ khổ quá mà",		--洞穴里白天睡帐篷
	ANNOUNCE_NOHUNGERSLEEP = "Cái bụng đang kêu",		--饿了无法睡觉
	ANNOUNCE_NOSLEEPONFIRE = "Ta không muốn trở thành thỏ nướng!",		--营帐着火无法睡觉
	ANNOUNCE_NODANGERSIESTA = "Ngươi nghiêm túc đó à?",		--危险，不能午睡
	ANNOUNCE_NONIGHTSIESTA = "Bây giờ ta không muốn hóng gió!",		--夜晚睡凉棚
	ANNOUNCE_NONIGHTSIESTA_CAVE = "Ở đây quá đáng sợ...",		--在洞穴里夜晚睡凉棚
	ANNOUNCE_NOHUNGERSIESTA = "Cái bụng kêu rất ghê!",		--饱度不足无法午睡
	ANNOUNCE_NODANGERAFK = "Cẩn thận!",		--战斗状态下线（已经移除）
	ANNOUNCE_NO_TRAP = "Dễ dàng!",		--没有陷阱？？？没调用
	ANNOUNCE_PECKED = "Ô! Ô! Ô Oa!",		--被小高鸟啄
	ANNOUNCE_QUAKE = "Đất đang rung!",		--地震
	ANNOUNCE_RESEARCH = "Cảm giác đầu cực kỳ thông minh!",		--没调用
	ANNOUNCE_SHELTER = "Cảm ơn ngài đại thụ đã che chở",		--下雨天躲树下
	ANNOUNCE_THORNS = "Bụi gai quấn lấy mùi thơm!",		--玫瑰、仙人掌、荆棘扎手
	ANNOUNCE_BURNT = "Nóng!!",		--烧完了
	ANNOUNCE_TORCH_OUT = "Xem ra phải làm một cây đuốc rồi",		--火把用完了
	ANNOUNCE_THURIBLE_OUT = "Ôi, hết rồi",		--香炉燃尽
	ANNOUNCE_FAN_OUT = "Hu hu hu... không phải ta làm hư!!",		--小风车停了
    ANNOUNCE_COMPASS_OUT = "Cây kim nhọn hư rồi",		--指南针用完了
	ANNOUNCE_TRAP_WENT_OFF = "...Cái thứ dính dính",		--触发陷阱（例如冬季陷阱）
	ANNOUNCE_UNIMPLEMENTED = "Bẫy chưa xong!!",		--没调用
	ANNOUNCE_WORMHOLE = "Cái miệng to xuýt nữa nuốc ta vào rồi!",		--跳虫洞
	ANNOUNCE_TOWNPORTALTELEPORT = "...Cảm thấy kỳ lạ!",		--豪华传送
	ANNOUNCE_CANFIX = "Sửa một cách dễ dàng!",		--墙壁可以修理
	ANNOUNCE_ACCOMPLISHMENT = "Xong!",		--没调用
	ANNOUNCE_ACCOMPLISHMENT_DONE = "Xong!",			--没调用
	ANNOUNCE_INSUFFICIENTFERTILIZER = "Ở đây cà rốt không lớn nổi",		--土地肥力不足
	ANNOUNCE_TOOL_SLIP = "Cái móng của ta không giữ được nó",		--工具滑出
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "Hay cho lôi công điện mẫu ngươi! Đợi ta về bẩm báo với Thái Âm Tinh Quân!",		--规避闪电
	ANNOUNCE_TOADESCAPING = "Đừng đi!",		--蟾蜍正在逃跑
	ANNOUNCE_TOADESCAPED = "Quay lại, con cóc!!",		--蟾蜍逃走了
	ANNOUNCE_DAMP = "Gió nhẹ thổi cùng mưa phùn, kèm với mùi đất!",		--潮湿（1级）
	ANNOUNCE_WET = "Lông của ta ướt hết rồi",		--潮湿（2级）
	ANNOUNCE_WETTER = "Ta giờ như con thỏ trong nồi canh, hu hu~",		--潮湿（3级）
	ANNOUNCE_SOAKED = "Ta đang đang bơi sao!",		--潮湿（4级）
	ANNOUNCE_WASHED_ASHORE = "Bơi thỏa thích!",		--暂无注释
    ANNOUNCE_DESPAWN = "Cảm thấy lạnh...",		--下线
	ANNOUNCE_BECOMEGHOST = "A!",		--变成鬼魂
	ANNOUNCE_GHOSTDRAIN = "Ơ...u u......",		--队友死亡掉san
	ANNOUNCE_PETRIFED_TREES = "Cái cây đáng sợ...",		--石化树
	ANNOUNCE_KLAUS_ENRAGE = "A! Thật đáng sợ thật đáng sợ!!",		--克劳斯被激怒（杀死了鹿）
	ANNOUNCE_KLAUS_UNCHAINED = "Cái bụng mọc răng kìa! Cứu tôi với!",		--克劳斯-未上锁的  猜测是死亡准备变身？
	ANNOUNCE_KLAUS_CALLFORHELP = "Nó đang gọi đồng bọn!",		--克劳斯召唤小偷
	ANNOUNCE_MOONALTAR_MINE =
	{
		GLASS_MED = "Bên trong có đồ?",		--暂无注释
		GLASS_LOW = "Sắp xong rồi!",		--暂无注释
		GLASS_REVEAL = "Tự do!",		--暂无注释
		IDOL_MED = "Bên trong có đồ?",		--暂无注释
		IDOL_LOW = "Sắp xong rồi!",		--暂无注释
		IDOL_REVEAL = "Tự do!",		--暂无注释
		SEED_MED = "Bên trong có đồ?",		--暂无注释
		SEED_LOW = "Sắp xong rồi!",		--暂无注释
		SEED_REVEAL = "Tự do!",		--暂无注释
	},
    ANNOUNCE_SPOOKED = "-Nhìn thấy một số thứ...",		--被吓到
	ANNOUNCE_BRAVERY_POTION = "Ta chưa từng sợ!",		--勇气药剂
	ANNOUNCE_MOONPOTION_FAILED = "Oái... chưa xảy ra chuyện gì",		--暂无注释
	ANNOUNCE_EATING_NOT_FEASTING = "Chắc đang hưởng thụ cùng người khác",		--暂无注释
	ANNOUNCE_WINTERS_FEAST_BUFF = "Ô, thấy chớp sáng!",		--暂无注释
	ANNOUNCE_IS_FEASTING = "Đồ ăn nhiều quá!",		--暂无注释
	ANNOUNCE_WINTERS_FEAST_BUFF_OVER = "Ơ! Cái thứ lấp lánh đi đâu rồi?",		--暂无注释
    ANNOUNCE_REVIVING_CORPSE = "Chúng ta là bạn rồi...？",		--没调用（熔炉）
    ANNOUNCE_REVIVED_OTHER_CORPSE = "Khá ổn rồi",		--没调用（熔炉）
    ANNOUNCE_REVIVED_FROM_CORPSE = "-Thật đáng sợ",		--没调用（熔炉）
    ANNOUNCE_FLARE_SEEN = "Lửa cháy trên không??",		--暂无注释
    ANNOUNCE_OCEAN_SILHOUETTE_INCOMING = "Có người muốn tới?",		--暂无注释
    ANNOUNCE_ROYALTY =
    {
        "Nhân vật quan trọng!",		--向带着蜂王帽的队友鞠躬
        "Con thỏ trắng đội vương miện!",		--向带着蜂王帽的队友鞠躬
        "Ngươi là người trong truyện cổ tích sao?",		--向带着蜂王帽的队友鞠躬
    },
    ANNOUNCE_ATTACH_BUFF_ELECTRICATTACK    = "Chớp nhoáng!",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_ATTACK            = "Muốn đánh nhau sao!",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_PLAYERABSORPTION  = "Cơ thể ta cứng hơn ngươi!",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_WORKEFFECTIVENESS = "...? Sao bỗng nhiên ta muốn làm việc ghê?",		--暂无注释
    ANNOUNCE_ATTACH_BUFF_MOISTUREIMMUNITY  = "... trở nên khô cằn...",		--暂无注释
    ANNOUNCE_DETACH_BUFF_ELECTRICATTACK    = "Ao ao ao...",		--暂无注释
    ANNOUNCE_DETACH_BUFF_ATTACK            = "Không muốn đánh nữa",		--暂无注释
    ANNOUNCE_DETACH_BUFF_PLAYERABSORPTION  = "Đừng đánh!",		--暂无注释
    ANNOUNCE_DETACH_BUFF_WORKEFFECTIVENESS = "Nó mệt rồi...",		--暂无注释
    ANNOUNCE_DETACH_BUFF_MOISTUREIMMUNITY  = "Sắp ướt rồi!",		--暂无注释
	ANNOUNCE_OCEANFISHING_LINESNAP = "Ê!",		--暂无注释
	ANNOUNCE_OCEANFISHING_LINETOOLOOSE = "Dây quá lỏng, cá sẽ chạy đi mất!",		--暂无注释
	ANNOUNCE_OCEANFISHING_GOTAWAY = "Không! Quay lại đi, cá ơi!",		--暂无注释
	ANNOUNCE_OCEANFISHING_BADCAST = "Rất khó!",		--暂无注释
	ANNOUNCE_OCEANFISHING_IDLE_QUOTE =
	{
		"Cá! Lại đây, cá!",		--暂无注释
		"Tu ti tu ti ta...",		--暂无注释
		"Cá đâu?",		--暂无注释
		"Hơi lâu",		--暂无注释
	},
	ANNOUNCE_WEIGHT = "Trọng lượng：{weight}",		--暂无注释
    ANNOUNCE_KINGCREATED = "Người cá có tân quốc vương!",		--暂无注释
    ANNOUNCE_KINGDESTROYED = "Quốc vương đời trước không ổn, sẽ có người tìm người khác tốt hơn!",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_THRONE = "Nơi đây không hợp với vua đầm lầy!",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_HOUSE = "Thật tráng lệ!",		--暂无注释
    ANNOUNCE_CANTBUILDHERE_WATCHTOWER = "Vệ sĩ sẽ bảo vệ nơi đây!",		--暂无注释
    ANNOUNCE_READ_BOOK =
    {
        BOOK_SLEEP = "Rất lâu rất lâu! Xưa kia...",		--暂无注释
        BOOK_BIRDS = "Có con chim Hỷ Thước giúp Chức Nữ tỷ tỷ không?",		--暂无注释
        BOOK_TENTACLES =  "Đây là quyển sách hay!",		--暂无注释
        BOOK_BRIMSTONE = "Ta muốn biết kết cục của câu chuyện!",		--暂无注释
        BOOK_GARDENING = "Nhiều từ lạ quá...",		--暂无注释
    },
    ANNOUNCE_WEAK_RAT = "Trông không hợp lắm...",		--暂无注释
    ANNOUNCE_CARRAT_START_RACE = "Xông lên!",		--暂无注释
    ANNOUNCE_CARRAT_ERROR_WRONG_WAY =
    {
        "Ê! Đi hướng này! Hướng này!",		--暂无注释
        "Cà ruột đi lộn hướng rồi!",		--暂无注释
    },
    ANNOUNCE_CARRAT_ERROR_FELL_ASLEEP = "Đừng ngủ! Dậy nào!",    		--暂无注释
    ANNOUNCE_CARRAT_ERROR_WALKING = "Chạy chậm quá!",    		--暂无注释
    ANNOUNCE_CARRAT_ERROR_STUNNED = "Sao vậy, sao ngươi không động đậy vậy?",		--暂无注释
    ANNOUNCE_GHOST_QUEST = "only_used_by_wendy",		--暂无注释
    ANNOUNCE_ABIGAIL_SUMMON =
	{
	},
    ANNOUNCE_GHOSTLYBOND_LEVELUP =
	{
	},
	BATTLECRY =
	{
		GENERIC = "Đừng sợ... ta... ta thắng được...",		--战斗
		PIG = "Cái tên tai to mặt lớn không tốt đẹp gì!",		--战斗 猪人
		PREY = "Giữ lại... giữ lại!",		--战斗 猎物？？大象？
		SPIDER = "Nếu ngươi ngoan ngoãn giao tơ nhện ra, thì ta sẽ bớt 1 gậy cho ngươi!",		--战斗 蜘蛛
		SPIDER_WARRIOR = "Ngươi muốn đấu một trận với ta sao!",		--战斗 蜘蛛战士
		DEER = "Ta không muốn người ta nói ta ức hiếp con nai tàn tật!",		--战斗 无眼鹿
	},
	COMBAT_QUIT =
	{
		GENERIC = "Ha ha ha!",		--攻击目标被卡住，无法攻击
		PIG = "Người lợn xấu xa chạy rồi",		--退出战斗-猪人
		PREY = "Quay lại đây!",		--退出战斗 猎物？？大象？
		SPIDER = "Uh... có khả năng là bạn của nhện",		-- 退出战斗 蜘蛛
		SPIDER_WARRIOR = "Thật khó mà thích ngươi được!",		--退出战斗 蜘蛛战士
	},
	DESCRIBE =
	{
		MULTIPLAYER_PORTAL = "Hy vọng bị tuyệt vọng quấn lấy, nở thành bông hồng tươi đẹp",		-- 物品名:"绚丽之门"
        MULTIPLAYER_PORTAL_MOONROCK = "Cánh cửa này có thể giúp ta quay về nhà!",		-- 物品名:"天体传送门"
        MOONROCKIDOL = "Nó đang nhìn ta",		-- 物品名:"月岩雕像" 制造描述:"重要人物"
        CONSTRUCTION_PLANS = "Hình như dễ làm!",		-- 物品名:未找到
        ANTLION =
        {
            GENERIC = "Ngươi muốn gì?",		-- 物品名:"蚁狮"->默认
            VERYHAPPY = "Trông cô ấy vui hơn hẳn!",		-- 物品名:"蚁狮"
            UNHAPPY = "Ha ha đừng tức giận!",		-- 物品名:"蚁狮"
        },
        ANTLIONTRINKET = "Quen biết một người thích cái này!",		-- 物品名:"沙滩玩具"
        SANDSPIKE = "A! Gai!",		-- 物品名:"沙刺"
        SANDBLOCK = "Cùng ta đắp cát nào",		-- 物品名:"沙堡"
        GLASSSPIKE = "Xiên đầu lợn sẽ rất đẹp!",		-- 物品名:"玻璃狼牙棒"
        GLASSBLOCK = "Có vẻ rất dễ bể",		-- 物品名:"玻璃城堡"
        ABIGAIL_FLOWER =
        {
            GENERIC ="Bông hoa chứa linh hồn",		-- 物品名:"阿比盖尔之花"->默认 制造描述:"一个神奇的纪念品"
			LEVEL1 = "Abigail trốn rồi",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
			LEVEL2 = "Cô ấy chậm rãi từ từ",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
			LEVEL3 = "Ra đây!",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
            LONG = "Thật là đẹp",		-- 物品名:"阿比盖尔之花"->还需要很久 制造描述:"一个神奇的纪念品"
            MEDIUM = "Ô?",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
            SOON = "Có chuyện gì sắp xảy ra vậy!",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
            HAUNTED_POCKET = "Xuống đây, hoa!",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
            HAUNTED_GROUND = "Không nên xuất hiện ở đây...",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品"
        },
        BALLOONS_EMPTY = "Trông rất đẹp",		-- 物品名:"韦斯的气球" 制造描述:"要是有更简单的方法该多好"
        BALLOON = "Ta cũng muốn một cái!!",		-- 物品名:"气球"
        BERNIE_INACTIVE =
        {
            BROKEN = "A... ta cũng muốn một cái",		-- 物品名:"伯尼" 制造描述:"这个疯狂的世界总有你熟悉的人"
            GENERIC = "Không chơi nữa sao?",		-- 物品名:"伯尼"->默认 制造描述:"这个疯狂的世界总有你熟悉的人"
        },
        BERNIE_ACTIVE = "Muốn chơi cùng nó!",		-- 物品名:"伯尼"
        BERNIE_BIG = "Đồ chơi thật là vui",		-- 物品名:"伯尼！"
        BOOK_BIRDS = "Hỷ Thước đang xây cầu trên mây, người yêu thương sẽ gặp nhau trên mây!",		-- 物品名:"世界鸟类手册" 制造描述:"涵盖1000个物种：习性、栖息地及叫声"
        BOOK_TENTACLES = "Thật quá độc ác",		-- 物品名:"触手的召唤" 制造描述:"让我们来了解一下地下的朋友们！"
        BOOK_GARDENING = "Huyền Sương Tiên Dược của Nguyện Cung đều do ta trồng đó",		-- 物品名:"应用园艺学" 制造描述:"讲述培育和照料植物的相关知识"
        BOOK_SLEEP = "Hằng Nga tỷ tỷ thường hay dỗ ta ngủ!",		-- 物品名:"睡前故事" 制造描述:"送你入梦的睡前故事"
        BOOK_BRIMSTONE = "Trò hề lừa con nít!",		-- 物品名:"末日将至！" 制造描述:"世界将在火焰和灾难中终结！"
        PLAYER =
        {
            GENERIC = "Ngươi hay lắm, %s!",		-- 物品名:未找到
            ATTACKER = "Không tin tưởng %s",		-- 物品名:未找到
            MURDERER = "...... %s là sát thủ!! ",		-- 物品名:未找到
            REVIVER = "Ta vốn không cần bất kỳ hỗ trợ nào!... nhưng vẫn cảm ơn",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Ngươi thật đáng sợ, %s",		-- 物品名:"幽灵"
            FIRESTARTER = "Ngươi sẽ đốt hết tất cả mọi thứ, %s!!",		-- 物品名:未找到
        },
        WILSON =
        {
            GENERIC = "Xin chào Wilson!",		-- 物品名:"威尔逊"->默认
            ATTACKER = "Như vậy là không tốt!",		-- 物品名:"威尔逊"
            MURDERER = "Biết ngay không thể tin mấy người này mà!",		-- 物品名:"威尔逊"
            REVIVER = "Thứ \"Khoa Học\" này rất hay, ",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "He he, trông ngươi có hơi ngốc!",		-- 物品名:"幽灵"
            FIRESTARTER = "Như vậy là vì \" Khoa Học\"？",		-- 物品名:"威尔逊"
        },
        WOLFGANG =
        {
            GENERIC = "Chào quý ngài râu ria cơ bắp!",		-- 物品名:"沃尔夫冈"->默认
            ATTACKER = "Không công bằng, đầu ngươi to hơn rất nhiều!",		-- 物品名:"沃尔夫冈"
            MURDERER = "Tên ác bá ngươi!",		-- 物品名:"沃尔夫冈"
            REVIVER = "Cho nên ngươi không còn sợ ta nữa phải không?",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Đừng sợ, ta sẽ tìm trái tim cho ngươi!",		-- 物品名:"幽灵"
            FIRESTARTER = "Đây là ý tồi",		-- 物品名:"沃尔夫冈"
        },
        WAXWELL =
        {
            GENERIC = "Ta cảm nhận được sự đen tối trong ngươi",		-- 物品名:"麦斯威尔"->默认
            ATTACKER = "Tên xấu xa ngươi!!",		-- 物品名:"麦斯威尔"
            MURDERER = "Ta biết tại sao không thích ngươi rồi",		-- 物品名:"麦斯威尔"
            REVIVER = "Ngươi dường như không xấu như vậy",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Đừng ồn, ta sẽ giúp ngươi!",		-- 物品名:"幽灵"
            FIRESTARTER = "Hắn làm đó! Hắn làm đó!",		-- 物品名:"麦斯威尔"
        },
        WX78 =
        {
            GENERIC = "Chào bạn người thiết",		-- 物品名:"WX-78"->默认
            ATTACKER = "Ôi! Dừng tay!",		-- 物品名:"WX-78"
            MURDERER = "Chiến tranh tàn khốc",		-- 物品名:"WX-78"
            REVIVER = "Không ngờ ngươi sẽ làm như vậy đóa...",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Trông ngươi có vẻ không vui",		-- 物品名:"幽灵"
            FIRESTARTER = "Ngươi đùa hơi quá rồi",		-- 物品名:"WX-78"
        },
        WILLOW =
        {
            GENERIC = "Xin chào nữ sĩ ngọn lửa!",		-- 物品名:"薇洛"->默认
            ATTACKER = "Ngươi cũng không mạnh đến như vậy!",		-- 物品名:"薇洛"
            MURDERER = "Nữ sĩ xấu xa!",		-- 物品名:"薇洛"
            REVIVER = "Kỳ thật ngươi cũng tốt lắm",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Đằng nào ngươi cũng... thay ta chơi với gấu con nhe? Được thôi, ta đi tìm trái tim",		-- 物品名:"幽灵"
            FIRESTARTER = "Hình như cô ta rất vui",		-- 物品名:"薇洛"
        },
        WENDY =
        {
            GENERIC = "Xin chào, hôm nay hai chị em khỏe không?",		-- 物品名:"温蒂"->默认
            ATTACKER = "Ê! Dừng tay!",		-- 物品名:"温蒂"
            MURDERER = "Ngươi chỉ muốn chơi với quỷ?",		-- 物品名:"温蒂"
            REVIVER = "Chào hỏi Abigail thay ngươi rồi",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Ngươi... thật sự cần một trái tim sao?",		-- 物品名:"幽灵"
            FIRESTARTER = "Tại sao ngươi lại làm như vậy?",		-- 物品名:"温蒂"
        },
        WOODIE =
        {
            GENERIC = "Xin chào quý ngài chặt gỗ!",		-- 物品名:"伍迪"->默认
            ATTACKER = "Đánh nhau với cây ấy, đừng đánh người ta!",		-- 物品名:"伍迪"
            MURDERER = "Nên lường trước việc ngài chặt gỗ chính là sát thủ!",		-- 物品名:"伍迪"
            REVIVER = "Quý ngài chặt gỗ rất tốt",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Cần giúp không?",		-- 物品名:"幽灵"
            BEAVER = "Cái người chặt gỗ đi đâu rồi?!",		-- 物品名:"伍迪"
            BEAVERGHOST = "Ta đi tìm trái tim, ngươi đi tìm quý ngài chặt gỗ về!",		-- 物品名:"伍迪"
            MOOSE = "Cái người chặt gỗ đi đâu rồi?!",		-- 物品名:"伍迪"
            MOOSEGHOST = "Ta đi tìm trái tim, ngươi đi tìm quý ngài chặt gỗ về!",		-- 物品名:"伍迪"
            GOOSE = "Cái người chặt gỗ đi đâu rồi?!",		-- 物品名:"伍迪"
            GOOSEGHOST = "Ta đi tìm trái tim, ngươi đi tìm quý ngài chặt gỗ về!",		-- 物品名:"伍迪"
            FIRESTARTER = "Không phải ngươi thích chặt gỗ sao, sao lại đốt rồi?",		-- 物品名:"伍迪"
        },
        WICKERBOTTOM =
        {
            GENERIC = "Kể chuyện cho ta nghe đi?",		-- 物品名:"薇克巴顿"->默认
            ATTACKER = "Tức cái gì? Ta không làm gì hết!",		-- 物品名:"薇克巴顿"
            MURDERER = "Uổng cho ta đã tin tưởng ngươi như vậy!",		-- 物品名:"薇克巴顿"
            REVIVER = "Rất cảm ơn! (Ta nói đúng không)",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Làm trái tim cho ngươi ngay!",		-- 物品名:"幽灵"
            FIRESTARTER = "Đừng có đốt lửa khắp nơi",		-- 物品名:"薇克巴顿"
        },
        WES =
        {
            GENERIC = "Xin chào chú hề",		-- 物品名:"韦斯"->默认
            ATTACKER = "Tránh ra!",		-- 物品名:"韦斯"
            MURDERER = "Lúc đầu không nên rời khỏi đây!",		-- 物品名:"韦斯"
            REVIVER = "Ồ... cảm ơn",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Còn trắng hơn so với lúc bình thường nữa",		-- 物品名:"幽灵"
            FIRESTARTER = "Ngươi là kẻ xấu",		-- 物品名:"韦斯"
        },
        WEBBER =
        {
            GENERIC = "Hazz, yêu tinh nhện!",		-- 物品名:"韦伯"->默认
            ATTACKER = "Sao ngươi khắt khe thế?",		-- 物品名:"韦伯"
            MURDERER = "Tưởng ngươi là bạn chứ!",		-- 物品名:"韦伯"
            REVIVER = "Ta biết chúng ta là bạn mà!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Đừng buồn, ta sẽ tìm trái tim cho ngươi!",		-- 物品名:"幽灵"
            FIRESTARTER = "Ngươi gây họa rồi!",		-- 物品名:"韦伯"
        },
        WATHGRITHR =
        {
            GENERIC = "Xin chào nữ sĩ Viking!",		-- 物品名:"薇弗德"->默认
            ATTACKER = "Nữ sĩ Viking muốn đánh nhau??",		-- 物品名:"薇弗德"
            MURDERER = "Đừng săn giết ta!!",		-- 物品名:"薇弗德"
            REVIVER = "... cảm ơn ngươi",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Ồ, trông ngươi thật đáng sợ!",		-- 物品名:"幽灵"
            FIRESTARTER = "Đó không phải là việc của nữ sĩ ngọn lửa sao",		-- 物品名:"薇弗德"
        },
        WINONA =
        {
            GENERIC = "Ngươi quen biết nữ sĩ đêm tối?",		-- 物品名:"薇诺娜"->默认
            ATTACKER = "Cái đó không an toàn!",		-- 物品名:"薇诺娜"
            MURDERER = "Ngươi đã phụ lòng tin của ta!!",		-- 物品名:"薇诺娜"
            REVIVER = "Sửa xong rồi!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Vốn tưởng rằng trước giờ ngươi không bỏ trốn chứ?",		-- 物品名:"幽灵"
            FIRESTARTER = "Có lẽ cô ấy chán sửa đồ rồi",		-- 物品名:"薇诺娜"
        },
        WORTOX =
        {
            GENERIC = "Tiểu \"Ác Ma\"？",		-- 物品名:"沃拓克斯"->默认
            ATTACKER = "Ngươi đúng là tên xấu xa!",		-- 物品名:"沃拓克斯"
            MURDERER = "Biết ngay ngươi không đáng tin!",		-- 物品名:"沃拓克斯"
            REVIVER = "Lại giở trò...？",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Thổi phù một tiếng là ngươi xuất hồn!!",		-- 物品名:"幽灵"
            FIRESTARTER = "Trông nó thật đáng sợ",		-- 物品名:"沃拓克斯"
        },
        WORMWOOD =
        {
            GENERIC = "Hazz, thực vật lá xanh!",		-- 物品名:"沃姆伍德"->默认
            ATTACKER = "Ôi! Cây cỏ dại vừa già vừa xấu như ngươi!",		-- 物品名:"沃姆伍德"
            MURDERER = "Chúng ta không thể làm bạn được!",		-- 物品名:"沃姆伍德"
            REVIVER = "Ngươi là cái cây tốt!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Ngươi đợi ở đây! Ta đi tìm người giúp!",		-- 物品名:"幽灵"
            FIRESTARTER = "Làm như vậy nguy hiếm lắm!",		-- 物品名:"沃姆伍德"
        },
        WARLY =
        {
            GENERIC = "Xin chào quý ngài đầu bếp!",		-- 物品名:"沃利"->默认
            ATTACKER = "Cứ tưởng ngươi là người tốt chứ!",		-- 物品名:"沃利"
            MURDERER = "Ngươi vốn không được xem là bạn!",		-- 物品名:"沃利"
            REVIVER = "Ngươi... giúp ta?",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Không! Sau này ai sẽ làm món ngon cho ta chứ...",		-- 物品名:"幽灵"
            FIRESTARTER = "Hắn ta cái gì cũng muốn nướng lên để ăn!",		-- 物品名:"沃利"
        },
        WURT =
        {
            GENERIC = "Xin chào, người cá!",		-- 物品名:"沃特"->默认
            ATTACKER = "Chúng ta phải đoàn kết!",		-- 物品名:"沃特"
            MURDERER = "Quả nhiên là kẻ địch mạnh!",		-- 物品名:"沃特"
            REVIVER = "Ta luôn tin tưởng vào ngươi!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
            GHOST = "Có thể tìm được trái tim ở nơi nào đó",		-- 物品名:"幽灵"
            FIRESTARTER = "Dừng tay! Ngươi sẽ khiến bọn ta gặp phải phiền phức!",		-- 物品名:"沃特"
        },
        MIGRATION_PORTAL =
        {
            GENERIC = "Cái này sẽ đi đâu?",		-- 物品名:"Matic 朋友之门"->默认
            OPEN = "Không biết bên còn lại có gì...",		-- 物品名:"Matic 朋友之门"->打开
            FULL = "Không có chỗ trống",		-- 物品名:"Matic 朋友之门"->满了
        },
        GLOMMER =
        {
            GENERIC = "He he, trên mình con sâu có thứ kết dính!",		-- 物品名:"罗姆"->默认
            SLEEPING = "Ngủ ngon, con sâu nhỏ",		-- 物品名:"罗姆"->睡着了
        },
        GLOMMERFLOWER =
        {
            GENERIC = "Cực kỳ rực sỡ?",		-- 物品名:"罗姆花"->默认
            DEAD = "Ta có hơi buồn",		-- 物品名:"罗姆花"->死了
        },
        GLOMMERWINGS = "Con sâu mọc cái cánh kỳ lạ",		-- 物品名:"罗姆翅膀"
        GLOMMERFUEL = "Đống nhầy của con sâu",		-- 物品名:"罗姆的黏液"
        BELL = "Phát ra âm thanh rất hay",		-- 物品名:"远古铃铛" 制造描述:"这可不是普通的铃铛"
        STATUEGLOMMER =
        {
            GENERIC = "Trông cục đá giống con sâu kỳ lạ",		-- 物品名:"罗姆雕像"->默认
            EMPTY = "Ôi... không phải ta làm đâu!",		-- 物品名:"罗姆雕像"
        },
        LAVA_POND_ROCK = "Có lẽ có thể mài thành một món đồ",		-- 物品名:"岩石"
		WEBBERSKULL = "Không nên để xương ở đây",		-- 物品名:"韦伯的头骨"
		WORMLIGHT = "Quả mọng vừa to vừa phát sáng!",		-- 物品名:"发光蓝莓"
		WORMLIGHT_LESSER = "Sáng một ít...",		-- 物品名:"较少发光蓝莓"
		WORM =
		{
		    PLANT = "Nguy hiểm và kỳ ngộ cùng tồn tại...",		-- 物品名:"洞穴蠕虫"
		    DIRT = "Đống đất!",		-- 物品名:"洞穴蠕虫"
		    WORM = "Con sâu!",		-- 物品名:"洞穴蠕虫"
		},
        WORMLIGHT_PLANT = "Thật là đẹp...",		-- 物品名:"神秘植物"
		MOLE =
		{
			HELD = "Nhóc đáng thương, đừng vặn tới vặn lui nữa!",		-- 物品名:"鼹鼠"->拿在手里
			UNDERGROUND = "Thổ địa, ngươi muốn đi đâu?",		-- 物品名:"鼹鼠"
			ABOVEGROUND = "Thật là thích lo chuyện bao đồng!",		-- 物品名:"鼹鼠"
		},
		MOLEHILL = "Nhà của chũi trùng",		-- 物品名:"鼹鼠丘"
		MOLEHAT = "Cái mùi thật khó ngửi",		-- 物品名:"鼹鼠帽" 制造描述:"为穿戴者提供夜视能力"
		EEL = "Xin chào lươn!",		-- 物品名:"鳗鱼"
		EEL_COOKED = "Lần này ngươi không phóng điện được rồi",		-- 物品名:"烤鳗鱼"
		UNAGI = "Không được đối xử với cá như vậy!",		-- 物品名:"鳗鱼料理"
		EYETURRET = "Không tấn công ta... đúng không?",		-- 物品名:"眼睛炮塔"
		EYETURRET_ITEM = "Đó là ý hay phải không...",		-- 物品名:"眼睛炮塔" 制造描述:"要远离讨厌的东西，就得杀了它们"
		MINOTAURHORN = "Thứ phàm tục, không ai không tránh!",		-- 物品名:"守护者之角"
		MINOTAURCHEST = "Thuyền trưởng thỏ đã tìm được kho báu",		-- 物品名:"大号华丽箱子"
		THULECITE_PIECES = "Vật nho nhỏ",		-- 物品名:"铥矿碎片"
		POND_ALGAE = "Ô, dính dính!",		-- 物品名:"水藻"
		GREENSTAFF = "Khảm một viên ngọc xanh đẹp",		-- 物品名:"拆解魔杖" 制造描述:"干净而有效的摧毁"
		GIFT = "Món quà tặng ta!",		-- 物品名:"礼物"
        GIFTWRAP = "Tại sao dùng thứ này gói thứ khác vậy?",		-- 物品名:"礼物包装" 制造描述:"把东西打包起来，好看又可爱！"
		POTTEDFERN = "Cái cây này có căn nhà nhỏ",		-- 物品名:"蕨类盆栽" 制造描述:"做个花盆，里面栽上蕨类植物"
        SUCCULENT_POTTED = "Giờ có nhà rồi",		-- 物品名:"多肉盆栽" 制造描述:"塞进陶盆的漂亮多肉植物"
		SUCCULENT_PLANT = "Sao ngươi ở một nơi như vậy?",		-- 物品名:"多肉植物"
		SUCCULENT_PICKED = "Ồ, làm hư rồi",		-- 物品名:"多肉植物"
		SENTRYWARD = "Con mắt của mặt trăng?",		-- 物品名:"月眼守卫" 制造描述:"绘图者最有价值的武器"
        TOWNPORTAL =
        {
			GENERIC = "Đưa ta đến bên cạnh đồng đội!",		-- 物品名:"强征传送塔"->默认 制造描述:"用沙子的力量聚集你的朋友们"
			ACTIVE = "Chuẩn bị xong rồi!",		-- 物品名:"强征传送塔"->激活了 制造描述:"用沙子的力量聚集你的朋友们"
		},
        TOWNPORTALTALISMAN =
        {
			GENERIC = "Đá dịch chuyển!",		-- 物品名:"沙漠石头"->默认
			ACTIVE = "Nhảy đến bênh cạnh đồng đội!",		-- 物品名:"沙漠石头"->激活了
		},
        WETPAPER = "Nó có thể chịu được tất cả",		-- 物品名:"纸张"
        WETPOUCH = "Tìm được báu vật trong ao!",		-- 物品名:"起皱的包裹"
        MOONROCK_PIECES = "Nguyệt Cung?",		-- 物品名:"月亮石碎块"
        MOONBASE =
        {
            GENERIC = "Kích hoạt sức mạnh của mặt trăng",		-- 物品名:"月亮石"->默认
            BROKEN = "Có người làm hư nó rồi!",		-- 物品名:"月亮石"
            STAFFED = "Có chuyện xảy ra rồi sao?",		-- 物品名:"月亮石"
            WRONGSTAFF = "Trông không đúng lắm",		-- 物品名:"月亮石"->插错法杖
            MOONSTAFF = "Là ánh trăng!",		-- 物品名:"月亮石"->已经插了法杖的（月杖）
        },
        MOONDIAL =
        {
			GENERIC = "Chiếu sáng quê hương tôi",		-- 物品名:"月晷"->默认 制造描述:"追踪月相！"
			NIGHT_NEW = "Trăng non cong cong nhìn như lông mày lá liễu",		-- 物品名:"月晷"->新月 制造描述:"追踪月相！"
			NIGHT_WAX = "Bán nguyệt như cung bắn vào tim ta!",		-- 物品名:"月晷"->上弦月 制造描述:"追踪月相！"
			NIGHT_FULL = "Trăng tròn phủ mặt đất!",		-- 物品名:"月晷"->满月 制造描述:"追踪月相！"
			NIGHT_WANE = "Mây che đầu tháng trăng hong, bụi cây hương tỏa từ trong ngát mùi",		-- 物品名:"月晷"->下弦月 制造描述:"追踪月相！"
			CAVE = "Ở đây không nhìn thấy quê hương",		-- 物品名:"月晷"->洞穴 制造描述:"追踪月相！"
        },
		THULECITE = "Trông rất kỳ lạ...",		-- 物品名:"铥矿石" 制造描述:"将小碎片合成一大块"
		ARMORRUINS = "Bảo vệ cực mạnh!",		-- 物品名:"铥矿甲" 制造描述:"炫目并且能提供保护"
		ARMORSKELETON = "Trông đáng sợ!",		-- 物品名:"骨头盔甲"
		SKELETONHAT = "Cảm thấy cái đầu rất lạ",		-- 物品名:"骨头头盔"
		RUINS_BAT = "Vung gậy!",		-- 物品名:"铥矿岩棒" 制造描述:"尖钉让一切变得更好"
		RUINSHAT = "Giờ ta chính là đại vương!!",		-- 物品名:"远古王冠" 制造描述:"附有远古力场！"
		NIGHTMARE_TIMEPIECE =
		{
            CALM = "Có vẻ an toàn",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            WARN = "Càng lúc càng kỳ lạ...",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            WAXING = "Có chuyện gì đó sắp xảy ra!",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            STEADY = "Vẫn luôn như vậy",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            WANING = "Nó sắp hết rồi!",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            DAWN = "Sắp kết thúc rồi, sắp kết thúc rồi",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
            NOMAGIC = "Hình như ổn rồi",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动"
		},
		BISHOP_NIGHTMARE = "Nó không đúng lắm!",		-- 物品名:"损坏的发条主教"
		ROOK_NIGHTMARE = "Nó sao rồi?!",		-- 物品名:"损坏的发条战车"
		KNIGHT_NIGHTMARE = "Ở trên có những thứ không tốt!",		-- 物品名:"损坏的发条骑士"
		MINOTAUR = "Trông nó giống như đang tức giận",		-- 物品名:"远古守护者"
		SPIDER_DROPPER = "Ta nhìn thấy ngươi trốn ở phía trên!",		-- 物品名:"穴居悬蛛"
		NIGHTMARELIGHT = "Giấc mơ rồi sẽ tỉnh, cũng sẽ đau",		-- 物品名:"梦魇灯座"
		NIGHTSTICK = "Ồ, sáng rồi!",		-- 物品名:"晨星锤" 制造描述:"用于夜间战斗的晨光"
		GREENGEM = "Dùng ngọc bích làm một cái mũ cột tóc",		-- 物品名:"绿宝石"
		MULTITOOL_AXE_PICKAXE = "Người làm ra cái này thật thông minh",		-- 物品名:"多用斧镐" 制造描述:"加倍实用"
		ORANGESTAFF = "Sức mạnh phá vỡ không gian!",		-- 物品名:"瞬移魔杖" 制造描述:"适合那些不喜欢走路的人"
		YELLOWAMULET = "Cảm thấy ấm ấm...",		-- 物品名:"魔光护符" 制造描述:"从天堂汲取力量"
		GREENAMULET = "Hữu ích khi tạo đồ!",		-- 物品名:"建造护符" 制造描述:"用更少的材料合成物品！"
		SLURPERPELT = "Cướp lông của nó",			-- 物品名:"铥矿奖章"->啜食者皮 制造描述:"跟踪周围魔力水平的流动"
		SLURPER = "Ha ha, nhột chết mất!",		-- 物品名:"啜食者"
		SLURPER_PELT = "Cướp lông của nó",		-- 物品名:"啜食者皮"
		ARMORSLURPER = "Dây lưng tốt",		-- 物品名:"饥饿腰带" 制造描述:"保持肚子不饿"
		ORANGEAMULET = "Khiến ta cảm thấy thượng đẳng",		-- 物品名:"懒人护符" 制造描述:"适合那些不喜欢捡东西的人"
		YELLOWSTAFF = "Chim vàng hót",		-- 物品名:"唤星者法杖" 制造描述:"召唤一个小星星"
		YELLOWGEM = "Bên trong có ánh sáng mặt trời",		-- 物品名:"黄宝石"
		ORANGEGEM = "Cục đá cam cổ quái",		-- 物品名:"橙宝石"
        OPALSTAFF = "Nó mượn sức mạnh mặt trăng!",		-- 物品名:"唤月者魔杖"
        OPALPRECIOUSGEM = "Tinh hoa của nhật nguyệt được ngưng tụ lại!",		-- 物品名:"彩虹宝石"
        TELEBASE =
		{
			VALID = "Chuẩn bị!",		-- 物品名:"传送焦点"->有效 制造描述:"装上宝石试试"
			GEMS = "Thiếu gì đó...",		-- 物品名:"传送焦点"->需要宝石 制造描述:"装上宝石试试"
		},
		GEMSOCKET =
		{
			VALID = "Cục đá bay lên rồi",		-- 物品名:"宝石底座"->有效
			GEMS = "Tìm cục đá phát sáng mà khảm vào trong",		-- 物品名:"宝石底座"->需要宝石
		},
		STAFFLIGHT = "Ánh sáng thiêu đốt!",		-- 物品名:"矮人之星"
        STAFFCOLDLIGHT = "Sương gió ở Quảng Hàn!",		-- 物品名:"极光"
        ANCIENT_ALTAR = "Trông khá cũ",		-- 物品名:"远古祭坛"
        ANCIENT_ALTAR_BROKEN = "Hư rồi",		-- 物品名:"损坏的远古祭坛"
        ANCIENT_STATUE = "Thật đáng sợ",		-- 物品名:"远古雕像"
        LICHEN = "Thực vật hang động",		-- 物品名:"洞穴苔藓"
		CUTLICHEN = "Giòn xốp",		-- 物品名:"苔藓"
		CAVE_BANANA = "Ôi... trái ngon",		-- 物品名:"洞穴香蕉"
		CAVE_BANANA_COOKED = "Ngon!",		-- 物品名:"烤香蕉"
		CAVE_BANANA_TREE = "Phía trên có trái!",		-- 物品名:"洞穴香蕉树"
		ROCKY = "Chào bạn!",		-- 物品名:"石虾"
		COMPASS =
		{
			GENERIC="Bên trong có kim nhọn!",		-- 物品名:"指南针"->默认 制造描述:"指向北方"
			N = "Bắc",		-- 物品名:"指南针" 制造描述:"指向北方"
			S = "Nam",		-- 物品名:"指南针" 制造描述:"指向北方"
			E = "Đông",		-- 物品名:"指南针" 制造描述:"指向北方"
			W = "Tây",		-- 物品名:"指南针" 制造描述:"指向北方"
			NE = "Đông Bắc",		-- 物品名:"指南针" 制造描述:"指向北方"
			SE = "Đông Nam",		-- 物品名:"指南针" 制造描述:"指向北方"
			NW = "Tây Bắc",		-- 物品名:"指南针" 制造描述:"指向北方"
			SW = "Tây Nam",		-- 物品名:"指南针" 制造描述:"指向北方"
		},
        HOUNDSTOOTH = "Răng chó",		-- 物品名:"犬牙"
        ARMORSNURTLESHELL = "Dính thật",		-- 物品名:"蜗壳护甲"
        BAT = "Đừng ăn sống dơi!",		-- 物品名:"洞穴蝙蝠"
        BATBAT = "Động thủ nào!",		-- 物品名:"蝙蝠棒" 制造描述:"所有科技都如此...耗费精神"
        BATWING = "Hết dơi rồi",		-- 物品名:"洞穴蝙蝠翅膀"
        BATWING_COOKED = "Ghê...",		-- 物品名:"烤蝙蝠翅膀"
        BATCAVE = "Ta không sợ!",		-- 物品名:"蝙蝠洞"
        BEDROLL_FURRY = "Lông xù...",		-- 物品名:"毛皮铺盖" 制造描述:"舒适地一觉睡到天亮！"
        BUNNYMAN = "Hay là chúng ta cùng nhau đi phơi nắng đi!",		-- 物品名:"兔人"
        FLOWER_CAVE = "Đóa hoa vô dụng",		-- 物品名:"荧光花"
        GUANO = "Dơi mà cũng đi đại tiện",		-- 物品名:"鸟粪"
        LANTERN = "Thắp sáng đi!",		-- 物品名:"提灯" 制造描述:"可加燃料、明亮、便携！"
        LIGHTBULB = "Ngươi nói \"Đừng bỏ vào miệng\" là có ý gì?",		-- 物品名:"荧光果"
        MANRABBIT_TAIL = "Hả? Không phải là ta làm rơi chứ!",		-- 物品名:"兔绒"
        MUSHROOMHAT = "Khiến cho cái đầu vừa đẹp vừa dính!",		-- 物品名:"蘑菇帽"
        MUSHROOM_LIGHT2 =
        {
            ON = "Ô, màu sắc đẹp!",		-- 物品名:"炽菇灯"->开启 制造描述:"受到火山岩浆灯饰学问的激发"
            OFF = "Nó có tác dụng gì không?",		-- 物品名:"炽菇灯"->关闭 制造描述:"受到火山岩浆灯饰学问的激发"
            BURNT = "Từng rất đẹp...",		-- 物品名:"炽菇灯"->烧焦的 制造描述:"受到火山岩浆灯饰学问的激发"
        },
        MUSHROOM_LIGHT =
        {
            ON = "Phát sáng!",		-- 物品名:"蘑菇灯"->开启 制造描述:"任何蘑菇的完美添加物"
            OFF = "Hình dáng của nó rất kỳ lạ",		-- 物品名:"蘑菇灯"->关闭 制造描述:"任何蘑菇的完美添加物"
            BURNT = "Không phải do ta làm!!",		-- 物品名:"蘑菇灯"->烧焦的 制造描述:"任何蘑菇的完美添加物"
        },
        SLEEPBOMB = "He he, ngủ ngon!",		-- 物品名:"睡袋" 制造描述:"可以扔掉的袋子睡意沉沉"
        MUSHROOMBOMB = "Chạy mau!",		-- 物品名:"炸弹蘑菇"
        SHROOM_SKIN = "Da lồi lõm",		-- 物品名:"蘑菇皮"
        TOADSTOOL_CAP =
        {
            EMPTY = "Xin chào? Có ai ở nhà không?",		-- 物品名:"毒菌蟾蜍"
            INGROUND = "Chúng ta gặp nhau ở bên trong",		-- 物品名:"毒菌蟾蜍"->在地里面
            GENERIC = "Cái mũ của ngươi rất tốt",		-- 物品名:"毒菌蟾蜍"->默认
        },
        TOADSTOOL =
        {
            GENERIC = "Ta muốn tránh xa nó",		-- 物品名:"毒菌蟾蜍"->默认
            RAGE = "Chắc chúng ta chọc giận nó rồi!",		-- 物品名:"毒菌蟾蜍"->愤怒
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "Bào tử nấm!",		-- 物品名:"孢子帽"->默认
            BURNT = "Ôi...",		-- 物品名:"孢子帽"->烧焦的
        },
        MUSHTREE_TALL =
        {
            GENERIC = "Mùi vị có vẻ ngon",		-- 物品名:"蓝蘑菇树"->默认
            BLOOM = "Có thứ vừa rớt xuống",		-- 物品名:"蓝蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "Lớn thật!",		-- 物品名:"红蘑菇树"->默认
            BLOOM = "Thật là đẹp!",		-- 物品名:"红蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "Thân gỗ thấp thấp",		-- 物品名:"绿蘑菇树"->默认
            BLOOM = "Cái đèn lơ lửng",		-- 物品名:"绿蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_TALL_WEBBED = "Cây này bị người nhện chiếm rồi",		-- 物品名:"蛛网蓝蘑菇树"
        SPORE_TALL =
        {
            GENERIC = "Thật là đẹp!",		-- 物品名:"蓝色孢子"->默认
            HELD = "Lông mao lơ lửng giờ là của ta!",		-- 物品名:"蓝色孢子"->拿在手里
        },
        SPORE_MEDIUM =
        {
            GENERIC = "Có thể bay lên!",		-- 物品名:"红色孢子"->默认
            HELD = "Lông mao lơ lửng giờ là của ta!",		-- 物品名:"红色孢子"->拿在手里
        },
        SPORE_SMALL =
        {
            GENERIC = "Nó đang nhảy múa!",		-- 物品名:"绿色孢子"->默认
            HELD = "Lông mao lơ lửng giờ là của ta!",		-- 物品名:"绿色孢子"->拿在手里
        },
        RABBITHOUSE =
        {
            GENERIC = "Xin chào, có bé thỏ ở nhà không (●'◡'●)",		-- 物品名:"兔屋"->默认 制造描述:"可容纳一只巨大的兔子及其所有物品"
            BURNT = "Tuy hơi tiếc...... nhưng lại muốn ăn nó!",		-- 物品名:"兔屋"->烧焦的 制造描述:"可容纳一只巨大的兔子及其所有物品"
        },
        SLURTLE = "Rất giống với người bạn của ta",		-- 物品名:"尖壳蜗牛"
        SLURTLE_SHELLPIECES = "Ôi, nó hư rồi!",		-- 物品名:"蜗壳碎片"
        SLURTLEHAT = "Bây giờ cái vỏ là của ta!",		-- 物品名:"蜗牛帽"
        SLURTLEHOLE = "Ôi, dính dính!",		-- 物品名:"尖壳蜗牛窝"
        SLURTLESLIME = "Chất nhầy có thể nổ!",		-- 物品名:"尖壳蜗牛黏液"
        SNURTLE = "Cái mặt đáng tin cậy",		-- 物品名:"圆壳蜗牛"
        SPIDER_HIDER = "Con nhện thần thánh!!",		-- 物品名:"洞穴蜘蛛"
        SPIDER_SPITTER = "Đờm do con nhện nhả ra!",		-- 物品名:"喷射蜘蛛"
        SPIDERHOLE = "Hang của người nhện!",		-- 物品名:"蛛网岩"
        SPIDERHOLE_ROCK = "Xin chào, có bé thỏ ở nhà không",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品"
        STALAGMITE = "Nham thạch hang động",		-- 物品名:"石笋"
        STALAGMITE_TALL = "Nham thạch to lớn trong hang động",		-- 物品名:"石笋"
        TREASURECHEST_TRAP = "Kho báu!",		-- 物品名:"宝箱"
        TURF_CARPETFLOOR = "Nền đất lông mềm mại",		-- 物品名:"地毯地板" 制造描述:"超级柔软气味像牦牛"
        TURF_CHECKERFLOOR = "Món đồ trên mặt đất",		-- 物品名:"棋盘地板" 制造描述:"精心制作成棋盘状的大理石地砖"
        TURF_DIRT = "Món đồ trên mặt đất",		-- 物品名:"泥土地皮"
        TURF_FOREST = "Món đồ trên mặt đất",		-- 物品名:"森林地皮"
        TURF_GRASS = "Món đồ trên mặt đất",		-- 物品名:"长草地皮"
        TURF_MARSH = "Món đồ trên mặt đất",		-- 物品名:"沼泽地皮" 制造描述:"沼泽在哪，家就在哪！"
        TURF_METEOR = "Món đồ trên mặt đất",		-- 物品名:"月坑地皮" 制造描述:"月球表面的月坑"
        TURF_PEBBLEBEACH = "Món đồ trên mặt đất",		-- 物品名:"岩石海滩地皮"
        TURF_ROAD = "Làm cho mặt đất dễ di chuyển hơn!",		-- 物品名:"卵石路" 制造描述:"修建你自己的道路，通往任何地方"
        TURF_ROCKY = "Món đồ trên mặt đất",		-- 物品名:"岩石地皮"
        TURF_SAVANNA = "Món đồ trên mặt đất",		-- 物品名:"热带草原地皮"
        TURF_WOODFLOOR = "Tấm gỗ",		-- 物品名:"木地板" 制造描述:"优质复合地板"
		TURF_CAVE="Món đồ trên mặt đất",		-- 物品名:"鸟粪地皮"
		TURF_FUNGUS="Món đồ trên mặt đất",		-- 物品名:"菌类地皮"
		TURF_SINKHOLE="Món đồ trên mặt đất",		-- 物品名:"黏滑地皮"
		TURF_UNDERROCK="Món đồ trên mặt đất",		-- 物品名:"洞穴岩石地皮"
		TURF_MUD="Món đồ trên mặt đất",		-- 物品名:"泥泞地皮"
		TURF_DECIDUOUS = "Món đồ trên mặt đất",		-- 物品名:"桦树地皮"
		TURF_SANDY = "Có bé thỏ ở nhà không",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品"
		TURF_BADLANDS = "Món đồ trên mặt đất",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品"
		TURF_DESERTDIRT = "Món đồ trên mặt đất",		-- 物品名:"沙漠地皮"
		TURF_FUNGUS_GREEN = "Món đồ trên mặt đất",		-- 物品名:"菌类地皮"
		TURF_FUNGUS_RED = "Món đồ trên mặt đất",		-- 物品名:"菌类地皮"
		TURF_DRAGONFLY = "Mặt đất có vảy!",		-- 物品名:"鳞状地板" 制造描述:"消除火灾蔓延速度"
		POWCAKE = "Ăn vào sẽ đau bụng, nhưng ngửi lại thấy thơm...",		-- 物品名:"芝士蛋糕"
        CAVE_ENTRANCE = "Bị cục đá cản đường",		-- 物品名:"堵住的陷洞"
        CAVE_ENTRANCE_RUINS = "Phía dưới có gì nhỉ?",		-- 物品名:"被堵住的陷洞"->单机 洞二入口
       	CAVE_ENTRANCE_OPEN =
        {
            GENERIC = "Phía dưới có đồ",		-- 物品名:"陷洞"->默认
            OPEN = "Muốn xem bên trong có gì!",		-- 物品名:"陷洞"->打开
            FULL = "Để ta vào xem!",		-- 物品名:"陷洞"->满了
        },
        CAVE_EXIT =
        {
            GENERIC = "Cũng thích phía dưới",		-- 物品名:"楼梯"->默认
            OPEN = "Nhớ thế giới bên ngoài",		-- 物品名:"楼梯"->打开
            FULL = "Cho ta ra ngoài!",		-- 物品名:"楼梯"->满了
        },
		MAXWELLPHONOGRAPH = "Âm nhạc phát ra từ bên trong!",		-- 物品名:"麦斯威尔的留声机"->单机 麦斯威尔留声机
		BOOMERANG = "Tấm gỗ quay vòng!",		-- 物品名:"回旋镖" 制造描述:"来自澳洲土著"
		PIGGUARD = "Hắn còn đáng sợ hơn người khác",		-- 物品名:"猪人守卫"
		ABIGAIL =
		{
            LEVEL1 =
            {
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
            },
            LEVEL2 =
            {
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
            },
            LEVEL3 =
            {
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
                "Ngươi... là người tốt phải không?",		-- 物品名:未找到 制造描述:未找到
            },
		},
		ADVENTURE_PORTAL = "Cái này sẽ đi đâu?",		-- 物品名:"麦斯威尔之门"->单机 麦斯威尔之门
		AMULET = "Đá phép thuật rất đẹp!",		-- 物品名:"重生护符" 制造描述:"逃离死神的魔爪"
		ANIMAL_TRACK = "Đi tlợn!",		-- 物品名:"动物足迹"
		ARMORGRASS = "Áo mưa bằng rơm!",		-- 物品名:"草甲" 制造描述:"提供少许防护"
		ARMORMARBLE = "Nặng thật đó...",		-- 物品名:"大理石甲" 制造描述:"它很重，但能够保护你"
		ARMORWOOD = "Áo gỗ!",		-- 物品名:"木甲" 制造描述:"为你抵御部分伤害"
		ARMOR_SANITY = "Nó đang thôn phệ linh hồn ta!!",		-- 物品名:"魂甲" 制造描述:"保护你的躯体，但无法保护你的心智"
		ASH =
		{
			GENERIC = "Hoa lan đã thành tro tàn, nhưng hương thơm ấy vẫn còn lưu lại trong Mạc Phủ",		-- 物品名:"灰烬"->默认
			REMAINS_GLOMMERFLOWER = "Ôi... không còn gì nữa",		-- 物品名:"灰烬"->单机专用
			REMAINS_EYE_BONE = "Tạm biệt, gậy lấp lánh",		-- 物品名:"灰烬"->单机专用
			REMAINS_THINGIE = "Hết rồi",		-- 物品名:"灰烬"->单机专用
		},
		AXE = "Xem ta dùng rìu khắc hoa củ cải cho ngươi",		-- 物品名:"斧头" 制造描述:"砍倒树木！"
		BABYBEEFALO =
		{
			GENERIC = "Nhỏ ghê",		-- 物品名:"小牦牛"->默认
		    SLEEPING = "Ngủ ngon!",		-- 物品名:"小牦牛"->睡着了
        },
        BUNDLE = "Để lại sau này dùng",		-- 物品名:"捆绑物资"
        BUNDLEWRAP = "Có thể gói rất nhiều đồ!",		-- 物品名:"捆绑包装" 制造描述:"打包你的东西的部分和袋子"
		BACKPACK = "Ta có hành lý rồi!",		-- 物品名:"背包" 制造描述:"携带更多物品"
		BACONEGGS = "Mau đem nó đi!",		-- 物品名:"培根煎蛋"
		BANDAGE = "Khiến cho vết thương đỡ hơn chút",		-- 物品名:"蜂蜜药膏" 制造描述:"愈合小伤口"
		BASALT = "Cục đá cứng!!",		-- 物品名:"玄武岩"
		BEARDHAIR = "Nó thật kỳ lạ...",		-- 物品名:"胡子"
		BEARGER = "Chạy mau!!",		-- 物品名:"狂暴熊獾"
		BEARGERVEST = "Cảm thấy ấm áp, mềm mại...",		-- 物品名:"熊皮背心" 制造描述:"熊皮背心"
		ICEPACK = "Cái túi mềm mại!",		-- 物品名:"保鲜背包" 制造描述:"容量虽小，但能保持东西新鲜"
		BEARGER_FUR = "Lông mềm với các đường vân",		-- 物品名:"熊皮" 制造描述:"毛皮再生"
		BEDROLL_STRAW = "Ngủ một giấc thật ấm áp",		-- 物品名:"草席卷" 制造描述:"一觉睡到天亮"
		BEEQUEEN = "Nghe nói mật của cô ấy là ngọt nhất!",		-- 物品名:"蜂王"
		BEEQUEENHIVE =
		{
			GENERIC = "Bên trong có mật không?",		-- 物品名:"蜂蜜地块"->默认
			GROWING = "Có hơi kỳ lạ...",		-- 物品名:"蜂蜜地块"->正在生长
		},
        BEEQUEENHIVEGROWN = "Có cảm giác không tốt lành gì...",		-- 物品名:"巨大蜂巢"
        BEEGUARD = "Con ong lớn!",		-- 物品名:"嗡嗡蜜蜂"
        HIVEHAT = "Ta là ong chúa của các ngươi!",		-- 物品名:"蜂王帽"
        MINISIGN =
        {
            GENERIC = "Ô, hình ảnh!",		-- 物品名:"小木牌"->默认
            UNDRAWN = "Phía trên không có gì",		-- 物品名:"小木牌"->没有画画
        },
        MINISIGN_ITEM = "Để nó ở đâu đây?",		-- 物品名:"小木牌" 制造描述:"用羽毛笔在这些上面画画"
		BEE =
		{
			GENERIC = "Ong chúa lấy nhụy trăm hoa, cực khổ vì ai, ngọt vì ai",		-- 物品名:"蜜蜂"->默认
			HELD = "Ha! Ngươi giờ trong túi ta rồi!",		-- 物品名:"蜜蜂"->拿在手里
		},
		BEEBOX =
		{
			READY = "Lấy mật lấy mật!!",		-- 物品名:"蜂箱"->准备好的 满的 制造描述:"贮存你自己的蜜蜂"
			FULLHONEY = "Để ta làm chút món ngọt nào!",		-- 物品名:"蜂箱"->蜂蜜满了 制造描述:"贮存你自己的蜜蜂"
			GENERIC = "Nhà của ong nhỏ",		-- 物品名:"蜂箱"->默认 制造描述:"贮存你自己的蜜蜂"
			NOHONEY = "Ôi, bên trong không có mật",		-- 物品名:"蜂箱"->没有蜂蜜 制造描述:"贮存你自己的蜜蜂"
			SOMEHONEY = "Không muốn đợi đâu!!",		-- 物品名:"蜂箱"->有一些蜂蜜 制造描述:"贮存你自己的蜜蜂"
			BURNT = "Nó hết kêu vo ve rồi...",		-- 物品名:"蜂箱"->烧焦的 制造描述:"贮存你自己的蜜蜂"
		},
		MUSHROOM_FARM =
		{
			STUFFED = "Hết chỗ trồng nấm rồi!",		-- 物品名:"蘑菇农场"->塞，满了？？ 制造描述:"种蘑菇"
			LOTS = "Ôi, rất nhiều món ngon đang lớn lên!",		-- 物品名:"蘑菇农场"->很多 制造描述:"种蘑菇"
			SOME = "Nó bắt đầu lớn rồi!",		-- 物品名:"蘑菇农场"->有一些 制造描述:"种蘑菇"
			EMPTY = "Cần nấm!",		-- 物品名:"蘑菇农场" 制造描述:"种蘑菇"
			ROTTEN = "Cần nhiều gỗ sống!",		-- 物品名:"蘑菇农场"->腐烂的--需要活木 制造描述:"种蘑菇"
			BURNT = "Cháy quá, không trồng nấm được",		-- 物品名:"蘑菇农场"->烧焦的 制造描述:"种蘑菇"
			SNOWCOVERED = "Có lẽ nấm đang ngủ?",		-- 物品名:"蘑菇农场"->被雪覆盖 制造描述:"种蘑菇"
		},
		BEEFALO =
		{
			FOLLOWER = "Đi theo ta!",		-- 物品名:"牦牛"->追随者
			GENERIC = "Lông mềm mại!",		-- 物品名:"牦牛"->默认
			NAKED = "Khoác bộ lông lên nhìn thật đẹp",		-- 物品名:"牦牛"->牛毛被刮干净了
			SLEEPING = "Ngủ ngon, lông mềm",		-- 物品名:"牦牛"->睡着了
            DOMESTICATED = "Nó là bạn",		-- 物品名:"牦牛"->驯化牛
            ORNERY = "Đừng tức giận!!",		-- 物品名:"牦牛"->战斗牛
            RIDER = "Muốn cưỡi!",		-- 物品名:"牦牛"->骑行牛
            PUDGY = "Lông mềm trông lớn ghê",		-- 物品名:"牦牛"->胖牛
		},
		BEEFALOHAT = "Cái sừng to lớn!",		-- 物品名:"牛角帽" 制造描述:"成为牛群中的一员！连气味也变得一样"
		BEEFALOWOOL = "Lông mềm nhỏ",		-- 物品名:"牛毛"
		BEEHAT = "Đội cái đó vào thì không thấy gì hết!",		-- 物品名:"养蜂帽" 制造描述:"防止被愤怒的蜜蜂蜇伤"
        BEESWAX = "Lấy trộm từ tổ ong đó!",		-- 物品名:"蜂蜡" 制造描述:"一种有用的防腐蜂蜡"
		BEEHIVE = "Nhà của ong",		-- 物品名:"野生蜂窝"
		BEEMINE = "Cẩn thận!",		-- 物品名:"蜜蜂地雷" 制造描述:"变成武器的蜜蜂会出什么问题？"
		BEEMINE_MAXWELL = "Không thích nó kêu vo ve",		-- 物品名:"麦斯威尔的蚊子陷阱"->单机 麦斯威尔的蚊子陷阱
		BERRIES = "Trái cây ngọt!",		-- 物品名:"浆果"
		BERRIES_COOKED = "Nướng lên càng ngọt hơn,",		-- 物品名:"烤浆果"
        BERRIES_JUICY = "Ô, vừa to vừa ngon!",		-- 物品名:"多汁浆果"
        BERRIES_JUICY_COOKED = "Muốn ăn hết!",		-- 物品名:"烤多汁浆果"
		BERRYBUSH =
		{
			BARREN = "Bón ít phân lên trên",		-- 物品名:"浆果丛"
			WITHERED = "Khô quá rồi",		-- 物品名:"浆果丛"->枯萎了
			GENERIC = "Món ăn vặt!",		-- 物品名:"浆果丛"->默认
			PICKED = "Phải đợi mới ra lại",		-- 物品名:"浆果丛"->被采完了
			DISEASED = "Bị bệnh nên không ra quả",		-- 物品名:"浆果丛"->生病了
			DISEASING = "Trông không đúng lắm",		-- 物品名:"浆果丛"->正在生病？？
			BURNING = "Tạm biệt",		-- 物品名:"浆果丛"->正在燃烧
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "Cần bón phân",		-- 物品名:"多汁浆果丛"
			WITHERED = "Nó không thích nóng",		-- 物品名:"多汁浆果丛"->枯萎了
			GENERIC = "Ăn nhanh thôi!",		-- 物品名:"多汁浆果丛"->默认
			PICKED = "Có thể ăn vặt! Có thể ăn vặt!",		-- 物品名:"多汁浆果丛"->被采完了
			DISEASED = "Trông có vẻ không đúng lắm",		-- 物品名:"多汁浆果丛"->生病了
			DISEASING = "Trông không đúng lắm",		-- 物品名:"多汁浆果丛"->正在生病？？
			BURNING = "Nóng nóng nóng!",		-- 物品名:"多汁浆果丛"->正在燃烧
		},
		BIGFOOT = "Cái chân to!",		-- 物品名:"大脚怪"->单机 大脚怪
		BIRDCAGE =
		{
			GENERIC = "Xây căn nhà ấm áp cho bé chim!",		-- 物品名:"鸟笼"->默认 制造描述:"为你的鸟类朋友提供一个幸福的家"
			OCCUPIED = "Bây giờ chim không cần đi đâu nữa!",		-- 物品名:"鸟笼"->被占领 制造描述:"为你的鸟类朋友提供一个幸福的家"
			SLEEPING = "Ngủ ngon, bé chim",		-- 物品名:"鸟笼"->睡着了 制造描述:"为你的鸟类朋友提供一个幸福的家"
			HUNGRY = "Con chim muốn ăn",		-- 物品名:"鸟笼"->有点饿了 制造描述:"为你的鸟类朋友提供一个幸福的家"
			STARVING = "Trông nó rất đói...",		-- 物品名:"鸟笼"->挨饿 制造描述:"为你的鸟类朋友提供一个幸福的家"
			DEAD = "Ôi, nó chết rồi",		-- 物品名:"鸟笼"->死了 制造描述:"为你的鸟类朋友提供一个幸福的家"
			SKELETON = "...",		-- 物品名:"骷髅"
		},
		BIRDTRAP = "Dùng để bắt chim!",		-- 物品名:"捕鸟陷阱" 制造描述:"捕捉会飞的动物"
		CAVE_BANANA_BURNT = "Đã phá hủy",		-- 物品名:"鸟笼"->烧焦的洞穴香蕉树 制造描述:"为你的鸟类朋友提供一个幸福的家"
		BIRD_EGG = "Có chim ở đó không",		-- 物品名:"鸟蛋"
		BIRD_EGG_COOKED = "Không muốn ăn!",		-- 物品名:"煎蛋"
		BISHOP = "Người thiết hoạt bát!!",		-- 物品名:"发条主教"
		BLOWDART_FIRE = "Muốn thử xem!",		-- 物品名:"火焰吹箭" 制造描述:"向你的敌人喷火"
		BLOWDART_SLEEP = "Đến lúc phải ngủ rồi!",		-- 物品名:"催眠吹箭" 制造描述:"催眠你的敌人"
		BLOWDART_PIPE = "Biu biu biu！",		-- 物品名:"吹箭" 制造描述:"向你的敌人喷射利齿"
		BLOWDART_YELLOW = "Thật dễ dàng!",		-- 物品名:"雷电吹箭" 制造描述:"朝你的敌人放闪电"
		BLUEAMULET = "Lạnh ghê!",		-- 物品名:"寒冰护符" 制造描述:"多么冰冷酷炫的护符"
		BLUEGEM = "Cục đá đẹp",		-- 物品名:"蓝宝石"
		BLUEPRINT =
		{
            COMMON = "Phía trên có hình vẽ!",		-- 物品名:"蓝图"
            RARE = "Trông phức tạp thật",		-- 物品名:"蓝图"->罕见的
        },
        SKETCH = "Ô, bộ xương!",		-- 物品名:"{item}骨架"
		BLUE_CAP = "Tốt cho bụng, không tốt cho tinh thần",		-- 物品名:"采摘的蓝蘑菇"
		BLUE_CAP_COOKED = "Ngửi thì thấy khác...",		-- 物品名:"烤蓝蘑菇"
		BLUE_MUSHROOM =
		{
			GENERIC = "Đồ ăn",		-- 物品名:"蓝蘑菇"->默认
			INGROUND = "Đợi màu sắc sẫm chút rồi quay lại",		-- 物品名:"蓝蘑菇"->在地里面
			PICKED = "Cây lớn rồi!",		-- 物品名:"蓝蘑菇"->被采完了
		},
		BOARDS = "Nguyên liệu chặt từ cây",		-- 物品名:"木板" 制造描述:"更平直的木头"
		BONESHARD = "Di hài ai đó",		-- 物品名:"骨头碎片"
		BONESTEW = "Thứ dơ bẩn phàm tục!",		-- 物品名:"炖肉汤"
		BUGNET = "Muốn bắt sâu!",		-- 物品名:"捕虫网" 制造描述:"抓虫子用的"
		BUSHHAT = "Không nhìn thấy ta!!",		-- 物品名:"灌木丛帽" 制造描述:"很好的伪装！"
		BUTTER = "Mùi giống hoa... thơm",		-- 物品名:"黄油"
		BUTTERFLY =
		{
			GENERIC = "Đêm đến bướm bay vào rèm thưa!",		-- 物品名:"蝴蝶"->默认
			HELD = "Bắt được rồi",		-- 物品名:"蝴蝶"->拿在手里
		},
		BUTTERFLYMUFFIN = "Bánh xốp thơm ngon!",		-- 物品名:"蝴蝶松饼"
		BUTTERFLYWINGS = "Thật tội nghiệp!",		-- 物品名:"蝴蝶翅膀"
		BUZZARD = "Con chim đầu trọc!",		-- 物品名:"秃鹫"
		SHADOWDIGGER = "Nó dùng loại phép thuật xấu xa!!",		-- 物品名:"蝴蝶"
		CACTUS =
		{
			GENERIC = "Mọc đầy gai",		-- 物品名:"仙人掌"->默认
			PICKED = "Mọc lại nào!",		-- 物品名:"仙人掌"->被采完了
		},
		CACTUS_MEAT_COOKED = "Ôi, đến giờ ăn rồi!",		-- 物品名:"烤仙人掌肉"
		CACTUS_MEAT = "Ối! Vẫn còn nhiều gai",		-- 物品名:"仙人掌肉"
		CACTUS_FLOWER = "Hình dạng xấu nhưng rất ngon",		-- 物品名:"仙人掌花"
		COLDFIRE =
		{
			EMBERS = "Sắp tắt lửa rồi",		-- 物品名:"冰火"->即将熄灭 制造描述:"这火是越烤越冷的冰火"
			GENERIC = "Chúng ta cùng kể chuyện quanh đống lửa đi,",		-- 物品名:"冰火"->默认 制造描述:"这火是越烤越冷的冰火"
			HIGH = "Lửa lớn!",		-- 物品名:"冰火"->高 制造描述:"这火是越烤越冷的冰火"
			LOW = "Lửa nhỏ lại rồi",		-- 物品名:"冰火"->低？ 制造描述:"这火是越烤越冷的冰火"
			NORMAL = "Lửa lạnh?",		-- 物品名:"冰火"->普通 制造描述:"这火是越烤越冷的冰火"
			OUT = "Tạm biệt",		-- 物品名:"冰火"->出去？外面？ 制造描述:"这火是越烤越冷的冰火"
		},
		CAMPFIRE =
		{
			EMBERS = "Sắp tắt lửa rồi",		-- 物品名:"营火"->即将熄灭 制造描述:"燃烧时发出光亮"
			GENERIC = "Chúng ta cùng kể chuyện quanh đống lửa đi,",		-- 物品名:"营火"->默认 制造描述:"燃烧时发出光亮"
			HIGH = "Lửa lớn!",		-- 物品名:"营火"->高 制造描述:"燃烧时发出光亮"
			LOW = "Lửa nhỏ lại rồi",		-- 物品名:"营火"->低？ 制造描述:"燃烧时发出光亮"
			NORMAL = "Ấm áp thoải mái",		-- 物品名:"营火"->普通 制造描述:"燃烧时发出光亮"
			OUT = "Tạm biệt",		-- 物品名:"营火"->出去？外面？ 制造描述:"燃烧时发出光亮"
		},
		CANE = "Ngươi thấy ta cần thứ này không?",		-- 物品名:"拐棍" 制造描述:"泰然自若地快步走"
		CATCOON = "Ngươi thật là đáng yêu",		-- 物品名:"浣熊猫"
		CATCOONDEN =
		{
			GENERIC = "Thứ bên trong đang cào vỏ",		-- 物品名:"空心树桩"->默认
			EMPTY = "Không có ai bên trong",		-- 物品名:"空心树桩"
		},
		CATCOONHAT = "Ha ha, bây giờ là một cái mũ",		-- 物品名:"浣熊猫帽子" 制造描述:"适合那些重视温暖甚于朋友的人"
		COONTAIL = "Giờ là của ta!",		-- 物品名:"浣熊猫尾巴"
		CARROT = "Cà rốt thơm giòn!",		-- 物品名:"胡萝卜"
		CARROT_COOKED = "Khiến nó thơm giòn ngon miệng hơn",		-- 物品名:"烤胡萝卜"
		CARROT_PLANTED = "Lớn lên nào! Lớn lên nào!",		-- 物品名:"胡萝卜"
		CARROT_SEEDS = "Trồng một hạt, thu cả giỏ!",		-- 物品名:"胡萝卜种子"
		CARTOGRAPHYDESK =
		{
			GENERIC = "Ta cần bản đồ!",		-- 物品名:"制图桌"->默认 制造描述:"准确地告诉别人你去过哪里"
			BURNING = "Tạm biệt!",		-- 物品名:"制图桌"->正在燃烧 制造描述:"准确地告诉别人你去过哪里"
			BURNT = "Không còn gì!",		-- 物品名:"制图桌"->烧焦的 制造描述:"准确地告诉别人你去过哪里"
		},
		WATERMELON_SEEDS = "Có thê trồng được rất nhiều dưa hấu!",		-- 物品名:"西瓜种子"
		CAVE_FERN = "Thực vật tươi tốt trong hang động",		-- 物品名:"蕨类植物"
		CHARCOAL = "Sẽ khiến cho móng tay dơ",		-- 物品名:"木炭"
        CHESSPIECE_PAWN = "Một quân cờ nhỏ nhất trong đó...",		-- 物品名:"卒子棋子"
        CHESSPIECE_ROOK =
        {
            GENERIC = "Tòa thành này nhỏ ghê",		-- 物品名:"战车棋子"->默认
            STRUGGLE = "Khởi động!",		-- 物品名:"战车棋子"->三基佬棋子晃动
        },
        CHESSPIECE_KNIGHT =
        {
            GENERIC = "Trông giống người thiết",		-- 物品名:"骑士棋子"->默认
            STRUGGLE = "Khởi động!",		-- 物品名:"骑士棋子"->三基佬棋子晃动
        },
        CHESSPIECE_BISHOP =
        {
            GENERIC = "Nhọn nhọn!",		-- 物品名:"主教棋子"->默认
            STRUGGLE = "Khởi động!",		-- 物品名:"主教棋子"->三基佬棋子晃动
        },
        CHESSPIECE_MUSE = "Nữ sĩ đáng sợ",		-- 物品名:"女王棋子"
        CHESSPIECE_FORMAL = "Tại những người quanh thứ này đều trở nên kỳ lạ vậy",		-- 物品名:"国王棋子"
        CHESSPIECE_HORNUCOPIA = "Tất cả đều ngon!",		-- 物品名:"丰饶角雕刻"
        CHESSPIECE_PIPE = "Không dễ thương chút nào, nhìn kỳ kỳ",		-- 物品名:"泡泡烟斗雕塑"
        CHESSPIECE_DEERCLOPS = "Sẽ không gây phiền phức cho chúng ta nữa",		-- 物品名:"独眼巨鹿棋子"
        CHESSPIECE_BEARGER = "！",		-- 物品名:"熊獾棋子"
        CHESSPIECE_MOOSEGOOSE =
        {
            "Ha ha! Nhìn mặt cô ta kìa!",		-- 物品名:"驼鹿棋子" 制造描述:未找到
        },
        CHESSPIECE_DRAGONFLY = "Con mắt mở to, trông thật kỳ bí",		-- 物品名:"龙蝇棋子"
        CHESSPIECE_BUTTERFLY = "Xấu quá",		-- 物品名:"月蛾棋子"
        CHESSPIECE_ANCHOR = "Sao trên mặt đất có cái neo?",		-- 物品名:"锚棋子"
        CHESSPIECE_MOON = "Trông rất giống thật!",		-- 物品名:"\\“月亮\\” 棋子"
        CHESSPIECE_CARRAT = "Ha ha, trông rất ngon",		-- 物品名:"胡萝卜鼠雕像"
        CHESSJUNK1 = "Hư hết rồi",		-- 物品名:"损坏的发条装置"
        CHESSJUNK2 = "Không phải do ta làm hư!!",		-- 物品名:"损坏的发条装置"
        CHESSJUNK3 = "Rối toàn bộ lên",		-- 物品名:"损坏的发条装置"
		CHESTER = "Cái túi lông lá, ta thích ngươi",		-- 物品名:"切斯特"
		CHESTER_EYEBONE =
		{
			GENERIC = "Nó đang nhìn ta!",		-- 物品名:"眼骨"->默认
			WAITING = "Dậy đi!",		-- 物品名:"眼骨"->需要等待
		},
		COOKEDMANDRAKE = "Không phải nhân sâm có hình mặt em bé sao, sao cái này lại là ông lão?",		-- 物品名:"烤曼德拉草"
		COOKEDMEAT = "Tiên nữ không ăn thịt đâu",		-- 物品名:"烤大肉"
		COOKEDMONSTERMEAT = "Ta không muốn làm khó bản thân vậy đâu",		-- 物品名:"烤怪物肉"
		COOKEDSMALLMEAT = "Ta không ăn",		-- 物品名:"烤小肉"
		COOKPOT =
		{
			COOKING_LONG = "Đồ ngon cần phải đợi...",		-- 物品名:"烹饪锅"->饭还需要很久 制造描述:"制作更好的食物"
			COOKING_SHORT = "Sắp xong rồi! Đợi chút đi",		-- 物品名:"烹饪锅"->饭快做好了 制造描述:"制作更好的食物"
			DONE = "Giờ ăn đã đến!",		-- 物品名:"烹饪锅"->完成了 制造描述:"制作更好的食物"
			EMPTY = "Hy vọng bên trong có đồ ăn",		-- 物品名:"烹饪锅" 制造描述:"制作更好的食物"
			BURNT = "Ôi...",		-- 物品名:"烹饪锅"->烧焦的 制造描述:"制作更好的食物"
		},
		CORN = "Rỉa từng hạt bắp, món ăn vặt giòn rụm",		-- 物品名:"玉米"
		CORN_COOKED = "Nổ rồi!",		-- 物品名:"爆米花"
		CORN_SEEDS = "Có thể trồng được rất nhiều bắp!",		-- 物品名:"玉米种子"
        CANARY =
		{
			GENERIC = "Chim vàng",		-- 物品名:"金丝雀"->默认
			HELD = "Bắt được ngươi rồi!",		-- 物品名:"金丝雀"->拿在手里
		},
        CANARY_POISONED = "Nó sao rồi?",		-- 物品名:"金丝雀（有翅膀的动物）"
		CRITTERLAB = "Nhìn thấy bên trong có thứ đang động đậy",		-- 物品名:"岩石巢穴"
        CRITTER_GLOMLING = "Con sâu thích nhảy",		-- 物品名:"小罗姆"
        CRITTER_DRAGONLING = "Người bạn tốt chúng ta phải đoàn kết!",		-- 物品名:"蜂群卫士"
		CRITTER_LAMB = "Đừng lo lắng, ta sẽ bảo vệ ngươi!",		-- 物品名:"小钢羊"
        CRITTER_PUPPY = "Muốn chơi chung không?",		-- 物品名:"小座狼"
        CRITTER_KITTEN = "... Ta đoán ngươi cũng rất tốt đó",		-- 物品名:"小浣猫"
        CRITTER_PERDLING = "Xin chào, con chim con!",		-- 物品名:"小火鸡"
		CRITTER_LUNARMOTHLING = "Ta thích ngươi",		-- 物品名:"小蛾子"
		CROW =
		{
			GENERIC = "Một con chim đen",		-- 物品名:"乌鸦"->默认
			HELD = "Ta sẽ tìm nhà mới cho ngươi",		-- 物品名:"乌鸦"->拿在手里
		},
		CUTGRASS = "Có thể chế ra rất nhiều thứ",		-- 物品名:"草"
		CUTREEDS = "Trong đầm lầy đều là những thứ hữu dụng",		-- 物品名:"采下的芦苇"
		CUTSTONE = "Cục đá và đá chẻ",		-- 物品名:"石砖" 制造描述:"切成正方形的石头"
		DEADLYFEAST = "Không nên ăn cái này",		-- 物品名:"致命的盛宴"->致命盛宴 单机
		DEER =
		{
			GENERIC = "Ngươi nên cắt tóc",		-- 物品名:"无眼鹿"->默认
			ANTLER = "Sừng hươu nhọn",		-- 物品名:"无眼鹿"
		},
        DEER_ANTLER = "Sừng hươu",		-- 物品名:"鹿角"
        DEER_GEMMED = "Đầu hươu không đúng lắm!",		-- 物品名:"无眼鹿"
		DEERCLOPS = "Trông cô ấy không vui",		-- 物品名:"独眼巨鹿"
		DEERCLOPS_EYEBALL = "Ô, muốn rờ",		-- 物品名:"独眼巨鹿眼球"
		EYEBRELLAHAT =	"Nó đang nhìn cái gì vậy?",		-- 物品名:"眼球伞" 制造描述:"面向天空的眼球让你保持干燥"
		DEPLETED_GRASS =
		{
			GENERIC = "Còn lại chút cỏ",		-- 物品名:"草"->默认
		},
        GOGGLESHAT = "Ta là thố tiên tử đi đầu trong lĩnh vực thời trang",		-- 物品名:"时髦的护目镜" 制造描述:"你可以瞪大眼睛看装饰性护目镜"
        DESERTHAT = "Sa mạc đáng ghét, nhưng có cái này thì đỡ hơn chút",		-- 物品名:"沙漠护目镜" 制造描述:"从你的眼睛里把沙子揉出来"
		DEVTOOL = "Thích cái này!",		-- 物品名:"开发工具"
		DEVTOOL_NODEV = "Làm không nổi",		-- 物品名:"草"
		DIRTPILE = "Bên trong có người không?",		-- 物品名:"可疑的土堆"
		DIVININGROD =
		{
			COLD = "Âm thanh nhỏ lại rồi",		-- 物品名:"冻伤"->冷了
			GENERIC = "Cái hộp kỳ lạ ở phía trên là gì vậy?",		-- 物品名:"探测杖"->默认 制造描述:"肯定有方法可以离开这里..."
			HOT = "Nó đang hướng về phía ta mà kêu!",		-- 物品名:"中暑"->热了
			WARM = "Âm thanh càng lúc càng lớn",		-- 物品名:"探测杖"->温暖 制造描述:"肯定有方法可以离开这里..."
			WARMER = "Âm thanh lớn quá rồi!",		-- 物品名:"探测杖" 制造描述:"肯定有方法可以离开这里..."
		},
		DIVININGRODBASE =
		{
			GENERIC = "Đây là gì?",		-- 物品名:"探测杖底座"->默认
			READY = "Còn thiếu vài món...",		-- 物品名:"探测杖底座"->准备好的 满的
			UNLOCKED = "Ta nghĩ bây giờ có thể dùng rồi!",		-- 物品名:"探测杖底座"->解锁了
		},
		DIVININGRODSTART = "Cái thứ này trông rất kỳ lạ",		-- 物品名:"探测杖底座"->单机探测杖底座
		DRAGONFLY = "Con mắt bự của ngươi có thể chế thuốc không?",		-- 物品名:"龙蝇"
		ARMORDRAGONFLY = "Rất nhiều vảy!!",		-- 物品名:"鳞甲" 制造描述:"脾气火爆的盔甲"
		DRAGON_SCALES = "Thật là đẹp...",		-- 物品名:"鳞片"
		DRAGONFLYCHEST = "Thích kiểu dáng của cái hộp này",		-- 物品名:"巨鳞宝箱" 制造描述:"一种结实且防火的容器"
		DRAGONFLYFURNACE =
		{
			HAMMERED = "Làm hư rồi...",		-- 物品名:"龙鳞火炉"->被锤了 制造描述:"给自己建造一个苍蝇暖炉"
			GENERIC = "Ngủ bên cạnh chắc chắn rất ấm", --no gems		-- 物品名:"龙鳞火炉"->默认 制造描述:"给自己建造一个苍蝇暖炉"
			NORMAL = "Khuôn mặt thân thiện", --one gem		-- 物品名:"龙鳞火炉"->普通 制造描述:"给自己建造一个苍蝇暖炉"
			HIGH = "Nóng!! Nóng quá!!", --two gems		-- 物品名:"龙鳞火炉"->高 制造描述:"给自己建造一个苍蝇暖炉"
		},
        HUTCH = "Khuôn mặt nhỏ thật đáng yêu!",		-- 物品名:"哈奇"
        HUTCH_FISHBOWL =
        {
            GENERIC = "Xin chào anh chàng tanh mùi cá!",		-- 物品名:"星空"->默认
            WAITING = "Đang ngủ?",		-- 物品名:"星空"->需要等待
        },
		LAVASPIT =
		{
			HOT = "Cục đá tỏa nhiệt!",		-- 物品名:"中暑"->热了
			COOL = "Giờ biến thành cục đá rồi",		-- 物品名:"龙蝇唾液"
		},
		LAVA_POND = "Quá nóng không thích hợp để bơi",		-- 物品名:"岩浆池"
		LAVAE = "Ôi, nó chỉ là đứa bé",		-- 物品名:"岩浆虫"
		LAVAE_COCOON = "Giờ ngủ trưa đã đến!",		-- 物品名:"冷冻虫卵"
		LAVAE_PET =
		{
			STARVING = "Phải nhanh chóng tìm đồ ăn cho ngươi!",		-- 物品名:"超级可爱岩浆虫"->挨饿
			HUNGRY = "Cần đồ ăn vặt!",		-- 物品名:"超级可爱岩浆虫"->有点饿了
			CONTENT = "Trông rất vui!",		-- 物品名:"超级可爱岩浆虫"->内容？？？、
			GENERIC = "Ôm nó cảm thấy rất ấm áp",		-- 物品名:"超级可爱岩浆虫"->默认
		},
		LAVAE_EGG =
		{
			GENERIC = "Cái thứ bên trong đang cào lớp vỏ",		-- 物品名:"岩浆虫卵"->默认
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "Muốn ôm không?",		-- 物品名:"冻伤"->冷了
			COMFY = "Bên trong rất ấm!",		-- 物品名:"岩浆虫卵"->舒服的
		},
		LAVAE_TOOTH = "Răng sữa của bé rớt rồi",		-- 物品名:"岩浆虫尖牙"
		DRAGONFRUIT = "Muốn nhả hạt sao?",		-- 物品名:"火龙果"
		DRAGONFRUIT_COOKED = "Nếm thử thì thấy giống thuốc",		-- 物品名:"烤火龙果"
		DRAGONFRUIT_SEEDS = "Hạt rất đẹp",		-- 物品名:"火龙果种子"
		DRAGONPIE = "Ôi ôi, đây là cái bánh ngon nhất!!",		-- 物品名:"火龙果派"
		DRUMSTICK = "Thật tội nghiệp",		-- 物品名:"鸟腿"
		DRUMSTICK_COOKED = "Ngươi bỏ chim vào lửa??",		-- 物品名:"炸鸟腿"
		DUG_BERRYBUSH = "Ngươi đi theo ta",		-- 物品名:"浆果丛"
		DUG_BERRYBUSH_JUICY = "Đưa ngươi về nhà",		-- 物品名:"多汁浆果丛"
		DUG_GRASS = "Để cái này ở đâu?",		-- 物品名:"草丛"
		DUG_MARSH_BUSH = "Cảm thấy nhớ nhà",		-- 物品名:"尖刺灌木"
		DUG_SAPLING = "Sẽ tìm cho ngươi một chỗ thật tốt",		-- 物品名:"树苗"
		DURIAN = "Ôi... mùi như phân",		-- 物品名:"榴莲"
		DURIAN_COOKED = "Mùi hôi thật khó ngửi!",		-- 物品名:"超臭榴莲"
		DURIAN_SEEDS = "Hạt thối của trái thối!",		-- 物品名:"榴莲种子"
		EARMUFFSHAT = "Có thể không bắt nó được không/(ㄒoㄒ)/~~",		-- 物品名:"兔耳罩" 制造描述:"毛茸茸的保暖物品"
		EGGPLANT = "Vừa to vừa tím vừa ngon!",		-- 物品名:"茄子"
		EGGPLANT_COOKED = "Nấu ra món có hương vị màu tím đậm đà",		-- 物品名:"烤茄子"
		EGGPLANT_SEEDS = "Mọc ra rất nhiều trái màu tím!",		-- 物品名:"茄子种子"
		ENDTABLE =
		{
			BURNT = "Cô bé nghịch lửa làm đó!",		-- 物品名:"茶几"->烧焦的 制造描述:"一张装饰桌"
			GENERIC = "Hoa trên bàn xấu ghê",		-- 物品名:"茶几"->默认 制造描述:"一张装饰桌"
			EMPTY = "Bỏ cá lên còn đẹp hơn hoa",		-- 物品名:"茶几" 制造描述:"一张装饰桌"
			WILTED = "Cá sẽ không khô héo như vậy",		-- 物品名:"茶几"->枯萎的 制造描述:"一张装饰桌"
			FRESHLIGHT = "Cây đèn của đồng đội",		-- 物品名:"茶几"->茶几-新的发光的 制造描述:"一张装饰桌"
			OLDLIGHT = "Cần một cây đèn mới", -- will be wilted soon, light radius will be very small at this point		-- 物品名:"茶几"->茶几-旧的发光的 制造描述:"一张装饰桌"
		},
		DECIDUOUSTREE =
		{
			BURNING = "A, nóng quá!!",		-- 物品名:"桦树"->正在燃烧
			BURNT = "Tạm biệt, bé cây",		-- 物品名:"桦树"->烧焦的
			CHOPPED = "Chặt thành khúc nhỏ!",		-- 物品名:"桦树"->被砍了
			POISON = "Ngươi đang nhìn gì đó?",		-- 物品名:"桦树"->毒化的
			GENERIC = "Cái cây lớn lên thật khỏe mạnh",		-- 物品名:"桦树"->默认
		},
		ACORN = "Mầm cây!",		-- 物品名:"桦木果"
        ACORN_SAPLING = "Mau lớn lên nhé!",		-- 物品名:"桦树树苗"
		ACORN_COOKED = "Ngon!",		-- 物品名:"烤桦木果"
		BIRCHNUTDRAKE = "Ha ha! Thật là vui!",		-- 物品名:"桦木果精"
		EVERGREEN =
		{
			BURNING = "Nên chặt cây trước rồi mới đốt lửa",		-- 物品名:"常青树"->正在燃烧
			BURNT = "Tạm biệt, cây thông",		-- 物品名:"常青树"->烧焦的
			CHOPPED = "Khúc gỗ thông",		-- 物品名:"常青树"->被砍了
			GENERIC = "Ta cũng thích chăm sóc cây",		-- 物品名:"常青树"->默认
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "Nên chặt cây trước rồi mới đốt lửa",		-- 物品名:"粗壮常青树"->正在燃烧
			BURNT = "Tạm biệt, cây thông",		-- 物品名:"粗壮常青树"->烧焦的
			CHOPPED = "Khúc gỗ thông",		-- 物品名:"粗壮常青树"->被砍了
			GENERIC = "Trông cây rất ốm, nên cho nó ăn chút",		-- 物品名:"粗壮常青树"->默认
		},
		TWIGGYTREE =
		{
			BURNING = "Tạm biệt, bé cây",		-- 物品名:"繁茂的树木"->正在燃烧
			BURNT = "Cháy sạch",		-- 物品名:"繁茂的树木"->烧焦的
			CHOPPED = "Chỉ còn lại gốc cây thấp thôi",		-- 物品名:"繁茂的树木"->被砍了
			GENERIC = "Cái cây ốm nhom",					-- 物品名:"繁茂的树木"->默认
			DISEASED = "Ta thấy chắc nó bệnh rồi...",		-- 物品名:"繁茂的树木"->生病了
		},
		TWIGGY_NUT_SAPLING = "Sẽ có ngày nó sẽ lớn lên thôi!",		-- 物品名:"繁茂的树苗"
        TWIGGY_OLD = "Nó đã trụ rất lâu ở trong rừng rậm rồi",		-- 物品名:"繁茂的树木"
		TWIGGY_NUT = "Bé cây",		-- 物品名:"繁茂的针叶树"->多枝树果
		EYEPLANT = "Nó đang nhìn xung quanh",		-- 物品名:"眼球草"
		INSPECTSELF = "Muốn ta làm mặt xấu sao?",		-- 物品名:"繁茂的树木"->检查自己
		FARMPLOT =
		{
			GENERIC = "Có thể trồng rau cho bản thân?!",		-- 物品名:未找到
			GROWING = "Đồ ăn đang lớn lên!",		-- 物品名:未找到
			NEEDSFERTILIZER = "Bỏ ít phân lên trên!",		-- 物品名:未找到
			BURNT = "Nóng quá, không thể trồng cây",		-- 物品名:未找到
		},
		FEATHERHAT = "Trông như chim, cách nghĩ cũng như chim",		-- 物品名:"羽毛帽" 制造描述:"头上的装饰"
		FEATHER_CROW = "Lông của chim đen",		-- 物品名:"黑色羽毛"
		FEATHER_ROBIN = "Lông của chim đỏ",		-- 物品名:"红色羽毛"
		FEATHER_ROBIN_WINTER = "Lông của chim trắng",		-- 物品名:"雪雀羽毛"
		FEATHER_CANARY = "Lông của chim vàng",		-- 物品名:"橙黄羽毛"
		FEATHERPENCIL = "Dụng cụ viết chữ tinh xảo",		-- 物品名:"羽毛笔" 制造描述:"是的，羽毛是必须的"
		FEM_PUPPET = "Ngươi bắt được chưa?",		-- 物品名:未找到
		FIREFLIES =
		{
			GENERIC = "Ánh trăng lấp lánh nhảy múa trên mặt hồ!",		-- 物品名:"萤火虫"->默认
			HELD = "Đừng chui vào người ta, ha ha!",		-- 物品名:"萤火虫"->拿在手里
		},
		FIREHOUND = "Không thích kiểu dáng của nó",		-- 物品名:"火猎犬"
		FIREPIT =
		{
			EMBERS = "Lửa sắp tắt rồi",		-- 物品名:"石头营火"->即将熄灭 制造描述:"一种更安全、更高效的营火"
			GENERIC = "Chúng ta cùng nhau kể chuyện quanh lửa trại nào,",		-- 物品名:"石头营火"->默认 制造描述:"一种更安全、更高效的营火"
			HIGH = "Lửa lớn!",		-- 物品名:"石头营火"->高 制造描述:"一种更安全、更高效的营火"
			LOW = "Lửa nhỏ lại rồi",		-- 物品名:"石头营火"->低？ 制造描述:"一种更安全、更高效的营火"
			NORMAL = "Thoải mái quá",		-- 物品名:"石头营火"->普通 制造描述:"一种更安全、更高效的营火"
			OUT = "Lát nữa sẽ nhóm lửa lại!",		-- 物品名:"石头营火"->出去？外面？ 制造描述:"一种更安全、更高效的营火"
		},
		COLDFIREPIT =
		{
			EMBERS = "Lửa sắp tắt rồi",		-- 物品名:"石头冰火"->即将熄灭 制造描述:"燃烧效率更高，但仍然越烤越冷"
			GENERIC = "Chúng ta cùng nhau kể chuyện quanh lửa trại nào,",		-- 物品名:"石头冰火"->默认 制造描述:"燃烧效率更高，但仍然越烤越冷"
			HIGH = "Lửa lớn!",		-- 物品名:"石头冰火"->高 制造描述:"燃烧效率更高，但仍然越烤越冷"
			LOW = "Lửa nhỏ lại rồi",		-- 物品名:"石头冰火"->低？ 制造描述:"燃烧效率更高，但仍然越烤越冷"
			NORMAL = "Lửa lạnh?",		-- 物品名:"石头冰火"->普通 制造描述:"燃烧效率更高，但仍然越烤越冷"
			OUT = "Lát nữa sẽ nhóm lửa lại!",		-- 物品名:"石头冰火"->出去？外面？ 制造描述:"燃烧效率更高，但仍然越烤越冷"
		},
		FIRESTAFF = "Pháp bảo của Hỏa Đức Chân Quân sao!",		-- 物品名:"火魔杖" 制造描述:"利用火焰的力量！"
		FIRESUPPRESSOR =
		{
			ON = "Cái máy này hay thật!",		-- 物品名:"雪球发射器"->开启 制造描述:"拯救植物，扑灭火焰，可添加燃料"
			OFF = "Nó đang ngủ",		-- 物品名:"雪球发射器"->关闭 制造描述:"拯救植物，扑灭火焰，可添加燃料"
			LOWFUEL = "Nó cần thêm nhiên liệu",		-- 物品名:"雪球发射器"->燃料不足 制造描述:"拯救植物，扑灭火焰，可添加燃料"
		},
		FISH = "A, đáng yêu quá!",		-- 物品名:"鱼"
		FISHINGROD = "Bắt lấy con cá!",		-- 物品名:"钓竿" 制造描述:"去钓鱼为了鱼"
		FISHSTICKS = "Tại sao lại đối xử với cá như vậy?!",		-- 物品名:"炸鱼排"
		FISHTACOS = "Ở không vui khi ở đây...",		-- 物品名:"鱼肉玉米卷"
		FISH_COOKED = "Cá là bạn, không phải đồ ăn!",		-- 物品名:"烤鱼"
		FLINT = "Nhọn quá!",		-- 物品名:"燧石"
		FLOWER =
		{
            GENERIC = "Tay tựa gió thu vờn lá ngọc, hoa xòe mặt nước vốc hạt kê",		-- 物品名:"花"->默认
            ROSE = "Hoa hồng góc cửa sổ, thay nhau tỏa hương thơm",		-- 物品名:"花"->玫瑰
        },
        FLOWER_WITHERED = "Khô héo",		-- 物品名:"枯萎的花"
		FLOWERHAT = "Cánh hoa bảo vệ tâm hồn ta!",		-- 物品名:"花环" 制造描述:"放松神经的东西"
		FLOWER_EVIL = "Chứa đầy sự tà ác",		-- 物品名:"邪恶花"
		FOLIAGE = "Màu tím là màu tốt nhất",		-- 物品名:"蕨叶"
		FOOTBALLHAT = "Muốn cùng ta chơi bóng bầu dục không!",		-- 物品名:"橄榄球头盔" 制造描述:"保护你的脑壳"
        FOSSIL_PIECE = "Xương âm u",		-- 物品名:"化石碎片"
        FOSSIL_STALKER =
        {
			GENERIC = "Cầm thêm mấy khúc xương âm u",		-- 物品名:"化石骨架"->默认
			FUNNY = "Bộ dạng nó vốn như vậy sao?",		-- 物品名:"化石骨架"->趣味？？
			COMPLETE = "...Tại sao chúng ta phải làm điều này?",		-- 物品名:"化石骨架"->准备好了
        },
        STALKER = "Nhận được ma thuật bóng tối!",		-- 物品名:"复活的骨架"
        STALKER_ATRIUM = "Nó là thật!",		-- 物品名:"远古织影者"
        STALKER_MINION = "Đứa nhóc đang bò!",		-- 物品名:"编织暗影"
        THURIBLE = "Lư hương kỳ lạ",		-- 物品名:"影子香炉"
        ATRIUM_OVERGROWTH = "Chữ cái kỳ lạ khiến ta đau đầu...",		-- 物品名:"古代的方尖碑"
		FROG =
		{
			DEAD = "Nó lên đầm lầy trên trời phải không",		-- 物品名:"青蛙"->死了
			GENERIC = "Bạn khỏe không?",		-- 物品名:"青蛙"->默认
			SLEEPING = "Ngủ ngon, bé cóc",		-- 物品名:"青蛙"->睡着了
		},
		FROGGLEBUNWICH = "Đây là một chiếc bánh sandwich khủng khiếp",		-- 物品名:"蛙腿三明治"
		FROGLEGS = "Con ếch tội nghiệp...",		-- 物品名:"蛙腿"
		FROGLEGS_COOKED = "Không dám nhìn",		-- 物品名:"烤蛙腿"
		FRUITMEDLEY = "Nhót tây... bánh quy... trộn lại?",		-- 物品名:"水果圣代"
		FURTUFT = "Ngửi thấy kỳ kỳ", 		-- 物品名:"毛丛"
		GEARS = "Nội tạng người thiết",		-- 物品名:"齿轮"
		GHOST = "Tránh ra!",		-- 物品名:"幽灵"
		GOLDENAXE = "Vàng rất cứng",		-- 物品名:"黄金斧头" 制造描述:"砍树也要有调！"
		GOLDENPICKAXE = "Bền hơn so với công cụ đập đá thông thường",		-- 物品名:"黄金鹤嘴锄" 制造描述:"像大Boss一样砸碎岩石"
		GOLDENPITCHFORK = "Công cụ nông nghiệp cao cấp",		-- 物品名:"黄金干草叉" 制造描述:"重新布置整个世界"
		GOLDENSHOVEL = "Công cụ đào đất cao cấp",		-- 物品名:"黄金铲子" 制造描述:"挖掘作用相同，但使用寿命更长"
		GOLDNUGGET = "Vàng lấp lánh ánh vàng!",		-- 物品名:"金块"
		GRASS =
		{
			BARREN = "Cần nhiều phân hơn",		-- 物品名:"草"
			WITHERED = "Nó nóng quá rồi",		-- 物品名:"草"->枯萎了
			BURNING = "Lửa!!",		-- 物品名:"草"->正在燃烧
			GENERIC = "Mấy cây cỏ",		-- 物品名:"草"->默认
			PICKED = "Vẫn chưa lớn",		-- 物品名:"草"->被采完了
			DISEASED = "Nó bệnh rồi, nhưng ta sẽ trị cho nó",		-- 物品名:"草"->生病了
			DISEASING = "Nó có vấn đề rồi",		-- 物品名:"草"->正在生病？？
		},
		GRASSGEKKO =
		{
			GENERIC = "Nên âm thầm tiếp cận nó",			-- 物品名:"草地壁虎"->默认
			DISEASED = "Tắc kè cỏ bị gì rồi",		-- 物品名:"草地壁虎"->生病了
		},
		GREEN_CAP = "Màu rất đẹp, chắc ngon lắm đây",		-- 物品名:"采摘的绿蘑菇"
		GREEN_CAP_COOKED = "Bây giờ mùi khác rồi",		-- 物品名:"烤绿蘑菇"
		GREEN_MUSHROOM =
		{
			GENERIC = "Nấm độc gây ảo giác",		-- 物品名:"绿蘑菇"->默认
			INGROUND = "Nó trốn mất rồi",		-- 物品名:"绿蘑菇"->在地里面
			PICKED = "Tất cả là của ta!",		-- 物品名:"绿蘑菇"->被采完了
		},
		GUNPOWDER = "Bột nổ",		-- 物品名:"火药" 制造描述:"一把火药"
		HAMBAT = "Còn to hơn cái chày giã thuốc của ta nữa",		-- 物品名:"火腿棒" 制造描述:"舍不得火腿套不着狼"
		HAMMER = "Đập vỡ tất cả!",		-- 物品名:"锤子" 制造描述:"敲碎各种东西"
		HEALINGSALVE = "Lạnh lạnh mát mát",		-- 物品名:"治疗药膏" 制造描述:"对割伤和擦伤进行消毒杀菌"
		HEATROCK =
		{
			FROZEN = "Mát lạnh",		-- 物品名:"暖石"->冰冻 制造描述:"储存热能供旅行途中使用"
			COLD = "Phù... lạnh cóng",		-- 物品名:"冻伤"->冷了
			GENERIC = "Chứa đầy năng lượng!",		-- 物品名:"暖石"->默认 制造描述:"储存热能供旅行途中使用"
			WARM = "Ấm ấm",		-- 物品名:"暖石"->温暖 制造描述:"储存热能供旅行途中使用"
			HOT = "Cục đá nóng!",		-- 物品名:"中暑"->热了
		},
		HOME = "Có ai ở đây không?",		-- 物品名:"暖石"->没调用 制造描述:"储存热能供旅行途中使用"
		HOMESIGN =
		{
			GENERIC = "Nó nói gì vậy?",		-- 物品名:"路牌"->默认 制造描述:"在世界中留下你的标记"
            UNWRITTEN = "Không có gì hết",		-- 物品名:"路牌"->没有写字 制造描述:"在世界中留下你的标记"
			BURNT = "Ôi được thôi",		-- 物品名:"路牌"->烧焦的 制造描述:"在世界中留下你的标记"
		},
		ARROWSIGN_POST =
		{
			GENERIC = "Nó nói gì vậy?",		-- 物品名:"指路标志"->默认 制造描述:"对这个世界指指点点或许只是打下手势"
            UNWRITTEN = "Không có gì hết!",		-- 物品名:"指路标志"->没有写字 制造描述:"对这个世界指指点点或许只是打下手势"
			BURNT = "Mùi nướng",		-- 物品名:"指路标志"->烧焦的 制造描述:"对这个世界指指点点或许只是打下手势"
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "Nó nói gì vậy?",		-- 物品名:"指路标志"->默认
            UNWRITTEN = "Không có gì hết!",		-- 物品名:"指路标志"->没有写字
			BURNT = "Chết rồi",		-- 物品名:"指路标志"->烧焦的
		},
		HONEY = "Xin chào mật ong của tôi",		-- 物品名:"蜂蜜"
		HONEYCOMB = "Lấy trộm từ chỗ ong mật!",		-- 物品名:"蜂巢"
		HONEYHAM = "Thịt lợn xông khói với mật ong",		-- 物品名:"蜜汁火腿"
		HONEYNUGGETS = "Lãng phí mật ong!",		-- 物品名:"蜜汁卤肉"
		HORN = "Tu tu!!",		-- 物品名:"牛角"
		HOUND = "Chó hư! Ta đánh",		-- 物品名:"猎犬"
		HOUNDCORPSE =
		{
			GENERIC = "Con chó kỳ lạ",		-- 物品名:"猎犬"->默认
			BURNING = "Hôi quá",		-- 物品名:"猎犬"->正在燃烧
			REVIVING = "Nó đang quay về!",		-- 物品名:"猎犬"
		},
		HOUNDBONE = "Sắc nhọn!",		-- 物品名:"犬骨"
		HOUNDMOUND = "Tìm được ổ chó rồi",		-- 物品名:"猎犬丘"
		ICEBOX = "Cái tủ lạnh",		-- 物品名:"冰箱" 制造描述:"延缓食物变质"
		ICEHAT = "Một cái mũ rất tốt",		-- 物品名:"冰帽" 制造描述:"用科学技术合成的冰帽"
		ICEHOUND = "Chó băng",		-- 物品名:"冰猎犬"
		INSANITYROCK =
		{
			ACTIVE = "Nó từ đâu đến vậy...？",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "Cục đá hoa văn biết trêu đùa...",		-- 物品名:"方尖碑"->没有激活
		},
		JAMMYPRESERVES = "Móng của ta đều dinh dính",		-- 物品名:"果酱"
		KABOBS = "Miếng thịt ghê tởm",		-- 物品名:"肉串"
		KILLERBEE =
		{
			GENERIC = "Trông khá tức giận",		-- 物品名:"杀人蜂"->默认
			HELD = "Tiếng vo ve nghe thật hay...",		-- 物品名:"杀人蜂"->拿在手里
		},
		KNIGHT = "người thiết lò xo",		-- 物品名:"发条骑士"
		KOALEFANT_SUMMER = "Mắt ngươi to ghê!",		-- 物品名:"大象"
		KOALEFANT_WINTER = "Mắt ngươi to ghê!",		-- 物品名:"大象"
		KRAMPUS = "Tên xấu xa!!",		-- 物品名:"坎普斯"
		KRAMPUS_SACK = "Nó kêu leng keng!",		-- 物品名:"坎普斯背包"
		LEIF = "Yêu quái cây!",		-- 物品名:"树精守卫"
		LEIF_SPARSE = "Yêu quái cây!!",		-- 物品名:"树精守卫"
		LIGHTER  = "Cái này có ý nghĩa đặc biệt đối với Willow",		-- 物品名:"薇洛的打火机" 制造描述:"火焰在雨中彻夜燃烧"
		LIGHTNING_ROD =
		{
			CHARGED = "Nó đang kêu roẹt roẹt!",		-- 物品名:"避雷针" 制造描述:"防雷劈"
			GENERIC = "Món đồ thần thánh tránh được tai họa?",		-- 物品名:"避雷针"->默认 制造描述:"防雷劈"
		},
		LIGHTNINGGOAT =
		{
			GENERIC = "Lông soắn!",		-- 物品名:"闪电羊"->默认
			CHARGED = "Chạy mau đi!",		-- 物品名:"闪电羊"
		},
		LIGHTNINGGOATHORN = "Có vết nứt!",		-- 物品名:"闪电羊角"
		GOATMILK = "Sữa tê dại",		-- 物品名:"带电的羊奶"
		LITTLE_WALRUS = "Xin chào! Muốn chơi chung không?",		-- 物品名:"小海象人"
		LIVINGLOG = "Cảm thấy không ổn...",		-- 物品名:"活木头" 制造描述:"用你的身体来代替\n你该干的活吧"
		LOG =
		{
			BURNING = "Lửa trại!",		-- 物品名:"木头"->正在燃烧
			GENERIC = "Chúng ta dựng lửa trại nào?",		-- 物品名:"木头"->默认
		},
		LUCY = "Ngươi là bạn của thợ đốn gỗ phải không?",		-- 物品名:"露西斧"
		LUREPLANT = "Tại sao trên cây có miếng thịt??",		-- 物品名:"食人花"
		LUREPLANTBULB = "Cái cây đáng ghét",		-- 物品名:"食人花种子"
		MALE_PUPPET = "Có vẻ nó mắc kẹt rồi!",		-- 物品名:"木头"
		MANDRAKE_ACTIVE = "Ơ! Nó đi theo ta!",		-- 物品名:"曼德拉草"
		MANDRAKE_PLANTED = "Nghe nói phải vào ban đêm mới nhổ được",		-- 物品名:"曼德拉草"
		MANDRAKE = "Bất động rồi",		-- 物品名:"曼德拉草"
        MANDRAKESOUP = "Anh bạn này làm canh thì rất ngon",		-- 物品名:"曼德拉草汤"
        MANDRAKE_COOKED = "Bữa khuya",		-- 物品名:"木头"
        MAPSCROLL = "Ở đâu trong bản đồ?",		-- 物品名:"地图" 制造描述:"向别人展示你看到什么！"
        MARBLE = "Cục đá lớn",		-- 物品名:"大理石"
        MARBLEBEAN = "Ôi! Nó làm rụng răng của ta rồi...",		-- 物品名:"大理石豌豆" 制造描述:"种植一片大理石森林"
        MARBLEBEAN_SAPLING = "Cục đá đó đang lớn lên phải không?",		-- 物品名:"大理石芽"
        MARBLESHRUB = "Cây đá",		-- 物品名:"大理石灌木"
        MARBLEPILLAR = "Nó khá là cũ",		-- 物品名:"大理石柱"
        MARBLETREE = "Thợ đốn gỗ chặt cây này đi! Ha ha...",		-- 物品名:"大理石树"
        MARSH_BUSH =
        {
			BURNT = "Thiêu rụi!",		-- 物品名:"尖刺灌木"->烧焦的
            BURNING = "Lửa! Lửa!",		-- 物品名:"尖刺灌木"->正在燃烧
            GENERIC = "Trong đầm có rất nhiều thứ này",		-- 物品名:"尖刺灌木"->默认
            PICKED = "Đau!",		-- 物品名:"尖刺灌木"->被采完了
        },
        BURNT_MARSH_BUSH = "Thật đau lòng...",		-- 物品名:"尖刺灌木"
        MARSH_PLANT = "Thực vật đầm lầy",		-- 物品名:"池塘边植物"
        MARSH_TREE =
        {
            BURNING = "Không phải ta làm!!",		-- 物品名:"针刺树"->正在燃烧
            BURNT = "Thiêu rụi rồi",		-- 物品名:"针刺树"->烧焦的
            CHOPPED = "Chặt sạch rồi!",		-- 物品名:"针刺树"->被砍了
            GENERIC = "Không nên chơi gần đó",		-- 物品名:"针刺树"->默认
        },
        MAXWELL = "Trông hắn không thân thiện",		-- 物品名:"麦斯威尔"->单机 麦斯威尔
        MAXWELLHEAD = "Đầu to thật!",		-- 物品名:"麦斯威尔的头"->单机 麦斯威尔的头
        MAXWELLLIGHT = "Không thích nó...",		-- 物品名:"麦斯威尔灯"->单机 麦斯威尔的灯
        MAXWELLLOCK = "Cần chìa khóa?",		-- 物品名:"噩梦锁"->单机 麦斯威尔的噩梦锁
        MAXWELLTHRONE = "Không nên xuất hiện ở đây...",		-- 物品名:"噩梦王座"->单机 麦斯威尔的噩梦王座
        MEAT = "Thịt? Tiên nữ không đụng vào thứ này đâu",		-- 物品名:"肉"
        MEATBALLS = "Ôi... mắc ói",		-- 物品名:"肉丸"
        MEATRACK =
        {
            DONE = "Đừng đụng vào nó",		-- 物品名:"晾肉架"->完成了 制造描述:"晾肉的架子"
            DRYING = "Hôi quá",		-- 物品名:"晾肉架"->正在风干 制造描述:"晾肉的架子"
            DRYINGINRAIN = "Làm như vậy chắc chưa nghĩ kỹ đâu",		-- 物品名:"晾肉架"->雨天风干 制造描述:"晾肉的架子"
            GENERIC = "Hắn dùng cái này để làm món ăn ghê tởm",		-- 物品名:"晾肉架"->默认 制造描述:"晾肉的架子"
            BURNT = "Ôi thôi",		-- 物品名:"晾肉架"->烧焦的 制造描述:"晾肉的架子"
            DONE_NOTMEAT = "Còn khó coi hơn lúc bình thường",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子"
            DRYING_NOTMEAT = "Cái đó trông không đúng lắm",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子"
            DRYINGINRAIN_NOTMEAT = "Nghe nói cái này có thể phơi khô đồ?",		-- 物品名:"晾肉架" 制造描述:"晾肉的架子"
        },
        MEAT_DRIED = "Khó gửi quá",		-- 物品名:"风干肉"
        MERM = "Xin chào!",		-- 物品名:"鱼人"
        MERMHEAD =
        {
            GENERIC = "Ai lại làm việc này...",		-- 物品名:"鱼头"->默认
            BURNT = "Hỏa táng cho nó",		-- 物品名:"鱼头"->烧焦的
        },
        MERMHOUSE =
        {
            GENERIC = "Đầm lầy ở đâu, ở đó có nhà",		-- 物品名:"漏雨的小屋"->默认
            BURNT = "Không không không!",		-- 物品名:"漏雨的小屋"->烧焦的
        },
        MINERHAT = "Đèn đội đầu thú vị",		-- 物品名:"矿工帽" 制造描述:"用你脑袋上的灯照亮夜晚"
        MONKEY = "He he, con khỉ vui nhộn",		-- 物品名:"暴躁猴"
        MONKEYBARREL = "Trông mặt cũng thân thiện",		-- 物品名:"猴子桶"
        MONSTERLASAGNA = "Bên trong... có gì?",		-- 物品名:"怪物千层饼"
        FLOWERSALAD = "Trông chẳng ra gì, nhưng nếm thử cũng không tệ",		-- 物品名:"花沙拉"
        ICECREAM = "Vừa ngọt vừa lạnh!",		-- 物品名:"冰淇淋"
        WATERMELONICLE = "Dưa hấu lạnh!",		-- 物品名:"西瓜冰棍"
        TRAILMIX = "Giòn rộp rộp rộp!",		-- 物品名:"什锦干果"
        HOTCHILI = "Không cần!",		-- 物品名:"辣椒炖肉"
        GUACAMOLE = "Cháo màu xanh rất ngon!",		-- 物品名:"鳄梨酱"
        MONSTERMEAT = "Thà giã thuốc cũng không muốn đụng phải thứ này",		-- 物品名:"怪物肉"
        MONSTERMEAT_DRIED = "Giờ càng khó gửi hơn!",		-- 物品名:"怪物肉干"
        MOOSE = "Đây chính là \"Mẹ Ngỗng\" mà Wickerbottom từng nói sao?",		-- 物品名:"漏雨的小屋"
        MOOSE_NESTING_GROUND = "Đây chính là nơi mà cô ta nuôi cục cưng",		-- 物品名:"漏雨的小屋"
        MOOSEEGG = "Có thể mở xem bên trong là gì được không?",		-- 物品名:"漏雨的小屋"
        MOSSLING = "He he, tên nhóc lắc lư thú vị",		-- 物品名:"驼鹿鹅幼崽"
        FEATHERFAN = "Cái này trên người con chim nào rớt xuống vậy?!",		-- 物品名:"羽毛扇" 制造描述:"超柔软、超大的扇子"
        MINIFAN = "He he he!",		-- 物品名:"旋转的风扇" 制造描述:"你得跑起来，才能带来风！"
        GOOSE_FEATHER = "Vù vù vù~ xem gió to đây!",		-- 物品名:"鹿鸭羽毛"
        STAFF_TORNADO = "Có thể khiến gió quấn lên!",		-- 物品名:"天气风向标" 制造描述:"把你的敌人吹走"
        MOSQUITO =
        {
            GENERIC = "Chắc sẽ không thích máu của ta đâu!",		-- 物品名:"蚊子"->默认
            HELD = "Bắt được ngươi rồi, côn trùng đốt người đáng ghét!",		-- 物品名:"蚊子"->拿在手里
        },
        MOSQUITOSACK = "Mùi tanh nồng quá",		-- 物品名:"蚊子血囊"
        MOUND =
        {
            DUG = "Có người để lại khúc xương ở đây",		-- 物品名:"坟墓"->被挖了
            GENERIC = "Nơi người chết yên nghỉ",		-- 物品名:"坟墓"->默认
        },
        NIGHTLIGHT = "Ánh sáng kỳ lạ",		-- 物品名:"魂灯" 制造描述:"用你的噩梦点亮夜晚"
        NIGHTMAREFUEL = "Sự tàn nhẫn tích tụ trong ác mộng!",		-- 物品名:"噩梦燃料" 制造描述:"傻子和疯子使用的邪恶残渣"
        NIGHTSWORD = "Ác mộng nhập thể!",		-- 物品名:"暗夜剑" 制造描述:"造成噩梦般的伤害"
        NITRE = "Cục đá thú vị",		-- 物品名:"硝石"
        ONEMANBAND = "Tùng! Tùng! Beng!",		-- 物品名:"独奏乐器" 制造描述:"疯子音乐家也有粉丝"
        OASISLAKE =
		{
			GENERIC = "Nước!!",		-- 物品名:"湖泊"->默认
			EMPTY = "Không! Nước đi đâu rồi?!",		-- 物品名:"湖泊"
		},
        PANDORASCHEST = "Trong rương chứa cái gì?",		-- 物品名:"华丽箱子"
        PANFLUTE = "Nghe một khúc nhạc",		-- 物品名:"排箫" 制造描述:"抚慰凶猛野兽的音乐"
        PAPYRUS = "Viết thêm câu chuyện",		-- 物品名:"莎草纸" 制造描述:"用于书写"
        WAXPAPER = "Trên tờ giấy này không có hình vẽ!",		-- 物品名:"蜡纸" 制造描述:"用于打包东西"
        PENGUIN = "Tụi nó là con chim thông minh, thà bơi chứ không bay",		-- 物品名:"企鹅"
        PERD = "Tránh ra, con chim kêu gru gru!",		-- 物品名:"火鸡"
        PEROGIES = "Bên trong chứa thịt??",		-- 物品名:"波兰水饺"
        PETALS = "He he, tiếp chiêu, bông hoa!",		-- 物品名:"花瓣"
        PETALS_EVIL = "Không muốn cầm mấy cái này",		-- 物品名:"恶魔花瓣"
        PHLEGM = "Ta từng ăn cái thứ còn tệ hơn cái này",		-- 物品名:"脓鼻涕"
        PICKAXE = "Công cụ đục đá",		-- 物品名:"鹤嘴锄" 制造描述:"凿碎岩石"
        PIGGYBACK = "Túi da lợn bẩn",		-- 物品名:"猪皮包" 制造描述:"能装许多东西，但会减慢你的速度"
        PIGHEAD =
        {
            GENERIC = "Ha ha!",		-- 物品名:"猪头"->默认
            BURNT = "Lợn giòn thơm! Tội đáng như vậy!",		-- 物品名:"猪头"->烧焦的
        },
        PIGHOUSE =
        {
            FULL = "Trông phòng toàn là người lợn dơ bẩn!",		-- 物品名:"猪屋"->满了 制造描述:"可以住一只猪"
            GENERIC = "Ngửi ngửi... ngửi giống như... người lợn!",		-- 物品名:"猪屋"->默认 制造描述:"可以住一只猪"
            LIGHTSOUT = "Xung quanh an toàn",		-- 物品名:"猪屋"->关灯了 制造描述:"可以住一只猪"
            BURNT = "He he, nhà người lợn cháy rồi!",		-- 物品名:"猪屋"->烧焦的 制造描述:"可以住一只猪"
        },
        PIGKING = "Tai to mặt bự không tốt đẹp gì!",		-- 物品名:"猪王"
        PIGMAN =
        {
            DEAD = "Đáng đời ngươi",		-- 物品名:"猪人"->死了
            FOLLOWER = "Tránh xa ra!",		-- 物品名:"猪人"->追随者
            GENERIC = "Người lợn...",		-- 物品名:"猪人"->默认
            GUARD = "Hắn còn đáng sợ hơn người khác",		-- 物品名:"猪人"->警戒
            WEREPIG = "Tránh xa ta ra!!",		-- 物品名:"猪人"->疯猪
        },
        PIGSKIN = "Đoạt từ chỗ lợn bẩn đó!!",		-- 物品名:"猪皮"
        PIGTENT = "Ở đó có người lợn?",		-- 物品名:"猪人"
        PIGTORCH = "Lửa!",		-- 物品名:"猪火炬"
        PINECONE = "Không ăn được",		-- 物品名:"松果"
        PINECONE_SAPLING = "Bé cây",		-- 物品名:"常青树苗"
        LUMPY_SAPLING = "Nó đã cố gắng hết sức rồi",		-- 物品名:"有疙瘩的树苗"
        PITCHFORK = "Công cụ có thể đâm trúng người",		-- 物品名:"干草叉" 制造描述:"铲地用具"
        PLANTMEAT = "Cảm thấy... hoang mang...",		-- 物品名:"叶子肉"
        PLANTMEAT_COOKED = "Không ăn đâu",		-- 物品名:"烤叶子肉"
        PLANT_NORMAL =
        {
            GENERIC = "Cây xanh nhiều lá!",		-- 物品名:"农作物"->默认
            GROWING = "Sắp lớn rồi phải không?",		-- 物品名:"农作物"->正在生长
            READY = "Mọi thứ đã sẵn sàng!",		-- 物品名:"农作物"->准备好的 满的
            WITHERED = "Thực vật cần uống nước?",		-- 物品名:"农作物"->枯萎了
        },
        POMEGRANATE = "Ơ, không phải ăn trực tiếp sao?",		-- 物品名:"石榴"
        POMEGRANATE_COOKED = "Ồ... được thôi...",		-- 物品名:"切片熟石榴"
        POMEGRANATE_SEEDS = "Có thể trồng ra rất nhiều lựu!",		-- 物品名:"石榴种子"
        POND = "Bọt nước bắn tung tóe!",		-- 物品名:"池塘"
        POOP = "Phải nuôi trồng thảo dược thật tốt, bón phân thì không thể thiếu",		-- 物品名:"肥料"
        FERTILIZER = "Cái gì? Là đống phân này sao",		-- 物品名:"便便桶" 制造描述:"少拉点在手上，多拉点在庄稼上"
        PUMPKIN = "Quả rất to!",		-- 物品名:"南瓜"
        PUMPKINCOOKIE = "Đây là bí ngô tốt nhất",		-- 物品名:"南瓜饼"
        PUMPKIN_COOKED = "Dinh dính!",		-- 物品名:"烤南瓜"
        PUMPKIN_LANTERN = "Một bộ mặt thân thiện",		-- 物品名:"南瓜灯" 制造描述:"长着鬼脸的照明用具"
        PUMPKIN_SEEDS = "Có thể trồng được rất nhiều bí ngô!",		-- 物品名:"南瓜种子"
        PURPLEAMULET = "Không muốn chơi nó nữa",		-- 物品名:"噩梦护符" 制造描述:"引起精神错乱"
        PURPLEGEM = "Thật là đẹp...",		-- 物品名:"紫宝石" 制造描述:"由两种颜色的宝石合成！"
        RABBIT =
        {
            GENERIC = "Đừng sợ, ta sẽ bảo vệ ngươi",		-- 物品名:"兔子"->默认
            HELD = "Chỗ ta có cà rốt, ngươi cần không?",		-- 物品名:"兔子"->拿在手里
        },
        RABBITHOLE =
        {
            GENERIC = "He he, cho ta mượn ít đồ, sẽ trả thứ khác",		-- 物品名:"兔洞"->默认
            SPRING = "Có ai ở nhà không?",		-- 物品名:"兔洞"->春天 or 潮湿
        },
        RAINOMETER =
        {
            GENERIC = "Hy vọng mưa rơi!",		-- 物品名:"雨量计"->默认 制造描述:"观测降雨机率"
            BURNT = "Ô được thôi",		-- 物品名:"雨量计"->烧焦的 制造描述:"观测降雨机率"
        },
        RAINCOAT = "Mặc áo mưa và đi trong mưa",		-- 物品名:"雨衣" 制造描述:"让你保持干燥的防水外套"
        RAINHAT = "Mưa không có gì đáng sợ đâu!",		-- 物品名:"防雨帽" 制造描述:"手感柔软，防雨必备"
        RATATOUILLE = "Cái này là ngon nhất!!",		-- 物品名:"蔬菜大杂烩"
        RAZOR = "Dùng để cạo",		-- 物品名:"剃刀" 制造描述:"剃掉你脏脏的山羊胡子"
        REDGEM = "Vừa đẹp vừa ấm",		-- 物品名:"红宝石"
        RED_CAP = "Mấy cái này chỉ có hại cho ngươi thôi",		-- 物品名:"采摘的红蘑菇"
        RED_CAP_COOKED = "Có chút hữu dụng...",		-- 物品名:"烤红蘑菇"
        RED_MUSHROOM =
        {
            GENERIC = "Xin chào nấm!",		-- 物品名:"红蘑菇"->默认
            INGROUND = "Ra đây!",		-- 物品名:"红蘑菇"->在地里面
            PICKED = "Không chừa lại cái gì",		-- 物品名:"红蘑菇"->被采完了
        },
        REEDS =
        {
            BURNING = "Đầm lầy đang cháy!",		-- 物品名:"芦苇"->正在燃烧
            GENERIC = "Trong đầm lầy có rất nhiều cái này!",		-- 物品名:"芦苇"->默认
            PICKED = "Muốn tìm thêm chút",		-- 物品名:"芦苇"->被采完了
        },
        RELIC = "Đồ cũ kỹ",		-- 物品名:"废墟"
        RUINS_RUBBLE = "Mấy cái này đều là đá",		-- 物品名:"损毁的废墟"
        RUBBLE = "Mấy cái này đều là đá",		-- 物品名:"碎石"
        RESEARCHLAB =
        {
            GENERIC = "Để ngươi mở mang chút ",		-- 物品名:"科学机器"->默认 制造描述:"解锁新的合成配方！"
            BURNT = "Thôi xong",		-- 物品名:"科学机器"->烧焦的 制造描述:"解锁新的合成配方！"
        },
        RESEARCHLAB2 =
        {
            GENERIC = "Thuật luyện kim... ta biết chút về thuật biến đá thành vàng",		-- 物品名:"炼金引擎"->默认 制造描述:"解锁更多合成配方！"
            BURNT = "Xong, nó không còn nữa",		-- 物品名:"炼金引擎"->烧焦的 制造描述:"解锁更多合成配方！"
        },
        RESEARCHLAB3 =
        {
            GENERIC = "Đừng đụng đến nó",		-- 物品名:"暗影操控器"->默认 制造描述:"这还是科学吗？"
            BURNT = "Cháy thì cũng tốt thôi",		-- 物品名:"暗影操控器"->烧焦的 制造描述:"这还是科学吗？"
        },
        RESEARCHLAB4 =
        {
            GENERIC = "Ta đoán ngươi vừa mới sắp xếp lại",		-- 物品名:"灵子分解器"->默认 制造描述:"增强高礼帽的魔力"
            BURNT = "Cái máy hình mũ kỳ lạ bị cháy rồi.",		-- 物品名:"灵子分解器"->烧焦的 制造描述:"增强高礼帽的魔力"
        },
        RESURRECTIONSTATUE =
        {
            GENERIC = "Đứa trẻ thế thân?",		-- 物品名:"替身人偶"->默认 制造描述:"以肉的力量让你重生"
            BURNT = "Nó khó ngửi quá",		-- 物品名:"替身人偶"->烧焦的 制造描述:"以肉的力量让你重生"
        },
        RESURRECTIONSTONE = "Sức mạnh mạnh mẽ!",		-- 物品名:"复活石"
        ROBIN =
        {
            GENERIC = "Màu đỏ thật là vui",		-- 物品名:"红雀"->默认
            HELD = "Con chim nhỏ của ta",		-- 物品名:"红雀"->拿在手里
        },
        ROBIN_WINTER =
        {
            GENERIC = "Chim tuyết, bay vào lòng ta",		-- 物品名:"雪雀"->默认
            HELD = "Nó thích làm thú cưng",		-- 物品名:"雪雀"->拿在手里
        },
        ROBOT_PUPPET = "Ngươi bắt nhốt chưa?",		-- 物品名:"雪雀"
        ROCK_LIGHT =
        {
            GENERIC = "Không thích nó",		-- 物品名:"熔岩坑"->默认
            OUT = "Ô",		-- 物品名:"熔岩坑"->出去？外面？
            LOW = "Biến thành đá rồi",		-- 物品名:"熔岩坑"->低？
            NORMAL = "Phát sáng!",		-- 物品名:"熔岩坑"->普通
        },
        CAVEIN_BOULDER =
        {
            GENERIC = "Cục đá rất to...",		-- 物品名:"岩石"->默认
            RAISED = "Với không tới!",		-- 物品名:"岩石"->提高了？
        },
        ROCK = "Chỉ là cục đá",		-- 物品名:"岩石"
        PETRIFIED_TREE = "Đây là cục đá hình cây?",		-- 物品名:"岩石"
        ROCK_PETRIFIED_TREE = "Đây là cục đá hình cây?",		-- 物品名:"石化树"
        ROCK_PETRIFIED_TREE_OLD = "Đây là cục đá hình cây?",		-- 物品名:"岩石"
        ROCK_ICE =
        {
            GENERIC = "Phù, bỏ lên tay thấy rất lạnh!",		-- 物品名:"迷你冰山"->默认
            MELTED = "Vũng nước bùn",		-- 物品名:"迷你冰山"->融化了
        },
        ROCK_ICE_MELTED = "Vũng nước bùn",		-- 物品名:"融化的冰矿"
        ICE = "Phù phù... lạnh quá",		-- 物品名:"冰"
        ROCKS = "Một đống đá",		-- 物品名:"石头"
        ROOK = "Trông khá hung dữ...",		-- 物品名:"发条战车"
        ROPE = "Dùng để trói",		-- 物品名:"绳子" 制造描述:"紧密编成的草绳，非常有用"
        ROTTENEGG = "Có mùi hôi dễ ngửi",		-- 物品名:"腐烂鸟蛋"
        ROYAL_JELLY = "Uhm~~~!",		-- 物品名:"蜂王浆"
        JELLYBEAN = "Đậu ngon",		-- 物品名:"彩虹糖豆"
        SADDLE_BASIC = "He he, phía trên có sừng nhỏ!",		-- 物品名:"鞍具" 制造描述:"你坐在动物身上仅仅是理论上"
        SADDLE_RACE = "Cuối cùng bươm bướm cũng có tác dụng!",		-- 物品名:"轻鞍具" 制造描述:"抵消掉完成目标所花费的时间或许"
        SADDLE_WAR = "Ta phải chiến đấu vì danh dự Thố Tiên!",		-- 物品名:"战争鞍具" 制造描述:"战场首领的王位"
        SADDLEHORN = "Công cụ txháo yên trên ngươi đám lông mềm",		-- 物品名:"鞍角" 制造描述:"把鞍具撬开"
        SALTLICK = "Ý ngươi là gì, \"Đồ chuyên dụng cho bò lai\"？",		-- 物品名:"舔舔盐块" 制造描述:"好好喂养你家的牲畜"
        BRUSH = "Lông trông kỳ lạ thật",		-- 物品名:"刷子" 制造描述:"减缓牦牛毛发的生长速度"
		SANITYROCK =
		{
			ACTIVE = "Tránh ra!",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "Có lẽ ta đang tưởng tượng...",		-- 物品名:"方尖碑"->没有激活
		},
		SAPLING =
		{
			BURNING = "Lửa!",		-- 物品名:"树苗"->正在燃烧
			WITHERED = "Quá khô",		-- 物品名:"树苗"->枯萎了
			GENERIC = "Cây nhỏ! Cho ta 1 cành",		-- 物品名:"树苗"->默认
			PICKED = "Lấy rồi!",		-- 物品名:"树苗"->被采完了
			DISEASED = "Uh... có vẻ khá tệ",		-- 物品名:"树苗"->生病了
			DISEASING = "Nó không ổn",		-- 物品名:"树苗"->正在生病？？
		},
   		SCARECROW =
   		{
			GENERIC = "Rất thích nó!",		-- 物品名:"友善的稻草人"->默认 制造描述:"模仿最新的秋季时尚"
			BURNING = "Không không không! Làm ơn đó!!",		-- 物品名:"友善的稻草人"->正在燃烧 制造描述:"模仿最新的秋季时尚"
			BURNT = "Tạm biệt",		-- 物品名:"友善的稻草人"->烧焦的 制造描述:"模仿最新的秋季时尚"
   		},
   		SCULPTINGTABLE=
   		{
			EMPTY = "Chế đồ từ đá!",		-- 物品名:"陶轮" 制造描述:"大理石在你手里将像黏土一样！"
			BLOCK = "Ta muốn làm đồ đẹp!",		-- 物品名:"陶轮"->锁住了 制造描述:"大理石在你手里将像黏土一样！"
			SCULPTURE = "Xem! Ta tự mình làm đó!",		-- 物品名:"陶轮"->雕像做好了 制造描述:"大理石在你手里将像黏土一样！"
			BURNT = "Không dùng được",		-- 物品名:"陶轮"->烧焦的 制造描述:"大理石在你手里将像黏土一样！"
   		},
        SCULPTURE_KNIGHTHEAD = "Hình như là đầu của người thiết",		-- 物品名:"可疑的大理石"
		SCULPTURE_KNIGHTBODY =
		{
			COVERED = "Cái đó là gì vậy?",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Nó hư rồi!",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Tất cả trong một!",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Có chuyện gì dó sắp xảy ra...",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        SCULPTURE_BISHOPHEAD = "Có vẻ chạy mất rồi",		-- 物品名:"可疑的大理石"
		SCULPTURE_BISHOPBODY =
		{
			COVERED = "Có hơi không đúng lắm",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Nó rớt đầu rồi!",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Xong toàn bộ rồi!",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Hả?",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        SCULPTURE_ROOKNOSE = "Uh, ta đoán cái này nên để ở đâu đó",		-- 物品名:"可疑的大理石"
		SCULPTURE_ROOKBODY =
		{
			COVERED = "Cục đá kỳ lạ...",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Thiếu gì đó...",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Xem ra ổn chút rồi",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Có chuyện gì đó sắp xảy ra rồi...",		-- 物品名:"大理石雕像"->准备好的 满的
		},
        GARGOYLE_HOUND = "Trông nó có vẻ sửng sốt!",		-- 物品名:"可疑的月亮石"
        GARGOYLE_WEREPIG = "Không thích bên cạnh có cái thứ này",		-- 物品名:"可疑的月亮石"
		SEEDS = "Cái này đặt trên đất!",		-- 物品名:"种子"
		SEEDS_COOKED = "Món ăn nho nhỏ!",		-- 物品名:"烤种子"
		SEWING_KIT = "Quay lại thỉnh giáo Chức Nữ...",		-- 物品名:"针线包" 制造描述:"缝补受损的衣物"
		SEWING_TAPE = "Winona rất biết sửa đồ",		-- 物品名:"可靠的胶布" 制造描述:"缝补受损的衣物"
		SHOVEL = "Dùng để xúc!",		-- 物品名:"铲子" 制造描述:"挖掘各种各样的东西"
		SILK = "Nhện nhả tơ!",		-- 物品名:"蜘蛛丝"
		SKELETON = "Hẹn gặp lại lần sau",		-- 物品名:"骷髅"
		SCORCHED_SKELETON = "Quá gần lửa trại",		-- 物品名:"易碎骨骼"
		SKULLCHEST = "Bên trong chắc có đồ tốt!",		-- 物品名:"骷髅箱"
		SMALLBIRD =
		{
			GENERIC = "Nó nhỏ thật!",		-- 物品名:"小鸟"->默认
			HUNGRY = "Muốn ăn đồ ăn vặt",		-- 物品名:"小鸟"->有点饿了
			STARVING = "Xem ra đói lắm rồi",		-- 物品名:"小鸟"->挨饿
			SLEEPING = "Ngủ ngon!",		-- 物品名:"小鸟"->睡着了
		},
		SMALLMEAT = "Ta sẽ không ăn cái này",		-- 物品名:"小肉"
		SMALLMEAT_DRIED = "Giờ thì khó ăn lắm rồi",		-- 物品名:"小风干肉"
		SPAT = "Xem ra cô ta không thoải mái",		-- 物品名:"钢羊"
		SPEAR = "Cây giáo đâm người ta!",		-- 物品名:"长矛" 制造描述:"使用尖的那一端"
		SPEAR_WATHGRITHR = "Cây giáo đâm người của nữ sĩ Viking!!",		-- 物品名:"战斗长矛" 制造描述:"黄金使它更锋利"
		WATHGRITHRHAT = "Đội cái này sẽ khiến ta trở lên mạnh mẽ giống nữ sĩ Viking không?",		-- 物品名:"战斗头盔" 制造描述:"独角兽是你的保护神"
		SPIDER =
		{
			DEAD = "Không mạnh như vậy",		-- 物品名:"蜘蛛"->死了
			GENERIC = "Con nhện nhỏ...",		-- 物品名:"蜘蛛"->默认
			SLEEPING = "Suỵt...",		-- 物品名:"蜘蛛"->睡着了
		},
		SPIDERDEN = "Giẫm phải thứ dính dính!",		-- 物品名:"蜘蛛巢"
		SPIDEREGGSACK = "Phù, đây là nơi mà bé nhện ra đời?",		-- 物品名:"蜘蛛卵" 制造描述:"跟你的朋友寻求点帮助"
		SPIDERGLAND = "Thuốc!",		-- 物品名:"蜘蛛腺"
		SPIDERHAT = "Ha ha, nhìn giống Webber",		-- 物品名:"蜘蛛帽" 制造描述:"蜘蛛们会喊你\"妈妈\""
		SPIDERQUEEN = "Cô ấy là nữ vương mạnh mẽ!",		-- 物品名:"蜘蛛女王"
		SPIDER_WARRIOR =
		{
			DEAD = "Ta chính là chiến thần của Nguyệt Cung, ha ha ha!!",		-- 物品名:"蜘蛛战士"->死了
			GENERIC = "Dũng sĩ nhện...",		-- 物品名:"蜘蛛战士"->默认
			SLEEPING = "Nó đang mơ giấc mơ của nhện",		-- 物品名:"蜘蛛战士"->睡着了
		},
		SPOILED_FOOD = "Trả về đất, về với tự nhiên nào",		-- 物品名:"腐烂食物"
        STAGEHAND =
        {
			AWAKE = "Đừng làm phiền ta!!",		-- 物品名:"舞台"->醒了
			HIDING = "Chuyện kỳ lạ...",		-- 物品名:"舞台"->藏起来了
        },
        STATUE_MARBLE =
        {
            GENERIC = "Hình dạng cục đá này rất kỳ lạ",		-- 物品名:"大理石雕像"->默认
            TYPE1 = "Nó mất đầu rồi!",		-- 物品名:"大理石雕像"->类型1
            TYPE2 = "Thật bi thương",		-- 物品名:"大理石雕像"->类型2
            TYPE3 = "Trông rất cao cấp và nhàm chán", --bird bath type statue		-- 物品名:"大理石雕像"
        },
		STATUEHARP = "Có ai đó làm hư nó rồi",		-- 物品名:"竖琴雕像"
		STATUEMAXWELL = "Trông giống người không vảy yếu ớt",		-- 物品名:"麦斯威尔雕像"
		STEELWOOL = "Cạo trúng người đó!",		-- 物品名:"钢丝绵"
		STINGER = "Cái đít của nó kêu vo ve",		-- 物品名:"蜂刺"
		STRAWHAT = "Ngứa quá...",		-- 物品名:"草帽" 制造描述:"帮助你保持清凉干爽"
		STUFFEDEGGPLANT = "Oa! Trong rau vẫn là rau!?",		-- 物品名:"酿茄子"
		SWEATERVEST = "Hắn mặc rất nhiều quần áo",		-- 物品名:"犬牙背心" 制造描述:"粗糙，但挺时尚"
		REFLECTIVEVEST = "Uh... màu cam",		-- 物品名:"清凉夏装" 制造描述:"穿上这件时尚的背心，保持凉爽和清醒"
		HAWAIIANSHIRT = "Mấy bông hoa đó không ổn rồi!",		-- 物品名:"花衬衫" 制造描述:"适合夏日穿着，或在周五便装日穿着"
		TAFFY = "Uhm~~! Dính răng ghê!",		-- 物品名:"太妃糖"
		TALLBIRD = "Chim chân dài",		-- 物品名:"高脚鸟"
		TALLBIRDEGG = "Nghe thấy bên trong có đồ!",		-- 物品名:"高脚鸟蛋"
		TALLBIRDEGG_COOKED = "Tội nghiệp",		-- 物品名:"煎高脚鸟蛋"
		TALLBIRDEGG_CRACKED =
		{
			COLD = "Ngươi lạnh không?",		-- 物品名:"冻伤"->冷了
			GENERIC = "Nó đang nở",		-- 物品名:"孵化中的高脚鸟蛋"->默认
			HOT = "Uh... nóng quá...",		-- 物品名:"中暑"->热了
			LONG = "Tại sao lại lâu như vậy!!",		-- 物品名:"孵化中的高脚鸟蛋"->还需要很久
			SHORT = "Sắp chui ra rồi!",		-- 物品名:"孵化中的高脚鸟蛋"->很快了
		},
		TALLBIRDNEST =
		{
			GENERIC = "Trứng của chim chân dài!",		-- 物品名:"高脚鸟巢"->默认
			PICKED = "Chim chân dài đang ngủ",		-- 物品名:"高脚鸟巢"->被采完了
		},
		TEENBIRD =
		{
			GENERIC = "Ngươi lớn lên thì hết vui rồi",		-- 物品名:"小高脚鸟"->默认
			HUNGRY = "Muốn ăn đồ ăn vặt",		-- 物品名:"小高脚鸟"->有点饿了
			STARVING = "Nó đói thì sẽ tức giận",		-- 物品名:"小高脚鸟"->挨饿
			SLEEPING = "Nó thích ngủ",		-- 物品名:"小高脚鸟"->睡着了
		},
		TELEPORTATO_BASE =
		{
			ACTIVE = "Sẽ quay về, đúng không?",		-- 物品名:"木制传送台"->激活了
			GENERIC = "Cái này sẽ quay về... thế giới khác?",		-- 物品名:"木制传送台"->默认
			LOCKED = "Thiếu gì đó?",		-- 物品名:"木制传送台"->锁住了
			PARTIAL = "Vẫn chưa xong sao?",		-- 物品名:"木制传送台"->已经有部分了
		},
		TELEPORTATO_BOX = "Cái rương thú vị",		-- 物品名:"盒状零件"
		TELEPORTATO_CRANK = "Đây là cái gì?",		-- 物品名:"曲柄零件"
		TELEPORTATO_POTATO = "Cục kim loại lớn",		-- 物品名:"金属土豆状零件"
		TELEPORTATO_RING = "Uh, lớn quá không bỏ lên vương miện được",		-- 物品名:"环状零件"
		TELESTAFF = "Sẽ khiến cho đầu cảm thấy kỳ quái...",		-- 物品名:"传送魔杖" 制造描述:"穿越空间的法杖！穿越时间的装置需另外购买"
		TENT =
		{
			GENERIC = "Đi ngủ",		-- 物品名:"帐篷"->默认 制造描述:"回复精神值，但要花费时间并导致饥饿"
			BURNT = "Nó bị cháy rồi",		-- 物品名:"帐篷"->烧焦的 制造描述:"回复精神值，但要花费时间并导致饥饿"
		},
		SIESTAHUT =
		{
			GENERIC = "Muốn ngủ trưa! Thì ngủ như vậy đi...",		-- 物品名:"遮阳篷"->默认 制造描述:"躲避烈日，回复精神值"
			BURNT = "Muốn tìm chỗ mới để ngủ trưa...",		-- 物品名:"遮阳篷"->烧焦的 制造描述:"躲避烈日，回复精神值"
		},
		TENTACLE = "Hắn cứ giẫm lên mấy cái này",		-- 物品名:"触手"
		TENTACLESPIKE = "Ha ha, gai nhọn!",		-- 物品名:"狼牙棒"
		TENTACLESPOTS = "Lấy da nó!",		-- 物品名:"触手皮"
		TENTACLE_PILLAR = "Gãi ngứa cho cái bụng của nó!",		-- 物品名:"大触手"
        TENTACLE_PILLAR_HOLE = "Bên trong đó có gì vậy?",		-- 物品名:"硕大的泥坑"
		TENTACLE_PILLAR_ARM = "Bọn nó nhỏ thật",		-- 物品名:"小触手"
		TENTACLE_GARDEN = "Tìm được nhà của nó rồi!",		-- 物品名:"大触手"
		TOPHAT = "Oh, hàng cao cấp!",		-- 物品名:"高礼帽" 制造描述:"最经典的帽子款式"
		TORCH = "Cây gậy giữ lửa",		-- 物品名:"火炬" 制造描述:"可携带的光源"
		TRANSISTOR = "Nó kêu vo ve! Bên trong chứa ong phải không?",		-- 物品名:"电子元件" 制造描述:"科学至上！滋滋滋！"
		TRAP = "Đáng nghi lắm!",		-- 物品名:"陷阱" 制造描述:"捕捉小型生物"
		TRAP_TEETH = "Trông rất đáng sợ",		-- 物品名:"犬牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西"
		TRAP_TEETH_MAXWELL = "Gai nhọn đáng sợ!",		-- 物品名:"麦斯威尔的尖牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西"
		TREASURECHEST =
		{
			GENERIC = "Oh, cái hộp đựng đồ!",		-- 物品名:"木箱"->默认 制造描述:"一种结实的容器"
			BURNT = "Không!",		-- 物品名:"木箱"->烧焦的 制造描述:"一种结实的容器"
		},
		TREASURECHEST_TRAP = "Báu vật!",		-- 物品名:"宝箱"
		SACRED_CHEST =
		{
			GENERIC = "Muốn biết bên trong có gì?",		-- 物品名:"古老的箱子"->默认
			LOCKED = "Ê!",		-- 物品名:"古老的箱子"->锁住了
		},
		TREECLUMP = "Không vượt qua được vì... a... vì...",		-- 物品名:"古老的箱子"
		TRINKET_1 = "Giầy nhỏ đẹp!", --Melted Marbles		-- 物品名:"熔化的弹珠"
		TRINKET_2 = "Tại sao không có âm thanh?!", --Fake Kazoo		-- 物品名:"假卡祖笛"
		TRINKET_3 = "Cái nơ không gỡ được!", --Gord's Knot		-- 物品名:"戈尔迪之结"
		TRINKET_4 = "Cái tên kỳ lạ", --Gnome		-- 物品名:"地精玩偶"
		TRINKET_5 = "Pằng pằng pằng!", --Toy Rocketship		-- 物品名:"迷你火箭"
		TRINKET_6 = "Cây gậy cong màu sắc kỳ lạ?", --Frazzled Wires		-- 物品名:"烂电线"
		TRINKET_7 = "Trò này khó quá!", --Ball and Cup		-- 物品名:"杯子和球玩具"
		TRINKET_8 = "Ken két, dễ vỡ", --Rubber Bung		-- 物品名:"硬化橡胶塞"
		TRINKET_9 = "Ở giữa mấy cục đá mày có cái động?", --Mismatched Buttons		-- 物品名:"不搭的纽扣"
		TRINKET_10 = "He he, cái răng vừa ngắn vừa tròn", --Dentures		-- 物品名:"二手假牙"
		TRINKET_11 = "Nó nói mọi thứ đều sẽ thuận lợi!", --Lying Robot		-- 物品名:"机器人玩偶"
		TRINKET_12 = "He he, nó chết rồi", --Dessicated Tentacle		-- 物品名:"干瘪的触手"
		TRINKET_13 = "Tiểu nữ sĩ kỳ lạ", --Gnomette		-- 物品名:"可爱的小玩偶"
		TRINKET_14 = "Cái này dùng để uống hay để rửa", --Leaky Teacup		-- 物品名:"漏水的茶杯"
		TRINKET_15 = "Trông giống... người thiết nhỏ...？", --Pawn		-- 物品名:"白衣主教"
		TRINKET_16 = "Trông giống... người thiết nhỏ...？", --Pawn		-- 物品名:"黑衣主教"
		TRINKET_17 = "Dụng cụ gim đồ ăn", --Bent Spork		-- 物品名:"弯曲的叉子"
		TRINKET_18 = "Bên trong có tiếng vọng", --Trojan Horse		-- 物品名:"玩具木马"
		TRINKET_19 = "Quay nào!", --Unbalanced Top		-- 物品名:"失衡上衣"
		TRINKET_20 = "Thêm một cái móng để gãi ngứa!", --Backscratcher		-- 物品名:"不求人"
		TRINKET_21 = "Phát minh của hắn thật là kỳ lạ", --Egg Beater		-- 物品名:"破搅拌器"
		TRINKET_22 = "Dây thừng nhỏ", --Frayed Yarn		-- 物品名:"磨损的纱线"
		TRINKET_23 = "Trông không giống sừng", --Shoehorn		-- 物品名:"鞋拔子"
		TRINKET_24 = "Trông rất vui", --Lucky Cat Jar		-- 物品名:"幸运猫扎尔"
		TRINKET_25 = "Sao ép cái cây được nhỏ như vậy?", --Air Unfreshener		-- 物品名:"臭气制造器"
		TRINKET_26 = "Phần ruột bên trong đâu rồi?", --Potato Cup		-- 物品名:"土豆杯"
		TRINKET_27 = "Trên đầu có cái móng nhỏ!", --Coat Hanger		-- 物品名:"钢丝衣架"
		TRINKET_28 = "Đây là tòa thành của con kiến phải không?", --Rook		-- 物品名:"白色战车"
        TRINKET_29 = "Đây là tòa thành của con kiến phải không?", --Rook		-- 物品名:"黑色战车"
        TRINKET_30 = "Trông giống... người thiết nhỏ...?", --Knight		-- 物品名:"白色骑士"
        TRINKET_31 = "Trông giống... người thiết nhỏ...？", --Knight		-- 物品名:"黑色骑士"
        TRINKET_32 = "Có thể nhìn thấy cái mặt đáng yêu!", --Cubic Zirconia Ball		-- 物品名:"立方氧化锆球"
        TRINKET_33 = "Ai từng thấy qua người kiến nhỏ như vậy chưa?", --Spider Ring		-- 物品名:"蜘蛛指环"
        TRINKET_34 = "Hình như an toàn", --Monkey Paw		-- 物品名:"猴子手掌"
        TRINKET_35 = "Có người uống rồi", --Empty Elixir		-- 物品名:"空的长生不老药"
        TRINKET_36 = "Cái răng rất nhọn!", --Faux fangs		-- 物品名:"人造尖牙"
        TRINKET_37 = "Không phải ta làm gãy!! Lúc tìm thấy thì đã như vậy rồi!", --Broken Stake		-- 物品名:"断桩"
        TRINKET_38 = "Luôn có thể nhìn thấy!", -- Binoculars Griftlands trinket		-- 物品名:"双筒望远镜"
        TRINKET_39 = "Có thể giữ ấm cho một bàn tay", -- Lone Glove Griftlands trinket		-- 物品名:"单只手套"
        TRINKET_40 = "Con bò lai này đang ngủ", -- Snail Scale Griftlands trinket		-- 物品名:"蜗牛秤"
        TRINKET_41 = "Ấm quá!", -- Goop Canister Hot Lava trinket		-- 物品名:"黏液罐"
        TRINKET_42 = "Ô, đồ chơi thú vị!", -- Toy Cobra Hot Lava trinket		-- 物品名:"玩具眼镜蛇"
        TRINKET_43 = "He he, nó mặc đồ trông rất thú vị!", -- Crocodile Toy Hot Lava trinket		-- 物品名:"鳄鱼玩具"
        TRINKET_44 = "Có người phá hư nhà của thực vật rồi", -- Broken Terrarium ONI trinket		-- 物品名:"破碎的玻璃罐"
        TRINKET_45 = "Nó có thể làm gì?", -- Odd Radio ONI trinket		-- 物品名:"奇怪的收音机"
        TRINKET_46 = "Uh...", -- Hairdryer ONI trinket		-- 物品名:"损坏的吹风机"
        LOST_TOY_1  = "Ha? Không lượm được!",		-- 物品名:"遗失的玻璃球"
        LOST_TOY_2  = "Ha? Không lượm được!",		-- 物品名:"多愁善感的卡祖笛"
        LOST_TOY_7  = "Ha? Không lượm được!",		-- 物品名:"珍视的抽线陀螺"
        LOST_TOY_10 = "Ha? Không lượm được!",		-- 物品名:"缺少的牙齿"
        LOST_TOY_11 = "Ha? Không lượm được!",		-- 物品名:"珍贵的玩具机器人"
        LOST_TOY_14 = "Ha? Không lượm được!",		-- 物品名:"妈妈最爱的茶杯"
        LOST_TOY_18 = "Ha? Không lượm được!",		-- 物品名:"宝贵的玩具木马"
        LOST_TOY_19 = "Ha? Không lượm được!",		-- 物品名:"最爱的陀螺"
        LOST_TOY_42 = "Ha? Không lượm được!",		-- 物品名:"装错的玩具眼镜蛇"
        LOST_TOY_43 = "Ha? Không lượm được!",		-- 物品名:"宠爱的鳄鱼玩具"
        HALLOWEENCANDY_1 = "Mấy cái này còn ngơn hơn táo!",		-- 物品名:"糖果苹果"
        HALLOWEENCANDY_2 = "Khiến cho bắp ngon hơn?!",		-- 物品名:"糖果玉米"
        HALLOWEENCANDY_3 = "Bắp!!",		-- 物品名:"不太甜的玉米"
        HALLOWEENCANDY_4 = "Không ngờ người kiến lại ngon như vậy!",		-- 物品名:"粘液蜘蛛"
        HALLOWEENCANDY_5 = "Một đớp nuốt trọn ngươi!",		-- 物品名:"浣猫糖果"
        HALLOWEENCANDY_6 = "Mùi vị không tệ",		-- 物品名:"\"葡萄干\""
        HALLOWEENCANDY_7 = "Phần của ngươi không ăn thì để ta ăn",		-- 物品名:"葡萄干"
        HALLOWEENCANDY_8 = "Rất ngon!",		-- 物品名:"鬼魂波普"
        HALLOWEENCANDY_9 = "Ngon hơn sâu thông thường!",		-- 物品名:"果冻虫"
        HALLOWEENCANDY_10 = "Uh, mùi vị bùn đất!",		-- 物品名:"触须棒棒糖"
        HALLOWEENCANDY_11 = "Ăn hết các ngươi!",		-- 物品名:"巧克力猪"
        HALLOWEENCANDY_12 = "Ăn hết vào bụng, quá ngon!", --ONI meal lice candy		-- 物品名:"糖果虱"
        HALLOWEENCANDY_13 = "Răng cửa sắp gãy rồi!", --Griftlands themed candy		-- 物品名:"无敌硬糖"
        HALLOWEENCANDY_14 = "Nóng nóng nóng!", --Hot Lava pepper candy		-- 物品名:"熔岩椒"
        CANDYBAG = "Đồ ngọt!!",		-- 物品名:"糖果袋" 制造描述:"只带万圣夜好吃的东西"
		HALLOWEEN_ORNAMENT_1 = "Phù! He he!",		-- 物品名:"幽灵装饰"
		HALLOWEEN_ORNAMENT_2 = "Có thể dùng làm trang sức!",		-- 物品名:"蝙蝠装饰"
		HALLOWEEN_ORNAMENT_3 = "He he... trông rất giống Webber", 		-- 物品名:"蜘蛛装饰"
		HALLOWEEN_ORNAMENT_4 = "Mấy cái này không đáng sợ chút nào",		-- 物品名:"触手装饰"
		HALLOWEEN_ORNAMENT_5 = "Món trang sức kỳ lạ",		-- 物品名:"悬垂蜘蛛装饰"
		HALLOWEEN_ORNAMENT_6 = "Con chim nhỏ xinh đẹp", 		-- 物品名:"乌鸦装饰"
		HALLOWEENPOTION_DRINKS_WEAK = "Không tốt như vậy...",		-- 物品名:"古老的箱子"
		HALLOWEENPOTION_DRINKS_POTENT = "Mùi vị tuyệt vời",		-- 物品名:"古老的箱子"
        HALLOWEENPOTION_BRAVERY = "Không sợ trời, không sợ đất!",		-- 物品名:"古老的箱子"
		HALLOWEENPOTION_MOON = "Hình như có thể vỡ được đó...",		-- 物品名:"月亮精华液"
		HALLOWEENPOTION_FIRE_FX = "Bình hoa lửa", 		-- 物品名:"古老的箱子"
		MADSCIENCE_LAB = "Bọt khí, bọt khí!",		-- 物品名:"疯狂科学家实验室" 制造描述:"疯狂实验无极限唯独神智有极限"
		LIVINGTREE_ROOT = "Gốc cây vẫn còn!", 		-- 物品名:"完全正常的树根"
		LIVINGTREE_SAPLING = "Đang sinh trưởng",		-- 物品名:"完全正常的树苗"
        DRAGONHEADHAT = "Trông đáng yêu giống như mặt của mình vậy",		-- 物品名:"幸运兽脑袋" 制造描述:"野兽装束的前端"
        DRAGONBODYHAT = "Thật là hợp!",		-- 物品名:"幸运兽躯体" 制造描述:"野兽装束的中间部分"
        DRAGONTAILHAT = "Ta luôn muốn có một cái đuôi!",		-- 物品名:"幸运兽尾巴" 制造描述:"野兽装束的尾端"
        PERDSHRINE =
        {
            GENERIC = "Ha? Ngươi muốn cái gì, con gà kêu gru gru?",		-- 物品名:"火鸡神龛"->默认 制造描述:"供奉庄严的火鸡"
            EMPTY = "Cần phải để thứ gì đó vào đây?",		-- 物品名:"火鸡神龛" 制造描述:"供奉庄严的火鸡"
            BURNT = "Cháy hết rồi",		-- 物品名:"火鸡神龛"->烧焦的 制造描述:"供奉庄严的火鸡"
        },
        REDLANTERN = "Uh, trông rất đẹp",		-- 物品名:"红灯笼" 制造描述:"照亮你的路的幸运灯笼"
        LUCKY_GOLDNUGGET = "Cục đá lấp lánh!",		-- 物品名:"幸运黄金"
        FIRECRACKERS = "Âm thanh lớn, sáng lấp lánh!!",		-- 物品名:"红色爆竹" 制造描述:"用重击来庆祝！"
        PERDFAN = "Gậy lông vũ!",		-- 物品名:"幸运扇" 制造描述:"额外的运气，超级多"
        REDPOUCH = "Âm thanh ting toong bên trong",		-- 物品名:"红袋子"
        WARGSHRINE =
        {
            GENERIC = "Nhà của chó vàng",		-- 物品名:"座狼龛"->默认 制造描述:"供奉黏土座狼"
            EMPTY = "Cần chút ánh sáng",		-- 物品名:"座狼龛" 制造描述:"供奉黏土座狼"
            BURNT = "Cháy sạch sẽ",		-- 物品名:"座狼龛"->烧焦的 制造描述:"供奉黏土座狼"
        },
        CLAYWARG =
        {
        	GENERIC = "Răng lớn!",		-- 物品名:"黏土座狼"->默认
        	STATUE = "Có cảm giác không tốt",		-- 物品名:"黏土座狼"->雕像
        },
        CLAYHOUND =
        {
        	GENERIC = "Cắn người đó!",		-- 物品名:"黏土猎犬"->默认
        	STATUE = "Ngươi do ai làm ra?",		-- 物品名:"黏土猎犬"->雕像
        },
        HOUNDWHISTLE = "Nó không có tiếng?",		-- 物品名:"幸运哨子" 制造描述:"对野猎犬吹哨"
        CHESSPIECE_CLAYHOUND = "Chó hư",		-- 物品名:"猎犬棋子"
        CHESSPIECE_CLAYWARG = "！",		-- 物品名:"座狼棋子"
		PIGSHRINE =
		{
            GENERIC = "Ai xây vậy??",		-- 物品名:"猪神龛"->默认 制造描述:"为富有的猪献贡"
            EMPTY = "Không cho ngươi cái gì hết",		-- 物品名:"猪神龛" 制造描述:"为富有的猪献贡"
            BURNT = "Xong, nó không còn nữa rồi",		-- 物品名:"猪神龛"->烧焦的 制造描述:"为富有的猪献贡"
		},
		PIG_TOKEN = "Trộm được thắt lưng của người lợn!",		-- 物品名:"金色腰带"
		PIG_COIN = "Mũi lợn lấp lánh!",		-- 物品名:"猪鼻铸币"
		YOTP_FOOD1 = "Phụt! Một mặt lợn xấu xí!",		-- 物品名:"致敬烤肉" 制造描述:"向猪王敬肉"
		YOTP_FOOD2 = "Tlợn lý mà nói ta tích bùn, nhưng cái thứ đó thì ta chịu",		-- 物品名:"八宝泥馅饼" 制造描述:"那里隐藏着什么？"
		YOTP_FOOD3 = "Đồ nướng đen ngòm",		-- 物品名:"鱼头串" 制造描述:"棍子上的荣华富贵"
		PIGELITE1 = "Tránh xa chút!", --BLUE		-- 物品名:"韦德"
		PIGELITE2 = "Con lợn xấu xa!", --RED		-- 物品名:"伊内修斯"
		PIGELITE3 = "Người lợn bẩn!", --WHITE		-- 物品名:"德米特里"
		PIGELITE4 = "Kẻ thù chung!", --GREEN		-- 物品名:"索耶"
		PIGELITEFIGHTER1 = "Tránh xa chút!", --BLUE		-- 物品名:"韦德"
		PIGELITEFIGHTER2 = "Con lớn xấu xa!", --RED		-- 物品名:"伊内修斯"
		PIGELITEFIGHTER3 = "Người lợn bẩn!", --WHITE		-- 物品名:"德米特里"
		PIGELITEFIGHTER4 = "Kẻ thù chung!", --GREEN		-- 物品名:"索耶"
		CARRAT_GHOSTRACER = "Cái đó rất đáng sợ",		-- 物品名:"查理的胡萝卜鼠"
        YOTC_CARRAT_RACE_START = "Cuộc thi về thực vật bắt đầu từ đây!",		-- 物品名:"起点" 制造描述:"冲向比赛！"
        YOTC_CARRAT_RACE_CHECKPOINT = "Nó có thể chỉ đường!",		-- 物品名:"检查点" 制造描述:"通往光荣之路上的一站"
        YOTC_CARRAT_RACE_FINISH =
        {
            GENERIC = "Đây là điểm cuối",		-- 物品名:"终点线"->默认 制造描述:"你走投无路了"
            BURNT = "Ao ao ao",		-- 物品名:"终点线"->烧焦的 制造描述:"你走投无路了"
            I_WON = "Yeah! Đánh bại ngươi rồi, ha ha!",		-- 物品名:"终点线" 制造描述:"你走投无路了"
            SOMEONE_ELSE_WON = ", {winner} bậy rồi...",		-- 物品名:"终点线" 制造描述:"你走投无路了"
        },
		YOTC_CARRAT_RACE_START_ITEM = "Đồ khởi điểm của cuộc thi thực vật",		-- 物品名:"起点套装" 制造描述:"冲向比赛！"
        YOTC_CARRAT_RACE_CHECKPOINT_ITEM = "Tìm đường giúp cà ruột",		-- 物品名:"检查点套装" 制造描述:"通往光荣之路上的一站"
		YOTC_CARRAT_RACE_FINISH_ITEM = "Điểm cuối ở đâu?",		-- 物品名:"终点线套装" 制造描述:"你走投无路了"
		YOTC_SEEDPACKET = "Món ăn vặt!",		-- 物品名:"种子包" 制造描述:"一包普通的混合种子"
		YOTC_SEEDPACKET_RARE = "Không biết có mọc ra thứ gì ngon không",		-- 物品名:"高级种子包" 制造描述:"一包高质量的种子"
		MINIBOATLANTERN = "Vật nổi trên nước đáng yêu",		-- 物品名:"漂浮灯笼" 制造描述:"闪着暖暖的光亮"
        YOTC_CARRATSHRINE =
        {
            GENERIC = "Giống con cà ruột",		-- 物品名:"胡萝卜鼠神龛"->默认 制造描述:"供奉灵活的胡萝卜鼠"
            EMPTY = "Muốn quà không?",		-- 物品名:"胡萝卜鼠神龛" 制造描述:"供奉灵活的胡萝卜鼠"
            BURNT = "Không phải ta làm hư đâu!!",		-- 物品名:"胡萝卜鼠神龛"->烧焦的 制造描述:"供奉灵活的胡萝卜鼠"
        },
        YOTC_CARRAT_GYM_DIRECTION =
        {
            GENERIC = "He he, cà ruột của ta là tốt nhất!",		-- 物品名:"方向健身房"->默认
            RAT = "Xông lên, xông lên!",		-- 物品名:"方向健身房"
            BURNT = "A...",		-- 物品名:"方向健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_SPEED =
        {
            GENERIC = "Cuộc thi tốc độ",		-- 物品名:"速度健身房"->默认
            RAT = "Quay nào, quay nào, quay!",		-- 物品名:"速度健身房"
            BURNT = "A...",		-- 物品名:"速度健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_REACTION =
        {
            GENERIC = "Huấn luyện thi chạy rất tốt!",		-- 物品名:"反应健身房"->默认
            RAT = "Tăng dần rồi",		-- 物品名:"反应健身房"
            BURNT = "A...",		-- 物品名:"反应健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_STAMINA =
        {
            GENERIC = "Máy nhảy dây",		-- 物品名:"耐力健身房"->默认
            RAT = "Nhảy nào!",		-- 物品名:"耐力健身房"
            BURNT = "A...",		-- 物品名:"耐力健身房"->烧焦的
        },
        YOTC_CARRAT_GYM_DIRECTION_ITEM = "Ta sẽ khiến cà ruột trở nên tốt nhất!",		-- 物品名:"方向健身房套装" 制造描述:"提高你的胡萝卜鼠的方向感"
        YOTC_CARRAT_GYM_SPEED_ITEM = "Dành cho cà ruột",		-- 物品名:"速度健身房套装" 制造描述:"增加你的胡萝卜鼠的速度"
        YOTC_CARRAT_GYM_STAMINA_ITEM = "Để cái này ở đâu?",		-- 物品名:"耐力健身房套装" 制造描述:"增强你的胡萝卜鼠的耐力"
        YOTC_CARRAT_GYM_REACTION_ITEM = "Phải lắp ráp rất nhiều thứ",		-- 物品名:"反应健身房套装" 制造描述:"加快你的胡萝卜鼠的反应时间"
        YOTC_CARRAT_SCALE_ITEM = "Có thể thấy con cà ruột nào là tốt nhất!",           		-- 物品名:"胡萝卜鼠秤套装" 制造描述:"看看你的胡萝卜鼠的情况"
        YOTC_CARRAT_SCALE =
        {
            GENERIC = "Phải lấy được con cà ruột tốt nhất!",		-- 物品名:"胡萝卜鼠秤"->默认
            CARRAT = "Không tốt lắm",		-- 物品名:"胡萝卜" 制造描述:"灵巧机敏，富含胡萝卜素"
            CARRAT_GOOD = "Là tuyển thủ rất tốt!",		-- 物品名:"胡萝卜鼠秤"
            BURNT = "Không phải do ta làm!!",		-- 物品名:"胡萝卜鼠秤"->烧焦的
        },
		BISHOP_CHARGE_HIT = "Ối!",		-- 物品名:"胡萝卜鼠秤"->被主教攻击
		TRUNKVEST_SUMMER = "Gắn cái mũi lên người!",		-- 物品名:"保暖小背心" 制造描述:"暖和，但算不上非常暖和"
		TRUNKVEST_WINTER = "Thoải mái...",		-- 物品名:"寒冬背心" 制造描述:"足以抵御冬季暴雪的保暖背心"
		TRUNK_COOKED = "Phù!",		-- 物品名:"象鼻排"
		TRUNK_SUMMER = "Lấy được cái mũi của nó rồi!",		-- 物品名:"象鼻"
		TRUNK_WINTER = "Lông mềm mại thật!",		-- 物品名:"冬象鼻"
		TUMBLEWEED = "Ngươi muốn đi đâu?",		-- 物品名:"风滚草"
		TURKEYDINNER = "Thật đáng sợ",		-- 物品名:"火鸡大餐"
		TWIGS = "Một đống cành cây",		-- 物品名:"树枝"
		UMBRELLA = "Có ô thì ta sẽ không bị ướt!",		-- 物品名:"雨伞" 制造描述:"遮阳挡雨！"
		GRASS_UMBRELLA = "Chẳng biết nó đẹp chỗ nào",		-- 物品名:"花伞" 制造描述:"漂亮轻便的保护伞"
		UNIMPLEMENTED = "Đó là cái gì?",		-- 物品名:"胡萝卜鼠秤"
		WAFFLES = "Bánh vuông ngọt!",		-- 物品名:"华夫饼"
		WALL_HAY =
		{
			GENERIC = "Ta thổi mấy cái thì cái tường đó bị đổ rồi!",		-- 物品名:"草墙"->默认
			BURNT = "Thường thì câu chuyện sẽ không kết thúc như vậy đâu",		-- 物品名:"草墙"->烧焦的
		},
		WALL_HAY_ITEM = "Wickerbottom từng kể một câu chuyện về nhà cỏ...",		-- 物品名:"草墙" 制造描述:"草墙墙体不太结实"
		WALL_STONE = "Trông cũng không tệ!",		-- 物品名:"石墙"
		WALL_STONE_ITEM = "Đống đá!",		-- 物品名:"石墙" 制造描述:"石墙墙体"
		WALL_RUINS = "Sao trông nó cũ như vậy?",		-- 物品名:"铥墙"
		WALL_RUINS_ITEM = "Có thể dùng nó để xây một cái tường kiên cố!",		-- 物品名:"铥墙" 制造描述:"这些墙可以承受相当多的打击"
		WALL_WOOD =
		{
			GENERIC = "Cho thêm mấy đóa hoa mặt trăng thì đẹp lắm",		-- 物品名:"木墙"->默认
			BURNT = "Ôi, từng là một bức tường tốt...",		-- 物品名:"木墙"->烧焦的
		},
		WALL_WOOD_ITEM = "Cái thứ nhọn nhọn thật là đẹp",		-- 物品名:"木墙" 制造描述:"木墙墙体"
		WALL_MOONROCK = "Tường mặt trăng!",		-- 物品名:"月岩壁"
		WALL_MOONROCK_ITEM = "Đá cứng",		-- 物品名:"月岩壁" 制造描述:"月球疯子之墙"
		FENCE = "Có thể chặn người lợn đáng ghét ở bên ngoài",		-- 物品名:"木栅栏"
        FENCE_ITEM = "Vật liệu xây hàng rào!",		-- 物品名:"木栅栏" 制造描述:"木栅栏部分"
        FENCE_GATE = "Ô thông minh đó, xây thêm cổng cho hàng rào",		-- 物品名:"木门"
        FENCE_GATE_ITEM = "Vật liệu xây cổng hàng rào!",		-- 物品名:"木门" 制造描述:"木栅栏的门"
		WALRUS = "Trông hắn có vẻ vừa to vừa quan trọng",		-- 物品名:"海象人"
		WALRUSHAT = "He he, ấm áp, cái tai rất thoải mái!",		-- 物品名:"海象的贝雷帽"
		WALRUS_CAMP =
		{
			EMPTY = "Có người từng đến đây",		-- 物品名:"海象营"
			GENERIC = "Tuy nhà làm bằng băng, nhưng trông rất ấm áp",		-- 物品名:"海象营"->默认
		},
		WALRUS_TUSK = "Ta lấy được răng của nó rồi!",		-- 物品名:"海象牙"
		WARDROBE =
		{
			GENERIC = "Có thể chơi game thay quần áo rồi!",		-- 物品名:"衣柜"->默认 制造描述:"随心变换面容"
            BURNING = "Lửa! Lửa!",		-- 物品名:"衣柜"->正在燃烧 制造描述:"随心变换面容"
			BURNT = "A...",		-- 物品名:"衣柜"->烧焦的 制造描述:"随心变换面容"
		},
		WARG = "Răng của nó lớn ghê",		-- 物品名:"座狼"
		WASPHIVE = "Trông đáng sợ... nhưng bên trong có đồ ngọt...",		-- 物品名:"杀人蜂蜂窝"
		WATERBALLOON = "Đừng sợ, không ném vào ngươi đâu...（he he）",		-- 物品名:"水球" 制造描述:"球体灭火"
		WATERMELON = "Đưa cho ta!!",		-- 物品名:"西瓜"
		WATERMELON_COOKED = "Dưa hấu nướng",		-- 物品名:"烤西瓜"
		WATERMELONHAT = "Ta yêu thích thời trang mới mẻ!",		-- 物品名:"西瓜帽" 制造描述:"提神醒脑，但感觉黏黏的"
		WAXWELLJOURNAL = "...Không thích câu chuyện trong quyển sách đó",		-- 物品名:"暗影魔法书" 制造描述:"这将让你大吃一惊"
		WETGOOP = "Thông thường ta thích những thứ dính dính ướt ướt này...",		-- 物品名:"失败料理"
        WHIP = "Tsa~~!!",		-- 物品名:"皮鞭" 制造描述:"提出有建设性的反馈意见"
		WINTERHAT = "Trông nó rất thoải mái!",		-- 物品名:"冬帽" 制造描述:"保持脑袋温暖"
		WINTEROMETER =
		{
			GENERIC = "Nó phải dựa vào cái này mới biết được lạnh nóng?",		-- 物品名:"温度测量仪"->默认 制造描述:"测量环境气温"
			BURNT = "Xong",		-- 物品名:"温度测量仪"->烧焦的 制造描述:"测量环境气温"
		},
        WINTER_TREE =
        {
            BURNT = "Từng rất đẹp...",		-- 物品名:"冬季圣诞树"->烧焦的
            BURNING = "Mau dập lửa!",		-- 物品名:"冬季圣诞树"->正在燃烧
            CANDECORATE = "Chừa từng thấy qua cái cây đẹp như vậy!",		-- 物品名:"冬季圣诞树"->烛台？？？
            YOUNG = "Mau lớn lên!",		-- 物品名:"冬季圣诞树"->还年轻
        },
		WINTER_TREESTAND =
		{
			GENERIC = "Phải tìm được một quả thông!",		-- 物品名:"圣诞树墩"->默认 制造描述:"种植并装饰一棵冬季圣诞树！"
            BURNT = "Ô...",		-- 物品名:"圣诞树墩"->烧焦的 制造描述:"种植并装饰一棵冬季圣诞树！"
		},
        WINTER_ORNAMENT = "Ô... thật là đẹp!",		-- 物品名:"圣诞小玩意"
        WINTER_ORNAMENTLIGHT = "Phát sáng!",		-- 物品名:"圣诞灯"
        WINTER_ORNAMENTBOSS = "Những thứ trên cây đều để lại vị trí rất đặc biệt!",		-- 物品名:"华丽的装饰"
		WINTER_ORNAMENTFORGE = "Phải để cái này lên cây?",		-- 物品名:"熔炉装饰"
		WINTER_ORNAMENTGORGE = "...？",		-- 物品名:"舒缓的装饰"
        WINTER_FOOD1 = "Ồ, cái này dùng để làm gì?", --gingerbread cookie		-- 物品名:"小姜饼"
        WINTER_FOOD2 = "Thứ hoa tuyết này cũng sẽ tan trên đầu lưỡi!", --sugar cookie		-- 物品名:"糖曲奇饼"
        WINTER_FOOD3 = "Giòn!！", --candy cane		-- 物品名:"拐杖糖"
        WINTER_FOOD4 = "Nhân bánh bên trong là gì?", --fruitcake		-- 物品名:"永远的水果蛋糕"
        WINTER_FOOD5 = "Là gỗ thật sao?", --yule log cake		-- 物品名:"巧克力树洞蛋糕"
        WINTER_FOOD6 = "Mau xem! Ta có thể cắn phát hết luôn!", --plum pudding		-- 物品名:"李子布丁"
        WINTER_FOOD7 = "Uh...", --apple cider		-- 物品名:"苹果酒"
        WINTER_FOOD8 = "Tuyệt vời", --hot cocoa		-- 物品名:"热可可"
        WINTER_FOOD9 = "Ực ực ực!", --eggnog		-- 物品名:"美味的蛋酒"
		WINTERSFEASTOVEN =
		{
			GENERIC = "Ngọn lửa cháy thật là lớn!",		-- 物品名:"砖砌烤炉"->默认 制造描述:"燃起了喜庆的火焰"
			COOKING = "Đang làm đồ ăn",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰"
			ALMOST_DONE_COOKING = "Bây giờ ăn được chưa? Bây giờ được chưa? Bây giờ?",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰"
			DISH_READY = "Đồ ăn được rồi!",		-- 物品名:"砖砌烤炉" 制造描述:"燃起了喜庆的火焰"
		},
		BERRYSAUCE = "Quả mọng thơm ngon!",		-- 物品名:"快乐浆果酱"
		BIBINGKA = "Rất thích cái này!",		-- 物品名:"比宾卡米糕"
		CABBAGEROLLS = "Cải trắng gói cải trắng!",		-- 物品名:"白菜卷"
		FESTIVEFISH = "Có người đang làm trò quái ác, nhìn kỹ đó là cá, kỳ thật là rau cải!",		-- 物品名:"节庆鱼料理"
		GRAVY = "Uh, là sô cô la!",		-- 物品名:"好肉汁"
		LATKES = "Hương vị thơm giòn!",		-- 物品名:"土豆饼"
		LUTEFISK = "Khoai tây hình con cá!",		-- 物品名:"苏打鱼"
		MULLEDDRINK = "Uh, bụng ấm ghê!",		-- 物品名:"香料潘趣酒"
		PANETTONE = "Bánh mì thơm thơm!",		-- 物品名:"托尼甜面包"
		PAVLOVA = "Đầu bếp cao cấp gọi là trứng... lòng trắng... vỏ",		-- 物品名:"巴甫洛娃蛋糕"
		PICKLEDHERRING = "Có người đang làm trò quái ác, nhìn kỹ đó là cá, kỳ thật là rau cải!",		-- 物品名:"腌鲱鱼"
		POLISHCOOKIE = "Túi trái cây thơm thơm!",		-- 物品名:"波兰饼干"
		PUMPKINPIE = "Uhm~~~!",		-- 物品名:"南瓜饼"
		ROASTTURKEY = "Con gà tây trông khó ăn, nhưng ngửi thấy giống mùi rau cải?",		-- 物品名:"烤火鸡"
		STUFFING = "Vụn bánh mì rất ngon",		-- 物品名:"烤火鸡面包馅"
		SWEETPOTATO = "Cái này còn ngon hơn khoai tây thông thường",		-- 物品名:"红薯焗饭"
		TAMALES = "Uh, bên trong rau cải có ớt",		-- 物品名:"塔马利"
		TOURTIERE = "Uh, bánh rau cải cay!",		-- 物品名:"饕餮馅饼"
		TABLE_WINTERS_FEAST =
		{
			GENERIC = "Bàn ăn cao cấp",		-- 物品名:"冬季盛宴餐桌"->默认 制造描述:"一起来享用冬季盛宴吧"
			HAS_FOOD = "Có đồ ăn ngon rồi! Có đồ ăn ngon rồi!",		-- 物品名:"冬季盛宴餐桌" 制造描述:"一起来享用冬季盛宴吧"
			WRONG_TYPE = "Cái này không được để ở đây!",		-- 物品名:"冬季盛宴餐桌" 制造描述:"一起来享用冬季盛宴吧"
			BURNT = "Ôi, tiệc kết thúc rồi?",		-- 物品名:"冬季盛宴餐桌"->烧焦的 制造描述:"一起来享用冬季盛宴吧"
		},
		GINGERBREADWARG = "Ăn hết toàn bộ các ngươi!", 		-- 物品名:"姜饼狼"
		GINGERBREADHOUSE = "Có ai ở nhà không?", 		-- 物品名:"姜饼猪屋"
		GINGERBREADPIG = "Không thể bỏ qua bánh quy người lợn nhỏ!",		-- 物品名:"姜饼猪"
		CRUMBS = "Còn lại chút mùi thơm",		-- 物品名:"饼干屑"
		WINTERSFEASTFUEL = "Xem ra rất ngon",		-- 物品名:"节日欢愉"
        KLAUS = "Có quà tặng ta không?",		-- 物品名:"克劳斯"
        KLAUS_SACK = "Mở nào! Mở nào!",		-- 物品名:"赃物袋"
		KLAUSSACKKEY = "Sừng hươu có hình dáng kỳ lạ...",		-- 物品名:"麋鹿茸"
		WORMHOLE =
		{
			GENERIC = "Cái gì đang động đậy vậy?",		-- 物品名:"虫洞"->默认
			OPEN = "Ha ha! Đây là một đường hầm mọc răng",		-- 物品名:"虫洞"->打开
		},
		WORMHOLE_LIMITED = "Cảm thấy không ổn?",		-- 物品名:"生病的虫洞"->一次性虫洞 单机
		ACCOMPLISHMENT_SHRINE = "Làm rất nhiều việc!",        		-- 物品名:"奖杯" 制造描述:"证明你作为一个人的价值"
		LIVINGTREE = "Ánh mắt nó nhìn ta không được đúng lắm",		-- 物品名:"完全正常的树"
		ICESTAFF = "Gậy lạnh phép thuật",		-- 物品名:"冰魔杖" 制造描述:"把敌人冰冻在原地"
		REVIVER = "Nó vẫn còn động đậy!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕"
		SHADOWHEART = "Rất... bi thương...",		-- 物品名:"暗影心房"
        ATRIUM_RUBBLE =
        {
			LINE_1 = "Ồ, bức tranh đang kể chuyện!",		-- 物品名:"古代的壁画"->第一行
			LINE_2 = "Không nhìn thấy hình ảnh",		-- 物品名:"古代的壁画"->第二行
			LINE_3 = "Xem ra... đã xảy ra chuyện không hay",		-- 物品名:"古代的壁画"->第三行
			LINE_4 = "Ở đây bắt đầu đáng sợ rồi...",		-- 物品名:"古代的壁画"->第四行
			LINE_5 = "Ở đây từng là thôn trang",		-- 物品名:"古代的壁画"->第五行
		},
        ATRIUM_STATUE = "Có gì đó không đúng lắm...",		-- 物品名:"远古雕像"
        ATRIUM_LIGHT =
        {
			ON = "Thật đáng sợ!",		-- 物品名:"古代的灯塔"->开启
			OFF = "Cần lửa không?",		-- 物品名:"古代的灯塔"->关闭
		},
        ATRIUM_GATE =
        {
			ON = "Không nên làm như vậy!",		-- 物品名:"古代的通道"->开启
			OFF = "Có người làm hư rồi",		-- 物品名:"古代的通道"->关闭
			CHARGING = "Xảy ra chuyện gì vậy?!",		-- 物品名:"古代的通道"->充能中
			DESTABILIZING = "-！",		-- 物品名:"古代的通道"->不稳定
			COOLDOWN = "Kết thúc rồi",		-- 物品名:"古代的通道"->冷却中
        },
        ATRIUM_KEY = "Cái này trông rất quan trọng!",		-- 物品名:"古代的钥匙"
		LIFEINJECTOR = "Không thích kim tiêm!",		-- 物品名:"强心针" 制造描述:"提高下你那日渐衰退的最大健康值吧"
		SKELETON_PLAYER =
		{
			MALE = "Bọn họ xảy ra chuyện rồi...",		-- 物品名:"骷髅"->男性
			FEMALE = "Bọn họ xảy ra chuyện rồi...",		-- 物品名:"骷髅"->女性
			ROBOT = "Bọn họ xảy ra chuyện rồi...",		-- 物品名:"骷髅"->机器人
			DEFAULT = "Tạm biệt...",		-- 物品名:"物品栏物品"->默认
		},
		HUMANMEAT = "Không được",		-- 物品名:"长猪"
		HUMANMEAT_COOKED = ", sau khi nó chín thì càng khó ngửi",		-- 物品名:"煮熟的长猪"
		HUMANMEAT_DRIED = "Không không không",		-- 物品名:"长猪肉干"
		ROCK_MOON = "Đá mặt trăng!",		-- 物品名:"岩石"
		MOONROCKNUGGET = "Là đá ở cố hương",		-- 物品名:"月岩"
		MOONROCKCRATER = "Con mắt này, thiếu một trái tim!",		-- 物品名:"有洞的月亮石" 制造描述:"用于划定地盘的岩石"
		MOONROCKSEED = "Đến ngắm Động Đình màu nguyệt sáng, dong thuyền mua rượu trắng mây mờ.",		-- 物品名:"天体宝球"
        REDMOONEYE = "Không thích màu đỏ tươi",		-- 物品名:"红色月眼"
        PURPLEMOONEYE = "Nhìn về nơi xa",		-- 物品名:"紫色月眼"
        GREENMOONEYE = "Góc nhìn cuộc sống",		-- 物品名:"绿色月眼"
        ORANGEMOONEYE = "Cái nhìn ấm áp",		-- 物品名:"橘色月眼"
        YELLOWMOONEYE = "Góc nhìn mơ hồ",		-- 物品名:"黄色月眼"
        BLUEMOONEYE = "Cái nhìn lạnh lẽo",		-- 物品名:"蓝色月眼"
        BOARRIOR = "Người- người lợn lớn...",		-- 物品名:"大熔炉猪战士"->大熔炉猪战士
        BOARON = "Ngươi là thân thích của người lợn??",		-- 物品名:"小猪"
        PEGHOOK = "Con côn trùng xấu xa!",		-- 物品名:"蝎子"
        TRAILS = "Nó... trông không thể rắn chắc như vậy... phù",		-- 物品名:"大猩猩"
        TURTILLUS = "Sao lại có gai?",		-- 物品名:"坦克龟"
        SNAPPER = "Răng",		-- 物品名:"鳄鱼指挥官"
		RHINODRILL = "Bọn họ cũng không ghê gớm như vậy!",		-- 物品名:"后扣帽犀牛兄弟"
		BEETLETAUR = "Ôi!!",		-- 物品名:"地狱独眼巨猪"
        LAVAARENA_PORTAL =
        {
            ON = "Tạm biệt!",		-- 物品名:"远古传送门"->开启
            GENERIC = "Cánh cửa kia dùng như thế nào?",		-- 物品名:"远古传送门"->默认
        },
        HEALINGSTAFF = "Phù, cây gậy xấu xí...",		-- 物品名:"生存魔杖"
        FIREBALLSTAFF = "Ầm ầm!",		-- 物品名:"地狱魔杖"
        HAMMER_MJOLNIR = "Cái búa mạnh mẽ!",		-- 物品名:"锻锤"
        SPEAR_GUNGNIR = "Đâm đâm đâm!",		-- 物品名:"尖齿矛"
        BLOWDART_LAVA = "Không nên chơi cái đó... hãy để ta chơi",		-- 物品名:"吹箭"
        BLOWDART_LAVA2 = "Trông có vẻ rất nguy hiểm!",		-- 物品名:"熔化吹箭"
        WEBBER_SPIDER_MINION = "Chúng ta đình chiến đi",		-- 物品名:"蜘蛛宝宝"
        BOOK_FOSSIL = "Phía trên viết gì vậy?",		-- 物品名:"石化之书"
		SPEAR_LANCE = "Xoay xoay xoay!",		-- 物品名:"螺旋矛"
		BOOK_ELEMENTAL = "Phía trên viết gì vậy?",		-- 物品名:"召唤之书"
        QUAGMIRE_ALTAR =
        {
        	GENERIC = "Đồ ăn đều để ở đó",		-- 物品名:"饕餮祭坛"->默认
        	FULL = "Nó vừa ăn xong!",		-- 物品名:"饕餮祭坛"->满了
    	},
		QUAGMIRE_SUGARWOODTREE =
		{
			GENERIC = "Bên trong có đồ ngọt!",		-- 物品名:"糖木树"->默认
			STUMP = "Có người đã chặt cây! Không phải là ta làm đâu!",		-- 物品名:"糖木树"->暴食模式糖木树只剩树桩了
			TAPPED_EMPTY = "Ôi... không còn gì",		-- 物品名:"糖木树"->暴食模式糖木树空了
			TAPPED_READY = "Ôi, đều là đồ ngọt!",		-- 物品名:"糖木树"->暴食模式糖木树好了
			TAPPED_BUGS = "Phù, mấy con côn trùng bị dính vào mấy đồ ngọt",		-- 物品名:"糖木树"->暴食模式糖木树上有蚂蚁
			WOUNDED = "Cái cây có ổn không?",		-- 物品名:"糖木树"->暴食糖木树生病了
		},
		QUAGMIRE_SPOTSPICE_SHRUB =
		{
			GENERIC = "Có cảm giác thân thương",		-- 物品名:"带斑点的小灌木"->默认
			PICKED = "Không có gì để ngắt",		-- 物品名:"带斑点的小灌木"->被采完了
		},
		QUAGMIRE_SALT_RACK =
		{
			READY = "Phía trên có gì đó!",		-- 物品名:"盐架"->准备好的 满的
			GENERIC = "Cái này có tác dụng gì?",		-- 物品名:"盐架"->默认
		},
		QUAGMIRE_SAFE =
		{
			GENERIC = "Muốn biết trong rương có gì...",		-- 物品名:"保险箱"->默认
			LOCKED = "Ôi, cho ta vào!!",		-- 物品名:"保险箱"->锁住了
		},
		QUAGMIRE_MUSHROOMSTUMP =
		{
			GENERIC = "Ôi, thì ra nấm mọc ở đây!",		-- 物品名:"蘑菇"->默认
			PICKED = "Không có nấm...?",		-- 物品名:"蘑菇"->被采完了
		},
        QUAGMIRE_RUBBLE_HOUSE =
        {
            "Xin chào?",		-- 物品名:"残破的房子" 制造描述:未找到
            "Ở đây thật hoang vu",		-- 物品名:"残破的房子" 制造描述:未找到
            "Trong nhà không có ai",		-- 物品名:"残破的房子" 制造描述:未找到
        },
        QUAGMIRE_SWAMPIGELDER =
        {
            GENERIC = "Tên người lợn này có hơi kỳ lạ",		-- 物品名:"沼泽猪长老"->默认
            SLEEPING = "Ngủ rồi?",		-- 物品名:"沼泽猪长老"->睡着了
        },
        QUAGMIRE_FOOD =
        {
        	GENERIC = "Nên bỏ nó vào cái miệng to lớn!",		-- 物品名:未找到
            MISMATCH = "Chắc nó sẽ không thích đâu",		-- 物品名:未找到
            MATCH = "Thích hợp với cái miệng to lớn!",		-- 物品名:未找到
            MATCH_BUT_SNACK = "Miệng to như vậy, trông đô ăn thật là nhỏ",		-- 物品名:未找到
        },
        QUAGMIRE_PARK_GATE =
        {
            GENERIC = "Hy vọng bên trong có đồ ngon",		-- 物品名:"铁门"->默认
            LOCKED = "Để ta vào!!",		-- 物品名:"铁门"->锁住了
        },
        QUAGMIRE_PIGEON =
        {
            DEAD = "Ơ, nó chết rồi",		-- 物品名:"鸽子"->死了 制造描述:"这是一只完整的活鸽"
            GENERIC = "Xin chào, chim nhỏ!",		-- 物品名:"鸽子"->默认 制造描述:"这是一只完整的活鸽"
            SLEEPING = "Chim nhỏ ngủ ngon",		-- 物品名:"鸽子"->睡着了 制造描述:"这是一只完整的活鸽"
        },
        WINONA_CATAPULT =
        {
        	GENERIC = "Máy ném đá! Máy ném đá!",		-- 物品名:"薇诺娜的投石机"->默认 制造描述:"向敌人投掷大石块"
        	OFF = "Tại sao không hoạt động vậy?",		-- 物品名:"薇诺娜的投石机"->关闭 制造描述:"向敌人投掷大石块"
        	BURNING = "Có vẻ rất nguy hiểm!",		-- 物品名:"薇诺娜的投石机"->正在燃烧 制造描述:"向敌人投掷大石块"
        	BURNT = "Hu hu hu...",		-- 物品名:"薇诺娜的投石机"->烧焦的 制造描述:"向敌人投掷大石块"
        },
        WINONA_SPOTLIGHT =
        {
        	GENERIC = "Đây là cái gì?",		-- 物品名:"薇诺娜的聚光灯"->默认 制造描述:"白天夜里都发光"
        	OFF = "Ta nghĩ nó mệt rồi",		-- 物品名:"薇诺娜的聚光灯"->关闭 制造描述:"白天夜里都发光"
        	BURNING = "Thứ ánh sáng này không đúng lắm",		-- 物品名:"薇诺娜的聚光灯"->正在燃烧 制造描述:"白天夜里都发光"
        	BURNT = "Không làm được gì",		-- 物品名:"薇诺娜的聚光灯"->烧焦的 制造描述:"白天夜里都发光"
        },
        WINONA_BATTERY_LOW =
        {
        	GENERIC = "Cái hộp này có cành cây kỳ lạ nhô ra",		-- 物品名:"薇诺娜的发电机"->默认 制造描述:"要确保电力供应充足"
        	LOWPOWER = "Cái đèn này càng lúc càng tối...",		-- 物品名:"薇诺娜的发电机"->没电了 制造描述:"要确保电力供应充足"
        	OFF = "Chắc là đang ngủ",		-- 物品名:"薇诺娜的发电机"->关闭 制造描述:"要确保电力供应充足"
        	BURNING = "Không phải là ta làm hư!!",		-- 物品名:"薇诺娜的发电机"->正在燃烧 制造描述:"要确保电力供应充足"
        	BURNT = "Chắc Winona làm một cái khác rồi!",		-- 物品名:"薇诺娜的发电机"->烧焦的 制造描述:"要确保电力供应充足"
        },
        WINONA_BATTERY_HIGH =
        {
        	GENERIC = "Nó có thể làm gì?",		-- 物品名:"薇诺娜的宝石发电机"->默认 制造描述:"这玩意烧宝石，所以肯定不会差"
        	LOWPOWER = "Có vẻ nó mệt rồi",		-- 物品名:"薇诺娜的宝石发电机"->没电了 制造描述:"这玩意烧宝石，所以肯定不会差"
        	OFF = "Nó nên làm gì đó nhỉ?",		-- 物品名:"薇诺娜的宝石发电机"->关闭 制造描述:"这玩意烧宝石，所以肯定不会差"
        	BURNING = "Nó như vậy có bình thường không,?",		-- 物品名:"薇诺娜的宝石发电机"->正在燃烧 制造描述:"这玩意烧宝石，所以肯定不会差"
        	BURNT = "Ôi, bỏ đi",		-- 物品名:"薇诺娜的宝石发电机"->烧焦的 制造描述:"这玩意烧宝石，所以肯定不会差"
        },
        COMPOSTWRAP = "(Ngửi)!!",		-- 物品名:"肥料包" 制造描述:"\"草本\"的疗法"
        ARMOR_BRAMBLE = "Làm từ thực vật!",		-- 物品名:"荆棘外壳" 制造描述:"让大自然告诉你什么叫\"滚开\""
        TRAP_BRAMBLE = "Bẫy thực vật!",		-- 物品名:"荆棘陷阱" 制造描述:"都有机会中招的干\n扰陷阱"
        BOATFRAGMENT03 = "Tạm biệt, cái thuyền nhỏ",		-- 物品名:"船碎片"
        BOATFRAGMENT04 = "Tạm biệt, cái thuyền nhỏ",		-- 物品名:"船碎片"
        BOATFRAGMENT05 = "Tạm biệt, cái thuyền nhỏ",		-- 物品名:"船碎片"
		BOAT_LEAK = "Ta thấy không vấn đề gì",		-- 物品名:"漏洞"
        MAST = "Cái này dùng cho thuyền?",		-- 物品名:"桅杆" 制造描述:"乘风破浪会有时"
        SEASTACK = "Ồ, cục đá to!",		-- 物品名:"浮堆"
        FISHINGNET = "Có thể vớt được rất nhiều cá",		-- 物品名:"渔网" 制造描述:"就是一张网"
        ANTCHOVIES = "Là cá? Hay là sâu bọ?",		-- 物品名:"蚁头凤尾鱼"
        STEERINGWHEEL = "Bây giờ ta là thuyền trưởng!!",		-- 物品名:"方向舵" 制造描述:"航海必备道具"
        ANCHOR = "Buộc vào thuyền",		-- 物品名:"锚" 制造描述:"船用刹车"
        BOATPATCH = "Tại sao phải vá thuyền? Để thuyền vào nước!",		-- 物品名:"船补丁" 制造描述:"打补丁永远不晚"
        DRIFTWOOD_TREE =
        {
            BURNING = "Nóng nóng nóng!",		-- 物品名:"浮木"->正在燃烧
            BURNT = "Không còn gì",		-- 物品名:"浮木"->烧焦的
            CHOPPED = "Chặt!",		-- 物品名:"浮木"->被砍了
            GENERIC = "Nó bị ngâm nước rồi!",		-- 物品名:"浮木"->默认
        },
        DRIFTWOOD_LOG = "Gỗ nổi",		-- 物品名:"浮木桩"
        MOON_TREE =
        {
            BURNING = "Mau cứu nó",		-- 物品名:"月树"->正在燃烧
            BURNT = "Đồ của Quảng Hàn,",		-- 物品名:"月树"->烧焦的
            CHOPPED = "Chỉ còn lại cái gốc",		-- 物品名:"月树"->被砍了
            GENERIC = "Xin cho tôi một đóa hoa",		-- 物品名:"月树"->默认
        },
		MOON_TREE_BLOSSOM = "Mùi thơm của quê hương",		-- 物品名:"月树花"
        MOONBUTTERFLY =
        {
        	GENERIC = "Trên Nguyệt Cung nguyệt nga bay múa, che đậy bầu trời",		-- 物品名:"月蛾"->默认
        	HELD = "Mềm ghê...",		-- 物品名:"月蛾"->拿在手里
        },
		MOONBUTTERFLYWINGS = "Có chút bụi",		-- 物品名:"月蛾翅膀"
        MOONBUTTERFLY_SAPLING = "Nguyệt nga hóa thành cây, quay về nhân gian",		-- 物品名:"月树苗"
        ROCK_AVOCADO_FRUIT = "Ôi! Cứng quá!",		-- 物品名:"石果"
        ROCK_AVOCADO_FRUIT_RIPE = "Chuẩn bị xong rồi!",		-- 物品名:"成熟石果"
        ROCK_AVOCADO_FRUIT_RIPE_COOKED = "Làm cho nó trở nên ngon hơn!",		-- 物品名:"熟石果"
        ROCK_AVOCADO_FRUIT_SPROUT = "Bây giờ vẫn còn quá nhỏ, không thể ăn được",		-- 物品名:"发芽的石果"
        ROCK_AVOCADO_BUSH =
        {
        	BARREN = "Ở đây không có trái cây",		-- 物品名:"石果灌木丛"
			WITHERED = "Có vẻ nó khát nước",		-- 物品名:"石果灌木丛"->枯萎了
			GENERIC = "Ha? Mấy trái cây này trông giống như cục đá vậy",		-- 物品名:"石果灌木丛"->默认
			PICKED = "Bây giờ có thể mọc lại được rồi!",		-- 物品名:"石果灌木丛"->被采完了
			DISEASED = "Trông có vẻ bệnh rồi",		-- 物品名:"石果灌木丛"->生病了
            DISEASING = "Nó có vấn đề rồi...",		-- 物品名:"石果灌木丛"->正在生病？？
			BURNING = "Lửa! Lửa!",		-- 物品名:"石果灌木丛"->正在燃烧
		},
        DEAD_SEA_BONES = "Con cá tội nghiệp, nó nên ở trong nước",		-- 物品名:"海骨"
        HOTSPRING =
        {
        	GENERIC = "Ta đi ngâm nước đây!",		-- 物品名:"温泉"->默认
        	BOMBED = "Trông có vẻ ấm áp thoải mái!",		-- 物品名:"温泉"
        	GLASS = "Đẹp quá!",		-- 物品名:"温泉"
			EMPTY = "Không có gì hết!",		-- 物品名:"温泉"
        },
        MOONGLASS = "Mũi dao hoài niệm!",		-- 物品名:"月亮碎片"
        MOONGLASS_ROCK = "Hoàn mỹ như ngọc bích",		-- 物品名:"月光玻璃"
        BATHBOMB = "Giờ muốn ném!",		-- 物品名:"沐浴球" 制造描述:"春天里来百花香？这点子把地都炸碎了"
        TRAP_STARFISH =
        {
            GENERIC = "Muốn đâm một cái!",		-- 物品名:"海星"->默认
            CLOSED = "Nó xuýt nữa nuốt ta vào rồi!",		-- 物品名:"海星"
        },
        DUG_TRAP_STARFISH = "Tìm một vị trí tốt cho ngươi, he he",		-- 物品名:"海星陷阱"
        SPIDER_MOON =
        {
        	GENERIC = "Chưa từng thấy người nhện như vậy",		-- 物品名:"破碎蜘蛛"->默认
        	SLEEPING = "Suỵt...",		-- 物品名:"破碎蜘蛛"->睡着了
        	DEAD = "Tạm biệt!",		-- 物品名:"破碎蜘蛛"->死了
        },
        MOONSPIDERDEN = "Ưu tiên người nhện ác độc!!",		-- 物品名:"破碎蜘蛛洞"
		FRUITDRAGON =
		{
			GENERIC = "Không người bình thường nào có thể chịu được màu xanh",		-- 物品名:"沙拉蝾螈"->默认
			RIPE = "Ngửi thấy giống... trái cây?",		-- 物品名:"沙拉蝾螈"
			SLEEPING = "Nó đang ngủ",		-- 物品名:"沙拉蝾螈"->睡着了
		},
        PUFFIN =
        {
            GENERIC = "Con chim mập",		-- 物品名:"海鹦鹉"->默认
            HELD = "Bắt được nó rồi!!",		-- 物品名:"海鹦鹉"->拿在手里
            SLEEPING = "Con chim nhỏ đang ngủ",		-- 物品名:"海鹦鹉"->睡着了
        },
		MOONGLASSAXE = "Chặt cũng thuận tay!",		-- 物品名:"月光玻璃斧" 制造描述:"脆弱而有效"
		GLASSCUTTER = "Bén ghê!",		-- 物品名:"玻璃刀" 制造描述:"尖端武器"
        ICEBERG =
        {
            GENERIC = "Cẩn thận!",		-- 物品名:"小冰山"->默认
            MELTED = "Tan hết rồi!",		-- 物品名:"小冰山"->融化了
        },
        ICEBERG_MELTED = "Tan hết rồi!",		-- 物品名:"融化的冰山"
        MINIFLARE = "Hoa lửa phát sáng!",		-- 物品名:"信号" 制造描述:"为你信任的朋友照亮前路"
		MOON_FISSURE =
		{
			GENERIC = "Cái đầu cảm thấy kỳ quái", 		-- 物品名:"天体裂隙"->默认
			NOLIGHT = "Tiếng vọng! Tiếng vọng! Tiếng vọng vọng...",		-- 物品名:"天体裂隙"
		},
        MOON_ALTAR =
        {
            MOON_ALTAR_WIP = "Vẫn cần ít đồ",		-- 物品名:未找到
            GENERIC = "Cần giúp đỡ không?",		-- 物品名:未找到
        },
        MOON_ALTAR_IDOL = "Ngươi muốn đi đâu?",		-- 物品名:"天体祭坛雕像"
        MOON_ALTAR_GLASS = "Nó ở đây trông rất bi thương",		-- 物品名:"天体祭坛底座"
        MOON_ALTAR_SEED = "Ngươi nên về nhà rồi",		-- 物品名:"天体祭坛宝球"
        MOON_ALTAR_ROCK_IDOL = "Xin chào?",		-- 物品名:"在呼唤我"
        MOON_ALTAR_ROCK_GLASS = "Xin chào?",		-- 物品名:"在呼唤我"
        MOON_ALTAR_ROCK_SEED = "Xin chào?",		-- 物品名:"在呼唤我"
        SEAFARING_PROTOTYPER =
        {
            GENERIC = "Làm đồ dùng trong nước!",		-- 物品名:"智囊团"->默认 制造描述:"海上科学"
            BURNT = "Những chuyện như vậy hình như thường xuyên xảy ra...",		-- 物品名:"智囊团"->烧焦的 制造描述:"海上科学"
        },
        BOAT_ITEM = "Có thể di chuyển trên mặt nước!",		-- 物品名:"船套装" 制造描述:"让大海成为你的领地"
        STEERINGWHEEL_ITEM = "Cái này đặt trên thuyền?",		-- 物品名:"方向舵套装" 制造描述:"航海必备道具"
        ANCHOR_ITEM = "Làm dụng cụ để buộc thuyền!",		-- 物品名:"锚套装" 制造描述:"船用刹车"
        MAST_ITEM = "Một phần quan trọng của thuyền",		-- 物品名:"桅杆套装" 制造描述:"乘风破浪会有时"
        MUTATEDHOUND =
        {
        	DEAD = "Tạm biệt chó con",		-- 物品名:"恐怖猎犬"->死了
        	GENERIC = "Hình như bệnh rồi?",		-- 物品名:"恐怖猎犬"->默认
        	SLEEPING = "Đừng đánh thức nó",		-- 物品名:"恐怖猎犬"->睡着了
        },
        MUTATED_PENGUIN =
        {
			DEAD = "Như vậy càng tốt",		-- 物品名:"月石企鸥"->死了
			GENERIC = "Có thể nhìn thấy bên trong của nó!",		-- 物品名:"月石企鸥"->默认
			SLEEPING = "Ngủ ngon, con chim nhỏ đáng sợ",		-- 物品名:"月石企鸥"->睡着了
		},
        CARRAT =
        {
        	DEAD = "Bất động rồi",		-- 物品名:"胡萝卜"->死了 制造描述:"灵巧机敏，富含胡萝卜素"
        	GENERIC = "Đó là cái gì?",		-- 物品名:"胡萝卜"->默认 制造描述:"灵巧机敏，富含胡萝卜素"
        	HELD = "Đồ ăn hay thú cưng?",		-- 物品名:"胡萝卜"->拿在手里 制造描述:"灵巧机敏，富含胡萝卜素"
        	SLEEPING = "Nó đang ngủ ngon",		-- 物品名:"胡萝卜"->睡着了 制造描述:"灵巧机敏，富含胡萝卜素"
        },
		BULLKELP_PLANT =
        {
            GENERIC = "Đồ ăn mọc ở nước!",		-- 物品名:"公牛海带"->默认
            PICKED = "Lát nữa sẽ mọc lại",		-- 物品名:"公牛海带"->被采完了
        },
		BULLKELP_ROOT = "Trồng thêm ít đồ ăn mọc dưới nước!",		-- 物品名:"公牛海带茎"
        KELPHAT = "Ta thà ăn nó cũng không muốn đội nó",		-- 物品名:"海花冠" 制造描述:"让人神经焦虑的东西"
		KELP = "Đồ ăn dưới biển!",		-- 物品名:"海带叶"
		KELP_COOKED = "Uh, dính dính!",		-- 物品名:"熟海带叶"
		KELP_DRIED = "Bánh quế mặn!",		-- 物品名:"干海带叶"
		GESTALT = "Bọn họ muốn kể chuyện",		-- 物品名:"虚空之影"
		COOKIECUTTER = "Trông rất thân thiện",		-- 物品名:"饼干切割机"
		COOKIECUTTERSHELL = "Ha ha! Giờ là của ta!",		-- 物品名:"饼干切割机壳"
		COOKIECUTTERHAT = "Cái mũ gai rất tốt!",		-- 物品名:"饼干切割机帽子" 制造描述:"穿着必须犀利"
		SALTSTACK =
		{
			GENERIC = "Cục đá kỳ lạ",		-- 物品名:"盐堆"->默认
			MINED_OUT = "Không có gì để cầm",		-- 物品名:"盐堆"
			GROWING = "Nó đang sinh trưởng!",		-- 物品名:"盐堆"->正在生长
		},
		SALTROCK = "Cục đá kỳ lạ",		-- 物品名:"盐晶"
		SALTBOX = "Rất thích hợp để giấu đồ ăn ngon",		-- 物品名:"盐盒" 制造描述:"用盐来储存食物"
		TACKLESTATION = "Không được đối xử với cá như vậy!",		-- 物品名:"钓具容器" 制造描述:"传统的用饵钓鱼"
		TACKLESKETCH = "Uh, hình ảnh!",		-- 物品名:"{item}广告"
        MALBATROSS = "Con chim xấu xa!",		-- 物品名:"邪天翁"
        MALBATROSS_FEATHER = "Lấy từ con chim xấu xa!",		-- 物品名:"邪天翁羽毛"
        MALBATROSS_BEAK = "Két! Két! He he...",		-- 物品名:"邪天翁喙"
        MAST_MALBATROSS_ITEM = "Buồm lông chim",		-- 物品名:"飞翼风帆套装" 制造描述:"像海鸟一样航向深蓝"
        MAST_MALBATROSS = "Có thể khiến thuyền bay được không? Không được? Ôi...",		-- 物品名:"飞翼风帆" 制造描述:"像海鸟一样航向深蓝"
		MALBATROSS_FEATHERED_WEAVE = "Con chim làm đó!",		-- 物品名:"羽毛帆布" 制造描述:"精美的羽毛布料"
        GNARWAIL =
        {
            GENERIC = "Con cá có bộ dạng kỳ quái",		-- 物品名:"一角鲸"->默认
            BROKENHORN = "Sừng gãy rồi cũng không tệ đến vậy!",		-- 物品名:"一角鲸"
            FOLLOWER = "Chúng ta giờ là bạn rồi!",		-- 物品名:"一角鲸"->追随者
            BROKENHORN_FOLLOWER = "Sừng gãy rồi cũng không tệ đến vậy!",		-- 物品名:"一角鲸"
        },
        GNARWAIL_HORN = "Ha ha! Giờ là của ta!",		-- 物品名:"一角鲸的角"
        WALKINGPLANK = "Ván nhảy!",		-- 物品名:"木板"
        OAR = "Có thể khiến cho thuyền di chuyển!",		-- 物品名:"桨" 制造描述:"划，划，划小船"
		OAR_DRIFTWOOD = "Có thể khiến cho thuyền di chuyển!",		-- 物品名:"浮木桨" 制造描述:"小桨要用浮木造？"
		OCEANFISHINGROD = "Phải bắt cá ở đại dương!",		-- 物品名:"海钓竿" 制造描述:"像职业选手一样钓鱼吧"
		OCEANFISHINGBOBBER_NONE = "Thiếu món đồ...",		-- 物品名:"鱼钩"
        OCEANFISHINGBOBBER_BALL = "Phao đang nổi!",		-- 物品名:"木球浮标" 制造描述:"经典设计，初学者和职业钓手两相宜"
        OCEANFISHINGBOBBER_OVAL = "Phao đang nổi!",		-- 物品名:"硬物浮标" 制造描述:"在经典浮标的基础上融入了时尚设计"
		OCEANFISHINGBOBBER_CROW = "Lông vũ đang nổi!",		-- 物品名:"黑羽浮标" 制造描述:"深受职业钓手的喜爱"
		OCEANFISHINGBOBBER_ROBIN = "Lông vũ đang nổi!",		-- 物品名:"红羽浮标" 制造描述:"深受职业钓手的喜爱"
		OCEANFISHINGBOBBER_ROBIN_WINTER = "Lông vũ đang nổi!",		-- 物品名:"蔚蓝羽浮标" 制造描述:"深受职业钓手的喜爱"
		OCEANFISHINGBOBBER_CANARY = "Lông vũ đang nổi!",		-- 物品名:"番红花羽浮标" 制造描述:"深受职业钓手的喜爱"
		OCEANFISHINGBOBBER_GOOSE = "Lông vũ lớn đang nổi!",		-- 物品名:"鹅羽浮标" 制造描述:"高级羽绒浮标"
		OCEANFISHINGBOBBER_MALBATROSS = "Lông vũ lớn đang nổi!",		-- 物品名:"邪天翁羽浮标" 制造描述:"高级巨鸟浮标"
		OCEANFISHINGLURE_SPINNER_RED = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"日出旋转亮片" 制造描述:"早起的鱼儿有虫吃"
		OCEANFISHINGLURE_SPINNER_GREEN = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"黄昏旋转亮片" 制造描述:"低光照环境里效果最好"
		OCEANFISHINGLURE_SPINNER_BLUE = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"夜飞侠旋转亮片" 制造描述:"适用于夜间垂钓"
		OCEANFISHINGLURE_SPOON_RED = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"日出匙型假饵" 制造描述:"早起的鱼儿有虫吃"
		OCEANFISHINGLURE_SPOON_GREEN = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"黄昏匙型假饵" 制造描述:"在夕阳中继续垂钓"
		OCEANFISHINGLURE_SPOON_BLUE = "Sẽ không gây tổn hại cho cá, đúng không?",		-- 物品名:"匙型假饵" 制造描述:"适用于夜间垂钓"
		OCEANFISH_SMALL_1 = "Ôi, nhỏ ghê!",		-- 物品名:"小孔雀鱼"
		OCEANFISH_SMALL_2 = "Làm thú cưng của ta nhe! Ta sẽ cho ngươi ăn, yêu thương ngươi",		-- 物品名:"针鼻喷墨鱼"
		OCEANFISH_SMALL_3 = "Hazz con cá nhỏ!",		-- 物品名:"小饵鱼"
		OCEANFISH_SMALL_4 = "Bé cá nhỏ!",		-- 物品名:"三文鱼苗"
		OCEANFISH_SMALL_5 = "He he, trông có hơi ngốc!",		-- 物品名:"爆米花鱼"
		OCEANFISH_MEDIUM_1 = "Rất dính!",		-- 物品名:"泥鱼"
		OCEANFISH_MEDIUM_2 = "Con mắt vừa to vừa đẹp!",		-- 物品名:"斑鱼"
		OCEANFISH_MEDIUM_3 = "Đầu đội vương miện gai!",		-- 物品名:"浮夸狮子鱼"
		OCEANFISH_MEDIUM_4 = "Ngươi làm thú cưng của ta được không?",		-- 物品名:"黑鲶鱼"
		OCEANFISH_MEDIUM_5 = "Cảm giác... có hơi kỳ lạ",		-- 物品名:"玉米鳕鱼"
		OCEANFISH_MEDIUM_6 = "Ngươi thật là đẹp!",		-- 物品名:"花锦鲤"
		OCEANFISH_MEDIUM_7 = "Thích cái vảy của ngươi!",		-- 物品名:"金锦鲤"
		PONDFISH = "A, đáng yêu quá!",		-- 物品名:"淡水鱼"
		PONDEEL = "Xin chào con cá dài!",		-- 物品名:"鳗鱼"
        FISHMEAT = "Không!!",		-- 物品名:"生鱼肉"
        FISHMEAT_COOKED = "Ta không ăn!",		-- 物品名:"鱼排"
        FISHMEAT_SMALL = "Nó chỉ là đứa trẻ...",		-- 物品名:"小鱼块"
        FISHMEAT_SMALL_COOKED = "Ai làm chuyện này chứ!",		-- 物品名:"烤小鱼块"
		SPOILED_FISH = "Ai đó đã không chăm sóc tốt thú cưng cá!",		-- 物品名:"变质的鱼"
		FISH_BOX = "Họ để thú cưng cá ở đây à?",		-- 物品名:"锡鱼桶" 制造描述:"保持鱼与网捕之日一样新鲜"
        POCKET_SCALE = "Thiết bị đo lường cổ quái",		-- 物品名:"弹簧秤" 制造描述:"随时称鱼的重量！"
		TROPHYSCALE_FISH =
		{
			GENERIC = "Đây là nhà của cá",		-- 物品名:"鱼类计重器"->默认 制造描述:"炫耀你的斩获"
			HAS_ITEM = "Trọng lượng: {weight}\nNgười bắt: {owner}",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获"
			BURNING = "A a a! Không không không!",		-- 物品名:"鱼类计重器"->正在燃烧 制造描述:"炫耀你的斩获"
			BURNT = "(ngửi) Nhà cá đáng thương...",		-- 物品名:"鱼类计重器"->烧焦的 制造描述:"炫耀你的斩获"
			OWNER = "Trọng lượng: {weight}\nNgười bắt: {owner}\nHe he cá của ta ngon nhất",		-- 物品名:"鱼类计重器" 制造描述:"炫耀你的斩获"
		},
		OCEANFISHABLEFLOTSAM = "Ô! Tìm được bùn rồi!",		-- 物品名:"海洋残骸"
		CALIFORNIAROLL = "Đợi đã... bên trong có cá!",		-- 物品名:"加州卷"
		SEAFOODGUMBO = "Con cá bên trong trông rất tội nghiệp...",		-- 物品名:"海鲜浓汤"
		SURFNTURF = "Hứ! Không muốn ăn!",		-- 物品名:"海鲜大排档"
		GHOSTLYELIXIR_SLOWREGEN = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"亡者补药" 制造描述:"时间会抚平所有伤口"
		GHOSTLYELIXIR_FASTREGEN = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"灵魂万灵药" 制造描述:"治疗重伤的强力药剂"
		GHOSTLYELIXIR_SHIELD = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"不屈药剂" 制造描述:"保护你的姐妹不受伤害"
		GHOSTLYELIXIR_ATTACK = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"易怒药水" 制造描述:"重燃阿比盖尔的战斗精神"
		GHOSTLYELIXIR_SPEED = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"强健精油" 制造描述:"给你的灵魂来一剂强心针"
		GHOSTLYELIXIR_RETALIATION = "Ô! Ta cũng muốn làm đồ thủ công,",		-- 物品名:"蒸馏复仇" 制造描述:"对敌人以牙还牙"
		SISTURN =
		{
			GENERIC = "Trong cái hũ có gì vậy?",		-- 物品名:"姐妹骨灰罐"->默认 制造描述:"让你疲倦的灵魂休息的地方"
			SOME_FLOWERS = "Hắn thích để đồ ở đây",		-- 物品名:"姐妹骨灰罐" 制造描述:"让你疲倦的灵魂休息的地方"
			LOTS_OF_FLOWERS = "Thì ra không phải...?",		-- 物品名:"姐妹骨灰罐" 制造描述:"让你疲倦的灵魂休息的地方"
		},
        PORTABLECOOKPOT_ITEM =
        {
            GENERIC = "Bỏ đồ vào trong thì sẽ rơi ra đồ ăn khác?",		-- 物品名:"便携烹饪锅"->默认 制造描述:"随时随地为美食家服务"
            DONE = "Ô~~!",		-- 物品名:"便携烹饪锅"->完成了 制造描述:"随时随地为美食家服务"
			COOKING_LONG = "Cái này lâu đó!",		-- 物品名:"便携烹饪锅"->饭还需要很久 制造描述:"随时随地为美食家服务"
			COOKING_SHORT = "Đồ ăn! Đồ ăn! Đồ ăn!",		-- 物品名:"便携烹饪锅"->饭快做好了 制造描述:"随时随地为美食家服务"
			EMPTY = "Hả? Bên trong không có gì!",		-- 物品名:"便携烹饪锅" 制造描述:"随时随地为美食家服务"
        },
        PORTABLEBLENDER_ITEM = "Lảo đảo!",		-- 物品名:"便携研磨器" 制造描述:"把原料研磨成粉状调味品"
        PORTABLESPICER_ITEM =
        {
            GENERIC = "Khiến cho món ăn có thêm mùi vị!",		-- 物品名:"便携香料站"->默认 制造描述:"调味让饭菜更可口"
            DONE = "Xong hết rồi!",		-- 物品名:"便携香料站"->完成了 制造描述:"调味让饭菜更可口"
        },
        SPICEPACK = "Bỏ đồ ăn vào trong!",		-- 物品名:"厨师袋" 制造描述:"使你的食物保持新鲜"
        SPICE_GARLIC = "Uh có thể khiến cho hơi thở trở nên thơm mát",		-- 物品名:"蒜粉" 制造描述:"用口臭防守是最好的进攻"
        SPICE_SUGAR = "Rất ngon!!",		-- 物品名:"蜂蜜水晶" 制造描述:"令人心平气和的甜美"
        SPICE_CHILI = "Đồ cay!",		-- 物品名:"辣椒面" 制造描述:"刺激十足的粉末"
        SPICE_SALT = "Thứ cát này rất ngon!",		-- 物品名:"调味盐" 制造描述:"为你的饭菜加点咸味"
        MONSTERTARTARE = "Đến cả cái tên cũng vô dụng",		-- 物品名:"怪物鞑靼"
        FRESHFRUITCREPES = "Bánh rán trái cây tính tế!",		-- 物品名:"鲜果可丽饼"
        FROGFISHBOWL = "...",		-- 物品名:"蓝带鱼排"
        POTATOTORNADO = "He he, khoai tây xoay tròn!",		-- 物品名:"花式回旋块茎"
        DRAGONCHILISALAD = "Uhm~, cảm ơn ngươi, đầu bếp cao cấp!",		-- 物品名:"辣龙椒沙拉"
        GLOWBERRYMOUSSE = "Ô, nó đang phát sáng!!",		-- 物品名:"发光蓝莓慕斯"
        VOLTGOATJELLY = "Ôi, nó lắc qua lắc lại!",		-- 物品名:"闪电羊肉冻"
        NIGHTMAREPIE = "He he, nó có cái mặt thật là vui",		-- 物品名:"恐怖国王饼"
        BONESOUP = "Không cần",		-- 物品名:"骨头汤"
        MASHEDPOTATOES = "Khoai tây nhào mềm mềm!",		-- 物品名:"奶油土豆泥"
        POTATOSOUFFLE = "Cái gì? Đây là khoai tây!!",		-- 物品名:"蓬松土豆酥"
        MOQUECA = "Bên trong có cá!",		-- 物品名:"海鲜杂烩"
        GAZPACHO = "Rất dính!",		-- 物品名:"芦笋冷汤"
        ASPARAGUSSOUP = "Uh...",		-- 物品名:"芦笋汤"
        VEGSTINGER = "Nước ớt tinh xảo!",		-- 物品名:"辛辣鸡尾酒"
        BANANAPOP = "Mùi hương sẽ thơm hơn khi trái cây cắm vào que!",		-- 物品名:"香蕉冻"
        CEVICHE = "Không cần - không cần",		-- 物品名:"酸橘汁腌鱼"
        SALSA = "Sốt rau cải tê cay!",		-- 物品名:"生鲜萨尔萨酱"
        PEPPERPOPPER = "Vốn dĩ không có \"nổ\", chỉ có cay mà thôi!",		-- 物品名:"爆炒填馅辣椒"
        TURNIP = "Món ăn vặt giòn giòn!",		-- 物品名:"大萝卜"
        TURNIP_COOKED = "Nướng thơm quá!",		-- 物品名:"烤大萝卜"
        TURNIP_SEEDS = "Có thể trồng được rất nhiều cà rốt!",		-- 物品名:"萝卜籽"
        GARLIC = "Có thể khiến cho hơi thở thơm mát!",		-- 物品名:"大蒜"
        GARLIC_COOKED = "Uh...Món ăn vặt hăng cay!",		-- 物品名:"烤大蒜"
        GARLIC_SEEDS = "Có thể trồng được rất nhiều tỏi!",		-- 物品名:"蒜籽"
        ONION = "Uh-uhm, rộp rộp!",		-- 物品名:"洋葱"
        ONION_COOKED = "Thơm ghê!",		-- 物品名:"烤洋葱"
        ONION_SEEDS = "Bé cà rốt",		-- 物品名:"洋葱籽"
        POTATO = "Rau củ dưới đất rất ngon",		-- 物品名:"土豆"
        POTATO_COOKED = "Uhm, khoai tây nóng!",		-- 物品名:"烤土豆"
        POTATO_SEEDS = "Chôn dưới đất, trồng thêm nhiều khoai tây!",		-- 物品名:"土豆籽"
        TOMATO = "Quả cà chua vừa ton vừa có nước!",		-- 物品名:"番茄"
        TOMATO_COOKED = "Dính dính",		-- 物品名:"烤番茄"
        TOMATO_SEEDS = "Có thể trồng được rất nhiều cà chua!",		-- 物品名:"番茄跟籽"
        ASPARAGUS = "Món ăn vặt rất ngon!", 		-- 物品名:"芦笋"
        ASPARAGUS_COOKED = "Món ăn vặt ngon nóng!",		-- 物品名:"烤芦笋"
        ASPARAGUS_SEEDS = "Có thể mọc ra rất nhiều món ăn vặt!",		-- 物品名:"芦笋种子"
        PEPPER = "Cay! Miệng thổi lửa luôn rồi!",		-- 物品名:"辣椒"
        PEPPER_COOKED = "Tại lại làm cho ớt hay hơn vậy?",		-- 物品名:"烤辣椒"
        PEPPER_SEEDS = "Có thể trồng ra rất nhiều ớt!",		-- 物品名:"辣椒籽"
        WEREITEM_BEAVER = "Bụng của nó đang trào ra ngoài",		-- 物品名:"俗气海狸像" 制造描述:"唤醒海狸人的诅咒"
        WEREITEM_GOOSE = "Muốn chơi với búp bê!",		-- 物品名:"俗气鹅像" 制造描述:"唤醒鹅人的诅咒"
        WEREITEM_MOOSE = "Wickerbottom nói lúc ăn phải ngậm miệng lại",		-- 物品名:"俗气鹿像" 制造描述:"唤醒鹿人的诅咒"
        MERMHAT = "Có thể khiến người ta hóa trang thành người cá thân thiện!",		-- 物品名:"聪明的伪装" 制造描述:"鱼人化你的朋友"
        MERMTHRONE =
        {
            GENERIC = "Ngai vàng của vua người cá!",		-- 物品名:"皇家地毯"->默认
            BURNT = "Ai làm vậy?!",		-- 物品名:"皇家地毯"->烧焦的
        },
        MERMTHRONE_CONSTRUCTION =
        {
            GENERIC = "Trong truyện cổ tích rất nhiều quốc vương... dường như rất dễ lên làm!",		-- 物品名:"皇家手工套装"->默认 制造描述:"建立一个新的鱼人王朝"
            BURNT = "Không không không!!",		-- 物品名:"皇家手工套装"->烧焦的 制造描述:"建立一个新的鱼人王朝"
        },
        MERMHOUSE_CRAFTED =
        {
            GENERIC = "Ta cũng muốn có một cái nhà!",		-- 物品名:"鱼人工艺屋"->默认 制造描述:"适合鱼人的家"
            BURNT = "Tại sao!?",		-- 物品名:"鱼人工艺屋"->烧焦的 制造描述:"适合鱼人的家"
        },
        MERMWATCHTOWER_REGULAR = "Cần vệ sĩ hoàng gia bảo vệ tân quốc vương!",		-- 物品名:"鱼人工艺屋" 制造描述:"适合鱼人的家"
        MERMWATCHTOWER_NOKING = "Vệ sĩ hoàng gia thiếu quốc vương...",		-- 物品名:"鱼人工艺屋" 制造描述:"适合鱼人的家"
        MERMKING = "Yeah! Trông ngươi giống quốc vương trong truyện cổ tích!",		-- 物品名:"鱼人之王"
        MERMGUARD = "Sẽ có ngày ta cũng sẽ trở nên mạnh mẽ như bọn họ vậy!",		-- 物品名:"忠诚鱼人守卫"
        MERM_PRINCE = "Muốn trở thành hoàng đế thì phải mập trước đã",		-- 物品名:"过程中的皇室"
        SQUID = "Đứng lại không được di chuyển, con mực kia!",		-- 物品名:"鱿鱼"
		GHOSTFLOWER = "Thật đáng sợ!",		-- 物品名:"哀悼荣耀"
        SMALLGHOST = "Phù! Ngươi - ngươi không dọa được ta đâu!",		-- 物品名:"小惊吓"
    },
    DESCRIBE_GENERIC = "Đó là gì?",		--检查物品描述的缺省值
    DESCRIBE_TOODARK = "Tối quá, tối quá",		--天太黑
    DESCRIBE_SMOLDERING = "Có thứ đang cháy âm ỉ...",		--冒烟
    EAT_FOOD =
    {
        TALLBIRDEGG_CRACKED = "Phù, ai lại ăn cái này chứ?",		--吃孵化的高脚鸟蛋
		WINTERSFEASTFUEL = "Uhm, rất ngọt!",		--暂无注释
    },
}
