

local  save_yutu_songs = Class(function(self, inst)
	self.inst = inst
	self.songs = {}
end)

function save_yutu_songs:PreInfoSave()
	for _, v in ipairs(MYTH_YUTU_SONGS) do
		if self.inst.components.builder and self.inst.components.builder:KnowsRecipe(v) then
			table.insert(self.songs,v)
		end
	end
end

function save_yutu_songs:DoneInfoLoad()
	for _,v in ipairs(self.songs) do
		if self.inst.components.builder then
			self.inst.components.builder:UnlockRecipe(v)
		end
	end
	self.songs = {}
end

return save_yutu_songs


