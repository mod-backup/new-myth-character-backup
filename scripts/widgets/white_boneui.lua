local Widget = require "widgets/widget"
local Image = require "widgets/image"
local Text = require "widgets/text"
local ImageButton = require "widgets/imagebutton"
local UIAnimButton = require "widgets/uianimbutton"

local UIAnim = require "widgets/uianim"

local function IsHUDScreen()
	local defaultscreen = false
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name and type(TheFrontEnd:GetActiveScreen().name) == "string" and TheFrontEnd:GetActiveScreen().name == "HUD" then
		defaultscreen = true
	end
	return defaultscreen
end

local White_Boneui = Class(Widget, function(self, owner)
	Widget._ctor(self, "White_Boneui")
	
	self.owner = owner
	self.wbbg = self:AddChild(Widget("wbbg")) 	
	-- ui移除时干掉监听器
	self.inst:ListenForEvent("onremove", function()
		if self.handler ~= nil then
			self.handler:Remove()
		end
	end)
	
	if self.owner:HasTag("white_bone") then
		--按键
		self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			self.wbimage.onclick() 
		end)

		self.wbimage = self.wbbg:AddChild(UIAnimButton("white_boneui", "white_boneui"))
		self.wbimage.animstate:PlayAnimation("idle")
		self.wbimage:SetOnClick(function()
			self.wbimage.animstate:PlayAnimation("click")
			self.wbimage.animstate:PushAnimation("idle",false)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],2)
		end)
    elseif self.owner:HasTag("monkey_king") then
    	--按键
    	self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			self.wbimage.onclick() 
		end)
		
		self.wbimage = self.wbbg:AddChild(UIAnimButton("monkey_kingui", "monkey_kingui"))
		self.wbimage.animstate:PlayAnimation("idle_normal")
		self.checkfn = function(owner)
			if owner._mknightvision:value() == false then
				if not self.wbimage.animstate:IsCurrentAnimation("turn_off") then
					self.wbimage.animstate:PlayAnimation("idle_normal")
				end
			end
		end
		self.wbimage:SetOnClick(function()
			if self.inst.click_incd then return end
			if self.owner._mknightvision ~= nil and self.owner._mknightvision:value() == false then
				self.wbimage.animstate:PlayAnimation("turn_on")
			else
				self.wbimage.animstate:PlayAnimation("turn_off")
			end
			self.inst.click_incd = true 
			self.owner:DoTaskInTime(0.3, function() self.inst.click_incd = false self.checkfn(self.owner) end)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],1)
		end)
		self.owner:ListenForEvent("monkeynightvisiondirty", function(inst)
			self.checkfn(inst)
		end)
    elseif self.owner:HasTag("pigsy") then
    	--按键
    	self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			if not (self.wbimage and self.wbimage.shown) then return end
			self.wbimage.onclick() 
		end)
		
		self.wbimage = self.wbbg:AddChild(UIAnimButton("pigsy_marryui", "pigsy_marryui"))
		self.wbimage.animstate:PlayAnimation("turnoff")
		self.checkfn = function(owner)
			if owner._becomehpig:value() == false then
				if not self.wbimage.animstate:IsCurrentAnimation("turnoff") then
					self.wbimage.animstate:PlayAnimation("turnoff")
				end
			end
		end
		self.wbimage:SetOnClick(function()
			if self.inst.click_incd then return end
			if self.owner._becomehpig ~= nil and self.owner._becomehpig:value() == false then
				self.wbimage.animstate:PlayAnimation("turnon")
				self:SpawnPgFx()
			else
				self.wbimage.animstate:PlayAnimation("turnoff")
			end
			self.inst.click_incd = true 
			self.owner:DoTaskInTime(0.3, function() self.inst.click_incd = false self.checkfn(self.owner) end)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],4)
		end)
		self.owner:ListenForEvent("becomehpigdirty", function(inst)
			self.checkfn(inst)
		end)
		self.owner:ListenForEvent("canbecomehpigdirty", function(inst)
			if self.owner._canbecomehpig:value() then
				self.wbimage:Show()
			end	
		end)
		self.wbimage:Hide()
		self.inst:DoTaskInTime(0.1,function()
			if self.owner._becomehpig:value() then
				self.wbimage.animstate:PlayAnimation("turnon")
			end
			if self.owner._canbecomehpig:value() then
				self.wbimage:Show()
			end			
		end)

	elseif self.owner:HasTag("yangjian") then
		--按键
		self.handler =
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			self.wbimage.onclick() 
		end)
		self.wbimage = self.wbbg:AddChild(UIAnimButton("yangjianui", "yangjianui"))
		self.wbimage.animstate:PlayAnimation("idle")
		self.wbimage:SetOnClick(function()
			if self.inst.click_incd then return end
			if self.owner._skyeye ~= nil and self.owner._skyeye:value() == false then
				self:SpawnYJFx()
				self.wbimage.animstate:PlayAnimation("turn_on")
				self.wbimage.animstate:PushAnimation("opened",false)
			else
				self.wbimage.animstate:PlayAnimation("turn_off")
				self.wbimage.animstate:PushAnimation("idle",false)
			end
			self.inst.click_incd = true 
			self.owner:DoTaskInTime(0.3, function() self.inst.click_incd = false end)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],3)
		end)
		self.owner:ListenForEvent("skyeye_dirty", function(inst)
			if inst._skyeye:value() == false then
				if not self.wbimage.animstate:IsCurrentAnimation("turn_off") then
					self.wbimage.animstate:PlayAnimation("idle")
				end
			end
		end)
		--黑黑白白
	elseif self.owner:HasTag("yama_commissioners") then
		self.handler =
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			self.wbimage.onclick() 
		end)
		self.wbimage = self.wbbg:AddChild(UIAnimButton("yama_commissionersui", "yama_commissionersui"))
		self.wbimage.animstate:PlayAnimation("white")

		self.checkfn = function(owner)
			if owner._commissioner_balck:value() == false then
				if not self.wbimage.animstate:IsCurrentAnimation("turn_off") then
					self.wbimage.animstate:PlayAnimation("white")
				end
			end
		end

		self.wbimage:SetOnClick(function()
			if self.inst.click_incd then return end
			self.inst.click_incd = true 
			self.owner:DoTaskInTime(1.2, function() self.inst.click_incd = false self.checkfn(self.owner) end)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],5)
		end)
		self.owner:ListenForEvent("yama_commissioner_dirty", function(inst)
			if self.owner._commissioner_balck:value() then 
				self.wbimage.animstate:PushAnimation("black",false)
				self:SpawnYAFx({116/255, 1/255, 55/255 , 0.6})
			else
				self:SpawnYAFx( {41/255, 160/255,173/255 , 0.6 })
				self.wbimage.animstate:PushAnimation("white",false)
			end
		end)
	--盘丝盘丝
	elseif self.owner:HasTag("madameweb") then
		self.handler =
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			self.wbimage.onclick() 
		end)
		self.wbimage = self.wbbg:AddChild(UIAnimButton("madamewebui", "madamewebui"))
		self.wbimage.animstate:PlayAnimation("white")

		self.checkfn = function(owner)
			if owner._beacmespider:value() == false then
				if not self.wbimage.animstate:IsCurrentAnimation("turn_off") then
					self.wbimage.animstate:PlayAnimation("white")
				end
			end
		end
		self.wbimage:SetOnClick(function()
			if self.inst.click_incd then return end
			self.inst.click_incd = true 
			self.owner:DoTaskInTime(1.2, function() self.inst.click_incd = false self.checkfn(self.owner) end)
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],6)
		end)
		self.owner:ListenForEvent("beacmespiderdirty", function(inst)
			if self.owner._beacmespider:value() then 
				self.wbimage.animstate:PushAnimation("black",false)
				self:SpawnMaFx()
			else
				self:SpawnMaFx()
				self.wbimage.animstate:PushAnimation("white",false)
			end
		end)
		self.owner:ListenForEvent("canbecomespiderdirty", function(inst)
			if self.owner._canbecomespider:value() then
				self.wbimage:Show()
			end	
		end)
		self.wbimage:Hide()
		self.inst:DoTaskInTime(0.1,function()
			if self.owner._beacmespider:value() then
				self.wbimage.animstate:PlayAnimation("turnon")
			end
			if self.owner._canbecomespider:value() then
				self.wbimage:Show()
			end			
		end)
	end
	if self.wbimage then
		self.wbimage:SetScale(.26,.26,.26)
	end
end)

function White_Boneui:SpawnYJFx()
	self.wbimagefx = self.wbimage:AddChild(UIAnimButton("lavaarena_hammer_attack_fx", "yj_spear_elec_shockfx_build"))
	self.wbimagefx:SetScale(0.8)
	self.wbimagefx:SetPosition(0,-90,0)
	self.wbimagefx.animstate:SetLightOverride(1)
	self.wbimagefx.animstate:PlayAnimation("crackle_loop")
	self.wbimagefx.inst:DoTaskInTime(self.wbimagefx.animstate:GetCurrentAnimationLength(),function(inst)
		self.wbimagefx:Kill()
	end)
end

function White_Boneui:SpawnPgFx()
	self.wbimagefx = self.wbimage:AddChild(UIAnimButton("collapse", "structure_collapse_fx"))
	self.wbimagefx:SetScale(0.8)
	self.wbimagefx:SetPosition(0,-100,0)
	self.wbimagefx.animstate:PlayAnimation("collapse_small")
	self.wbimagefx.inst:DoTaskInTime(self.wbimagefx.animstate:GetCurrentAnimationLength(),function(inst)
		self.wbimagefx:Kill()
	end)
end

function White_Boneui:SpawnMaFx()
	self.wbimagefx = self.wbimage:AddChild(UIAnimButton("madameweb_explode", "spider_queen_build"))
	self.wbimagefx:SetScale(0.6)
	self.wbimagefx:SetPosition(0,-160,0)
	self.wbimagefx.animstate:PlayAnimation("anim")
	self.wbimagefx.inst:DoTaskInTime(self.wbimagefx.animstate:GetCurrentAnimationLength(),function(inst)
		self.wbimagefx:Kill()
	end)
end

function White_Boneui:SpawnYAFx(colour)
	if self.wbimagefx then
		self.wbimagefx:Kill()
	end
	self.wbimagefx = self.wbimage:AddChild(UIAnimButton("alterguardian_meteor", "token_commissioner_fx"))
	self.wbimagefx:SetScale(0.8)
	self.wbimagefx:SetPosition(0,-100,0)
	if colour then
		self.wbimagefx.animstate:SetMultColour(unpack(colour))
	end
	self.wbimagefx.animstate:PlayAnimation("meteor_charge")
	self.wbimagefx.animstate:SetBloomEffectHandle("shaders/anim.ksh")
	self.wbimagefx.inst:DoTaskInTime(0.63,function()
		self.wbimagefx:Kill()
		self.wbimagefx = nil
	end)
end

return White_Boneui