

local function oncurrent(self,current)
	if self.inst.madameweb_silkvalue then
		self.inst.madameweb_silkvalue:set(current)
	end
end

local function OnTaskTick(inst, self, period)
    self:DoDec(period)
end

local  madameweb_silkvalue = Class(function(self, inst)
	self.inst = inst
	self.current = 0
	self.max = 100

	self.inst:ListenForEvent("oneat",function(doer,data) --蜘蛛腺体的
		if data and data.food and data.food.components.edible then
			if data.food.components.edible:GetSanity(self.inst) > 0 then
				self:DoDelta(2)
			end
		end
	end)

	self.inst:ListenForEvent("murdered",function(doer,data)
		if data and data.victim and (data.victim:HasTag("spider") or  data.victim:HasTag("flying")) then
			local num = data.victim.components.stackable and data.victim.components.stackable:StackSize() or  1
			self:DoDelta(num*1)
		end
	end)
	self.inst:ListenForEvent("firedamage",function(doer,data)
		self:DoDelta(-1)
	end)

	local period = 1
    self.inst:DoPeriodicTask(period, OnTaskTick, nil, self, period)
end,
nil,
{
	current = oncurrent,
}
)

function madameweb_silkvalue:DoDelta(delta)
	if delta < 0 and self.inst.components.inventory:EquipHasTag("1/2silkvalue") then
		delta = delta * 0.5 --嘤嘤嘤可以有效降低损耗
	end
	local old =  self.current/self.max
    self.current = math.clamp(self.current + delta, 0, self.max)
	local new = self.current/self.max
	if new <= TUNING.HUNGRY_THRESH and old > TUNING.HUNGRY_THRESH then
		self.inst.components.talker:Say(STRINGS.NAMES.MADAMEWEB_SILKVALUELOW)
	end
end

function madameweb_silkvalue:DoDec(dt)
    if  self.inst.components.health:IsDead() or self.inst:HasTag("playerghost") then
        return
    end
	local  delta
	if TheWorld.state.isnight or  --夜晚
		self.inst:HasTag("madameweb_spider") or
		self.inst.components.inventory:EquipHasTag("1/2silkvalue") or  --特殊装备
		self.inst.sg:HasStateTag("sleeping")  then --我在睡觉呢
		delta = 2/48
	else
		delta = 1/48
	end
	if self.inst:HasTag("madameweb_spider") then
		delta =  delta + 0.3 --蜘蛛形态一秒+ 1
	end
	self:DoDelta(delta*dt)
end

madameweb_silkvalue.LongUpdate = madameweb_silkvalue.DoDec

function madameweb_silkvalue:OnSave()
	return {current = self.current}
end

function madameweb_silkvalue:OnLoad(data)
    if data then
		if data.current then
			self.current = data.current
		end
    end
end

return madameweb_silkvalue


