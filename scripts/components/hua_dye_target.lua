local DYE_COLOURS = {
    {1, 1, 1, 1},
    {203/255, 70/255, 70/255, 1},
    {203/255, 124/255, 70/255, 1},
    {220/255, 208/255, 122/255, 1},
    {129/255, 178/255, 97/255, 1},
    {65/255, 160/255, 142/255, 1},
    {8/255, 124/255, 173/255, 1},
    {139/255, 90/255, 168/255, 1},
    {205/255, 85/255, 162/255, 1},
}

local hua_dye_target = Class(function(self, inst)
    self.inst = inst

    inst:AddTag("hua_can_be_dyed")

    self.current_colour_id = 1
    self:ApplyDye()
end)

function hua_dye_target:OnLoad(data)
    if data then
        if data.current_colour_id then
            self.current_colour_id = data.current_colour_id
            self:ApplyDye()
        end
    end
end

function hua_dye_target:OnSave()
    return {
        current_colour_id = self.current_colour_id,
    }
end

function hua_dye_target:ApplyDye()
    self.inst.AnimState:SetMultColour(unpack(DYE_COLOURS[self.current_colour_id]))
end

function hua_dye_target:ChangeDye()
    self.current_colour_id = self.current_colour_id + 1
    if self.current_colour_id > #DYE_COLOURS then
        self.current_colour_id = 1
    end
    self:ApplyDye(self.current_colour_id)
end

return hua_dye_target