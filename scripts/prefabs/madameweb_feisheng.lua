local assets = {
    Asset("ATLAS", "images/inventoryimages/madameweb_feisheng.xml"),
}
local SHADECANOPY_MUST_TAGS = {"shadecanopy"}
local SHADECANOPY_SMALL_MUST_TAGS = {"shadecanopysmall"}

local function canfly(pos,doer)
    if doer and doer.components.rider  and doer.components.rider:IsRiding() then
        return false
    end
    if TheWorld:HasTag("cave") then
        return true
    else
        local canopy = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE, SHADECANOPY_MUST_TAGS)
        local canopy_small = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE_SMALL, SHADECANOPY_SMALL_MUST_TAGS)
        if #canopy > 0 or #canopy_small > 0 then
            return true
        end
    end
end
local function builder_onbuilt(inst, builder)
    local pt = builder:GetPosition()
    if not canfly(pt,builder) then
        builder.components.talker:Say(STRINGS.NAMES.MADAMEWEB_CANTSILKFLY)
    elseif not builder.components.health:IsDead() then
        builder.components.madameweb_silkvalue:DoDelta(-10)
        builder.sg:GoToState("myth_silkfly_up")
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()

    inst:AddTag("CLASSIFIED")

    inst.persists = false

    inst:DoTaskInTime(0, inst.Remove)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.OnBuiltFn = builder_onbuilt

    return inst
end

return Prefab("madameweb_feisheng", fn, assets)