
local MakePlayerCharacter = require "prefabs/player_common"


local assets = {
    Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
}
local prefabs = {}

local BEAVERVISION_COLOURCUBES =
{
    day = resolvefilepath("images/colour_cubes/madameweb_vision_cc.tex"),
    dusk = resolvefilepath("images/colour_cubes/madameweb_vision_cc.tex"),
    night = resolvefilepath("images/colour_cubes/madameweb_vision_cc.tex"),
    full_moon = resolvefilepath("images/colour_cubes/madameweb_vision_cc.tex"),
}

local start_inv = {}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.MADAMEWEB
end

local function FlyActionFilter(inst, action)
    return action == ACTIONS.ATTACK
end

local function GetHopDistance(inst, speed_mult)
	return speed_mult < 0.8 and TUNING.WILSON_HOP_DISTANCE_SHORT
			or speed_mult >= 1.2 and TUNING.WILSON_HOP_DISTANCE_FAR
			or TUNING.WILSON_HOP_DISTANCE
end

local allowstate = {
	idle = true,
	madameweb_spider_idle = true,
	run = true,
	run_start = true,
	run_stop = true,
	madameweb_spider_walk = true,
	madameweb_spider_walk_pre = true,
	madameweb_spider_walk_pst = true,
	hit = true,
    attack = true,
    madameweb_attack = true,
    madameweb_spawnsilk = true,
    myth_sg_pre = true,
    frozen = true,
	madameweb_thaw = true,
	madameweb_frozen = true,
    madameweb_spawnsilk = true,
    myth_silkfly_jump_pre = true,
    myth_silkfly_jump = true,
    myth_silkfly_jump_pst = true,
}

local function stopunderground(inst,data)
	if data and not allowstate[data.statename] then
		inst._beacmespider:set(false)
	end
end

local function Empty()
    return nil
end

local function LeftClickPicker(inst, target)
    if target ~= nil and target ~= inst then
        if inst.replica.combat:CanTarget(target) then
            return (not target:HasTag("player") or inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK))
                and inst.components.playeractionpicker:SortActionList({ ACTIONS.ATTACK }, target, nil)
                or nil
        end
    end
end

local function RightClickPicker(inst, target,pos)
    if target ~= nil and target == inst  then
        return inst.components.playeractionpicker:SortActionList({ACTIONS.MYTHBIANHUAN }, target, nil)
    elseif not target and TheWorld.GroundCreep:OnCreep(pos.x, pos.y, pos.z) and math.floor(inst.madameweb_silkvalue:value()) >= TUNING.MADAMEWEB_FLYDOWN_COST  then
        return inst.components.playeractionpicker:SortActionList({ACTIONS.MYTH_SILKFLY },pos)
    end
    return nil
end

local REGISTERED_FIND_ATTACK_TARGET_TAGS = TheSim:RegisterFindTags({ "_combat" }, { "INLIMBO" })
local function ActionButton(inst, force_target)
    if not inst.components.playercontroller:IsDoingOrWorking() then
        local x, y, z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, inst.components.playercontroller.directwalking and 4 or 8, REGISTERED_FIND_ATTACK_TARGET_TAGS)
        for i, v in ipairs(ents) do
            if v ~= inst and v.entity:IsVisible() and inst.replica.combat:CanTarget(v) then
                return BufferedAction(inst, v, ACTIONS.ATTACK )
            end
        end
    end
end

local SPIDER_TAGS = {"spider",}

local function spider_disable(inst)
    if inst.updatespidertask then
        inst.updatespidertask:Cancel()
        inst.updatespidertask = nil
    end
    inst:RemoveTag("spiderdisguise")
    inst.components.leader:RemoveFollowersByTag("spider", function(follower)
        if follower and follower.components.follower then
            if follower.components.follower:GetLoyaltyPercent() > 0 then
                return false
            else
                return true
            end
        end
    end)
end

local function spider_update(inst)
    if inst and inst.components.leader then
        inst.components.leader:RemoveFollowersByTag("pig")
        local x,y,z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x,y,z, TUNING.SPIDERHAT_RANGE, SPIDER_TAGS)
        for k,v in pairs(ents) do
            if v.components.follower and not v.components.follower.leader and not inst.components.leader:IsFollower(v) and inst.components.leader.numfollowers < 10 then
                inst.components.leader:AddFollower(v)
            end
        end
    end
end

local function spider_enable(inst)
    inst.components.leader:RemoveFollowersByTag("pig")
    inst:AddTag("spiderdisguise")
    inst.updatespidertask = inst:DoPeriodicTask(0.5, spider_update, 1)
end

local notdattack = {'wall',"companion","abigail","madameweb_baby"}
if not TheNet:GetPVPEnabled() then
    table.insert(notdattack,"player")
end
local function areahitcheck(target,inst)
    for _,v in ipairs(notdattack) do
        if target:HasTag(v) then
            return false
        end
    end
    if target.components.follower ~= nil and target.components.follower.leader == inst then
        return false
    end
    return true
end


local function IsValidVictim(victim)
    return victim ~= nil
        and not ((victim:HasTag("prey") and not victim:HasTag("hostile")) or
                victim:HasTag("veggie") or
                victim:HasTag("structure") or
                victim:HasTag("wall") or
                victim:HasTag("balloon") or
                victim:HasTag("groundspike") or
                victim:HasTag("smashable") or
                victim:HasTag("companion"))
        and victim.components.health ~= nil
        and victim.components.combat ~= nil
end

local function onkilled(inst, data)
    local victim = data.victim
    if IsValidVictim(victim) then
        inst.components.sanity:DoDelta(victim:HasTag("epic") and 10 or 5)
    end
end

local function SetVision(inst, enable)
    local enable = inst._beacmespider:value()
	if enable then
        --阴影
		inst.DynamicShadow:SetSize(7, 3)
		inst:AddTag("madameweb_spider")
        inst:AddTag("spiderqueen")
        --设置贴图
        inst.AnimState:AddOverrideBuild("madameweb_spider1")

		if inst.components.inventory then
			inst.components.inventory:Close()
		end
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller.actionbuttonoverride = ActionButton
        end
        if inst.components.playeractionpicker ~= nil then
            inst.components.playeractionpicker.leftclickoverride = LeftClickPicker
            inst.components.playeractionpicker.rightclickoverride = RightClickPicker
            inst.components.playeractionpicker.pointspecialactionsfn = nil
        end

		if inst.components.catcher ~= nil then
			inst.components.catcher:SetEnabled(false)
		end
		--if inst.components.playeractionpicker ~= nil then
			--inst.components.playeractionpicker:PushActionFilter(FlyActionFilter, 668)
		--end	
		if inst.components.talker ~= nil then
			inst.components.talker:IgnoreAll("madameweb_spider")
		end
		if inst.components.firebug ~= nil then
			inst.components.firebug:Disable()
		end	
        if inst.components.hunger ~= nil then
            inst.components.hunger.burnratemodifiers:SetModifier("madameweb_spider", 2)
        end
        if inst.components.locomotor then
            inst.components.locomotor.walkspeed = 4
            inst.components.locomotor.runspeed = 4
            inst.components.locomotor:SetAllowPlatformHopping(false)
        end

        inst.components.playervision:ForceNightVision(true)
        inst.components.playervision:SetCustomCCTable(BEAVERVISION_COLOURCUBES)

		if  TheWorld.ismastersim then
            inst.components.combat.damagemultiplier = 1
            inst.components.combat:SetDefaultDamage(100)
            inst.components.combat:SetAreaDamage(4, 1,areahitcheck)
            inst.components.combat:SetRange(6)
            inst.components.combat:SetAttackPeriod(1.2)
            inst.components.health:SetAbsorptionAmount(0.5)
            Myth_MakePoisonable(inst)
			if inst.sg and not inst.sg:HasStateTag("madameweb_spider") and not inst.sg:HasStateTag("dead") then
				inst.sg:GoToState("madameweb_spider_idle")
			end
            spider_enable(inst)
			local fx = SpawnPrefab("madameweb_explode")
			fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst:ListenForEvent("newstate", stopunderground)
            inst:ListenForEvent("killed", onkilled)
		end
    else
		inst.DynamicShadow:SetSize(1.3, .6)
		inst.AnimState:ClearOverrideSymbol("madameweb_spider1")
        --inst.AnimState:ClearOverrideSymbol("wormmovefx")

		if inst.components.inventory then
			inst.components.inventory:Open()
		end
		inst:RemoveTag("madameweb_spider")
        inst:RemoveTag("spiderqueen")

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller.actionbuttonoverride = nil
        end
        if inst.components.playeractionpicker ~= nil then
            inst.components.playeractionpicker.leftclickoverride = nil
            inst.components.playeractionpicker.rightclickoverride = nil
            inst.components.playeractionpicker.pointspecialactionsfn = nil
        end
		if inst.components.catcher ~= nil then
			inst.components.catcher:SetEnabled(true)
		end
		--if inst.components.playeractionpicker ~= nil then
		--	inst.components.playeractionpicker:PopActionFilter(FlyActionFilter)
		--end
		if inst.components.firebug ~= nil then
			inst.components.firebug:Enable()
		end
        if inst.components.locomotor then
            inst.components.locomotor.walkspeed = TUNING.WILSON_WALK_SPEED
            inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED
            inst.components.locomotor:SetAllowPlatformHopping(true)
        end
		if inst.components.talker ~= nil then
			inst.components.talker:StopIgnoringAll("madameweb_spider")
		end
        if inst.components.hunger ~= nil then
            inst.components.hunger.burnratemodifiers:RemoveModifier("madameweb_spider")
        end	
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)

		if  TheWorld.ismastersim then
            inst.components.combat.damagemultiplier = 0.75
            inst.components.combat:SetDefaultDamage(TUNING.UNARMED_DAMAGE)
            inst.components.combat:SetAttackPeriod(TUNING.WILSON_ATTACK_PERIOD)
            inst.components.combat:SetAreaDamage(nil, nil)
            inst.components.combat:SetRange(TUNING.DEFAULT_ATTACK_RANGE)
            inst.components.health:SetAbsorptionAmount(0)
            Myth_RemovePoisonable(inst)
			if inst.sg and inst.sg:HasStateTag("madameweb_spider") and not inst.sg:HasStateTag("dead") then
				inst.sg:GoToState("pigsy_wakeup")
			end
            spider_disable(inst)
			local fx = SpawnPrefab("madameweb_explode")
			fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst:RemoveEventCallback("newstate", stopunderground)
            inst:RemoveEventCallback("killed", onkilled)
		end
    end
end

local function onusebh(inst)
	inst.sg:GoToState("madameweb_spawnsilk")
	return true
end

local function onhungerchange(inst, data)
    if inst:HasTag("playerghost") or
        inst.components.health:IsDead() then
        return
    end
	if inst.components.hunger.current <= 0 then
		if inst._beacmespider:value() then
			inst._beacmespider:set(false)
		end
	end
end

local function onbecamehuman(inst)
    inst:ListenForEvent("hungerdelta", onhungerchange)
	--inst.components.locomotor:SetExternalSpeedMultiplier(inst, "madameweb_speed", 1)
    onhungerchange(inst)
end

local function onbecameghost(inst)
    inst:RemoveEventCallback("hungerdelta", onhungerchange)
   --inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "madameweb_speed")
end

local function onload(inst,data)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
	if data then 
		if data.spiderable then
			inst.canbecomespider = true
			inst._canbecomespider:set(true)
		end
	end
end

local function onsave(inst, data)
	data.spiderable = inst.canbecomespider
end

local FIRE_TAGS = { "fire" }
local function sanityfn(inst)--, dt)
    local delta = inst.components.burnable:IsBurning() and -TUNING.SANITYAURA_LARGE or 0
    local x, y, z = inst.Transform:GetWorldPosition()
    local max_rad = 10
    local ents = TheSim:FindEntities(x, y, z, max_rad, FIRE_TAGS)
    for i, v in ipairs(ents) do
        if v.components.burnable ~= nil and v.components.burnable:IsBurning() then
            local rad = v.components.burnable:GetLargestLightRadius() or 1
            local sz = TUNING.SANITYAURA_TINY * math.min(max_rad, rad) / max_rad
            local distsq = inst:GetDistanceSqToInst(v) - 9
            delta = delta - sz / math.max(1, distsq)
        end
    end
    if inst:HasTag("madameweb_spider") then 
        delta =  delta - 3/10
    end
    return delta
end

local function CLIENT_Webber_HostileTest(inst, target)
    return (target:HasTag("hostile") or target:HasTag("pig") or target:HasTag("catcoon"))
        and (not target:HasTag("spiderden"))
        and (not target:HasTag("madameweb_baby"))
        and (not target:HasTag("madameweb_spidenden"))
        and (not target:HasTag("spider") or target:HasTag("spiderqueen"))
end

local function flyRightClickPicker(inst, target)
    local x,y,z = inst.Transform:GetWorldPosition()
    if TheWorld.Map:IsValidTileAtPoint(x, y, z) then
        return inst.components.playeractionpicker:SortActionList({ ACTIONS.MYTH_SILKFLY }, target, nil)
    end
    return nil
end

local function onflying(inst)
    local enable = inst._silkfly:value()
	if enable then
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller.actionbuttonoverride = Empty
		end		 
        if inst.components.playeractionpicker then
            inst.components.playeractionpicker.leftclickoverride = Empty
            inst.components.playeractionpicker.rightclickoverride = flyRightClickPicker
        end
		if inst.components.locomotor then
			inst.components.locomotor:SetSlowMultiplier(0.6)
			inst.components.locomotor.pathcaps = { player = true, ignorecreep = true ,allowocean = true}
			inst.components.locomotor.fasteronroad = false
			inst.components.locomotor:SetTriggersCreep(false)
			inst.components.locomotor:SetAllowPlatformHopping(false)
		end
        inst.components.playervision:ForceNightVision(true)
        inst.components.playervision:SetCustomCCTable(BEAVERVISION_COLOURCUBES)
    else
		if inst.components.locomotor then
			inst.components.locomotor:SetSlowMultiplier(0.6)
			inst.components.locomotor.pathcaps = { player = true, ignorecreep = true }
			inst.components.locomotor.fasteronroad = true
			inst.components.locomotor:SetTriggersCreep(not inst:HasTag("spiderwhisperer"))
			inst.components.locomotor:SetAllowPlatformHopping(true)
			inst.components.locomotor.hop_distance_fn = GetHopDistance
		end
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller.actionbuttonoverride = nil
		end		
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)
        if inst.components.playeractionpicker then
            inst.components.playeractionpicker.rightclickoverride = nil
            inst.components.playeractionpicker.leftclickoverride = nil
        end
    end
end

local function tumblefn(inst)
    if inst.components.mk_silkflyer and inst.components.mk_silkflyer.flying and not inst.components.health:IsDead()
        and not inst.sg:HasStateTag("silkfly_down") then
		inst.sg:GoToState("mythtumble")
	end
end

local function onlearn(inst)
	if not inst.canbecomespider then
		inst.canbecomespider = true
		inst._canbecomespider:set(true)
	end
end
--ThePlayer._canbecomespider:set(true) ThePlayer.canbecomespider = true
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "madameweb.tex" )
	inst.soundsname = "sfx"
	inst.talker_path_override = 'Pipa/'
    inst:AddTag("madameweb")
    inst:AddTag("playermonster")
    inst:AddTag("monster")
    inst:AddTag("can_feiwen")
    --inst:AddTag("spiderwhisperer")
    inst:DoTaskInTime(0.1,function()
        inst:AddTag("spiderwhisperer")
    end)

    inst:AddTag(UPGRADETYPES.SPIDER.."_upgradeuser")

    inst.BIANHUAN_STR = "MA_BIANHUAN"

    inst.AnimState:AddOverrideBuild("madameweb_kissbuild")

    inst:ListenForEvent("silkflydirty", onflying)

    inst.madameweb_silkvalue = net_byte(inst.GUID,"madameweb_silkvalue", "madameweb_silkvalue_dirty")
    inst._canbecomespider = net_bool(inst.GUID, "madameweb._canbecomespider", "canbecomespiderdirty")
    inst._beacmespider = net_bool(inst.GUID, "madameweb._beacmespider", "beacmespiderdirty")
	inst:ListenForEvent("beacmespiderdirty", SetVision)
    --ThePlayer._beacmespider:set(false)
    inst._silkfly = net_bool(inst.GUID, "madameweb.silkfly", "silkflydirty")
    inst:ListenForEvent("silkflydirty", onflying)

	inst:DoTaskInTime(0,function()
		if inst.replica.builder then
 			local old_HasCharacterIngredient = inst.replica.builder.HasCharacterIngredient	
			inst.replica.builder.HasCharacterIngredient = function(self,ingredient,...)
				if ingredient.type == CHARACTER_INGREDIENT.MYTH_MADAMEWEB then
					local current = math.floor(inst.madameweb_silkvalue:value())
					return current >= ingredient.amount, current
				end
				return old_HasCharacterIngredient(self,ingredient,...)
			end
		end
	end)

    inst.HostileTest = CLIENT_Webber_HostileTest
end

local function ReleaseSpiderFollowers(inst)
    for follower, v in pairs(inst.components.leader.followers) do
        if follower:HasTag("spider") and follower.defensive then
            follower.defensive = false
            follower.no_targeting = false
        end
    end
end

local function OnAttacked(inst,data)
    ReleaseSpiderFollowers(inst)
    inst.components.combat:ShareTarget(data.attacker, 12, function(dude) return dude:HasTag("madameweb_baby") and not dude.components.health:IsDead() end, 10)
end
local function DoAttack(inst,data)
    if data and data.target then
        inst.components.combat:ShareTarget(data.target, 12, function(dude) return dude:HasTag("madameweb_baby") and not dude.components.health:IsDead() end, 10)
    end
end
local function customidleanimfn(inst)
    return not (inst.components.mk_silkflyer.flying and inst:HasTag("bainshen")) and "myth_feiwen_idle" or nil
end

local function ActionString(inst, action)
    return (action.action == ACTIONS.MURDER and STRINGS.MURDERED_BY_MADAMEWEB)  or nil
end
local master_postinit = function(inst)

    inst.ActionStringOverride = ActionString

    inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

    inst.myth_poisonblocker = true
    inst.canbecomespider = false
    inst.Myth_Learn_Skill = onlearn

    inst.customidlestate = "myth_feiwen_idle"

	inst:AddComponent("mk_bh")
	inst.components.mk_bh.onusefn = onusebh

    if inst.components.eater ~= nil then
        inst.components.eater:SetStrongStomach(true)
        inst.components.eater:SetCanEatRawMeat(true)
    end

	inst.components.foodaffinity:AddPrefabAffinity("bird_egg", TUNING.AFFINITY_15_CALORIES_HUGE)
    inst.components.foodaffinity:AddPrefabAffinity("bird_egg_cooked", TUNING.AFFINITY_15_CALORIES_HUGE)

	inst.components.health:SetMaxHealth(TUNING.MADAMEWEB_HEALTH)
	inst.components.hunger:SetMax(TUNING.MADAMEWEB_HUNGER)
	inst.components.sanity:SetMax(TUNING.MADAMEWEB_SANITY)
    inst.components.sanity.night_drain_mult = 0
    --inst.components.sanity.neg_aura_mult = 0
    inst.components.sanity.custom_rate_fn = sanityfn

    inst.components.combat.damagemultiplier = 0.75
	
	inst.components.hunger.hungerrate = 1 * TUNING.WILSON_HUNGER_RATE

    local old_DoDelta =  inst.components.health.DoDelta
    inst.components.health.DoDelta = function(self,amount,...)
        if amount < 0 and self.inst:HasTag("madameweb_spider") and self.inst.components.madameweb_silkvalue.current > 1 then
            self.inst.components.madameweb_silkvalue:DoDelta(0.8*amount/10)
            amount = amount * 0.2 --蛛丝可以减伤80*
        end
        return old_DoDelta(self,amount,...)
    end

    local old_grogginess =  inst.components.grogginess.AddGrogginess
    inst.components.grogginess.AddGrogginess = function(self,...)
        if self.inst.components.mk_silkflyer and self.inst.components.mk_silkflyer.flying then
            if self.inst.TumbleFn then
                self.inst:TumbleFn()
            end
            return
        end
        return old_grogginess(self,...)
    end

    local old_cold = inst.components.freezable.AddColdness
    inst.components.freezable.AddColdness = function(self,...)
        if self.inst.components.mk_silkflyer and self.inst.components.mk_silkflyer.flying then
            return
        end
        return old_cold(self,...)
    end

    local old_delta =  inst.components.hunger.DoDelta
    inst.components.hunger.DoDelta = function(self,delta, overtime, ignore_invincible)
        if self.inst.components.mk_silkflyer and self.inst.components.mk_silkflyer.flying then
            ignore_invincible = true
        end
        return old_delta(self,delta, overtime, ignore_invincible)
    end

    --inst.components.locomotor.SetTriggersCreep = function(self,...)
    --    self.triggerscreep = false
    --end
    inst.components.locomotor:SetTriggersCreep(false)
    inst.components.locomotor.fasteroncreep = true

    inst:AddComponent("madameweb_silkvalue")
    inst:AddComponent("mk_silkflyer")
    local old_HasCharacterIngredient = inst.components.builder.HasCharacterIngredient
	inst.components.builder.HasCharacterIngredient = function(self, ingredient, ...)
		if ingredient.type == CHARACTER_INGREDIENT.MYTH_MADAMEWEB then
			local madameweb_silkvalue = self.inst.components.madameweb_silkvalue
			if madameweb_silkvalue ~= nil then
				local current = math.ceil(self.inst.components.madameweb_silkvalue.current)
				return current >= ingredient.amount, current
			end
		end
		return old_HasCharacterIngredient(self, ingredient, ...)
	end
    
	local old_RemoveIngredients = inst.components.builder.RemoveIngredients
	inst.components.builder.RemoveIngredients = function(self, ingredients, recname)
		old_RemoveIngredients(self, ingredients, recname)
		local recipe = AllRecipes[recname]
		if recipe then
			for k, v in pairs(recipe.character_ingredients) do
				if v.type == CHARACTER_INGREDIENT.MYTH_MADAMEWEB then
                    if recname == "madameweb_feisheng" then
                        return
                    end
                    if self.inst.components.madameweb_silkvalue then
                        self.inst.components.madameweb_silkvalue:DoDelta(-v.amount)
                    end
				end
			end
		end
	end

    inst:ListenForEvent("attacked",      OnAttacked)
    inst:ListenForEvent("onattackother", ReleaseSpiderFollowers)
    inst:ListenForEvent("death",         ReleaseSpiderFollowers)
    inst:ListenForEvent("onremove",      ReleaseSpiderFollowers)
    inst:ListenForEvent("onhitother",      DoAttack)
    
    inst.TumbleFn = tumblefn
	inst.OnLoad = onload
    inst.OnNewSpawn = onload
    inst.OnSave = onsave
end

return MakePlayerCharacter("madameweb", prefabs, assets, common_postinit, master_postinit, start_inv)
