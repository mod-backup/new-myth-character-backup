local Badge = require "widgets/badge"
local UIAnim = require "widgets/uianim"

local yama_gathersoul_Badge = Class(Badge, function(self, owner, art)
    Badge._ctor(self, nil, owner,  { 254 / 255, 253 / 255, 237 / 255, 1 }, "yama_status_soul")

	self.inst:ListenForEvent("yama_status_soul_dirty", function(inst,data)
		self:SetPercent(self.owner.yama_status_soul:value()/100, 100)
	end,self.owner)
    self:SetPercent(self.owner.yama_status_soul:value()/100, 100)
end)

function yama_gathersoul_Badge:SetPercent(val, max)
    Badge.SetPercent(self, val, max)
end

return yama_gathersoul_Badge
