local timeMap = { --buff持续时间
    workup = 90,
    insomnia = 480,
    fireimmune = 90,
    iceimmune = 90,
    iceshield = 90,

    nocure = 30,
    weakattack = 30,
    weakdefense = 30,
    nolove = 30,
    sweetdream = 30,
}
local countMap = { --buff刷新时间
    workup = 4,
    insomnia = 4,
    fireimmune = 4,
    iceimmune = 4,
    iceshield = 4,

    nocure = 4,
    weakattack = 4,
    weakdefense = 4,
    nolove = 4,
    sweetdream = 4,
}
local songMap = { --音乐配置
    workup = {
        name = "Jadehare/sfx/workup",
        volume = nil, --声音大小：0<x<=1
    },
    insomnia = {
        name = "Jadehare/sfx/insomnia",
        volume = nil,
    },
    fireimmune = {
        name = "Jadehare/sfx/fireimmune",
        volume = nil,
    },
    iceimmune = {
        name = "Jadehare/sfx/iceimmune",
        volume = nil,
    },
    iceshield = {
        name = "Jadehare/sfx/iceshield",
        volume = nil,
    },

    nocure = {
        name = "Jadehare/sfx/nocure",
        volume = nil,
    },
    weakattack = {
        name = "Jadehare/sfx/weakattack",
        volume = nil,
    },
    weakdefense = {
        name = "Jadehare/sfx/weakdefense",
        volume = nil,
    },
    nolove = {
        name = "Jadehare/sfx/nolove",
        volume = nil,
    },
    sweetdream = {
        name = "Jadehare/sfx/sweetdream",
        volume = nil,
    },
}
local colorMap = {
    workup = { --金色调
        base = { r = 255/255, g = 225/255, b = 115/255 },
        light = { r = 255/255, g = 155/255, b = 0/255 },
    },
    insomnia = { --绿色调
        base = { r = 180/255, g = 255/255, b = 159/255 },
        light = { r = 58/255, g = 201/255, b = 104/255 },
    },
    fireimmune = { --红色调
        base = { r = 255/255, g = 124/255, b = 103/255 },
        light = { r = 216/255, g = 66/255, b = 34/255 },
    },
    iceimmune = { --天蓝色调
        base = { r = 118/255, g = 255/255, b = 240/255 },
        light = { r = 22/255, g = 225/255, b = 243/255 },
    },
    iceshield = { --白色调
        base = { r = 203/255, g = 255/255, b = 227/255 },
        light = { r = 203/255, g = 255/255, b = 227/255 },
    },

    nocure = { --粉紫色调
        base = { r = 164/255, g = 51/255, b = 210/255 },
    },
    weakattack = { --蓝紫色调
        base = { r = 100/255, g = 51/255, b = 210/255 },
    },
    weakdefense = { --橙色调
        base = { r = 234/255, g = 129/255, b = 24/255 },
    },
    nolove = { --黑色调
        base = { r = 35/255, g = 35/255, b = 20/255 },
    },
    sweetdream = { --深蓝色调
        base = { r = 51/255, g = 77/255, b = 210/255 },
    },

}

-----

local function IsAlive(inst)
	return inst.components.health ~= nil and not inst.components.health:IsDead() and not inst:HasTag("playerghost")
end

local function GetRandomPos(x, y, z, radius, angle)
    local rad = radius or math.random() * 3
    local ang = angle or math.random() * 2 * PI
    return x + rad * math.cos(ang), y, z - rad * math.sin(ang)
end

local function SpawnFx_other(other, songkey)
    local taskname = "task_m_"..songkey
    local timename = "time_m_"..songkey

    if other[taskname] ~= nil then
        other[taskname]:Cancel()
    end
    other[timename] = GetTime() + timeMap[songkey] --用的buff机制，所以这里单独给玩家计时
    other[taskname] = other:DoPeriodicTask(0.5,
        function()
            if not IsAlive(other) or GetTime() >= other[timename] then
                other[taskname]:Cancel()
                other[taskname] = nil
                other[timename] = nil
                return
            end

            local x, y, z = other.Transform:GetWorldPosition()
            local times = math.random(1, 2)
            for i = 1, times, 1 do
                local x2, y2, z2 = GetRandomPos(x, y, z, 0.5+math.random(), nil)
                local fx = SpawnPrefab("song_m_"..songkey.."_note_fx")
                if fx then
                    fx.Transform:SetPosition(x2, y2, z2)
                end
            end
        end,
    0)
end

local function SpawnFx_doer(doer, songkey)
    if doer.task_m_playpipafx ~= nil then
        doer.task_m_playpipafx:Cancel()
    end
    doer.task_m_playpipafx = doer:DoPeriodicTask(0.5,
        function()
            local x, y, z = doer.Transform:GetWorldPosition()
            local times = math.random(1, 2)
            for i = 1, times, 1 do
                local x2, y2, z2 = GetRandomPos(x, y, z, 0.5+math.random(), nil)
                local fx = SpawnPrefab("song_m_"..songkey.."_note_fx")
                if fx then
                    fx.Transform:SetPosition(x2, y2, z2)
                end
            end

            times = math.random(3, 5)
            local rad = 0.5+math.random()*4
            for i = 1, times, 1 do
                local x2, y2, z2 = GetRandomPos(x, y, z, rad+math.random()*0.5, nil)
                local fx = SpawnPrefab("song_m_"..songkey.."_wave_fx")
                if fx then
                    fx.Transform:SetPosition(x2, y2, z2)
                end
            end
        end,
    0)
end

local function SpawnFx_enemy(v, songkey, endfn)
    local taskname = "task_m_"..songkey
    local timename = "time_m_"..songkey

    if v[taskname] ~= nil then
        v[taskname]:Cancel()
    end
    v[timename] = GetTime() + timeMap[songkey] --用的buff机制，所以这里单独计时
    v[taskname] = v:DoPeriodicTask(0.5,
        function()
            if v.components.health == nil or v.components.health:IsDead() or GetTime() >= v[timename] then
                v[taskname]:Cancel()
                v[taskname] = nil
                v[timename] = nil

                if endfn ~= nil then
                    endfn(v)
                end
                return
            end

            local x, y, z = v.Transform:GetWorldPosition()
            local times = math.random(1, 2)
            for i = 1, times, 1 do
                local x2, y2, z2 = GetRandomPos(x, y, z, 0.5+math.random(), nil)
                local fx = SpawnPrefab("song_m_"..songkey.."_note_fx")
                if fx then
                    fx.Transform:SetPosition(x2, y2, z2)
                end
            end
        end,
    0)
end

-----

local function CostHunger(inst, value)
    if inst.components.hunger ~= nil and not inst.components.hunger:IsStarving() then
        --第二个参数能使数值变化时不播放音效
        inst.components.hunger:DoDelta(-value, true)
        return true
    end
    return false
end

local function CostSanity(inst, value)
    if inst.components.sanity ~= nil and inst.components.sanity.current > 0 then
        --第二个参数能使数值变化时不播放音效
        inst.components.sanity:DoDelta(-value, true)
        return true
    end
    return false
end

local function CheckPlayWithHunger(inst, value)
    if not IsAlive(inst) then
        inst:PushEvent("playenough")
        return true
    end
    if not CostHunger(inst, value) then
        if inst.components.talker ~= nil then
            inst.components.talker:Say(GetString(inst, "DESCRIBE", { "GUITAR_JADEHARE", "NOCOST" }))
        end
        inst:PushEvent("playenough")
        return true
    end
    return false
end

local function CheckPlayWithSanity(inst, value)
    if not IsAlive(inst) then
        inst:PushEvent("playenough")
        return true
    end
    if not CostSanity(inst, value) then
        if inst.components.talker ~= nil then
            inst.components.talker:Say(GetString(inst, "DESCRIBE", { "GUITAR_JADEHARE", "NOCOST" }))
        end
        inst:PushEvent("playenough")
        return true
    end
    return false
end

-----

--从singinginspiration.lua中复制
local function HasFriendlyLeader(target, singer, PVP_enabled)
    local target_leader = (target.components.follower ~= nil) and target.components.follower.leader or nil

    if target_leader and target_leader.components.inventoryitem then
        target_leader = target_leader.components.inventoryitem:GetGrandOwner()
        -- Don't attack followers if their follow object has no owner, unless its pvp, then there are no rules!
        if target_leader == nil then
            return not PVP_enabled
        end
    end

    return  (target_leader ~= nil and (target_leader == singer or (not PVP_enabled and target_leader:HasTag("player"))))
			or (not PVP_enabled and target.components.domesticatable and target.components.domesticatable:IsDomesticated())
			or (not PVP_enabled and target.components.saltlicker and target.components.saltlicker.salted)
end

local function CanDebuff(doer, v, x, y, z)
    if v.components.farmplanttendable ~= nil then
        if v:GetDistanceSqToPoint(x, y, z) < 64 then --8范围内
            v.components.farmplanttendable:TendTo(doer)
        end
        return false
    end

    if
        v.components.health ~= nil and not v.components.health:IsDead()
        and v.components.combat ~= nil
        and (
            doer.components.combat:TargetIs(v) --弹琴者仇恨的
            or ( --仇恨弹琴者或其他玩家的
                v.components.combat.target ~= nil and
                (v.components.combat:TargetIs(doer) or v.components.combat.target:HasTag("player"))
            )
            or (
                (v:HasTag("hostile") or v:HasTag("monster")) --敌对生物
                and not HasFriendlyLeader(v, doer, false) --不是自己的跟随者
            )
        )
    then
        return true
    end

    return false
end

local function TendCrops(x, y, z, doer, radius)
    local ents = TheSim:FindEntities(
        x, y, z, radius, { "tendable_farmplant" },
        { "FX", "DECOR", "INLIMBO" }, nil
    )
    for i, v in ipairs(ents) do
        if v.components.farmplanttendable ~= nil then
            v.components.farmplanttendable:TendTo(doer)
        end
    end
end

local function OnTouchCommon(inst, player, songkey)
    if
        player.components.debuffable ~= nil and player.components.debuffable:IsEnabled() and IsAlive(player)
	then
        player["time_m_"..songkey] = { replace_min = timeMap[songkey] }
        player.components.debuffable:AddDebuff("buff_m_"..songkey, "buff_m_"..songkey)
        SpawnFx_other(player, songkey)
    end
end

local function CreateNoteCommon(doer, color, touchfn)
    local x, y, z = doer.Transform:GetWorldPosition()
    TendCrops(x, y, z, doer, 8)

    local x2, y2, z2 = GetRandomPos(x, y, z, 2+math.random()*2, nil)
    local note = SpawnPrefab("guitar_jadehare_note")
    if note ~= nil then
        note.touchfxcolor = color.base
        note.OnMusicTouch = touchfn
        note.AnimState:SetMultColour(color.base.r, color.base.g, color.base.b, 1)
        note.Light:SetColour(color.light.r, color.light.g, color.light.b)
		note.Light:Enable(true)
        note.Transform:SetPosition(x2, y2, z2)
    end
end

local function PlayEndCommon(doer, guitar)
    if doer.task_m_playpipa ~= nil then
        doer.task_m_playpipa:Cancel()
        doer.task_m_playpipa = nil
    end
    if doer.task_m_playpipafx ~= nil then
        doer.task_m_playpipafx:Cancel()
        doer.task_m_playpipafx = nil
    end
    doer.SoundEmitter:KillSound("song_m")
end

-----

local function OnPlayDo(doer, guitar, songkey, costdata, fn, fxfn)
    if costdata.type == 1 then
        if CheckPlayWithHunger(doer, costdata.value_pre) then
            return
        end
    elseif costdata.type == 2 then
        if CheckPlayWithSanity(doer, costdata.value_pre) then
            return
        end
    end

    doer.AnimState:PlayAnimation("soothingplay_loop", true)
    if songMap[songkey].name ~= nil then
        doer.SoundEmitter:PlaySound(songMap[songkey].name, "song_m")
        if songMap[songkey].volume ~= nil then
            doer.SoundEmitter:SetVolume("song_m", songMap[songkey].volume)
        end
    end

    if doer.task_m_playpipa ~= nil then
        doer.task_m_playpipa:Cancel()
    end
    doer.task_m_playpipa = doer:DoPeriodicTask(countMap[songkey],
        function()
            if costdata.type == 1 then
                if CheckPlayWithHunger(doer, costdata.value_do) then
                    PlayEndCommon(doer, guitar)
                    return
                end
            elseif costdata.type == 2 then
                if CheckPlayWithSanity(doer, costdata.value_do) then
                    PlayEndCommon(doer, guitar)
                    return
                end
            end

            fn(doer, guitar)
        end,
    2)

    if fxfn ~= nil then
        fxfn(doer, guitar)
    end
end

-----

local songs = {
    { --《田中乐》
        name = "song_m_workup",
        fn_start = nil,
        cost_hunger  = true,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "workup",
                { type = 1, value_pre = 5, value_do = 4 },
                function(doer, guitar)
                    CreateNoteCommon(doer, colorMap.workup, function(inst, player)
                        OnTouchCommon(inst, player, "workup")
                    end)
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "workup")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.workup.base,
            wave = colorMap.workup.light,
        },
    },
    { --《春光曲》
        name = "song_m_insomnia",
        fn_start = nil,
        cost_hunger  = true,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "insomnia",
                { type = 1, value_pre = 5, value_do = 4 },
                function(doer, guitar)
                    CreateNoteCommon(doer, colorMap.insomnia, function(inst, player)
                        OnTouchCommon(inst, player, "insomnia")
                    end)
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "insomnia")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.insomnia.base,
            wave = colorMap.insomnia.light,
        },
    },
    { --《浴火奏》
        name = "song_m_fireimmune",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "fireimmune",
                { type = 2, value_pre = 5, value_do = 4 },
                function(doer, guitar)
                    CreateNoteCommon(doer, colorMap.fireimmune, function(inst, player)
                        OnTouchCommon(inst, player, "fireimmune")
                    end)
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "fireimmune")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.fireimmune.base,
            wave = colorMap.fireimmune.light,
        },
    },
    { --《寒风调》
        name = "song_m_iceimmune",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "iceimmune",
                { type = 2, value_pre = 5, value_do = 4 },
                function(doer, guitar)
                    CreateNoteCommon(doer, colorMap.iceimmune, function(inst, player)
                        OnTouchCommon(inst, player, "iceimmune")
                    end)
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "iceimmune")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.iceimmune.base,
            wave = colorMap.iceimmune.light,
        },
    },
    { --《梦飞霜》
        name = "song_m_iceshield",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "iceshield",
                { type = 2, value_pre = 5, value_do = 4 },
                function(doer, guitar)
                    CreateNoteCommon(doer, colorMap.iceshield, function(inst, player)
                        OnTouchCommon(inst, player, "iceshield")
                    end)
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "iceshield")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.iceshield.base,
            wave = colorMap.iceshield.light,
        },
    },

    { --《怨缠身》
        name = "song_m_nocure",
        fn_start = nil,
        cost_hunger  = true,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "nocure",
                { type = 1, value_pre = 5, value_do = 3 },
                function(doer, guitar)
                    local x, y, z = doer.Transform:GetWorldPosition()
                    local ents = TheSim:FindEntities(
                        x, y, z, 25, nil,
                        { "player", "playerghost", "FX", "DECOR", "INLIMBO" }, { "tendable_farmplant", "_health" }
                    )
                    for i, v in ipairs(ents) do
                        if v ~= doer and v:IsValid() and CanDebuff(doer, v, x, y, z) then
                            if v.buff_m_nocure == nil then
                                --改造DoDelta()，为了优化，不想用全局组件改造，这里仅局部改造
                                local DoDelta_old = v.components.health.DoDelta
                                v.components.health.DoDelta = function(self, amount, ...)
                                    if amount > 0 and v.buff_m_nocure then
                                        return 0
                                    end
                                    return DoDelta_old(self, amount, ...)
                                end
                            end
                            v.buff_m_nocure = true
                            SpawnFx_enemy(v, "nocure", function(v)  --特效与buff结束控制
                                v.buff_m_nocure = false
                            end)
                        end
                    end
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "nocure")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.nocure.base,
            wave = colorMap.nocure.base,
        },
    },
    { --《春风化雨》
        name = "song_m_weakattack",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "weakattack",
                { type = 2, value_pre = 5, value_do = 3 },
                function(doer, guitar)
                    local x, y, z = doer.Transform:GetWorldPosition()
                    local ents = TheSim:FindEntities(
                        x, y, z, 25, nil,
                        { "player", "playerghost", "FX", "DECOR", "INLIMBO" }, { "tendable_farmplant", "_health" }
                    )
                    for i, v in ipairs(ents) do
                        if v ~= doer and v:IsValid() and CanDebuff(doer, v, x, y, z) then
                            if v.task_m_weakattack == nil then
                                v.components.combat.externaldamagemultipliers:SetModifier("song_m_weakattack", 0.75)
                            end
                            SpawnFx_enemy(v, "weakattack", function(v)  --特效与buff结束控制
                                if v.components.combat ~= nil then
                                    v.components.combat.externaldamagemultipliers:RemoveModifier("song_m_weakattack")
                                end
                            end)
                        end
                    end
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "weakattack")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.weakattack.base,
            wave = colorMap.weakattack.base,
        },
    },
    { --《霸王卸甲》
        name = "song_m_weakdefense",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "weakdefense",
                { type = 2, value_pre = 5, value_do = 3 },
                function(doer, guitar)
                    local x, y, z = doer.Transform:GetWorldPosition()
                    local ents = TheSim:FindEntities(
                        x, y, z, 25, nil,
                        { "player", "playerghost", "FX", "DECOR", "INLIMBO" }, { "tendable_farmplant", "_health" }
                    )
                    for i, v in ipairs(ents) do
                        if v ~= doer and v:IsValid() and CanDebuff(doer, v, x, y, z) then
                            if v.buff_m_weakdefense == nil then --说明没改过health:SetAbsorptionAmount(amount)
                                local SetAbsorptionAmount_old = v.components.health.SetAbsorptionAmount
                                v.components.health.SetAbsorptionAmount = function(self, amount, ...)
                                    if amount == nil or v.buff_m_weakdefense == nil or v.buff_m_weakdefense < 0 then
                                        SetAbsorptionAmount_old(self, amount, ...)
                                        return
                                    end

                                    v.buff_m_weakdefense = amount --记下尝试改的防御值
                                    if amount >= 0.25 then
                                        self.absorb = amount - 0.25
                                        v.components.combat.externaldamagetakenmultipliers:RemoveModifier("song_m_weakdefense")
                                    else
                                        self.absorb = 0
                                        v.components.combat.externaldamagetakenmultipliers:SetModifier(
                                            "song_m_weakdefense", 1+(0.25-amount)
                                        )
                                    end
                                end
                            end
                            v.buff_m_weakdefense = 0
                            v.components.health:SetAbsorptionAmount(v.components.health.absorb)
                            SpawnFx_enemy(v, "weakdefense", function(v)  --特效与buff结束控制
                                if v.components.combat ~= nil then
                                    v.components.combat.externaldamagetakenmultipliers:RemoveModifier("song_m_weakdefense")
                                end
                                local absorb_now = v.buff_m_weakdefense
                                v.buff_m_weakdefense = -1
                                if v.components.health ~= nil then
                                    v.components.health:SetAbsorptionAmount(absorb_now) --恢复已有的吸收值
                                end
                            end)
                        end
                    end
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "weakdefense")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.weakdefense.base,
            wave = colorMap.weakdefense.base,
        },
    },
    { --《流水无情》
        name = "song_m_nolove",
        fn_start = nil,
        cost_hunger  = true,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "nolove",
                { type = 1, value_pre = 5, value_do = 3 },
                function(doer, guitar)
                    local x, y, z = doer.Transform:GetWorldPosition()
                    local ents = TheSim:FindEntities(
                        x, y, z, 25, nil,
                        { "FX", "DECOR", "INLIMBO" }, { "tendable_farmplant", "beefalo" }
                    )
                    for i, v in ipairs(ents) do
                        if v ~= doer and v:IsValid() then
                            if v.components.farmplanttendable ~= nil then
                                if v:GetDistanceSqToPoint(x, y, z) < 64 then --8范围内
                                    v.components.farmplanttendable:TendTo(doer)
                                end
                            elseif
                                v.components.health ~= nil and not v.components.health:IsDead()
                                and v.components.herdmember ~= nil
                            then
                                local herd = v.components.herdmember:GetHerd()
                                if herd ~= nil and herd.components.mood ~= nil and herd.components.mood:IsInMood() then
                                    herd.components.mood:ResetMood()
                                end

                                local xx, yy, zz = v.Transform:GetWorldPosition()
                                local times = math.random(1, 2)
                                for i2 = 1, times, 1 do
                                    local xx2, yy2, zz2 = GetRandomPos(xx, yy, zz, 0.5+math.random(), nil)
                                    local fx = SpawnPrefab("song_m_nolove_note_fx")
                                    if fx then
                                        fx.Transform:SetPosition(xx2, yy2, zz2)
                                    end
                                end
                            end
                        end
                    end
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "nolove")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.nolove.base,
            wave = colorMap.nolove.base,
        },
    },
    { --《夜阑谣》
        name = "song_m_sweetdream",
        fn_start = nil,
        fn_do = function(doer, guitar)
            OnPlayDo(
                doer, guitar, "sweetdream",
                { type = 2, value_pre = 5, value_do = 3 },
                function(doer, guitar)
                    local x, y, z = doer.Transform:GetWorldPosition()
                    local ents = TheSim:FindEntities(
                        x, y, z, 25, nil,
                        { "player", "playerghost", "FX", "DECOR", "INLIMBO" }, { "sleeper", "tendable_farmplant" }
                    )
                    for i, v in ipairs(ents) do
                        if v ~= doer and v:IsValid() and (v.buff_m_sweetdream or CanDebuff(doer, v, x, y, z)) then
                            if v.components.sleeper ~= nil then
                                v.components.sleeper:AddSleepiness(5, 5)
                            elseif v.components.grogginess ~= nil then
                                v.components.grogginess:AddGrogginess(5, 5)
                            else
                                v:PushEvent("knockedout")
                            end

                            --为了持续给敌人催眠：因为多数生物会在睡着时主动放弃攻击对象，导致CanDebuff()失效
                            v.buff_m_sweetdream = true

                            local xx, yy, zz = v.Transform:GetWorldPosition()
                            local times = math.random(1, 2)
                            for i2 = 1, times, 1 do
                                local xx2, yy2, zz2 = GetRandomPos(xx, yy, zz, 0.5+math.random(), nil)
                                local fx = SpawnPrefab("song_m_sweetdream_note_fx")
                                if fx then
                                    fx.Transform:SetPosition(xx2, yy2, zz2)
                                end
                            end
                        end
                    end
                end,
                function(doer, guitar)
                    SpawnFx_doer(doer, "sweetdream")
                end
            )
        end,
        fn_end = PlayEndCommon,
        fxs = {
            note = colorMap.sweetdream.base,
            wave = colorMap.sweetdream.base,
        },
    },
}

return songs
