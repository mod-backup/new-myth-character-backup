local assets =
{
    Asset("ANIM", "anim/pound_medicine_myth.zip"),
    Asset("ANIM", "anim/medicine_pestle_anim.zip"),
    Asset("ANIM", "anim/medicine_pestle_anim_frog.zip"),
    Asset("ANIM", "anim/medicine_pestle_anim_winter.zip"),
	Asset("ANIM", "anim/medicine_pestle_anim_laurel.zip"),
    Asset("ANIM", "anim/swap_medicine_pestle_myth.zip"),
	
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth.xml"),
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth_winter.xml"),
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth_frog.xml"),
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth_apricot.xml"),
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth_laurel.xml"),
	Asset("ATLAS", "images/inventoryimages/medicine_pestle_myth_soprano.xml"),
}

local function lantern_onremovefx(fx)
    fx._lantern._lit_fx_inst = nil
end

local heldfxs = {
	apricot = "apricot_flower_fx_held",
	laurel = "laurel_flower_fx_held",
}
local function onequip(inst, owner)
	inst.components.myth_itemskin:OverrideSymbol(owner, "swap_object")
	local skin = inst.components.myth_itemskin.skin:value()
	if  heldfxs[skin] ~= nil then
		if inst._lit_fx_inst == nil then
			inst._lit_fx_inst = SpawnPrefab(heldfxs[skin])
			inst._lit_fx_inst._lantern = inst
			inst._lit_fx_inst.entity:AddFollower()
			inst:ListenForEvent("onremove", lantern_onremovefx, inst._lit_fx_inst)
		end
		inst._lit_fx_inst.entity:SetParent(owner.entity)
		inst._lit_fx_inst.Follower:FollowSymbol(owner.GUID, "swap_object", 40, -20, 0)
		--inst._lit_fx_inst.Follower:FollowSymbol(owner.GUID, "spider_repellent", 40, -20, 0)
	end
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function off(inst)
    local fx = inst._lit_fx_inst
    if fx ~= nil then
        if fx.KillFX ~= nil then
            inst._lit_fx_inst = nil
            inst:RemoveEventCallback("onremove", lantern_onremovefx, fx)
            --fx:RemoveEventCallback("enterlimbo", lantern_enterlimbo, inst)
            fx._lastpos = fx._lastpos or fx:GetPosition()
            fx.entity:SetParent(nil)
            if fx.Follower ~= nil then
                fx.Follower:FollowSymbol(0, "", 0, 0, 0)
            end
            fx.Transform:SetPosition(fx._lastpos:Get())
            fx:KillFX()
        else
            fx:Remove()
        end
    end
end

local function changefx(inst,change)
	local owner  = inst.components.equippable:IsEquipped() and inst.components.inventoryitem.owner  or nil
	if change then
		if inst._lit_fx_inst ~= nil and owner then
			inst._lit_fx_inst.Follower:FollowSymbol(owner.GUID, "spider_repellent", 40, -20, 0)
		end
	elseif inst._lit_fx_inst ~= nil then
		inst._lit_fx_inst.Follower:FollowSymbol(owner.GUID, "swap_object", 40, -20, 0)
	end
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
	off(inst)
end

--特殊物品特殊作用
local specileffct = {
	-- deerclops_eyeball = function(player,item)
	-- 	if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
	-- 		player.components.health:DoDelta(60)
	-- 		if player.components.debuffable ~= nil and player.components.debuffable:IsEnabled() then
	-- 			player.components.debuffable:AddDebuff("myth_freezebuff", "myth_freezebuff")
	-- 		end
	-- 	end
	-- end, --巨鹿
	moonbutterflywings	= function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			player.components.health:DoDelta(15)
			if player.components.sanity then
				player.components.sanity:DoDelta(20)
			end
		end
	end, --月蛾翅膀
	cactus_meat	= function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.sanity then
				player.components.sanity:DoDelta(15)
			end
		end
	end, --	仙人掌肉
	cactus_flower = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			player.components.health:DoDelta(20)
			if player.components.sanity then
				player.components.sanity:DoDelta(15)
			end
			if player.components.debuffable ~= nil and player.components.debuffable:IsEnabled() then
				player.components.debuffable:AddDebuff("myth_sanity_regenbuff", "myth_sanity_regenbuff")
			end
		end
	end ,--	仙人掌花
	spidergland	= function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if not player:HasTag("white_bone") then
				player.components.health:DoDelta(20)
			end
		end
	end, --	蜘蛛腺
	-- royal_jelly	= function(player)
    --     if player.components.debuffable ~= nil and player.components.debuffable:IsEnabled() and
    --         not (player.components.health ~= nil and player.components.health:IsDead()) and
    --         not player:HasTag("playerghost") then
    --         player.components.debuffable:AddDebuff("healthregenbuff", "healthregenbuff")
    --     end
	-- end, --蜂王浆
	foliage	= function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if not player:HasTag("white_bone") then
				player.components.health:DoDelta(8)
			end
		end
	end, --	蕨叶
	-- mandrake = function(player)
	-- 	if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
	-- 		if not player:HasTag("white_bone") then
	-- 			player.components.health:DoDelta(300)
	-- 		end
	-- 	end
	-- end, --	曼德拉草
	-- wormlight = function(player)
	-- 	if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
	-- 		if player.components.eater then
	-- 			for k =1 ,2 do
	-- 				local item = SpawnPrefab("wormlight")
	-- 				if player.components.eater:CanEat(item) then
	-- 					player.components.eater:Eat(item)
	-- 				else
	-- 					item:Remove()
	-- 				end
	-- 			end
	-- 		end
	-- 	end
	-- end, --	发光浆果
	-- wormlight_lesser = function(player)
	-- 	if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
	-- 		if player.components.eater then
	-- 			for k =1 ,2 do
	-- 				local item = SpawnPrefab("wormlight_lesser")
	-- 				if player.components.eater:CanEat(item) then
	-- 					player.components.eater:Eat(item)
	-- 				else
	-- 					item:Remove()
	-- 				end
	-- 			end
	-- 		end
	-- 	end
	-- end ,--	小发光浆果
	lightbulb = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.sanity then
				player.components.sanity:DoDelta(5)
			end
		end
	end ,--	荧光果
	petals = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.sanity then
				player.components.sanity:DoDelta(5)
			end
		end
	end, --	花瓣
	butterflywings = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if not player:HasTag("white_bone") then
				player.components.health:DoDelta(8)
			end
		end
	end, --	蝴蝶翅膀
	succulent_picked = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if not player:HasTag("white_bone") then
				player.components.health:DoDelta(20)
			end
		end
	end, --多肉
	-- lightninggoathorn = function(player)
    --     if player.components.debuffable ~= nil and player.components.debuffable:IsEnabled() and
    --         not (player.components.health ~= nil and player.components.health:IsDead()) and
    --         not player:HasTag("playerghost") then
    --         player.components.debuffable:AddDebuff("buff_electricattack", "buff_electricattack")
    --     end
	-- end,--羊角
	-- minotaurhorn = function(player)
	-- 	if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
	-- 		player.components.health:DoDelta(300)
	-- 	elseif	player:HasTag("playerghost") then
	-- 		player:PushEvent("respawnfromghost")
	-- 	end
	-- end, --犀牛角
	moon_tree_blossom = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.sanity then
				player.components.sanity:DoDelta(5)
			end
		end
	end,--月树花
	plantmeat = function(player)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.moisture then
				player.components.moisture:DoDelta(-15)
			end
		end
	end,--叶肉
}

local IngredientValues = {
	fruit = function(player, values)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			if player.components.sanity then
				player.components.sanity:DoDelta(values or 0)
			end
		end
	end,
	veggie = function(player, values)
		if player.components.health and not player.components.health:IsDead() and not player:HasTag("playerghost") then
			player.components.health:DoDelta(values or 0)
		end
	end,
}

local cooking = require("cooking")
local ingredients = cooking.ingredients
local function checkfoodvalue(food)
	if ingredients[food.prefab] ~= nil then
		local tags = ingredients[food.prefab].tags
		if tags.fruit ~= nil then
			food.addvalue = tags.fruit > 0.5 and 15 or 5
			return IngredientValues.fruit, food.addvalue
		elseif  tags.veggie ~= nil then
			food.addvalue = tags.veggie > 0.5 and 20 or 8
			return IngredientValues.veggie, food.addvalue
		end
	end
	return nil,0
end

local colourskin = {
	apricot = { r = 247 / 255, g = 164 / 255, b = 158 / 255 },
}
local function spawnfx(inst,medicine)
    local fx = SpawnPrefab("myth_yutu_heal_fx")
	local colour = nil
    fx.entity:AddFollower():FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, -50, 0)
	if medicine and medicine.weapon  and medicine.weapon.components.myth_itemskin.skin then
		local skin = medicine.weapon.components.myth_itemskin.skin:value()
		colour = colourskin[skin]
	end
    fx:Setup(inst,colour)
end

local skin_fxbuild = {
	apricot = "myth_lifeplant_fx_pink",
	laurel = "myth_lifeplant_fx_laurel",
}

local function addbuff(inst,player,fn1,medicine)
	local projectile = SpawnPrefab("myth_sparkle")
	if projectile then
		if medicine and medicine.weapon  and medicine.weapon.components.myth_itemskin.skin then
			local skin = medicine.weapon.components.myth_itemskin.skin:value()
			if skin_fxbuild[skin] ~= nil then
				projectile.AnimState:SetBuild(skin_fxbuild[skin])
			end
		end
		projectile.Transform:SetPosition(inst.Transform:GetWorldPosition())
		if  projectile.components.projectile then
			projectile.components.projectile:SetOnHitFn(function(hua, owner, target)
				if target:IsValid() then
					if fn1.fn ~= nil then
						fn1.fn(target, fn1.valus)
						spawnfx(target,medicine)
					end
					hua:Remove()
				end
			end)
			projectile.components.projectile:Throw(projectile, player)
		end
	end
end

local function daoyao(inst,slot)
	inst.task[slot] = nil
	if inst.owner and inst.owner:IsValid() then
		local container = inst.components.container
		local fns = {[slot] = {}}
		local item = container:GetItemInSlot(slot)

		if item ~= nil then
			if inst.owner and inst.owner.components.hunger then
				inst.owner.components.hunger:DoDelta(-2)
			end

			if item.OnPowderUseFn ~= nil then --药粉作用(不参与已有的 fns[slot].fn 等逻辑)
				item.OnPowderUseFn(item, inst.owner, false)
			elseif specileffct[item.prefab] ~= nil then --特殊材料作用
				fns[slot].fn = specileffct[item.prefab]
			else --默认作用
				fns[slot].fn, fns[slot].valus = checkfoodvalue(item)
			end

			container:RemoveItem(container:GetItemInSlot(slot)):Remove()
		end

		if fns[slot].fn ~= nil then
			--先对捣药者自己用
			fns[slot].fn(inst.owner, fns[slot].valus)
			spawnfx(inst.owner,inst)

			--然后再对周围的玩家用
			local x, y, z = inst.owner.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(x, 0, z, 15, {"player"})
			for i, v in ipairs(ents) do
				if v ~= inst.owner then
					addbuff(inst.owner,v,fns[slot],inst)
				end
			end
		end

	end
end

local function onget(inst,data)
	if data and data.slot then
		if	inst.task[data.slot] ~= nil then
			inst.task[data.slot]:Cancel()
		end
		inst.task[data.slot] = inst:DoTaskInTime(3,function() daoyao(inst,data.slot) end)
	end
end

local function onlose(inst,data)
	if data and data.slot then
		if	inst.task[data.slot] ~= nil then
			inst.task[data.slot]:Cancel()
			inst.task[data.slot] = nil
		end
	end
end

local function containerfn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    if not TheWorld.ismastersim then
		inst.OnEntityReplicated = function(inst)
			inst.replica.container:WidgetSetup("medicine_pestle_myth")
		end
        return inst
    end
	inst.task = {}
    inst:AddComponent("container")
    inst.components.container:WidgetSetup("medicine_pestle_myth")
	inst.components.container.OnRemoveEntity = inst.components.container.DropEverything

	inst.persists = false

    inst:ListenForEvent("itemget", onget)
    inst:ListenForEvent("itemlose", onlose)

    return inst
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("swap_medicine_pestle_myth")
    inst.AnimState:SetBuild("swap_medicine_pestle_myth")
    inst.AnimState:PlayAnimation("idle_none")

    inst:AddTag("sharp")
    inst:AddTag("pointy")
	inst:AddTag("medicine_pestle_myth")
    inst:AddTag("weapon")
	inst:AddTag("myth_removebydespwn")
	
	MakeInventoryFloatable(inst)
	
    inst:AddComponent("myth_itemskin")
    inst.components.myth_itemskin.character = 'myth_yutu'
    inst.components.myth_itemskin.prefab = 'medicine_pestle_myth'
    inst.components.myth_itemskin:SetData{

		soprano = {
            swap = {build = "swap_medicine_pestle_myth", folder = "swap_soprano"},
            icon = {atlas = "medicine_pestle_myth_soprano.xml", image = "medicine_pestle_myth_soprano"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_soprano"},			
		},
		laurel = {
            swap = {build = "swap_medicine_pestle_myth", folder = "swap_laurel"},
            icon = {atlas = "medicine_pestle_myth_laurel.xml", image = "medicine_pestle_myth_laurel"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_laurel"},
        },
		apricot = {
            swap = {build = "swap_medicine_pestle_myth", folder = "swap_apricot"},
            icon = {atlas = "medicine_pestle_myth_apricot.xml", image = "medicine_pestle_myth_apricot"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_apricot"},
        },
        frog = {
            swap = {build = "swap_medicine_pestle_myth", folder = "swap_frog"},
            icon = {atlas = "medicine_pestle_myth_frog.xml", image = "medicine_pestle_myth_frog"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_frog"},
        },
        winter = {
        	swap = {build = "swap_medicine_pestle_myth", folder = "swap_winter"},
            icon = {atlas = "medicine_pestle_myth_winter.xml", image = "medicine_pestle_myth_winter"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_winter"},
        },
        default = {
            swap = {build = "swap_medicine_pestle_myth", folder = "swap_none"},
            icon = {atlas = "medicine_pestle_myth.xml", image = "medicine_pestle_myth"},
            anim = {bank = "swap_medicine_pestle_myth", build = "swap_medicine_pestle_myth", anim = "idle_none"},
        },
    }
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.ChangeFX = changefx
	
    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(51)
	inst.components.weapon:SetRange(-0.3)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/medicine_pestle_myth.xml"
	
    inst:AddComponent("equippable")
	inst.components.equippable.restrictedtag = "myth_yutu"
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("medicine_pestle_myth", fn, assets),
	Prefab("medicine_pestle_container_myth", containerfn)
