local function CommonSay(doer, say)
    if doer.components.talker ~= nil then
        doer.components.talker:Say(say)
    end
end

local hua_internet_topology = Class(function(self, inst)
    self.inst = inst
    self.new_node_id = 1
    self.new_link_id = 1

    self.nodes = {}
    self.links = {}

    self.node_to_links = {}
    self.link_to_parms = {}
end)

function hua_internet_topology:OnLoad(data)
    if data then
        if data.new_node_id then
            self.new_node_id = data.new_node_id
        end
        if data.new_link_id then
            self.new_link_id = data.new_link_id
        end
    end
end

function hua_internet_topology:OnSave()
    return {
        new_node_id = self.new_node_id,
        new_link_id = self.new_link_id,
    }
end

--------------
--- 基础部分
--------------

function hua_internet_topology:GetNode(id)
    if id and self.nodes[id] and self.nodes[id]:IsValid() then
        return self.nodes[id]
    end
end

function hua_internet_topology:RegisterNode(node)
    local id = node.node_id
    if id == nil then
        id = self.new_node_id
        node.node_id = id

        self.new_node_id = self.new_node_id + 1
    end

    assert(not self.nodes[id], "注册了重复的节点 ID！！！！")
    self.nodes[id] = node
    self.node_to_links[node] = self.node_to_links[node] or {}
end

function hua_internet_topology:RemoveNode(node)
    local id = node.node_id
    if id == nil then
        print("ERROR：啊？被移除的节点居然没有 ID！！！")
        return
    end

    self.nodes[id] = nil

    -- 以下部分为破坏所有相关的连线
    local to_be_removed = {} -- 防止 next 错误
    for link in pairs(self.node_to_links[node]) do
        to_be_removed[link] = true
    end
    for link in pairs(to_be_removed) do
        link:Remove()
    end
    -- .......................

    self.node_to_links[node] = nil
end

function hua_internet_topology:RegisterLink(link)
    local id = link.link_id
    if id == nil then
        id = self.new_link_id
        link.link_id = id

        self.new_link_id = self.new_link_id + 1
    end

    assert(not self.links[id], "注册了重复的线段 ID！！！！")
    local node1 = self:GetNode(link.node1_id)
    local node2 = self:GetNode(link.node2_id)
    if node1 == nil or node2 == nil then -- 一条没用的线段
        return
    end

    self.links[id] = link
    self.node_to_links[node1][link] = true
    self.node_to_links[node2][link] = true

    local x1, _, z1 = node1.Transform:GetWorldPosition()
    local x2, _, z2 = node2.Transform:GetWorldPosition()
    local A = z2 - z1
    local B = x1 - x2
    local C = x1 * (z1 - z2) + z1 * (x2 - x1)
    local length = math.sqrt((x1 - x2) * (x1 - x2) + (z1 - z2) * (z1 - z2))
    local angle = math.atan2(z1 - z2, x2 - x1) / DEGREES -- 此角度顺时针为正

    self.link_to_parms[link] = {
        length = length,
        x1 = x1,
        z1 = z1,
        x2 = x2,
        z2 = z2,
        angle = angle,
        A = A,
        B = B,
        C = C,
    }

    link.Transform:SetPosition(x1, 0, z1)
    link:SetSurface(angle, length)
    self:DrawNodesAnimation()
end

function hua_internet_topology:RemoveLink(link)
    local id = link.link_id
    if id == nil then
        print("ERROR：啊？被移除的线段居然没有 ID！！！")
        return
    end

    self.links[id] = nil

    local node1 = self:GetNode(link.node1_id)
    local node2 = self:GetNode(link.node2_id)
    if node1 and self.node_to_links[node1] then
        self.node_to_links[node1][link] = nil
    end
    if node2 and self.node_to_links[node2] then
        self.node_to_links[node2][link] = nil
    end

    self:DrawNodesAnimation()
end

--------------
--- 替换柱子的贴图，图个省事直接全图重算
--------------

function hua_internet_topology:DrawNodesAnimation()
    for _, node in pairs(self.nodes) do
        node.AnimState:PlayAnimation((self.node_to_links[node] and IsTableEmpty(self.node_to_links[node])) and "idle_none" or "idle")
    end
end

--------------
--- 建造部分
--------------

function hua_internet_topology:CheckLinkBetweenNodes(node1, node2)
    for l1 in pairs(self.node_to_links[node1]) do
        for l2 in pairs(self.node_to_links[node2]) do
            if l1 == l2 then -- 存在两个节点之间的连线
                return
            end
        end
    end

    return true
end

function hua_internet_topology:BuildLink(doer, node1, node2)
    if doer.components.constructionbuilder == nil then
        return
    end

    local x1, _, z1 = node1.Transform:GetWorldPosition()
    local x2, _, z2 = node2.Transform:GetWorldPosition()
    local dsq = distsq(x1, z1, x2, z2)
    if dsq > TUNING.HUA_INTERNET_BUILD_DIST * TUNING.HUA_INTERNET_BUILD_DIST then
        CommonSay(doer, STRINGS.HUA_INTERNET_BUILDER.FAILED_DIST)
        return
    end

    if dsq < 0.1 then -- 防止除数为零
        return
    end

    if not self:CheckLinkBetweenNodes(node1, node2) then
        CommonSay(doer, STRINGS.HUA_INTERNET_BUILDER.FAILED_EXIST)
        return
    end

    local length = math.sqrt((x1 - x2) * (x1 - x2) + (z1 - z2) * (z1 - z2))
    local count = math.clamp(math.floor(TUNING.HUA_INTERNET_COST_MIN_SILK + TUNING.HUA_INTERNET_COST_UNIT_SILK * length * 0.25), 1, 100)
    local builder = SpawnPrefab("hua_internet_builder_" .. count)
    if builder == nil then
        CommonSay(doer, STRINGS.HUA_INTERNET_BUILDER.FAILED_PREFAB .. "hua_internet_builder_" .. count)
        return
    end

    builder.doer = doer
    builder.node1 = node1
    builder.node2 = node2
    builder.Transform:SetPosition(doer.Transform:GetWorldPosition())
    doer.components.constructionbuilder:StartConstruction(builder)

    return true
end

function hua_internet_topology:FinishLink(doer, node1, node2)
    if not self:CheckLinkBetweenNodes(node1, node2) then
        CommonSay(doer, STRINGS.HUA_INTERNET_BUILDER.FAILED_EXIST)
        return
    end

    local link = SpawnPrefab("hua_internet_link")
    link:SetNodes(node1, node2)
    self:RegisterLink(link)

    return true
end

--------------
--- 使用部分
--------------

function hua_internet_topology:CheckLinkIsNear(player, link)
    local parms = self.link_to_parms[link]
    if parms == nil then
        return
    end

    local A = parms.A
    local B = parms.B
    local C = parms.C
    local pos = player:GetPosition()
    local d2 = (A * pos.x + B * pos.z + C) * (A * pos.x + B * pos.z + C) / (A * A + B * B)
    if d2 > TUNING.HUA_INTERNET_USE_DIST * TUNING.HUA_INTERNET_USE_DIST then
        return
    end

    local shadow = ((pos.x - parms.x1) * (parms.x2 - parms.x1) + (pos.z - parms.z1) * (parms.z2 - parms.z1)) / parms.length
    if shadow > parms.length + TUNING.HUA_INTERNET_USE_DIST or shadow < -TUNING.HUA_INTERNET_USE_DIST then
        return
    end

    return true
end

function hua_internet_topology:GetAnotherNodeOnLink(link, one_node)
    local node1 = self:GetNode(link.node1_id)
    return node1 == one_node and self:GetNode(link.node2_id) or node1
end

function hua_internet_topology:UseInternet(player, node)
    if node and self.node_to_links[node] then
        for link in pairs(self.node_to_links[node]) do
            if self:CheckLinkIsNear(player, link) then -- 附近有通往目的地的线就可以了，不需要管是哪根线

                -- 扣除耐久
                local item = TheHua:CheckGeneralSpiderManItem(player)
                if item and item.components.finiteuses then
                    item.components.finiteuses:Use(1)
                end

                player:PushEvent("hua_internet_destination_go", {node = node, last_node = self:GetAnotherNodeOnLink(link, node)})
                return
            end
        end
    end

    CommonSay(player, STRINGS.HUA_INTERNET_USE.FAILED_NO_LINK)
end

function hua_internet_topology:TravelToNextNode(player, last_node, current_node)
    -- 判什么空，直接开干
    local min_length = 1314
    local next_node
    for link in pairs(self.node_to_links[current_node]) do
        local candidate_node = self:GetAnotherNodeOnLink(link, current_node)
        if candidate_node ~= last_node then
            local len = self.link_to_parms[link].length
            if len < min_length then
                min_length = len
                next_node = candidate_node
            end
        end
    end

    if next_node then
        player:PushEvent("hua_internet_destination_go", {node = next_node, last_node = current_node})
    else
        player.sg:GoToState("hua_internet_waiting") -- 防止失足坠海
    end
end

return hua_internet_topology