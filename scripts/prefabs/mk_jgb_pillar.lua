local assets =
{
    Asset("ANIM", "anim/mk_jgb.zip"),
	Asset("ANIM", "anim/mk_jgb_spin.zip"),
}

local function OnBuilt(inst, builder)
	if not( builder and builder:IsValid() and builder.prefab == 'monkey_king')then
		return 
	end
	local pos = builder:GetPosition()
	pos.y =0
	local a = -builder.Transform:GetRotation()*DEGREES
	local dist = 4
	local skin = builder.AnimState:GetBuild()

	local targetpos = pos + Vector3(dist*math.cos(a), 0, dist*math.sin(a))
	local pillar = SpawnPrefab("mk_jgb_pillar")
	pillar.owner = builder
	pillar.Transform:SetPosition(targetpos:Get())
	pillar.AnimState:PlayAnimation("pillar_drop")
	pillar:DoTaskInTime(0.5, function() 
		pillar.Physics:SetActive(true) 

		if not (builder._is_player_astral ~= nil and  builder._is_player_astral:value()) then
			pillar.components.groundpounder:GroundPound()
			pillar:DoTaskInTime(0.1, function() 
				pillar.components.groundpounder:GroundPound()
			end)
		end
		ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .025, 1.25, pillar, 40)
	end)
	pillar:SetSkin(skin)

	inst:Remove()
end

local function rec()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	-- inst.entity:AddNetwork()

	inst:AddTag("NOCLICK")

	inst.OnBuilt = OnBuilt
	return inst
end

local function SetSkin(inst, skin)
	if skin == 'monkey_king_sea' then
		inst.skin = "sea"
	elseif skin == 'monkey_king_fire' then
		inst.skin = "fire"
	elseif skin == 'monkey_king_opera' then
		inst.skin = "opera"
	elseif skin == 'monkey_king_ear' then
		inst.skin = "ear"
	elseif skin == 'monkey_king_wine' then
		inst.skin = "wine"
	elseif skin == 'monkey_king' or skin == 'monkey_king_none' then
		inst.skin = "none"
	else
		inst.skin = inst.skin or "none"
	end

	if inst.skin == "none" then
		inst.AnimState:ClearOverrideSymbol("p_none")
	else
		inst.AnimState:OverrideSymbol("p_none", "mk_jgb", "p_"..inst.skin)
	end
end

local function isocean(inst,pos)
	local pos = pos or inst:GetPosition()
	if TheWorld.Map:IsOceanAtPoint(pos.x, 0, pos.z, false) then
		inst.components.floater:OnLandedServer()
	else
		inst.pillar_fx = inst:SpawnChild("mk_jgb_pillar_fx")
		inst.pillar_fx.Transform:SetScale(1/2,1/2,1/2)
	end
end

local function OnSave(inst, data)
	data.skin = inst.skin
end

local function OnLoad(inst, data)
	inst.skin = data and data.skin or "none"
	inst:SetSkin()
	inst.Physics:SetActive(true)
	isocean(inst)
end

local function GetPoints(pt)
    local points = {}
    local radius = 1
    for i = 1, 4 do
        local theta = 0
        local circ = 2*PI*radius
        local numPoints = circ * 0.25
        for p = 1, numPoints do
            if not points[i] then
                points[i] = {}
            end
            local offset = Vector3(radius * math.cos(theta), 0, -radius * math.sin(theta))
            local point = pt + offset
            table.insert(points[i], point)
            theta = theta - (2*PI/numPoints)
        end

        radius = radius + 3
    end
    return points
end
local function spawnwaves(inst, numWaves, totalAngle, waveSpeed, wavePrefab, initialOffset, idleTime, instantActivate, random_angle)
    SpawnAttackWaves(
        inst:GetPosition(),
        (not random_angle and inst.Transform:GetRotation()) or nil,
        initialOffset or (inst.Physics and inst.Physics:GetRadius()) or nil,
        numWaves,
        totalAngle,
        waveSpeed,
        wavePrefab,
        idleTime,
        instantActivate
    )
end
local WALKABLEPLATFORM_TAGS = {"walkableplatform"}
local function DoDamage(inst)
	local map = TheWorld.Map
	ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .015, .8, inst, 20)
	local pt = inst:GetPosition()
	local points = GetPoints(pt)
	local ents = TheSim:FindEntities(pt.x, 0, pt.z,8,nil, {"player","playerghost", "abigail","INLIMBO","companion","laozi","FX", "NOCLICK", "DECOR"})       
	for _,v in pairs(ents)do
		if v:IsValid() and v.components.workable ~= nil and v.components.workable:CanBeWorked() and	v.components.workable.action ~= ACTIONS.NET then
			v.components.workable:Destroy(inst)
		end
		if v:IsValid() and v.components.health ~= nil and not v.components.health:IsDead() then
			if inst.owner ~= nil and inst.owner.components.combat:CanTarget(v) and v.components.combat  then
				v.components.combat:GetAttacked(inst.owner,200, nil, "mk_striker")
			elseif inst.components.combat:CanTarget(v) then
				inst.components.combat:DoAttack(v, nil, nil, nil, 1)
			end
		end
	end
	local platform_ents = TheSim:FindEntities(pt.x, 0, pt.z, 4 + TUNING.MAX_WALKABLE_PLATFORM_RADIUS, WALKABLEPLATFORM_TAGS, { "FX", "NOCLICK", "DECOR", "INLIMBO" })
	for i, p_ent in ipairs(platform_ents) do
		if p_ent ~= inst and p_ent:IsValid() and p_ent.Transform ~= nil and p_ent.components.boatphysics ~= nil then
			local v2x, v2y, v2z = p_ent.Transform:GetWorldPosition()
			local mx, mz = v2x - pt.x, v2z - pt.z
			if mx ~= 0 or mz ~= 0 then
				local normalx, normalz = VecUtil_Normalize(mx, mz)
				p_ent.components.boatphysics:ApplyForce(normalx, normalz, 5)
			end
		end
	end
	isocean(inst,pt)
	if map:IsOceanAtPoint(pt.x, 0, pt.z, false) then
		spawnwaves(inst, 6, 360, 6, nil, nil, 1, nil, true)
		return
	end
	SpawnPrefab("groundpoundring_fx").Transform:SetPosition(pt.x, 0, pt.z)
	for i = 1, 2 do
		if points[i] then
			for k, v in pairs(points[i]) do
				if map:IsPassableAtPoint(v:Get()) then
					SpawnPrefab("groundpound_fx").Transform:SetPosition(v.x, 0, v.z)
				end
			end
		end
	end
end

local function pillar()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst.Transform:SetScale(2,2,2)

	MakeObstaclePhysics(inst, 1, 10)
	inst.Physics:SetActive(false)

	inst.AnimState:SetBank('mk_jgb')
	inst.AnimState:SetBuild('mk_jgb')
	inst.AnimState:SetPercent('pillar_drop', 1)
	inst.AnimState:Hide("shadow")
	MakeInventoryFloatable(inst, "large", 0.6, {0.6, 1.2, 0.6})

	inst.SetSkin = SetSkin
	inst.skin = "none"

	inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("inspectable")

	inst:AddComponent("myth_groundpounder")
	inst.components.groundpounder = inst.components.myth_groundpounder
	inst.components.groundpounder.destroyer = true
	inst.components.groundpounder.noTags = {"FX", "NOCLICK", "DECOR", "INLIMBO", "player", "laozi"}

	inst:AddComponent("mk_jgb_pillar")

	inst:AddComponent("combat")
	inst.components.combat.defaultdamage= 200

	inst.DoDamage =  DoDamage
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad

	return inst
end


local function SetSpinSkin(inst, skin)
	inst.skin = skin or "none"
	if inst.skin == "none" then
		inst.AnimState:ClearOverrideSymbol("swap_none")
	else
		inst.AnimState:OverrideSymbol("swap_none", "mk_jgb_spin", "swap_"..inst.skin)
	end
end

local function OnSpinSave(inst, data)
	data.skin = inst.skin
end

local function OnSpinLoad(inst, data)
	if data and data.skin then
		inst.skin = data.skin
		inst:SetSpinSkin()
		if inst.components.projectile then
			inst.components.projectile.onmiss(inst)
		end
	end
end
local function onhit(inst, attacker, target)
	local j = SpawnPrefab("mk_jgb")
	j.components.myth_itemskin:ChangeSkin(inst.skin or "none")
	if target and target:IsValid() and not (target.components.health and target.components.health:IsDead()) and target.components.inventory then
		target:PushEvent("catchjgb")
		target.components.inventory:Equip(j)
	else
	    j.Transform:SetPosition(inst.Transform:GetWorldPosition())
    end
    inst:Remove()
end

local function DoSpinDamage(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, 2, {"_combat", }, { "playerghost", "INLIMBO", "player","companion","wall","abigail","laozi","FX" })) do
        if not inst.damagetargets[v] and v:IsValid() and not (v.components.health ~= nil and v.components.health:IsDead()) then
            --if inst.components.combat and inst.components.combat:CanTarget(v) then
                inst.damagetargets[v] = true
				--inst.components.combat:DoAttack(v)
				v.components.combat:GetAttacked(inst, 34,nil)
            --end
        end
    end
end

local function spin()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	MakeInventoryPhysics(inst)
	RemovePhysicsColliders(inst)

	inst.AnimState:SetBank('mk_jgb_spin')
	inst.AnimState:SetBuild('mk_jgb_spin')
	inst.AnimState:PlayAnimation('apin', true)
 
    --inst.Transform:SetScale(0.6, 0.6, 0.6)
	inst.AnimState:OverrideSymbol("fx_lunge_streak", "monkey_king_lunge_new", "fx_lunge_streak")
	inst:AddTag("projectile")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
	inst.damagetargets = {}
	inst.SetSkin = SetSpinSkin
	inst.skin = "none"

	--inst:AddComponent("combat")
    --inst.components.combat:SetDefaultDamage(34)

    inst:AddComponent("projectile")
    inst.components.projectile:SetSpeed(10)
    inst.components.projectile:SetOnHitFn(onhit)
	inst.components.projectile:SetOnMissFn(onhit)
	inst.components.projectile:SetHitDist(2)
	--inst.components.projectile:SetRange(60)
	inst.components.projectile:SetLaunchOffset(Vector3(0, 0.5, 0))
	local old_OnUpdate = inst.components.projectile.OnUpdate
	inst.components.projectile.OnUpdate = function(self,dt)
		old_OnUpdate(self,dt)
		if self.inst and self.inst:IsValid() then
			self.speed = math.min(20,self.speed + 30*dt)
			self.inst.Physics:SetMotorVel(self.speed, 0, 0)
		end
	end
	inst.damagetask = inst:DoPeriodicTask(0,DoSpinDamage,0.1)
	inst.OnSave = OnSpinSave
	inst.OnLoad = OnSpinLoad
	return inst
end

local function makeaoe(name, fx, size)
    local function fn()
        local inst = SpawnPrefab(fx)
        inst.AnimState:SetScale(size, size)
        return inst
    end
    return Prefab(name, fn)
end

local function fxfn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("sinkhole")
    inst.AnimState:SetBuild("antlion_sinkhole")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(2)
    inst.Transform:SetEightFaced()
    inst:AddTag("fx")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst.persists = false
    return inst
end
return Prefab("mk_jgb_rec", rec), 
	Prefab("mk_jgb_pillar", pillar, assets),
	Prefab("mk_jgb_spin", spin),
	Prefab("mk_jgb_pillar_fx", fxfn),
	makeaoe("jgb_reticuleaoe", "reticuleaoe", 2.2),
	makeaoe("jgb_reticuleaoeping", "reticuleaoeping", 2.2)

