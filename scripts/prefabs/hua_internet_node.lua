local assets = {
    Asset("ANIM", "anim/hua_internet_node.zip"),
}

local assets_sea = {
    Asset("ANIM", "anim/hua_internet_platform.zip"),
    Asset("ANIM", "anim/hua_internet_node_sea.zip"),
}

local function OnDig(inst)
    local collapse_fx = SpawnPrefab("collapse_small")
    collapse_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    collapse_fx:SetMaterial("wood")

    inst:Remove()
end

local function RemoveNode(inst)
    TheWorld.components.hua_internet_topology:RemoveNode(inst)
end

local function RegisterNode(inst)
    TheWorld.components.hua_internet_topology:RegisterNode(inst)
end

local function OnSave(inst, data)
    data.node_id = inst.node_id
end

local function OnLoad(inst, data)
    if data == nil then
        return
    end

    inst.node_id = data.node_id
end

local function UpdateHighLight(inst)
    if ThePlayer == nil or not ThePlayer:IsValid() then
        return
    end

    local chosen_node = ThePlayer.player_classified and ThePlayer.player_classified.hua_internet_chosen_node and ThePlayer.player_classified.hua_internet_chosen_node:value()
    if chosen_node == nil or not chosen_node:IsValid() or chosen_node == inst then
        inst.AnimState:SetHighlightColour()
        return
    end

    if chosen_node:GetDistanceSqToInst(inst) <= TUNING.HUA_INTERNET_BUILD_DIST * TUNING.HUA_INTERNET_BUILD_DIST then
        inst.AnimState:SetHighlightColour(0.4, 0.4, 0.4, 1)
    else
        inst.AnimState:SetHighlightColour()
    end
end

local function common(name)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank(name)
    inst.AnimState:SetBuild(name)
    inst.AnimState:PlayAnimation("idle_none")

    inst:AddTag("hua_internet_node")

    inst.entity:SetPristine()

    if TheNet:GetIsClient() then
        inst:DoPeriodicTask(FRAMES * 10, UpdateHighLight, math.random() * FRAMES * 10)
    end

    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumBurnable(inst)
    MakeMediumPropagator(inst)

    inst:AddComponent("inspectable")

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_TINY)

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetOnFinishCallback(OnDig)
    inst.components.workable:SetWorkLeft(5)

    inst:ListenForEvent("onremove", RemoveNode)
    inst:DoTaskInTime(FRAMES, RegisterNode)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

local function fn()
    return common("hua_internet_node")
end

local function MakeSeaPlatform()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst:AddTag("NOCLICK")
    inst:AddTag("FX")

    inst.AnimState:SetBank("hua_internet_platform")
    inst.AnimState:SetBuild("hua_internet_platform")
    inst.AnimState:PlayAnimation("hello")
    inst.AnimState:SetSortOrder(ANIM_SORT_ORDER.OCEAN_BOAT)
    inst.AnimState:SetFinalOffset(1)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)

    return inst
end

local function SeaOnCollide(inst, data)
    local boat_physics = data.other.components.boatphysics
    if boat_physics ~= nil then
        local hit_velocity = math.floor(math.abs(boat_physics:GetVelocity() * data.hit_dot_velocity) / boat_physics.max_velocity + 0.5)
        inst.components.workable:WorkedBy(data.other, hit_velocity * TUNING.SEASTACK_MINE)
    end
end

local function sea_fn()
    local inst = common("hua_internet_node_sea",true)

    TheHua:MakeWaterCrackablePhysics(inst, 0.80, 2, 0.75)
    inst:SetPhysicsRadiusOverride(2.35)

    inst:AddTag("ignorewalkableplatforms")

    MakeInventoryFloatable(inst, "med", 0.1, {1.1, 0.9, 1.1})
    inst.components.floater.bob_percent = 0
    
    inst:AddChild(MakeSeaPlatform())

    if not TheWorld.ismastersim then
        return inst
    end

    inst:ListenForEvent("on_collide", SeaOnCollide)

    return inst
end

return Prefab("hua_internet_node", fn, assets), Prefab("hua_internet_node_sea", sea_fn, assets_sea)