require "behaviours/follow"
require "behaviours/wander"

local Ghost_ClementBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function IsValid(target)
    return target ~= nil and target:IsValid() and target.entity:IsVisible() and
        target.components.health ~= nil and not target.components.health:IsDead()
end

local function FixCurseTarget(inst)
    if IsValid(inst.cursetarget) and not inst.cursetarget:IsInLimbo() then
        inst.OnLinkPlayer(inst, inst.cursetarget) --重新确定跟随关系（防止被召唤者攻击后就follow组件不跟随召唤者了）
    else
        inst.cursetarget = nil
    end
end

local function GetCombatTarget(inst)
    FixCurseTarget(inst)

    --因为follower组件自带跟随leader的攻击指令以及被打指令，所以这里不用主动寻找leader的仇恨者去攻击
    -- if inst.cursetarget ~= nil then --只有召唤者存在时，才会主动保护召唤者
    --     if
    --         not (
    --             IsValid(inst.components.combat.target)
    --             and not inst.components.combat.target:IsInLimbo()
    --             and inst:GetDistanceSqToInst(inst.components.combat.target) < TUNING.GHOST_FOLLOW_DSQ
    --         )
    --     then
    --         inst.components.combat:SetTarget(nil)

    --         local ent = FindEntity(inst.cursetarget, 25,
    --             function(target)
    --                 if
    --                     IsValid(target)
    --                     and target.components.combat ~= nil
    --                     and target.components.combat.target ~= nil and target.components.combat:TargetIs(inst.cursetarget)
    --                 then
    --                     return true
    --                 end
    --                 return false
    --             end,
    --             {"_combat", "_health"}, {"INLIMBO", "noauradamage", "veggie", "structure", "wall", "groundspike", "smashable"}, nil
    --         )
    --         if ent ~= nil then
    --             inst.components.combat:SetTarget(ent)
    --         end
    --     end
    -- end

    return inst.components.combat.target
end

local function GetCurseTarget(inst)
    FixCurseTarget(inst)

    -- if inst.cursetarget == nil then --test:不会易主的
    --     local someone = FindEntity(inst, 10,
    --         function(target)
    --             if IsValid(target) then
    --                 return true
    --             end
    --             return false
    --         end,
    --         {"player"}, {"playerghost", "INLIMBO"}, nil
    --     )
    --     if someone ~= nil and inst.OnLinkPlayer ~= nil then
    --         inst.OnLinkPlayer(inst, someone)
    --     end
    -- end

    return inst.cursetarget
end

function Ghost_ClementBrain:OnStart()
    local root = PriorityNode({
        --优先跟踪自己的仇恨对象
        WhileNode(function() return GetCombatTarget(self.inst) ~= nil end, "FollowMyEnemy",
            Follow(self.inst,
                function() return self.inst.components.combat.target end, --不管条件，因为条件限制写在combat:SetKeepTargetFunction里
                TUNING.GHOST_RADIUS*.25, TUNING.GHOST_RADIUS*.5, TUNING.GHOST_RADIUS
            )
        ),

        --其次跟踪可跟踪对象
        Follow(self.inst,
            function() return GetCurseTarget(self.inst) end,
            TUNING.ABIGAIL_DEFENSIVE_MIN_FOLLOW, TUNING.ABIGAIL_DEFENSIVE_MED_FOLLOW, TUNING.ABIGAIL_DEFENSIVE_MAX_FOLLOW
        ),

        --没有跟踪对象就徘徊
        Wander(self.inst)
    }, 1)

    self.bt = BT(self.inst, root)
end

return Ghost_ClementBrain
