require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/wander"
require "behaviours/doaction"
require "behaviours/avoidlight"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/useshield"

local BrainCommon = require "brains/braincommon"

local SEE_FOOD_DIST = 10

local TRADE_DIST = 20

local MAX_WANDER_DIST = 32

local DAMAGE_UNTIL_SHIELD = 50
local SHIELD_TIME = 3
local AVOID_PROJECTILE_ATTACKS = false
local HIDE_WHEN_SCARED = true

local SpiderBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local GETTRADER_MUST_TAGS = { "player" }
local function GetTraderFn(inst)
    return inst.components.trader ~= nil
        and FindEntity(inst, TRADE_DIST, function(target) return inst.components.trader:IsTryingToTradeWithMe(target) end, GETTRADER_MUST_TAGS)
        or nil
end

local function KeepTraderFn(inst, target)
    return inst.components.trader ~= nil
        and inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetFaceTargetFn(inst)
    return inst.components.follower.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

function SpiderBrain:OnStart()

    local pre_nodes = PriorityNode({
        BrainCommon.PanicWhenScared(self.inst, .3),
        WhileNode(function() return self.inst.components.hauntable and self.inst.components.hauntable.panic end, "PanicHaunted", Panic(self.inst)),
        WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire", Panic(self.inst)),
    })

    local post_nodes = PriorityNode({
        FaceEntity(self.inst, GetTraderFn, KeepTraderFn),
        Wander(self.inst, function() return self.inst.components.knownlocations:GetLocation("home") end, MAX_WANDER_DIST)
    })

    local attack_nodes = PriorityNode({
        ChaseAndAttack(self.inst, SpringCombatMod(TUNING.SPIDER_AGGRESSIVE_MAX_CHASE_TIME)),
    })
    local defensive_follow = PriorityNode({
        Follow(self.inst, function() return self.inst.components.follower.leader end, 
                TUNING.SPIDER_DEFENSIVE_MIN_FOLLOW, TUNING.SPIDER_DEFENSIVE_MED_FOLLOW, TUNING.SPIDER_DEFENSIVE_MAX_FOLLOW),  
    })

    local follow_nodes = PriorityNode({
        IfNode(function() return self.inst.components.follower.leader ~= nil end, "DefensiveFollow",
            defensive_follow),
        IfNode(function() return self.inst.components.follower.leader ~= nil end, "HasLeader",
            FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn )),
    })

    local root =
        PriorityNode(
        {
            pre_nodes,
            attack_nodes,
            follow_nodes,
            post_nodes,

        }, 1)
        
    self.bt = BT(self.inst, root)
end

return SpiderBrain