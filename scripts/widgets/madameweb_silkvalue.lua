local Badge = require "widgets/badge"
local UIAnim = require "widgets/uianim"

local madameweb_silkvalue = Class(Badge, function(self, owner, art)
    Badge._ctor(self, nil, owner,  { 255 / 255, 191 / 255, 218 / 255, 1 }, "madameweb_silkvalue")
	self.inst:ListenForEvent("madameweb_silkvalue_dirty", function(inst,data)
		self:SetPercent(self.owner.madameweb_silkvalue:value()/100, 100)
	end,self.owner)
    self:SetPercent(self.owner.madameweb_silkvalue:value()/100, 100)
end)

function madameweb_silkvalue:SetPercent(val, max)
    Badge.SetPercent(self, val, max)
end

return madameweb_silkvalue
