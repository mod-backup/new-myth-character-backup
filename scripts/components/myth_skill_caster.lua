local myth_skill_caster = Class(function(self, inst)
    self.inst = inst
end)

-- 释放技能
function myth_skill_caster:CastSkill(skill_name, pos, target)
    if IsEntityDeadOrGhost(self.inst, true) then
        return
    end
    local fn = HUA_SKILL_CONSTANTS.SKILL_CAST_FN[skill_name]
    -- 添加判定
    --print(fn)
    if fn and fn(self, self.inst, pos, target) then
        -- 释放成功
    end
end

return myth_skill_caster
