local assets =
{
    Asset("ANIM", "anim/trap.zip"),
    Asset("SOUND", "sound/common.fsb"),
}

local sounds =
{
    close = "dontstarve/common/trap_close",
    rustle = "dontstarve/common/trap_rustle",
}

local function onharvested(inst)
    inst:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    --inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    --inst.MiniMapEntity:SetIcon("rabbittrap.png")

    inst.AnimState:SetBank("spider_cocoon")
    inst.AnimState:SetBuild("madameweb_pitfall_cocoon")
    inst.AnimState:PlayAnimation("idle")

    inst.AnimState:HideSymbol("bedazzled_flare")

    inst.Transform:SetScale(0.3, 0.3, 0.3)

    inst:AddTag("trap")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.sounds = sounds

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "trap"

    inst:AddComponent("inspectable")

    inst:AddComponent("trap")
    inst.components.trap.targettag = "canbetrapped"
    inst.components.trap:SetOnHarvestFn(onharvested)
    inst.components.trap.baitsortorder = 1

    MakeHauntableLaunch(inst)

    inst:SetStateGraph("SG_cocoontrap")

    return inst
end

return Prefab("madameweb_pitfall_trap", fn, assets)
