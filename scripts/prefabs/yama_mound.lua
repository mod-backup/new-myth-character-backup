local assets =
{
    Asset("ANIM", "anim/gravestones.zip"),
}

local prefabs =
{
    "ghost",
    "amulet",
    "redgem",
    "gears",
    "bluegem",
    "nightmarefuel",
	"bat",
}

for k = 1, NUM_TRINKETS do
    table.insert(prefabs, "trinket_"..tostring(k))
end
for k = 1, NUM_HALLOWEEN_ORNAMENTS do
    table.insert(prefabs, "halloween_ornament_"..tostring(k))
end


local LOOTS =
{
    nightmarefuel = 1,
    amulet = 1,
    gears = 1,
    redgem = 5,
    bluegem = 5,
}

local function onfinishcallback(inst, worker)
    inst.AnimState:PlayAnimation("dug")
    inst:RemoveComponent("workable")
    if worker ~= nil then
        if worker.components.sanity ~= nil then
            worker.components.sanity:DoDelta(-TUNING.SANITY_SMALL)
        end
        inst.components.lootdropper:SpawnLootPrefab("spoiled_food")
    end
end

local function GetStatus(inst)
    if not inst.components.workable then
        return "DUG"
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("gravestone")
    inst.AnimState:SetBuild("gravestones")
    inst.AnimState:PlayAnimation("gravedirt")

    inst:AddTag("grave")

    inst:SetPrefabNameOverride("mound")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst:AddComponent("lootdropper")

    inst.components.workable:SetOnFinishCallback(onfinishcallback)

    inst.ghost = nil
    inst.ghost_of_a_chance = 0.1

    inst.persists = false

    return inst
end

return Prefab("yama_mound", fn, assets, prefabs)
