

local Poisonable = Class(function(self, inst)
	self.inst = inst
	self.fxlevel = 1
	self.poisoned = false
	self.duration = 20 --持续时间
	self.damageinterval = 2 --伤害间隔时间
	self.damagetime = 0 --当前时间
	self.start_time = nil	

	inst:ListenForEvent("death",function()
		if self.poisoned then
			self:DonePoisoning()
		end
	end)
end)

function Poisonable:CanBePoisoned() --给别的mod兼容用 变量或者标签都可以
	return	not (self.inst:HasTag("myth_poisonblocker") or self.inst.myth_poisonblocker) and
		self.inst.prefab ~= "wx78"
end

---开始瞎编代码
function Poisonable:Poison() --中毒咯
	if self:CanBePoisoned() and self.inst.components.health and not self.inst.components.health:IsDead() then
		if self.poisoned then
			if self.fxlevel < 5 then
				self.fxlevel = self.fxlevel + 1
				self:SpawnFX()
			end
		else
			self.poisoned = true
			self.fxlevel = 1
			self.inst:StartUpdatingComponent(self)
			self:SpawnFX()
		end
		if self.inst:HasTag("epic") then
			self.damageinterval = 12-2*self.fxlevel
		end
		self.duration = 20
		self.start_time = GetTime()
	end
end

function Poisonable:OnUpdate(dt)
	self.duration =  self.duration - dt
	self.damagetime = self.damagetime + dt
	if  self.damagetime >= self.damageinterval then
		self:DoPoison()
		self.damagetime = 0
	end
	if self.start_time ~= nil and (GetTime() - self.start_time) > 10  and (self.fxlevel > 1)  then
		self.start_time = GetTime()
		self.fxlevel = self.fxlevel - 1
		if self.inst:HasTag("epic") then
			self.damageinterval = 12-2*self.fxlevel
		end
		self:SpawnFX()
	end
	if self.duration < 0 then
		self:DonePoisoning()
	end	
end


function Poisonable:DoPoison()
	if not self.inst:IsInLimbo() then
		if self.inst.components.health and not self.inst.components.health:IsDead() then
			if self.inst:HasTag("epic") then
				local damage = math.min(500,self.inst.components.health:GetMaxWithPenalty()*0.01)
				self.inst.components.health:DoDelta(-damage,false, nil, false, nil, true)
			else
				self.inst.components.health:DoDelta(-2*self.fxlevel, false, nil, false, nil, true)
			end
		end
	end
end

function Poisonable:DonePoisoning()
	self.inst:StopUpdatingComponent(self)
	self:KillFX()
	self.poisoned = false
	self.start_time = nil
	self.damagetime = 0
	self.duration = 20
end

function Poisonable:Cure(curer)
	self:DonePoisoning()
	if curer and curer.components.finiteuses then
		curer.components.finiteuses:Use()
	elseif curer and curer.components.stackable then
		curer.components.stackable:Get(1):Remove()
	end
end

function Poisonable:SpawnFX()
	if not self.poison_fx then
		self.poison_fx = SpawnPrefab("myth_poison_fx")
		self.poison_fx.Transform:SetScale(self.inst.Transform:GetScale())
		local scale = (self.inst:HasTag("smallcreature") and 1)
		or (self.inst:HasTag("largecreature") and 3)
		or 2
		self.poison_fx:SetLevel(scale)
		self.poison_fx.entity:SetParent(self.inst.entity)
	else
		self.poison_fx.AnimState:SetBuild("myth_poisonbuild_"..self.fxlevel)
	end
end

function Poisonable:KillFX()
	if self.poison_fx then
		self.poison_fx:StopBubbles()
		self.poison_fx  = nil
	end
end

function Poisonable:OnRemoveFromEntity()
	self:KillFX()
end
--[[
function Poisonable:OnSave()    
	return 
	{
		poisoned = self.poisoned,
		poisontimeleft = self.start_time and self.duration - (GetTime() - self.start_time) or nil,
	}
end

function Poisonable:OnLoad(data)
	if data.poisoned and data.poisontimeleft then
		self:Poison(false, data.poisontimeleft)
	end
end]]

return Poisonable
