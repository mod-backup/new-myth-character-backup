require("constants")
local Text = require "widgets/text"
local TextButton = require "widgets/textbutton"
local Widget = require "widgets/widget"
local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local UIAnimButton = require "widgets/uianimbutton"

local function istbl(val) return type(val) == 'table' end
local function isstr(val) return type(val) == 'string' end
local function isnum(val) return type(val) == 'number' end

--------------------------------------------------
local MOSHI = {
	[0] = {posfn = function(w,h)
			return  Point(110 ,110,0)
		end, 
		ZhenLie = {}
	},
	[1] = {posfn = function(w,h)
			return Point(2*w -300 , h+150,0)
		end, 
		ZhenLie = {}
	},
	[2] = {posfn = function(w,h)
			return Point(2*w - 200 ,100,0)
		end, 
		ZhenLie = {}
	},
}

for i=1, 4 do
	MOSHI[0].ZhenLie[i] = Point(i * (i == 1 and 90 or  90), 0, 0)
	MOSHI[1].ZhenLie[i] = Point(0, i * (i == 1 and -90 or  -90), 0)
	local angle = math.rad(90 + ( i - 1 ) * 50)
	local range = 115
	MOSHI[2].ZhenLie[i] = Point( math.cos( angle ) * range, math.sin( angle ) * range, 0 )
end

local function IsHUDScreen()
	local defaultscreen = false
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name and type(TheFrontEnd:GetActiveScreen().name) == "string" and TheFrontEnd:GetActiveScreen().name == "HUD" then
		defaultscreen = true
	end
	return defaultscreen
end

--====角色技能部分
local function AddEventui(self,image,name,clickfn)
	local skill = ImageButton("images/skills/"..image..".xml", image..".tex")
	skill:SetHoverText(name,{
		font_size = 18,
		font = NEWFONT_OUTLINE,
		offset_x = 0,
		offset_y = 60,
	})
	skill:SetFocusScale(1.1, 1.1, 1.1)
	skill:SetOnClick(clickfn)
	self:AddUI(skill)
	return skill
end

local function yizhuqiangtian(self)
	if self.owner and self.owner.replica.inventory and (self.owner.replica.inventory:Has("mk_jgb",1) or self.owner.replica.inventory:EquipHasTag("mk_jgb")) and 
		not (self.owner._inmythcamea and self.owner._inmythcamea:value() ~= nil)  and 
		not (self.owner._inhuacamea and self.owner._inhuacamea:value() ~= nil)  and 
		not (self.owner._is_player_astral ~= nil and  self.owner._is_player_astral:value())
		then
		self.owner:HuaStartTargeting("mk_yzqt")
	end
end

local function addpigsyui(self)
	local skill = AddEventui(self,"pigsy_off",STRINGS.MYTH_SKILL_PIGBS,function()
		SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],4)
	end)
	self.handler = 
	TheInput:AddKeyDownHandler(KEY_H, function() 
		if not IsHUDScreen() then return end
		skill.onclick() 
	end)
	self.inst:ListenForEvent("becomehpigdirty",function()
		local name = self.owner._becomehpig:value() and "pigsy_on" or "pigsy_off"
		skill:SetTextures("images/skills/"..name..".xml", name..".tex")
	end,self.owner)
end

local function addskytrackui(self)
	local skill = AddEventui(self,"skytrack",STRINGS.MYTH_SKILL_HUAYING,function()
		SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],9)
	end)
end

local function addbahyui(self)
	local skill = AddEventui(self,"yama_bahy",STRINGS.NAMES.MYTH_BAHY,function()
		SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],12)
	end)
end

local function addmadaui(self)
	local skill = AddEventui(self,"madameweb_off",STRINGS.MYTH_SKILL_MABS,function()
		SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],6)
	end)
	self.handler = 
	TheInput:AddKeyDownHandler(KEY_H, function() 
		if not IsHUDScreen() then return end
		skill.onclick() 
	end)
	self.inst:ListenForEvent("beacmespiderdirty",function()
		local name = self.owner._beacmespider:value() and "madameweb_on" or "madameweb_off"
		skill:SetTextures("images/skills/"..name..".xml", name..".tex")
	end,self.owner)
end

local myth_characters = {
	monkey_king = function(self)
		local skill = AddEventui(self,"mk_hyoff",STRINGS.MYTH_SKILL_HY,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],1)
		end)
		self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			skill.onclick() 
		end)
		self.inst:ListenForEvent("monkeynightvisiondirty",function()
			local name = self.owner._mknightvision:value() and "mk_hyon" or "mk_hyoff"
			skill:SetTextures("images/skills/"..name..".xml", name..".tex")
		end,self.owner)

		--定身
		if self.owner._mkdsf:value() then
			AddEventui(self,"mk_dsf",STRINGS.NAMES.MK_DSF,function()
				SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],7)
			end)
		else
			self.inst:ListenForEvent("monkeydsfdirty",function()
				AddEventui(self,"mk_dsf",STRINGS.NAMES.MK_DSF,function()
					SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],7)
				end)
			end,self.owner)
		end
		--一柱擎天
		if self.owner._mkyzqt:value() then
			AddEventui(self,"mk_yzqt",STRINGS.MYTH_SKILL_YZQT,function()
				yizhuqiangtian(self)
			end)
		else
			self.inst:ListenForEvent("monkeydsfdirty",function()
				if self.owner._mkyzqt:value() then
					AddEventui(self,"mk_yzqt",STRINGS.MYTH_SKILL_YZQT,function()
						yizhuqiangtian(self)
					end)
				end
			end,self.owner)
		end
	end,
	--白骨
	white_bone = function(self)
		--变身
		local skill = AddEventui(self,"white_bone_off",STRINGS.MYTH_SKILL_WBBS,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],2)
		end)
		self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			skill.onclick() 
		end)
		self.inst:ListenForEvent("white_bonebeautifuldirty",function()
			local name = self.owner._isbeauty:value() and "white_bone_on" or "white_bone_off"
			skill:SetTextures("images/skills/"..name..".xml", name..".tex")
		end,self.owner)
	end,

	--八戒
	pigsy = function(self)
		if self.owner._canbecomehpig:value() then
			addpigsyui(self)
		else
			self.inst:ListenForEvent("canbecomehpigdirty",function()
				if self.owner._canbecomehpig:value() then
					addpigsyui(self)
				end
			end,self.owner)
		end
	end,

	--yangjian
	yangjian = function(self)
		--天眼
		local skill = AddEventui(self,"skyeye_off",STRINGS.MYTH_SKILL_TIANYAN,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],3)
		end)
		self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			skill.onclick() 
		end)
		self.inst:ListenForEvent("skyeye_dirty",function()
			local name = self.owner._skyeye:value() and "skyeye_on" or "skyeye_off"
			skill:SetTextures("images/skills/"..name..".xml", name..".tex")
		end,self.owner)

		--换鹰
		AddEventui(self,"yangjian_buzzard",STRINGS.NAMES.YANGJIAN_BUZZARD_SPAWN,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],8)
		end)
		if self.owner._skytrack:value() then
			addskytrackui(self)
		else
			self.inst:ListenForEvent("skytrack_dirty",function()
				if self.owner._skytrack:value() then
					addskytrackui(self)
				end
			end,self.owner)
		end
	end,
	myth_yutu = function(self)
		AddEventui(self,"yutu_daoyao",STRINGS.NAMES.YT_DAOYAO,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],10)
		end)
		AddEventui(self,"yutu_pipa",STRINGS.MYTH_SKILL_PIPA,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],11)
		end)
	end,
	yama_commissioners = function(self)
		local skill = AddEventui(self,"yama_white",STRINGS.MYTH_SKILL_YMBS,function()
			SendModRPCToServer( MOD_RPC["myth_transform"]["myth_transform"],5)
		end)
		self.handler = 
		TheInput:AddKeyDownHandler(KEY_H, function() 
			if not IsHUDScreen() then return end
			skill.onclick() 
		end)
		self.inst:ListenForEvent("yama_commissioner_dirty",function()
			local name = self.owner._commissioner_balck:value() and "yama_black" or "yama_white"
			skill:SetTextures("images/skills/"..name..".xml", name..".tex")
		end,self.owner)

		if self.owner._yama_bahy:value() then
			addbahyui(self)
		else
			self.inst:ListenForEvent("yama_bahy_dirty",function()
				if self.owner._yama_bahy:value() then
					addbahyui(self)
				end
			end,self.owner)
		end
	end,
	madameweb = function(self)
		if self.owner._canbecomespider:value() then
			addmadaui(self)
		else
			self.inst:ListenForEvent("canbecomespiderdirty",function()
				if self.owner._canbecomespider:value() then
					addmadaui(self)
				end
			end,self.owner)
		end
	end
}
---======

-------------谢谢青木大佬的帮助！！
local MYTH_SKILLUI = Class(Widget, function(self, owner,scr_w,scr_h)
	Widget._ctor(self, "MYTH_SKILLUI")
	self.owner = owner
	if not myth_characters[owner.prefab] then
		return
	end
	self.shicha = 6
	self.YanTime = true
	self.MoShi = 0
	self.scr_w = scr_w
	self.scr_h = scr_h

	self:SetPosition(110,110)
	self.ens = {}
	self.F = 0
	self.ico = self:AddChild(ImageButton("images/skills/myth_skill_button.xml", "myth_skill_button.tex"))
	self.ico.OnControl = function(_self, control, down)
		if not down then
			if control == 29 then
				self.YanTime = not self.YanTime
				if next(self.ens) then
					self:StartUpdating()
				end
			elseif control == 1 and TheInput:IsKeyDown(KEY_ALT) then
				self.MoShi = ( self.MoShi + 1 ) % ( #MOSHI + 1 )
				self:DoPaiLie()
			end
			if self.followhandler then
				self.followhandler:Remove()
				self.oldpos = nil
				self.olduipos = nil
				self.followhandler = nil
				local newpos = self:GetPosition()
				TheSim:SetPersistentString("myth_skillui",string.format("return %f,Vector3(%f,%f,%f)",self.MoShi,newpos:Get()),false)
			end
			TheFrontEnd:LockFocus(false)
		elseif down and control == 1 then
			TheFrontEnd:LockFocus(true)
			self.oldpos = TheInput:GetScreenPosition()
			self.olduipos = self:GetWorldPosition()
			if self.followhandler == nil then
				self.followhandler = TheInput:AddMoveHandler(function(x, y)
					local newpos = TheInput:GetScreenPosition()
					self:SetPosition(self.olduipos + ( newpos - self.oldpos ) )
				end)
			end
		end
	end
	self.inst:DoTaskInTime(0,function()
		TheSim:GetPersistentString("myth_skillui",function(load_success, str)
			if load_success then
				local fn = loadstring(str)
				if type(fn) == "function" then
					local moshi,pos = fn()
					self.MoShi = moshi
					self:DoPaiLie(true)
					self:SetPosition(pos:Get())
				end
			end
		end)
	end)

	self.inst:ListenForEvent("onremove", function()
		if self.handler ~= nil then
			self.handler:Remove()
		end
	end)
	self:Hide()

	myth_characters[owner.prefab](self)

	--待测试
	if self.owner.player_classified then 
		self.inst:ListenForEvent("isghostmodedirty",function()
			if self.owner.player_classified ~= nil and self.owner.player_classified.isghostmode:value() then
				self:Hide()
			elseif next(self.ens) then
				self:Show()
			end
		end,self.owner.player_classified)
	end
end)

function MYTH_SKILLUI:AddUI(target)
	if target then
		self:AddChild(target)
		table.insert(self.ens, target)
		local len = #self.ens
		local zhen = MOSHI[self.MoShi]
		if zhen and zhen.ZhenLie and zhen.ZhenLie[len] then
			target:SetPosition(zhen.ZhenLie[len])
		end
		if not self.shown then
			self:Show()
		end
	end
end

function MYTH_SKILLUI:DoPaiLie(load)
	local zhenlie = MOSHI[self.MoShi] or nil
	if zhenlie then
		self:SetPosition(zhenlie.posfn(self.scr_w,self.scr_h):Get())
		for i,v in ipairs(self.ens) do
			if zhenlie.ZhenLie[i] then
				v:SetPosition(zhenlie.ZhenLie[i])
			end
		end
		if not load then
			local newpos = self:GetPosition()
			TheSim:SetPersistentString("myth_skillui",string.format("return %f,Vector3(%f,%f,%f)",self.MoShi,newpos:Get()),false)
		end
	end
end

function MYTH_SKILLUI:OnUpdate(dt)
	if self.YanTime then
		self.F = self.F + 1
		local minsize = false
		for i,v in ipairs(self.ens) do
			local size = v:GetLooseScale()
			if ( i - 1 ) * 2 < self.F then
				if size < 1 then
					minsize = true
					if not v:IsVisible() then
						v:Show()
					end
					v:SetScale( math.min( size + dt * self.shicha, 1 ) )
				end
			else
				return
			end
		end
		if minsize then
			return
		end
		self:StopUpdating()
	else
		for i=#self.ens, 1, -1 do
			local v = self.ens[i]
			if v and v:IsVisible() then
				local size = v:GetLooseScale()
				local newsize = size - dt * self.shicha
				v:SetScale( math.max( newsize, .1 ) )
				if newsize > .1 then
					return
				else
					v:Hide()
				end
			end
		end	
		self.F = 0
		self:StopUpdating()
	end
end

return MYTH_SKILLUI