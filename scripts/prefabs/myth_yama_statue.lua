

local assets = {
    Asset("ANIM", "anim/myth_yama_statue.zip"),
    Asset("ANIM", "anim/myth_yama_statue_actions.zip"),
    Asset("ATLAS", "images/inventoryimages/myth_yama_statue1.xml"),
}

--建造数据
local construction_data = {
    {level = 1, product = "myth_yama_statue2"},
    {level = 2, product = "myth_yama_statue3"},
    {level = 3, product = "myth_yama_statue4"},
}

--建造触发
local function OnConstructed(inst, doer)
    local concluded = true
    for i, v in ipairs(CONSTRUCTION_PLANS[inst.prefab] or {}) do
        if inst.components.constructionsite:GetMaterialCount(v.type) < v.amount then
            concluded = false
            break
        end
    end
    if concluded then
        --生成咯
        local new = ReplacePrefab(inst, inst.product)
        new.AnimState:PlayAnimation("statue"..new.level.."_pre")
        new.AnimState:PushAnimation("statue"..new.level,false)
    end
end

local fires = {
    [2] = {
        {
            prefab = "yama_fire_yellow",
            offset = {0,0,0},
            symbol = "a",
        },
        {
            prefab = "yama_fire_yellow",
            offset = {0,0,0},
            symbol = "b",
        },
    },

    [3] = {
        {
            prefab = "yama_fire_yellow",
            offset = {0,0,0},
            symbol = "a",
        },
        {
            prefab = "yama_fire_yellow",
            offset = {0,0,0},
            symbol = "b",
        },
    },

    [4] = {
        {
            prefab = "yama_fire_green",
            offset = {0,0,0},
            symbol = "c",
        },
        {
            prefab = "yama_fire_green",
            offset = {0,0,0},
            symbol = "d",
        },
        {
            prefab = "yama_fire_green",
            offset = {0,0,0},
            symbol = "e",
        },
        {
            prefab = "yama_fire_green",
            offset = {0,0,0},
            symbol = "f",
        },
        {
            prefab = "yama_fire_purple",
            offset = {0,0,0},
            symbol = "g",
        },
        {
            prefab = "yama_fire_purple",
            offset = {0,0,0},
            symbol = "h",
        },
    },
}

local function ShouldAcceptItem(inst, item,doer)
    return doer and doer:HasTag("yama_commissioners") and item and item:HasTag("soul_lost")
end

local healvalue = {
    hunger = {5,5,5,15},
    sanity = {2,2,2,5},
    health = {5,5,5,8},
}

local recipes ={
    "myth_higanbana_item","myth_cqf"
}
local function OnGetItemFromPlayer(inst, giver, item)
    if giver and giver:HasTag("player")  then
        giver.components.hunger:DoDelta(healvalue.hunger[inst.level] or 0)
        giver.components.health:DoDelta(healvalue.sanity[inst.level] or 0)
        giver.components.sanity:DoDelta(healvalue.health[inst.level] or 0)
        if inst.components.constructionsite then
            inst.components.constructionsite:AddMaterial(item.prefab, 1)
        end
        if inst.level == 4 and giver.components.builder then
            for _ ,v in ipairs(recipes) do
			    if not giver.components.builder:KnowsRecipe(v) and
                    giver.components.builder:CanLearn(v) then
				    giver.components.builder:UnlockRecipe(v)
			    end
            end
        end
    end
end

local function onhit(inst)
    inst.AnimState:PlayAnimation("statue"..inst.level.."_hit")
    inst.AnimState:PushAnimation("statue"..inst.level, false)
end

local function onbuilt(inst)
	inst:Hide()
	inst:DoTaskInTime(0,function()
		local x,y,z = inst.Transform:GetWorldPosition()
		x, y, z = TheWorld.Map:GetTileCenterPoint(x, 0, z)
		inst.Transform:SetPosition(x,0, z)
        inst.AnimState:PlayAnimation("statue"..inst.level.."_pre")
        inst.AnimState:PushAnimation("statue"..inst.level,false)
		inst:Show()
	end)
end

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

local Stats = require("stats")
local function OnConstruct(self,doer, items)
	local stats = {
		prefab = self.inst.prefab,
		target = tostring(self.inst.GUID),
		recipe_items = {}
	}

    self.builder = nil
    local x, y, z = self.inst.Transform:GetWorldPosition()
    for i, v in ipairs(items) do
        local remainder = self:AddMaterial(v.prefab, v.components.stackable ~= nil and v.components.stackable:StackSize() or 1)
		table.insert(stats.recipe_items, {prefab = v.prefab, count = (v.components.stackable ~= nil and v.components.stackable:StackSize() or 1) - remainder})
        if remainder > 0 then
            if v.components.stackable ~= nil then
                v.components.stackable:SetStackSize(math.min(remainder, v.components.stackable.maxsize))
            end
            v.components.inventoryitem:RemoveFromOwner(true)
            v.components.inventoryitem:DoDropPhysics(x, y, z, true)
            if v:HasTag("soul_yama") then
                v:PushEvent("ondropped")
            end
        else
            v:Remove()
        end
    end

	stats.victory = self:IsComplete()
	Stats.PushMetricsEvent("constructionsite", doer, stats)

    if self.onconstructedfn ~= nil then
        self.onconstructedfn(self.inst, doer)
    end
end

local droploot = {
    [2] = {"boards","charcoal","charcoal","charcoal","beeswax","goldnugget","goldnugget","goldnugget","goldnugget","goldnugget"},
    [3] = {"boards","boards","boards","livinglog","livinglog","livinglog","livinglog","livinglog","livinglog","marble","marble","driftwood_log","driftwood_log"},
    [4] = {"foliage","foliage","foliage","foliage","foliage","foliage","foliage","foliage","dug_marsh_bush","dug_marsh_bush","dug_marsh_bush",},
}

local needhide = {"a","b","c","d","e","f","g","h","fire"}

local restrainrange ={
    16,24,32,40
}
local function MakeLab(id,postinit)
    local name = "statue"..id

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        --inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank("myth_yama_statue")
        inst.AnimState:SetBuild("myth_yama_statue")
        inst.AnimState:PlayAnimation(name)

        inst.MiniMapEntity:SetIcon("myth_yama_statue1.tex")
        for _ , v in ipairs(needhide) do
            inst.AnimState:HideSymbol(v)
        end

        MakeObstaclePhysics(inst, 1)

        inst:AddTag("myth_yama_statue")

        inst:AddTag("soullostbox")

        inst.onusesgname = "give"

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst.level = id
        inst.restrainrange = restrainrange[inst.level] * restrainrange[inst.level]

        inst:AddComponent("inspectable")
        if id < 4 then
            inst.product = construction_data[id]["product"]
            inst:AddComponent("constructionsite")
            inst.components.constructionsite:SetConstructionPrefab("construction_container")
            inst.components.constructionsite:SetOnConstructedFn(OnConstructed)
            inst.components.constructionsite.OnConstruct = OnConstruct
        end

        if  fires[id] ~= nil then
            for _, v in ipairs(fires[id]) do
                local fx = SpawnPrefab(v.prefab)
                fx.entity:SetParent(inst.entity)
                fx.entity:AddFollower()
                fx.Follower:FollowSymbol(inst.GUID, v.symbol, v.offset[1], v.offset[2], 0)           
            end
        end

        inst:AddComponent("trader")
        inst.components.trader:SetAcceptTest(ShouldAcceptItem)
        inst.components.trader.onaccept = OnGetItemFromPlayer

        inst:AddComponent("lootdropper")
        if droploot[inst.level]  then
            inst.components.lootdropper:SetLoot(droploot[inst.level])
        end
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(3)
        inst.components.workable:SetOnFinishCallback(onhammered)
        inst.components.workable:SetOnWorkCallback(onhit)

        TheWorld.components.yama_statuemanager:AddMember(inst)

        if id == 1 then
            inst:ListenForEvent("onbuilt", onbuilt)
        elseif id == 4 then --无常新修改
            inst:AddComponent("sanityaura")
            inst.components.sanityaura.max_distsq = 144 --12的范围
            inst.components.sanityaura.aurafn = function(inst, observer)
                if observer and observer:HasTag("yama_commissioners") then
                    return 2.45 --147/60 /30*60*2 = 9.8/min
                end
                return 0
            end
        end
        return inst
    end

    return Prefab("myth_yama_"..name, fn, assets)
end

local ret = {}
for i = 1, 4 do
    table.insert(ret, MakeLab(i))
end

local function placerfn(inst)
    inst.components.placer.snap_to_tile = true

	inst.outline = SpawnPrefab("tile_outline")
	inst.outline.entity:SetParent(inst.entity)

	inst.components.placer:LinkEntity(inst.outline)
end

table.insert(ret, MakePlacer("myth_yama_statue1_placer", "myth_yama_statue", "myth_yama_statue", "statue1",nil,nil,nil,nil,nil,nil,placerfn))
return unpack(ret)