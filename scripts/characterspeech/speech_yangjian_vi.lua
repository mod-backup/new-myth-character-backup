return
{
	ACTIONFAIL =
	{
        REPAIR =
        {
            WRONGPIECE = "Yêu nghiệt ngu ngốc nhà ngươi.",		--化石骨架拼接错误
        },
        BUILD =
        {
            MOUNTED = "Lầu cao từ đất bằng, làm móng cần phải vững.",		--建造失败（骑乘状态）
        },
		SHAVE =
		{
			AWAKEBEEFALO = "Lúc này mà cạo, thì thật là ngu ngốc",		--给醒着的牛刮毛
			GENERIC = "Ta sẽ không để hắn cạo lông!",		--刮牛毛失败
			NOBITS = "Không có gì để cạo!",		--给没毛的牛刮毛
		},
		STORE =
		{
			GENERIC = "Đầy rồi.",		--存放东西失败
			NOTALLOWED = "Món đồ này không bỏ vào được.",		--存放东西--不被允许
			INUSE = "Ta có thể đợi.",		--暂无注释
		},
        CONSTRUCT =
        {
            INUSE = "Nhị Lang có thể đợi đến phiên mình.",		--建筑正在使用
            NOTALLOWED = "Không phù hợp.",		--建筑不允许使用
            EMPTY = "Ta cần một ít nguyên liệu.",		--建筑空了
            MISMATCH = "Ôi chao, kế hoạch thất bại rồi.",		--升级套件错误（目前用不到）
        },
		RUMMAGE =
		{	
			GENERIC = "Ta không thể làm như vậy.",		--搜索失败？
			INUSE = "Bọn họ đang vùi đầu tìm kiếm trong đống rác đó.",		--暂无注释
		},
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "Chìa khóa không đúng với ổ khóa.",		--使用克劳斯钥匙
        	KLAUS = "Ta hơi bận!!!",		--克劳斯
        },
		ACTIVATE = 
		{
			LOCKED_GATE = "Cánh cổng đen bị khóa rồi.",		--远古钥匙
		},
        COOK =
        {
            GENERIC = "Nấu cơm còn khó hơn làm phép.",		--做饭失败
            INUSE = "Khói lửa nhân gian.",		--暂无注释
            TOOFAR = "Quân tử không chui vào bếp!",		--做饭失败-太远
        },
        GIVE =
        {
            GENERIC = "Đem thứ này cho đi sao?",		--暂无注释
            DEAD = "Có sống thì mới có chết.",		--给予 -目标死亡
            SLEEPING = "Bất tỉnh nhân sự, không thèm quan tâm.",		--给予--目标睡觉
            BUSY = "Đợi lát rồi thử lại.",		--给予--目标正忙
            ABIGAILHEART = "Đáng thử một lần.",		--暂无注释
            GHOSTHEART = "Có lẽ, đây không phải ý hay.",		--暂无注释
            NOTGEM = "Ta sẽ không gắn món đồ này lên!",		--暂无注释
            WRONGGEM = "Ngọc không có tác dụng.",		--暂无注释
            NOTSTAFF = "Hình dạng có vẻ không đúng.",		--暂无注释
            MUSHROOMFARM_NEEDSSHROOM = "Một cây nấm có thể có nhiều tác dụng.",		--暂无注释
            MUSHROOMFARM_NEEDSLOG = "Một khúc gỗ sống có thể có nhiều tác dụng.",		--暂无注释
            SLOTFULL = "Chúng ta để một số thứ ở đây.",		--暂无注释
            FOODFULL = "Ở đây đã đầy đồ ăn rồi.",		--暂无注释
            NOTDISH = "Chắc chắn nó không muốn ăn.",		--暂无注释
            DUPLICATE = "Cái này có rồi.",		--暂无注释
            NOTSCULPTABLE = "Cho dù là ta cũng không thể biến thứ đó thành tượng được.",		--暂无注释
            NOTATRIUMKEY = "Hình dạng có vẻ không đúng.",		--暂无注释
            CANTSHADOWREVIVE = "Nó sẽ không hồi sinh.",		--暂无注释
            WRONGSHADOWFORM = "Ráp sai.",		--暂无注释
            NOMOON = "Ta muốn nhìn thấy mặt trăng.",		--暂无注释
			PIGKINGGAME_MESSY = "Ta phải xử lý chút.",		--暂无注释
			PIGKINGGAME_DANGER = "Hiện tại đang nguy hiểm.",		--暂无注释
			PIGKINGGAME_TOOLATE = "Đã quá muộn rồi.",		--暂无注释
        },
        GIVETOPLAYER =
        {
            FULL = "Ngươi cần tìm đồ chứa!",		--给玩家一个东西 -但是背包满了
            DEAD = "Có sống thì mới có chết..",		--给死亡的玩家一个东西
            SLEEPING = "Bất tỉnh nhân sự, hổng thèm quan tâm.",		--给睡觉的玩家一个东西
            BUSY = "Đợi chút rồi thử lại.",		--给忙碌的玩家一个东西
        },
        GIVEALLTOPLAYER =
        {
            FULL = "Ngươi cần tìm đồ chứa!",		--给人一组东西 但是背包满了
            DEAD = "Có sống thì mới có chết.",		--给于死去的玩家一组物品
            SLEEPING = "Bất tỉnh nhân sự rồi.",		--给于正在睡觉的玩家一组物品
            BUSY = "Đợi chút rồi thử lại.",		--给于正在忙碌的玩家一组物品
        },
        WRITE =
        {
            GENERIC = "Ta cảm thấy nó hiện giờ rất tốt.",		--鞋子失败
            INUSE = "Để hắn viết đi.",		--暂无注释
        },
        DRAW =
        {
            NOIMAGE = "Nếu ta có món đồ đó, thì dễ rồi.",		--画图缺乏图像
        },
        CHANGEIN =
        {
            GENERIC = "Hiện ta không muốn thay.",		--换装失败 
            BURNING = "Hiện giờ rất nguy hiểm!",		--换装失败-着火了
            INUSE = "Một lần chỉ có thể thay một kiểu.",		--暂无注释
        },
        ATTUNE =
        {
            NOHEALTH = "Không đủ máu, không thể tạo.",		--制造肉雕像血量不足
        },
        MOUNT =
        {
            TARGETINCOMBAT = "Con trâu điên kia!",		--骑乘，牛正在战斗
            INUSE = "Quân tử không lấy đồ người khác!",		--骑乘（牛被占据）
        },
        SADDLE =
        {
            TARGETINCOMBAT = "Trấn an nó trước.",		--给战斗状态的牛上鞍
        },
        TEACH =
        {
            KNOWN = "Cái này ta biết rồi.",		--学习已经知道的蓝图
            CANTLEARN = "Cái này không học được.",		--学习无法学习的蓝图
            WRONGWORLD = "Bản này không dùng được.",		--暂无注释
        },
        WRAPBUNDLE =
        {
            EMPTY = "Có thể gói ghém hành lý.",		--打包纸是空的
        },
        PICKUP =
        {
			RESTRICTION = "Kỹ năng chưa thuần thục, không dùng được.",		--暂无注释
			INUSE = "Ta nên đợi đến phiên.",		--暂无注释
        },
        SLAUGHTER =
        {
            TOOFAR = "Nó chạy mất rồi.",		--屠杀？？ 因为太远而失败
        },
        REPLATE =
        {
            MISMATCH = "Nó cần cái đĩa khác.", 		--暂无注释
            SAMEDISH = "Ta chỉ cần một cái đĩa.", 		--暂无注释
        },
	},
	ACTIONFAIL_GENERIC = "Dương Tiễn không thể làm được.",		--动作失败
	ANNOUNCE_DIG_DISEASE_WARNING = "Trông nó có vẻ đỡ hơn rồi.",		--挖起生病的植物
	ANNOUNCE_PICK_DISEASE_WARNING = "Nó bệnh rồi.",		--（植物生病）
	ANNOUNCE_ADVENTUREFAIL = "Lần này không thuận lợi lắm. Ta phải thử lần nữa.",		--暂无注释
    ANNOUNCE_MOUNT_LOWHEALTH = "Hình như con thú hoang đó bị thương rồi.",		--牛血量过低
	ANNOUNCE_BEES = "Tránh xa ta chút!!",		--戴养蜂帽被蜜蜂蛰
	ANNOUNCE_BOOMERANG = "Món đồ chơi!",		--回旋镖
	ANNOUNCE_CHARLIE = "Ta muốn xem ngươi còn những thủ đoạn hèn hạ nào nữa!",		--查理即将攻击
	ANNOUNCE_CHARLIE_ATTACK = "Đánh lén người khác, không có chút bản lĩnh nào!",		--被查理攻击
	ANNOUNCE_COLD = "Gió lạnh vào người!",		--过冷
	ANNOUNCE_HOT = "Nóng không chịu nổi, nóng như cái lò!",		--过热
	ANNOUNCE_CRAFTING_FAIL = "Thiết một số thành phần quan trọng.",		--暂无注释
	ANNOUNCE_DEERCLOPS = "Nghe có vẻ là tên to xác nào đó!",		--boss来袭
	ANNOUNCE_CAVEIN = "Cả vùng đất đang run rẩy!",		--要地震了？？？
	ANNOUNCE_ANTLION_SINKHOLE = 
	{
		"Mặt đất đang lắc lư!",		--蚁狮地震
		"Động đất!",		--蚁狮地震
		"Thổ địa nơi đây đâu rồi!",		--蚁狮地震
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "Để nó yên tĩnh đã.",		--向蚁狮致敬
        "Cho ngươi cống phẩm, ngồi yên đó cho ta.",		--给蚁狮上供
        "Nó sẽ tạm thời yên tĩnh một chút...",		--给蚁狮上供
	},
	ANNOUNCE_SACREDCHEST_YES = "Ta cảm thấy ta xứng đáng.",		--远古宝箱物品正确给出蓝图
	ANNOUNCE_SACREDCHEST_NO = "Cái hộp chứa bảo bối.",		--远古宝箱
	ANNOUNCE_DUSK = "Ánh chiều tà thật đẹp, chỉ tiếc sắp hoàng hôn..",		--进入黄昏
	ANNOUNCE_EAT =
	{
		GENERIC = "Món ngon!",		--吃东西
		PAINFUL = "Nhạt như nước ốc.",		--吃怪物肉
		SPOILED = "Đừng hòng lấy đó ra để xỉ nhục Dương Tiễn!",		--吃腐烂食物
		STALE = "Con người không thể trường sinh, đồ vật sao để lâu được.",		--吃黄色食物
		INVALID = "Chẳng lẽ ngươi muốn ta nuốc vào?",		--拒吃
		YUCKY = "Đến Hao Thiên Khuyển cũng không muốn ăn.",		--吃红色食物
	},
    ANNOUNCE_ENCUMBERED =
    {
        "Thở dốc... phù phù...",		--搬运雕像、可疑的大理石
        "Ta nên thuê... lực sĩ dời núi...",		--搬运雕像、可疑的大理石
        "Nâng lên... dùng tâm của ngươi...",		--搬运雕像、可疑的大理石
        "Đây không phải là... công việc... của ta...",		--搬运雕像、可疑的大理石
        "Vì... mệnh lệnh!",		--搬运雕像、可疑的大理石
        "Cái thú này... khiến quần áo ta rách rưới.",		--搬运雕像、可疑的大理石
        "Hứ...!",		--搬运雕像、可疑的大理石
        "Thiên mệnh khó trái, giống như dời núi...",		--搬运雕像、可疑的大理石
        "Mẫu thân... muội muội...",		--搬运雕像、可疑的大理石
    },
    ANNOUNCE_ATRIUM_DESTABILIZING = 
    {
		"Hồi kết của vạn vật!",		--中庭击杀boss后即将刷新
		"Kỷ nguyên mới sắp bắt đầu!",		--中庭震动
		"Luân hồi sẽ dừng.",		--中庭击杀boss后即将刷新
	},
    ANNOUNCE_RUINS_RESET = "Thú rừng quay về rồi!",		--克劳斯复活
    ANNOUNCE_SNARED = "Xương yêu tà!!",		--远古嘤嘤怪的骨笼
    ANNOUNCE_REPELLED = "Bóng tối bảo vệ!",		--嘤嘤怪保护状态
	ANNOUNCE_ENTER_DARK = "Bóng tối quấn thân, pháp nhãn mở rộng!",		--进入黑暗
	ANNOUNCE_ENTER_LIGHT = "Dưới ánh hào quang, tà ác biến mất!",		--进入光源
	ANNOUNCE_FREEDOM = "Ta tự do rồi!",		--暂无注释
	ANNOUNCE_HIGHRESEARCH = "Ta cảm thấy ta thật thông minh!",		--暂无注释
	ANNOUNCE_HOUNDS = "Hao Thiên, mấy cái thứ này giao cho ngươi đó.",		--猎犬将至
	ANNOUNCE_WORMS = "Đói cồn cào?",		--蠕虫袭击
	ANNOUNCE_HUNGRY = "Đáng ghét! Thuật Nhịn Ăn hết hiệu quả rồi.",		--饥饿
	ANNOUNCE_HUNT_BEAST_NEARBY = "Thời thiếu niên săn được hổ, bỏ lên ngựa ung dung quay về..",		--即将找到野兽
	ANNOUNCE_HUNT_LOST_TRAIL = "Hao Thiên sao ngươi để mất dấu rồi.",		--猎物（大象脚印丢失）
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "Loại đất ẩm này không có dấu chân.",		--大猎物丢失踪迹
	ANNOUNCE_INV_FULL = "Không thể chế đồ sao?",		--身上的物品满了
	ANNOUNCE_KNOCKEDOUT = "Ôi, cái đầu của tôi!",		--被催眠
	ANNOUNCE_LOWRESEARCH = "Ta chẳng học được gì từ đó hết.",		--暂无注释
	ANNOUNCE_MOSQUITOS = "A! Tránh ra!",		--未知
    ANNOUNCE_NOWARDROBEONFIRE = "Tủ đồ cháy rồi!",		--橱柜着火
    ANNOUNCE_NODANGERGIFT = "Thời điểm hiện tại không thích hợp để mở quà!",		--周围有危险的情况下打开礼物
    ANNOUNCE_NOMOUNTEDGIFT = "Ta phải xuống bò đã.",		--骑牛的时候打开礼物
	ANNOUNCE_NODANGERSLEEP = "Đang chiếu đấu nào dám nghỉ ngơi!",		--危险，不能睡觉
	ANNOUNCE_NODAYSLEEP = "Thời điểm tốt, không được lãng phí.",		--白天睡帐篷
	ANNOUNCE_NODAYSLEEP_CAVE = "Ta không mệt.",		--洞穴里白天睡帐篷
	ANNOUNCE_NOHUNGERSLEEP = "Đói đến không ngủ được!",		--饿了无法睡觉
	ANNOUNCE_NOSLEEPONFIRE = "Bây giờ mà vào thì chỉ có chết.",		--营帐着火无法睡觉
	ANNOUNCE_NODANGERSIESTA = "Bây giờ nghỉ trưa thì quá nguy hiểm!",		--危险，不能午睡
	ANNOUNCE_NONIGHTSIESTA = "Ngủ ở đây sẽ bị lạnh đó.",		--夜晚睡凉棚
	ANNOUNCE_NONIGHTSIESTA_CAVE = "Ta không cho rằng ta có thể thoải mái nghỉ ngơi ở đây",		--在洞穴里夜晚睡凉棚
	ANNOUNCE_NOHUNGERSIESTA = "Ta đói quá rồi, không thể nghỉ ngơi!",		--饱度不足无法午睡
	ANNOUNCE_NODANGERAFK = "Giờ không phải là lúc chạy trốn!",		--战斗状态下线（已经移除）
	ANNOUNCE_NO_TRAP = "Được thôi, cũng dễ mà.",		--没有陷阱？？？
	ANNOUNCE_PECKED = "Lòng lang dạ sói!",		--被小高鸟啄
	ANNOUNCE_QUAKE = "Nghe có vẻ không ổn.",		--地震
	ANNOUNCE_RESEARCH = "Sống đến già học đến chết!",		--暂无注释
	ANNOUNCE_SHELTER = "Gió thổi không lạnh dưới cây liễu!",		--下雨天躲树下
	ANNOUNCE_THORNS = "Hái hoa hồng!",		--玫瑰扎手
	ANNOUNCE_BURNT = "Đốt thành tro bụi!",		--烧完了
	ANNOUNCE_TORCH_OUT = "Tắt lửa rồi!",		--火把用完了
	ANNOUNCE_THURIBLE_OUT = "Gió cuốn mây tan đốt sạch sẽ.",		--香炉燃尽
	ANNOUNCE_FAN_OUT = "Quạt của ta bay theo gió rồi.",		--小风车停了
    ANNOUNCE_COMPASS_OUT = "La bàn dùng hết trong một ngày.",		--指南针用完了
	ANNOUNCE_TRAP_WENT_OFF = "Ôi chao.",		--触发陷阱（例如冬季陷阱）
	ANNOUNCE_UNIMPLEMENTED = "Ồ! Cái này vẫn chưa được.",		--暂无注释
	ANNOUNCE_WORMHOLE = "Không vào hang cọp, sau bắt được cọp.",		--跳虫洞
	ANNOUNCE_TOWNPORTALTELEPORT = "Ta không chắc đó có phải là pháp lực hay không.",		--豪华传送
	ANNOUNCE_CANFIX = "Ta nghĩ ta có thể sửa nó!",
		--墙壁可以修理
	ANNOUNCE_ACCOMPLISHMENT = "Ta cảm  nhận được sự thành công!",		--暂无注释
	ANNOUNCE_ACCOMPLISHMENT_DONE = "Nếu bây giờ bạn của ta nhìn thấy ta như vậy thì tốt rồi...",			--暂无注释
	ANNOUNCE_INSUFFICIENTFERTILIZER = "Cho thêm ít đất chét tường.",		--土地肥力不足
	ANNOUNCE_TOOL_SLIP = "Oh, món đồ đó quá trơn!",		--工具滑出
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "Âm thanh như tách sông, sấm chớp như ập xuống!",		--规避闪电
	ANNOUNCE_TOADESCAPING = "Con cóc đang chạy trốn.",		--蟾蜍正在逃跑
	ANNOUNCE_TOADESCAPED = "Con cóc trốn mất rồi.",		--蟾蜍逃走了
	ANNOUNCE_DAMP = "Nước mưa dần dần thấm vào áo.",		--潮湿（1级）
	ANNOUNCE_WET = "Không sao cứ huýt sáo mà đi.",		--潮湿（2级）
	ANNOUNCE_WETTER = "Đêm qua nhìn mây không hiểu ý, hôm nay trời mưa ướt cả người!",		--潮湿（3级）
	ANNOUNCE_SOAKED = "Mây đen từng tầng đổ cơn mưa, sóng đánh liên hồi tạo mây khói.",		--潮湿（4级）
    ANNOUNCE_DESPAWN = "Ta có thể thấy ánh sáng!",		--下线
	ANNOUNCE_BECOMEGHOST = "Âm dương cách biệt!",		--变成鬼魂
	ANNOUNCE_GHOSTDRAIN = "Tâm trạng của ta sắp tụt hết rồi...",		--队友死亡掉san
	ANNOUNCE_PETRIFED_TREES = "Sinh mạng của cây sắp hết.",		--石化树
	ANNOUNCE_KLAUS_ENRAGE = "Nào, dùng cơn giận mà đối mặt ta!",		--克劳斯被激怒（杀死了鹿）
	ANNOUNCE_KLAUS_UNCHAINED = "Dây xích của nó được tháo rồi!",		--克劳斯-未上锁的  猜测是死亡准备变身？
	ANNOUNCE_KLAUS_CALLFORHELP = "Nó đang cầu cứu!",		--克劳斯召唤小偷
    ANNOUNCE_SPOOKED = "Ngươi nhìn thấy không?!",		--被吓到
	ANNOUNCE_BRAVERY_POTION = "Đám cây kia trông không có gì đáng sợ.",		--勇气药剂
    ANNOUNCE_REVIVING_CORPSE = "Để ta giúp ngươi.",		--暂无注释
    ANNOUNCE_REVIVED_OTHER_CORPSE = "Rực rỡ hẳng lên!",		--暂无注释
    ANNOUNCE_REVIVED_FROM_CORPSE = "Đỡ nhiều rồi, cảm ơn nhe.",		--暂无注释
    ANNOUNCE_ROYALTY =
    {
        "Bệ hạ.",		--向带着蜂王帽的队友鞠躬
        "Điện hạ.",		--向带着蜂王帽的队友鞠躬
        "Quân chủ của tôi!",		--向带着蜂王帽的队友鞠躬
    },
	BATTLECRY =
	{
		GENERIC = "Ý chí chiến đấu mãnh liệt.",		--战斗
		PIG = "Trư Tinh nhỏ nhoi, cũng dám khiêu khích thần tiên!",		--战斗 猪人
		PREY = "Vung kiếm là chết!",		--战斗 猎物？？大象？
		SPIDER = "Chưa hóa thành hình, lại dám hại người!",		--战斗 蜘蛛
		SPIDER_WARRIOR = "Cũng có chút bản lĩnh!",		--战斗 蜘蛛战士
		DEER = "Thịt nai tươi ngon!",		--暂无注释
	},
	COMBAT_QUIT =
	{
		GENERIC = "Chắc chắn hắn nếm được sự lợi hại của ta rồi!",		--攻击目标被卡住，无法攻击
		PIG = "Lần này tạm thời tha cho hắn đi.",		--退出战斗-猪人
		PREY = "Trốn lẹ vậy!",		--退出战斗 猎物？？大象？
		SPIDER = "Con vật tà ác.",		-- 退出战斗 蜘蛛
		SPIDER_WARRIOR = "Tha cho ngươi một mạng đó!",		--退出战斗 蜘蛛战士
	},
	DESCRIBE =
	{
		MULTIPLAYER_PORTAL = "Con mắt kỳ lạ.",		-- 物品名:"绚丽之门"
        MULTIPLAYER_PORTAL_MOONROCK = "Vượt qua ngân hà tiến lên phía trước.",		-- 物品名:"天体传送门"
        MOONROCKIDOL = "Chìa khóa cửa.",		-- 物品名:"月岩雕像" 制造描述:"重要人物。"
        CONSTRUCTION_PLANS = "Không biết đây là đâu.",		-- 物品名:未找到
        ANTLION =
        {
            GENERIC = "Trên người ta có thứ mà nó muốn.",		-- 物品名:"蚁狮"->默认
            VERYHAPPY = "Xem ra mối quan hệ giữa ta với nó không tệ.",		-- 物品名:"蚁狮"
            UNHAPPY = "Trông có vẻ đang tức giận.",		-- 物品名:"蚁狮"
        },
        ANTLIONTRINKET = "Sẽ có ai đó hứng thú với nó.",		-- 物品名:"沙滩玩具"
        SANDSPIKE = "Ta lẽ ra sẽ bị đâm xuyên!",		-- 物品名:"沙刺"
        SANDBLOCK = "Đúng là kiên cố!",		-- 物品名:"沙堡"
        GLASSSPIKE = "Ta không còn nhớ lúc bị đâm xuyên.",		-- 物品名:"玻璃狼牙棒"
        GLASSBLOCK = "Đồ của đứa trẻ.",		-- 物品名:"玻璃城堡"
        ABIGAIL_FLOWER =
        {
            GENERIC ="Quá đẹp, khiến người ta lưu luyến khó quên.",		-- 物品名:"阿比盖尔之花"->默认 制造描述:"一个神奇的纪念品。"
            LONG = "Lúc nhìn thứ đó thì nguyên thần của ta bị thương rồi.",		-- 物品名:"阿比盖尔之花"->还需要很久 制造描述:"一个神奇的纪念品。"
            MEDIUM = "Nó khiến ta sởn tóc gáy.",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            SOON = "Đóa hoa này có một câu chuyện!",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            HAUNTED_POCKET = "Ta nên bỏ nó.",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
            HAUNTED_GROUND = "Ta sẽ tìm hiểu rốt cuộc nó đã làm những gì.",		-- 物品名:"阿比盖尔之花" 制造描述:"一个神奇的纪念品。"
        },
        BALLOONS_EMPTY = "Trông như tiền người hề.",		-- 物品名:"韦斯的气球" 制造描述:"要是有更简单的方法该多好。"
        BALLOON = "Làm sao chúng nó lơ lửng được nhỉ?",		-- 物品名:"气球" 制造描述:"一个神奇的纪念品。"
        BERNIE_INACTIVE =
        {
            BROKEN = "Nó rốt cuộc cũng đất vỡ ngói bể.",		-- 物品名:"伯尼" 制造描述:"这个疯狂的世界总有你熟悉的人。"
            GENERIC = "Nó đã bị cháy rụi.",		-- 物品名:"伯尼"->默认 制造描述:"这个疯狂的世界总有你熟悉的人。"
        },
        BERNIE_ACTIVE = "Con gấu bông quanh quẩn quanh đây.",		-- 物品名:"伯尼" 制造描述:"这个疯狂的世界总有你熟悉的人。"
        BERNIE_BIG = "Tuyệt đối đừng đụng vào Willow.",		-- 物品名:"伯尼！" 制造描述:"这个疯狂的世界总有你熟悉的人。"
        BOOK_BIRDS = "Một quyển sách chim.",		-- 物品名:"世界鸟类手册" 制造描述:"涵盖1000个物种：习性、栖息地及叫声。"
        BOOK_TENTACLES = "Có người sẽ bị dụ đọc quyển sách này.",		-- 物品名:"触手的召唤" 制造描述:"让我们来了解一下地下的朋友们！"
        BOOK_GARDENING = "Đọc cả nửa ngày cũng không thấy sự sinh trưởng của cây.",		-- 物品名:"应用园艺学" 制造描述:"讲述培育和照料植物的相关知识。"
        BOOK_SLEEP = "Kỳ lạ, đây chỉ là mã điện báo 500 trang.",		-- 物品名:"睡前故事" 制造描述:"送你入梦的睡前故事。"
        BOOK_BRIMSTONE = "Thế giới sẽ bị hủy diệt.",		-- 物品名:"末日将至！" 制造描述:"世界将在火焰和灾难中终结！"
        PLAYER =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:未找到
            ATTACKER = "Trông %s rất giỏi biến...",		-- 物品名:未找到
            MURDERER = "Kẻ giết người!",		-- 物品名:未找到
            REVIVER = "%s, bạn của hồn ma.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s có thể dùng một trái tim.",		-- 物品名:"幽灵"
            FIRESTARTER = "Đốt nó cũng không thể giải quyết vấn đề, %s.",		-- 物品名:未找到
        },
        WILSON =
        {
            GENERIC = "Ngươi là người tốt.",		-- 物品名:"威尔逊"->默认
            ATTACKER = "Đúng vậy. Trông ta đáng sợ vậy sao?",		-- 物品名:"威尔逊"
            MURDERER = "Sự tồn tại của ngươi đã phạm phải thiên quy, %s!",		-- 物品名:"威尔逊"
            REVIVER = "%s đã thực hiện lý tưởng của chúng ta một cách chuyên nghiệp.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Tốt nhất chuẩn bị sẵn hồi sinh. Đừng để người ta bay lơ lửng như vậy.",		-- 物品名:"幽灵"
            FIRESTARTER = "Đốt nó cũng không thể giải quyết vấn đề, %s.",		-- 物品名:"威尔逊"
        },
        WOLFGANG =
        {
            GENERIC = "Rất vui gặp được bạn, %s!",		-- 物品名:"沃尔夫冈"->默认
            ATTACKER = "Đừng khiêu chiến với kẻ mạnh...",		-- 物品名:"沃尔夫冈"
            MURDERER = "Kẻ giết người! Ta sẽ bắt được ngươi!",		-- 物品名:"沃尔夫冈"
            REVIVER = "%s chỉ là một con gấu nhồi bông to lớn.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Ta đã nói với ngươi đừng cố kéo cục đá đó.",		-- 物品名:"幽灵"
            FIRESTARTER = "Trên thực tế ngươi không thể dập lửa, %s!",		-- 物品名:"沃尔夫冈"
        },
        WAXWELL =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"麦斯威尔"->默认
            ATTACKER = "Hình như ngươi nói chuyện bị líu lưỡi rồi.",		-- 物品名:"麦斯威尔"
            MURDERER = "Ta sẽ dạy ngươi logic và suy luận... Đây là điểm mạnh của ta!",		-- 物品名:"麦斯威尔"
            REVIVER = "%s dùng năng lực của mình vào việc chính nghĩa.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Đừng nhìn tôi như vậy, %s! Tôi đang cố gắng đây!",		-- 物品名:"幽灵"
            FIRESTARTER = "%s chỉ xin lửa.",		-- 物品名:"麦斯威尔"
        },
        WX78 =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"WX-78"->默认
            ATTACKER = "%s, ta nghĩ chúng ta phải xử lý mệnh lệnh quan trọng của ngươi...",		-- 物品名:"WX-78"
            MURDERER = "%s! Ngươi đã phạm luật rồi!",		-- 物品名:"WX-78"
            REVIVER = "Ta đoán là %s khiến cho module đồng cảm khởi động và vận hành.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Ta luôn nghĩ %s sẽ có tim. Bây giờ thì ta chắc chắn rồi!",		-- 物品名:"幽灵"
            FIRESTARTER = "%s! Trông ngươi có vẻ sắp tan chảy rồi. Xảy ra chuyện gì vậy?",		-- 物品名:"WX-78"
        },
        WILLOW =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"Willow"->默认
            ATTACKER = "%s trói chặt cái đứa dùng bật lửa đó...",		-- 物品名:"Willow"
            MURDERER = "Kẻ giết người! Kẻ phóng hỏa!",		-- 物品名:"Willow"
            REVIVER = "%s, bạn của hồn ma.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s, ta dám khẳng định ngươi mong có một trái tim.",		-- 物品名:"幽灵"
            FIRESTARTER = "Lại nào?",		-- 物品名:"Willow"
        },
        WENDY =
        {
            GENERIC = "Chào bạn, %s!",		-- 物品名:"温蒂"->默认
            ATTACKER = "%s không có vật nhọn, có không?",		-- 物品名:"温蒂"
            MURDERER = "Kẻ giết người! ",		-- 物品名:"温蒂"
            REVIVER = "%s xem hồn ma là người nhà.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Ta nhìn thấy 2 người! Tốt nhất ta tạo một trái tim cho %s.",		-- 物品名:"幽灵"
            FIRESTARTER = "Ta biết là ngươi châm lửa, %s.",		-- 物品名:"温蒂"
        },
        WOODIE =
        {
            GENERIC = "Chào bạn, %s!",		-- 物品名:"伍迪"->默认
            ATTACKER = "Dạo này %s có chút sức sống...",		-- 物品名:"伍迪"
            MURDERER = "Hung thủ! Cầm lấy cây rìu, chúng ta chặt nó!",		-- 物品名:"伍迪"
            REVIVER = "%s đã cứu thịt xông khói của mọi người.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s, “vũ trụ” bao gồm hư không phải không?",		-- 物品名:"幽灵"
            BEAVER = "%s chặt cây điên cuồng, không dừng lại được!",		-- 物品名:"伍迪"
            BEAVERGHOST = "%s, nếu ta không hồi sinh ngươi, thì ngươi có giận không?",		-- 物品名:"伍迪"
            FIRESTARTER = "%s, đừng tự đốt bản thân.",		-- 物品名:"伍迪"
        },
        WICKERBOTTOM =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"薇克巴顿"->默认
            ATTACKER = "%s chuẩn bị cầm sách ném vào ta.",		-- 物品名:"薇克巴顿"
            MURDERER = "Người phê bình đến rồi!",		-- 物品名:"薇克巴顿"
            REVIVER = "Ta rất quý trọng nguyên lý của %s.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Hình như không đúng lắm, không phải sao, %s?",		-- 物品名:"幽灵"
            FIRESTARTER = "Ta tin ngươi phải có lý do mới châm lửa.",		-- 物品名:"薇克巴顿"
        },
        WES =
        {
            GENERIC = "Chào bạn, %s!",		-- 物品名:"韦斯"->默认
            ATTACKER = "%s im lặng...",		-- 物品名:"韦斯"
            MURDERER = "Dùng kịch câm để biểu đạt điều đó!",		-- 物品名:"韦斯"
            REVIVER = "%s đã đột phá trong tư duy.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Làm sao dùng động tác kịch câm để biểu thị “Tôi muốn tạo đồ hồi sinh”？",		-- 物品名:"幽灵"
            FIRESTARTER = "Đợi đã, đừng nói với ta. Là ngươi đốt lửa.",		-- 物品名:"韦斯"
        },
        WEBBER =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"韦伯"->默认
            ATTACKER = "Ta sẽ dùng giấy cói để gói, đề phòng bất trắc.",		-- 物品名:"韦伯"
            MURDERER = "Hung thủ giết người! Ta sẽ tiêu diệt ngươi, %s!",		-- 物品名:"韦伯"
            REVIVER = "%s cùng người khác hòa thành một khối.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "%s rất muốn ta cho nó một trái tim.",		-- 物品名:"幽灵"
            FIRESTARTER = "Chúng ta phải mở hội nghị phòng cháy.",		-- 物品名:"韦伯"
        },
        WATHGRITHR =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"薇格弗德"->默认
            ATTACKER = "Nếu có thể, ta sẽ tránh nắm đấm của %s.",		-- 物品名:"薇格弗德"
            MURDERER = "%s trở nên điên loạn!",		-- 物品名:"薇格弗德"
            REVIVER = "Tinh thần %s đầy đủ.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Làm tốt lắm. Ngươi vẫn chưa thoát khỏi Valhalla đâu, %s.",		-- 物品名:"幽灵"
            FIRESTARTER = "%s kết nhiều cao thủ",		-- 物品名:"薇格弗德"
        },
        WINONA =
        {
            GENERIC = "Xin chào, %s!",		-- 物品名:"薇诺娜"->默认
            ATTACKER = "%s là tai họa ngầm an toàn.",		-- 物品名:"薇诺娜"
            MURDERER = "Dừng ở đây được rồi, %s!",		-- 物品名:"薇诺娜"
            REVIVER = "Ngươi đúng là hữu dụng đó, %s.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Giống như có người dội gáo nước vào kế hoạch của ngươi.",		-- 物品名:"幽灵"
            FIRESTARTER = "Cháy hết đồ trong xưởng rồi.",		-- 物品名:"薇诺娜"
        },
        WORTOX =
        {
            GENERIC = "Chào bạn, %s!",		-- 物品名:"沃拓克斯"->默认
            ATTACKER = "%s! Ta biết ngươi không đáng tin mà!",		-- 物品名:"沃拓克斯"
            MURDERER = "Đến lúc chống lại ác quỷ sừng dài đó rồi!",		-- 物品名:"沃拓克斯"
            REVIVER = "Cảm ơn sự giúp đỡ của %s.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Ta từ chối sự tồn tại của hồn ma và ác quỷ.",		-- 物品名:"幽灵"
            FIRESTARTER = "%s đang trở thành gánh nặng.",		-- 物品名:"沃拓克斯"
        },
        WORMWOOD =
        {
            GENERIC = "Chào bạn, %s!",		-- 物品名:"沃姆伍德"->默认
            ATTACKER = "Hình như hôm nay %s kích động hơn bình thường.",		-- 物品名:"沃姆伍德"
            MURDERER = "Chuẩn bị bị diệt cỏ đi, cỏ dại, %s!",		-- 物品名:"沃姆伍德"
            REVIVER = "%s trước giờ không bỏ rơi bạn của anh ấy.",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
            GHOST = "Cần giúp đỡ chứ, anh bạn?",		-- 物品名:"幽灵"
            FIRESTARTER = "Ta tưởng ngươi ghét lửa, %s.",		-- 物品名:"沃姆伍德"
        },
        MIGRATION_PORTAL =
        {
            GENERIC = "Nếu ta có bạn bè, thì sẽ đem đồ chơi gặp bọn họ.",		-- 物品名:"Matic 朋友之门"->默认
            OPEN = "Sau khi qua đó, ta vẫn là ta chứ?",		-- 物品名:"Matic 朋友之门"->打开
            FULL = "Hình như bên đó rất nóng.",		-- 物品名:"Matic 朋友之门"->满了
        },
        GLOMMER = "Rất đáng yêu.",		-- 物品名:"格罗姆"
        GLOMMERFLOWER =
        {
            GENERIC = "Cánh hoa từ từ nở rộ.",		-- 物品名:"格罗姆花"->默认
            DEAD = "Cánh hoa khô héo, từ từ tỏa sáng.",		-- 物品名:"格罗姆花"->死了
        },
        GLOMMERWINGS = "Buổi sáng bình an nhe!",		-- 物品名:"格罗姆翅膀"
        GLOMMERFUEL = "Tuy hơi ghê nhưng lại chứa rất nhiều năng lượng.",		-- 物品名:"格罗姆的黏液"
        BELL = "Đinh đinh đinh.",		-- 物品名:"远古铃铛" 制造描述:"这可不是普通的铃铛。"
        STATUEGLOMMER =
        {
            GENERIC = "Thứ này Dương Tiễn chưa từng thấy qua.",		-- 物品名:"格罗姆雕像"->默认
            EMPTY = "Bị bể rồi, có nở lại không?",		-- 物品名:"格罗姆雕像"
        },
        LAVA_POND_ROCK = "Nơi đây khá tốt.",		-- 物品名:"岩石"
		WEBBERSKULL = "Đứa trẻ đáng thương. Chắc phải cử hành tang lễ đàng hoàng cho nó.",		-- 物品名:"韦伯的头骨"
		WORMLIGHT = "Trông có vẻ ngon.",		-- 物品名:"发光蓝莓"
		WORMLIGHT_LESSER = "Có hơi nhiều nếp nhăn.",		-- 物品名:"较少发光蓝莓"
		WORM =
		{
		    PLANT = "Ta đã nhìn thấu ngươi rồi.",		-- 物品名:"洞穴蠕虫"
		    DIRT = "Trông cứ như đống đất vậy.",		-- 物品名:"洞穴蠕虫"
		    WORM = "Nó là một con giun.",		-- 物品名:"洞穴蠕虫"
		},
        WORMLIGHT_PLANT = "Ta đã nhìn thấu ngươi.",		-- 物品名:"神秘植物"
		MOLE =
		{
			HELD = "Ồ ~ Thổ Địa Công Công.",		-- 物品名:"鼹鼠"->拿在手里
			UNDERGROUND = "Đừng hòng trộm đồ của ta.",		-- 物品名:"鼹鼠"
			ABOVEGROUND = "Cái búa đâu rồi...",		-- 物品名:"鼹鼠"
		},
		MOLEHILL = "Cái hang thật là đẹp!",		-- 物品名:"鼹鼠丘"
		MOLEHAT = "Thiên Nhãn của ta không cần cái đó.",		-- 物品名:"鼹鼠帽" 制造描述:"为穿戴者提供夜视能力。"
		EEL = "Nó có thể làm thành một món ăn.",		-- 物品名:"鳗鱼"
		EEL_COOKED = "Thật là thơm!",		-- 物品名:"烤鳗鱼"
		UNAGI = "Hy vọng sẽ không khiến bất kỳ người nào biến thành lươn!",		-- 物品名:"鳗鱼料理"
		EYETURRET = "Hy vọng nó không nhìn về phía ta.",		-- 物品名:"眼睛炮塔"
		EYETURRET_ITEM = "Ta nghĩ nó đang ngủ.",		-- 物品名:"眼睛炮塔" 制造描述:"要远离讨厌的东西，就得杀了它们。"
		MINOTAURHORN = "Nhiên Tê tránh phàm trần!",		-- 物品名:"守护者之角"
		MINOTAURCHEST = "Cực kỳ quý giá.",		-- 物品名:"大号华丽箱子"
		THULECITE_PIECES = "Mảnh Thulecite.",		-- 物品名:"铥矿碎片"
		POND_ALGAE = "Rong rêu bên bờ hồ",		-- 物品名:"水藻"
		GREENSTAFF = "Hóa giải căn cơ của sự vật.",		-- 物品名:"拆解魔杖" 制造描述:"干净而有效的摧毁。"
		GIFT = "Cho ta cái đó phải không?",		-- 物品名:"礼物"
        GIFTWRAP = "Ta quá lợi hại rồi!",		-- 物品名:"礼物包装" 制造描述:"把东西打包起来，好看又可爱！"
		POTTEDFERN = "Trong chậu có cây dương xỉ",		-- 物品名:"蕨类盆栽" 制造描述:"做个花盆，里面栽上蕨类植物。"
        SUCCULENT_POTTED = "Trong chậu có cây mọng nước.",		-- 物品名:"多肉盆栽" 制造描述:"塞进陶盆的漂亮多肉植物。"
		SUCCULENT_PLANT = "Ở đó có nha đam.",		-- 物品名:"多肉植物"
		SUCCULENT_PICKED = "Món đồ giải trí.",		-- 物品名:"多肉植物"
		SENTRYWARD = "Công cụ đo đạc rất tinh vi.",		-- 物品名:"月眼守卫" 制造描述:"绘图者最有价值的武器。"
        TOWNPORTAL =
        {
			GENERIC = "Nhìn xa ngàn dặm một ngày về.",		-- 物品名:"强征传送塔"->默认 制造描述:"用沙子的力量聚集你的朋友们。"
			ACTIVE = "Gió cát nổi lên đưa về nhà.",		-- 物品名:"强征传送塔"->激活了 制造描述:"用沙子的力量聚集你的朋友们。"
		},
        TOWNPORTALTALISMAN = 
        {
			GENERIC = "Mũ ma thuật mini.",		-- 物品名:"沙漠石头"->默认
			ACTIVE = "Cục đá gai góc.",		-- 物品名:"沙漠石头"->激活了
		},
        WETPAPER = "Có thể làm lá bùa.",		-- 物品名:"纸张"
        WETPOUCH = "Cái túi này có thể dùng tạm.",		-- 物品名:"起皱的包裹"
        MOONROCK_PIECES = "Chắc ta có thể đập vỡ được nó.",		-- 物品名:"月亮石碎块"
        MOONBASE =
        {
            GENERIC = "Đặt cây trượng vào.",		-- 物品名:"月亮石"->默认
            BROKEN = "Bể rồi.",		-- 物品名:"月亮石"
            STAFFED = "Sau đó thì sao?",		-- 物品名:"月亮石"
            WRONGSTAFF = "Ta cảm thấy rõ ràng cái này không đúng.",		-- 物品名:"月亮石"
            MOONSTAFF = "Đốt cục đá thần kỳ đó.",		-- 物品名:"月亮石"
        },
        MOONDIAL = 
        {
			GENERIC = "Ánh trăng chưa đầy bể, dòng nước vẫn cuốn trôi.",		-- 物品名:"月晷"->默认 制造描述:"追踪月相！"
			NIGHT_NEW = "Trăng non mới mọc.",		-- 物品名:"月晷" 制造描述:"追踪月相！"
			NIGHT_WAX = "Trăng sắp tràn đầy.",		-- 物品名:"月晷" 制造描述:"追踪月相！"
			NIGHT_FULL = "Nghênh đón trăng rằm.",		-- 物品名:"月晷" 制造描述:"追踪月相！"
			NIGHT_WANE = "Nhật nguyệt tan biến.",		-- 物品名:"月晷" 制造描述:"追踪月相！"
			CAVE = "Hang động nơi đâu trăng đều soi sáng.",		-- 物品名:"月晷" 制造描述:"追踪月相！"
        },
		THULECITE = "Ẩn chứa năng lượng cực lớn.",		-- 物品名:"铥矿石" 制造描述:"将小碎片合成一大块。"
		ARMORRUINS = "Nó nhẹ một cách lạ thường.",		-- 物品名:"铥矿甲" 制造描述:"炫目并且能提供保护。"
		ARMORSKELETON = "Xương boong boong",		-- 物品名:"骨头盔甲" 制造描述:"追踪月相！"
		SKELETONHAT = "Nó khiến ta cảm thấy đáng sợ.",		-- 物品名:"骨头头盔" 制造描述:"追踪月相！"
		RUINS_BAT = "Số lượng khá nhiều.",		-- 物品名:"铥矿岩棒" 制造描述:"尖钉让一切变得更好。"
		RUINSHAT = "Tóc ta thế nào?",		-- 物品名:"远古王冠" 制造描述:"附有远古力场！"
		NIGHTMARE_TIMEPIECE =
		{
            CALM = "Tất cả đều ổn.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            WARN = "Cảm thấy ở đây có năng lượng ma thuật khá mạnh.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            WAXING = "Càng lúc càng trở nên dày đặc.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            STEADY = "Dường như đã ổn định lại rồi.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            WANING = "Cảm giác năng lượng ma thuật đang dần yếu đi.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            DAWN = "Quái bóng tối sắp biến mất rồi!",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
            NOMAGIC = "Ở đây không xuất hiện năng lượng ma thuật.",		-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
		},
		BISHOP_NIGHTMARE = "Nó đang sụp đổ!",		-- 物品名:"损坏的发条主教" 制造描述:"跟踪周围魔力水平的流动。"
		ROOK_NIGHTMARE = "Thật đáng sợ!",		-- 物品名:"损坏的发条战车" 制造描述:"跟踪周围魔力水平的流动。"
		KNIGHT_NIGHTMARE = "Một con đồng hồ mã tàn bạo!",		-- 物品名:"损坏的đồng hồ mã" 制造描述:"跟踪周围魔力水平的流动。"
		MINOTAUR = "Trông tên đó có vẻ không vui.",		-- 物品名:"远古守护者" 制造描述:"跟踪周围魔力水平的流动。"
		SPIDER_DROPPER = "Nhắc nhở: đừng đến đó xem.",		-- 物品名:"穴居悬蛛" 制造描述:"跟踪周围魔力水平的流动。"
		NIGHTMARELIGHT = "Ảo ảnh chẳng là gì đối với ta.",		-- 物品名:"梦魇灯座" 制造描述:"跟踪周围魔力水平的流动。"
		NIGHTSTICK = "Nó có điện!",		-- 物品名:"晨星锤" 制造描述:"用于夜间战斗的晨光。"
		GREENGEM = "Lấp lánh chói lọi.",		-- 物品名:"绿宝石" 制造描述:"跟踪周围魔力水平的流动。"
		MULTITOOL_AXE_PICKAXE = "Thật cao minh!",		-- 物品名:"多用斧镐" 制造描述:"加倍实用。"
		ORANGESTAFF = "Cái này tốt hơn đi bộ.",		-- 物品名:"瞬移魔杖" 制造描述:"适合那些不喜欢走路的人。"
		YELLOWAMULET = "Cảm thấy ấm áp.",		-- 物品名:"魔光护符" 制造描述:"从天堂汲取力量。"
		GREENAMULET = "Tất cả base đều cần có một cái!",		-- 物品名:"建造护符" 制造描述:"用更少的材料合成物品！"
		SLURPERPELT = "Bộ dạng khi chết đều giống nhau.",			-- 物品名:"铥矿奖章" 制造描述:"跟踪周围魔力水平的流动。"
		SLURPER = "Nhiều lông thật!",		-- 物品名:"啜食者" 制造描述:"跟踪周围魔力水平的流动。"
		SLURPER_PELT = "Bộ dạng khi chết đều giống nhau.",		-- 物品名:"啜食者皮" 制造描述:"跟踪周围魔力水平的流动。"
		ARMORSLURPER = "Một cái đai ướt át giúp bạn chống đói.",		-- 物品名:"饥饿腰带" 制造描述:"保持肚子不饿。"
		ORANGEAMULET = "Tốc biến có tác dụng rất lớn.",		-- 物品名:"懒人护符" 制造描述:"适合那些不喜欢捡东西的人。"
		YELLOWSTAFF = "Sức mạnh ngôi sao dẫn lối.",		-- 物品名:"唤星者法杖" 制造描述:"召唤一个小星星。"
		YELLOWGEM = "Viên ngọc này màu vàng đó.",		-- 物品名:"黄宝石" 制造描述:"跟踪周围魔力水平的流动。"
		ORANGEGEM = "Một viên ngọc cam.",		-- 物品名:"橙宝石" 制造描述:"跟踪周围魔力水平的流动。"
        OPALSTAFF = "Gậy phép ánh trăng dẫn lối.",		-- 物品名:"唤月者魔杖" 制造描述:"跟踪周围魔力水平的流动。"
        OPALPRECIOUSGEM = "Viên ngọc này trông thật đặc biệt.",		-- 物品名:"彩虹宝石" 制造描述:"跟踪周围魔力水平的流动。"
        TELEBASE = 
		{
			VALID = "Chuẩn bị xong rồi.",		-- 物品名:"传送焦点"->有效 制造描述:"装上宝石试试。"
			GEMS = "Cần thêm ngọc tím.",		-- 物品名:"传送焦点"->需要宝石 制造描述:"装上宝石试试。"
		},
		GEMSOCKET = 
		{
			VALID = "Chuẩn bị ổn thỏa rồi.",		-- 物品名:"宝石底座"->有效
			GEMS = "Cần một viên ngọc.",		-- 物品名:"宝石底座"->需要宝石
		},
		STAFFLIGHT = "Ánh lửa nóng rực.",		-- 物品名:"矮人之星"
        STAFFCOLDLIGHT = "Lạnh đến tê tái.",		-- 物品名:"极光"
        ANCIENT_ALTAR = "Kiến trúc cổ xưa và thần bí.",		-- 物品名:"远古祭坛"
        ANCIENT_ALTAR_BROKEN = "Triết trúc đổ nát.",		-- 物品名:"损坏的远古祭坛"
        ANCIENT_STATUE = "Nó dường như tách biệt với thế giới.",		-- 物品名:"远古雕像"
        LICHEN = "Chỉ có tảo xanh mới có thể sống trong điều kiện thiếu sáng.",		-- 物品名:"洞穴苔藓"
		CUTLICHEN = "Món ăn no bụng.",		-- 物品名:"苔藓"
		CAVE_BANANA = "Thật là mềm.",		-- 物品名:"洞穴香蕉"
		CAVE_BANANA_COOKED = "Ngon!",		-- 物品名:"烤香蕉"
		CAVE_BANANA_TREE = "Không biết nó có thể quang hợp được hay không.",		-- 物品名:"洞穴香蕉树"
		ROCKY = "Bông tai của nó thật đáng sợ.",		-- 物品名:"石虾"
		COMPASS =
		{
			GENERIC="Ta nhìn về phía nào vậy?",		-- 物品名:"指南针"->默认 制造描述:"指向北方。"
			N = "Bắc.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			S = "Nam.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			E = "Đông.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			W = "Tây.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			NE = "Đông Bắc.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			SE = "Đông Nam.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			NW = "Tây Bắc.",		-- 物品名:"指南针" 制造描述:"指向北方。"
			SW = "Tây Nam.",		-- 物品名:"指南针" 制造描述:"指向北方。"
		},
        HOUNDSTOOTH = "Sắc bén vô cùng!",		-- 物品名:"犬牙" 制造描述:"指向北方。"
        ARMORSNURTLESHELL = "Vật của kẻ hèn nhát.",		-- 物品名:"蜗壳护甲" 制造描述:"指向北方。"
        BAT = "Nhớ giải độc!",		-- 物品名:"洞穴蝙蝠" 制造描述:"指向北方。"
        BATBAT = "Ta dám cá, nếu ta có được 2 cái thì ta có thể bay lên.",		-- 物品名:"蝙蝠棒" 制造描述:"所有科技都如此...耗费精神。"
        BATWING = "Chớ ăn sống.",		-- 物品名:"洞穴蝙蝠翅膀" 制造描述:"指向北方。"
        BATWING_COOKED = "Chí ít nó không sống lại.",		-- 物品名:"烤蝙蝠翅膀" 制造描述:"指向北方。"
        BATCAVE = "Ta không muốn đánh thức bọn nó.",		-- 物品名:"蝙蝠洞" 制造描述:"指向北方。"
        BEDROLL_FURRY = "Đúng là vừa ấm vừa thoải mái.",		-- 物品名:"毛皮铺盖" 制造描述:"舒适地一觉睡到天亮！"
        BUNNYMAN = "Con thỏ tinh này thật là vạm vỡ.",		-- 物品名:"兔人" 制造描述:"指向北方。"
        FLOWER_CAVE = "Đom đóm nhỏ nhoi, chiếu sáng đường đi.",		-- 物品名:"荧光花" 制造描述:"指向北方。"
        GUANO = "A! Cục cứt.",		-- 物品名:"鸟粪" 制造描述:"指向北方。"
        LANTERN = "Xua tan bóng tối.",		-- 物品名:"提灯" 制造描述:"可加燃料、明亮、便携！"
        LIGHTBULB = "Kỳ lạ, trông có vẻ rất ngon.",		-- 物品名:"荧光果" 制造描述:"指向北方。"
        MANRABBIT_TAIL = "Cầm nó trên tay thật là thích.",		-- 物品名:"兔绒" 制造描述:"指向北方。"
        MUSHROOMHAT = "Người đội nó trông thật dễ thương.",		-- 物品名:"蘑菇帽" 制造描述:"指向北方。"
        MUSHROOM_LIGHT2 =
        {
            ON = "Một ngon đèn đáng để ngắm.",		-- 物品名:"炽菇灯"->开启 制造描述:"受到火山岩浆灯饰学问的激发。"
            OFF = "Đèn tuy đẹp, nhưng làm phiền giấc ngủ người khác.",		-- 物品名:"炽菇灯"->关闭 制造描述:"受到火山岩浆灯饰学问的激发。"
            BURNT = "Một thứ tầm thường, sao tồn tại lâu được?",		-- 物品名:"炽菇灯"->烧焦的 制造描述:"受到火山岩浆灯饰学问的激发。"
        },
        MUSHROOM_LIGHT =
        {
            ON = "Cây nấm có thể thắp sáng kìa.",		-- 物品名:"蘑菇灯"->开启 制造描述:"任何蘑菇的完美添加物。"
            OFF = "Nó là một cái đèn nấm to lớn.",		-- 物品名:"蘑菇灯"->关闭 制造描述:"任何蘑菇的完美添加物。"
            BURNT = "Đã bị thiêu rụi.",		-- 物品名:"蘑菇灯"->烧焦的 制造描述:"任何蘑菇的完美添加物。"
        },
        SLEEPBOMB = "Ném một cái khiến ta mệt rã.",		-- 物品名:"睡袋" 制造描述:"可以扔掉的袋子睡意沉沉。"
        MUSHROOMBOMB = "Một đám mây nấm được tạo ra!",		-- 物品名:"炸弹蘑菇" 制造描述:"任何蘑菇的完美添加物。"
        SHROOM_SKIN = "Toàn là cục u!",		-- 物品名:"蘑菇皮" 制造描述:"任何蘑菇的完美添加物。"
        TOADSTOOL_CAP =
        {
            EMPTY = "Chỉ là cái động trên đất mà thôi.",		-- 物品名:"毒菌蟾蜍"
            INGROUND = "Có thứ mọc ra kìa.",		-- 物品名:"毒菌蟾蜍"->在地里面
            GENERIC = "Chỉ muốn chặt cái cây nấm đó.",		-- 物品名:"毒菌蟾蜍"->默认
        },
        TOADSTOOL =
        {
            GENERIC = "Ôi! Ta không muốn hôn trúng cái thứ đó!",		-- 物品名:"毒菌蟾蜍"->默认
            RAGE = "Nó nhảy nhót điên cuồng!",		-- 物品名:"毒菌蟾蜍"->愤怒
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "Đem đi đi, thần tiên sao lại đội cái thứ này!",		-- 物品名:"孢子帽"->默认
            BURNT = "Cái thứ vô xỉ, đốt nó đi!",		-- 物品名:"孢子帽"->烧焦的
        },
        MUSHTREE_TALL =
        {
            GENERIC = "Cây nấm đó to ghê.",		-- 物品名:"蓝蘑菇树"->默认
            BLOOM = "Nhìn từ xa thì không thấy, nhưng nó hôi thật.",		-- 物品名:"蓝蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "Mấy cái thứ này mọc trong phòng tắm của ta.",		-- 物品名:"红蘑菇树"->默认
            BLOOM = "Mấy cái này khiến ta thấy khó chịu.",		-- 物品名:"红蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "Cây nấm ma thuật?",		-- 物品名:"绿蘑菇树"->默认
            BLOOM = "Nó đang cố gắng sinh sôi.",		-- 物品名:"绿蘑菇树"->蘑菇树繁殖？？
        },
        MUSHTREE_TALL_WEBBED = "Đám nhện cảm thấy cái đó rất quan trọng.",		-- 物品名:"蛛网蓝蘑菇树"
        SPORE_TALL =
        {
            GENERIC = "Nó trôi nổi khắp nơi.",		-- 物品名:"蓝色孢子"->默认
            HELD = "Ta muốn để ánh sáng trong túi.",		-- 物品名:"蓝色孢子"->拿在手里
        },
        SPORE_MEDIUM =
        {
            GENERIC = "Không còn bận tâm gì trên thế gian này.",		-- 物品名:"红色孢子"->默认
            HELD = "Ta muốn để ánh sáng trong túi.",		-- 物品名:"红色孢子"->拿在手里
        },
        SPORE_SMALL =
        {
            GENERIC = "Đó là cảnh tượng mà mắt nhìn thấy về bào tử.",		-- 物品名:"绿色孢子"->默认
            HELD = "Ta muốn để ánh sáng trong túi.",		-- 物品名:"绿色孢子"->拿在手里
        },
        RABBITHOUSE =
        {
            GENERIC = "Cái đó không phải là cà rốt.",		-- 物品名:"兔屋"->默认 制造描述:"可容纳一只巨大的兔子及其所有物品。"
            BURNT = "Cái đó không phải là cà rốt nướng.",		-- 物品名:"兔屋"->烧焦的 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        },
        SLURTLE = "Ọe... mắc ói...",		-- 物品名:"尖壳蜗牛" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SLURTLE_SHELLPIECES = "Mảnh ghép không hoàn hảo.",		-- 物品名:"蜗壳碎片" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SLURTLEHAT = "Nó sẽ làm rối tóc ta.",		-- 物品名:"蜗牛帽" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SLURTLEHOLE = "Cái gò ghê tởm.",		-- 物品名:"尖壳蜗牛窝" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SLURTLESLIME = "Vì nó hữu dụng nên ta mới đụng vào nó.",		-- 物品名:"尖壳蜗牛黏液" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SNURTLE = "Tuy không tởm cho lắm, nhưng vẫn thấy tởm.",		-- 物品名:"圆壳蜗牛" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SPIDER_HIDER = "A!! Nhện nhiều quá!",		-- 物品名:"洞穴蜘蛛" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SPIDER_SPITTER = "Ta ghét nhện!",		-- 物品名:"喷射蜘蛛" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SPIDERHOLE = "Nó bám đầy mạng nhện.",		-- 物品名:"蛛网岩" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        SPIDERHOLE_ROCK = "Nó bám đầy mạng nhện.",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        STALAGMITE = "Ta thấy nó chỉ là cục đá.",		-- 物品名:"石笋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        STALAGMITE_TALL = "Cục đá, cục đá, cục đá,...",		-- 物品名:"石笋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TREASURECHEST_TRAP = "Thật là tiện!",		-- 物品名:"宝箱" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_CARPETFLOOR = "Rất bám.",		-- 物品名:"地毯地板" 制造描述:"超级柔软。气味像牦牛。"
        TURF_CHECKERFLOOR = "Mấy cái này rất đẹp.",		-- 物品名:"棋盘地板" 制造描述:"精心制作成棋盘状的大理石地砖。"
        TURF_DIRT = "Một nền đất.",		-- 物品名:"泥土地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_FOREST = "Một nền đất.",		-- 物品名:"森林地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_GRASS = "Một nền đất.",		-- 物品名:"长草地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_MARSH = "Một nền đất.",		-- 物品名:"沼泽地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_ROAD = "Đường đá xây qua loa.",		-- 物品名:"卵石路" 制造描述:"修建你自己的道路，通往任何地方。"
        TURF_ROCKY = "Một nền đất.",		-- 物品名:"岩石地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_SAVANNA = "Một nền đất.",		-- 物品名:"热带草原地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        TURF_WOODFLOOR = "Chỗ này là sàn gỗ.",		-- 物品名:"木地板" 制造描述:"优质复合地板。"
		TURF_CAVE="Lại là một loại đất.",		-- 物品名:"鸟粪地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_FUNGUS="Lại là một loại đất.",		-- 物品名:"菌类地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_SINKHOLE="Lại là một loại đất.",		-- 物品名:"黏滑地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_UNDERROCK="Lại là một loại đất.",		-- 物品名:"洞穴岩石地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_MUD="Lại là một loại đất.",		-- 物品名:"泥泞地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_DECIDUOUS = "Lại là một loại đất.",		-- 物品名:"桦树地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_SANDY = "Lại là một loại đất.",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_BADLANDS = "Lại là một loại đất.",		-- 物品名:"兔屋" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_DESERTDIRT = "Một nền đất.",		-- 物品名:"沙漠地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_FUNGUS_GREEN = "Một nền đất.",		-- 物品名:"菌类地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_FUNGUS_RED = "Một nền đất.",		-- 物品名:"菌类地皮" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
		TURF_DRAGONFLY = "Ngươi muốn chứng minh nó có thể chống lửa sao?",		-- 物品名:"鳞状地板" 制造描述:"消除火灾蔓延速度。"
		POWCAKE = "emmm, Hao Thiên Khuyển ngươi muốn ăn không?",		-- 物品名:"芝士蛋糕" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        CAVE_ENTRANCE = "Không biết chỗ nham thạch đó có thể đưa đi được không nhỉ.",		-- 物品名:"堵住的陷洞" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
        CAVE_ENTRANCE_RUINS = "Nó có thể ẩn chứa điều gì đó.",		-- 物品名:"被堵住的陷洞" 制造描述:"可容纳一只巨大的兔子及其所有物品。"
       	CAVE_ENTRANCE_OPEN = 
        {
            GENERIC = "Vùng đất này đang chống lại ta!",		-- 物品名:"陷洞"->默认
            OPEN = "Động không đáy này thật là sâu.",		-- 物品名:"陷洞"->打开
            FULL = "Ta sẽ đợi có người ra.",		-- 物品名:"陷洞"->满了
        },
        CAVE_EXIT = 
        {
            GENERIC = "Ta nghĩ ta nên đợi ở dưới.",		-- 物品名:"楼梯"->默认
            OPEN = "Ta tạm thời không muốn thám hiểm.",		-- 物品名:"楼梯"->打开
            FULL = "Phía trên quá chật!",		-- 物品名:"楼梯"->满了
        },
		MAXWELLPHONOGRAPH = "Thì ra âm thanh phát ra từ đó.",		-- 物品名:"麦斯威尔的留声机"
		BOOMERANG = "Ta trước giờ chưa từng thất thủ!",		-- 物品名:"回旋镖" 制造描述:"来自澳洲土著。"
		PIGGUARD = "Trông nó không thân thiện cho lắm.",		-- 物品名:"猪人守卫"
		ABIGAIL = "A, cô ta có một cái nơ thật đẹp.",		-- 物品名:"阿比盖尔"
		ADVENTURE_PORTAL = "Ta không muốn bị lừa lần nữa.",		-- 物品名:"麦斯威尔之门"
		AMULET = "Ta cảm thấy an toàn khi mang theo nó.",		-- 物品名:"重生护符" 制造描述:"逃离死神的魔爪。"
		ANIMAL_TRACK = "Dấu vết mà đồ ăn để lại. Ý ta là... một con thú.",		-- 物品名:"动物足迹"
		ARMORGRASS = "Hy vọng bên trong không có sâu.",		-- 物品名:"草甲" 制造描述:"提供少许防护。"
		ARMORMARBLE = "Trông nó rất là nặng.",		-- 物品名:"大理石甲" 制造描述:"它很重，但能够保护你。"
		ARMORWOOD = "Đó là một bộ giáp rất vừa vặn.",		-- 物品名:"木甲" 制造描述:"为你抵御部分伤害。"
		ARMOR_SANITY = "Hộ thân thì dễ, tâm trí thì khó.",		-- 物品名:"魂甲" 制造描述:"保护你的躯体，但无法保护你的心智。"
		ASH =
		{
			GENERIC = "Rừng xanh cháy thành tro, mây khói bay mịt mù.",		-- 物品名:"灰烬"->默认
			REMAINS_GLOMMERFLOWER = "Khi dịch chuyển, hoa sẽ bị ngọn lửa nuốt chửng!",		-- 物品名:"灰烬"->单机专用
			REMAINS_EYE_BONE = "Khi dịch chuyển, mắt xương sẽ bị ngọn lửa nuốt chửng!",		-- 物品名:"灰烬"->单机专用
			REMAINS_THINGIE = "Rừng xanh cháy thành tro, mây khói bay mịt mù.",		-- 物品名:"灰烬"->单机专用
		},
		AXE = "Chặt đổ rừng cây.",		-- 物品名:"斧头" 制造描述:"砍倒树木！"
		BABYBEEFALO = 
		{
			GENERIC = "A. Đáng yêu quá!",		-- 物品名:"小牦牛"->默认
		    SLEEPING = "Mơ một giấc mộng đẹp.",		-- 物品名:"小牦牛"->睡着了
        },
        BUNDLE = "Tất cả nguyên liệu của chúng ta đều ở bên trong!",		-- 物品名:"捆绑物资"
        BUNDLEWRAP = "Gói đồ lại sẽ đem đi dễ dàng hơn.",		-- 物品名:"捆绑包装" 制造描述:"打包你的东西的部分和袋子。"
		BACKPACK = "Đeo hành lý.",		-- 物品名:"背包" 制造描述:"携带更多物品。"
		BACONEGGS = "Chỉ là thịt và trứng chiên.",		-- 物品名:"培根煎蛋"
		BANDAGE = "Có vẻ rất vệ sinh đó.",		-- 物品名:"蜂蜜药膏" 制造描述:"愈合小伤口。"
		BASALT = "Cứng quá! Gõ không bể!",		-- 物品名:"玄武岩"
		BEARDHAIR = "Khi nó không phải là của mình thì mới thấy ghê.",		-- 物品名:"胡子"
		BEARGER = "Con gấu thật là to.",		-- 物品名:"狂暴熊獾"
		BEARGERVEST = "Chào mừng đến trạm ngủ đông!",		-- 物品名:"熊皮背心" 制造描述:"熊皮背心。"
		ICEPACK = "Túi da lông có công hiệu giữ nhiệt.",		-- 物品名:"保鲜背包" 制造描述:"容量虽小，但能保持东西新鲜。"
		BEARGER_FUR = "Một tấm da lông dày.",		-- 物品名:"熊皮" 制造描述:"毛皮再生。"
		BEDROLL_STRAW = "Trông khá dễ chịu, nhưng mùi hơi khó chịu.",		-- 物品名:"草席卷" 制造描述:"一觉睡到天亮。"
		BEEQUEEN = "Để nó xa ta chút!",		-- 物品名:"蜂王"
		BEEQUEENHIVE = 
		{
			GENERIC = "Dính quá, không đi được.",		-- 物品名:"蜂蜜地块"->默认
			GROWING = "Cái thứ đó hồi trước ở đây sao?",		-- 物品名:"蜂蜜地块"->正在生长
		},
        BEEQUEENHIVEGROWN = "Yêu nghiệt phương nào, trốn ở trong đây!",		-- 物品名:"巨大蜂巢"
        BEEGUARD = "Nó đang bảo vệ nữ vương.",		-- 物品名:"嗡嗡蜜蜂"
        HIVEHAT = "Khi ta đội vào, có vẻ thế giới không còn điên dại nữa.",		-- 物品名:"蜂王帽"
        MINISIGN =
        {
            GENERIC = "Ta có thể vẽ đẹp hơn!",		-- 物品名:"小木牌"->默认
            UNDRAWN = "Chúng ta nên vẽ gì đó lên trên.",		-- 物品名:"小木牌"->没有画画
        },
        MINISIGN_ITEM = "Không quá hữu dụng. Chúng ta nên bỏ nó xuống.",		-- 物品名:"小木牌" 制造描述:"用羽毛笔在这些上面画画。"
		BEE =
		{
			GENERIC = "Vù vù vù, bay trong bụi hoa.",		-- 物品名:"蜜蜂"->默认
			HELD = "Cẩn thận!",		-- 物品名:"蜜蜂"->拿在手里
		},
		BEEBOX =
		{
			READY = "Bên trong chứa đầy mật ong.",		-- 物品名:"蜂箱"->准备好了 制造描述:"贮存你自己的蜜蜂。"
			FULLHONEY = "Bên trong chứa đầy mật ong.",		-- 物品名:"蜂箱"->蜂蜜满了 制造描述:"贮存你自己的蜜蜂。"
			GENERIC = "Mật ong!",		-- 物品名:"蜂箱"->默认 制造描述:"贮存你自己的蜜蜂。"
			NOHONEY = "Không có gì.",		-- 物品名:"蜂箱"->没有蜂蜜 制造描述:"贮存你自己的蜜蜂。"
			SOMEHONEY = "Cần phải đợi một chút.",		-- 物品名:"蜂箱"->有一些蜂蜜 制造描述:"贮存你自己的蜜蜂。"
			BURNT = "Sao lại bị cháy rồi?!!",		-- 物品名:"蜂箱"->烧焦的 制造描述:"贮存你自己的蜜蜂。"
		},
		MUSHROOM_FARM =
		{
			STUFFED = "Nhiều nấm ghê!",		-- 物品名:"蘑菇农场"->塞，满了？？ 制造描述:"种蘑菇。"
			LOTS = "Nấm mọc đầy trên gỗ.",		-- 物品名:"蘑菇农场"->很多 制造描述:"种蘑菇。"
			SOME = "Hiện tại nó đang phát triển.",		-- 物品名:"蘑菇农场"->有一些 制造描述:"种蘑菇。"
			EMPTY = "Nó có thể dùng một bào tử hoặc một cây nấm để trồng.",		-- 物品名:"蘑菇农场" 制造描述:"种蘑菇。"
			ROTTEN = "Khúc gỗ chết rồi. Chúng ta cần một khúc gỗ sống để thay thế nó.",		-- 物品名:"蘑菇农场"->腐烂的--需要活木 制造描述:"种蘑菇。"
			BURNT = "Khúc gỗ mục nát đã sinh ra nó.",		-- 物品名:"蘑菇农场"->烧焦的 制造描述:"种蘑菇。"
			SNOWCOVERED = "Tôi cho rằng nó không sống được trong thời tiết giá lạnh.",		-- 物品名:"蘑菇农场"->被雪覆盖 制造描述:"种蘑菇。"
		},
		BEEFALO =
		{
			FOLLOWER = "Hắn im lặng đi theo ta.",		-- 物品名:"牦牛"->追随者
			GENERIC = "Đó là một con bò lai!",		-- 物品名:"牦牛"->默认
			NAKED = "Ài, nó rất đau buồn.",		-- 物品名:"牦牛"->牛毛被刮干净了
			SLEEPING = "Mấy con này ngủ như chết.",		-- 物品名:"牦牛"->睡着了
            DOMESTICATED = "Không có con nào hồi như con này.",		-- 物品名:"牦牛"->驯化牛
            ORNERY = "Trông nó có vẻ rất tức giận.",		-- 物品名:"牦牛"->战斗牛
            RIDER = "Trông nó cũng dễ cưỡi.",		-- 物品名:"牦牛"->骑行牛
            PUDGY = "Uh, chắc nó ăn quá nhiều rồi.",		-- 物品名:"牦牛"
		},
		BEEFALOHAT = "Ta không muốn đội cái này.",		-- 物品名:"牛角帽" 制造描述:"成为牛群中的一员！连气味也变得一样。"
		BEEFALOWOOL = "Đồ giữ ấm rất tốt.",		-- 物品名:"牛毛"
		BEEHAT = "Bảo vệ gia của bạn, nhưng sẽ làm hư kiểu tóc mà bạn đã làm.",		-- 物品名:"养蜂帽" 制造描述:"防止被愤怒的蜜蜂蜇伤。"
        BEESWAX = "Chống hỏng rất tốt!",		-- 物品名:"蜂蜡" 制造描述:"一种有用的防腐蜂蜡。"
		BEEHIVE = "Ong vo ve không ngừng.",		-- 物品名:"野生蜂窝"
		BEEMINE = "Khi nó bị kích động thì sẽ kêu vo ve lên.",		-- 物品名:"蜜蜂地雷" 制造描述:"变成武器的蜜蜂。会出什么问题？"
		BEEMINE_MAXWELL = "Con muỗi điên dại bị nhốt trong bom!",		-- 物品名:"麦斯威尔的蚊子陷阱"
		BERRIES = "Dâu rừng thơm ngon.",		-- 物品名:"浆果"
		BERRIES_COOKED = "Ta cho rằng nấu lên cũng không khiến nó trở nên ngon hơn.",		-- 物品名:"烤浆果"
        BERRIES_JUICY = "Mùi thơm tỏa ra.",		-- 物品名:"多汁浆果"
        BERRIES_JUICY_COOKED = "Nấu đến nước dâu đầy tay!",		-- 物品名:"烤多汁浆果"
		BERRYBUSH =
		{
			BARREN = "Ta nghĩ nó cần bón phân.",		-- 物品名:"浆果丛"
			WITHERED = "Héo rồi.",		-- 物品名:"浆果丛"->枯萎了
			GENERIC = "Ta nghĩ nó ăn được.",		-- 物品名:"浆果丛"->默认
			PICKED = "Có lẽ nó sẽ sinh sôi trở lại?",		-- 物品名:"浆果丛"->被采完了
			DISEASED = "Trông có vẻ bệnh khá nặng.",		-- 物品名:"浆果丛"->生病了
			DISEASING = "A... có gì đó không đúng.",		-- 物品名:"浆果丛"->正在生病？？
			BURNING = "Dễ cháy.",		-- 物品名:"浆果丛"->正在燃烧
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "Đất không đủ màu mỡ.",		-- 物品名:"多汁浆果丛"
			WITHERED = "Nhiệt độ cao sẽ khiến dâu mọng mất nước!",		-- 物品名:"多汁浆果丛"->枯萎了
			GENERIC = "Ta nên để dành đến lúc ăn cơm.",		-- 物品名:"多汁浆果丛"->默认
			PICKED = "Nó đang có gắng mọc lại.",		-- 物品名:"多汁浆果丛"->被采完了
			DISEASED = "Trông nó không ổn cho lắm.",		-- 物品名:"多汁浆果丛"->生病了
			DISEASING = "A... có gì đó không đúng.",		-- 物品名:"多汁浆果丛"->正在生病？？
			BURNING = "Nó cháy sắp hết rồi.",		-- 物品名:"多汁浆果丛"->正在燃烧
		},
		BIGFOOT = "Cái chân thật là to lớn.",		-- 物品名:"大脚怪"
		BIRDCAGE =
		{
			GENERIC = "Sớm đã bị nhốt trong lồng vàng, không thể tự do hót trong rừng cây.",		-- 物品名:"鸟笼"->默认 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			OCCUPIED = "Một lồng không thể có 2 chim",		-- 物品名:"鸟笼"->被占领 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			SLEEPING = "Ngủ rồi, quên đi nỗi sầu.",		-- 物品名:"鸟笼"->睡着了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			HUNGRY = "Kẻ bị nhốt chỉ có thể đợi cái chết.",		-- 物品名:"鸟笼"->有点饿了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			STARVING = "Đã lâu không được cho ăn rồi nhỉ?",		-- 物品名:"鸟笼"->挨饿 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			DEAD = "Có lẽ chết còn tốt hơn bị nhốt?",		-- 物品名:"鸟笼"->死了 制造描述:"为你的鸟类朋友提供一个幸福的家。"
			SKELETON = "Chết trong uất ức.",		-- 物品名:"骷髅" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		},
		BIRDTRAP = "Mạng nhện đem lại nhiều lợi ích!",		-- 物品名:"捕鸟陷阱" 制造描述:"捕捉会飞的动物。"
		CAVE_BANANA_BURNT = "Không phải lỗi của ta!",		-- 物品名:"鸟笼" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BIRD_EGG = "Tại sao không có nước luộc trứng.",		-- 物品名:"鸟蛋" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BIRD_EGG_COOKED = "Ngon thật!",		-- 物品名:"煎蛋" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BISHOP = "Lùi về sau, nhà truyền giáo!",		-- 物品名:"发条主教" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BLOWDART_FIRE = "Cái thứ này không an toàn.",		-- 物品名:"火焰吹箭" 制造描述:"向你的敌人喷火。"
		BLOWDART_SLEEP = "Đừng có hút ngược vào.",		-- 物品名:"催眠吹箭" 制造描述:"催眠你的敌人。"
		BLOWDART_PIPE = "Ta sẽ từ từ luyện làm sao để thổi nến!",		-- 物品名:"吹箭" 制造描述:"向你的敌人喷射利齿。"
		BLOWDART_YELLOW = "Độ chính xác của nó khiến người ta kinh ngạc.",		-- 物品名:"雷电吹箭" 制造描述:"朝你的敌人放闪电。"
		BLUEAMULET = "Bảo vệ chống cái lạnh!",		-- 物品名:"寒冰护符" 制造描述:"多么冰冷酷炫的护符。"
		BLUEGEM = "Nó tỏa ra ánh sáng mang năng lượng giá lạnh.",		-- 物品名:"蓝宝石" 制造描述:"为你的鸟类朋友提供一个幸福的家。"
		BLUEPRINT = 
		{ 
            COMMON = "Học thuật một ngày, đạo pháp thâm sâu!",		-- 物品名:"蓝图"
            RARE = "Cực kỳ tinh xảo!",		-- 物品名:"蓝图"->罕见的
        },
        SKETCH = "Một bản vẽ về tượng. Phải tìm nơi để làm.",		-- 物品名:"{item}骨架"
		BLUE_CAP = "Nó rất kỳ lạ, còn dính dính nữa.",		-- 物品名:"采摘的蓝蘑菇"
		BLUE_CAP_COOKED = "Lần này thì khác rồi",		-- 物品名:"烤蓝蘑菇"
		BLUE_MUSHROOM =
		{
			GENERIC = "Là nấm.",		-- 物品名:"蓝蘑菇"->默认
			INGROUND = "Nó đang ngủ.",		-- 物品名:"蓝蘑菇"->在地里面
			PICKED = "Không biết nó có mọc lại không?",		-- 物品名:"蓝蘑菇"->被采完了
		},
		BOARDS = "Ván.",		-- 物品名:"木板" 制造描述:"更平直的木头。"
		BONESHARD = "Mảnh xương.",		-- 物品名:"骨头碎片"
		BONESTEW = "Cảm giác no nê đến ọc ra ngoài.",		-- 物品名:"炖肉汤"
		BUGNET = "Công dụng bắt sâu.",		-- 物品名:"捕虫网" 制造描述:"抓虫子用的。"
		BUSHHAT = "Có vẻ có gai.",		-- 物品名:"灌木丛帽" 制造描述:"很好的伪装！"
		BUTTER = "Trông khá giống Kim Chuyên của Na Tra!",		-- 物品名:"黄油"
		BUTTERFLY =
		{
			GENERIC = "Bươm bướm, bay vào hoa cỏ khó mà kiếm được.",		-- 物品名:"蝴蝶"->默认
			HELD = "Cuối cùng cũng bắt được ngươi rồi!",		-- 物品名:"蝴蝶"->拿在手里
		},
		BUTTERFLYMUFFIN = "Chúng ta làm mất sách nấu ăn rồi, thôi thì trổ tài một chút vậy.",		-- 物品名:"蝴蝶松饼"
		BUTTERFLYWINGS = "Con bướm tội nghiệp.",		-- 物品名:"蝴蝶翅膀"
		BUZZARD = "Con diều hâu xấu đê tiện.",		-- 物品名:"秃鹫"
		SHADOWDIGGER = "Ồ, cũng tốt. Giờ sẽ tạo thêm người đứa nữa.",		-- 物品名:"蝴蝶"
		CACTUS = 
		{
			GENERIC = "Tuy có gai, nhưng rất ngon.",		-- 物品名:"仙人掌"->默认
			PICKED = "Khô héo, nhưng vẫn còn khá nhiều gai.",		-- 物品名:"仙人掌"->被采完了
		},
		CACTUS_MEAT_COOKED = "Món nướng đến từ xa mạc.",		-- 物品名:"烤仙人掌肉"
		CACTUS_MEAT = "Biến nó thành một món ngon, phải làm sạch gai trên thân nó trước.",		-- 物品名:"仙人掌肉"
		CACTUS_FLOWER = "Đóa hoa đẹp có nhiều gai.",		-- 物品名:"仙人掌花"
		COLDFIRE =
		{
			EMBERS = "Phải thêm dầu vào lửa, nếu không sẽ tắt.",		-- 物品名:"冰火"->即将熄灭 制造描述:"这火是越烤越冷的冰火。"
			GENERIC = "Xua tan bóng tối xung quanh.",		-- 物品名:"冰火"->默认 制造描述:"这火是越烤越冷的冰火。"
			HIGH = "Lớn lớn quá rồi!",		-- 物品名:"冰火"->高 制造描述:"这火是越烤越冷的冰火。"
			LOW = "Lửa hơi nhỏ.",		-- 物品名:"冰火"->低？ 制造描述:"这火是越烤越冷的冰火。"
			NORMAL = "Thật thoải mái.",		-- 物品名:"冰火"->普通 制造描述:"这火是越烤越冷的冰火。"
			OUT = "Ồ, tắt lửa rồi.",		-- 物品名:"冰火"->出去？外面？ 制造描述:"这火是越烤越冷的冰火。"
		},
		CAMPFIRE =
		{
			EMBERS = "Không đốt thì lửa sẽ tắt.",		-- 物品名:"营火"->即将熄灭 制造描述:"燃烧时发出光亮。"
			GENERIC = "Xóa tan bóng tối bao chùm.",		-- 物品名:"营火"->默认 制造描述:"燃烧时发出光亮。"
			HIGH = "Lửa lớn quá rồi!",		-- 物品名:"营火"->高 制造描述:"燃烧时发出光亮。"
			LOW = "Lửa hơi nhỏ.",		-- 物品名:"营火"->低？ 制造描述:"燃烧时发出光亮。"
			NORMAL = "Thật thoải mái.",		-- 物品名:"营火"->普通 制造描述:"燃烧时发出光亮。"
			OUT = "Ồ, tắt lửa rồi.",		-- 物品名:"营火"->出去？外面？ 制造描述:"燃烧时发出光亮。"
		},
		CANE = "Ta không phải là ông già bảy tám chục tuổi.",		-- 物品名:"拐棍" 制造描述:"泰然自若地快步走。"
		CATCOON = "Cái đồ tinh nghịch.",		-- 物品名:"浣熊猫" 制造描述:"燃烧时发出光亮。"
		CATCOONDEN = 
		{
			GENERIC = "Cái tổ trong cái gốc cây.",		-- 物品名:"空心树桩"->默认
			EMPTY = "Chủ của cái tổ này chết rồi.",		-- 物品名:"空心树桩"
		},
		CATCOONHAT = "Cái mũ che cái tai.",		-- 物品名:"浣熊猫帽子" 制造描述:"适合那些重视温暖甚于朋友的人。"
		COONTAIL = "Ta cảm thấy nó vẫn còn lắc lư.",		-- 物品名:"浣熊猫尾巴"
		CARROT = "Món ngon vừa miệng.",		-- 物品名:"胡萝卜"
		CARROT_COOKED = "Mềm nhũn.",		-- 物品名:"烤胡萝卜"
		CARROT_PLANTED = "Một sinh vật mọc lên từ đất.",		-- 物品名:"胡萝卜"
		CARROT_SEEDS = "Hạt giống cà rốt.",		-- 物品名:"胡萝卜种子"
		CARTOGRAPHYDESK =
		{
			GENERIC = "Những sách lượt đưa ra đều trong một trướng nhỏ nhoi!",		-- 物品名:"制图桌"->默认 制造描述:"准确地告诉别人你去过哪里。"
			BURNING = "Những việc này nên dừng lại ở đây.",		-- 物品名:"制图桌"->正在燃烧 制造描述:"准确地告诉别人你去过哪里。"
			BURNT = "Giờ chỉ còn tro bụi mà thoai.",		-- 物品名:"制图桌"->烧焦的 制造描述:"准确地告诉别人你去过哪里。"
		},
		WATERMELON_SEEDS = "Hạt giống dưa hấu.",		-- 物品名:"西瓜种子" 制造描述:"准确地告诉别人你去过哪里。"
		CAVE_FERN = "Nó là một loài dương xỉ.",		-- 物品名:"蕨类植物" 制造描述:"准确地告诉别人你去过哪里。"
		CHARCOAL = "Vừa nhỏ vừa đen, mùi giống như khúc gỗ bị cháy.",		-- 物品名:"木炭" 制造描述:"准确地告诉别人你去过哪里。"
        CHESSPIECE_PAWN = "Ta cũng đồng cảm.",		-- 物品名:"卒子棋子" 制造描述:"准确地告诉别人你去过哪里。"
        CHESSPIECE_ROOK =
        {
            GENERIC = "Nó có vẻ nặng hơn mình tưởng.",		-- 物品名:"战车棋子"->默认
            STRUGGLE = "Các quân cờ đang tự di chuyển!",		-- 物品名:"战车棋子"->三基佬棋子晃动
        },
        CHESSPIECE_KNIGHT =
        {
            GENERIC = "Nó chắc chắn là con ngựa.",		-- 物品名:"骑士棋子"->默认
            STRUGGLE = "Các quân cợ đang tự di chuyển!",		-- 物品名:"骑士棋子"->三基佬棋子晃动
        },
        CHESSPIECE_BISHOP =
        {
            GENERIC = "Là ngựa bằng đá.",		-- 物品名:"主教棋子"->默认
            STRUGGLE = "Các quân cợ đang tự di chuyển!",		-- 物品名:"主教棋子"->三基佬棋子晃动
        },
        CHESSPIECE_MUSE = "Ừ... trông rất quen thuộc.",		-- 物品名:"女王棋子"
        CHESSPIECE_FORMAL = "Đối với ta mà nói, trông không có gì gọi là cao quý.",		-- 物品名:"国王棋子"
        CHESSPIECE_HORNUCOPIA = "Cứ ngắm nó hoài khiến bụng ta kêu ầm ĩ lên rồi.",		-- 物品名:"丰饶角雕刻"
        CHESSPIECE_PIPE = "Nó chưa bao giờ là đồ ăn của ta cả.",		-- 物品名:"泡泡烟斗雕塑"
        CHESSPIECE_DEERCLOPS = "Cảm giác ánh mắt nó đang dõi theo ngươi.",		-- 物品名:"独眼巨鹿棋子"
        CHESSPIECE_BEARGER = "Nó thật là to lớn.",		-- 物品名:"熊獾棋子"
        CHESSPIECE_MOOSEGOOSE = "Ôi. Nó giống như thật vậy.",		-- 物品名:"驼鹿棋子"
        CHESSPIECE_DRAGONFLY = "A, nó khiến ta nhớ lại một số việc không hay.",		-- 物品名:"龙蝇棋子"
        CHESSPIECE_CLAYHOUND = "Đằng nào cũng xích lại rồi, ta không còn sợ nữa.",		-- 物品名:"猎犬棋子"
        CHESSPIECE_CLAYWARG = "Không ngờ ta lại không bị giết!",		-- 物品名:"座狼棋子"
        CHESSJUNK1 = "Một đống cờ nát.",		-- 物品名:"损坏的发条装置"
        CHESSJUNK2 = "Một đống cờ nát khác.",		-- 物品名:"损坏的发条装置"
        CHESSJUNK3 = "Rất nhiều cờ nát.",		-- 物品名:"损坏的发条装置"
		CHESTER = "Một cái hộp... chân ngắn.",		-- 物品名:"切斯特"
		CHESTER_EYEBONE =
		{
			GENERIC = "Bây giờ Dương Tiễn có ~ 4 con mắt.",		-- 物品名:"眼骨"->默认
			WAITING = "Nó ngủ rồi.",		-- 物品名:"眼骨"->需要等待
		},
		COOKEDMANDRAKE = "Đứa trẻ tội nghiệp.",		-- 物品名:"烤曼德拉草"
		COOKEDMEAT = "Thịt đã nướng chín.",		-- 物品名:"烤大肉"
		COOKEDMONSTERMEAT = "Chỉ ngon hơn ăn sống một chút.",		-- 物品名:"烤怪物肉"
		COOKEDSMALLMEAT = "Bây giờ ta không còn sợ vi trùng còn sống nữa!",		-- 物品名:"烤小肉"
		COOKPOT =
		{
			COOKING_LONG = "Cần phải chờ thêm chút.",		-- 物品名:"烹饪锅"->饭还需要很久 制造描述:"制作更好的食物。"
			COOKING_SHORT = "Đừng sốt ruột, hiện tại vẫn chưa ăn được!",		-- 物品名:"烹饪锅"->饭快做好了 制造描述:"制作更好的食物。"
			DONE = "Ừ! Ăn được rồi!",		-- 物品名:"烹饪锅"->完成了 制造描述:"制作更好的食物。"
			EMPTY = "Nhìn thôi cũng thấy đói rồi.",		-- 物品名:"烹饪锅" 制造描述:"制作更好的食物。"
			BURNT = "Cái nối bị cháy rồi.",		-- 物品名:"烹饪锅"->烧焦的 制造描述:"制作更好的食物。"
		},
		CORN = "Sao ta không nấu lên thử!",		-- 物品名:"玉米" 制造描述:"制作更好的食物。"
		CORN_COOKED = "Nấu chín rồi!",		-- 物品名:"爆米花" 制造描述:"制作更好的食物。"
		CORN_SEEDS = "Đây là hạt giống bắp.",		-- 物品名:"玉米种子" 制造描述:"制作更好的食物。"
        CANARY =
		{
			GENERIC = "Màu vàng à? Chẳng lẽ là con khỉ ngang ngược đó?",		-- 物品名:"金丝雀"->默认
			HELD = "Bắt được chim, bỏ vào lồng.",		-- 物品名:"金丝雀"->拿在手里
		},
        CANARY_POISONED = "Nó rất tốt.",		-- 物品名:"金丝雀（有翅膀的动物）"
		CRITTERLAB = "Bên trong có gì vậy?",		-- 物品名:"岩石巢穴"
        CRITTER_GLOMLING = "Một sinh vật đầy không khí đang đập cánh.",		-- 物品名:"小格罗姆"
        CRITTER_DRAGONLING = "Nó đã bò vào trong lòng ta.",		-- 物品名:"蜂群卫士"
		CRITTER_LAMB = "Chất dịch của nó ít hơn mẹ nó.",		-- 物品名:"小钢羊"
        CRITTER_PUPPY = "Đối với con quái thú nhỏ mà nói, thật là quá đáng yêu!",		-- 物品名:"小座狼"
        CRITTER_KITTEN = "Ngươi sẽ trở thành trợ thủ đắc lực của ta",		-- 物品名:"小浣猫"
        CRITTER_PERDLING = "Người bạn lông vũ của ta.",		-- 物品名:"小火鸡"
		CROW =
		{
			GENERIC = "Chỉ là một con quạ?",		-- 物品名:"乌鸦"->默认
			HELD = "Hình như ngươi không vui cho lắm.",		-- 物品名:"乌鸦"->拿在手里
		},
		CUTGRASS = "Cỏ khô để lão ngưu của ta nhai.",		-- 物品名:"草"
		CUTREEDS = "Dùng để chế giấy?",		-- 物品名:"采下的芦苇"
		CUTSTONE = "Lấy đức thu phục lòng dân.",		-- 物品名:"石砖" 制造描述:"切成正方形的石头。"
		DEADLYFEAST = "Bữa tiệc rất hữu ích.",		-- 物品名:"致命的盛宴"
		DEER =
		{
			GENERIC = "Ta có thêm 1 con, người mất đi 2 con.",		-- 物品名:"无眼鹿"->默认
			ANTLER = "Sừng hươu khiến người khác chú ý.",		-- 物品名:"无眼鹿"
		},
        DEER_ANTLER = "Có hay rớt xuống không?",		-- 物品名:"鹿角"
        DEER_GEMMED = "Bị con dã thú kia khống chế rồi!",		-- 物品名:"无眼鹿"
		DEERCLOPS = "Hay là ta cho ngươi mượn một con mắt nhé!",		-- 物品名:"独眼巨鹿"
		DEERCLOPS_EYEBALL = "Khác hoàn toàn với Thiên Nhãn của ta.",		-- 物品名:"独眼巨鹿眼球"
		EYEBRELLAHAT =	"Nó sẽ bảo vệ người đội nó.",		-- 物品名:"眼球伞" 制造描述:"面向天空的眼球让你保持干燥。"
		DEPLETED_GRASS =
		{
			GENERIC = "Cánh đồng mênh mông chắc chắn có cỏ khô.",		-- 物品名:"草"->默认
		},
        GOGGLESHAT = "Rất thời trang.... nhưng thiếu gì đó.",		-- 物品名:"时髦的护目镜" 制造描述:"你可以瞪大眼睛看装饰性护目镜。"
        DESERTHAT = "Bộ phận quan trọng lại không bảo vệ được thì có tác dụng gì.",		-- 物品名:"沙漠护目镜" 制造描述:"从你的眼睛里把沙子揉出来。"
		DEVTOOL = "Nghe giống mùi thịt xông khói!",		-- 物品名:"开发工具"
		DEVTOOL_NODEV = "Ta không đủ khỏe, không làm gì nó được.",		-- 物品名:"草"
		DIRTPILE = "Đây là đống đất... hay là dấu chân?",		-- 物品名:"可疑的土堆"
		DIVININGROD =
		{
			COLD = "Tín hiệu rất yếu.",		-- 物品名:"冻伤" 制造描述:"肯定有方法可以离开这里..."
			GENERIC = "Nó là thiết bị gì đó có thể tự động dẫn đường.",		-- 物品名:"探测杖"->默认 制造描述:"肯定有方法可以离开这里..."
			HOT = "Cái thứ này bị mát rồi!",		-- 物品名:"中暑" 制造描述:"肯定有方法可以离开这里..."
			WARM = "Ta đang đi đúng hướng.",		-- 物品名:"探测杖"->温暖 制造描述:"肯定有方法可以离开这里..."
			WARMER = "Chắc chắn rất gần đây.",		-- 物品名:"探测杖" 制造描述:"肯定有方法可以离开这里..."
		},
		DIVININGRODBASE =
		{
			GENERIC = "Ta muốn biết nó có tác dụng gì.",		-- 物品名:"探测杖底座"->默认
			READY = "Có vẻ cần phải có chìa khóa.",		-- 物品名:"探测杖底座"->准备好了
			UNLOCKED = "Bây giờ cái máy có thể hoạt động rồi!",		-- 物品名:"探测杖底座"->解锁了
		},
		DIVININGRODSTART = "Cây trượng đó có vẻ rất hữu dụng!",		-- 物品名:"探测杖底座"
		DRAGONFLY = "Đó là con chuồn chuồn!",		-- 物品名:"龙蝇"
		ARMORDRAGONFLY = "Chiến giáp nóng hừng hực, khoác lên người thật là phấn khởi!",		-- 物品名:"鳞甲" 制造描述:"脾气火爆的盔甲。"
		DRAGON_SCALES = "Đây là chiến lợi phẩm của ta.",		-- 物品名:"鳞片"
		DRAGONFLYCHEST = "Đúng là báu vật!",		-- 物品名:"巨鳞宝箱" 制造描述:"一种结实且防火的容器。"
		DRAGONFLYFURNACE = 
		{
			HAMMERED = "Có vẻ không đúng lắm.",		-- 物品名:"龙鳞火炉"->被锤了 制造描述:"给自己建造一个苍蝇暖炉。"
			GENERIC = "Rất nóng nhưng lại không sáng.", --no gems		-- 物品名:"龙鳞火炉"->默认 制造描述:"给自己建造一个苍蝇暖炉。"
			NORMAL = "Nó đang nháy mắt với ta đó à?", --one gem		-- 物品名:"龙鳞火炉"->普通 制造描述:"给自己建造一个苍蝇暖炉。"
			HIGH = "Nóng quá!", --two gems		-- 物品名:"龙鳞火炉"->高 制造描述:"给自己建造一个苍蝇暖炉。"
		},
        HUTCH = "Hutch Danglefish, P.I.",		-- 物品名:"哈奇" 制造描述:"给自己建造一个苍蝇暖炉。"
        HUTCH_FISHBOWL =
        {
            GENERIC = "Sâu xa vô cùng, nhìn như trời sao.",		-- 物品名:"星空"->默认
            WAITING = "Ngủ rất say?",		-- 物品名:"星空"->需要等待
        },
		LAVASPIT = 
		{
			HOT = "Nước miếng nóng hổi.",		-- 物品名:"中暑"
			COOL = "Ta thích gọi nó là nước miếng khô.",		-- 物品名:"龙蝇唾液"
		},
		LAVA_POND = "Đẹp đến vậy sao!",		-- 物品名:"岩浆池"
		LAVAE = "Nóng quá, không làm gì được.",		-- 物品名:"岩浆虫"
		LAVAE_COCOON = "Nguội nào, yên nào.",		-- 物品名:"冷冻虫卵"
		LAVAE_PET = 
		{
			STARVING = "Cái thứ đáng yêu như vậy nhất định đói rồi.",		-- 物品名:"超级可爱岩浆虫"->挨饿
			HUNGRY = "Ta nghe thấy bụng nó đang kêu lên.",		-- 物品名:"超级可爱岩浆虫"->有点饿了
			CONTENT = "Hình như nó đang vui.",		-- 物品名:"超级可爱岩浆虫"->内容？？？、
			GENERIC = "A. Ai nói nó là quái vật chứ?",		-- 物品名:"超级可爱岩浆虫"->默认
		},
		LAVAE_EGG = 
		{
			GENERIC = "Bên trong tỏa ra hơi ấm yếu ớt.",		-- 物品名:"岩浆虫卵"->默认
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "Ta cảm thấy trái trứng không đủ nhiệt.",		-- 物品名:"冻伤"
			COMFY = "Ta chưa từng nhìn thấy một trái trứng vui vẻ.",		-- 物品名:"岩浆虫卵"->舒服的
		},
		LAVAE_TOOTH = "Một cái răng trùng nham!",		-- 物品名:"岩浆虫尖牙"
		DRAGONFRUIT = "Trái kỳ lạ.",		-- 物品名:"火龙果"
		DRAGONFRUIT_COOKED = "Cái trái này vẫn thấy rất kỳ lạ.",		-- 物品名:"烤火龙果"
		DRAGONFRUIT_SEEDS = "Hạt giống trái cổ quái.",		-- 物品名:"火龙果种子"
		DRAGONPIE = "Cái bánh rồng lửa này ăn rất no.",		-- 物品名:"火龙果派"
		DRUMSTICK = "Có thể làm ra món đùi gà nướng rất ngon.",		-- 物品名:"鸟腿"
		DRUMSTICK_COOKED = "Ăn như hổ đói!",		-- 物品名:"炸鸟腿"
		DUG_BERRYBUSH = "Bây giờ có thể đem nó đi bất kỳ nơi đâu.",		-- 物品名:"浆果丛"
		DUG_BERRYBUSH_JUICY = "Có thể trồng cây này gần nhà.",		-- 物品名:"多汁浆果丛"
		DUG_GRASS = "Bây giờ có thể trồng ở bất kỳ chỗ nào.",		-- 物品名:"草丛"
		DUG_MARSH_BUSH = "Cái này cần phải trồng.",		-- 物品名:"尖刺灌木"
		DUG_SAPLING = "Cần xới đất, từ từ trồng.",		-- 物品名:"树苗"
		DURIAN = "Ôi, hôi quá!",		-- 物品名:"榴莲"
		DURIAN_COOKED = "Mau đem đi đi!",		-- 物品名:"超臭榴莲"
		DURIAN_SEEDS = "Hạt giống sầu riêng.",		-- 物品名:"榴莲种子"
		EARMUFFSHAT = "Dương Tiễn ta sao có thể đội cái này được chứ.",		-- 物品名:"兔耳罩" 制造描述:"毛茸茸的保暖物品。"
		EGGPLANT = "Món ngon lấp bụng.",		-- 物品名:"茄子"
		EGGPLANT_COOKED = "Để ta thử xem.",		-- 物品名:"烤茄子"
		EGGPLANT_SEEDS = "Đây là hạt giống cà tím.",		-- 物品名:"茄子种子"
		ENDTABLE = 
		{
			BURNT = "Khô mục khó giữ, giống như sương mai.",		-- 物品名:"茶几"->烧焦的 制造描述:"一张装饰桌。"
			GENERIC = "Cách cắm hoa, cũng chỉ như vậy thôi.",		-- 物品名:"茶几"->默认 制造描述:"一张装饰桌。"
			EMPTY = "Hoa nở người không cắm, thời gian trôi đi hoa còn đâu.",		-- 物品名:"茶几" 制造描述:"一张装饰桌。"
			WILTED = "Hoa như tuổi xuân, khó giữ được lâu.",		-- 物品名:"茶几"->枯萎的 制造描述:"一张装饰桌。"
			FRESHLIGHT = "Một chút ánh sáng chiếu vào nơi âm u.",		-- 物品名:"茶几"->茶几-新的发光的 制造描述:"一张装饰桌。"
			OLDLIGHT = "Đèn tắt như mất đi sự sống.", -- will be wilted soon, light radius will be very small at this point		-- 物品名:"茶几"->茶几-旧的发光的 制造描述:"一张装饰桌。"
		},
		DECIDUOUSTREE = 
		{
			BURNING = "Đáng tiếc.",		-- 物品名:"桦树"->正在燃烧
			BURNT = "Cháy thì thôi vậy",		-- 物品名:"桦树"->烧焦的
			CHOPPED = "Một gốc cây!",		-- 物品名:"桦树"->被砍了
			POISON = "Có vẻ việc hái trộm dẻ Bulô đã chọc giận nó rồi!",		-- 物品名:"桦树"->毒化的
			GENERIC = "Cành lá rậm rạp.",		-- 物品名:"桦树"->默认
		},
		ACORN = "Hạt cứng vừa miệng.",		-- 物品名:"桦木果"
        ACORN_SAPLING = "Nó sắp lớn thành cây rồi!",		-- 物品名:"桦树树苗"
		ACORN_COOKED = "Nướng rất vừa.",		-- 物品名:"烤桦木果"
		BIRCHNUTDRAKE = "Hạt dẻ tức giận.",		-- 物品名:"桦木果精"
		EVERGREEN =
		{
			BURNING = "Đáng tiếc.",		-- 物品名:"常青树"->正在燃烧
			BURNT = "Muốn khiến nó chết, phải làm nó điên.",		-- 物品名:"常青树"->烧焦的
			CHOPPED = "Một gốc cây!",		-- 物品名:"常青树"->被砍了
			GENERIC = "Mọc đầy lá kim.",		-- 物品名:"常青树"->默认
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "Đáng tiếc.",		-- 物品名:"粗壮常青树"->正在燃烧
			BURNT = "Cháy thì thôi vậy",		-- 物品名:"粗壮常青树"->烧焦的
			CHOPPED = "Một gốc cây!",		-- 物品名:"粗壮常青树"->被砍了
			GENERIC = "Cái cây tội nghiệp này không có quả.",		-- 物品名:"粗壮常青树"->默认
		},
		TWIGGYTREE = 
		{
			BURNING = "Đáng tiếc.",		-- 物品名:"繁茂的树木"->正在燃烧
			BURNT = "Cháy thì thôi vậy",		-- 物品名:"繁茂的树木"->烧焦的
			CHOPPED = "Một gốc cây!",		-- 物品名:"繁茂的树木"->被砍了
			GENERIC = "Rất ít lá.",					-- 物品名:"繁茂的树木"->默认
			DISEASED = "Trông nó không ổn, rất khác thường.",		-- 物品名:"繁茂的树木"->生病了
		},
		TWIGGY_NUT_SAPLING = "Nó sống mà không cần sự hỗ trợ nào.",		-- 物品名:"繁茂的树苗"
        TWIGGY_OLD = "Cái cây này trông rất yếu.",		-- 物品名:"繁茂的树木"
		TWIGGY_NUT = "Bên trong nó có một cái cây muốn mọc ra.",		-- 物品名:"繁茂的针叶树"
		EYEPLANT = "Ta cảm thấy có người đang nhìn ta.",		-- 物品名:"眼球草"
		INSPECTSELF = "Ta không thiếu tay chân chứ?",		-- 物品名:"繁茂的树木"
		FARMPLOT =
		{
			GENERIC = "Ta nên thử trồng ít xem sao.",		-- 物品名:未找到
			GROWING = "Lớn rồi, cái cây, lớn rồi!",		-- 物品名:未找到
			NEEDSFERTILIZER = "Nên bón phân.",		-- 物品名:未找到
			BURNT = "Cháy thành tro thì không lớn được",		-- 物品名:未找到
		},
		FEATHERHAT = "Biến thành chim!",		-- 物品名:"羽毛帽" 制造描述:"头上的装饰。"
		FEATHER_CROW = "Lông quạ.",		-- 物品名:"乌鸦羽毛"
		FEATHER_ROBIN = "Lông chim.",		-- 物品名:"红色羽毛"
		FEATHER_ROBIN_WINTER = "Trắng trẻo như tuyết.",		-- 物品名:"雪雀羽毛"
		FEATHER_CANARY = "Lông màu vàng.",		-- 物品名:"橙黄羽毛"
		FEATHERPENCIL = "Ghi chép hàng vạn quyển sách.",		-- 物品名:"羽毛笔" 制造描述:"是的，羽毛是必须的。"
		FEM_PUPPET = "Cô ấy bị nhốt rồi!",		-- 物品名:未找到
		FIREFLIES =
		{
			GENERIC = "Cô ấy thích không nhỉ?",		-- 物品名:"萤火虫"->默认
			HELD = "Bọn chúng phát sáng trong túi của ta!",		-- 物品名:"萤火虫"->拿在手里
		},
		FIREHOUND = "Cái thứ đó đang phát ra ánh sáng đỏ.",		-- 物品名:"火猎犬"
		FIREPIT =
		{
			EMBERS = "Lửa sắp tắt rồi.",		-- 物品名:"石头营火"->即将熄灭 制造描述:"一种更安全、更高效的营火。"
			GENERIC = "Xua tan bóng tối xung quanh.",		-- 物品名:"石头营火"->默认 制造描述:"一种更安全、更高效的营火。"
			HIGH = "Ngọn lửa hừng hực lướt qua ngón tay!",		-- 物品名:"石头营火"->高 制造描述:"一种更安全、更高效的营火。"
			LOW = "Lửa nhỏ lại rồi.",		-- 物品名:"石头营火"->低？ 制造描述:"一种更安全、更高效的营火。"
			NORMAL = "Thật thoải mái.",		-- 物品名:"石头营火"->普通 制造描述:"一种更安全、更高效的营火。"
			OUT = "Tìm thêm chút củi nào.",		-- 物品名:"石头营火"->出去？外面？ 制造描述:"一种更安全、更高效的营火。"
		},
		COLDFIREPIT =
		{
			EMBERS = "Lửa sắp tắt rồi.",		-- 物品名:"石头冰火"->即将熄灭 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			GENERIC = "Xua tan bóng tối xung quanh.",		-- 物品名:"石头冰火"->默认 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			HIGH = "Ngọn lửa hừng hực lướt qua ngón tay!",		-- 物品名:"石头冰火"->高 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			LOW = "Lửa nhỏ lại rồi.",		-- 物品名:"石头冰火"->低？ 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			NORMAL = "Thật thoải mái.",		-- 物品名:"石头冰火"->普通 制造描述:"燃烧效率更高，但仍然越烤越冷。"
			OUT = "Tìm thêm chút củi nào.",		-- 物品名:"石头冰火"->出去？外面？ 制造描述:"燃烧效率更高，但仍然越烤越冷。"
		},
		FIRESTAFF = "Chẳng lẽ là vật của Hỏa Đức Chân Quân?",		-- 物品名:"火魔杖" 制造描述:"利用火焰的力量！"
		FIRESUPPRESSOR = 
		{	
			ON = "Bắt đầu dập lửa!",		-- 物品名:"雪球发射器"->开启 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
			OFF = "Mọi thứ đều bình ổn rồi.",		-- 物品名:"雪球发射器"->关闭 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
			LOWFUEL = "Nhiên liệu sắp hết rồi.",		-- 物品名:"雪球发射器"->燃料不足 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		},
		FISH = "Chàng trai trẻ, cầm cần đốt thời gian.",		-- 物品名:"鱼" 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		FISHINGROD = "Móc câu, dây câu và cần câu!",		-- 物品名:"钓竿" 制造描述:"去钓鱼。为了鱼。"
		FISHSTICKS = "Cái này có thể lấp đầy bụng.",		-- 物品名:"炸鱼排" 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		FISHTACOS = "Xốp giòn rất ngon!",		-- 物品名:"鱼肉玉米卷" 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		FISH_COOKED = "Nướng rất đều.",		-- 物品名:"烤鱼" 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		FLINT = "Một viên đá cứng.",		-- 物品名:"燧石" 制造描述:"拯救植物，扑灭火焰，可添加燃料。"
		FLOWER = 
		{
            GENERIC = "Hoa đẹp xứng mỹ nhân, mỹ nhân gả lang quân.",		-- 物品名:"花"->默认
            ROSE = "Hoa hồng tặng người.",		-- 物品名:"花"->玫瑰
        },
        FLOWER_WITHERED = "Ta thấy nó phơi nắng chưa đủ đâu.",		-- 物品名:"枯萎的花"
		FLOWERHAT = "Đội lên có thể giảm tụt não.",		-- 物品名:"花环" 制造描述:"放松神经的东西。"
		FLOWER_EVIL = "Hơi thở tà ác",		-- 物品名:"邪恶花"
		FOLIAGE = "Có từ cái cây nhiều lá.",		-- 物品名:"蕨叶"
		FOOTBALLHAT = "Ta không thích vận động.",		-- 物品名:"橄榄球头盔" 制造描述:"保护你的脑壳。"
        FOSSIL_PIECE = "Ma khí như giòi trong xương chân.",		-- 物品名:"化石碎片"
        FOSSIL_STALKER =
        {
			GENERIC = "Vẫn còn một số mảnh vỡ chưa tìm được.",		-- 物品名:"化石骨架"->默认
			FUNNY = "Trực giác ta nói rằng có gì đó không đúng.",		-- 物品名:"化石骨架"->趣味？？
			COMPLETE = "Nó còn sống! Ồ còn thiếu một trái tim.",		-- 物品名:"化石骨架"->准备好了
        },
        STALKER = "Hài cốt và bóng tối dung hợp với nhau!",		-- 物品名:"复活的骨架"
        STALKER_ATRIUM = "Tại sao nó phải lớn như vậy chứ?",		-- 物品名:"远古织影者"
        STALKER_MINION = "Nó sẽ cắn vào mắt cá chân đó!",		-- 物品名:"编织暗影"
        THURIBLE = "Khói đen tỏa xung quanh.",		-- 物品名:"影子香炉"
        ATRIUM_OVERGROWTH = "Ta không biết những ký hiệu này.",		-- 物品名:"古代的方尖碑"
		FROG =
		{
			DEAD = "Ghê tởm.",		-- 物品名:"青蛙"->死了
			GENERIC = "Nó thật đáng yêu!",		-- 物品名:"青蛙"->默认
			SLEEPING = "A, nhìn cái tướng ngủ của nó kìa!",		-- 物品名:"青蛙"->睡着了
		},
		FROGGLEBUNWICH = "Sandwich đùi.",		-- 物品名:"蛙腿三明治"
		FROGLEGS = "Ta nghe nói nó sẽ trở thành món ngon.",		-- 物品名:"蛙腿"
		FROGLEGS_COOKED = "Mùi giống thịt gà.",		-- 物品名:"烤蛙腿"
		FRUITMEDLEY = "Hương thơm nức mũi.",		-- 物品名:"水果圣代"
		FURTUFT = "Lông trắng đen.", 		-- 物品名:"毛丛"
		GEARS = "Một linh kiện máy móc.",		-- 物品名:"齿轮"
		GHOST = "Mạo phạm thiên thần, đày xuống A Tỳ.",		-- 物品名:"幽灵"
		GOLDENAXE = "Một cây rìu thật là đẹp.",		-- 物品名:"黄金斧头" 制造描述:"砍树也要有格调！"
		GOLDENPICKAXE = "Không phải vàng có đặc tính mềm sao?",		-- 物品名:"黄金鹤嘴锄" 制造描述:"像大Boss一样砸碎岩石。"
		GOLDENPITCHFORK = "Ta có thể khiến cái nĩa trở nên tinh xảo như vậy sao?",		-- 物品名:"黄金干草叉" 制造描述:"重新布置整个世界。"
		GOLDENSHOVEL = "Chúng ta phải đào hố mới được.",		-- 物品名:"黄金铲子" 制造描述:"挖掘作用相同，但使用寿命更长。"
		GOLDNUGGET = "Vật phàm tục.",		-- 物品名:"金块"
		GRASS =
		{
			BARREN = "Nó cần phân.",		-- 物品名:"草"
			WITHERED = "Khô héo rồi.",		-- 物品名:"草"->枯萎了
			BURNING = "Cháy nhanh thật!",		-- 物品名:"草"->正在燃烧
			GENERIC = "Một bụi cỏ.",		-- 物品名:"草"->默认
			PICKED = "Nó bị cắt vào thời điểm phát triển tốt nhất.",		-- 物品名:"草"->被采完了
			DISEASED = "Trông nó không khỏe lắm.",		-- 物品名:"草"->生病了
			DISEASING = "Ô... có một số chỗ không ổn.",		-- 物品名:"草"->正在生病？？
		},
		GRASSGEKKO = 
		{
			GENERIC = "Đây là con tắc kè nhiều cỏ.",			-- 物品名:"草地壁虎"->默认
			DISEASED = "Trông nó không khỏe lắm.",		-- 物品名:"草地壁虎"->生病了
		},
		GREEN_CAP = "Trông nó rất bình thường.",		-- 物品名:"采摘的绿蘑菇"
		GREEN_CAP_COOKED = "Tỉnh táo nào...",		-- 物品名:"烤绿蘑菇"
		GREEN_MUSHROOM =
		{
			GENERIC = "Là nấm.",		-- 物品名:"绿蘑菇"->默认
			INGROUND = "Đang ngủ.",		-- 物品名:"绿蘑菇"->在地里面
			PICKED = "Không biết nó có mọc lại không?",		-- 物品名:"绿蘑菇"->被采完了
		},
		GUNPOWDER = "Tốt nhất ngươi không nên thử.",		-- 物品名:"火药" 制造描述:"一把火药。"
		HAMBAT = "Đùi ướp thượng hạng.",		-- 物品名:"火腿棒" 制造描述:"舍不得火腿套不着狼。"
		HAMMER = "Phá mọi thứ!",		-- 物品名:"锤子" 制造描述:"敲碎各种东西。"
		HEALINGSALVE = "Đồ trị thương đơn giản.",		-- 物品名:"治疗药膏" 制造描述:"对割伤和擦伤进行消毒杀菌。"
		HEATROCK =
		{
			FROZEN = "Lạnh thấu xương.",		-- 物品名:"暖石"->冰冻 制造描述:"储存热能供旅行途中使用。"
			COLD = "Một cục đá lạnh.",		-- 物品名:"冻伤" 制造描述:"储存热能供旅行途中使用。"
			GENERIC = "Đồ giữ nhiệt.",		-- 物品名:"暖石"->默认 制造描述:"储存热能供旅行途中使用。"
			WARM = "Giữ ấm tâm hồn bạn!",		-- 物品名:"暖石"->温暖 制造描述:"储存热能供旅行途中使用。"
			HOT = "Cứ như ngâm trong suối nước nóng.",		-- 物品名:"中暑" 制造描述:"储存热能供旅行途中使用。"
		},
		HOME = "Chắc chắn có người ở đây.",		-- 物品名:"暖石" 制造描述:"储存热能供旅行途中使用。"
		HOMESIGN =
		{
			GENERIC = "Phía trên viết Ngươi Đang Ở Đây.",		-- 物品名:"路牌"->默认 制造描述:"在世界中留下你的标记。"
            UNWRITTEN = "Cái bảng này đang để không.",		-- 物品名:"路牌"->没有写字 制造描述:"在世界中留下你的标记。"
			BURNT = "“Đừng đùa với lửa.”",		-- 物品名:"路牌"->烧焦的 制造描述:"在世界中留下你的标记。"
		},
		ARROWSIGN_POST =
		{
			GENERIC = "Nó nói Hướng Này Nè.",		-- 物品名:"指路标志"->默认 制造描述:"对这个世界指指点点。或许只是打下手势。"
            UNWRITTEN = "Cái bảng này đang để không.",		-- 物品名:"指路标志"->没有写字 制造描述:"对这个世界指指点点。或许只是打下手势。"
			BURNT = "Đừng đùa với lửa.",		-- 物品名:"指路标志"->烧焦的 制造描述:"对这个世界指指点点。或许只是打下手势。"
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "Nó nói Hướng Này Nè.",		-- 物品名:"指路标志"->默认
            UNWRITTEN = "Cái bảng này đang để không.",		-- 物品名:"指路标志"->没有写字
			BURNT = "Đừng đùa với lửa.",		-- 物品名:"指路标志"->烧焦的
		},
		HONEY = "Thật là ngọt!",		-- 物品名:"蜂蜜"
		HONEYCOMB = "Mật ong ở trong đó.",		-- 物品名:"蜂巢"
		HONEYHAM = "Thơm ngọt vừa miệng.",		-- 物品名:"蜜汁火腿"
		HONEYNUGGETS = "Mùi giống thịt gà, nhưng ta thấy nó không phải là thịt gà.",		-- 物品名:"蜜汁卤肉"
		HORN = "Nghe có vẻ trong đó là nông trại bò.",		-- 物品名:"牛角"
		HOUND = "Con súc sinh không biết tốt xấu!",		-- 物品名:"猎犬"
		HOUNDBONE = "Xương khô mà thôi.",		-- 物品名:"犬骨"
		HOUNDMOUND = "Hao Thiên đi thám thính tình hình.",		-- 物品名:"猎犬丘"
		ICEBOX = "Lúc nhỏ, cha thường dùng miệng giếng để thay thế cái này!",		-- 物品名:"冰箱" 制造描述:"延缓食物变质。"
		ICEHAT = "Giữ lạnh.",		-- 物品名:"冰帽" 制造描述:"用科学技术合成的冰帽。"
		ICEHOUND = "Mỗi mùa đều có chó săn sao?",		-- 物品名:"冰猎犬"
		INSANITYROCK =
		{
			ACTIVE = "Người có độ minh mẫn bình thường như ta cũng phải chịu thôi!",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "Nó giống như một cái kim tự tháp hơn là tháp đá.",		-- 物品名:"方尖碑"->没有激活
		},
		JAMMYPRESERVES = "Có lẽ nên kiếm cái vại.",		-- 物品名:"果酱"
		KABOBS = "Bữa trưa trên xiên que.",		-- 物品名:"肉串"
		KILLERBEE =
		{
			GENERIC = "Ong sát thủ!",		-- 物品名:"杀人蜂"->默认
			HELD = "Cái thứ này thật nguy hiểm.",		-- 物品名:"杀人蜂"->拿在手里
		},
		KNIGHT = "Nào! Đấu với ta một trận!",		-- 物品名:"đồng hồ mã"
		KOALEFANT_SUMMER = "Nó chắc chắn rất là ngon.",		-- 物品名:"大象"
		KOALEFANT_WINTER = "Trông nó vừa ấm, thịt vừa nhiều.",		-- 物品名:"大象"
		KRAMPUS = "Nó muốn trộm đồ của ta!",		-- 物品名:"坎普斯"
		KRAMPUS_SACK = "Toàn là chất nhầy của Krampus.",		-- 物品名:"坎普斯背包"
		LEIF = "Dám đấu 1 trận không!",		-- 物品名:"树精守卫"
		LEIF_SPARSE = "Dám đấu 1 trận không!",		-- 物品名:"树精守卫"
		LIGHTER = "Vận may của cô ấy là cái quẹt.",		-- 物品名:"Willow的打火机" 制造描述:"火焰在雨中彻夜燃烧。"
		LIGHTNING_ROD =
		{
			CHARGED = "Dương Tiễn ở đây, thu hết sấm sét!",		-- 物品名:"避雷针" 制造描述:"防雷劈。"
			GENERIC = "Dương Tiễn ở đây, thu hết sấm sét!",		-- 物品名:"避雷针"->默认 制造描述:"防雷劈。"
		},
		LIGHTNINGGOAT = 
		{
			GENERIC = "Be!",		-- 物品名:"闪电羊"->默认
			CHARGED = "Ta không cho rằng nó thích bị sét đánh.",		-- 物品名:"闪电羊"
		},
		LIGHTNINGGOATHORN = "Nó giống như cột thu lôi mini.",		-- 物品名:"闪电羊角"
		GOATMILK = "Thơm ngon, và bốc lên nghi ngút!",		-- 物品名:"带电的羊奶"
		LITTLE_WALRUS = "Đợi nó lớn lên, thì sẽ không đáng yêu ngây thơ như vậy nữa.",		-- 物品名:"小海象人"
		LIVINGLOG = "Khúc gỗ này chứa đầy oán hận.",		-- 物品名:"活木头" 制造描述:"用你的身体来代替你该干的活吧。"
		LOG =
		{
			BURNING = "Khúc gỗ nóng!",		-- 物品名:"木头"->正在燃烧
			GENERIC = "Khúc gỗ.",		-- 物品名:"木头"->默认
		},
		LUCY = "Cái rìu này còn đẹp hơn cái mà hồi trước ta từng dùng.",		-- 物品名:"露西斧"
		LUREPLANT = "Nó khiến người ta mê mẩn.",		-- 物品名:"食人花"
		LUREPLANTBULB = "Bây giờ ta có thể xây dựng cánh đồng thịt của ta rồi.",		-- 物品名:"食人花种子"
		MALE_PUPPET = "Nó bị nhốt rồi!",		-- 物品名:"木头"
		MANDRAKE_ACTIVE = "Nhân sâm?",		-- 物品名:"曼德拉草"
		MANDRAKE_PLANTED = "Ta từng nghe một số lời đồn kỳ lạ về cái này.",		-- 物品名:"曼德拉草"
		MANDRAKE = "Ta từng nghe nói về ngươi.",		-- 物品名:"曼德拉草"
        MANDRAKESOUP = "Ồ, nó sẽ không còn tỉnh lại nữa.",		-- 物品名:"曼德拉草汤"
        MANDRAKE_COOKED = "Nó trông có vẻ hết kỳ dị rồi.",		-- 物品名:"木头"
        MAPSCROLL = "Một tờ giấy trắng. Trông có vẻ vô dụng.",		-- 物品名:"地图" 制造描述:"向别人展示你看到什么！"
        MARBLE = "Cực phẩm!",		-- 物品名:"大理石"
        MARBLEBEAN = "Ta đem trâu nhà ta đi đổi đây.",		-- 物品名:"大理石豌豆" 制造描述:"种植一片大理石森林。"
        MARBLEBEAN_SAPLING = "Hình như có khắc cái gì đó.",		-- 物品名:"大理石芽"
        MARBLESHRUB = "Hình như cũng có lý.",		-- 物品名:"大理石灌木"
        MARBLEPILLAR = "Ta nghĩ ta có cách dùng nó.",		-- 物品名:"大理石柱"
        MARBLETREE = "Ta nghĩ rìu không thể chặt được nó.",		-- 物品名:"大理石树"
        MARSH_BUSH =
        {
            BURNING = "Cháy nhanh thật!",		-- 物品名:"尖刺灌木"->正在燃烧
            GENERIC = "Trông nó nhiều gai ghê.",		-- 物品名:"尖刺灌木"->默认
            PICKED = "Ôi chao!",		-- 物品名:"尖刺灌木"->被采完了
        },
        BURNT_MARSH_BUSH = "Cháy hết toàn bộ.",		-- 物品名:"尖刺灌木"
        MARSH_PLANT = "Đây là một cái cây.",		-- 物品名:"池塘边植物"
        MARSH_TREE =
        {
            BURNING = "Gai nhọn! Lửa!",		-- 物品名:"针刺树"->正在燃烧
            BURNT = "Bây giờ nó là một cây gai bị cháy.",		-- 物品名:"针刺树"->烧焦的
            CHOPPED = "Bây giờ không còn nhiều gai nữa rồi!",		-- 物品名:"针刺树"->被砍了
            GENERIC = "Mấy cái gai đó trông rất bén!",		-- 物品名:"针刺树"->默认
        },
        MAXWELL = "Ta ghét tên đó.",		-- 物品名:"麦斯威尔"
        MAXWELLHEAD = "Ta có thể thấy thứ trong lỗ chân lông của nó.",		-- 物品名:"麦斯威尔的头"
        MAXWELLLIGHT = "Ta muốn biết bọn họ làm như thế nào.",		-- 物品名:"麦斯威尔灯"
        MAXWELLLOCK = "Trông như lỗ khóa.",		-- 物品名:"噩梦锁"
        MAXWELLTHRONE = "Trông nó không thoải mái chút nào.",		-- 物品名:"噩梦王座"
        MEAT = "Tuy hơi tanh, nhưng vẫn tạm ổn.",		-- 物品名:"肉"
        MEATBALLS = "Nó chỉ là một cục thịt mà thôi.",		-- 物品名:"肉丸"
        MEATRACK =
        {
            DONE = "Thịt khô ăn được rồi!",		-- 物品名:"晾肉架"->完成了 制造描述:"晾肉的架子。"
            DRYING = "Thịt khô cần một khoảng thời gian.",		-- 物品名:"晾肉架"->正在风干 制造描述:"晾肉的架子。"
            DRYINGINRAIN = "Trời mưa thì cần thời gian dài.",		-- 物品名:"晾肉架"->雨天风干 制造描述:"晾肉的架子。"
            GENERIC = "Ta nên phơi khô ít thịt.",		-- 物品名:"晾肉架"->默认 制造描述:"晾肉的架子。"
            BURNT = "Giá phơi cháy rồi.",		-- 物品名:"晾肉架"->烧焦的 制造描述:"晾肉的架子。"
        },
        MEAT_DRIED = "Thịt này đủ khô rồi.",		-- 物品名:"风干肉" 制造描述:"晾肉的架子。"
        MERM = "Rất tanh mùi cá!",		-- 物品名:"鱼人" 制造描述:"晾肉的架子。"
        MERMHEAD =
        {
            GENERIC = "Lúc nào cũng ngửi thấy, hôi chết ta rồi.",		-- 物品名:"鱼头"->默认
            BURNT = "Mùi thịt người cá cháy càng không ổn chút nào.",		-- 物品名:"鱼头"->烧焦的
        },
        MERMHOUSE =
        {
            GENERIC = "Ai sẽ ở nơi này chứ?",		-- 物品名:"鱼人屋"->默认
            BURNT = "Bây giờ không có chỗ ở rồi.",		-- 物品名:"鱼人屋"->烧焦的
        },
        MINERHAT = "Thiết bị chiếu sáng.",		-- 物品名:"矿工帽" 制造描述:"用你脑袋上的灯照亮夜晚。"
        MONKEY = "Cái tên hiếu kỳ.",		-- 物品名:"暴躁猴"
        MONKEYBARREL = "Cái thứ đó vừa động đậy phải không?",		-- 物品名:"猴子桶"
        MONSTERLASAGNA = "Món này xúc phạm người ăn.",		-- 物品名:"怪物千层饼"
        FLOWERSALAD = "Một đĩa lá xanh.",		-- 物品名:"花沙拉"
        ICECREAM = "Ta rất muốn ăn kem!",		-- 物品名:"冰淇淋"
        WATERMELONICLE = "Dưa hấu lạnh.",		-- 物品名:"西瓜冰棍"
        TRAILMIX = "Điểm tâm khỏe mạnh.",		-- 物品名:"什锦干果"
        HOTCHILI = "Cay đến nỗi phun lửa!",		-- 物品名:"辣椒炖肉"
        GUACAMOLE = "Món ăn mà Avogadro thích nhất.",		-- 物品名:"鳄梨酱"
        MONSTERMEAT = "Ồ, ta không nên ăn cái này.",		-- 物品名:"怪物肉"
        MONSTERMEAT_DRIED = "Thịt khô có mùi kỳ lạ.",		-- 物品名:"怪物肉干"
        MOOSE = "Cái thứ đó là gì vậy.",		-- 物品名:"鱼人屋"
        MOOSE_NESTING_GROUND = "Nó để con nó ở đây.",		-- 物品名:"鱼人屋"
        MOOSEEGG = "Những con con dường như đang cố gắng tránh bị giật điện.",		-- 物品名:"鱼人屋"
        MOSSLING = "Ha ha! Chắc chắn ngươi không phải là tia điện.",		-- 物品名:"驼鹿鹅幼崽"
        FEATHERFAN = "Công dụng hạ nhiệt.",		-- 物品名:"羽毛扇" 制造描述:"超柔软、超大的扇子。"
        MINIFAN = "Gió thổi từ phía sau trở nên mạnh hơn.",		-- 物品名:"旋转的风扇" 制造描述:"你得跑起来，才能带来风！"
        GOOSE_FEATHER = "Lông xù lên!",		-- 物品名:"鹿鸭羽毛"
        STAFF_TORNADO = "Sức mạnh hủy diệt của gió lốc.",		-- 物品名:"天气风向标" 制造描述:"把你的敌人吹走。"
        MOSQUITO =
        {
            GENERIC = "Con muỗi đáng ghét.",		-- 物品名:"蚊子"->默认
            HELD = "Ê, đó là máu của ta phải không?",		-- 物品名:"蚊子"->拿在手里
        },
        MOSQUITOSACK = "Đây là máu...",		-- 物品名:"蚊子血囊"
        MOUND =
        {
            DUG = "Có lẽ, hắn xứng đáng với điều này.",		-- 物品名:"坟墓"->被挖了
            GENERIC = "Ta dám cá ở dưới có đồ ngon đủ loại!",		-- 物品名:"坟墓"->默认
        },
        NIGHTLIGHT = "Ác mộng đang cháy, nhưng cũng không ảnh hưởng đến lý trí của ta.",		-- 物品名:"魂灯" 制造描述:"用你的噩梦点亮夜晚。"
        NIGHTMAREFUEL = "Đao hạ vong phách! Mau đi đầu thai!",		-- 物品名:"噩梦燃料" 制造描述:"傻子和疯子使用的邪恶残渣。"
        NIGHTSWORD = "Lưỡng bại câu thương.",		-- 物品名:"暗夜剑" 制造描述:"造成噩梦般的伤害。"
        NITRE = "Ta không phải là nhà địa chất.",		-- 物品名:"硝石"
        ONEMANBAND = "Chúng ta nên thêm cái chuông bò.",		-- 物品名:"独奏乐器" 制造描述:"疯子音乐家也有粉丝。"
        OASISLAKE = "Nước trong.",		-- 物品名:"湖泊"
        PANDORASCHEST = "Chắc chắn bên trong có đồ tốt!",		-- 物品名:"华丽箱子"
        PANFLUTE = "Hát bài dạ khúc cho động vật nghe.",		-- 物品名:"排箫" 制造描述:"抚慰凶猛野兽的音乐。"
        PAPYRUS = "Những tờ giấy.",		-- 物品名:"莎草纸" 制造描述:"用于书写。"
        WAXPAPER = "Những tờ giấy sáp.",		-- 物品名:"蜡纸" 制造描述:"用于打包东西。"
        PENGUIN = "Chắc chắn đến mùa sinh sản rồi.",		-- 物品名:"企鹅"
        PERD = "Con chim ngu ngốc! Tránh xa quả dâu ra!",		-- 物品名:"火鸡"
        PEROGIES = "Mùi vị của mấy cái này không tệ.",		-- 物品名:"波兰水饺"
        PETALS = "Cho mấy bông hoa này biết ai mới là lão đại!",		-- 物品名:"花瓣"
        PETALS_EVIL = "Không cầm nó được không vậy?",		-- 物品名:"恶魔花瓣"
        PHLEGM = "Nhầy nhụa và trơn trượt, mùi vị mặn mặn.",		-- 物品名:"脓鼻涕"
        PICKAXE = "Muốn đập đá không?",		-- 物品名:"鹤嘴锄" 制造描述:"凿碎岩石。"
        PIGGYBACK = "Heo con về... nhà rồi.",		-- 物品名:"猪皮包" 制造描述:"能装许多东西，但会减慢你的速度。"
        PIGHEAD =
        {
            GENERIC = "Trông như vật hiến tế của dã thú.",		-- 物品名:"猪头"->默认
            BURNT = "Trong mềm ngoài xốp.",		-- 物品名:"猪头"->烧焦的
        },
        PIGHOUSE =
        {
            FULL = "Động Vân Sạn?",		-- 物品名:"猪屋"->满了 制造描述:"可以住一只猪。"
            GENERIC = "Nhà của đám heo này cũng không tệ.",		-- 物品名:"猪屋"->默认 制造描述:"可以住一只猪。"
            LIGHTSOUT = "Đi nghỉ đi!",		-- 物品名:"猪屋"->关灯了 制造描述:"可以住一只猪。"
            BURNT = "Hy vọng không sao đi.",		-- 物品名:"猪屋"->烧焦的 制造描述:"可以住一只猪。"
        },
        PIGKING = "Trông còn ngu hơn tên Bát Giới đó.",		-- 物品名:"猪王" 制造描述:"可以住一只猪。"
        PIGMAN =
        {
            DEAD = "Siêu thoát.",		-- 物品名:"猪人"->死了
            FOLLOWER = "Từ nay về sau ngươi đi theo ta.",		-- 物品名:"猪人"->追随者
            GENERIC = "Tuy là yêu quái nhưng không có yêu khí.",		-- 物品名:"猪人"->默认
            GUARD = "Trông rất nghiêm.",		-- 物品名:"猪人"->警戒
            WEREPIG = "Tẩu hỏa nhập ma!",		-- 物品名:"猪人"->疯猪
        },
        PIGSKIN = "Phía trên vẫn còn đuôi.",		-- 物品名:"猪皮"
        PIGTENT = "Mùi khá giống thịt xông khói.",		-- 物品名:"猪人"
        PIGTORCH = "Trông khá thoải mái.",		-- 物品名:"猪火炬"
        PINECONE = "Ta có thể thấy bên trong có cây nhỏ đang cố gắng lớn lên.",		-- 物品名:"松果"
        PINECONE_SAPLING = "Nó sắp phát triển thành cái cây to lớn rồi!",		-- 物品名:"常青树苗"
        LUMPY_SAPLING = "Cái cây này mọc bằng cách nào vậy?",		-- 物品名:"有疙瘩的树苗"
        PITCHFORK = "Bây giờ ta chỉ cần tham gia vào đám bạo loạn.",		-- 物品名:"干草叉" 制造描述:"铲地用具。"
        PLANTMEAT = "Trông chẳng ra làm sao.",		-- 物品名:"食人花肉"
        PLANTMEAT_COOKED = "Chí ít nó bị nấu chín rồi.",		-- 物品名:"烤食人花肉"
        PLANT_NORMAL =
        {
            GENERIC = "Cành lá rậm rạp.",		-- 物品名:"农作物"->默认
            GROWING = "Ôi, lớn chậm quá!",		-- 物品名:"农作物"->正在生长
            READY = "Thu hoạch nào.",		-- 物品名:"农作物"->准备好了
            WITHERED = "Nó chết vì nóng.",		-- 物品名:"农作物"->枯萎了
        },
        POMEGRANATE = "Thơm ngọt.",		-- 物品名:"石榴"
        POMEGRANATE_COOKED = "Xử lý rất chuyên nghiệp!",		-- 物品名:"切片熟石榴"
        POMEGRANATE_SEEDS = "Đây là hạt giống thạch lựu.",		-- 物品名:"石榴种子"
        POND = "Nhìn không thấy đáy!",		-- 物品名:"池塘"
        POOP = "Tránh xa ta ra!",		-- 物品名:"肥料"
        FERTILIZER = "Một thùng phân.",		-- 物品名:"便便桶" 制造描述:"少拉点在手上，多拉点在庄稼上。"
        PUMPKIN = "Bí đỏ mềm dẻo.",		-- 物品名:"南瓜"
        PUMPKINCOOKIE = "Bánh bí đỏ thơm ngon!",		-- 物品名:"南瓜饼"
        PUMPKIN_COOKED = "Tại sao nó không thành bánh nướng chứ?",		-- 物品名:"烤南瓜"
        PUMPKIN_LANTERN = "Thật đáng sợ!",		-- 物品名:"南瓜灯" 制造描述:"长着鬼脸的照明用具。"
        PUMPKIN_SEEDS = "Đây là hạt giống bí đỏ.",		-- 物品名:"南瓜种子"
        PURPLEAMULET = "Bóng tối đang thì thầm với ta.",		-- 物品名:"噩梦护符" 制造描述:"引起精神错乱。"
        PURPLEGEM = "Nó chứa năng lượng thần bí.",		-- 物品名:"紫宝石" 制造描述:"由两种颜色的宝石合成！"
        RABBIT =
        {
            GENERIC = "Ngươi đang tìm cà rốt sao?",		-- 物品名:"兔子"->默认
            HELD = "Hao Thiên Khuyển bắt được rồi sao?",		-- 物品名:"兔子"->拿在手里
        },
        RABBITHOLE =
        {
            GENERIC = "Chỉ là hang thỏ thôi.",		-- 物品名:"兔洞"->默认
            SPRING = "Sụp rồi, không biết có bị gì không.",		-- 物品名:"兔洞"->春天 or 潮湿
        },
        RAINOMETER =
        {
            GENERIC = "Nó có thể dự đoán được mưa hay không.",		-- 物品名:"雨量计"->默认 制造描述:"观测降雨机率。"
            BURNT = "Bộ phận đo lường có khói xanh rồi.",		-- 物品名:"雨量计"->烧焦的 制造描述:"观测降雨机率。"
        },
        RAINCOAT = "Áo che mưa.",		-- 物品名:"雨衣" 制造描述:"让你保持干燥的防水外套。"
        RAINHAT = "Tóc rối rồi.",		-- 物品名:"防雨帽" 制造描述:"手感柔软，防雨必备。"
        RATATOUILLE = "Đây là món ăn rất tốt.",		-- 物品名:"蔬菜大杂烩" 制造描述:"观测降雨机率。"
        RAZOR = "Cột đá bén lên gậy nhỏ. Là dụng cụ vệ sinh cá nhân!",		-- 物品名:"剃刀" 制造描述:"剃掉你脏脏的山羊胡子。"
        REDGEM = "Nó lóe lên ánh sáng ấm áp.",		-- 物品名:"红宝石" 制造描述:"观测降雨机率。"
        RED_CAP = "Cái mùi kỳ quái.",		-- 物品名:"采摘的红蘑菇" 制造描述:"观测降雨机率。"
        RED_CAP_COOKED = "Bây giờ khác với nấm sống rồi...",		-- 物品名:"烤红蘑菇" 制造描述:"观测降雨机率。"
        RED_MUSHROOM =
        {
            GENERIC = "Là nấm.",		-- 物品名:"红蘑菇"->默认
            INGROUND = "Nó đang ngủ.",		-- 物品名:"红蘑菇"->在地里面
            PICKED = "Không biết nó mọc lại không?",		-- 物品名:"红蘑菇"->被采完了
        },
        REEDS =
        {
            BURNING = "Cháy rất lớn.",		-- 物品名:"芦苇"->正在燃烧
            GENERIC = "Một nhúm sậy.",		-- 物品名:"芦苇"->默认
            PICKED = "Tất cả sậy dùng được đều được cắt.",		-- 物品名:"芦苇"->被采完了
        },
        RELIC = "Đồ dùng trong nhà cổ xưa.",		-- 物品名:"废墟"
        RUINS_RUBBLE = "Cái này có thể sửa được.",		-- 物品名:"损毁的废墟"
        RUBBLE = "Chỉ là mấy cục đá nát thôi.",		-- 物品名:"碎石"
        RESEARCHLAB =
        {
            GENERIC = "Một sản phẩm của khoa học kỹ thuật.",		-- 物品名:"科学机器"->默认 制造描述:"解锁新的合成配方！"
            BURNT = "Khoa học? Đồ chơi sợ lửa.",		-- 物品名:"科学机器"->烧焦的 制造描述:"解锁新的合成配方！"
        },
        RESEARCHLAB2 =
        {
            GENERIC = "Nhật nguyệt luân chuyển, phân tách vạn vật.",		-- 物品名:"炼金引擎"->默认 制造描述:"解锁更多合成配方！"
            BURNT = "Khoa học đến cỡ nào cũng không thể tránh được lửa.",		-- 物品名:"炼金引擎"->烧焦的 制造描述:"解锁更多合成配方！"
        },
        RESEARCHLAB3 =
        {
            GENERIC = "Ai to gan như vậy? Không sợ bị phản phệ!",		-- 物品名:"暗影操控器"->默认 制造描述:"这还是科学吗？"
            BURNT = "Bất luận nó là gì, thì vẫn bị cháy thôi.",		-- 物品名:"暗影操控器"->烧焦的 制造描述:"这还是科学吗？"
        },
        RESEARCHLAB4 =
        {
            GENERIC = "Yêu khí?",		-- 物品名:"灵子分解器"->默认 制造描述:"增强高礼帽的魔力。"
            BURNT = "Lửa cũng không thật sự giải quyết được mọi vấn đề...",		-- 物品名:"灵子分解器"->烧焦的 制造描述:"增强高礼帽的魔力。"
        },
        RESURRECTIONSTATUE =
        {
            GENERIC = "Vật thế thân.",		-- 物品名:"替身人偶"->默认 制造描述:"以肉的力量让你重生。"
            BURNT = "Không dùng được nữa rồi.",		-- 物品名:"替身人偶"->烧焦的 制造描述:"以肉的力量让你重生。"
        },
        RESURRECTIONSTONE = "Chạm vào bệ đá là ý tưởng hay.",		-- 物品名:"复活石" 制造描述:"以肉的力量让你重生。"
        ROBIN =
        {
            GENERIC = "Mùa đông đã kết thúc rồi sao?",		-- 物品名:"红雀"->默认
            HELD = "Nó thích cái túi của ta.",		-- 物品名:"红雀"->拿在手里
        },
        ROBIN_WINTER =
        {
            GENERIC = "Sinh mạng của vùng đất lạnh giá.",		-- 物品名:"雪雀"->默认
            HELD = "Nó mềm mại như vậy sao.",		-- 物品名:"雪雀"->拿在手里
        },
        ROBOT_PUPPET = "Bị nhốt rồi!",		-- 物品名:"雪雀"
        ROCK_LIGHT =
        {
            GENERIC = "Hố dung nham bao bọc bởi lớp đá.",		-- 物品名:"熔岩坑"->默认
            OUT = "Trong rất dễ phá.",		-- 物品名:"熔岩坑"->出去？外面？
            LOW = "Lớp dung nham đang bao lấy lớp đá.",		-- 物品名:"熔岩坑"->低？
            NORMAL = "Thật thoải mái.",		-- 物品名:"熔岩坑"->普通
        },
        CAVEIN_BOULDER =
        {
            GENERIC = "Ta nghĩ rằng ta có thể nâng nó lên được.",		-- 物品名:"岩石"->默认
            RAISED = "Quá cao rồi.",		-- 物品名:"岩石"->提高了？
        },
        ROCK = "Túi của ta không đựng được nó.",		-- 物品名:"岩石"
        PETRIFIED_TREE = "Nhìn sợ ngây người rồi.",		-- 物品名:"岩石"
        ROCK_PETRIFIED_TREE = "Trông thật đáng sợ.",		-- 物品名:"石化树"
        ROCK_PETRIFIED_TREE_OLD = "Trông thật đáng sợ.",		-- 物品名:"岩石"
        ROCK_ICE =
        {
            GENERIC = "Thấy ngươi thì thấy lạnh rồi.",		-- 物品名:"冰矿"->默认
            MELTED = "Nó trở nên vô dụng khi chưa đóng băng.",		-- 物品名:"冰矿"->融化了
        },
        ROCK_ICE_MELTED = "Nó trở nên vô dụng khi chưa đóng băng.",		-- 物品名:"融化的冰矿"
        ICE = "Thấy ngươi là thấy lạnh rồi.",		-- 物品名:"冰"
        ROCKS = "Chúng ta có thể dùng nó để chế đồ.",		-- 物品名:"石头"
        ROOK = "Công chiếm thành trì!",		-- 物品名:"发条战车"
        ROPE = "Một loại dây thừng ngắn.",		-- 物品名:"绳子" 制造描述:"紧密编成的草绳，非常有用。"
        ROTTENEGG = "Ọe! Hôi chết đi được!",		-- 物品名:"腐烂鸟蛋"
        ROYAL_JELLY = "Cực phẩm thượng đẳng!",		-- 物品名:"蜂王浆"
        JELLYBEAN = "Vừa có thạch vừa có đậu hà lan.",		-- 物品名:"彩虹糖豆"
        SADDLE_BASIC = "Có nó thì có thể cưỡi con đó rồi",		-- 物品名:"鞍具" 制造描述:"你坐在动物身上。仅仅是理论上。"
        SADDLE_RACE = "Cái yên đang bay!",		-- 物品名:"轻鞍具" 制造描述:"抵消掉完成目标所花费的时间。或许。"
        SADDLE_WAR = "Vấn đề duy nhất đó là cái yên của đội trưởng mòn rồi.",		-- 物品名:"战争鞍具" 制造描述:"战场首领的王位。"
        SADDLEHORN = "Cái này có thể gỡ yên xuống.",		-- 物品名:"鞍角" 制造描述:"把鞍具撬开。"
        SALTLICK = "Bao lâu mới tới bên trong?",		-- 物品名:"舔舔盐块" 制造描述:"好好喂养你家的牲畜。"
        BRUSH = "Con bò chắc chắn rất thích cái này.",		-- 物品名:"刷子" 制造描述:"减缓牦牛毛发的生长速度。"
		SANITYROCK =
		{
			ACTIVE = "Cục đá này trông như phát điên!",		-- 物品名:"方尖碑"->激活了
			INACTIVE = "Phần còn lại của nó đâu rồi?",		-- 物品名:"方尖碑"->没有激活
		},
		SAPLING =
		{
			BURNING = "Cháy nhanh thật!",		-- 物品名:"树苗"->正在燃烧
			WITHERED = "Nếu hạ nhiệt cho nó, có lẽ nó sẽ đỡ hơn.",		-- 物品名:"树苗"->枯萎了
			GENERIC = "Mầm cây thật đáng yêu!",		-- 物品名:"树苗"->默认
			PICKED = "Nó đã được dạy cho bài học rồi.",		-- 物品名:"树苗"->被采完了
			DISEASED = "Trông nó không khỏe lắm.",		-- 物品名:"树苗"->生病了
			DISEASING = "Ồ... có một số chỗ không ổn lắm.",		-- 物品名:"树苗"->正在生病？？
		},
   		SCARECROW = 
   		{
			GENERIC = "Con quạ không có chỗ để hót rồi.",		-- 物品名:"友善的稻草人"->默认 制造描述:"模仿最新的秋季时尚。"
			BURNING = "Bù nhìn đang cháy.",		-- 物品名:"友善的稻草人"->正在燃烧 制造描述:"模仿最新的秋季时尚。"
			BURNT = "Có người mưu sát bù nhìn rồi!",		-- 物品名:"友善的稻草人"->烧焦的 制造描述:"模仿最新的秋季时尚。"
   		},
   		SCULPTINGTABLE=
   		{
			EMPTY = "Chúng ta có thể dùng thứ này để làm tượng đá.",		-- 物品名:"陶轮" 制造描述:"大理石在你手里将像黏土一样！"
			BLOCK = "Chuẩn bị điêu khắc.",		-- 物品名:"陶轮"->锁住了 制造描述:"大理石在你手里将像黏土一样！"
			SCULPTURE = "Một kiệt tác!",		-- 物品名:"陶轮"->雕像做好了 制造描述:"大理石在你手里将像黏土一样！"
			BURNT = "Cháy rụi hoàn toàn.",		-- 物品名:"陶轮"->烧焦的 制造描述:"大理石在你手里将像黏土一样！"
   		},
        SCULPTURE_KNIGHTHEAD = "Phần còn lại đâu rồi?",		-- 物品名:"可疑的大理石" 制造描述:"大理石在你手里将像黏土一样！"
		SCULPTURE_KNIGHTBODY = 
		{
			COVERED = "Là một tượng đá cẩm thạch kỳ quái.",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Ta đoán nó đang vỡ dưới sức ép lớn.",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Cuối cùng cũng ghép xong rồi.",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Bên trong đang động đậy.",		-- 物品名:"大理石雕像"->准备好了
		},
        SCULPTURE_BISHOPHEAD = "Đó là một cái đầu phải không?",		-- 物品名:"可疑的大理石"
		SCULPTURE_BISHOPBODY = 
		{
			COVERED = "Trông khá cũ, nhưng cảm thấy rất mới.",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Có một mảnh ghép khá lớn vẫn chưa tìm thấy.",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Sau đo thì sao?",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Bên trong đang động đậy.",		-- 物品名:"大理石雕像"->准备好了
		},
        SCULPTURE_ROOKNOSE = "Cái này ở đâu vậy?",		-- 物品名:"可疑的大理石"
		SCULPTURE_ROOKBODY = 
		{
			COVERED = "Một bức tượng đá cẩm thạch.",		-- 物品名:"大理石雕像"->被盖住了-三基佬雕像可以敲大理石的时候
			UNCOVERED = "Nó không ở trong trạng thái tốt nhất.",		-- 物品名:"大理石雕像"->没有被盖住-三基佬雕像被开采后
			FINISHED = "Tất cả đều sửa xong rồi.",		-- 物品名:"大理石雕像"->三基佬雕像修好了
			READY = "Bên trong đang động đậy.",		-- 物品名:"大理石雕像"->准备好了
		},
        GARGOYLE_HOUND = "Ta không thích cái ánh mắt đó, nó đang nhìn ta.",		-- 物品名:"可疑的月亮石"
        GARGOYLE_WEREPIG = "Trông nó rất sống động.",		-- 物品名:"可疑的月亮石"
		SEEDS = "Mỗi hạt đều là một câu đố nhỏ.",		-- 物品名:"种子"
		SEEDS_COOKED = "Tinh hoa của sự sống đều bị nướng lên rồi!",		-- 物品名:"烤种子"
		SEWING_KIT = "Ta không giỏi thêu thùa.",		-- 物品名:"针线包" 制造描述:"缝补受损的衣物。"
		SEWING_TAPE = "Sửa chữa khá tốt.",		-- 物品名:"可靠的胶布" 制造描述:"缝补受损的衣物。"
		SHOVEL = "Dưới đất trôn cất rất nhiều bí mật.",		-- 物品名:"铲子" 制造描述:"挖掘各种各样的东西。"
		SILK = "Kết dính, một loại nguyên liệu đa dụng.",		-- 物品名:"蜘蛛丝"
		SKELETON = "Sớm ngày đầu thai.",		-- 物品名:"骷髅"
		SCORCHED_SKELETON = "Thật đáng sợ.",		-- 物品名:"易碎骨骼"
		SKULLCHEST = "Không chắc có nên mở hay không.",		-- 物品名:"骷髅箱"
		SMALLBIRD =
		{
			GENERIC = "Một con chim nhỏ xíu.",		-- 物品名:"小鸟"->默认
			HUNGRY = "Trông nó có vẻ đang đói.",		-- 物品名:"小鸟"->有点饿了
			STARVING = "Nó chắc chắn rất đói.",		-- 物品名:"小鸟"->挨饿
		},
		SMALLMEAT = "Hao Thiên, đến đây, cho ngươi.",		-- 物品名:"小肉"
		SMALLMEAT_DRIED = "Một miếng thịt khô.",		-- 物品名:"小风干肉"
		SPAT = "Con thú tính khí nóng nảy.",		-- 物品名:"钢羊"
		SPEAR = "Cái này không bằng Tam Tiêm Lưỡng Nhẫn Đao của ta.",		-- 物品名:"长矛" 制造描述:"使用尖的那一端。"
		SPEAR_WATHGRITHR = "Trông nó có vẻ đâm được đó.",		-- 物品名:"战斗长矛" 制造描述:"黄金使它更锋利。"
		WATHGRITHRHAT = "Cái mũ này thật đặc biệt.",		-- 物品名:"战斗头盔" 制造描述:"独角兽是你的保护神。"
		SPIDER =
		{
			DEAD = "Tởm.",		-- 物品名:"蜘蛛"->死了
			GENERIC = "Ta ghét nhện.",		-- 物品名:"蜘蛛"->默认
			SLEEPING = "Đợi nó tỉnh dậy rồi chém chết nó.",		-- 物品名:"蜘蛛"->睡着了
		},
		SPIDERDEN = "Dính ghê!",		-- 物品名:"蜘蛛巢"
		SPIDEREGGSACK = "Ta hy vọng mấy cái này đừng nở ra.",		-- 物品名:"蜘蛛卵" 制造描述:"跟你的朋友寻求点帮助。"
		SPIDERGLAND = "Nó có mùi thuốc bảo quản khá nồng.",		-- 物品名:"蜘蛛腺"
		SPIDERHAT = "Đừng hòng ta đội lên.",		-- 物品名:"蜘蛛帽" 制造描述:"蜘蛛们会喊你“妈妈”。"
		SPIDERQUEEN = "Nào! Ăn một đao của ta nào.",		-- 物品名:"蜘蛛女王"
		SPIDER_WARRIOR =
		{
			DEAD = "Cuối cùng cũng thoát rồi!",		-- 物品名:"蜘蛛战士"->死了
			GENERIC = "Có vẻ hung dữ hơn nhện bình thường.",		-- 物品名:"蜘蛛战士"->默认
			SLEEPING = "Ta nên giữ khoảng cách.",		-- 物品名:"蜘蛛战士"->睡着了
		},
		SPOILED_FOOD = "Đó là đồ ăn mốc đầy rong rêu.",		-- 物品名:"腐烂食物"
        STAGEHAND =
        {
			AWAKE = "Cổ quái?",		-- 物品名:"舞台"->醒了
			HIDING = "Ẩn chứa gì đó.",		-- 物品名:"舞台"->藏起来了
        },
        STATUE_MARBLE = 
        {
            GENERIC = "Tượng đá cẩm thạch cao cấp",		-- 物品名:"大理石雕像"->默认
            TYPE1 = "Đừng để mất lý trí!",		-- 物品名:"大理石雕像"->类型1
            TYPE2 = "Tượng đá.",		-- 物品名:"大理石雕像"->类型2
        },
		STATUEHARP = "Đầu của nó sao vậy?",		-- 物品名:"竖琴雕像"
		STATUEMAXWELL = "Nó hơi lùn chút.",		-- 物品名:"麦斯威尔雕像"
		STEELWOOL = "Sợi thép thô ráp.",		-- 物品名:"钢丝绵"
		STINGER = "Trông rất sắc nhọn.",		-- 物品名:"蜂刺"
		STRAWHAT = "Cái mũ luôn làm rối kiểu tóc của ta.",		-- 物品名:"草帽" 制造描述:"帮助你保持清凉干爽。"
		STUFFEDEGGPLANT = "Thật sự chất đầy rồi!",		-- 物品名:"酿茄子"
		SWEATERVEST = "Chiếc áo trong này rất bảnh.",		-- 物品名:"犬牙背心" 制造描述:"粗糙，但挺时尚。"
		REFLECTIVEVEST = "Tránh ra, mặt trời tà ác!",		-- 物品名:"清凉夏装" 制造描述:"穿上这件时尚的背心，保持凉爽和清醒。"
		HAWAIIANSHIRT = "Mặc nó để thử nghiệm thì không an toàn!",		-- 物品名:"花衬衫" 制造描述:"适合夏日穿着，或在周五便装日穿着。"
		TAFFY = "Nếu ta đi khám răng, mà chúng nó biết được thì sẽ điên lên mất.",		-- 物品名:"太妃糖"
		TALLBIRD = "Con chim rất cao!",		-- 物品名:"高脚鸟"
		TALLBIRDEGG = "Nó sẽ nở chứ?",		-- 物品名:"高脚鸟蛋"
		TALLBIRDEGG_COOKED = "Vừa ngon vừa dinh dưỡng.",		-- 物品名:"煎高脚鸟蛋"
		TALLBIRDEGG_CRACKED =
		{
			COLD = "Nó đang run hay là ta đang run vậy?",		-- 物品名:"冻伤"
			GENERIC = "Nó đang nở!",		-- 物品名:"孵化中的高脚鸟蛋"->默认
			HOT = "Trứng mà cũng đổ mồ hôi sao?",		-- 物品名:"中暑"
			LONG = "Ta nghĩ nó cần một khoảng thời gian...",		-- 物品名:"孵化中的高脚鸟蛋"->还需要很久
			SHORT = "Nó có thể nở ra bất kỳ lúc nào.",		-- 物品名:"孵化中的高脚鸟蛋"->很快了
		},
		TALLBIRDNEST =
		{
			GENERIC = "Nó đúng là trái trứng khác thường!",		-- 物品名:"高脚鸟巢"->默认
			PICKED = "Cái ổ trống trơn.",		-- 物品名:"高脚鸟巢"->被采完了
		},
		TEENBIRD =
		{
			GENERIC = "Con chim không cao lắm.",		-- 物品名:"小高脚鸟"->默认
			HUNGRY = "Ngươi đói đến không chịu nổi rồi?",		-- 物品名:"小高脚鸟"->有点饿了
			STARVING = "Ánh mắt nó rất giận dữ.",		-- 物品名:"小高脚鸟"->挨饿
		},
		TELEPORTATO_BASE =
		{
			ACTIVE = "Có cái này thì ta có thể xuyên thời không!",		-- 物品名:"木制传送台"->激活了
			GENERIC = "Hình như cái này có thể thông tới thế giới khác!",		-- 物品名:"木制传送台"->默认
			LOCKED = "Còn thiếu một số thứ gì đó.",		-- 物品名:"木制传送台"->锁住了
			PARTIAL = "Cái phát minh này sắp hoàn thành rồi!",		-- 物品名:"木制传送台"->已经有部分了
		},
		TELEPORTATO_BOX = "Nó có thể không chế cả vũ trụ.",		-- 物品名:"盒状零件"
		TELEPORTATO_CRANK = "Rắn chắc đủ để đối phó với nhưng thử thách nguy hiểm nhất.",		-- 物品名:"曲柄零件"
		TELEPORTATO_POTATO = "Củ khoai tây vàng này chứa sức mạnh lớn đến đáng sợ...",		-- 物品名:"金属土豆状零件"
		TELEPORTATO_RING = "Một cái vòng có thể hội tụ năng lượng không gian.",		-- 物品名:"环状零件"
		TELESTAFF = "Nó sẽ giúp ta thấy cả thế giới này.",		-- 物品名:"传送魔杖" 制造描述:"穿越空间的法杖！穿越时间的装置需另外购买。"
		TENT = 
		{
			GENERIC = "Ta mà không ngủ thì sẽ điên mất.",		-- 物品名:"帐篷"->默认 制造描述:"回复精神值，但要花费时间并导致饥饿。"
			BURNT = "Không còn cái gì để ngủ rồi.",		-- 物品名:"帐篷"->烧焦的 制造描述:"回复精神值，但要花费时间并导致饥饿。"
		},
		SIESTAHUT = 
		{
			GENERIC = "Nơi nghỉ ngơi tránh nắng an toàn.",		-- 物品名:"遮阳篷"->默认 制造描述:"躲避烈日，回复精神值。"
			BURNT = "Bây giờ nó không che được bao nhiêu nắng rồi.",		-- 物品名:"遮阳篷"->烧焦的 制造描述:"躲避烈日，回复精神值。"
		},
		TENTACLE = "Cái đó trông khá nguy hiểm.",		-- 物品名:"触手" 制造描述:"躲避烈日，回复精神值。"
		TENTACLESPIKE = "Nó vừa nhọn vừa dính.",		-- 物品名:"狼牙棒" 制造描述:"躲避烈日，回复精神值。"
		TENTACLESPOTS = "Ta nghĩ đây là cơ quan sinh dục của nó.",		-- 物品名:"触手皮" 制造描述:"躲避烈日，回复精神值。"
		TENTACLE_PILLAR = "Xúc tua trơn trượt.",		-- 物品名:"大触手" 制造描述:"躲避烈日，回复精神值。"
        TENTACLE_PILLAR_HOLE = "Rất hôi, nhưng đáng để thử.",		-- 物品名:"硕大的泥坑" 制造描述:"躲避烈日，回复精神值。"
		TENTACLE_PILLAR_ARM = "Xúc tua nhỏ trơn trượt.",		-- 物品名:"小触手" 制造描述:"躲避烈日，回复精神值。"
		TENTACLE_GARDEN = "Lại là một cái xúc tua trơn trượt.",		-- 物品名:"大触手" 制造描述:"躲避烈日，回复精神值。"
		TOPHAT = "Cái mũ rất tốt.",		-- 物品名:"高礼帽" 制造描述:"最经典的帽子款式。"
		TORCH = "Món đồ xua tan bóng tối.",		-- 物品名:"火炬" 制造描述:"可携带的光源。"
		TRANSISTOR = "Dòng điện khiến nó kêu roẹt roẹt.",		-- 物品名:"电子元件" 制造描述:"科学至上！滋滋滋！"
		TRAP = "Ta xếp nó rất sát nhau.",		-- 物品名:"陷阱" 制造描述:"捕捉小型生物。"
		TRAP_TEETH = "Nó sẽ gây sát thương lên những ai đạp vào.",		-- 物品名:"犬牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西。"
		TRAP_TEETH_MAXWELL = "Ta không muốn đạp lên nó!",		-- 物品名:"麦斯威尔的尖牙陷阱" 制造描述:"弹出来并咬伤任何踩到它的东西。"
		TREASURECHEST = 
		{
			GENERIC = "Đây là rương đồ!",		-- 物品名:"木箱"->默认 制造描述:"一种结实的容器。"
			BURNT = "Cái rương đó bị cháy thành tro rồi.",		-- 物品名:"木箱"->烧焦的 制造描述:"一种结实的容器。"
		},
		TREASURECHEST_TRAP = "Thật là tiện!",		-- 物品名:"宝箱" 制造描述:"一种结实的容器。"
		SACRED_CHEST = 
		{
			GENERIC = "Ta nghe thấy nó thì thầm muốn thứ gì đó.",		-- 物品名:"古老的箱子"->默认
			LOCKED = "Nó bị khóa rồi.",		-- 物品名:"古老的箱子"->锁住了
		},
		TREECLUMP = "Có phải có người muốn nhốt ta ở đây.",		-- 物品名:"古老的箱子"
		TRINKET_1 = "Có lẽ Willow cùng bọn họ sống rất vui vẻ?", --Melted Marbles		-- 物品名:"熔化的弹珠"
		TRINKET_2 = "Ngươi và Kazoo có mối quan hệ gì?", --Fake Kazoo		-- 物品名:"假卡祖笛"
		TRINKET_3 = "Đó là nút thắt chết tiệt. Cái thắt không bao giờ mở được.", --Gord's Knot		-- 物品名:"戈尔迪之结"
		TRINKET_4 = "Nó nhất định là món đồ tôn giáo nào đó", --Gnome		-- 物品名:"地精玩偶"
		TRINKET_5 = "Không may là nó quá nhỏ, ta không thể ngồi lên đó.", --Toy Rocketship		-- 物品名:"迷你火箭"
		TRINKET_6 = "Những ngày tháng tải điện của nó đã kết thúc rồi.", --Frazzled Wires		-- 物品名:"烂电线"
		TRINKET_7 = "Không có thời gian vui chơi!", --Ball and Cup		-- 物品名:"杯子和球玩具"
		TRINKET_8 = "Tốt quá rồi. Có cái nút để chặn rồi.", --Rubber Bung		-- 物品名:"硬化橡胶塞"
		TRINKET_9 = "Ta vốn rất thích khóa kéo.", --Mismatched Buttons		-- 物品名:"不搭的纽扣"
		TRINKET_10 = "Nó sắp trở thành đồ mà Wes thích rồi.", --Dentures		-- 物品名:"二手假牙"
		TRINKET_11 = "Hutch nói với ta những lời nói dối tươi đẹp.", --Lying Robot		-- 物品名:"机器人玩偶"
		TRINKET_12 = "Đây là muốn xin thử nghiệm à.", --Dessicated Tentacle		-- 物品名:"干瘪的触手"
		TRINKET_13 = "Nhất định là món đồ tôn giáo nào đó.", --Gnomette		-- 物品名:"可爱的小玩偶"
		TRINKET_14 = "Bây giờ mà được uống ít trà, thì hay biết mấy...", --Leaky Teacup		-- 物品名:"漏水的茶杯"
		TRINKET_15 = "...Maxwell lại làm rơi trang phục của hắn rồi.", --Pawn		-- 物品名:"白衣主教"
		TRINKET_16 = "...Maxwell lại làm rơi trang phục của hắn rồi.", --Pawn		-- 物品名:"黑衣主教"
		TRINKET_17 = "Nĩa cong nhìn đáng sợ.", --Bent Spork		-- 物品名:"弯曲的叉子"
		TRINKET_18 = "Ta muốn biết nó ẩn chứa điều gì?", --Trojan Horse		-- 物品名:"玩具木马"
		TRINKET_19 = "Đây là con quay.", --Unbalanced Top		-- 物品名:"失衡上衣"
		TRINKET_20 = "Wigfrid không ngừng xông đến, dùng thứ đó đánh ta?!", --Backscratcher		-- 物品名:"不求人"
		TRINKET_21 = "Cái máy đánh trứng này bị cong queo rồi.", --Egg Beater		-- 物品名:"破搅拌器"
		TRINKET_22 = "Ta có vài câu về chuỗi ký tự này.", --Frayed Yarn		-- 物品名:"磨损的纱线"
		TRINKET_23 = "Ta có thể tự mang giày, cảm ơn.", --Shoehorn		-- 物品名:"鞋拔子"
		TRINKET_24 = "Ta nghĩ chắc Wickerbottom có con mèo.", --Lucky Cat Jar		-- 物品名:"幸运猫扎尔"
		TRINKET_25 = "Ngửi mùi thấy không tươi lắm.", --Air Unfreshener		-- 物品名:"臭气制造器"
		TRINKET_26 = "Đồ ăn và ly! Cuối cùng cũng ở chung.", --Potato Cup		-- 物品名:"土豆杯"
		TRINKET_27 = "Nếu ngươi mở nó ra, ngươi có thể đâm người khác từ xa.", --Coat Hanger		-- 物品名:"钢丝衣架"
		TRINKET_28 = "Đây đúng là âm mưu.", --Rook		-- 物品名:"白色战车"
        TRINKET_29 = "Đây đúng là âm mưu.", --Rook		-- 物品名:"黑色战车"
        TRINKET_30 = "Sao hắn cứ ném lung tung vậy.", --Knight		-- 物品名:"白色骑士"
        TRINKET_31 = "Sao hắn cứ ném lung tung vậy.", --Knight		-- 物品名:"黑色骑士"
        TRINKET_32 = "Ta biết người thích cái này!", --Cubic Zirconia Ball		-- 物品名:"立方氧化锆球"
        TRINKET_33 = "Hy vọng không thu hút nhện tới.", --Spider Ring		-- 物品名:"蜘蛛指环"
        TRINKET_34 = "Để bọn ta hứa nào.", --Monkey Paw		-- 物品名:"猴子手掌"
        TRINKET_35 = "Rất khó để tìm cái bình tốt ở gần đây.", --Empty Elixir		-- 物品名:"空的长生不老药"
        TRINKET_36 = "Ăn hết tất cả kẹo thì có lẽ ta sẽ cần đến mấy thứ này.", --Faux fangs		-- 物品名:"人造尖牙"
        TRINKET_37 = "Ta không tin vào hiện tượng siêu nhiên.", --Broken Stake		-- 物品名:"断桩"
        TRINKET_38 = "Ta nghĩ nó đến từ thế giới khác, một thế giới đầy sự lừa gạt.", -- Binoculars Griftlands trinket		-- 物品名:"双筒望远镜"
        TRINKET_39 = "Ta muốn biết cái còn lại ở đâu?", -- Lone Glove Griftlands trinket		-- 物品名:"单只手套"
        TRINKET_40 = "Ta cầm nó giống như đang đi chợ.", -- Snail Scale Griftlands trinket		-- 物品名:"蜗牛秤"
        TRINKET_41 = "Rờ vào thì thấy nóng.", -- Goop Canister Hot Lava trinket		-- 物品名:"黏液罐"
        TRINKET_42 = "Nó chứa ký ức tuổi trẻ của người nào đó.", -- Toy Cobra Hot Lava trinket		-- 物品名:"玩具眼镜蛇"
        TRINKET_43= "Nó không giỏi nhảy.", -- Crocodile Toy Hot Lava trinket		-- 物品名:"鳄鱼玩具"
        TRINKET_44 = "Nó là tiêu bản của loại thực vật nào đó.", -- Broken Terrarium ONI trinket		-- 物品名:"破碎的玻璃罐"
        TRINKET_45 = "Nó bắt được tần số ở thế giới khác.", -- Odd Radio ONI trinket		-- 物品名:"奇怪的收音机"
        TRINKET_46 = "Có lẽ là công cụ khảo sát động lực học không khí?", -- Hairdryer ONI trinket		-- 物品名:"损坏的吹风机"
        HALLOWEENCANDY_1 = "Cho dù bị sâu răng thì cũng đáng, đúng không?",		-- 物品名:"糖果苹果"
        HALLOWEENCANDY_2 = "Phải nhuyễn cỡ nào mới làm ra những thứ này!",		-- 物品名:"糖果玉米"
        HALLOWEENCANDY_3 = "Là… hạt bắp.",		-- 物品名:"不太甜的玉米"
        HALLOWEENCANDY_4 = "Trên đường đi nó cứ ngọ nguậy.",		-- 物品名:"粘液蜘蛛"
        HALLOWEENCANDY_5 = "Hôm sau răng của ta chắc sẽ biểu tình cho coi.",		-- 物品名:"浣猫糖果"
        HALLOWEENCANDY_6 = "Ta… không cho rằng ta sẽ ăn mấy cái này.",		-- 物品名:"“葡萄干”"
        HALLOWEENCANDY_7 = "Bất kỳ ai gặp phải mấy thứ này đều sẽ kích động.",		-- 物品名:"葡萄干"
        HALLOWEENCANDY_8 = "Chỉ có kẻ ngốc mới không thích cái thứ này.",		-- 物品名:"鬼魂波普"
        HALLOWEENCANDY_9 = "Dính răng.",		-- 物品名:"果冻虫"
        HALLOWEENCANDY_10 = "Chỉ có kẻ ngốc mới không thích cái thứ này.",		-- 物品名:"触须棒棒糖"
        HALLOWEENCANDY_11 = "Mùi vị còn ngon hơn hàng thật.",		-- 物品名:"巧克力猪"
        HALLOWEENCANDY_12 = "Cục kẹo này vừa nhúc nhích phải không?", --ONI meal lice candy		-- 物品名:"糖果虱"
        HALLOWEENCANDY_13 = "Ôi chao, cái hàm tội nghiệp của ta.", --Griftlands themed candy		-- 物品名:"无敌硬糖"
        HALLOWEENCANDY_14 = "Ta không ăn được cay.", --Hot Lava pepper candy		-- 物品名:"熔岩椒"
        CANDYBAG = "Nó là loại món ngọt thơm ngon nào đó.",		-- 物品名:"糖果袋" 制造描述:"只带万圣夜好吃的东西。"
		HALLOWEEN_ORNAMENT_1 = "Một món trang sức có thể treo trên cây.",		-- 物品名:"幽灵装饰"
		HALLOWEEN_ORNAMENT_2 = "Trang sức kỳ quái.",		-- 物品名:"蝙蝠装饰"
		HALLOWEEN_ORNAMENT_3 = "Khúc gỗ này rất hợp để treo lên.", 		-- 物品名:"蜘蛛装饰"
		HALLOWEEN_ORNAMENT_4 = "Chụt chụt như thật.",		-- 物品名:"触手装饰"
		HALLOWEEN_ORNAMENT_5 = "Trang sức tám tay.",		-- 物品名:"悬垂蜘蛛装饰"
		HALLOWEEN_ORNAMENT_6 = "Dạo gần đây mọi người đều bàn tán về việc trang trí cây.", 		-- 物品名:"乌鸦装饰"
		HALLOWEENPOTION_DRINKS_WEAK = "Cứ tưởng nó sẽ lớn lắm chứ.",		-- 物品名:"古老的箱子"
		HALLOWEENPOTION_DRINKS_POTENT = "Thuốc mạnh.",		-- 物品名:"古老的箱子"
        HALLOWEENPOTION_BRAVERY = "Tràn đầy dũng khí.",		-- 物品名:"古老的箱子"
		HALLOWEENPOTION_FIRE_FX = "Kết tinh của lửa.", 		-- 物品名:"古老的箱子"
		MADSCIENCE_LAB = "Đồ vật điên loạn!",		-- 物品名:"疯狂科学家实验室" 制造描述:"疯狂实验无极限。唯独神智有极限。"
		LIVINGTREE_ROOT = "Bên trong có gì đó! Ta phải diệt cỏ tận gốc.", 		-- 物品名:"完全正常的树根"
		LIVINGTREE_SAPLING = "Nó sẽ to đến đáng sợ.",		-- 物品名:"完全正常的树苗"
        DRAGONHEADHAT = "Ai muốn lên phía trước?",		-- 物品名:"幸运兽脑袋" 制造描述:"野兽装束的前端。"
        DRAGONBODYHAT = "Phần ở giữa khiến ta có hơi do dự.",		-- 物品名:"幸运兽躯体" 制造描述:"野兽装束的中间部分。"
        DRAGONTAILHAT = "Cái mũ tây phương rồng thần vẫy đuôi.",		-- 物品名:"幸运兽尾巴" 制造描述:"野兽装束的尾端。"
        PERDSHRINE =
        {
            GENERIC = "Hình như nó muốn thứ gì đó.",		-- 物品名:"火鸡神龛"->默认 制造描述:"供奉庄严的火鸡。"
            EMPTY = "Ta muốn trồn một số thứ ở đây.",		-- 物品名:"火鸡神龛" 制造描述:"供奉庄严的火鸡。"
            BURNT = "Biến mất hoàn toàn rồi.",		-- 物品名:"火鸡神龛"->烧焦的 制造描述:"供奉庄严的火鸡。"
        },
        REDLANTERN = "Cái lồng đèn này đặc biệt hơn những cái lồng đèn khác.",		-- 物品名:"红灯笼" 制造描述:"照亮你的路的幸运灯笼。"
        LUCKY_GOLDNUGGET = "Sự phát hiện đầy may mắn!",		-- 物品名:"幸运黄金" 制造描述:"供奉庄严的火鸡。"
        FIRECRACKERS = "Tiếng pháo nổ đón năm mới.",		-- 物品名:"红色爆竹" 制造描述:"用重击来庆祝！"
        PERDFAN = "Rất lớn.",		-- 物品名:"幸运扇" 制造描述:"额外的运气，超级多。"
        REDPOUCH = "Bên trong có gì không?",		-- 物品名:"红袋子" 制造描述:"供奉庄严的火鸡。"
        WARGSHRINE = 
        {
            GENERIC = "Ta phải làm thứ gì đó thật vui.",		-- 物品名:"座狼龛"->默认 制造描述:"供奉黏土座狼。"
            EMPTY = "Ta phải thêm đuốc vào trong.",		-- 物品名:"座狼龛" 制造描述:"供奉黏土座狼。"
            BURNT = "Nó cháy rụi rồi.",		-- 物品名:"座狼龛"->烧焦的 制造描述:"供奉黏土座狼。"
        },
        CLAYWARG = 
        {
        	GENERIC = "Quái vật đất sét!",		-- 物品名:"黏土座狼"->默认
        	STATUE = "Có phải nó vừa động đậy không?",		-- 物品名:"黏土座狼"->雕像
        },
        CLAYHOUND = 
        {
        	GENERIC = "Nó đã thoát khỏi sự ràng buộc!",		-- 物品名:"黏土猎犬"->默认
        	STATUE = "Nó trông như thật.",		-- 物品名:"黏土猎犬"->雕像
        },
        HOUNDWHISTLE = "Cái này có thể ngăn cản bước chân của chó.",		-- 物品名:"幸运哨子" 制造描述:"对野猎犬吹哨。"
        CHESSPIECE_CLAYHOUND = "Đằng nào cũng khóa rồi, ta không sợ đâu.",		-- 物品名:"猎犬棋子"
        CHESSPIECE_CLAYWARG = "Vậy mà ta không bị ăn thịt!",		-- 物品名:"座狼棋子"
		PIGSHRINE =
		{
            GENERIC = "Có nhiều thứ phải làm.",		-- 物品名:"猪神龛"->默认 制造描述:"为富有的猪献贡。"
            EMPTY = "Nó muốn có thịt.",		-- 物品名:"猪神龛" 制造描述:"为富有的猪献贡。"
            BURNT = "Cháy rụi rồi.",		-- 物品名:"猪神龛"->烧焦的 制造描述:"为富有的猪献贡。"
		},
		PIG_TOKEN = "Cái này trông có vẻ rất quan trọng.",		-- 物品名:"金色腰带" 制造描述:"为富有的猪献贡。"
		YOTP_FOOD1 = "Một bữa tiệc linh đình cho ta.",		-- 物品名:"致敬烤肉" 制造描述:"向猪王敬肉。"
		YOTP_FOOD2 = "Đồ ăn chỉ có dã thú mới thích.",		-- 物品名:"八宝泥馅饼" 制造描述:"那里隐藏着什么？"
		YOTP_FOOD3 = "Không tinh tế chút nào.",		-- 物品名:"鱼头串" 制造描述:"棍子上的荣华富贵。"
		PIGELITE1 = "Nhìn cái gì vậy?", --BLUE		-- 物品名:"韦德" 制造描述:"为富有的猪献贡。"
		PIGELITE2 = "Nó từng tham gia đào vàng!", --RED		-- 物品名:"伊格内修斯" 制造描述:"为富有的猪献贡。"
		PIGELITE3 = "Trong mắt ngươi có bùn!", --WHITE		-- 物品名:"德米特里" 制造描述:"为富有的猪献贡。"
		PIGELITE4 = "Chẳng lẽ ngươi không muốn đánh người khác sao?", --GREEN		-- 物品名:"索耶" 制造描述:"为富有的猪献贡。"
		BISHOP_CHARGE_HIT = "Ờ!",		-- 物品名:"猪神龛" 制造描述:"为富有的猪献贡。"
		TRUNKVEST_SUMMER = "Trang phục bình thường hoang dã.",		-- 物品名:"保暖小背心" 制造描述:"暖和，但算不上非常暖和。"
		TRUNKVEST_WINTER = "Trang phục sinh tồn mùa đông cần phải có.",		-- 物品名:"寒冬背心" 制造描述:"足以抵御冬季暴雪的保暖背心。"
		TRUNK_COOKED = "Không biết sao nó giống cái vòi hơn trước khi nấu.不知怎么回事比以前更像鼻子了。",		-- 物品名:"象鼻排" 制造描述:"为富有的猪献贡。"
		TRUNK_SUMMER = "Một cái vòi thông gió.",		-- 物品名:"象鼻" 制造描述:"为富有的猪献贡。"
		TRUNK_WINTER = "Một cái vòi nhiều lông.",		-- 物品名:"冬象鼻" 制造描述:"为富有的猪献贡。"
		TUMBLEWEED = "Lễ vật của sự may mắn.",		-- 物品名:"风滚草" 制造描述:"为富有的猪献贡。"
		TURKEYDINNER = "Trời~",		-- 物品名:"火鸡大餐" 制造描述:"为富有的猪献贡。"
		TWIGS = "Một cành cây.",		-- 物品名:"树枝" 制造描述:"为富有的猪献贡。"
		UMBRELLA = "Mở dù cho ai.",		-- 物品名:"雨伞" 制造描述:"遮阳挡雨！"
		GRASS_UMBRELLA = "Mở dù cho ai.",		-- 物品名:"花伞" 制造描述:"漂亮轻便的保护伞。"
		UNIMPLEMENTED = "Có vẻ vẫn chưa hoàn thành! Cẩn thận nguy hiểm.",		-- 物品名:"猪神龛" 制造描述:"为富有的猪献贡。"
		WAFFLES = "Ta không biết có nên thêm siro hay không.",		-- 物品名:"华夫饼" 制造描述:"为富有的猪献贡。"
		WALL_HAY = 
		{	
			GENERIC = "Ta nghĩ chỉ có thể dùng tạm thôi.",		-- 物品名:"草墙"->默认
			BURNT = "Hoàn toàn vô dụng.",		-- 物品名:"草墙"->烧焦的
		},
		WALL_HAY_ITEM = "Cái này không phải là ý hay.",		-- 物品名:"草墙" 制造描述:"草墙墙体。不太结实。"
		WALL_STONE = "Tường tốt.",		-- 物品名:"石墙"
		WALL_STONE_ITEM = "Nó khiến ta có cảm giác an toàn.",		-- 物品名:"石墙" 制造描述:"石墙墙体。"
		WALL_RUINS = "Một bức tường cổ xưa.",		-- 物品名:"铥墙"
		WALL_RUINS_ITEM = "Một bức tường kiên cố.",		-- 物品名:"铥墙" 制造描述:"这些墙可以承受相当多的打击。"
		WALL_WOOD = 
		{
			GENERIC = "Nhọn đó!",		-- 物品名:"木墙"->默认
			BURNT = "Cháy rụi rồi!",		-- 物品名:"木墙"->烧焦的
		},
		WALL_WOOD_ITEM = "Cọc gỗ!",		-- 物品名:"木墙" 制造描述:"木墙墙体。"
		WALL_MOONROCK = "Kỳ ảo và nhẵn bóng!",		-- 物品名:"月岩壁"
		WALL_MOONROCK_ITEM = "Rất đẹp, nhưng cứng đến không ngờ.",		-- 物品名:"月岩壁" 制造描述:"月球疯子之墙。"
		FENCE = "Chỉ là cái hàng rào gỗ thôi.",		-- 物品名:"木栅栏"
        FENCE_ITEM = "Có nó thì có thể tạo ra hàng rào kiên cố và đẹp.",		-- 物品名:"木栅栏" 制造描述:"木栅栏部分。"
        FENCE_GATE = "Có lúc mở, có lúc đóng.",		-- 物品名:"木门"
        FENCE_GATE_ITEM = "Có nó thì có thể tạo ra cánh cửa kiên cố và đẹp.",		-- 物品名:"木门" 制造描述:"木栅栏的门。"
		WALRUS = "Hải mã là kẻ săn mồi bẩm sinh.",		-- 物品名:"海象人"
		WALRUSHAT = "Ấm áp và minh mẫn.",		-- 物品名:"海象的贝雷帽"
		WALRUS_CAMP =
		{
			EMPTY = "Xem ra có người đóng trại ở đây.",		-- 物品名:"海象营"
			GENERIC = "Xem ra bên trong ấm áp và thoải mái.",		-- 物品名:"海象营"->默认
		},
		WALRUS_TUSK = "Chắc chắn nó có tác dụng.",		-- 物品名:"海象牙"
		WARDROBE = 
		{
			GENERIC = "Nó chứa bí mật của những điều cấm kỵ và bóng tối...",		-- 物品名:"衣柜"->默认 制造描述:"随心变换面容。"
            BURNING = "Cháy nhanh thật!",		-- 物品名:"衣柜"->正在燃烧 制造描述:"随心变换面容。"
			BURNT = "Kiểu dáng nó lỗi thời rồi.",		-- 物品名:"衣柜"->烧焦的 制造描述:"随心变换面容。"
		},
		WARG = "Ngươi không dễ đối phó chút nào, con chó.",		-- 物品名:"座狼" 制造描述:"随心变换面容。"
		WASPHIVE = "Ta thấy đám ong đó điên hết rồi.",		-- 物品名:"杀人蜂蜂窝" 制造描述:"随心变换面容。"
		WATERBALLOON = "Đồ chơi trẻ em!",		-- 物品名:"水球" 制造描述:"球体灭火。"
		WATERMELON = "Vừa sệt vừa ngọt.",		-- 物品名:"西瓜" 制造描述:"随心变换面容。"
		WATERMELON_COOKED = "Nhiều nước và ấm.",		-- 物品名:"烤西瓜" 制造描述:"随心变换面容。"
		WATERMELONHAT = "Nước sẽ chảy từ mặt bạn xuống.",		-- 物品名:"西瓜帽" 制造描述:"提神醒脑，但感觉黏黏的。"
		WAXWELLJOURNAL = "Thật đáng sợ.",		-- 物品名:"暗影魔法书" 制造描述:"这将让你大吃一惊。"
		WETGOOP = "Không phải ta làm.",		-- 物品名:"失败料理" 制造描述:"随心变换面容。"
        WHIP = "Không có gì có thể đem lại sự yên tĩnh hơn tiếng vang lớn.",		-- 物品名:"皮鞭" 制造描述:"提出有建设性的反馈意见。"
		WINTERHAT = "Khi mùa đông đến, thì nó sẽ phát huy tác dụng.",		-- 物品名:"冬帽" 制造描述:"保持脑袋温暖。"
		WINTEROMETER = 
		{
			GENERIC = "Thủy ngân đó.",		-- 物品名:"温度测量仪"->默认 制造描述:"测量环境气温。"
			BURNT = "Không thể do đạc được nữa.",		-- 物品名:"温度测量仪"->烧焦的 制造描述:"测量环境气温。"
		},
        WINTER_TREE =
        {
            BURNT = "Ảnh hưởng đến không khí ngày lễ.",		-- 物品名:"冬季圣诞树"->烧焦的
            BURNING = "Ta cho rằng đó là sai lầm.",		-- 物品名:"冬季圣诞树"->正在燃烧
            CANDECORATE = "Lễ hội mùa đông vui vẻ!",		-- 物品名:"冬季圣诞树"->烛台？？？
            YOUNG = "Sắp đên lễ hội mùa đông rồi!",		-- 物品名:"冬季圣诞树"->还年轻
        },
		WINTER_TREESTAND = 
		{
			GENERIC = "Ta cần một quả thông.",		-- 物品名:"圣诞树墩"->默认 制造描述:"种植并装饰一棵冬季圣诞树！"
            BURNT = "Ảnh hưởng đến không khí ngày lễ.",		-- 物品名:"圣诞树墩"->烧焦的 制造描述:"种植并装饰一棵冬季圣诞树！"
		},
        WINTER_ORNAMENT = "Mỗi một người đều sẽ tham gia một trò vui.",		-- 物品名:"圣诞小玩意" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_ORNAMENTLIGHT = "Một cái cây không có điện thì không được xem là hoàn hảo.",		-- 物品名:"圣诞灯" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_ORNAMENTBOSS = "Cái này khiến người ta rất ấn tượng.",		-- 物品名:"华丽的装饰" 制造描述:"种植并装饰一棵冬季圣诞树！"
		WINTER_ORNAMENTFORGE = "Ta nên bỏ lửa vào nó.",		-- 物品名:"熔炉装饰" 制造描述:"种植并装饰一棵冬季圣诞树！"
		WINTER_ORNAMENTGORGE = "Không biết tại sao, nó khiến ta cảm thấy đói.",		-- 物品名:"舒缓的装饰" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD1 = "Kết cấu sai, nhưng ta sẽ không để ý đến nó.", --gingerbread cookie		-- 物品名:"小姜饼" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD2 = "Bánh vừa miệng.", --sugar cookie		-- 物品名:"糖曲奇饼" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD3 = "Cơn đau răng trong ngày lễ giáng sinh sắp bắt đầu rồi", --candy cane		-- 物品名:"拐杖糖" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD4 = "Lần trải nghiệm này có hơi thất đức.", --fruitcake		-- 物品名:"永远的水果蛋糕" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD5 = "Có thể ăn món khác dâu mọng thì thật là tốt.", --yule log cake		-- 物品名:"巧克力树洞蛋糕" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD6 = "Bỏ trực tiếp vào miệng!", --plum pudding		-- 物品名:"李子布丁" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD7 = "Quả táo không ruột đầy rượu ngon.", --apple cider		-- 物品名:"苹果酒" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD8 = "Sao nó giữ nhiệt được? Một ly cacao nóng?", --hot cocoa		-- 物品名:"热可可" 制造描述:"种植并装饰一棵冬季圣诞树！"
        WINTER_FOOD9 = "Tại sao mùi của nó tuyệt như vậy?", --eggnog		-- 物品名:"美味的蛋酒" 制造描述:"种植并装饰一棵冬季圣诞树！"
        KLAUS = "Nó rốt cuộc là cái thứ gì vậy!",		-- 物品名:"克劳斯" 制造描述:"种植并装饰一棵冬季圣诞树！"
        KLAUS_SACK = "Chúng ta phải mở cái thứ này ra.",		-- 物品名:"赃物袋" 制造描述:"种植并装饰一棵冬季圣诞树！"
		KLAUSSACKKEY = "Cái sừng hươu này thật là đẹp.",		-- 物品名:"麋鹿茸" 制造描述:"种植并装饰一棵冬季圣诞树！"
		WORMHOLE =
		{
			GENERIC = "Muốn đi đường tắt, thì phải đi đường nguy hiểm.",		-- 物品名:"虫洞"->默认
			OPEN = "Muốn đi đường tắt, thì phải đi đường nguy hiểm.",		-- 物品名:"虫洞"->打开
		},
		WORMHOLE_LIMITED = "A, cái thứ này trông không đúng lắm.",		-- 物品名:"生病的虫洞"
		ACCOMPLISHMENT_SHRINE = "Ta muốn dùng nó, ta muốn cho cả thế giới này biết những gì ta làm.",        		-- 物品名:"奖杯" 制造描述:"证明你作为一个人的价值。"
		LIVINGTREE = "Nó đang nhìn ta phải không?",		-- 物品名:"完全正常的树"
		ICESTAFF = "Rờ vào thì thấy lạnh.",		-- 物品名:"冰魔杖" 制造描述:"把敌人冰冻在原地。"
		REVIVER = "Cắt thịt moi tim, âm dương xoay chuyển!",		-- 物品名:"告密的心" 制造描述:"鬼魂朋友复活了，好可怕。"
		SHADOWHEART = "Là nó..., nguồn gốc ma thuật của bộ xương",		-- 物品名:"暗影心房"
        ATRIUM_RUBBLE = 
        {
			LINE_1 = "Nó mô tả nền văn minh cổ xưa. Xem ra mọi người vừa đói vừa sợ.",		-- 物品名:"古代的壁画"->第一行
			LINE_2 = "Tấm ván này mòn quá rồi, không thể đọc được.",		-- 物品名:"古代的壁画"->第二行
			LINE_3 = "Bóng tối dần dần tiếp cận thành phố và người dân trong thành phố.",		-- 物品名:"古代的壁画"->第三行
			LINE_4 = "Da của họ đang tróc ra. Có vẻ bọn họ ở dưới lòng đất lại không như vậy.",		-- 物品名:"古代的壁画"->第四行
			LINE_5 = "Có thể thấy đây là thành phố lớn có các thành tựu khoa học kỹ thuật.",		-- 物品名:"古代的壁画"->第五行
		},
        ATRIUM_STATUE = "Xem ra nó không hoàn toàn là thật.",		-- 物品名:"远古雕像"
        ATRIUM_LIGHT = 
        {
			ON = "Ánh sáng khiến người ta bất an.",		-- 物品名:"古代的灯塔"->开启
			OFF = "Nó cần nguồn kích hoạt.",		-- 物品名:"古代的灯塔"->关闭
		},
        ATRIUM_GATE =
        {
			ON = "Quay lại trạng thái vận chuyển bình thường.",		-- 物品名:"古代的通道"->开启
			OFF = "Bộ phận quan trọng lại không sao.",		-- 物品名:"古代的通道"->关闭
			CHARGING = "Nó đang nhận năng lượng.",		-- 物品名:"古代的通道"->充能中
			DESTABILIZING = "Thông đạo đang dao động.",		-- 物品名:"古代的通道"->不稳定
			COOLDOWN = "Nó cần thời gian để hồi phục. Ta cũng vậy.",		-- 物品名:"古代的通道"->冷却中
        },
        ATRIUM_KEY = "Đây là năng lượng bên trong nó phát ra.",		-- 物品名:"古代的钥匙"
		LIFEINJECTOR = "Phục hồi phần cơ thể hư tổn!",		-- 物品名:"强心针" 制造描述:"提高下你那日渐衰退的最大健康值吧。"
		SKELETON_PLAYER =
		{
			MALE = "%s nhất định là chết lúc tiến hành thử nghiệm cùng %s.",		-- 物品名:"骷髅"->男性
			FEMALE = "%s nhất định là chết lúc tiến hành thử nghiệm cùng %s.",		-- 物品名:"骷髅"->女性
			ROBOT = "%s nhất định là chết lúc tiến hành thử nghiệm cùng %s.",		-- 物品名:"骷髅"->机器人
			DEFAULT = "%s nhất định là chết lúc tiến hành thử nghiệm cùng %s.",		-- 物品名:"物品栏物品"
		},
		HUMANMEAT = "Thịt chính là thịt. Có gì khác biệt?",		-- 物品名:"长猪"
		HUMANMEAT_COOKED = "Nấu lên trắng nõn, nhưng về đạo đức thì màu tro.",		-- 物品名:"煮熟的长猪"
		HUMANMEAT_DRIED = "Phơi khô thì không phài thịt người rồi, đúng không?",		-- 物品名:"长猪肉干"
		ROCK_MOON = "Cục đá đó đến từ mặt trăng.",		-- 物品名:"岩石"
		MOONROCKNUGGET = "Cục đá đó đến từ mặt trăng.",		-- 物品名:"月岩"
		MOONROCKCRATER = "Ta nên gắn cái thứ phát sáng đó vào trong để nghiên cứu.",		-- 物品名:"有洞的月亮石" 制造描述:"用于划定地盘的岩石。"
		MOONROCKSEED = "Hằng đêm nhớ đến nàng, đến cả trăng trên trời cũng buồn theo!",		-- 物品名:"天体宝球"
        REDMOONEYE = "Nó có thể thấy được bao cây số, cũng có thể bị nhìn thấy ngoài bây nhiêu cây số.",		-- 物品名:"红色月眼"
        PURPLEMOONEYE = "Trở thành nhà chế tạo giỏi, nhưng ta hy vọng nó đừng nhìn ta như vậy.",		-- 物品名:"紫色月眼"
        GREENMOONEYE = "Cái thứ này có mối quan hệ mật thiết với nơi đây.",		-- 物品名:"绿色月眼"
        ORANGEMOONEYE = "Có cái này hỗ trợ quan sát, không có ai bị lạc đường.",		-- 物品名:"橘色月眼"
        YELLOWMOONEYE = "Cái này sẽ chỉ đường cho từng người.",		-- 物品名:"黄色月眼"
        BLUEMOONEYE = "Cẩn thận quan sát luôn là điều sáng suốt.",		-- 物品名:"蓝色月眼"
        BOARRIOR = "Ngươi to thật!",		-- 物品名:"大熔炉猪战士"
        BOARON = "Ta đối phó hắn được!",		-- 物品名:"小猪"
        PEGHOOK = "Cái thứ mà nó phun ra có tính ăn mòn!",		-- 物品名:"蝎子"
        TRAILS = "Cánh ta của nó đô ghê.",		-- 物品名:"大猩猩"
        TURTILLUS = "Vỏ nó có gai nhọn!",		-- 物品名:"坦克龟"
        SNAPPER = "Cái con này cắn người đó.",		-- 物品名:"鳄鱼指挥官"
		RHINODRILL = "Cái mũi của nó rất thích hợp để kiếm sống.",		-- 物品名:"后扣帽犀牛兄弟"
		BEETLETAUR = "Ta đứng ở đây cũng ngửi thấy mùi của nó!",		-- 物品名:"地狱独眼巨猪"
        LAVAARENA_PORTAL = 
        {
            ON = "Ta phải đi đây.",		-- 物品名:"远古传送门"->开启
            GENERIC = "Đến được, thì chắc về được nhỉ?",		-- 物品名:"远古传送门"->默认
        },
        HEALINGSTAFF = "Dùng năng lực tái sinh.",		-- 物品名:"生存魔杖"
        FIREBALLSTAFF = "Gọi sao bằng từ trời xuống.",		-- 物品名:"地狱魔杖"
        HAMMER_MJOLNIR = "Cái búa này nặng thật.",		-- 物品名:"锻锤"
        SPEAR_GUNGNIR = "Ta có thể dùng nó để sạc nhanh.",		-- 物品名:"尖齿矛"
        BLOWDART_LAVA = "Ta có thể dùng món vũ khí này trong một phạm vi nhất định.",		-- 物品名:"吹箭"
        BLOWDART_LAVA2 = "Nó dùng luồng khí mạnh lưu chuyển để kích bắn.",		-- 物品名:"熔化吹箭"
        WEBBER_SPIDER_MINION = "Có thể nói bọn họ đang chiến đấu vì chúng ta.",		-- 物品名:"蜘蛛宝宝"
        BOOK_FOSSIL = "Như vậy có thể giữ chân đám quái vật được một chút.",		-- 物品名:"石化之书"
		SPEAR_LANCE = "Nó đánh thẳng vào chỗ hiểm.",		-- 物品名:"螺旋矛"
		BOOK_ELEMENTAL = "Ta không thấy rõ những chữ này.",		-- 物品名:"召唤之书"
        QUAGMIRE_ALTAR = 
        {
        	GENERIC = "Chúng ta tốt nhất bắt đầu nấu chút tế phẩm.",		-- 物品名:"饕餮祭坛"->默认
        	FULL = "Nó đang tiêu hóa.",		-- 物品名:"饕餮祭坛"->满了
    	},
		QUAGMIRE_SUGARWOODTREE = 
		{
			GENERIC = "Nó có rất nhiều nhựa cây ngọt ngon.",		-- 物品名:"糖木树"->默认
			STUMP = "Cái cây đó đi đâu rồi? Đúng là câu đố khó.",		-- 物品名:"糖木树"->暴食模式糖木树只剩树桩了
			TAPPED_EMPTY = "Nhựa rất nhiều.",		-- 物品名:"糖木树"->暴食模式糖木树空了
			TAPPED_READY = "Nhựa màu vàng thơm ngọt.",		-- 物品名:"糖木树"->暴食模式糖木树好了
			TAPPED_BUGS = "Kiến vì vậy mà mò đến.",		-- 物品名:"糖木树"->暴食模式糖木树上有蚂蚁
			WOUNDED = "Xem ra nó bệnh rồi.",		-- 物品名:"糖木树"->暴食糖木树生病了
		},
		QUAGMIRE_SPOTSPICE_SHRUB = 
		{
			GENERIC = "Nó khiến ta nhớ đến quái vật xúc tu.",		-- 物品名:"带斑点的小灌木"->默认
			PICKED = "Bụi cây này hái không được bao nhiêu quả.",		-- 物品名:"带斑点的小灌木"->被采完了
		},
		QUAGMIRE_SALT_RACK =
		{
			READY = "Muối bám đầy trên dây rồi.",		-- 物品名:"盐架"->准备好了
			GENERIC = "Dao ở Tịnh Châu bóng loáng như nước, muối ở nước Ngô trắng tinh hơn tuyết.",		-- 物品名:"盐架"->默认
		},
		QUAGMIRE_SAFE = 
		{
			GENERIC = "Đây là két sắt, dùng để giữ an toàn cho đồ vật.",		-- 物品名:"保险箱"->默认
			LOCKED = "Không có chìa khóa thì không mở được.",		-- 物品名:"保险箱"->锁住了
		},
		QUAGMIRE_MUSHROOMSTUMP =
		{
			GENERIC = "Mấy cái này là nấm sao? Đây đúng là vấn đề nan giải.",		-- 物品名:"蘑菇"->默认
			PICKED = "Chắc sẽ không mọc lại đâu.",		-- 物品名:"蘑菇"->被采完了
		},
        QUAGMIRE_RUBBLE_HOUSE = 
		{
			"Không một ai.",		-- 物品名:"残破的房子" 制造描述:未找到
			"Cái trấn nhỏ này bị tàn phá rồi.",		-- 物品名:"残破的房子" 制造描述:未找到
			"Không biết bọn họ đã chọc giận ai rồi.",		-- 物品名:"残破的房子" 制造描述:未找到
		},
        QUAGMIRE_SWAMPIGELDER =
        {
            GENERIC = "Ta đoán ngươi là lão đại ở đây?",		-- 物品名:"沼泽猪长老"->默认
            SLEEPING = "Nó ngủ rồi.",		-- 物品名:"沼泽猪长老"->睡着了
        },
        QUAGMIRE_FOOD =
        {
        	GENERIC = "Ta nên cúng nó lên bàn thờ Thao Thiết.",		-- 物品名:未找到
            MISMATCH = "Không phải là thứ mà nó muốn.",		-- 物品名:未找到
            MATCH = "Như vậy sẽ trấn an thần trên trời.",		-- 物品名:未找到
            MATCH_BUT_SNACK = "Thật đó, cái này rất giống đồ ăn vặt.",		-- 物品名:未找到
        },
        QUAGMIRE_PARK_GATE =
        {
            GENERIC = "Cần phải có chìa khóa.",		-- 物品名:"铁门"->默认
            LOCKED = "Khóa rất chặt.",		-- 物品名:"铁门"->锁住了
        },
        QUAGMIRE_PIGEON =
        {
            DEAD = "Chết rồi.",		-- 物品名:"鸽子"->死了 制造描述:"这是一只完整的活鸽。"
            GENERIC = "Lông cánh đầy đủ.",		-- 物品名:"鸽子"->默认 制造描述:"这是一只完整的活鸽。"
            SLEEPING = "Ngủ rồi.",		-- 物品名:"鸽子"->睡着了 制造描述:"这是一只完整的活鸽。"
        },
        WINONA_CATAPULT =
        {
            GENERIC = "Cô ấy tạo một hệ thống bảo vệ tự động.",		-- 物品名:"薇诺娜的投石机"->默认 制造描述:"向敌人投掷大石块"
            OFF = "Cần có điện.",		-- 物品名:"薇诺娜的投石机"->关闭 制造描述:"向敌人投掷大石块"
            BURNING = "Lửa cháy hừng hực!",		-- 物品名:"薇诺娜的投石机"->正在燃烧 制造描述:"向敌人投掷大石块"
            BURNT = "Ai cũng không cứu được nó.",		-- 物品名:"薇诺娜的投石机"->烧焦的 制造描述:"向敌人投掷大石块"
        },
        WINONA_SPOTLIGHT =
        {
            GENERIC = "Ý kiến hay!",		-- 物品名:"薇诺娜的聚光灯"->默认 制造描述:"白天夜里都发光"
            OFF = "Cần có điện.",		-- 物品名:"薇诺娜的聚光灯"->关闭 制造描述:"白天夜里都发光"
            BURNING = "Lửa cháy hừng hực!",		-- 物品名:"薇诺娜的聚光灯"->正在燃烧 制造描述:"白天夜里都发光"
            BURNT = "Ai cũng không cứu được nó.",		-- 物品名:"薇诺娜的聚光灯"->烧焦的 制造描述:"白天夜里都发光"
        },
        WINONA_BATTERY_LOW =
        {
            GENERIC = "Có vẻ khá mạnh. Sức mạnh này đến từ đâu?",		-- 物品名:"薇诺娜的发电机"->默认 制造描述:"要确保电力供应充足"
            LOWPOWER = "Lượng điện đang giảm.",		-- 物品名:"薇诺娜的发电机"->没电了 制造描述:"要确保电力供应充足"
            OFF = "Nếu Winona quá tải, ta vẫn có thể khiến nó hoạt động lại.",		-- 物品名:"薇诺娜的发电机"->关闭 制造描述:"要确保电力供应充足"
            BURNING = "Lửa cháy hừng hực!",		-- 物品名:"薇诺娜的发电机"->正在燃烧 制造描述:"要确保电力供应充足"
            BURNT = "Ai cũng không cứu được nó.",		-- 物品名:"薇诺娜的发电机"->烧焦的 制造描述:"要确保电力供应充足"
        },
        WINONA_BATTERY_HIGH =
        {
            GENERIC = "Ý tưởng hay!",		-- 物品名:"薇诺娜的宝石发电机"->默认 制造描述:"这玩意烧宝石，所以肯定不会差。"
            LOWPOWER = "Sắp hết điện rồi.",		-- 物品名:"薇诺娜的宝石发电机"->没电了 制造描述:"这玩意烧宝石，所以肯定不会差。"
            OFF = "Điện không đủ.",		-- 物品名:"薇诺娜的宝石发电机"->关闭 制造描述:"这玩意烧宝石，所以肯定不会差。"
            BURNING = "Lửa cháy hừng hực!",		-- 物品名:"薇诺娜的宝石发电机"->正在燃烧 制造描述:"这玩意烧宝石，所以肯定不会差。"
            BURNT = "Ai cũng không cứu được nó.",		-- 物品名:"薇诺娜的宝石发电机"->烧焦的 制造描述:"这玩意烧宝石，所以肯定不会差。"
        },
        COMPOSTWRAP = "Wormwood muốn ta thử một miếng, nhưng ta lịch sự từ chối.",		-- 物品名:"肥料包" 制造描述:"\"
        ARMOR_BRAMBLE = "Phòng thủ là cách tấn công tốt nhất.",		-- 物品名:"荆棘护甲" 制造描述:"让大自然告诉你什么叫\"
        TRAP_BRAMBLE = "Ai đạp trúng chắc chắn sẽ bị đâm.",		-- 物品名:"荆棘陷阱" 制造描述:"都有机会中招的干扰陷阱。"
    },
    DESCRIBE_GENERIC = "Nó là cái thứ... gì vậy.",		--检查物品描述的缺省值
    DESCRIBE_TOODARK = "Tối quá, không thấy gì hết!",		--天太黑
    DESCRIBE_SMOLDERING = "Cái thứ đó sắp cháy rồi.",		--冒烟
    EAT_FOOD =
    {
        TALLBIRDEGG_CRACKED = "Ồ, mỏ chim.",		--吃孵化的高脚鸟蛋
    },
}
