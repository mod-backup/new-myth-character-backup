local function DelaySpawnPrefab2(world, prefab, x, z, fn)
    local inst = SpawnPrefab(prefab)
    if inst then
        inst.Transform:SetPosition(x, 0, z)
        if fn then
            fn(inst)
        end
    end
end

local function DelaySpawnPrefab(time, prefab, x, z, fn)
    TheWorld:DoTaskInTime(time, DelaySpawnPrefab2, prefab, x, z, fn)
end

GLOBAL.HUA_SKILL_CONSTANTS = {}

-- 技能列表
HUA_SKILL_CONSTANTS.SKILL_NAME = {
    mk_yzqt = "一柱擎天",
    hua_beautiful = "花花不爱我",
}

-- 技能效果
HUA_SKILL_CONSTANTS.SKILL_CAST_FN = {
    mk_yzqt = function(self, caster, pos, target)
        local pillar = SpawnPrefab("mk_jgb_pillar")
        local skin = caster.AnimState:GetBuild()
        pillar.owner = caster
        pillar.Transform:SetPosition(pos:Get())
        pillar.AnimState:PlayAnimation("pillar_drop")
        pillar:DoTaskInTime(0.5, function() 
            pillar.Physics:SetActive(true) 
            pillar:DoDamage()
        end)
        pillar:SetSkin(skin)
    end,
    hua_beautiful = function(self, caster, pos, target)
        local dir = (pos - caster:GetPosition()):GetNormalized()
        local x, _, z = caster.Transform:GetWorldPosition()
        local step = 1.5
        for i = 0.5, 30.5 do
            DelaySpawnPrefab(0.01 * i, "explode_small", x + dir.x * i * step, z + dir.z * i * step)
        end
    end,
}

HUA_SKILL_CONSTANTS.SKILL_RETICULE_EDIT = {
    mk_yzqt = {
        reticule = "reticuleaoesummon",
        ping = "reticuleaoesummonping",
        sg = "mk_yzqt",
        cooldown = 10,
        range = 64,
    },
    hua_beautiful = {
        reticule = "reticuleline2",
        ping = "",
        cooldown = 10,
        reticulescale = 2,
        range = 999,
    },
}

local ROTATING_RETICULE = {
    reticuleline = true,
    reticuleline2 = true,
    reticulelong = true,
    reticulelongmulti = true,
}

local function UpdatePositionFn(inst, pos, reticule, ease, smoothing, dt, pingtarget)
    local self = inst.components.reticule

    if ROTATING_RETICULE[self.reticuleprefab] then
        local x, y, z = inst.Transform:GetWorldPosition()
        reticule.Transform:SetPosition(x, 0, z)
        local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
        if ease and dt ~= nil then
            local rot0 = reticule.Transform:GetRotation()
            local drot = rot - rot0
            rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
        end
        reticule.Transform:SetRotation(rot)
        return
    end

    reticule.Transform:SetPosition(pos:Get())
end

HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX = {}
for k, v in pairs(HUA_SKILL_CONSTANTS.SKILL_RETICULE_EDIT) do
    local data = {
        reticuleprefab = "reticuleaoe",
        pingprefab = "reticuleaoeping",
        updatepositionfn = UpdatePositionFn,
        validcolour = { 1, .75, 0, 1 },
        invalidcolour = { .5, 0, 0, 1 },
        ease = true,
        mouseenabled = true,
        ispassableatallpoints = true,
        range = v.range or 8,
        noreticule = v.noreticule,
        sg = v.sg,
        nosg = v.nosg,
    }

    if v.reticule then
        data.reticuleprefab = v.reticule
    end
    if v.ping then
        data.pingprefab = v.ping
        if data.pingprefab == "" then
            data.pingprefab = nil
        end
    end
    if v.reticulescale then
        data.animscale = v.reticulescale
    end
    if v.targettag then
        data.targettag = v.targettag
    end

    HUA_SKILL_CONSTANTS.SKILL_RETICULE_INDEX[k] = data
end

--[[
local Reticule = require("components/reticule")
local old_CreateReticule = Reticule.CreateReticule
function Reticule:CreateReticule()
    old_CreateReticule(self)

    if self.reticule then
        local animscale = self.animscale or 1
        self.reticule.AnimState:SetScale(animscale * 1.5, animscale * 1.5)
    end
end]]

local PlayerActionPicker = require("components/playeractionpicker")
local old_GetRightClickActions = PlayerActionPicker.GetRightClickActions
function PlayerActionPicker:GetRightClickActions(position, target)
    if self.inst.hua_skill then
        return {BufferedAction(self.inst, nil, ACTIONS.CASTAOE, self.inst, position, self.inst.hua_skill, 999)}
    end
    return old_GetRightClickActions(self, position, target)
end

ACTIONS.CASTAOE.stroverridefn = function(act)
    return act.recipe and HUA_SKILL_CONSTANTS.SKILL_NAME[act.recipe]
end

local old_CASTAOE = ACTIONS.CASTAOE.fn
ACTIONS.CASTAOE.fn = function(act)
    local act_pos = act:GetActionPoint()
    if act.recipe then -- 是超厉害得大大大果汁做的技能
        local doer = act.doer
        if doer and doer.components.myth_skill_caster then
            doer.components.myth_skill_caster:CastSkill(act.recipe, act_pos, act.target)
            return true
        end
        return false
    end
    return old_CASTAOE(act)
end