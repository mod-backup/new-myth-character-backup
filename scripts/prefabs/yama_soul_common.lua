
local function HasSoul(victim)
    return not (victim:HasTag("veggie") or --眼球草 西瓜南瓜之类的
                victim:HasTag("player") or --玩家不需要掉吧
                victim:HasTag("structure") or --建筑
                victim:HasTag("wall") or --墙
                victim:HasTag("balloon") or --气球
                victim:HasTag("soulless") or --没有灵魂的？
                victim:HasTag("chess") or --机械生物
                victim:HasTag("shadow") or --暗影生物
                victim:HasTag("shadowcreature") or --暗影生物
                victim:HasTag("shadowminion") or --暗影生物
                victim:HasTag("shadowchesspiece") or --暗影boss
                victim:HasTag("groundspike") or --某些奇怪的东西
                victim:HasTag("smashable"))
        and (not victim.components.myth_hassoul or victim.components.myth_hassoul.hassoul )
        and (  (victim.components.combat ~= nil and victim.components.health ~= nil)
            or victim.components.murderable ~= nil )
end

local function GetNumSouls(victim)
    -- return (victim:HasTag("ghost") and 2) --幽灵2
        -- or (victim:HasTag("epic") and math.random(7, 8)) --boss 需要加强吗？ 待定
        -- or 1
	return 1
end

local function GetSoul(victim)
    return victim.yama_soul
        or (victim:HasTag("ghost") and "soul_specter_spawn") --敌对生物
        or (victim:HasTag("hostile") and "soul_ghast_spawn") --敌对生物
        or (victim:HasTag("epic") and "soul_ghast_spawn") --boss
        or (victim:HasTag("monster") and "soul_ghast_spawn") --怪物
        or "soul_specter_spawn"
end

return {
    HasSoul = HasSoul,
    GetNumSouls = GetNumSouls,
    GetSoul = GetSoul,
}
