local prefs = {}

local colorBlack = { r = 116/255, g = 1/255, b = 55/255 }
local colorWhite = { r = 41/255, g = 160/255, b = 173/255 }

--------------------------------------------------------------------------
--[[ 通用函数 ]]
--------------------------------------------------------------------------

local function GetRandomPos(x, y, z, radius, angle)
    local rad = radius or math.random() * 3
    local ang = angle or math.random() * 2 * PI
    return x + rad * math.cos(ang), y, z - rad * math.sin(ang)
end

local skinvlaue = {
	yama_commissioners_lotus = "lotus",
	yama_commissioners = "none",
}
local function ExchangeItem(player, item, src_pos, changedata)
	local newitem = changedata[item.prefab]
	if newitem ~= nil then
		newitem = SpawnPrefab(newitem)
		if newitem ~= nil then
			if item.prevcontainer == nil then
				newitem.prevslot = item.prevslot
			end
			-- newitem.prevcontainer = item.prevcontainer --为了格子图片显示正确，取消容器回归机制
			newitem.Transform:SetPosition(player.Transform:GetWorldPosition())
			if item.OnPickExchange ~= nil then
				item.OnPickExchange(item, newitem, player)
			end

			if newitem.components.myth_itemskin ~= nil then
				--黑白因为自身皮肤的特性 当前的皮肤保持跟玩家皮肤一致不能混搭
				--print(player.components.skinner.skin_name)
				local skin = skinvlaue[player.components.skinner.skin_name] or "none"
				if newitem.components.myth_itemskin.skin:value() ~= skin then
					newitem.components.myth_itemskin:ChangeSkin(skin) 
				end
			end
			if item.isinequipslot then
				item:Remove()
				player.components.inventory:Equip(newitem)
			else
				item:Remove()
				player.components.inventory:GiveItem(newitem, nil, src_pos)
			end

			return true
		end
	end

	item.isinequipslot = nil
	return false
end

--这个方法不行了，装备函数与物品栏组件响应套得太深，不好改
--[[
local function ExchangeItemOnEquip(player, item)
	local itemmap = player:HasTag("commissioner_black") and {
		hat_commissioner_white = "hat_commissioner_black",
		pennant_commissioner = "whip_commissioner",
		bell_commissioner = "token_commissioner"
	} or {
		hat_commissioner_black = "hat_commissioner_white",
		whip_commissioner = "pennant_commissioner",
		token_commissioner = "bell_commissioner"
	}
	if itemmap[item.prefab] ~= nil then
		local newitem = SpawnPrefab(itemmap[item.prefab])
		if newitem ~= nil then
			if item.OnPickExchange ~= nil then
				item.OnPickExchange(item, newitem, player)
			end

			--装备新道具之前，需要移除原道具，不然又会触发ExchangeItem()，导致物品栏多一个相同的新道具
			player:DoTaskInTime(3*FRAMES, function()
				item.inventoryitem:SetOnPickupFn(nil)
				player.components.inventory:Unequip(item.components.equippable.equipslot)
				item.components.inventoryitem:OnRemoved()
				item.prevslot = nil
				item.prevcontainer = nil
				item:Remove()
				player.components.inventory:Equip(newitem)
			end)
		end
		return true
	end

	return false
end
]]--

local function OnPickUp(inst, pickupguy, src_pos)
	if pickupguy ~= nil then
		local needdrop = false
		if pickupguy:HasTag("player") then
			if pickupguy:HasTag("commissioner_black") then
				return ExchangeItem(pickupguy, inst, src_pos, {
					hat_commissioner_white = "hat_commissioner_black",
					pennant_commissioner = "whip_commissioner",
					bell_commissioner = "token_commissioner",
				})
			elseif pickupguy:HasTag("commissioner_white") then
				return ExchangeItem(pickupguy, inst, src_pos, {
					hat_commissioner_black = "hat_commissioner_white",
					whip_commissioner = "pennant_commissioner",
					token_commissioner = "bell_commissioner",
				})
			else
				needdrop = true
			end
		elseif
			pickupguy.prefab == "krampus" or
			pickupguy.prefab == "eyeplant" or pickupguy.prefab == "lureplant" or
			pickupguy.prefab == "monkey"
		then
			needdrop = true
		end

		if needdrop then
			if pickupguy.components.inventory ~= nil then
				pickupguy:DoTaskInTime(0, function()
					pickupguy.components.inventory:DropItem(inst, true, true)
				end)
			end
		end
	end
end

local function MakeItem(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			-- inst.entity:AddSoundEmitter()
			inst.entity:AddAnimState()
			inst.entity:AddNetwork()
			inst.entity:AddMiniMapEntity()

			inst.MiniMapEntity:SetIcon(data.name..".tex")

			inst:AddTag("nobundling") --该标签使其不能被放进打包纸
			inst:AddTag("commissioneritem")

			MakeInventoryPhysics(inst)

			if data.floatable ~= nil then
				MakeInventoryFloatable(inst, data.floatable[2], data.floatable[3], data.floatable[4])
				if data.floatable[1] ~= nil then
					local OnLandedClient_old = inst.components.floater.OnLandedClient
					inst.components.floater.OnLandedClient = function(self)
						OnLandedClient_old(self)
						self.inst.AnimState:SetFloatParams(data.floatable[1], 1, self.bob_percent)
					end
				end
			end

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				if data.fn_between ~= nil then
					data.fn_between(inst)
				end
				return inst
			end

			inst:AddComponent("inspectable")

			inst:AddComponent("inventoryitem")
			inst.components.inventoryitem.imagename = data.name
			inst.components.inventoryitem.atlasname = "images/inventoryimages/"..data.name..".xml"
			inst.components.inventoryitem:SetOnPickupFn(OnPickUp) --黑白无常才能捡起该物品

			inst:AddComponent("equippable")
			-- inst.components.equippable.restrictedtag = "yama_commissioners" --黑白无常才能装备该物品

			MakeHauntableLaunch(inst)

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

local function MakeFx(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddNetwork()

			inst:AddTag("FX")

			--Dedicated server does not need to spawn the local fx
			if not TheNet:IsDedicated() then
				--Delay one frame so that we are positioned properly before starting the effect
				--or in case we are about to be removed
				inst:DoTaskInTime(0, function(proxy)
					local inst2 = CreateEntity()

					inst2:AddTag("FX")
					inst2:AddTag("NOCLICK")
					--[[Non-networked entity]]

					inst2.entity:AddTransform()
					inst2.entity:AddAnimState()
					inst2.entity:SetCanSleep(false)
					inst2.persists = false

					local parent = proxy.entity:GetParent()
					if parent ~= nil then
						inst2.entity:SetParent(parent.entity)
					end

					inst2.Transform:SetFromProxy(proxy.GUID)

					if data.fn_anim ~= nil then
						data.fn_anim(inst2)
					end

					inst2:ListenForEvent("animover", inst2.Remove)
				end)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				return inst
			end

			inst.persists = false
			inst:DoTaskInTime(3, inst.Remove)

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

--------------------------------------------------------------------------
--[[ 黑：无常官帽 ]]
--------------------------------------------------------------------------

local function GetSoulNumber(hat, tag)
	local souls = hat.components.container:FindItem(function(item)
		return item:HasTag(tag)
	end)
	if souls ~= nil and souls.components.stackable ~= nil then
		return souls.components.stackable:StackSize() or 1
	else
		return 0
	end
end

local function UpdateSoulData(owner, hat, hands, numberghast, numberspecter)
	local numghast = numberghast or GetSoulNumber(hat, "soul_ghast")
	local numspecter = numberspecter or GetSoulNumber(hat, "soul_specter")

	--更新人物自身数据
	-- if owner ~= nil and owner.OnSoulChange ~= nil then
	-- 	owner.OnSoulChange(owner, numghast, "soul_ghast")
	-- 	owner.OnSoulChange(owner, numspecter, "soul_specter")
	-- end
	-- if owner ~= nil then
	-- 	if numghast > 0 then
	-- 		AddTag(owner, "myth_token_user")
	-- 	else
	-- 		RemoveTag(owner, "myth_token_user")
	-- 	end
	-- end

	--更新无常官帽数据
	if hat ~= nil and hat.OnSoulChange ~= nil then
		hat.OnSoulChange(hat, numghast+numspecter, "all")
	end

	--更新勾魂索数据
	if hands ~= nil and hands.OnSoulChange ~= nil then
		hands.OnSoulChange(hands, numghast, "soul_ghast")
	end
end

local function OnBoxItemChange(inst, data)
	local owner = inst.components.inventoryitem ~= nil and inst.components.inventoryitem:GetGrandOwner() or nil
	if
		owner ~= nil and owner:HasTag("yama_commissioners") and
		owner.components.inventory ~= nil and inst.components.container ~= nil and
		data ~= nil and (data.item or data.prev_item)
	then
		--更新数据：本来这里也控制善恶魂不能待在其他容器里，但目前改到善恶魂自己来判断
		UpdateSoulData(owner, inst, owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS), nil, nil)
	end
end

local function OnEquip_hat(inst, owner)
	if  inst.components.myth_itemskin.skin:value() == "lotus" then
		inst.components.myth_itemskin:OverrideSymbol(owner, "swap_body_tall")
	else
		owner.AnimState:ClearOverrideSymbol("swap_hat")
		if owner:HasTag("commissioner_white") then
			owner.AnimState:OverrideSymbol("headbase", "hat_commissioner_white", "headbase")
			owner.AnimState:OverrideSymbol("SWAP_FACE", "hat_commissioner_white", "SWAP_FACE")
		else
			owner.AnimState:OverrideSymbol("headbase", "hat_commissioner_black", "headbase")
			owner.AnimState:OverrideSymbol("SWAP_FACE", "hat_commissioner_black", "SWAP_FACE")
		end

		owner.AnimState:Show("HAT")
		owner.AnimState:Show("HAIR_HAT")
		owner.AnimState:Hide("HAIR_NOHAT")
		owner.AnimState:Hide("HAIR")

		owner.AnimState:Show("HEAD")
		owner.AnimState:Hide("HEAD_HAT")
	end

	if inst.components.container ~= nil then
        inst.components.container:Open(owner)

		if owner.components.inventory ~= nil then
			UpdateSoulData(owner, inst, owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS), nil, nil)
		end
    end
end

local function OnUnequip_hat(inst, owner)
	if  inst.components.myth_itemskin.skin:value() == "lotus" then
		owner.AnimState:ClearOverrideSymbol("swap_body_tall")
	else
		owner.AnimState:ClearOverrideSymbol("swap_hat")
		owner.AnimState:ClearOverrideSymbol("headbase")
		owner.AnimState:ClearOverrideSymbol("SWAP_FACE")

		owner.AnimState:Hide("HAT")
		owner.AnimState:Hide("HAIR_HAT")
		owner.AnimState:Show("HAIR_NOHAT")
		owner.AnimState:Show("HAIR")

		owner.AnimState:Show("HEAD")
		owner.AnimState:Hide("HEAD_HAT")
	end

	if inst.components.container ~= nil then
        inst.components.container:Close()

		if owner.components.inventory ~= nil then
			UpdateSoulData(owner, inst, owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS), 0, 0)
		end
    end
end

local function InitHat(inst)
	inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
	inst.components.equippable:SetOnEquip(OnEquip_hat)
	inst.components.equippable:SetOnUnequip(OnUnequip_hat)

	inst:AddComponent("waterproofer")
	inst.components.waterproofer:SetEffectiveness(TUNING.WATERPROOFNESS_SMALL)

	inst:AddComponent("insulator")

	inst:AddComponent("container")

	inst:AddComponent("armor")
    inst.components.armor:InitCondition(100, 0)
    inst.components.armor.indestructible = true --无敌的护甲
	inst.OnSoulChange = function(inst, soulnumber, tag)
		if soulnumber ~= nil and soulnumber >= 5 then
			inst.components.armor:InitCondition(100, math.min(0.8, math.floor(soulnumber/5)*0.1))
		else
			inst.components.armor:InitCondition(100, 0)
		end
	end

	inst:ListenForEvent("itemget", OnBoxItemChange) --得到整格物品时，触发该事件
	inst:ListenForEvent("itemlose", OnBoxItemChange) --失去整格物品时，或通过container组件移除时，触发该事件
	--以上两个事件只能监听整格物品的获取与失去，一旦只涉及物品数量的部分改变就无法监听

	inst.OnPickExchange = function(inst, newhat, player)
		inst:RemoveEventCallback("itemget", OnBoxItemChange)
		inst:RemoveEventCallback("itemlose", OnBoxItemChange)

		--将目前的善恶魂转移到新帽子里去
		if inst.components.container ~= nil and newhat.components.container ~= nil then
			local items = inst.components.container:RemoveAllItems()
			for i,v in ipairs(items) do
				newhat.components.container:GiveItem(v)
			end
		end
	end
end

----------

MakeItem({
	name = "hat_commissioner_black",
	assets = {
		Asset("ANIM", "anim/hat_commissioner_black.zip"),
		Asset("ANIM", "anim/hat_commissioner_white.zip"),
		Asset("ANIM", "anim/hat_commissioner_black_lotus.zip"),
		Asset("ANIM", "anim/hat_commissioner_white_lotus.zip"),
		Asset("ATLAS", "images/inventoryimages/hat_commissioner_black.xml"),
		Asset("IMAGE", "images/inventoryimages/hat_commissioner_black.tex"),
		Asset("ATLAS", "images/inventoryimages/hat_commissioner_black_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/hat_commissioner_black_lotus.tex"),
	},
	prefabs = nil,
	floatable = {0.07, "med", 0.15, 0.6},
	fn_common = function(inst)
		inst.AnimState:SetBank("hat_commissioner_black")
		inst.AnimState:SetBuild("hat_commissioner_black")
		inst.AnimState:PlayAnimation("anim")

		inst:AddTag("hat")
		inst:AddTag("waterproofer")
		inst:AddTag("soullostbox") --这个标签代表能装善恶魂
		inst:AddTag("myth_removebydespwn")
		inst:AddTag("hide_percentage")

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "hat_commissioner_black"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "hat_commissioner_black_lotus", folder = "swap_body_tall"},
				icon = {atlas = "hat_commissioner_black_lotus.xml", image = "hat_commissioner_black_lotus"},
				anim = {bank = "hat_commissioner_black_lotus", build = "hat_commissioner_black_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "hat_commissioner_black", folder = "headbase"},
				icon = {atlas = "hat_commissioner_black.xml", image = "hat_commissioner_black"},
				anim = {bank = "hat_commissioner_black", build = "hat_commissioner_black", anim = "anim"},
			}
		}
	end,
	fn_between = function(inst)
		inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup("hat_commissioner_black") end
	end,
	fn_server = function(inst)
		InitHat(inst)

		inst.components.equippable.restrictedtag = "commissioner_black"

        -- inst.components.insulator:SetSummer()
        inst.components.insulator:SetInsulation(TUNING.INSULATION_SMALL)

    	inst.components.container:WidgetSetup("hat_commissioner_black")
	end,
})

--------------------------------------------------------------------------
--[[ 白：无常官帽 ]]
--------------------------------------------------------------------------

MakeItem({
	name = "hat_commissioner_white",
	assets = {
		Asset("ANIM", "anim/hat_commissioner_black.zip"),
		Asset("ANIM", "anim/hat_commissioner_white.zip"),
		Asset("ATLAS", "images/inventoryimages/hat_commissioner_white.xml"),
		Asset("IMAGE", "images/inventoryimages/hat_commissioner_white.tex"),
		Asset("ATLAS", "images/inventoryimages/hat_commissioner_white_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/hat_commissioner_white_lotus.tex"),
	},
	prefabs = nil,
	floatable = {0.05, "med", 0.2, 0.6},
	fn_common = function(inst)
		inst.AnimState:SetBank("hat_commissioner_white")
		inst.AnimState:SetBuild("hat_commissioner_white")
		inst.AnimState:PlayAnimation("anim")

		inst:AddTag("hat")
		inst:AddTag("waterproofer")
		inst:AddTag("soullostbox")
		inst:AddTag("myth_removebydespwn")
		inst:AddTag("hide_percentage")

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "hat_commissioner_white"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "hat_commissioner_white_lotus", folder = "swap_body_tall"},
				icon = {atlas = "hat_commissioner_white_lotus.xml", image = "hat_commissioner_white_lotus"},
				anim = {bank = "hat_commissioner_white_lotus", build = "hat_commissioner_white_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "hat_commissioner_white", folder = "headbase"},
				icon = {atlas = "hat_commissioner_white.xml", image = "hat_commissioner_white"},
				anim = {bank = "hat_commissioner_white", build = "hat_commissioner_white", anim = "anim"},
			}
		}
	end,
	fn_between = function(inst)
		inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup("hat_commissioner_white") end
	end,
	fn_server = function(inst)
		InitHat(inst)

		inst.components.equippable.restrictedtag = "commissioner_white"

        inst.components.insulator:SetSummer()
        inst.components.insulator:SetInsulation(TUNING.INSULATION_SMALL)

    	inst.components.container:WidgetSetup("hat_commissioner_white")
	end,
})

--------------------------------------------------------------------------
--[[ 黑：勾魂索 ]]
--------------------------------------------------------------------------

local whipDamage = { 42.5, 68, 81.6 }
local whipRange = { TUNING.WHIP_RANGE, TUNING.WHIP_RANGE/2, TUNING.WHIP_RANGE/2 }

local function canuseininventory_myth(inst,doer)
	return inst.replica.equippable:IsEquipped() and doer and
	doer.replica.inventory ~= nil and
	doer.replica.inventory:IsOpenedBy(doer)
end

MakeItem({
	name = "whip_commissioner",
	assets = {
		Asset("ANIM", "anim/whip_commissioner.zip"),
		Asset("ANIM", "anim/swap_whip_commissioner.zip"),
		Asset("ANIM", "anim/whip_commissioner_lotus.zip"),
		Asset("ANIM", "anim/swap_whip_commissioner_lotus.zip"),
		Asset("ATLAS", "images/inventoryimages/whip_commissioner.xml"),
		Asset("IMAGE", "images/inventoryimages/whip_commissioner.tex"),
		Asset("ATLAS", "images/inventoryimages/whip_commissioner_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/whip_commissioner_lotus.tex"),
	},
	prefabs = nil,
	floatable = {nil, "large", 0.1, 0.45},
	fn_common = function(inst)
		inst.AnimState:SetBank("whip_commissioner")
		inst.AnimState:SetBuild("whip_commissioner")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("myth_whip")
		--inst:AddTag("pocketwatch")
		inst:AddTag("weapon")
		inst:AddTag("myth_removebydespwn")
    	inst.MYTH_USE_TYPE = "WHIP"
		inst.onusesgname = "doshortaction"
		inst.myth_use_needtag = "commissioner_black"
		inst.canuseininventory_myth = canuseininventory_myth

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "whip_commissioner"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "swap_whip_commissioner_lotus", folder = "swap_object"},
				icon = {atlas = "whip_commissioner_lotus.xml", image = "whip_commissioner_lotus"},
				anim = {bank = "whip_commissioner_lotus", build = "whip_commissioner_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "swap_whip_commissioner", folder = "swap_object"},
				icon = {atlas = "whip_commissioner.xml", image = "whip_commissioner"},
				anim = {bank = "whip_commissioner", build = "whip_commissioner", anim = "idle"},
			}
		}
	end,
	fn_between = nil,
	fn_server = function(inst)
		inst.OnSoulChange = function(inst, soulnumber, tag)
			local idx = 1
			if soulnumber ~= nil then
				if soulnumber > 17 then
					idx = 3
				elseif soulnumber > 12 then
					idx = 2
				end
			end
			inst.components.weapon:SetDamage(whipDamage[idx])
			inst.components.weapon:SetRange(whipRange[idx])
		end

		inst.components.equippable.restrictedtag = "commissioner_black"
		inst.components.equippable.equipslot = EQUIPSLOTS.HANDS
		inst.components.equippable:SetOnEquip(function(inst, owner)
			local build = inst.AnimState:GetBuild()
			owner.AnimState:OverrideSymbol("swap_object", "swap_"..build, "swap_object")
			owner.AnimState:OverrideSymbol("whipline", "swap_"..build, "whipline")

			owner.AnimState:Show("ARM_carry")
			owner.AnimState:Hide("ARM_normal")

			--装备时更新一下数值
			if owner.components.inventory ~= nil then
				local hat = owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
				if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
					inst.OnSoulChange(inst, GetSoulNumber(hat, "soul_ghast"), "soul_ghast")
					return
				end
			end
			inst.OnSoulChange(inst, nil, nil)
		end)
		inst.components.equippable:SetOnUnequip(function(inst, owner)
			-- owner.AnimState:ClearOverrideSymbol("swap_object")
			owner.AnimState:ClearOverrideSymbol("whipline")
			owner.AnimState:Hide("ARM_carry")
    		owner.AnimState:Show("ARM_normal")

			--脱下时直接恢复默认数值
			inst.OnSoulChange(inst, nil, nil)
		end)

		inst:AddComponent("weapon")
		inst.OnSoulChange(inst, nil, nil)
		inst.components.weapon:SetOnAttack(function(inst, attacker, target)
			if target ~= nil and target:IsValid() then
				local x, y, z = inst.Transform:GetWorldPosition()
				local x1, y1, z1 = target.Transform:GetWorldPosition()
				local angle = -math.atan2(z1 - z, x1 - x)

				local snap = SpawnPrefab("impact")
				snap.Transform:SetPosition(x1, y1, z1)
				snap.Transform:SetRotation(angle * RADIANS)
			end
		end)

		inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = false
		inst.components.myth_use_inventory.canusescene  = false
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if doer ~= nil and doer.components.inventory ~= nil then
				local hat = doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
				if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
					local has = hat.components.container:Has("soul_ghast", 5)
					if has then
						hat.components.container:ConsumeByName("soul_ghast", 5)
						if doer.components.debuffable ~= nil and doer.components.debuffable:IsEnabled() and
							not (doer.components.health ~= nil and doer.components.health:IsDead()) and
							not doer:HasTag("playerghost") then
							doer.components.debuffable:AddDebuff("whip_commissioner_buff", "whip_commissioner_buff")
						end
					else
						doer.components.talker:Say(GetString(doer, "DESCRIBE", {"TOKEN_COMMISSIONER", "NOSOUL"}))
					end
				else
					doer.components.talker:Say(GetString(doer, "DESCRIBE", {"TOKEN_COMMISSIONER", "NOSOUL"}))
				end
				return true
			end
		end)
	end,
})

--------------------------------------------------------------------------
--[[ 黑：镇魂令 ]]
--------------------------------------------------------------------------

--从singinginspiration.lua中复制
local function HasFriendlyLeader(target, singer, PVP_enabled)
    local target_leader = (target.components.follower ~= nil) and target.components.follower.leader or nil

    if target_leader and target_leader.components.inventoryitem then
        target_leader = target_leader.components.inventoryitem:GetGrandOwner()
        -- Don't attack followers if their follow object has no owner, unless its pvp, then there are no rules!
        if target_leader == nil then
            return not PVP_enabled
        end
    end

    return  (target_leader ~= nil and (target_leader == singer or (not PVP_enabled and target_leader:HasTag("player"))))
			or (not PVP_enabled and target.components.domesticatable and target.components.domesticatable:IsDomesticated())
			or (not PVP_enabled and target.components.saltlicker and target.components.saltlicker.salted)
end

local function AddEnemyDebuffFx(target, fxnamepre)
    -- target:DoTaskInTime(math.random()*0.25, function()
    --     local x, y, z = target.Transform:GetWorldPosition()
    --     local fx = SpawnPrefab(fx) --"battlesong_instant_panic_fx"
    --     if fx then
    --         fx.Transform:SetPosition(x, y, z)
    --     end

    --     return fx
    -- end)

	if target.components.combat ~= nil and not target:HasTag("ghost") then
		if target.blocksouldofxtask == nil then
			local fx = SpawnPrefab(fxnamepre.."_do_fx")
			if fx ~= nil then
				target.blocksouldofxtask = target:DoTaskInTime(.5, function(target)
					target.blocksouldofxtask = nil
				end)

				fx.entity:AddFollower():FollowSymbol(target.GUID, target.components.combat.hiteffectsymbol, 0, -50, 0)
				fx:Setup(target)
			end
		end
	else
		local fx = SpawnPrefab(fxnamepre.."_do_fx")
		if fx ~= nil then
			fx.Transform:SetPosition(target.Transform:GetWorldPosition())
		end
	end
end

MakeItem({
	name = "token_commissioner",
	assets = {
		Asset("ANIM", "anim/token_commissioner.zip"),
		Asset("ANIM", "anim/swap_token_commissioner.zip"),
		Asset("ANIM", "anim/token_commissioner_lotus.zip"),
		Asset("ANIM", "anim/swap_token_commissioner_lotus.zip"),
		Asset("ATLAS", "images/inventoryimages/token_commissioner.xml"),
		Asset("IMAGE", "images/inventoryimages/token_commissioner.tex"),
		Asset("ATLAS", "images/inventoryimages/token_commissioner_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/token_commissioner_lotus.tex"),
	},
	prefabs = {"token_commissioner_dot_fx", "token_commissioner_ground_fx", "soul_ghast_do_fx",},
	floatable = {0.15, "small", 0.4, 0.6},
	fn_common = function(inst)
		inst.AnimState:SetBank("token_commissioner")
		inst.AnimState:SetBuild("token_commissioner")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("nopunch") --无武器组件的装备攻击时默认伤害为 UNARMED_DAMAGE = 10
		inst:AddTag("myth_removebydespwn")

		inst.myth_use_needtag = "commissioner_black"
    	inst.MYTH_USE_TYPE = nil
		inst.onusesgname = "use_token_commissioner"

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "token_commissioner"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "swap_token_commissioner_lotus", folder = "swap_object"},
				icon = {atlas = "token_commissioner_lotus.xml", image = "token_commissioner_lotus"},
				anim = {bank = "token_commissioner_lotus", build = "token_commissioner_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "swap_token_commissioner", folder = "swap_object"},
				icon = {atlas = "token_commissioner.xml", image = "token_commissioner"},
				anim = {bank = "token_commissioner", build = "token_commissioner", anim = "idle"},
			}
		}
	end,
	fn_between = nil,
	fn_server = function(inst)
		inst.components.equippable.restrictedtag = "commissioner_black"
		inst.components.equippable.equipslot = EQUIPSLOTS.HANDS
		inst.components.equippable:SetOnEquip(function(inst, owner)
			inst.components.myth_itemskin:OverrideSymbol(owner, "swap_object")
			--owner.AnimState:OverrideSymbol("swap_object", "swap_token_commissioner", "swap_object")
			owner.AnimState:Show("ARM_carry")
			owner.AnimState:Hide("ARM_normal")
		end)
		inst.components.equippable:SetOnUnequip(function(inst, owner)
			-- owner.AnimState:ClearOverrideSymbol("swap_object")
			owner.AnimState:Hide("ARM_carry")
    		owner.AnimState:Show("ARM_normal")
		end)

		inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = true
		inst.components.myth_use_inventory.canusescene  = false
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if doer ~= nil and doer.components.inventory ~= nil then
				local hat = doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
				if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
					local souls = hat.components.container:FindItem(function(item)
						return item:HasTag("soul_ghast")
					end)
					if souls ~= nil then
						if souls.components.stackable ~= nil then --由于只消耗1，所以这里不需要提前判断总数量
							souls = souls.components.stackable:Get()
						end
						souls:Remove()

						--触发施法台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"TOKEN_COMMISSIONER", "SPELL"}))
						end

						--惊恐附近生物
						local x, y, z = doer.Transform:GetWorldPosition()
						local entities_near_me = TheSim:FindEntities(
							x, y, z, TUNING.BATTLESONG_ATTACH_RADIUS,
							{"_combat", "_health"},
							{"INLIMBO", "epic", "structure", "wall", "balloon", "groundspike", "smashable", "companion"}
						)
						local PVP_enabled = TheNet:GetPVPEnabled()
						for _, ent in ipairs(entities_near_me) do
							if
								ent.components.hauntable ~= nil and ent.components.hauntable.panicable
								and doer.components.combat:CanTarget(ent)
								and not HasFriendlyLeader(ent, doer, PVP_enabled)
								and (not ent:HasTag("prey") or (ent:HasTag("prey") and ent:HasTag("hostile")))
							then
								ent.components.hauntable:Panic(9)
								AddEnemyDebuffFx(ent, "soul_ghast")
							end
						end

						--生成特效
						SpawnPrefab("token_commissioner_dot_fx").Transform:SetPosition(x, y, z)
						SpawnPrefab("token_commissioner_ground_fx").Transform:SetPosition(x, y, z)
						inst:DoTaskInTime(0.15, function()
							SpawnPrefab("token_commissioner_ground_fx").Transform:SetPosition(x, y, z)
						end)
						inst:DoTaskInTime(0.3, function()
							SpawnPrefab("token_commissioner_ground_fx").Transform:SetPosition(x, y, z)
						end)
					else
						--触发缺恶魂的台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"TOKEN_COMMISSIONER", "NOSOUL"}))
						end
					end
				else
					--触发缺无常官帽的台词
					if doer.components.talker ~= nil then
						doer.components.talker:Say(GetString(doer, "DESCRIBE", {"TOKEN_COMMISSIONER", "NOHAT"}))
					end
				end

				return true
			end
			return false
		end)
	end,
})

--------------------------------------------------------------------------
--[[ 白：招魂幡 ]]
--------------------------------------------------------------------------

local function SpawnGhost(doer, x, y, z)
	if doer == nil then
		return
	end

	local ghost = SpawnPrefab("ghost_clement")
	if ghost ~= nil then
		-- if ghost.components.colouradder == nil then --这个组件的效果太过了，绝绝子
		-- 	ghost:AddComponent("colouradder")
		-- end
		-- ghost.components.colouradder:PushColour("pennant_commissioner", 119/255, 197/255, 192/255, 0.1)
		-- ghost.AnimState:SetMultColour(119/255, 197/255, 192/255, 1) --这个方式的效果会在鬼魂攻击后消失，无语子

		if ghost.OnLinkPlayer ~= nil then
			ghost.OnLinkPlayer(ghost, doer)
		end
		ghost.Transform:SetPosition(x, y, z)
		AddEnemyDebuffFx(ghost, "soul_specter")
	end
end

local function onattack(inst, attacker, target, skipsanity)
    if not target:IsValid() then
        return
	elseif
		attacker and not target:IsInLimbo() and
		target.components.hauntable ~= nil and
        not (target.components.inventoryitem ~= nil and target.components.inventoryitem:IsHeld()) and
        not (target:HasTag("haunted") or target:HasTag("catchable"))
	then
		if target:HasTag("pass_commissioner") then
			local exchangedata = {
				pass_commissioner_ylw = "pass_commissioner_wz",
				pass_commissioner_wz = "pass_commissioner_zk",
				pass_commissioner_zk = "pass_commissioner_cj",
				pass_commissioner_cj = "pass_commissioner_lzd",
				pass_commissioner_lzd = "pass_commissioner_mp",
				pass_commissioner_mp = "pass_commissioner_ylw",
			}
			local newpass = exchangedata[target.prefab] or "pass_commissioner_ylw"
			local isactived = target.isactived
			newpass = ReplacePrefab(target, newpass)
			if newpass ~= nil then
				attacker:PushEvent("haunt", { target = newpass })
				newpass.components.hauntable:DoHaunt(attacker)
				if isactived then --如果之前是激活状态，继续激活状态
					newpass.fn_mythuse(newpass, nil, true)
				end
			end
		else
			attacker:PushEvent("haunt", { target = target })
			target.components.hauntable:DoHaunt(attacker)
		end
	end
end

local function onspell(inst, doer, pos)
	if doer ~= nil and doer.components.inventory ~= nil then
		local hat = doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
		if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
			local souls = hat.components.container:FindItem(function(item)
				return item:HasTag("soul_specter")
			end)
			if
				souls ~= nil and souls.components.stackable ~= nil
				and souls.components.stackable:StackSize() ~= nil and souls.components.stackable:StackSize() >= 3
			then
				if souls.components.stackable:StackSize() == 3 then
					souls = hat.components.container:RemoveItem(souls, true)
				else --由于container:RemoveItem只会在整个叠加物移除时才推事件，所以这里主动更新一下
					souls = souls.components.stackable:Get(3)
					OnBoxItemChange(hat, {prev_item = souls})
				end
				souls:Remove()

				--触发施法台词
				if doer.components.talker ~= nil then
					doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "SPELL"}))
				end

				--生成助战鬼魂
				local x, y, z = pos.x,0,pos.z  --doer.Transform:GetWorldPosition()
				for i = 1, 3, 1 do
					local x2, y2, z2 = GetRandomPos(x, y, z, 2+math.random()*3)
					SpawnPrefab("white_bone_raisefx").Transform:SetPosition(x2, y2, z2)
					doer:DoTaskInTime(0.5+math.random()*0.5, function()
						SpawnGhost(doer, x2, y2, z2)
					end)
				end
				--生成特效
				SpawnPrefab("pennant_commissioner_dot_fx").Transform:SetPosition(x, y, z)
				SpawnPrefab("pennant_commissioner_ground_fx").Transform:SetPosition(x, y, z)
				inst.components.rechargeable:StartRecharging()
			else
				--触发缺善魂的台词
				if doer.components.talker ~= nil then
					doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "NOSOUL"}))
				end
			end
		else
			--触发缺无常官帽的台词
			if doer.components.talker ~= nil then
				doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "NOHAT"}))
			end
		end
		--return true
	end
end

local function ReticuleTargetFn()
	local player = ThePlayer
	local ground = TheWorld.Map
	local pos = Vector3()
	for r = 7, 0, -.25 do
	  pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
	  if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
		return pos
	  end
	end
	return pos
end

local function aoespellsg(inst,doer,action)
    return "use_pennant_commissioner"
end

MakeItem({
	name = "pennant_commissioner",
	assets = {
		Asset("ANIM", "anim/pennant_commissioner.zip"),
		Asset("ANIM", "anim/swap_pennant_commissioner.zip"),
		Asset("ANIM", "anim/pennant_commissioner_lotus.zip"),
		Asset("ANIM", "anim/swap_pennant_commissioner_lotus.zip"),
		Asset("ATLAS", "images/inventoryimages/pennant_commissioner.xml"),
		Asset("IMAGE", "images/inventoryimages/pennant_commissioner.tex"),
		Asset("ATLAS", "images/inventoryimages/pennant_commissioner_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/pennant_commissioner_lotus.tex"),
	},
	prefabs = {
		"pennant_commissioner_dot_fx", "pennant_commissioner_ground_fx", "ghost_clement",
		"white_bone_raisefx", "soul_specter_do_fx",
	},
	floatable = {0.2, "small", 0.4, 0.4},
	fn_common = function(inst)
		inst.AnimState:SetBank("pennant_commissioner")
		inst.AnimState:SetBuild("pennant_commissioner")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("nopunch")
		inst:AddTag("pennant_commissioner")
		inst:AddTag("weapon")
		inst:AddTag("rangedweapon")
		inst:AddTag("myth_removebydespwn")
		inst:AddTag("rechargeable")

		inst:AddComponent("aoetargeting")
		inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoesmall"
		inst.components.aoetargeting.reticule.pingprefab = "reticuleaoesmallping"
		inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
		inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
		inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
		inst.components.aoetargeting.reticule.ease = true
		inst.components.aoetargeting.reticule.mouseenabled = true
		inst.components.aoetargeting:SetRange(12)

		inst.myth_aoespellsg = aoespellsg

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "pennant_commissioner"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "swap_pennant_commissioner_lotus", folder = "swap_object"},
				icon = {atlas = "pennant_commissioner_lotus.xml", image = "pennant_commissioner_lotus"},
				anim = {bank = "pennant_commissioner_lotus", build = "pennant_commissioner_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "swap_pennant_commissioner", folder = "swap_object"},
				icon = {atlas = "pennant_commissioner.xml", image = "pennant_commissioner"},
				anim = {bank = "pennant_commissioner", build = "pennant_commissioner", anim = "idle"},
			}
		}
	end,
	fn_between = nil,
	fn_server = function(inst)
		inst.components.equippable.restrictedtag = "commissioner_white"
		inst.components.equippable.equipslot = EQUIPSLOTS.HANDS
		inst.components.equippable:SetOnEquip(function(inst, owner)
			--owner.AnimState:OverrideSymbol("swap_object", "swap_pennant_commissioner", "swap_object")
			inst.components.myth_itemskin:OverrideSymbol(owner, "swap_object")
			owner.AnimState:Show("ARM_carry")
			owner.AnimState:Hide("ARM_normal")
		end)
		inst.components.equippable:SetOnUnequip(function(inst, owner)
			-- owner.AnimState:ClearOverrideSymbol("swap_object")
			owner.AnimState:Hide("ARM_carry")
    		owner.AnimState:Show("ARM_normal")
		end)

		inst:AddComponent("myth_rechargeable")
		inst.components.rechargeable = inst.components.myth_rechargeable
		inst.components.rechargeable:SetRechargeTime(10)
		inst:RegisterComponentActions("rechargeable")

		inst:AddComponent("weapon")
		inst.components.weapon:SetDamage(0)
		inst.components.weapon:SetRange(8, 10)
		inst.components.weapon:SetOnAttack(onattack)
		inst.components.weapon:SetProjectile("yama_fire_projetile")

		inst:AddComponent("myth_aoespell")
		inst.components.aoespell = inst.components.myth_aoespell
		inst.components.aoespell:SetSpellFn(onspell)
		inst:RegisterComponentActions("aoespell")

		--[[inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = true
		inst.components.myth_use_inventory.canusescene  = false
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if doer ~= nil and doer.components.inventory ~= nil then
				local hat = doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
				if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
					local souls = hat.components.container:FindItem(function(item)
						return item:HasTag("soul_specter")
					end)
					if
						souls ~= nil and souls.components.stackable ~= nil
						and souls.components.stackable:StackSize() ~= nil and souls.components.stackable:StackSize() >= 3
					then
						if souls.components.stackable:StackSize() == 3 then
							souls = hat.components.container:RemoveItem(souls, true)
						else --由于container:RemoveItem只会在整个叠加物移除时才推事件，所以这里主动更新一下
							souls = souls.components.stackable:Get(3)
							OnBoxItemChange(hat, {prev_item = souls})
						end
						souls:Remove()

						--触发施法台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "SPELL"}))
						end

						--生成助战鬼魂
						local x, y, z = doer.Transform:GetWorldPosition()
						for i = 1, 3, 1 do
							local x2, y2, z2 = GetRandomPos(x, y, z, 2+math.random()*3)
							SpawnPrefab("white_bone_raisefx").Transform:SetPosition(x2, y2, z2)
							doer:DoTaskInTime(0.5+math.random()*0.5, function()
								SpawnGhost(doer, x2, y2, z2)
							end)
						end

						--生成特效
						SpawnPrefab("pennant_commissioner_dot_fx").Transform:SetPosition(x, y, z)
						SpawnPrefab("pennant_commissioner_ground_fx").Transform:SetPosition(x, y, z)
					else
						--触发缺善魂的台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "NOSOUL"}))
						end
					end
				else
					--触发缺无常官帽的台词
					if doer.components.talker ~= nil then
						doer.components.talker:Say(GetString(doer, "DESCRIBE", {"PENNANT_COMMISSIONER", "NOHAT"}))
					end
				end

				return true
			end
			return false
		end)]]
	end,
})

--------------------------------------------------------------------------
--[[ 白：摄魂铃 ]]
--------------------------------------------------------------------------

local function SetBuffAround(doer, x, y, z)
	local PVP_enabled = TheNet:GetPVPEnabled()
	local ents = TheSim:FindEntities(
		x, y, z, 20,
		nil,
		{"ghost", "white_bone", "bone_pet", "shadowcreature", "shadow", "playerghost", "FX", "DECOR", "INLIMBO"},
		{"sleeper", "player", "tendable_farmplant"}
	)
	for _, v in ipairs(ents) do
		if v ~= doer and v:IsValid() then
			if v.components.farmplanttendable ~= nil then --照顾农作物
				if v:GetDistanceSqToInst(doer) < 16 then
					v.components.farmplanttendable:TendTo(doer)
				end
			elseif
				(v:HasTag("player") and PVP_enabled) or --pvp模式下对玩家也生效
				(not v:HasTag("player") and not HasFriendlyLeader(v, doer, PVP_enabled)) --不对跟随者生效
			then
				if	--没有处于无法睡眠的状态
					not (v.components.freezable ~= nil and v.components.freezable:IsFrozen()) and
					not (v.components.pinnable ~= nil and v.components.pinnable:IsStuck()) and
					not (v.components.fossilizable ~= nil and v.components.fossilizable:IsFossilized())
				then
					--让坐骑睡着
					local mount = v.components.rider ~= nil and v.components.rider:GetMount() or nil
					if mount ~= nil then
						mount:PushEvent("ridersleep", {sleepiness = 5, sleeptime = 5})
					end

					if v:HasTag("player") then
						v:PushEvent("yawn", {grogginess = 5, knockoutduration = 5})
					elseif v.components.sleeper ~= nil then
						v.components.sleeper:AddSleepiness(5, 5)
					elseif v.components.grogginess ~= nil then
						v.components.grogginess:AddGrogginess(5, 5)
					else
						v:PushEvent("knockedout")
					end
				end

				if v.task_bell_myth == nil then
					if v.components.combat ~= nil then
						v.components.combat.externaldamagemultipliers:SetModifier("bell_commissioner", 0.75)
					end
					if v.components.locomotor ~= nil then
						v.components.locomotor:SetExternalSpeedMultiplier(doer, "bell_commissioner", 0.75)
					end
				else
					v.task_bell_myth:Cancel()
					v.task_bell_myth = nil
				end
				if v.components.combat ~= nil or v.components.locomotor ~= nil then
					if v.components.colouradder == nil then
						v:AddComponent("colouradder")
					end
					v.components.colouradder:PushColour("bell_commissioner", 40/255, 127/255, 173/255, 0.1)

					v.task_bell_myth = v:DoTaskInTime(14+math.random()*2, function(v)
						if v.components.combat ~= nil then
							v.components.combat.externaldamagemultipliers:RemoveModifier("bell_commissioner")
						end
						if v.components.locomotor ~= nil and doer ~= nil then
							v.components.locomotor:RemoveExternalSpeedMultiplier(doer, "bell_commissioner")
						end
						if v.components.colouradder ~= nil then
							v.components.colouradder:PopColour("bell_commissioner")
						end
						v.task_bell_myth = nil
					end)

					AddEnemyDebuffFx(v, "soul_specter")
				end
			end
		end

	end
end

local function SetBellFx(bell, doer, x, y, z)
	SpawnPrefab("pennant_commissioner_dot_fx").Transform:SetPosition(x, y, z)
	SpawnPrefab("bell_commissioner_ground_fx").Transform:SetPosition(x, y, z)
	-- bell:DoTaskInTime(0.15, function()
	-- 	SpawnPrefab("bell_commissioner_ground_fx").Transform:SetPosition(x, y, z)
	-- end)
end

MakeItem({
	name = "bell_commissioner",
	assets = {
		Asset("ANIM", "anim/bell_commissioner.zip"),
		Asset("ANIM", "anim/swap_bell_commissioner.zip"),
		Asset("ANIM", "anim/bell_commissioner_lotus.zip"),
		Asset("ANIM", "anim/swap_bell_commissioner_lotus.zip"),
		Asset("ATLAS", "images/inventoryimages/bell_commissioner.xml"),
		Asset("IMAGE", "images/inventoryimages/bell_commissioner.tex"),
		Asset("ATLAS", "images/inventoryimages/bell_commissioner_lotus.xml"),
		Asset("IMAGE", "images/inventoryimages/bell_commissioner_lotus.tex"),
	},
	prefabs = {"pennant_commissioner_dot_fx", "bell_commissioner_ground_fx", "soul_specter_do_fx",},
	floatable = {0.15, "small", 0.4, 0.6},
	fn_common = function(inst)
		inst.AnimState:SetBank("bell_commissioner")
		inst.AnimState:SetBuild("bell_commissioner")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("nopunch")
		inst:AddTag("myth_removebydespwn")

		inst.myth_use_needtag = "commissioner_white"
    	inst.MYTH_USE_TYPE = nil
		inst.onusesgname = "use_bell_commissioner"

		inst:AddComponent("myth_itemskin")
		inst.components.myth_itemskin.prefab = "bell_commissioner"
		inst.components.myth_itemskin.character = "yama_commissioners"
		inst.components.myth_itemskin:SetData{
			lotus = {
				swap = {build = "swap_bell_commissioner_lotus", folder = "swap_object"},
				icon = {atlas = "bell_commissioner_lotus.xml", image = "bell_commissioner_lotus"},
				anim = {bank = "bell_commissioner_lotus", build = "bell_commissioner_lotus", anim = "idle"},
			},
			default = {
				swap = {build = "swap_bell_commissioner", folder = "swap_object"},
				icon = {atlas = "bell_commissioner.xml", image = "bell_commissioner"},
				anim = {bank = "bell_commissioner", build = "bell_commissioner", anim = "idle"},
			}
		}
	end,
	fn_between = nil,
	fn_server = function(inst)
		inst.components.equippable.restrictedtag = "commissioner_white"
		inst.components.equippable.equipslot = EQUIPSLOTS.HANDS
		inst.components.equippable:SetOnEquip(function(inst, owner)
			if  inst.components.myth_itemskin.skin:value() == "lotus" then
				owner.AnimState:OverrideSymbol("lantern_overlay", "swap_bell_commissioner_lotus", "swap_object")
				owner.AnimState:HideSymbol("swap_object")

				owner.AnimState:Show("ARM_carry")
				owner.AnimState:Hide("ARM_normal")
				owner.AnimState:Show("LANTERN_OVERLAY")
			else
				inst.components.myth_itemskin:OverrideSymbol(owner, "swap_object")
				owner.AnimState:Show("ARM_carry")
				owner.AnimState:Hide("ARM_normal")
			end
		end)
		inst.components.equippable:SetOnUnequip(function(inst, owner)
			if  inst.components.myth_itemskin.skin:value() == "lotus" then
				owner.AnimState:ClearOverrideSymbol("lantern_overlay")
				owner.AnimState:Hide("LANTERN_OVERLAY")
				owner.AnimState:ShowSymbol("swap_object")
			end
			owner.AnimState:Hide("ARM_carry")
    		owner.AnimState:Show("ARM_normal")
		end)

		inst:AddComponent("myth_use_inventory")
		inst.components.myth_use_inventory.canuse = true
		inst.components.myth_use_inventory.canusescene  = false
		inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
			if doer ~= nil and doer.components.inventory ~= nil then
				local hat = doer.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
				if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
					local souls = hat.components.container:FindItem(function(item)
						return item:HasTag("soul_specter")
					end)
					if souls ~= nil then
						if souls.components.stackable ~= nil then --由于只消耗1，所以这里不需要提前判断总数量
							souls = souls.components.stackable:Get()
						end
						souls:Remove()

						--触发施法台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"BELL_COMMISSIONER", "SPELL"}))
						end

						--催眠并削弱附近生物
						local x, y, z = doer.Transform:GetWorldPosition()
						SetBuffAround(doer, x, y, z)

						--生成特效
						inst:DoTaskInTime(0.43, function()
							doer.SoundEmitter:PlaySound("dontstarve/common/together/celestial_orb/active", nil, 0.5)
							SetBellFx(inst, doer, x, y, z)
							inst:DoTaskInTime(0.8, function()
								SetBellFx(inst, doer, x, y, z)
							end)
						end)
					else
						--触发缺善魂的台词
						if doer.components.talker ~= nil then
							doer.components.talker:Say(GetString(doer, "DESCRIBE", {"BELL_COMMISSIONER", "NOSOUL"}))
						end
					end
				else
					--触发缺无常官帽的台词
					if doer.components.talker ~= nil then
						doer.components.talker:Say(GetString(doer, "DESCRIBE", {"BELL_COMMISSIONER", "NOHAT"}))
					end
				end

				return true
			end
			return false
		end)
	end,
})

--------------------------------------------------------------------------
--[[ 黑：镇魂令使用时的特效 ]]
--------------------------------------------------------------------------

local function InitGroundFx(inst)
	inst.AnimState:SetBank("bearger_ring_fx")
	inst.AnimState:SetBuild("bearger_ring_fx")
	inst.AnimState:PlayAnimation("idle")
	inst.AnimState:SetFinalOffset(3)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)
end

local function InitDotFx(inst)
	inst.AnimState:SetBank("alterguardian_meteor")
	inst.AnimState:SetBuild("token_commissioner_fx")
	inst.AnimState:PlayAnimation("meteor_charge", false)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	inst:DoTaskInTime(0.63, function()
		inst:Remove()
	end)
end

MakeFx({
	name = "token_commissioner_dot_fx",
	assets = {
		Asset("ANIM", "anim/alterguardian_meteor.zip"), --官方的
		Asset("ANIM", "anim/token_commissioner_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitDotFx(inst)
		inst.AnimState:SetMultColour(colorBlack.r, colorBlack.g, colorBlack.b, 0.6)
	end,
})

MakeFx({
	name = "token_commissioner_ground_fx",
	assets = {
		Asset("ANIM", "anim/bearger_ring_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitGroundFx(inst)
		inst.AnimState:SetScale(0.4, 0.4)
		inst.AnimState:SetMultColour(73/255, 7/255, 105/255, 0.8)
	end,
})

--------------------------------------------------------------------------
--[[ 白：招魂幡使用时的特效 ]]
--------------------------------------------------------------------------

MakeFx({
	name = "pennant_commissioner_dot_fx",
	assets = {
		Asset("ANIM", "anim/alterguardian_meteor.zip"), --官方的
		Asset("ANIM", "anim/token_commissioner_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitDotFx(inst)
		inst.AnimState:SetMultColour(colorWhite.r, colorWhite.g, colorWhite.b, 0.6)
	end,
})

MakeFx({
	name = "pennant_commissioner_ground_fx",
	assets = {
		Asset("ANIM", "anim/alterguardian_meteor.zip"), --官方的
		Asset("ANIM", "anim/pennant_commissioner_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		inst.AnimState:SetBank("alterguardian_meteor")
		inst.AnimState:SetBuild("pennant_commissioner_fx")
		inst.AnimState:PlayAnimation("meteorground_loop", false)
		inst.AnimState:SetFinalOffset(3)
		-- inst.AnimState:SetScale(0.6, 0.6)
		inst.AnimState:SetMultColour(195/255, 236/255, 198/255, 1)
		inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

		inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
		inst.AnimState:SetLayer(LAYER_BACKGROUND)
		inst.AnimState:SetSortOrder(3)

		inst:DoTaskInTime(0.8, function()
			inst:Remove()
		end)
	end,
})

--------------------------------------------------------------------------
--[[ 白：摄魂铃使用时的特效 ]]
--------------------------------------------------------------------------

MakeFx({
	name = "bell_commissioner_ground_fx",
	assets = {
		Asset("ANIM", "anim/bearger_ring_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitGroundFx(inst)
		inst.AnimState:SetScale(0.5, 0.5)
		inst.AnimState:SetMultColour(195/255, 236/255, 198/255, 0.8)
	end,
})

--------------------------------------------------------------------------
--[[ 善魂失控时的特效 ]]
--------------------------------------------------------------------------

MakeFx({
	name = "soul_specter_dot_fx",
	assets = {
		Asset("ANIM", "anim/alterguardian_meteor.zip"), --官方的
		Asset("ANIM", "anim/token_commissioner_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitDotFx(inst)
		inst.AnimState:SetScale(0.7, 0.7)
		inst.AnimState:SetMultColour(colorWhite.r, colorWhite.g, colorWhite.b, 0.6)
	end,
})

MakeFx({
	name = "soul_specter_ground_fx",
	assets = {
		Asset("ANIM", "anim/bearger_ring_fx.zip"),
	},
	prefabs = nil,
	fn_anim = function(inst)
		InitGroundFx(inst)
		inst.AnimState:SetScale(0.3, 0.3)
		inst.AnimState:SetMultColour(195/255, 236/255, 198/255, 0.8)
	end,
})

--------------------------------------------------------------------------
--[[ 善恶魂(物品栏) ]]
--------------------------------------------------------------------------

local canaccept = {
	myth_yama_statue1 = true,
	myth_yama_statue2 = true,
	myth_yama_statue3 = true,
}

local function needfn(inst,target,doer)
	return canaccept[target.prefab]
end

local function ongive(inst,target,doer)
	if target.components.trader ~= nil then
		local able, reason = target.components.trader:AbleToAccept(inst, doer)
		if able then
			target.components.trader:AcceptGift(doer, inst)
		end
	end
end

local function MakeSoul(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddSoundEmitter()
			inst.entity:AddLight()
			inst.entity:AddNetwork()

			MakeInventoryPhysics(inst)
    		RemovePhysicsColliders(inst)

			inst.Light:SetRadius(.6)
			inst.Light:SetFalloff(1)
			inst.Light:SetIntensity(.5)

			inst.AnimState:SetBank("wortox_soul_ball")
			inst.AnimState:SetBuild(data.name.."_ball")
			inst.AnimState:PlayAnimation("idle_loop", true)
			inst.AnimState:SetScale(0.8, 0.8)

			inst.myth_useitem_needfn = needfn
			inst.MYTH_USEITEM_TYPE  = "GIVE"
			inst.onuseitemsgname = "give"

			inst:AddTag("nosteal")
    		inst:AddTag("NOCLICK")
			inst:AddTag("soul_lost")
			inst:AddTag("nobundling") --该标签使其不能被放进打包纸

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				return inst
			end

			inst:AddComponent("inventoryitem")
			inst.components.inventoryitem.canbepickedup = false
			inst.components.inventoryitem.imagename = data.name
			inst.components.inventoryitem.atlasname = "images/inventoryimages/"..data.name..".xml"
			inst.components.inventoryitem:SetOnDroppedFn(function(inst)
				inst.Light:Enable(true)
			end)
    		inst.components.inventoryitem:SetOnPickupFn(function(inst)
				inst.Light:Enable(false)
			end)

			inst:AddComponent("myth_useitem")
			inst.components.myth_useitem.onuse = ongive

			inst:AddComponent("stackable")
			inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM
			-- inst.components.stackable.forcedropsingle = true

			inst:AddComponent("inspectable")

			inst:AddComponent("tradable")

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

-----

local function ToGround_soul(inst)
	inst.persists = false
    if inst._task == nil then
        inst._task = inst:DoTaskInTime(1.8+math.random()*0.4, function(inst)
			inst:ListenForEvent("animover", inst.Remove)
			inst.AnimState:PlayAnimation("idle_pst")
			inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/spawn", nil, .5)

			inst.OnSoulLostControl(inst)
		end)
    end
    if inst.AnimState:IsCurrentAnimation("idle_loop") then
        inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())
    end
end

local function InitSoul(inst, soultag)
    -- inst.components.inventoryitem:SetOnDroppedFn(function(inst) --为了落地时从一组分离成一堆单个的
	-- 	if inst.components.stackable ~= nil and inst.components.stackable:IsStack() then
	-- 		local x, y, z = inst.Transform:GetWorldPosition()
	-- 		local num = 10 - #TheSim:FindEntities(x, y, z, 4, {soultag})
	-- 		if num > 0 then
	-- 			for i = 1, math.min(num, inst.components.stackable:StackSize()) do
	-- 				local soul = inst.components.stackable:Get()
	-- 				soul.Physics:Teleport(x, y, z)
	-- 				soul.components.inventoryitem:OnDropped(true)
	-- 			end
	-- 		end
	-- 	end
	-- end)

	inst._task = nil
	inst:ListenForEvent("onputininventory", function(inst)
		inst.persists = true
		if inst._task ~= nil then
			inst._task:Cancel()
			inst._task = nil
		end

		--如果灵魂没有被放到无常官帽中（鼠标格除外），自动回到无常官帽中，失败就丢弃
		local owner = inst.components.inventoryitem.owner
		if owner ~= nil then
			if owner:HasTag("soullostbox") then --被放进了无常官帽或其他能装善恶魂的容器：bingo!
				return
			elseif owner.prefab == "construction_container" then
				return
			else
				local ownergrand = inst.components.inventoryitem:GetGrandOwner()
				if ownergrand ~= nil then
					inst:DoTaskInTime(0, function()
						local item = nil
						if ownergrand.components.inventory ~= nil then --可能是玩家
							if inst == ownergrand.components.inventory:GetActiveItem() then --在鼠标格子里：bingo!
								return
							end

							item = inst.components.inventoryitem:RemoveFromOwner(true) or inst

							--如果戴了无常官帽，则把善恶魂装进帽子里
							local hat = ownergrand.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
							if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
								if hat.components.container:GiveItem(item) then --返回false说明无常官帽装不下了
									return --经过container:GiveItem(v)后v会发生变化
								end
							end
						elseif ownergrand.components.container ~= nil then --可能是箱子
							item = inst.components.inventoryitem:RemoveFromOwner(true) or inst
						end

						--没戴无常官帽，或者帽子装不下了，丢弃多余的善恶魂
						if item ~= nil then
							--改至：Container:DropItem()
							--由于官帽的位置是0.0.0，所以这里设置为最有可能正确的位置
							item.Transform:SetPosition(ownergrand.Transform:GetWorldPosition())
							item.components.inventoryitem:OnDropped(true)
							item.prevcontainer = nil
							item.prevslot = nil
							owner:PushEvent("dropitem", {item = item})
						end
					end)
				end
			end
		end
	end)
	inst:ListenForEvent("ondropped", ToGround_soul)
	ToGround_soul(inst)

	inst:ListenForEvent("stacksizechange", function(inst, data)
		local owner = inst.components.inventoryitem ~= nil and inst.components.inventoryitem:GetGrandOwner() or nil
		if
			owner ~= nil and owner:HasTag("yama_commissioners") and
			owner.components.inventory ~= nil and
			data ~= nil and data.stacksize
		then
			local hat = owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
			if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
				--终于确定了这个灵魂是装在无常管帽里的，所以，需要监听它的数量变化
				if inst.components.inventoryitem:IsHeldBy(hat) and hat.components.container:IsHolding(inst) then
					local numberghast = nil
					local numberspecter = nil
					if soultag == "soul_ghast" then
						numberghast = data.stacksize
					else
						numberspecter = data.stacksize
					end
					UpdateSoulData(
						owner, hat, owner.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS),
						numberghast, numberspecter
					)
				end
			end
		end
	end)
end

-----

local function SoulToMind(soul, count)
	local x, y, z = soul.Transform:GetWorldPosition()
	for i, v in ipairs(AllPlayers) do
		if
			v:HasTag("yama_commissioners") and --只针对无常
			not (v.components.health:IsDead() or v:HasTag("playerghost")) and
			v.entity:IsVisible() and
			v.components.sanity ~= nil and
			v:GetDistanceSqToPoint(x, y, z) < 900 --30范围内
		then
			--undo：可以模仿植物人那种，逐渐下降的精神值，而不是这种直接减少的方式
			v.components.sanity:DoDelta(-5*count)
		end
	end
end

local function CanLoseControl(soul, soultype, count, x, y, z)
	local myth_yama_statues = TheSim:FindEntities(x, y, z, 40, nil, {"INLIMBO"}, {"myth_yama_statue","pass_commissioner","player"})
	for _,v in ipairs(myth_yama_statues) do
		if v:IsValid() then
			if v:HasTag("myth_yama_statue") then --阎罗像：符合距离
				if v.restrainrange and v:GetDistanceSqToPoint(x, y, z) <= v.restrainrange then
					return false
				end
			elseif v.prefab == "pass_commissioner_ylw" then --路引·阎罗王：符合距离
				if v:GetDistanceSqToPoint(x, y, z) <= 576 then --24*24
					return false
				end
			elseif v.prefab == "pass_commissioner_wz" then --路引·魏征：善魂、激活、符合距离
				if soultype == 0 and v.isactived and v:GetDistanceSqToPoint(x, y, z) <= 576 then --24*24
					local x2, y2, z2 = v.Transform:GetWorldPosition()
					local players = TheSim:FindEntities(x2, y2, z2, 24, {"player"}, {"INLIMBO", "playerghost"})
					for _,player in ipairs(players) do
						if player:IsValid() and player.components.health ~= nil and not player.components.health:IsDead() then
							player.components.hunger:DoDelta(5*count) --undo: 班花说要改成全局的参数
							player.components.health:DoDelta(2*count)
							player.components.sanity:DoDelta(5*count)
							AddEnemyDebuffFx(player, "soul_specter") --灵魂特效
						end
					end
					return false
				end
			elseif v.prefab == "pass_commissioner_zk" then --路引·钟馗：恶魂、激活、符合距离
				if soultype == 1 and v.isactived and v:GetDistanceSqToPoint(x, y, z) <= 576 then --24*24
					local x2, y2, z2 = v.Transform:GetWorldPosition()
					local players = TheSim:FindEntities(x2, y2, z2, 24, {"player"}, {"INLIMBO", "playerghost"})
					for _,player in ipairs(players) do
						if
							player:IsValid() and
							player.components.health ~= nil and not player.components.health:IsDead() and
							player.components.debuffable ~= nil and player.components.debuffable:IsEnabled()
						then
							player["time_m_ghostbuster"] = { replace_min = 4 }
							player.components.debuffable:AddDebuff("buff_m_ghostbuster", "buff_m_ghostbuster")
							AddEnemyDebuffFx(player, "soul_ghast") --灵魂特效
						end
					end
					return false
				end
			elseif v:HasTag("player") then --玩家：携带路引·阎罗王、符合距离
				if v.components.inventory ~= nil and v.components.inventory:HasItemWithTag("soulcontroller", 1) then
					if v:GetDistanceSqToPoint(x, y, z) <= 576 then --24*24
						return false
					end
				end
			end
		end
	end
	return true
end
local function OnSoulLostControl_black(soul)
	local x, y, z = soul.Transform:GetWorldPosition()
	local count = soul.components.stackable ~= nil and soul.components.stackable:StackSize() or 1

	--抑制暴动
	if not CanLoseControl(soul, 1, count, x, y, z) then
		return
	end

	--产生怨魂
	soul:DoTaskInTime(0.3, function(inst) --得趁灵魂动画播放完之前进行该操作
		for i = 1, count, 1 do
			local ghost = SpawnPrefab("ghost_irritated")
			if ghost ~= nil then
				ghost.Transform:SetPosition(x, y, z)
				AddEnemyDebuffFx(ghost, "soul_ghast")
			end
		end
	end)

	SoulToMind(soul, count)
end
local function OnSoulLostControl_white(soul)
	local x, y, z = soul.Transform:GetWorldPosition()
	local count = soul.components.stackable ~= nil and soul.components.stackable:StackSize() or 1

	SpawnPrefab("soul_specter_dot_fx").Transform:SetPosition(x, y, z)
	SpawnPrefab("soul_specter_ground_fx").Transform:SetPosition(x, y, z)

	--抑制暴动
	if not CanLoseControl(soul, 0, count, x, y, z) then
		return
	end

	--原地炸开并无视对象进行催眠
	soul:DoTaskInTime(0.3, function(inst) --得趁灵魂动画播放完之前进行该操作
		local ents = TheSim:FindEntities(
			x, y, z, 14,
			nil,
			{"ghost", "white_bone", "bone_pet", "shadowcreature", "shadow", "playerghost", "FX", "DECOR", "INLIMBO"},
			{"sleeper", "player"}
		)
		local sleeptime = 5*count
		for _, v in ipairs(ents) do
			if v:IsValid() then
				if	--没有处于无法睡眠的状态
					not (v.components.freezable ~= nil and v.components.freezable:IsFrozen()) and
					not (v.components.pinnable ~= nil and v.components.pinnable:IsStuck()) and
					not (v.components.fossilizable ~= nil and v.components.fossilizable:IsFossilized())
				then
					--让坐骑睡着
					local mount = v.components.rider ~= nil and v.components.rider:GetMount() or nil
					if mount ~= nil then
						mount:PushEvent("ridersleep", {sleepiness = 4, sleeptime = sleeptime})
					end

					if v:HasTag("player") then
						v:PushEvent("yawn", {grogginess = 4, knockoutduration = sleeptime})
					elseif v.components.sleeper ~= nil then
						v.components.sleeper:AddSleepiness(4, sleeptime)
					elseif v.components.grogginess ~= nil then
						v.components.grogginess:AddGrogginess(4, sleeptime)
					else
						v:PushEvent("knockedout")
					end

					AddEnemyDebuffFx(v, "soul_specter")
				end
			end
		end
	end)

	SoulToMind(soul, count)
end

MakeSoul({
	name = "soul_ghast",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_ghast_ball.zip"),
		Asset("ATLAS", "images/inventoryimages/soul_ghast.xml"),
		Asset("IMAGE", "images/inventoryimages/soul_ghast.tex"),
	},
	prefabs = {"ghost_irritated", "soul_ghast_do_fx",},
	fn_common = function(inst)
		inst:AddTag("soul_ghast")
		inst:AddTag("soul_yama")
		-- inst.tint = colorBlack

		inst.Light:SetColour(225/255, 85/255, 158/255)
		inst.Light:Enable(true)
	end,
	fn_server = function(inst)
		inst.OnSoulLostControl = OnSoulLostControl_black
		InitSoul(inst, "soul_ghast")
	end,
})

MakeSoul({
	name = "soul_specter",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_specter_ball.zip"),
		Asset("ATLAS", "images/inventoryimages/soul_specter.xml"),
		Asset("IMAGE", "images/inventoryimages/soul_specter.tex"),
	},
	prefabs = {"soul_specter_dot_fx", "soul_specter_ground_fx", "soul_specter_do_fx",},
	fn_common = function(inst)
		inst:AddTag("soul_specter")
		inst:AddTag("soul_yama")

		-- inst.tint = colorWhite

		inst.Light:SetColour(colorWhite.r, colorWhite.g, colorWhite.b)
		inst.Light:Enable(true)
	end,
	fn_server = function(inst)
		inst.OnSoulLostControl = OnSoulLostControl_white
		InitSoul(inst, "soul_specter")
	end,
})

--------------------------------------------------------------------------
--[[ 善恶魂(地面)：改至wortox_soul_spawn.lua ]]
--------------------------------------------------------------------------

local speedSoulSpawn = 5

local function CreateTail(soulitemname)
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    MakeInventoryPhysics(inst)
    inst.Physics:ClearCollisionMask()

    inst.AnimState:SetBank("wortox_soul_ball")
    inst.AnimState:SetBuild(soulitemname.."_ball")
    inst.AnimState:PlayAnimation("disappear")
    inst.AnimState:SetScale(0.8, 0.8)
    inst.AnimState:SetFinalOffset(3)

    inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function OnUpdateProjectileTail(inst)--, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    for tail, _ in pairs(inst._tails) do
        tail:ForceFacePoint(x, y, z)
    end
    if inst.entity:IsVisible() then
        local tail = CreateTail(inst.soulitemname)
        local rot = inst.Transform:GetRotation()
        tail.Transform:SetRotation(rot)
        rot = rot * DEGREES
        local offsangle = math.random() * 2 * PI
        local offsradius = (math.random() * .2 + .2) * 0.8
        local hoffset = math.cos(offsangle) * offsradius
        local voffset = math.sin(offsangle) * offsradius
        tail.Transform:SetPosition(x + math.sin(rot) * hoffset, y + voffset, z + math.cos(rot) * hoffset)
        tail.Physics:SetMotorVel(speedSoulSpawn * (.2 + math.random() * .3), 0, 0)
        inst._tails[tail] = true
		--临时修改
        inst:ListenForEvent("onremove", function(tail)
			if inst._tails and inst._tails[tail] then
				inst._tails[tail] = nil
			end
		end,tail)
        tail:ListenForEvent("onremove", function(inst)
            tail.Transform:SetRotation(tail.Transform:GetRotation() + math.random() * 30 - 15)
        end, inst)
    end
end

local function OnHasTailDirty(inst)
	if inst._hastail:value() then
		if inst._tails == nil then
			inst._tails = {}
			if inst.components.updatelooper == nil then
				inst:AddComponent("updatelooper")
			end
			inst.components.updatelooper:AddOnUpdateFn(OnUpdateProjectileTail)
		end
	else
		inst._tails = nil
		if inst.components.updatelooper ~= nil then
			inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateProjectileTail)
		end
	end
end

-----

local function GetSoulTag(soul)
	if soul.issoulinverted then --颠倒黑白
		if soul.soulitemname == "soul_ghast" then
			return "soul_specter"
		else
			return "soul_ghast"
		end
	else
		return soul.soulitemname
	end
end
local function CanOwnerGetSoul(player, soultag)
	if
		player:HasTag("yama_commissioners") and --只针对无常
		not (player.components.health:IsDead() or player:HasTag("playerghost")) and
		not (player.sg ~= nil and (player.sg:HasStateTag("nomorph") or player.sg:HasStateTag("silentmorph"))) and
		player.entity:IsVisible()
	then
		local hat = player.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
		if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
			return (GetSoulNumber(hat, soultag) < TUNING.STACK_SIZE_MEDITEM), hat
		end
	end
	return false, nil
end
local function WaitingForOwner(inst)
	--期间不断寻找无常
	if inst._seektask ~= nil then
		inst._seektask:Cancel()
	end
	inst._seektask = inst:DoPeriodicTask(.5, function(inst)
		local x, y, z = inst.Transform:GetWorldPosition()
		local absorber = nil --路引
		local rangesq_absorber = 576
		local doer = nil --普通玩家
		local rangesq_doer = 64
		local prdoer = nil --优先级玩家
		local rangesq_prdoer = 64
		local ssb = TheSim:FindEntities(
			x, y, z, 32,
			{"commissioner_book"},
			{"INLIMBO"}
		)
		local ents = TheSim:FindEntities(
			x, y, z, 24,
			nil,
			{"INLIMBO", "playerghost"},
			{"player", "soulabsorber", "soulinvertor"}
		)
		for _,v in ipairs(ssb) do
			if v:IsValid() and v.CanTakeSoul and v:CanTakeSoul() then
				absorber = v
			end
		end
		if not absorber then
			for _,v in ipairs(ents) do
				if v:IsValid() then
					if v:HasTag("soulinvertor") then --加入路引·陆之道的作用
						inst.issoulinverted = true
					elseif v.fn_cangetsoul ~= nil then
						if v.fn_cangetsoul(v, inst.soulitemname) then
							local distsq = v:GetDistanceSqToPoint(x, y, z)
							if absorber == nil or distsq < rangesq_absorber then
								rangesq_absorber = distsq
								absorber = v
							end
						end
					elseif absorber == nil then ------路引不存在时，才有玩家的份
						if v:HasTag(inst.prioritytag) then
							local distsq = v:GetDistanceSqToPoint(x, y, z)
							if
								distsq <= 64 and --玩家范围只有8
								(prdoer == nil or distsq < rangesq_prdoer)
								and CanOwnerGetSoul(v, GetSoulTag(inst)) --因为善恶魂prefab名和tag名一样，所以这样写
							then
								rangesq_prdoer = distsq
								prdoer = v
							end
					elseif prdoer == nil then ------优先玩家不存在时，才有其他玩家的份
								local distsq = v:GetDistanceSqToPoint(x, y, z)
							if
								distsq <= 64 and --玩家范围只有8
								(doer == nil or distsq < rangesq_doer)
								and CanOwnerGetSoul(v, GetSoulTag(inst)) --因为善恶魂prefab名和tag名一样，所以这样写
							then
								rangesq_doer = distsq
								doer = v
							end
						end
					end
				end
			end
		end

		if absorber ~= nil or prdoer ~= nil or doer ~= nil then
			inst.components.projectile:Throw(inst, absorber or prdoer or doer, inst)
		end
	end, 1)

	--找不到的话，一段时间后失控并消失
	if inst._timeouttask ~= nil then
		inst._timeouttask:Cancel()
	end
	inst._timeouttask = inst:DoTaskInTime(18+4*math.random(), function(inst)
		inst._timeouttask = nil
		if inst._seektask ~= nil then
			inst._seektask:Cancel()
			inst._seektask = nil
		end
		inst:ListenForEvent("animover", inst.Remove)
		inst.AnimState:PlayAnimation("idle_pst")
		inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/spawn", nil, .5)

		inst.OnSoulLostControl(inst)
	end)
end

-----

local function PushColour(inst, addval, multval, tint)
    if inst.components.highlight == nil then
		inst.AnimState:SetHighlightColour(tint.r * addval, tint.g * addval, tint.b * addval, 0)
		inst.AnimState:OverrideMultColour(multval, multval, multval, 1)
    else
        inst.AnimState:OverrideMultColour()
    end
end

local function PopColour(inst)
    if inst.components.highlight == nil then
        inst.AnimState:SetHighlightColour()
    end
    inst.AnimState:OverrideMultColour()
end

local function OnUpdateTargetTint(inst)--, dt)
    if inst._tinttarget:IsValid() then
        local curframe = inst.AnimState:GetCurrentAnimationTime() / FRAMES
		if inst.isInFx then
			if curframe < 10 then
				local k = curframe / 10
				k = k * k
				PushColour(inst._tinttarget, (1-k)*.7, k*.7+.3, inst.tint)
				return
			end
		elseif curframe < 15 then
            local k = curframe / 15
            k = k * k
            PushColour(inst._tinttarget, 1-k, k, inst.tint)
			return
        end

		inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateTargetTint)
		inst.OnRemoveEntity = nil
		PopColour(inst._tinttarget)
    else
        inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateTargetTint)
        inst.OnRemoveEntity = nil
    end
end

local function OnTargetDirty(inst)
    if inst._target:value() ~= nil and inst._tinttarget == nil then
        if inst.components.updatelooper == nil then
            inst:AddComponent("updatelooper")
        end
        inst.components.updatelooper:AddOnUpdateFn(OnUpdateTargetTint)
        inst._tinttarget = inst._target:value()
        inst.OnRemoveEntity = function(inst)
			if inst._tinttarget:IsValid() then
				PopColour(inst._tinttarget)
			end
		end
    end
end

local function Setup(inst, target)
    inst._target:set(target)
    if not TheNet:IsDedicated() then
        OnTargetDirty(inst)
    end
	if inst.isInFx then
		if target.SoundEmitter ~= nil then
			target.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/spawn", nil, .5)
		end
	else
		inst.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/spawn", nil, .5)
	end
end

-----

local function MakeSoulSpawn(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddSoundEmitter()
			inst.entity:AddLight()
			inst.entity:AddNetwork()

			MakeInventoryPhysics(inst)
    		RemovePhysicsColliders(inst)

			inst.Light:SetRadius(.6)
			inst.Light:SetFalloff(1)
			inst.Light:SetIntensity(.5)

			inst.AnimState:SetBank("wortox_soul_ball")
			inst.AnimState:SetBuild(data.name_item.."_ball")
			inst.AnimState:PlayAnimation("idle_pre")
			inst.AnimState:SetScale(0.8, 0.8)
			inst.AnimState:SetFinalOffset(3)

			inst:AddTag("soul_lost_spawn")
			--weapon (from weapon component) added to pristine state for optimization
			inst:AddTag("weapon")
			--projectile (from projectile component) added to pristine state for optimization
			inst:AddTag("projectile")

			inst.soulitemname = data.name_item

			inst._target = net_entity(inst.GUID, data.name_item.."._target", "targetdirty")
    		inst._hastail = net_bool(inst.GUID, data.name_item.."._hastail", "hastaildirty")

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				inst:ListenForEvent("targetdirty", OnTargetDirty)
        		inst:ListenForEvent("hastaildirty", OnHasTailDirty)

				return inst
			end

			inst.AnimState:PushAnimation("idle_loop", true)

			inst:AddComponent("weapon")
			inst.components.weapon:SetDamage(0)

			inst:AddComponent("projectile")
			inst.components.projectile:SetSpeed(speedSoulSpawn)
			inst.components.projectile:SetHitDist(.5)
			inst.components.projectile:SetOnThrownFn(function(inst, owner, target, attacker)
				if inst._timeouttask ~= nil then
					inst._timeouttask:Cancel()
				end
				inst._timeouttask = inst:DoTaskInTime(10, function(inst) --追随一段时间后，还没追到就停下来重新开始检测
					inst._timeouttask = nil
					inst.Physics:Stop()
					inst.components.projectile:Miss(inst.components.projectile.target)
				end)

				if inst._seektask ~= nil then
					inst._seektask:Cancel()
					inst._seektask = nil
				end

				inst.AnimState:Hide("blob")
				inst._hastail:set(true)
				if not TheNet:IsDedicated() then
					OnHasTailDirty(inst)
				end
			end)
			inst.components.projectile:SetOnHitFn(function(inst, attacker, target)
				if inst._timeouttask ~= nil then
					inst._timeouttask:Cancel()
				end

				if target then
					local soultag = GetSoulTag(inst)
					if target.fn_getsoul ~= nil and target.fn_cangetsoul ~= nil then --路引的吸收
						if target.fn_cangetsoul(target, inst.soulitemname) then
							target.fn_getsoul(target, inst.soulitemname, 1,inst)

							local x, y, z = inst.Transform:GetWorldPosition()
							local fx = SpawnPrefab(inst.soulitemname.."_in_fx")
							if fx ~= nil then
								fx.Transform:SetPosition(x, y, z)
								fx:Setup(target)
							end

							return
							inst:Remove()
						end
					else --玩家的吸收
						local isvalidtarget, hat = CanOwnerGetSoul(target, soultag)
						if isvalidtarget and hat ~= nil then
							local soulitem = SpawnPrefab(soultag)
							if soulitem ~= nil then
								if hat.components.container:GiveItem(soulitem, nil, target:GetPosition()) then
									local x, y, z = inst.Transform:GetWorldPosition()
									local fx = SpawnPrefab(soultag.."_in_fx")
									if fx ~= nil then
										fx.Transform:SetPosition(x, y, z)
										fx:Setup(target)
									end

									inst:Remove()
									return
								else
									soulitem:Remove() --装不下了就删除item
								end
							end
						end
					end
				end
				inst.components.projectile:Miss(target)
			end)
			inst.components.projectile:SetOnMissFn(function(inst, attacker, target)
				inst._hastail:set(false)
				if not TheNet:IsDedicated() then
					OnHasTailDirty(inst)
				end
				WaitingForOwner(inst)
			end)

			WaitingForOwner(inst)
			inst.persists = false
			inst.Setup = Setup

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

-----

MakeSoulSpawn({
	name = "soul_ghast_spawn",
	name_item = "soul_ghast",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_ghast_ball.zip"),
	},
	prefabs = {
		"ghost_irritated",
		"soul_ghast", "soul_ghast_in_fx", "soul_ghast_do_fx",
	},
	fn_common = function(inst)
		inst.tint = colorBlack
		inst.prioritytag = "commissioner_black"

		inst.Light:SetColour(225/255, 85/255, 158/255)
		inst.Light:Enable(true)
	end,
	fn_server = function(inst)
		inst.OnSoulLostControl = OnSoulLostControl_black
	end,
})

MakeSoulSpawn({
	name = "soul_specter_spawn",
	name_item = "soul_specter",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_specter_ball.zip"),
	},
	prefabs = {
		"soul_specter_dot_fx", "soul_specter_ground_fx",
		"soul_specter", "soul_specter_in_fx", "soul_specter_do_fx",
	},
	fn_common = function(inst)
		inst.tint = colorWhite
		inst.prioritytag = "commissioner_white"

		inst.Light:SetColour(colorWhite.r, colorWhite.g, colorWhite.b)
		inst.Light:Enable(true)
	end,
	fn_server = function(inst)
		inst.OnSoulLostControl = OnSoulLostControl_white
	end,
})

--------------------------------------------------------------------------
--[[ 善恶魂(地面)被吸收特效：改至wortox_soul_in_fx.lua ]]
--------------------------------------------------------------------------

local function MakeSoulInFx(data)
	table.insert(prefs, Prefab(
		data.name.."_in_fx",
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddNetwork()

			inst.AnimState:SetBank("wortox_soul_ball")
			inst.AnimState:SetBuild(data.name.."_ball")
			inst.AnimState:PlayAnimation("idle_pst")
			inst.AnimState:SetTime(6 * FRAMES)
			inst.AnimState:SetScale(0.8, 0.8)
			inst.AnimState:SetFinalOffset(3)

			inst:AddTag("FX")

			inst.isInFx = true
			inst._target = net_entity(inst.GUID, data.name.."_in_fx._target", "targetdirty")

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				inst:ListenForEvent("targetdirty", OnTargetDirty)

				return inst
			end

			inst:ListenForEvent("animover", inst.Remove)
			inst.persists = false
			inst.Setup = Setup

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

-----

MakeSoulInFx({
	name = "soul_ghast",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_ghast_ball.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.tint = colorBlack
	end,
	fn_server = nil,
})

MakeSoulInFx({
	name = "soul_specter",
	assets = {
		Asset("ANIM", "anim/wortox_soul_ball.zip"), --官方灵魂（item）动画
		Asset("ANIM", "anim/soul_specter_ball.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.tint = colorWhite
	end,
	fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 善恶魂发挥作用的特效：改至wortox_soul_heal_fx.lua ]]
--------------------------------------------------------------------------

local function OnUpdateTargetTint_do(inst)--, dt)
    if inst._tinttarget:IsValid() then
        local curframe = inst.AnimState:GetCurrentAnimationTime() / FRAMES
        if curframe < 10 then
            if inst._tinttarget.components.colouradder ~= nil then
				local k = curframe / 10 * .5
				local TINT = inst.tint
                inst._tinttarget.components.colouradder:PushColour(inst, TINT.r * k, TINT.g * k, TINT.b * k, 0)
            end
        elseif curframe < 40 then
            if inst._tinttarget.components.colouradder ~= nil then
				local k = (curframe - 10) / 30
            	k = (1 - k * k) * .5
				local TINT = inst.tint
                inst._tinttarget.components.colouradder:PushColour(inst, TINT.r * k, TINT.g * k, TINT.b * k, 0)
            end
        else
            inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateTargetTint_do)
            if inst._tinttarget.components.colouradder ~= nil then
                inst._tinttarget.components.colouradder:PopColour(inst)
            end
        end
    else
        inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateTargetTint_do)
    end
end

-----

local function MakeSoulDoFx(data)
	table.insert(prefs, Prefab(
		data.name.."_do_fx",
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddNetwork()

			inst.AnimState:SetBank("wortox_soul_heal_fx")
			inst.AnimState:SetBuild(data.name.."_do_fx")
			inst.AnimState:PlayAnimation("heal")
			inst.AnimState:SetFinalOffset(3)
			inst.AnimState:SetScale(1.5, 1.5)
			inst.AnimState:SetDeltaTimeMultiplier(2)

			inst:AddTag("FX")

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				return inst
			end

			if math.random() < .5 then
				inst.AnimState:SetScale(-1.5, 1.5)
			end

			inst:ListenForEvent("animover", inst.Remove)
			inst.persists = false
			inst.Setup = function(inst, target)
				if inst.components.updatelooper == nil then
					inst:AddComponent("updatelooper")
					inst.components.updatelooper:AddOnUpdateFn(OnUpdateTargetTint_do)
					inst._tinttarget = target
				end
				-- if target.SoundEmitter ~= nil then
				-- 	target.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/heal")
				-- end
			end

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

-----

MakeSoulDoFx({
	name = "soul_ghast",
	assets = {
		Asset("ANIM", "anim/wortox_soul_heal_fx.zip"), --官方灵魂（加血）动画
		Asset("ANIM", "anim/soul_ghast_do_fx.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.tint = colorBlack
	end,
	fn_server = nil,
})

MakeSoulDoFx({
	name = "soul_specter",
	assets = {
		Asset("ANIM", "anim/wortox_soul_heal_fx.zip"), --官方灵魂（加血）动画
		Asset("ANIM", "anim/soul_specter_do_fx.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.tint = colorWhite
	end,
	fn_server = nil,
})

--------------------------------------------------------------------------
--[[ 怨魂、恩魂 ]]
--------------------------------------------------------------------------

local function MakeGhost(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddAnimState()
			inst.entity:AddSoundEmitter()
			inst.entity:AddLight()
			inst.entity:AddNetwork()

			MakeGhostPhysics(inst, .5, .5)

			inst.Light:SetIntensity(.6)
			inst.Light:SetRadius(.5)
			inst.Light:SetFalloff(.6)

			inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
			inst.AnimState:SetLightOverride(TUNING.GHOST_LIGHT_OVERRIDE)

			inst.AnimState:SetBank("ghost")
			inst.AnimState:SetBuild(data.name.."_build")
			inst.AnimState:PlayAnimation("idle", true)

			inst:AddTag("ghost")
			inst:AddTag("flying")
			inst:AddTag("noauradamage")

			--trader (from trader component) added to pristine state for optimization
			inst:AddTag("trader")

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				return inst
			end

			inst.cursetarget = nil
			inst.taskremove = nil

			inst:SetBrain(data.brain)

			inst:AddComponent("locomotor")
			inst.components.locomotor.directdrive = true

			inst:SetStateGraph(data.sg)

			inst:AddComponent("sanityaura")
			inst.components.sanityaura.aurafn = function(inst, observer)
				return observer:HasTag("yama_commissioners") and 0 or -TUNING.SANITYAURA_MED
			end

			inst:AddComponent("inspectable")
			inst:AddComponent("lootdropper")
			inst:AddComponent("health")

			inst:AddComponent("combat")
			inst.components.combat:SetDefaultDamage(20)
			inst.components.combat.playerdamagepercent = TUNING.GHOST_DMG_PLAYER_PERCENT

			inst:AddComponent("aura") --这里面的排除标签使得鬼魂之间无法相互攻击
			inst.components.aura.radius = 2.5 --普通鬼魂默认1.5、艾比盖尔4
			inst.components.aura.tickperiod = TUNING.GHOST_DMG_PERIOD

			inst:AddComponent("trader")
    		inst.components.trader:SetAbleToAcceptTest(function(inst, item)
				return false, item.prefab == "reviver" and "GHOSTHEART" or nil
			end)

			inst:ListenForEvent("death", function(inst)
				inst.components.aura:Enable(false)
			end)
    		inst:ListenForEvent("attacked", function(inst, data)
				if data.attacker == nil then
					inst.components.combat:SetTarget(nil)
				elseif not data.attacker:HasTag("noauradamage") then
					inst.components.combat:SetTarget(data.attacker)
				end
			end)

			inst.taskremove = inst:DoTaskInTime(60+math.random()*3, function(inst)
				inst.sg:GoToState("dissipate")
			end)

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

-----

local brain_irritated = require "brains/ghost_irritatedbrain"

MakeGhost({ --怨魂
	name = "ghost_irritated",
	assets = {
		Asset("ANIM", "anim/player_ghost_withhat.zip"), --官方鬼魂动画
		Asset("ANIM", "anim/ghost_irritated_build.zip"),
		Asset("SOUND", "sound/ghost.fsb"),
	},
	prefabs = nil,
	brain = brain_irritated,
	sg = "SGghost_myth",
	fn_common = function(inst)
		inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl_LP", "howl", 0.5)

		inst.color_light_normal = colorBlack
		inst.color_mult_angry = { r = 208/255, g = 93/255, b = 170/255 }
		inst.Light:SetColour(colorBlack.r, colorBlack.g, colorBlack.b)
		inst.Light:Enable(true)

		inst:AddTag("monster")
		inst:AddTag("hostile")
		inst:AddTag("soulless")
	end,
	fn_server = function(inst)
		inst.components.locomotor.walkspeed = 2 --普通鬼魂2、阿比盖尔5
		inst.components.locomotor.runspeed = 3

		inst.components.health:SetMaxHealth(150) --普通鬼魂200

		inst.components.combat:SetKeepTargetFunction(function(inst, target)
			if target ~= nil and inst:GetDistanceSqToInst(target) < TUNING.GHOST_FOLLOW_DSQ then
				return true
			end

			--如果当前的仇恨者距离太远就放弃追踪
			inst.cursetarget = nil
			return false
		end)

		inst.components.aura.auratestfn = function(inst, target)
			if
				target:HasTag("player") or --专打玩家
				inst.components.combat:TargetIs(target) or --自己仇恨的
				(target.components.combat.target ~= nil and target.components.combat:TargetIs(inst)) --仇恨自己的
			then
				return true
			end

			return false
		end
	end,
})

local brain_clement = require "brains/ghost_clementbrain"

MakeGhost({ --恩魂
	name = "ghost_clement",
	assets = {
		Asset("ANIM", "anim/player_ghost_withhat.zip"), --官方鬼魂动画
		Asset("ANIM", "anim/ghost_clement_build.zip"),
		Asset("SOUND", "sound/ghost.fsb"),
	},
	prefabs = nil,
	brain = brain_clement,
	sg = "SGghost_myth",
	fn_common = function(inst)
		inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_attack_LP", "howl", 0.3)

		inst.color_light_normal = colorWhite
		inst.color_mult_angry = { r = 240/255, g = 124/255, b = 201/255 }
		inst.Light:SetColour(colorWhite.r, colorWhite.g, colorWhite.b)
		inst.Light:Enable(true)

		inst:AddTag("girl")
		inst:AddTag("NOBLOCK")  --不妨碍玩家摆放建筑物
		inst:AddTag("soulless")
	end,
	fn_server = function(inst)
		inst:AddComponent("follower") --为了能让它更好的跟随召唤者，加该组件优化一下

		inst.components.locomotor.walkspeed = 2 --普通鬼魂2、阿比盖尔5
		inst.components.locomotor.runspeed = 4 --这个可以比怨魂快，因为怨魂移速太快会很恶心

		inst.components.health:SetMaxHealth(150) --普通鬼魂200

		inst.components.combat:SetKeepTargetFunction(function(inst, target)
			--仇恨者还在范围内
			if
				target ~= nil and
				not target:HasTag("noauradamage") and
				inst:GetDistanceSqToInst(target) < TUNING.GHOST_FOLLOW_DSQ
			then
				if inst.cursetarget ~= nil and inst.cursetarget:IsValid() then
					--存在召唤者的话，离召唤者太远就需要放弃仇恨者去追随召唤者
					return inst.cursetarget ~= target and inst:GetDistanceSqToInst(inst.cursetarget) < 400
				end
				return true
			end
			return false
		end)

		inst.components.aura.auratestfn = function(inst, target)
			--绝不攻击召唤者
			if inst.cursetarget ~= nil and target == inst.cursetarget then
				return false
			end

			if
				inst.components.combat:TargetIs(target) or --自己仇恨的
				(
					target.components.combat.target ~= nil and
					(
						target.components.combat:TargetIs(inst) or --仇恨自己的
						(inst.cursetarget ~= nil and target.components.combat:TargetIs(inst.cursetarget)) --仇恨召唤者的
					)
				)
			then
				return true
			end

			return false --不主动攻击任何对象
		end

		inst.OnLinkPlayer = function(inst, player)
			inst.cursetarget = player
			--主人攻击别人，随从就能攻击这个人，该功能在leader组件里，如果想要加假攻击就可以从这里入手
			if player.components.leader ~= nil and not player.components.leader:IsFollower(inst) then
				player:PushEvent("makefriend")
                player.components.leader:AddFollower(inst)
			end
		end
	end,
})

--------------------
--------------------

return unpack(prefs)
