local function OnTimer(inst, data)
    if data.name == "regenover" then
        inst.components.debuff:Stop()
    end	
end

---speed
local function setnew(inst)
    local current = inst.components.yama_transform.current
    if current == "white" then
        inst.components.combat.externaldamagemultipliers:RemoveModifier("whip_commissioner_speedbuff")
        inst.components.locomotor:SetExternalSpeedMultiplier(inst, "whip_commissioner_speedbuff", 1.25)
        if inst.whip_commissioner_speedbuff_fx then
            inst.whip_commissioner_speedbuff_fx.Light:SetColour(40 / 255, 264 / 255, 218 / 255)
        end
    else
        inst.components.combat.externaldamagemultipliers:SetModifier("whip_commissioner_speedbuff", 1.25)
        inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "whip_commissioner_speedbuff")
        if inst.whip_commissioner_speedbuff_fx then
            inst.whip_commissioner_speedbuff_fx.Light:SetColour(210 / 255, 59 / 255, 221 / 255)
        end
    end
    if inst.Spawn_Circle_Fires then
        inst:Spawn_Circle_Fires()
    end
end

local function OnSpeed(inst, target)
    if target.components.locomotor ~= nil and target.components.health ~= nil and
        not target.components.health:IsDead() and
        not target:HasTag("playerghost") and target.components.yama_transform then

        inst:ListenForEvent("yama_transform", setnew, target)
        if target.whip_commissioner_speedbuff_fx == nil then
            target.whip_commissioner_speedbuff_fx = SpawnPrefab("flyerfx_yama_fx")
            target.whip_commissioner_speedbuff_fx.entity:SetParent(target.entity)
        end
        setnew(target)
    else
        inst.components.debuff:Stop()
    end
end

local function OnSpeedStart(inst, target) 
    inst.entity:SetParent(target.entity)
    inst.Transform:SetPosition(0, 0, 0) 
    OnSpeed(inst, target)
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
end

local function OnSpeedExtended(inst, target)
    inst.components.timer:StopTimer("regenover")
    inst.components.timer:StartTimer("regenover", 20)
end
local function OnSpeedDeath(inst,target)
	if target.components.locomotor ~= nil then
		target.components.locomotor:RemoveExternalSpeedMultiplier(target, "whip_commissioner_speedbuff")
	end
	if target.components.combat ~= nil then
		target.components.combat.externaldamagemultipliers:RemoveModifier("whip_commissioner_speedbuff")
	end
    if target.KillCircleFires then
        target:KillCircleFires()
    end
    if target.whip_commissioner_speedbuff_fx ~= nil then
        target.whip_commissioner_speedbuff_fx:Remove()
        target.whip_commissioner_speedbuff_fx = nil
    end

    inst:RemoveEventCallback("yama_transform", setnew, target)
	inst:Remove()
end
--=====================================
local function makebuffs(name,data)
	local function fn()
		local inst = CreateEntity()
        inst.entity:AddTransform()
        inst.entity:AddNetwork()
		inst.entity:Hide()
		inst.persists = false

        inst:AddTag("CLASSIFIED")
        inst:AddTag("NOCLICK")
        inst:AddTag("NOBLOCK")

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end
        
		inst:AddComponent("debuff")
		inst.components.debuff:SetAttachedFn(data.start)
		inst.components.debuff:SetDetachedFn(data.death)
		inst.components.debuff:SetExtendedFn(data.extended)
		inst.components.debuff.keepondespawn = true

        if  data.time ~= nil then
		    inst:AddComponent("timer")
		    inst.components.timer:StartTimer("regenover", data.time)
            inst:ListenForEvent("timerdone", OnTimer)
        end

		return inst
	end
	return Prefab(name, fn)
end

return 
        makebuffs("whip_commissioner_buff",{start = OnSpeedStart , extended =OnSpeedExtended ,death = OnSpeedDeath ,time = 20 })