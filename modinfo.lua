local L = locale ~= "zh" and locale ~= "zhr" --true-英文; false-中文

name = L and "[DST] Myth Characters" or "[DST] 神话人物测试版"
author = ""
version = "6.6.6"
description = "trying to debug"
forumthread = ""

api_version = 6
api_version_dst = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false

all_clients_require_mod = true
-- client_only_mod = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = -5 --后加载
server_filter_tags = L and { "MYTH CHARACTERS" } or { "神话人物测试版" }

configuration_options =
{
    --[[L and {
        name = 'Skillbadge',
        label = 'Skill badge',
        hover = 'Show skill badge',
        options =
        {
            {description = 'Yes', data = true},
            {description = 'No', data = false},
        },
        default = true
    } or {
        name = 'Skillbadge',
        label = '角色技能按钮',
        hover = '是否显示角色技能的按钮',
        options =
        {
            {description = '是', data = true},
            {description = '否', data = false},
        },
        default = true
    },]]
}

--兼容动态加载mod
StaticAssetsReg = {'monkey_king', 'myth_skins_prefab'}
