
local function onisastral(self,isastral)
	if self.inst._is_player_astral then
		self.inst._is_player_astral:set(isastral)
	end
end

local function onplayer(self,player)
	if self.inst._is_player_astral_player then
		self.inst._is_player_astral_player:set(player)
	end
	if player then 
		self.inst:AddTag("has_astral_player")
	else
		self.inst:RemoveTag("has_astral_player")
	end
end

local  player_astral = Class(function(self, inst)
	self.inst = inst
	self.isastral = false
	self.player = nil
	self.onremove = function(player)
		if  self.player ~= nil  then
			self:StopPlayer()
		end
	end
	self.body_pos = {}
	self.onstop = function()
		if  self.player ~= nil and self.inst.components.locomotor:WantsToMoveForward() then
			self:StopPlayer()
		end
	end
	self.dosetpos = function()
		if self.player and self.player:IsValid() then
			self.inst.Transform:SetPosition(self.player.Transform:GetWorldPosition())
		end
	end
end,
nil,
{
	isastral = onisastral,
	player = onplayer,
}
)

function player_astral:IsAstral()
    return self.isastral
end

local function GetHopDistance(inst, speed_mult)
	return speed_mult < 0.8 and TUNING.WILSON_HOP_DISTANCE_SHORT
			or speed_mult >= 1.2 and TUNING.WILSON_HOP_DISTANCE_FAR
			or TUNING.WILSON_HOP_DISTANCE
end

local banactions ={
	--[ACTIONS.PICKUP]= true, --拾取
	--[ACTIONS.PICK]= true, --采集
	[ACTIONS.SLEEPIN]= true, --睡觉
	--[ACTIONS.MYTH_ENTER_HOUSE]= true,--小房子
	--[ACTIONS.MOUNT]= true,--骑牛
	[ACTIONS.MIGRATE]= true,
	[ACTIONS.ATTACK]= true,
	[ACTIONS.EAT]= true,
	[ACTIONS.FEED]= true,
	[ACTIONS.HAUNT]= true,
	--[ACTIONS.JUMPIN]= true,		
}


local function FlyActionFilter(inst, action)
    return not banactions[action]
end

local function changephysics(inst, data)
	if inst.Physics then
		if inst.Physics:GetCollisionMask() ~= 32 then
			RemovePhysicsColliders(inst)
		end
	end
end

--ThePlayer.components.sanity:AddSanityPenalty("myth_flyer.sanity", 0.25)
local function setastraltredirect(inst,amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
	if not (afflicter and (afflicter:HasTag("nightmare") or afflicter:HasTag("shadow") or afflicter:HasTag("shadowchesspiece"))) then
		return true
	end
	if inst.components.sanity then
		inst.components.sanity:DoDelta(amount)
	end
	return true
end

function player_astral:SetAstral(val)
	local inst = self.inst
    if val then
		if inst.components.playeractionpicker ~= nil then
			inst.components.playeractionpicker:PushActionFilter(FlyActionFilter, 557)
		end
		if inst.Physics then
			RemovePhysicsColliders(inst)
		end
		inst.DynamicShadow:Enable(false)
        inst.AnimState:SetMultColour(0.3, 0.3, 0.3, 0.3)

		if not self.myth_astralsanity then
			self.myth_astralsanity = SpawnPrefab("myth_astralsanity")
			self.myth_astralsanity.entity:SetParent(inst.entity)
		end
		if inst.components.inventory then
			for k, v in pairs(inst.components.inventory.equipslots) do
            	if  k  ~= "hands" then
                	inst.components.inventory:DropItem(v, true, true)
           		end
       	 	end
		end

		if inst.components.grue ~= nil then
			inst.components.grue:AddImmunity("player_astral")
		end	

		if inst.components.health ~= nil then
			inst.components.health.externalfiredamagemultipliers:SetModifier("player_astral", 1 - TUNING.ARMORDRAGONFLY_FIRE_RESIST)
			inst.components.health.setastraltredirect = setastraltredirect
		end

		if inst.components.temperature ~= nil then
			inst.components.temperature.oldastraltemp = inst.components.temperature.current 
			inst.components.temperature.setastraltemp = 34
			inst.components.temperature:SetTemperature(34)
		end
		--ThePlayer.components.temperature:SetModifier("player_astral", 34)

		if inst.components.hunger then
			inst.components.hunger:Pause() 
		end
		if inst.components.locomotor then
			inst.components.locomotor:SetSlowMultiplier(0.6)
			inst.components.locomotor.pathcaps = { player = true, ignorecreep = true ,allowocean = true}
			inst.components.locomotor.fasteronroad = false
			inst.components.locomotor:SetTriggersCreep(false)
			inst.components.locomotor:SetAllowPlatformHopping(false)
		end

		if inst.components.drownable then
			inst.components.drownable.enabled = false
		end
		if inst.components.combat then
			inst.components.combat.externaldamagemultipliers:SetModifier("player_astral", 0)
		end

		inst:ListenForEvent("newstate", changephysics)
		--监听脑残变化结束 此状态
	else
		self.astral_body = nil

		if self.myth_astralsanity then
			self.myth_astralsanity:Remove()
			self.myth_astralsanity = nil
		end

		if inst.components.playeractionpicker ~= nil then
			inst.components.playeractionpicker:PopActionFilter(FlyActionFilter)
		end
		if inst.Physics then
			ChangeToCharacterPhysics(inst)
		end
		inst.DynamicShadow:Enable(true)

		inst.AnimState:SetMultColour(1, 1, 1, 1)

		if inst.components.bloomer then
			inst.components.bloomer:PopBloom("player_astral")
		end

		if inst.components.hunger then
			inst.components.hunger:Resume()
		end

		if inst.components.temperature ~= nil then
			inst.components.temperature.setastraltemp = nil
			if inst.components.temperature.oldastraltemp ~= nil then
				inst.components.temperature:SetTemperature(inst.components.temperature.oldastraltemp)
				inst.components.temperature.oldastraltemp = nil
			end
		end

		if inst.components.grue ~= nil then
			inst.components.grue:RemoveImmunity("player_astral")
		end	
		if inst.components.health ~= nil then
			inst.components.health.externalfiredamagemultipliers:RemoveModifier("player_astral")
			inst.components.health.setastraltredirect = nil
		end

		if inst.components.locomotor then
			inst.components.locomotor:SetSlowMultiplier(0.6)
			inst.components.locomotor.pathcaps = { player = true, ignorecreep = true }
			inst.components.locomotor.fasteronroad = true
			inst.components.locomotor:SetTriggersCreep(not inst:HasTag("spiderwhisperer"))
			inst.components.locomotor:SetAllowPlatformHopping(true)
			inst.components.locomotor.hop_distance_fn = GetHopDistance
		end
		if inst.components.drownable then
			inst.components.drownable.enabled = true
		end
		if inst.components.combat then
			inst.components.combat.externaldamagemultipliers:RemoveModifier("player_astral")
		end
		if self:HasPlayer() then
			self:StopPlayer()
		end
		inst:RemoveEventCallback("newstate", changephysics)
	end

	self.isastral = val
end

function player_astral:HasPlayer()
	return self.player ~= nil
end

function player_astral:SetPlayer(player)
	self.player = player
	player:SpawnChild("white_bone_raisefx")
	player.AnimState:SetHaunted(true)
	self.inst:Hide()
	self.inst.Physics:SetActive(false)
	self.pos_task = self.inst:DoPeriodicTask(0,self.dosetpos)
	self.inst:ListenForEvent("onremove",self.onremove,self.player)
	self.inst:ListenForEvent("death",self.onremove,self.player)
	self.inst:ListenForEvent("locomote",self.onstop)
	self.inst:ListenForEvent("onremove",self.onremove)

	player._is_be_ghosted:set(true)
	player._is_be_ghosted_players[self.inst] = true
	if self.player.components.locomotor then
		self.player.components.locomotor:SetExternalSpeedMultiplier(self.player, "_is_be_ghosted", 1.1)
	end
	if not self.player.myth_ghostedsanity then
		self.player.myth_ghostedsanity = SpawnPrefab("myth_ghostedsanity")
		self.player.myth_ghostedsanity.entity:SetParent(self.player.entity)
		self.player.myth_ghostedsanity.player = self.player
	end
end

function player_astral:StopPlayer()
	if self.player then
		if self.pos_task then
			self.pos_task:Cancel()
			self.pos_task = nil
		end
		self.player.AnimState:SetHaunted(false)
		self.inst:RemoveEventCallback("onremove",self.onremove,self.player)
		self.inst:RemoveEventCallback("death",self.onremove,self.player)
		self.inst:RemoveEventCallback("locomote",self.onstop)
		self.inst:RemoveEventCallback("onremove",self.onremove)
		self.player._is_be_ghosted_players[self.inst] = nil
		if next(self.player._is_be_ghosted_players) == nil then
			self.player._is_be_ghosted:set(false)
			if self.player.components.locomotor then
				self.player.components.locomotor:RemoveExternalSpeedMultiplier(self.player, "_is_be_ghosted")
			end
			--self.player.AnimState:SetHaunted(false)
			if self.player.myth_ghostedsanity then
				self.player.myth_ghostedsanity:Remove()
				self.player.myth_ghostedsanity = nil
			end
		end
		self.player = nil
		self.inst:Show()
		self.inst.Physics:SetActive(true)
	end
end

function player_astral:DespawnBody()
	if self.astral_body and self.astral_body:IsValid() then
		ErodeAway(self.astral_body)
	end
end

player_astral.OnRemoveEntity = player_astral.DespawnBody


function player_astral:OnSave()
	if self.astral_body and self.astral_body:IsValid() then
		local x , y ,z = self.astral_body.Transform:GetWorldPosition()
		self.body_pos[TheShard:GetShardId()] = {x , y ,z}
	end
    return { body_pos = self.body_pos}
end

function player_astral:OnLoad(data)
    if data and data.body_pos then
		if data.body_pos[TheShard:GetShardId()] ~= nil then
			self.inst:DoTaskInTime(0,function()
				self.inst.Transform:SetPosition(unpack(data.body_pos[TheShard:GetShardId()]))
				data.body_pos[TheShard:GetShardId()] = nil
			end)
		end
    end
end

return player_astral


