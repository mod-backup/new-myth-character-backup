local assets = {
    Asset("ANIM", "anim/spider_cocoon.zip"),
    Asset("ANIM", "anim/madameweb_pitfall_cocoon.zip"),
}

local function OnAttacked(inst, data)
    if not IsEntityDead(inst, true) then
        inst.AnimState:PlayAnimation("cocoon_" .. inst.cocoon_style .. "_hit")
        inst.AnimState:PushAnimation("cocoon_" .. inst.cocoon_style)
    end
end

local function CalcDamage(inst, attacker, damage, weapon, stimuli)
    if inst ~= attacker and not IsEntityDead(inst, true) then
        inst.total_outside_damage = inst.total_outside_damage + damage
    end
end

local function SelfAttack(inst)
    inst.components.combat:GetAttacked(inst, inst.self_attack_damage)
end

local PushAnimations = {
    small = {"grow_sac_to_small"},
    medium = {"grow_sac_to_small","grow_small_to_medium"},
    large = {"grow_sac_to_small","grow_small_to_medium","grow_medium_to_large"},
}

local function Catch(inst, guy)
    guy:PushEvent("ontrapped", { trapper = inst })

    inst.stored_guy_data = guy:GetSaveRecord()

    local period = guy.components.combat and guy.components.combat.min_attack_period or 6
    local damage = guy.components.combat and guy.components.combat.defaultdamage or 10

    inst.self_attack_period = period
    inst.self_attack_damage = damage
    inst.self_attack_task = inst:DoPeriodicTask(period, SelfAttack)

    -- 用来适配生物大小
    local scale = 1
    local cocoon_style = (guy:HasTag("largecreature") and "medium")
    or "small"
    local x,y,z = guy.Transform:GetScale()
    if cocoon_style == "medium" then
        scale = 1.2
    elseif guy:HasTag("smallcreature") then
        scale = 0.8
    end
    inst.cocoon_style = cocoon_style
    inst.Transform:SetScale(x*scale,y*scale,z*scale)
    if PushAnimations[inst.cocoon_style] ~= nil then
        for _,v in ipairs(PushAnimations[inst.cocoon_style]) do
            inst.AnimState:PushAnimation(v)
        end
    end

    inst.AnimState:PushAnimation("cocoon_" .. inst.cocoon_style)

    -- 拜拜
    guy.persists = false
    guy:RemoveFromScene()
    guy:Remove()
end

local function Release(inst)
    local record = inst.stored_guy_data
    local guy
    if record then
        inst.stored_guy_data = nil

        -- 扣除外部攻击掉的血
        local hp = record.data and record.data.health and record.data.health.health
        if hp then
            record.data.health.health = hp - (inst.total_outside_damage or 0)
        end

        guy = SpawnSaveRecord(record)
        if guy then
            guy.Transform:SetPosition(inst.Transform:GetWorldPosition())

            if inst.components.burnable:IsBurning() then
                guy.components.burnable:Ignite()
            end
            Myth_DoPoison(guy)
        end
    end

    if not IsEntityDead(inst, true) then
        inst.components.health:SetPercent(0)
    end

    return guy
end

local function OnKilled(inst)
    if inst.self_attack_task then
        inst.self_attack_task:Cancel()
        inst.self_attack_task = nil
    end

    inst.AnimState:PlayAnimation("cocoon_dead")
    inst:Release()
    RemovePhysicsColliders(inst)

    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spiderLair_destroy")
end

local function OnSave(inst, data)
    data.stored_guy_data = inst.stored_guy_data
    data.total_outside_damage = inst.total_outside_damage
    data.self_attack_period = inst.self_attack_period
    data.self_attack_damage = inst.self_attack_damage
end

local function OnLoad(inst, data)
    inst.stored_guy_data = data.stored_guy_data
    inst.total_outside_damage = 0 --data.total_outside_damage
    inst.self_attack_period = data.self_attack_period
    inst.self_attack_damage = data.self_attack_damage
    --inst.self_attack_task = inst:DoPeriodicTask(period, SelfAttack)
    inst:Release()
end

local function OnEntitySleep(inst)
    inst.total_outside_damage = 0
    inst:Release()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 0.5)

    inst.AnimState:SetBank("spider_cocoon")
    inst.AnimState:SetBuild("madameweb_pitfall_cocoon")
    inst.AnimState:PlayAnimation("cocoon_small")
    inst.AnimState:HideSymbol("bedazzled_flare")

    inst:AddTag("soulless")

    --inst.Transform:SetScale(0.7, 0.7, 0.7)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.cocoon_style = "small"

    MakeMediumBurnable(inst)
    inst.components.burnable:SetBurnTime(5) -- 太耐烧，可能都来不及烧掉就被踢烂了

    MakeMediumPropagator(inst)

    inst:AddComponent("inspectable")

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_TINY)

    inst.total_outside_damage = 0
    inst.stored_guy_data = nil

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(999999)
    inst.components.combat.redirectdamagefn = CalcDamage

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(200)
    inst:ListenForEvent("death", OnKilled)

    inst:ListenForEvent("attacked", OnAttacked)

    inst.Catch = Catch
    inst.Release = Release
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnEntitySleep = Release

    return inst
end

return Prefab("madameweb_pitfall_cocoon", fn, assets)