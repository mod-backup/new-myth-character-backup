local function OnConstructed(inst)
    if inst.doer == nil or inst.node1 == nil or inst.node2 == nil or not inst.node1:IsValid() or not inst.node2:IsValid() then
        return
    end

    for i, v in ipairs(CONSTRUCTION_PLANS[inst.prefab] or {}) do
        if inst.components.constructionsite:GetMaterialCount(v.type) < v.amount then
            return
        end
    end

    TheWorld.components.hua_internet_topology:FinishLink(inst.doer, inst.node1, inst.node2)
end

local function CheckWorkFailed(inst)
    if inst.doer == nil or inst.node1 == nil or inst.node2 == nil or not inst.node1:IsValid() or not inst.node2:IsValid() then
        inst:Remove()
        return
    end

    if not TheWorld.components.hua_internet_topology:CheckLinkBetweenNodes(inst.node1, inst.node2) then -- 有人抢先造好了连线
        inst:Remove()
        return
    end
end
local EMPTY_TABLE = {}
local function OnConstruct(self,doer, items)
    if self.must_full then -- 傻逼克雷
        self.builder = nil
        local x, y, z = self.inst.Transform:GetWorldPosition()
        for i, v in ipairs(CONSTRUCTION_PLANS[self.inst.prefab] or EMPTY_TABLE) do
            local item = items[i]
            local prefab = item.prefab
            local count = item.components.stackable and item.components.stackable:StackSize() or 1
            if v.type ~= prefab or v.amount > count then -- 不符合要求，全部倒出来

                if doer.components.talker ~= nil then
                    doer.components.talker:Say(STRINGS.HUA_INTERNET_BUILDER.FAILED_MATERIAL)
                end
                for _, it in ipairs(items) do
                    it.components.inventoryitem:RemoveFromOwner(true)
                    it.components.inventoryitem:DoDropPhysics(x, y, z, true)
                end
                return
            end
        end
    end
    return self.old_OnConstruct(self,doer, items)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    inst:AddTag("constructionsite")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("constructionsite")
    inst.components.constructionsite:SetConstructionPrefab("construction_container")
    inst.components.constructionsite:SetOnConstructedFn(OnConstructed)
    inst.components.constructionsite:OnStopConstruction(inst.Remove)
    inst.components.constructionsite.must_full = true -- 傻逼克雷
    inst.components.constructionsite.old_OnConstruct = inst.components.constructionsite.OnConstruct
    inst.components.constructionsite.OnConstruct = OnConstruct

    -- 用于判断是否有人抢先做好了连线，如果两人同时修好会炸档，就去掉这个功能，那么就会有一个人损失材料
    inst:DoPeriodicTask(0, CheckWorkFailed)

    return inst
end

-- 【【【【【【【【【【【【【如果需求的蜘蛛网可能超过 100 个，要适当增加】】】】】】】】】】】】
local prefabs = {}

for i = 1, 100 do
    prefabs[#prefabs + 1] = Prefab("hua_internet_builder_" .. i, fn)
end

return unpack(prefabs)