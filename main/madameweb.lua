TUNING.MADAMEWEB_FLYDOWN_COST = 10

--免疫毒云的攻击
AddPrefabPostInit("sporecloud", function(inst) 
	if inst.components.aura and inst.components.aura.auraexcludetags then
        table.insert(inst.components.aura.auraexcludetags,"madameweb")
	end
end)

--[[不需要了
local function nospiderrecipe(self)
	local old = self.SetMainCharacter
	function self:SetMainCharacter(maincharacter)
		local changed = nil
		if maincharacter and maincharacter.prefab and maincharacter.prefab == "madameweb" then
			--临时修改一下
			changed = GLOBAL.CUSTOM_RECIPETABS
			GLOBAL.CUSTOM_RECIPETABS.SPIDERCRAFT = nil --我不要蜘蛛
		end
		old(self,maincharacter)
		if changed ~= nil then
			GLOBAL.CUSTOM_RECIPETABS = changed
		end
	end
end
AddClassPostConstruct('screens/playerhud', nospiderrecipe)]]


--可以操作蜘蛛巢穴
local function onuse(inst,doer)
	local stage = inst.data and inst.data.stage
	if stage then
		if stage < 2 then
			return false
		elseif inst.components.shaveable and inst.components.shaveable.on_shaved then
			local shaved = inst.shaving
			inst.components.shaveable.on_shaved(inst,doer)
			inst.shaving = shaved
			if doer.components.madameweb_silkvalue then
				doer.components.madameweb_silkvalue:DoDelta(10)
			end
			return true
		end
	end
end

AddPrefabPostInit("spiderden",function(inst)
	inst.myth_use_needtag = "madameweb"
	inst.onusesgname = "dolongaction"
	inst.MYTH_USE_TYPE = "SPIDERDEN"
    if not GLOBAL.TheWorld.ismastersim then
        return inst
    end
    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = true
	inst.components.myth_use_inventory:SetOnUseFn(onuse)
end)

--===========================================
local zijiren = true
local upvaluehelper = require "components/myth_upvaluehelper"
AddPrefabPostInit("world",function(inst) --加给世界是只运行一次 不需要hook每一个蜘蛛
    if not GLOBAL.TheWorld.ismastersim then
        return inst
    end
	if zijiren then
		if GLOBAL.Prefabs.rabbit and GLOBAL.Prefabs.rabbit.fn then
            local IsCrazyGuy = upvaluehelper.Get(GLOBAL.Prefabs.rabbit.fn, "IsCrazyGuy")
            if IsCrazyGuy then
                local old_IsCrazyGuy = IsCrazyGuy
                local function NewIsCrazyGuy(guy)
                    if guy and guy.prefab == "myth_yutu" then
                        return false
                    end
                    return old_IsCrazyGuy(guy)
                end
                upvaluehelper.Set(GLOBAL.Prefabs.rabbit.fn, "IsCrazyGuy", NewIsCrazyGuy)
            end
        end

		if GLOBAL.Prefabs.bunnyman and GLOBAL.Prefabs.bunnyman.fn then
            local IsCrazyGuy = upvaluehelper.Get(GLOBAL.Prefabs.bunnyman.fn, "IsCrazyGuy")
            if IsCrazyGuy then
                local old_IsCrazyGuy = IsCrazyGuy
                local function NewIsCrazyGuy(guy)
                    if guy and guy.prefab == "myth_yutu" then
                        return false
                    end
                    return old_IsCrazyGuy(guy)
                end
                upvaluehelper.Set(GLOBAL.Prefabs.bunnyman.fn, "IsCrazyGuy", NewIsCrazyGuy)
            end
        end
		
		zijiren = false
	end
end)
--================================================

--使用腺体增加蛛丝值
AddPrefabPostInit("spidergland",function(inst)
	inst:AddTag("silkcontaineritem")
    if inst.components.healer then
		local old_fn = inst.components.healer.onhealfn
		inst.components.healer.onhealfn = function(inst,target)
			if target.components.madameweb_silkvalue then
				target.components.madameweb_silkvalue:DoDelta(5)
			end
			if old_fn then
				old_fn(inst,target)
			end
		end
	end
end)

--不能使用哨子
AddPrefabPostInit("spider_whistle",function(inst)
    if inst.components.followerherder then
		local old_fn = inst.components.followerherder.canherdfn
		inst.components.followerherder.canherdfn = function(whistle, leader)
			if leader:HasTag("madameweb") then
				return false, "WEBBERONLY"
			end
			return old_fn(whistle, leader)
		end
	end
end)

local function needfn(inst,target,doer)
	return target.prefab == "madameweb_armor"
end

local function ongive(inst,target,doer)
	if target and target.components.fueled then
		target.components.fueled:DoDelta(960, doer)
		if inst.components.stackable then
			inst.components.stackable:Get():Remove()
		else
			inst:Remove()
		end
	end
end

--蜘蛛丝可以修复
AddPrefabPostInit("silk",function(inst)
	inst:AddTag("silkcontaineritem")

	inst.myth_useitem_needfn = needfn
	inst.MYTH_USEITEM_TYPE  = "GIVE"
	inst.onuseitemsgname = "give"

    if not GLOBAL.TheWorld.ismastersim then
        return inst
    end
	inst:AddComponent("myth_useitem")
	inst.components.myth_useitem.onuse = ongive
end)

--薄丝飞吻
local function ValidToolWork(target)
	return target ~= nil and
	target.components.workable ~= nil and
	target.components.workable:CanBeWorked() and
	target.components.workable:GetWorkAction() == ACTIONS.NET and
	not (target.components.health ~= nil and target.components.health:IsDead()) 
end
local  MYTH_FEIWEN = Action({priority = 1 , distance=10})
MYTH_FEIWEN.id = "MYTH_FEIWEN"
MYTH_FEIWEN.str = STRINGS.MYTH_FEIWEN_STR
MYTH_FEIWEN.fn = function(act)
    if act.target ~= nil and act.target:IsValid() and not (act.target.components.health ~= nil and act.target.components.health:IsDead()) then
		local kiss = act.target:SpawnChild("madameweb_kissfx") --特效待修改

		local scale = (act.target:HasTag("smallcreature") and 1)
		or (act.target:HasTag("largecreature") and 3)
		or 2
		local scale1 = act.target.Transform:GetScale()
		kiss.Transform:SetScale(scale * scale1,scale * scale1,scale * scale1)

		if ValidToolWork(act.target) and (act.doer.components.madameweb_silkvalue and act.doer.components.madameweb_silkvalue.current >= 1) then 
			act.doer:RemoveTag("can_feiwen")
			act.doer:DoTaskInTime(10,function(doer)
				doer:AddTag("can_feiwen")
			end)
			act.target.components.workable:WorkedBy(act.doer)
			act.doer.components.madameweb_silkvalue:DoDelta(-1)
		else
			local cost =  (act.target:HasTag("smallcreature") and 1)
			or ((act.target:HasTag("largecreature") or act.target:HasTag("epic") ) and 2)
			or 1.5
			if not (act.doer.components.madameweb_silkvalue and act.doer.components.madameweb_silkvalue.current >= cost) then
				return true
			end
			act.doer.components.madameweb_silkvalue:DoDelta(-cost)
			act.doer:RemoveTag("can_feiwen")
			act.doer:DoTaskInTime(10,function(doer)
				doer:AddTag("can_feiwen")
			end)
			if act.target.components.locomotor then
				act.target.components.locomotor:SetExternalSpeedMultiplier(act.target, "myth_feiwen", 0)
			end
			if act.target.myth_feiwen_task then
				act.target.myth_feiwen_task:Cancel()
			end
			act.target.myth_feiwen_task = act.target:DoTaskInTime(5,function(target)
				if target and target.components.locomotor ~= nil then
					target.components.locomotor:RemoveExternalSpeedMultiplier(target, "myth_feiwen")
				end
				act.target.myth_feiwen_task = nil
			end)
		end
        return true
    end
end
AddAction(MYTH_FEIWEN)
AddComponentAction("SCENE", "locomotor" , function(inst, doer, actions,right)
    if right and doer:HasTag("can_feiwen")  and inst:HasTag("locomotor") and doer ~= inst and not inst:HasTag("rideable") 
		and doer.replica.inventory and doer.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil then
		if not inst:HasTag("player") or TheNet:GetPVPEnabled() then
       		table.insert(actions, ACTIONS.MYTH_FEIWEN)
		end
    end
end)

AddComponentAction("SCENE", "feiwenable" , function(inst, doer, actions,right)
    if right and doer:HasTag("madameweb") and not inst:HasTag("rideable") then
       	table.insert(actions, ACTIONS.MYTH_FEIWEN)
    end
end)

local special = {"fireflies"}
for _,v in ipairs(special) do
	AddPrefabPostInit(v,function(inst)
		if TheWorld.ismastersim then
			inst:AddComponent("feiwenable")
		end
	end)
end

AddStategraphActionHandler("wilson",ActionHandler(ACTIONS.MYTH_FEIWEN, "myth_feiwen"))
AddStategraphActionHandler("wilson_client",ActionHandler(ACTIONS.MYTH_FEIWEN, "myth_feiwen"))
AddStategraphState("wilson", State {
    name = "myth_feiwen",
    tags = {"doing", "busy",},
    onenter = function(inst)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("mythkissbye_pre")
		inst.AnimState:PushAnimation("mythkissbye_loop")
		inst.AnimState:PushAnimation("mythkissbye_pst",false)
        local buffaction = inst:GetBufferedAction()
        if buffaction ~= nil then
            if buffaction.pos ~= nil then
                inst:ForceFacePoint(buffaction:GetActionPoint():Get())
            end
        end
    end,
    timeline =
    {
        TimeEvent(1, function(inst)
            inst:PerformBufferedAction()
        end),
    },
    events = {
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    }
})

AddStategraphState("wilson_client", State {
    name = "myth_feiwen",
    tags = {"doing", "busy",},
    onenter = function(inst)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("mythkissbye_pre")
		inst.AnimState:PushAnimation("mythkissbye_loop")
		inst.AnimState:PushAnimation("mythkissbye_pst",false)
		inst:PerformPreviewBufferedAction()
        local buffaction = inst:GetBufferedAction()
        if buffaction ~= nil then
            if buffaction.pos ~= nil then
                inst:ForceFacePoint(buffaction:GetActionPoint():Get())
            end
        end
    end,
    events = {
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    }
})

AddStategraphState("wilson", State {
    name = "myth_feiwen_idle",
    tags = { "idle", "canrotate" },
    onenter = function(inst) 
		if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) ~= nil then
			inst.sg:GoToState("idle")
			return
		end
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("mythkissbye_pre")
		inst.AnimState:PushAnimation("mythkissbye_loop")
		inst.AnimState:PushAnimation("mythkissbye_pst",false)
    end,
    events = {
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    }
})

--毒属性
local function poisonable(victim)
    return not (victim:HasTag("veggie") or --眼球草 西瓜南瓜之类的
                victim:HasTag("structure") or --建筑
                victim:HasTag("wall") or --墙
                victim:HasTag("balloon") or --气球
                victim:HasTag("chess") or --机械生物
                victim:HasTag("shadow") or --暗影生物
                victim:HasTag("shadowcreature") or --暗影生物
                victim:HasTag("shadowminion") or --暗影生物
                victim:HasTag("shadowchesspiece") or --暗影boss
                victim:HasTag("groundspike") or --某些奇怪的东西
                victim:HasTag("smashable"))
        and (victim.components.combat ~= nil and victim.components.health ~= nil)
end

GLOBAL.Myth_DoPoison = function(inst)
	if not poisonable(inst) then 
		return
	end
	if not inst.components.myth_poisonable then
		inst:AddComponent("myth_poisonable")
	end
	inst.components.myth_poisonable:Poison()
end

local function onhit(inst,data)
	local target = data and data.target or nil
	if target and target:IsValid() then
		if not (target.components.health and target.components.health:IsDead()) then
			Myth_DoPoison(target)
		end
	end
end

GLOBAL.Myth_MakePoisonable = function(inst)
	inst:ListenForEvent("onhitother",onhit)
end
GLOBAL.Myth_RemovePoisonable = function(inst)
	inst:RemoveEventCallback("onhitother",onhit)
end

local old_sty =  ACTIONS.DEPLOY.strfn
ACTIONS.DEPLOY.strfn = function(act)
    if  act.invobject ~= nil and act.invobject:HasTag("silkcocoon") then
		return "SILKCOCOON"
	end
	return old_sty(act)
end

--不可以睡蜘蛛巢
local actions = upvaluehelper.Get(EntityScript.CollectActions,"COMPONENT_ACTIONS")
if  actions and  actions.SCENE then
	local old = actions.SCENE.sleepingbag
	actions.SCENE.sleepingbag = function(inst, doer, actions, right)
		if inst:HasTag("spiderden") and doer:HasTag("madameweb") then
			return
		end
		old(inst, doer, actions, right)
	end
end


local function EventPostInit(self)
    local old_fn = self.actionhandlers[ACTIONS.DEPLOY].deststate
    self.actionhandlers[ACTIONS.DEPLOY].deststate = function(inst, action)
		if action and action.invobject and action.invobject:HasTag("silkcocoon") then
			if inst.replica.rider ~= nil and inst.replica.rider:IsRiding() then
				return "doupaction_rider"
			else
				return "doupaction"
			end
        end
        return old_fn(inst, action)
    end
end
AddStategraphPostInit('wilson', EventPostInit)
AddStategraphPostInit('wilson_client', EventPostInit)

AddStategraphState("wilson", State {
    name = "myth_silkfly_up",
    tags = { "notalking", "busy", "nopredict",},
    onenter = function(inst)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("silkflyup")
		inst.components.hunger:Pause()
    end,
	onupdate = function(inst, dt)
		if inst.sg.statemem.multcolourflyup ~= nil then
			inst.sg.statemem.multcolourflyup = inst.sg.statemem.multcolourflyup + dt*2
			local a =  math.max(0, 1 - inst.sg.statemem.multcolourflyup )
			inst.AnimState:SetMultColour(a,a,a,a)
		end
	end,
    timeline =
    {
        TimeEvent(0.5, function(inst)
			inst.components.health:SetInvincible(true)
        end),
        TimeEvent(1.2, function(inst)
			inst:SetCameraDistance(68)
			inst.sg.statemem.multcolourflyup = 0
        end),
        TimeEvent(1.4, function(inst)
			if inst.components.mk_silkflyer then
				inst.components.mk_silkflyer:SetFlying(true)
			end
			inst.sg.statemem.issilkfly_up = true
        end),
    },
    events = {
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    },
	onexit = function(inst)
		if inst.sg.statemem.issilkfly_up then
			inst.AnimState:SetMultColour(0,0,0,0)
		end
		inst.components.hunger:Resume()
	end,
})

AddStategraphState("wilson", State {
    name = "myth_silkfly_down",
    tags = { "notalking", "busy", "nopredict","silkfly_down"},
    onenter = function(inst)
		inst.AnimState:SetBuild(inst.components.mk_silkflyer.build or inst.prefab)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("silkflydown")
		inst.components.health:SetInvincible(true)
		inst.components.hunger:Pause()
		inst.sg.statemem.multcolourflydown = 0
		inst:Show()
    end,
	onupdate = function(inst, dt)
		if inst.sg.statemem.multcolourflydown ~= nil then
			inst.sg.statemem.multcolourflydown = inst.sg.statemem.multcolourflydown + dt*3
			local a =  math.max(0, inst.sg.statemem.multcolourflydown)
			inst.AnimState:SetMultColour(a,a,a,a)
		end
	end,
    timeline =
    {
        TimeEvent(1.1, function(inst)
			if inst.components.mk_silkflyer then
				inst.components.mk_silkflyer:SetFlying(false)
			end
			inst.sg.statemem.silkflyerdown = true
        end),
        TimeEvent(1.2, function(inst)
			for k,v in pairs(inst.components.inventory.equipslots) do
				local item = inst.components.inventory:Unequip(k)
				if item then
					inst.components.inventory:Equip(item)
				end
			end
        end),
    },
    events = {
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    },
	onexit = function(inst)
		if not inst.sg.statemem.silkflyerdown then
			if inst.components.mk_silkflyer then
				inst.components.mk_silkflyer:SetFlying(false)
			end
			for k,v in pairs(inst.components.inventory.equipslots) do
				local item = inst.components.inventory:Unequip(k)
				if item then
					inst.components.inventory:Equip(item)
				end
			end
		end
		inst.components.health:SetInvincible(false)
		inst.AnimState:SetMultColour(1,1,1,1)
		inst.components.hunger:Resume()
	end,
})

AddStategraphState("wilson", State {
    name = "mythtumble",
    tags = { "notalking", "busy", "nopredict","silkfly_down"},
    onenter = function(inst)
		inst.AnimState:SetBuild(inst.components.mk_silkflyer.build or inst.prefab)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("mythtumble")
		inst.components.health:SetInvincible(true)
		inst.components.hunger:Pause()
		inst.sg.statemem.multcolourflydown = 0
		inst:Show()
    end,
	onupdate = function(inst, dt)
		if inst.sg.statemem.multcolourflydown ~= nil then
			inst.sg.statemem.multcolourflydown = inst.sg.statemem.multcolourflydown + dt*3
			if inst.sg.statemem.multcolourflydown  > 1 then
				inst.sg.statemem.multcolourflydown = 1
			end
			local a =  math.max(0, inst.sg.statemem.multcolourflydown)
			inst.AnimState:SetMultColour(a,a,a,a)
		end
	end,
    timeline =
    {
        TimeEvent(0.6, function(inst)
			if inst.components.mk_silkflyer then
				inst.components.mk_silkflyer:SetFlying(false)
			end
			inst.sg.statemem.silkflyerdown = true
			inst.components.health:SetInvincible(false)
			inst.components.health:DoDelta(-20, false, "mythtumble")
        end),
        TimeEvent(0.7, function(inst)
			for k,v in pairs(inst.components.inventory.equipslots) do
				local item = inst.components.inventory:Unequip(k)
				if item then
					inst.components.inventory:Equip(item)
				end
			end
        end),
    },
    events = {
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    },
	onexit = function(inst)
		if not inst.sg.statemem.silkflyerdown then
			if inst.components.mk_silkflyer then
				inst.components.mk_silkflyer:SetFlying(false)
			end
		end
		inst.components.health:SetInvincible(false)
		inst.AnimState:SetMultColour(1,1,1,1)
		inst.components.hunger:Resume()
	end,
})

AddStategraphState("wilson_client", State {
    name = "myth_silkfly_down",
    tags = { "notalking", "busy", "nopredict","silkfly_down"},
    onenter = function(inst)
		inst:PerformPreviewBufferedAction()
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("silkflydown")
    end,
    events = {
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    },
})

local  MYTH_SILKFLY = Action({distance=36,priority = 1})
MYTH_SILKFLY.id = "MYTH_SILKFLY"
MYTH_SILKFLY.strfn = function(act)
    if  act.doer ~= nil and act.doer:HasTag("madameweb_spider") then
		return "MYTH_SILKFLY_DOWN"
	end
	return "MYTH_SILKFLY_STR"
end
MYTH_SILKFLY.fn = function(act)
	return true
end
AddAction(MYTH_SILKFLY)
AddStategraphActionHandler("wilson",ActionHandler(ACTIONS.MYTH_SILKFLY, function(inst, action)
	if inst:HasTag("madameweb_spider") then
		return "myth_silkfly_jump_pre"
	else
		return "myth_silkfly_down"
	end
end))
AddStategraphActionHandler("wilson_client",ActionHandler(ACTIONS.MYTH_SILKFLY, function(inst, action)
	if inst:HasTag("madameweb_spider") then
		return "myth_silkfly_jump_pre"
	else
		return "myth_silkfly_down"
	end
end))

STRINGS.ACTIONS.MYTH_SILKFLY = {
    MYTH_SILKFLY_STR = STRINGS.MYTH_SILKFLY_STR,
    MYTH_SILKFLY_DOWN = STRINGS.MYTH_SILKFLY_DOWN,
}

--产卵  madameweb_spawnsilk
AddStategraphState("wilson", State {
    name = "madameweb_spawnsilk",
    tags = { "notalking", "nopredict","silkfly_down"},
    onenter = function(inst)
        inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("ma_spawnsilk_pre")
		inst.AnimState:PushAnimation("ma_spawnsilk_loop")
		inst.AnimState:PushAnimation("ma_spawnsilk_pst",false)
    end,
    timeline =
    {
		TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/scream_short") end),
        
		TimeEvent(0.8+4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/givebirth_voice") end),

		TimeEvent(0.8+8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/givebirth_foley") end),
		TimeEvent(0.8+10*FRAMES, function(inst)
			local x,y,z = inst.Transform:GetWorldPosition()
			if math.abs(z) > 1500 then
				return
			end
			if TheWorld.Map:IsAboveGroundAtPoint(x,y,z) and inst.components.madameweb_silkvalue and inst.components.madameweb_silkvalue.current >= 5 then
				local fx = SpawnPrefab("madameweb_groundsilk")
				fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
				if fx.ResetBrother then
					fx:ResetBrother()
				end
				inst.components.madameweb_silkvalue:DoDelta(-5)
			end
		end),
    },
    events = {
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
    },
	onexit = function(inst)
	end,
})


local function ForceStopHeavyLifting(inst)
    if inst.components.inventory:IsHeavyLifting() then
        inst.components.inventory:DropItem(
            inst.components.inventory:Unequip(EQUIPSLOTS.BODY),
            true,
            true
        )
    end
end
AddStategraphState("wilson", State{
    name = "madameweb_frozen",
    tags = { "busy", "frozen", "nopredict", "nodangle","madameweb_spider" },

    onenter = function(inst)
        if inst.components.pinnable ~= nil and inst.components.pinnable:IsStuck() then
            inst.components.pinnable:Unstick()
        end

        ForceStopHeavyLifting(inst)
        inst.components.locomotor:Stop()
        inst:ClearBufferedAction()

        inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
        inst.AnimState:PlayAnimation("madameweb_frozen")
        inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:EnableMapControls(false)
            inst.components.playercontroller:Enable(false)
        end

        if inst.components.freezable == nil then
            inst.sg:GoToState("idle", true)
        elseif inst.components.freezable:IsThawing() then
            inst.sg.statemem.isstillfrozen = true
            inst.sg:GoToState("madameweb_thaw")
        elseif not inst.components.freezable:IsFrozen() then
            inst.sg:GoToState("idle", true)
        end
    end,

    events =
    {
        EventHandler("onthaw", function(inst)
            inst.sg.statemem.isstillfrozen = true
            inst.sg:GoToState("madameweb_thaw")
        end),
        EventHandler("unfreeze", function(inst)
            inst.sg:GoToState("idle", true)
        end),
    },

    onexit = function(inst)
        if not inst.sg.statemem.isstillfrozen then
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:EnableMapControls(true)
                inst.components.playercontroller:Enable(true)
            end
        end
        inst.AnimState:ClearOverrideSymbol("swap_frozen")
    end,
})
AddStategraphState("wilson", State{
	name = "madameweb_thaw",
	tags = { "busy", "thawing", "nopredict", "nodangle","madameweb_spider" },

	onenter = function(inst)
		inst.components.locomotor:Stop()
		inst:ClearBufferedAction()

		inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
		inst.AnimState:PlayAnimation("madameweb_frozen_pst", true)
		inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")

		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller:EnableMapControls(false)
			inst.components.playercontroller:Enable(false)
		end
	end,

	events =
	{
		EventHandler("unfreeze", function(inst)
			inst.sg:GoToState("idle", true)
		end),
	},

	onexit = function(inst)
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller:EnableMapControls(true)
			inst.components.playercontroller:Enable(true)
		end
		inst.SoundEmitter:KillSound("thawing")
		inst.AnimState:ClearOverrideSymbol("swap_frozen")
	end,
})


--[[盘丝不受蜘蛛噩梦光环影响
AddComponentPostInit("sanityaura", function(self)
	local oldGetAura = self.GetAura
	function self:GetAura(observer)
		if self.inst:HasTag("spider") and (observer and observer:HasTag("madameweb")) then
			return 0
		end
		return oldGetAura(self,observer)
	end
end)]]

--我跳跳跳跳
AddStategraphState("wilson", State {
	name = "myth_silkfly_jump_pre",
	tags = {"doing", "busy", "nointerrupt", "nomorph" },

	onenter = function(inst)
		inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("madameweb_hang_up_pre")
		local buffaction = inst:GetBufferedAction()
		local pos = buffaction ~= nil and buffaction:GetActionPoint() or nil
		if pos ~= nil then
			inst:ForceFacePoint(pos:Get())
			inst.sg.statemem.targetpos = pos
		end
	end,
	events =
	{
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				if inst.AnimState:IsCurrentAnimation("madameweb_hang_up_pre") then
					inst.components.madameweb_silkvalue:DoDelta(-TUNING.MADAMEWEB_FLYDOWN_COST)
					inst:PerformBufferedAction()
					inst.sg:GoToState("myth_silkfly_jump", {pos = inst.sg.statemem.targetpos })
				else
					inst.sg:GoToState("idle")
				end
			end
		end),
	},
	onexit = function(inst)
	end,
})

AddStategraphState("wilson_client", State {
	name = "myth_silkfly_jump_pre",
	tags = {"doing", "busy", "nointerrupt", "nomorph" },

	onenter = function(inst)
		inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("madameweb_hang_up_pre")
		inst.AnimState:PushAnimation("madameweb_hang_up_pst", false)
		inst:PerformPreviewBufferedAction()
		inst.sg:SetTimeout(2)
	end,
	onupdate = function(inst)
		if inst:HasTag("doing") then
			if inst.entity:FlattenMovementPrediction() then
				inst.sg:GoToState("idle", "noanim")
			end
		elseif inst.bufferedaction == nil then
			inst.sg:GoToState("idle")
		end
	end,

	ontimeout = function(inst)
		inst:ClearBufferedAction()
		inst.sg:GoToState("idle")
	end,
})
AddStategraphState("wilson", State {
	name = "myth_silkfly_jump",
	tags = { "aoe", "doing", "busy", "nointerrupt", "nopredict", "nomorph" },

	onenter = function(inst, data)
		if data ~= nil and data.pos then
			inst.sg.statemem.targetpos = data.pos
			inst.AnimState:PlayAnimation("madameweb_hang_up_pst")
			return
		end
		--很遗憾你失败了
		inst.sg:GoToState("idle", true)
	end,

	timeline =
	{
		TimeEvent(FRAMES, function(inst)
			inst.DynamicShadow:Enable(false)
			inst.sg:AddStateTag("noattack")
			inst.components.health:SetInvincible(true)
		end),
	},

	events =
	{
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg.statemem.superjump = true
				inst:Hide()
				inst.Physics:Teleport(inst.sg.statemem.targetpos.x, 0, inst.sg.statemem.targetpos.z)
				inst.sg:GoToState("myth_silkfly_jump_pst")
			end
		end),
	},

	onexit = function(inst)
		if not inst.sg.statemem.superjump then
			inst.components.health:SetInvincible(false)
		end
		inst:Show()
	end,
})


local function GetPoints(pt)
    local points = {}
    local radius = 1

    for i = 1, 4 do
        local theta = 0
        local circ = 2*PI*radius
        local numPoints = circ * 0.25
        for p = 1, numPoints do

            if not points[i] then
                points[i] = {}
            end

            local offset = Vector3(radius * math.cos(theta), 0, -radius * math.sin(theta))
            local point = pt + offset

            table.insert(points[i], point)

            theta = theta - (2*PI/numPoints)
        end

        radius = radius + 3

    end
    return points
end

AddStategraphState("wilson", State {
	name = "myth_silkfly_jump_pst",
	tags = { "aoe", "doing", "busy", "noattack", "nopredict", "nomorph" },

	onenter = function(inst, data)
		inst.AnimState:PlayAnimation("madameweb_hang_down_pre")
		inst.AnimState:PushAnimation("madameweb_hang_down_pst",false)
		inst.components.health:SetInvincible(true)
	end,

	timeline =
	{
		TimeEvent(0.4, function(inst)
			inst.components.health:SetInvincible(false)
		end),
		TimeEvent(0.75, function(inst)
			local map = TheWorld.Map
			ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .015, .8, inst, 20)
			local pt = inst:GetPosition()
			SpawnPrefab("groundpoundring_fx").Transform:SetPosition(pt.x, 0, pt.z)
			local points = GetPoints(pt)
			local ents = TheSim:FindEntities(pt.x, 0, pt.z,6,{"_combat","_health" }, {"spider","madameweb_spidenden","playerghost", "abigail","INLIMBO", "player","companion","wall","madameweb_baby" })           
		   	for _,v in pairs(ents)do
				if v and v.components.health and not v.components.health:IsDead() then
					if not (v.components.follower and v.components.follower.leader == inst) then
						v.components.combat:GetAttacked(inst,100,nil)
						Myth_DoPoison(v)
					end
				end
			end
			for i = 1, 2 do
				if points[i] then
					for k, v in pairs(points[i]) do
						if map:IsPassableAtPoint(v:Get()) then
							SpawnPrefab("groundpound_fx").Transform:SetPosition(v.x, 0, v.z)
						end
					end
				end
			end
		end),
	},
	events =
	{
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
	},

	onexit = function(inst)
		inst.components.health:SetInvincible(false)
	end,
})


AddStategraphState("wilson", State {
	name = "madameweb_becamespider",
	tags = {"doing", "busy", "noattack", "nopredict", "nomorph","madameweb_spider"},

	onenter = function(inst, data)
		inst.Physics:Stop()
		inst.AnimState:AddOverrideBuild("madameweb_spider1")
		inst.AnimState:PlayAnimation("madameweb_spider")
	end,

	timeline =
	{
		TimeEvent(1.1, function(inst)
			inst.sg.statemem.spiderover = true
			inst.sg:RemoveStateTag("busy")
			inst._beacmespider:set(true)
		end),
	},
	events =
	{
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("madameweb_spider_idle")
			end
		end),
	},

	onexit = function(inst)
		if not inst.sg.statemem.spiderover then
			inst.AnimState:ClearOverrideSymbol("madameweb_spider1")
		end
	end,
})

--[[-奇怪的修改 我要开了礼物谁也别拦我！
local DANGER_ONEOF_TAGS = { "monster", "pig", "_combat" }
local DANGER_NOPIG_ONEOF_TAGS = { "monster", "_combat" }
local function MyNearDanger(inst)
    local hounded = TheWorld.components.hounded
    if hounded ~= nil and (hounded:GetWarning() or hounded:GetAttacking()) then
        return true
    end
    local burnable = inst.components.burnable
    if burnable ~= nil and (burnable:IsBurning() or burnable:IsSmoldering()) then
        return true
    end
    local nospiderdanger = inst:HasTag("spiderwhisperer") or inst:HasTag("spiderdisguise") or inst:HasTag("madameweb")
    local nopigdanger = not inst:HasTag("monster")
    return FindEntity(inst, 10,
        function(target)
            return (target.components.combat ~= nil and target.components.combat.target == inst)
                or ((target:HasTag("monster") or (not nopigdanger and target:HasTag("pig"))) and
                    not target:HasTag("player") and
                    not (nospiderdanger and (target:HasTag("spider") or target:HasTag("madameweb_baby"))) and
                    not (inst.components.sanity:IsSane() and target:HasTag("shadowcreature")))
        end,
        nil, nil, nopigdanger and DANGER_NOPIG_ONEOF_TAGS or DANGER_ONEOF_TAGS) ~= nil
end

local canopen = false
AddStategraphPostInit(
    "wilson",
    function(sg)
        local opengift = sg.states["opengift"]
        if opengift ~= nil and canopen then
			local IsNearDanger = upvaluehelper.Get(opengift.onenter,"IsNearDanger")	
			if IsNearDanger ~= nil then
				local old = IsNearDanger
				local function newIsNearDanger(inst)
					if inst.prefab == "madameweb" then
						return MyNearDanger(inst) --开！都给我开！
					end
					return old(inst)
				end
				upvaluehelper.Set(opengift.onenter,"IsNearDanger",newIsNearDanger)	
			end
			canopen = false
		end
	end
)]]

