local itemassets =
{
    Asset("ANIM", "anim/myth_plant_manzhushahua_item.zip"),
    Asset("ANIM", "anim/myth_plant_manzhushahua.zip"),
	Asset("ATLAS", "images/inventoryimages/myth_higanbana_item.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_higanbana_item_active.xml"),
}

local prefabs =
{
	"oceanfishingbobber_twig_projectile",
}


local function hasghost(inst)
    for i, v in ipairs(AllPlayers) do
        if v:HasTag("playerghost") and not v.myth_notbeghost then
            return true
        end
    end   
    return false
end

local function checkghost(inst)
    if hasghost(inst) then
        inst._beactive = true
        inst.AnimState:PlayAnimation("open",true)
        inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_higanbana_item_active.xml"
        inst.components.inventoryitem:ChangeImageName("myth_higanbana_item_active")
    else
        inst._beactive = false
        inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_higanbana_item.xml"
        inst.components.inventoryitem:ChangeImageName("myth_higanbana_item")
        inst.AnimState:PlayAnimation("close",true)
    end

    if inst._beactive and inst.components.rechargeable:IsCharged() then
        inst.components.myth_use_inventory.canuse = true
    else
        inst.components.myth_use_inventory.canuse = false
    end
end

local function OnCharged(inst)
	if inst.components.myth_use_inventory ~= nil then
	    inst.components.myth_use_inventory.canuse = inst._beactive
	end
end

local function OnDischarged(inst)
	if inst.components.myth_use_inventory ~= nil then
        inst.components.myth_use_inventory.canuse = false
	end
end


local function spawanfxs(x,y,z,prefab,rad,need,max,nottag,inst,pos,limit)
	local max = max or 50
	local num = 0
	local map = TheWorld.Map
	for k = 1,max do
		local offset = FindValidPositionByFan(
			math.random() * 2 * PI,
			math.random() * rad,
			20,
			function(offset)
				local pt = Vector3(x + offset.x, 0, z + offset.z)
				return map:IsPassableAtPoint(pt.x, 0, pt.z,false,true)
					and not map:IsPointNearHole(pt)
					and #TheSim:FindEntities(pt.x, 0, pt.z, limit or 1, nottag) <= 0
			end
		)
		if offset ~= nil then
            local fx = SpawnPrefab(prefab)
			fx.Transform:SetPosition(x + offset.x, 0, z + offset.z)
            if pos then
                fx.endpos = pos
                fx.flower = inst
            end
			num = num + 1
		end
		if num >= need then
			break
		end
	end
end

local function onuse(inst,doer)
    inst.components.rechargeable:Discharge(15) --持续20秒
    local pos = doer:GetPosition()
    for i, v in ipairs(AllPlayers) do
        if v:HasTag("playerghost") and not v.myth_notbeghost and v ~= doer  then
            local x,y,z = v.Transform:GetWorldPosition()
            spawanfxs(x,0,z,"myth_higanbana_tele",3,math.random(10,12),40,"myth_higanbana_tele",inst,pos)
        end
    end  
    local x,y,z = doer.Transform:GetWorldPosition()
    spawanfxs(pos.x,0,pos.z,"myth_higanbana_tele",3,math.random(10,11),40,"myth_higanbana_tele")

    return true
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_plant_manzhushahua")
    inst.AnimState:SetBuild("myth_plant_manzhushahua_item")
    inst.AnimState:PlayAnimation("close")

    MakeInventoryFloatable(inst)

    inst.myth_use_needtag = "yama_commissioners"

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst._beactive = false

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_higanbana_item.xml"

    inst:AddComponent("tradable")

    inst:AddComponent("inspectable")

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(5)
    inst.components.finiteuses:SetUses(5)
    inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = true
    inst.components.myth_use_inventory:SetOnUseFn(onuse)

	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetOnDischargedFn(OnDischarged)
	inst.components.rechargeable:SetOnChargedFn(OnCharged)

    MakeHauntable(inst)

    inst:ListenForEvent("myth_flower_ghostchange", function()
        checkghost(inst)
    end,TheWorld)
    inst:DoTaskInTime(0,checkghost)

    return inst
end

local function OnDirty(inst)
    if inst._player:value() and  ThePlayer ~= nil and ThePlayer == inst._player:value() then
        inst.AnimState:PlayAnimation("idle_"..inst.num:value().."_pre")
        inst.AnimState:PushAnimation("idle_"..inst.num:value().."_loop")
    end
end

local function setowner(inst,owner)
    inst._player:set(owner)
end

local function timedone(inst)
    inst.AnimState:PlayAnimation("idle_"..inst.num:value().."_pst")
    inst:DoTaskInTime(0.9,ErodeAway)
end

local function revivefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_plant_manzhushahua")
    inst.AnimState:SetBuild("myth_plant_manzhushahua")

    inst.num = net_tinybyte(inst.GUID, "manzhushahua._num")

    inst._player = net_entity(inst.GUID, "manzhushahua._player", "playerdirty")

    inst:SetPrefabNameOverride("myth_higanbana_item")
    if not TheNet:IsDedicated() then
        inst:ListenForEvent("playerdirty", OnDirty)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.num:set(math.random(3))

    inst:AddComponent("inspectable")

    inst.SetOwner = setowner

    inst.persists = false

    inst.entity:SetCanSleep(false)

    inst.timedone_task = inst:DoTaskInTime(10,timedone)

    MakeHauntable(inst)
    inst.components.hauntable:SetOnUnHauntFn(function(inst,doer)
        if doer and inst._player:value() == doer then
            inst.timedone_task :Cancel()
            inst.timedone_task  = nil
            return true
        end
    end)

    return inst
end

--=======================传送的

local function teletimedone(inst)
    inst.AnimState:PlayAnimation("idle_"..inst.num.."_pst")
    inst:DoTaskInTime(0.9,ErodeAway)
end

local BLOOM_CHOICES =
{
    ["stalker_bulb"] = .5,
    ["stalker_bulb_double"] = .5,
    ["stalker_berry"] = 1,
    ["stalker_fern"] = 8,
}

local STALKERBLOOM_TAGS = { "stalkerbloom" }
local function DoPlantBloom(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local map = TheWorld.Map
    local offset = FindValidPositionByFan(
        math.random() * 2 * PI,
        math.random() * 3,
        12,
        function(offset)
            local x1 = x + offset.x
            local z1 = z + offset.z
            return map:IsPassableAtPoint(x1, 0, z1)
                and map:IsDeployPointClear(Vector3(x1, 0, z1), nil, 1)
                and #TheSim:FindEntities(x1, 0, z1, 2.5, STALKERBLOOM_TAGS) < 4
        end
    )

    if offset ~= nil then
        SpawnPrefab(weighted_random_choice(BLOOM_CHOICES)).Transform:SetPosition(x + offset.x, 0, z + offset.z)
    end
end

local function Teleport(doer,pos)
    doer:SnapCamera()
    doer:ScreenFade(true, 2)

    local ents = TheSim:FindEntities(pos.x, 0, pos.z, 6, {"myth_higanbana_tele"})
    if #ents > 0 then
        local one = #ents ~= 1 and ents[math.random(#ents)] or ents[1]
        if one and one.timedone_task then
            one.timedone_task:Cancel()
            teletimedone(one)
            pos = one:GetPosition()
        end
    end

    if doer.Physics ~= nil then
        doer.Physics:Teleport(pos:Get())
    elseif doer.Transform ~= nil then
        doer.Transform:SetPosition(pos:Get())
    end
    if doer._bah_plant_bloomtask then
        doer._bah_plant_bloomtask:Cancel()
    end
    doer._bah_plant_bloomtask = doer:DoPeriodicTask(3 * FRAMES, DoPlantBloom, 2 * FRAMES)
    doer:DoTaskInTime(3,function(inst)
        if inst._bah_plant_bloomtask then
            inst._bah_plant_bloomtask:Cancel()
            inst._bah_plant_bloomtask = nil
        end
    end)
end

local function telefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_plant_manzhushahua")
    inst.AnimState:SetBuild("myth_plant_manzhushahua")

    local s = GetRandomMinMax(0.8, 1.2)
    inst.Transform:SetScale(s, s, s)

    inst.entity:SetPristine()
    inst:SetPrefabNameOverride("myth_higanbana_item")

    inst:AddTag("myth_higanbana_tele")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.num = math.random(3)

    inst.AnimState:PlayAnimation("idle_"..inst.num.."_pre")
    inst.AnimState:PushAnimation("idle_"..inst.num.."_loop")

    inst:AddComponent("inspectable")

    inst.persists = false

    inst.entity:SetCanSleep(false)

    inst.timedone_task = inst:DoTaskInTime(20,teletimedone)

    MakeHauntable(inst)
    inst.components.hauntable:SetOnHauntFn(function(inst,doer)
        if doer and inst.endpos ~= nil and  inst.flower and inst.flower:IsValid() then
            inst.timedone_task:Cancel()
            Teleport(doer,inst.endpos)
            if inst.flower and inst.flower.components.finiteuses then
                inst.flower.components.finiteuses:Use(1)
            end
            teletimedone(inst)
            return true
        end
    end)
    return inst
end

---还阳咯

local function builder_onbuilt(inst, builder)

    if builder then
        local x,y,z = builder.Transform:GetWorldPosition()
        builder.components.sanity:DoDelta(-15)
        spawanfxs(x,0,z,"myth_higanbana_tele",8,30,50,"myth_higanbana_tele",nil,nil,0.6)
        local ents = TheSim:FindEntities(x, 0, z, 8, {"playerghost"})
        local base = {5,30,1} --复活的基础属性
        local hat = builder.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") then
            local hassoul,soulnum = hat.components.container:HasItemWithTag("soul_yama", 1)
            if hassoul then
                base[1] = base[1] + 10 * soulnum
                base[2] = base[2] + 5 * soulnum
                base[3] = base[3] + 3 * soulnum
                hat.components.container:DestroyContents()
            end
        end
        for i, v in ipairs(ents) do
            v:PushEvent("respawnfromghost",{ source = inst })
            v.myth_higanbana_revive_states = {base[1],base[2],base[3]}
            v:DoTaskInTime(3,function(player)
                if player.myth_higanbana_revive_states and not v:HasTag("playerghost") then
                    player.myth_higanbana_revive_states = nil
                end
            end)
        end
    end
    inst:Remove()
end

local function huanyangfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()

    inst:AddTag("CLASSIFIED")

    inst.persists = false

    inst:DoTaskInTime(0, inst.Remove)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.OnBuiltFn = builder_onbuilt

    return inst
end


local function CheckMigrationPets(inst, item)
    if inst.migrationpets ~= nil then
        if item.components.petleash ~= nil then
            for k, v in pairs(item.components.petleash:GetPets()) do
                table.insert(inst.migrationpets, v)
            end
        end

        if item.components.migrationpetowner ~= nil then
            local pet = item.components.migrationpetowner:GetPet()
            if pet ~= nil then
                table.insert(inst.migrationpets, pet)
            end
        end

        if item.components.container ~= nil then
            for k, v in pairs(item.components.container.slots) do
                if v ~= nil then
                    CheckMigrationPets(inst, v)
                end
            end
        end
    end
end

local function check_rebornplayer(inst)
    for i,v in ipairs(AllPlayers) do
        if inst:IsValid() and v:IsValid() and (inst.save_player and inst.save_player.id == v.userid) and inst:GetDistanceSqToInst(v) < 16 then
            local data = inst.save_player.inventory
            if data then
                --生成物品
                if data.items ~= nil then
                    for k, v in pairs(data.items) do
                        local item = SpawnSaveRecord(v)
                        if item ~= nil then
                            CheckMigrationPets(v, item)
                            if item.Transform then
                                item.Transform:SetPosition(inst.Transform:GetWorldPosition())
                            end
                            --self:GiveItem(item, k)
                        end
                    end
                end
                if data.equip ~= nil then
                    for k, v in pairs(data.equip) do
                        local item = SpawnSaveRecord(v)
                        if item ~= nil then
                            if item.Transform then
                                item.Transform:SetPosition(inst.Transform:GetWorldPosition())
                            end
                            CheckMigrationPets(v, item)
                            --self:Equip(item)
                        end
                    end
                end
                if data.activeitem ~= nil then
                    local item = SpawnSaveRecord(data.activeitem)
                    if item ~= nil then
                        if item.Transform then
                            item.Transform:SetPosition(inst.Transform:GetWorldPosition())
                        end
                        CheckMigrationPets(v, item)
                        --self:GiveItem(item)
                    end
                end
            end
            inst:Remove()
        end
    end
end

local function onload(inst, data)
    if data ~= nil and data.save_player ~= nil then
        inst.save_player = data.save_player 
    end
end

local function onsave(inst, data)
    data.save_player = inst.save_player or nil
end

local function rebornfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_plant_manzhushahua")
    inst.AnimState:SetBuild("myth_plant_manzhushahua")

    local s = GetRandomMinMax(0.8, 1.2)
    inst.Transform:SetScale(s, s, s)

    inst.entity:SetPristine()
    --inst:SetPrefabNameOverride("myth_higanbana_item")

    if not TheWorld.ismastersim then
        return inst
    end
    inst.save_player = {}

    inst.num = math.random(3)

    inst.AnimState:PlayAnimation("idle_"..inst.num.."_pre")
    inst.AnimState:PushAnimation("idle_"..inst.num.."_loop")

    inst:AddComponent("inspectable")

    inst.entity:SetCanSleep(false)

    inst.check_rebornplayer = inst:DoPeriodicTask(1,check_rebornplayer,2)

    inst.OnSave = onsave
    inst.OnLoad = onload

    MakeHauntable(inst)
    return inst
end

return Prefab("myth_higanbana_item", fn, itemassets),
    Prefab("myth_higanbana_revive", revivefn),
    Prefab("myth_higanbana_tele", telefn),
    Prefab("myth_higanbana_reborn", rebornfn),
    Prefab("myth_bahy", huanyangfn)
