require "prefabs/winter_ornaments"

local function OnStartBundling(inst)--, doer)
    inst.components.stackable:Get():Remove()
end

local function MakeWrap(name, containerprefab, tag, cheapfuel)
    local assets =
    {
        Asset("ANIM", "anim/"..name..".zip"),
        Asset( "ATLAS", "images/inventoryimages/"..name..".xml" ),
    }

    local prefabs =
    {
        name,
        containerprefab,
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.AnimState:SetBank("madameweb_silkcocoon")
        inst.AnimState:SetBuild("madameweb_silkcocoon")
        inst.AnimState:PlayAnimation("item")

        if tag ~= nil then
            inst:AddTag(tag)
        end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("stackable")
        inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

        inst:AddComponent("inspectable")
        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem:SetSinks(true)
		inst.components.inventoryitem.atlasname = "images/inventoryimages/"..name..".xml"

        inst:AddComponent("bundlemaker")
        inst.components.bundlemaker:SetBundlingPrefabs(containerprefab, name.."_packed")
        inst.components.bundlemaker:SetOnStartBundlingFn(OnStartBundling)

        inst:AddComponent("fuel")
        inst.components.fuel.fuelvalue = cheapfuel and TUNING.TINY_FUEL or TUNING.MED_FUEL

        MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
        MakeSmallPropagator(inst)
        inst.components.propagator.flashpoint = 10 + math.random() * 5
        MakeHauntableLaunchAndIgnite(inst)

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

local function onburnt(inst)
    inst.burnt = true
    inst.components.unwrappable:Unwrap()
end

local function onignite(inst)
    inst.components.unwrappable.canbeunwrapped = false
end

local function onextinguish(inst)
    inst.components.unwrappable.canbeunwrapped = true
end

local function MakeBundle(name, onesize, variations, loot, tossloot, setupdata, bank, build, inventoryimage)
    local assets =
    {
        Asset("ANIM", "anim/madameweb_silkcocoon.zip"),
		Asset( "ATLAS", "images/inventoryimages/"..name..".xml" ),
    }
    local prefabs =
    {
        "ash",
    }

    if loot ~= nil then
        for i, v in ipairs(loot) do
            table.insert(prefabs, v)
        end
    end

    local function OnWrapped(inst, num, doer)
        if doer ~= nil and doer.SoundEmitter ~= nil then
            doer.SoundEmitter:PlaySound("dontstarve/common/together/packaged")
        end
    end

    local function OnUnwrapped(inst, pos, doer)
        if inst.burnt then
            SpawnPrefab("ash").Transform:SetPosition(pos:Get())
        else
            local loottable = (setupdata ~= nil and setupdata.lootfn ~= nil) and setupdata.lootfn(inst, doer) or loot
            if loottable ~= nil then
                local moisture = inst.components.inventoryitem:GetMoisture()
                local iswet = inst.components.inventoryitem:IsWet()
                for i, v in ipairs(loottable) do
                    local item = SpawnPrefab(v)
                    if item ~= nil then
                        if item.Physics ~= nil then
                            item.Physics:Teleport(pos:Get())
                        else
                            item.Transform:SetPosition(pos:Get())
                        end
                        if item.components.inventoryitem ~= nil then
                            item.components.inventoryitem:InheritMoisture(moisture, iswet)
                            if doer and doer.components.inventory then
                                doer.components.inventory:GiveItem(item)
                            elseif tossloot then
                                item.components.inventoryitem:OnDropped(true, .5)
                            end
                        end
                    end
                end
            end
        end
        if doer ~= nil and doer.SoundEmitter ~= nil then
            doer.SoundEmitter:PlaySound("dontstarve/common/together/packaged")
        end
        inst:Remove()
    end

    local OnSave = variations ~= nil and function(inst, data)
        data.variation = inst.variation
    end or nil

    local OnPreLoad = variations ~= nil and function(inst, data)
        if data ~= nil then
            inst.variation = data.variation
        end
    end or nil
    local SHADECANOPY_MUST_TAGS = {"shadecanopy"}
    local SHADECANOPY_SMALL_MUST_TAGS = {"shadecanopysmall"}

    local function custom_candeploy_fn(inst, pos, mouseover, deployer, rot)
        if TheWorld:HasTag("cave") then
            return true
        elseif  math.abs(pos.z) > 1500 then  --一般小房子的坐标都是超过了1500 所以懒得判定具体的了
            return true
        else
            local canopy = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE, SHADECANOPY_MUST_TAGS)
            local canopy_small = TheSim:FindEntities(pos.x,pos.y,pos.z, TUNING.SHADE_CANOPY_RANGE_SMALL, SHADECANOPY_SMALL_MUST_TAGS)
            if #canopy > 0 or #canopy_small > 0 then
                return true
            end
        end
    end

    local function item_ondeploy(inst, pt, deployer)
        local new = SpawnPrefab("madameweb_silkcocoon_hanged")
        if new then
            new.Transform:SetPosition(pt:Get())
            new.AnimState:PlayAnimation("place")
            new.AnimState:PushAnimation("idle")

            if inst.components.unwrappable and inst.components.unwrappable.itemdata ~= nil then
                local self = inst.components.unwrappable
                local num = 0
                new.components.unwrappable.itemdata = self.itemdata
                new.components.unwrappable.origin = self.origin
                for i, v in ipairs(self.itemdata) do
                    if v.prefab == "fireflies" then
                        num = v.data and v.data.stackable and v.data.stackable.stack  or 1
                    end
                end
                if num ~= 0 then
                    new.lightfireflies = num
                    new:FireFliesLight(TheWorld.state.phase)
                end
            end
        end
        inst:Remove()
    end

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.AnimState:SetBank("madameweb_silkcocoon")
        inst.AnimState:SetBuild("madameweb_silkcocoon")
        inst.AnimState:PlayAnimation("packed")

        inst:AddTag("bundle")

        inst:AddTag("unwrappable")
        inst:AddTag("silkcocoon")

        inst._custom_candeploy_fn = custom_candeploy_fn

        if setupdata ~= nil and setupdata.common_postinit ~= nil then
            setupdata.common_postinit(inst, setupdata)
        end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")

        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem:SetSinks(true)
		inst.components.inventoryitem.atlasname = "images/inventoryimages/"..name..".xml"

        inst:AddComponent("unwrappable")
        inst.components.unwrappable:SetOnWrappedFn(OnWrapped)
        inst.components.unwrappable:SetOnUnwrappedFn(OnUnwrapped)

        MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
        MakeSmallPropagator(inst)
        inst.components.propagator.flashpoint = 10 + math.random() * 5
        inst.components.burnable:SetOnBurntFn(onburnt)
        inst.components.burnable:SetOnIgniteFn(onignite)
        inst.components.burnable:SetOnExtinguishFn(onextinguish)

        inst:AddComponent("deployable")
        inst.components.deployable:SetDeployMode(DEPLOYMODE.CUSTOM)
        inst.components.deployable.ondeploy = item_ondeploy

        MakeHauntableLaunchAndIgnite(inst)

        if setupdata ~= nil and setupdata.master_postinit ~= nil then
            setupdata.master_postinit(inst, setupdata)
        end

        inst.OnSave = OnSave
        inst.OnPreLoad = OnPreLoad

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

local function MakeContainer(name, build)
    local assets =
    {
        Asset("ANIM", "anim/"..build..".zip"),
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()

        inst:AddTag("bundle")

        --V2C: blank string for controller action prompt
        inst.name = " "

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("container")
        inst.components.container:WidgetSetup(name)

        inst.persists = false

        return inst
    end

    return Prefab(name, fn, assets)
end

local BURN_DURATION = 2
local function OnStartBurnAnim(inst)
    inst.persists = false

    if inst.components.inspectable ~= nil then
        inst:RemoveComponent("inspectable")
    end

    if inst.components.pickable ~= nil then
        inst:RemoveComponent("pickable")
    end
    
    inst.components.burnable:SetOnExtinguishFn(inst.Remove)

    local theta = math.random() * 2 * PI
    local spd = math.random() * 2
    local ash = SpawnPrefab("ash")
    ash.Transform:SetPosition(inst:GetPosition():Get())
    ash.Physics:SetVel(math.cos(theta) * spd, 8 + math.random() * 4, math.sin(theta) * spd)

    inst:Remove()
end

local function OnExtinguishNotFinishedBurning(inst)
    if inst.burn_anim_task ~= nil then
        inst.burn_anim_task:Cancel()
        inst.burn_anim_task = nil
    end
end

local function OnIgnite(inst, source, doer)
    inst.burn_anim_task = inst:DoTaskInTime(BURN_DURATION, OnStartBurnAnim)
    inst.components.burnable:SetOnExtinguishFn(OnExtinguishNotFinishedBurning)
end

local function onpicked(inst, picker)
    local pos = inst:GetPosition()
    local new = SpawnPrefab("madameweb_silkcocoon_packed")
    if new then
        if inst.components.unwrappable and inst.components.unwrappable.itemdata ~= nil then
            local self = inst.components.unwrappable
            new.components.unwrappable.itemdata = self.itemdata
            new.components.unwrappable.origin = self.origin
        end
        if picker and picker.components.inventory then
            picker.components.inventory:GiveItem(new,nil,pos)
        else
            new.components.inventoryitem:DoDropPhysics(pos.x, pos.y, pos.z)
        end
    end
    inst:Remove()
    return true
end

local function lightlight(inst,phase)
    if phase and phase == "night" and (inst.lightfireflies > 0) then
        inst.Light:Enable(true)
        inst.Light:SetIntensity(0.8)
        inst.AnimState:ShowSymbol("lantern_overlay")
        inst.DynamicShadow:Enable(false)
        if inst.lightfireflies < 2 then
            inst.Light:SetRadius(1.5)
        elseif inst.lightfireflies < 3 then
            inst.Light:SetRadius(2)
        elseif inst.lightfireflies < 4 then    
            inst.Light:SetRadius(2.5) 
        elseif inst.lightfireflies < 5 then    
            inst.Light:SetRadius(3)    
        else
            inst.Light:SetRadius(5) 
            inst.Light:SetIntensity(0.9)
        end
    else
        inst.Light:Enable(false)
        inst.AnimState:HideSymbol("lantern_overlay")
        inst.DynamicShadow:Enable(true)
    end
end

local function OnLoad(inst, data)
	if data then
		inst.lightfireflies = data.lightfireflies or 0
        lightlight(inst, TheWorld.state.phase)
	end
end

local function OnSave(inst, data)
	if inst.lightfireflies ~= nil then
		data.lightfireflies = inst.lightfireflies
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.shadow = inst.entity:AddDynamicShadow()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddLight()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("madameweb_silkcocoon_hanged.tex")

	inst.shadow:SetSize( 1.5, .75 )
    
	inst.AnimState:SetBank("madameweb_silkcocoon")
    inst.AnimState:SetBuild("madameweb_silkcocoon")
	inst.AnimState:PlayAnimation("idle", true)

    inst.MYTH_USE_TYPE = "HARVEST"
    inst.onusesgname = "doshortaction"
    local num = math.random(5)

    inst.AnimState:OverrideSymbol("cocoon_0", "madameweb_silkcocoon", "cocoon_"..(num-1))

    inst:AddTag("flying")
    inst:AddTag("NOBLOCK")
    inst:AddTag("useconstruct_sg")

    inst.Light:SetFalloff(0.75)
    inst.Light:SetIntensity(0.8)
    inst.Light:SetRadius(2)
    inst.Light:SetColour(180 / 255, 195 / 255, 150 / 255)
    inst.Light:Enable(false)

    inst.AnimState:HideSymbol("lantern_overlay")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.AnimState:SetTime(math.random() * 2)
    
    inst.lightfireflies = 0

    inst:AddComponent("unwrappable")
    inst.components.unwrappable.canbeunwrapped = false

	inst:AddComponent("inspectable")
    
    --inst:AddComponent("pickable")
    --inst.components.pickable.picksound = "dontstarve/wilson/harvest_berries"
    --inst.components.pickable.onpickedfn = onpicked
    --inst.components.pickable.canbepicked = true
    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = true
	inst.components.myth_use_inventory:SetOnUseFn(onpicked)

    MakeSmallBurnable(inst, nil, nil, nil, "cocoon_0")
    inst.components.burnable.fxdata[1].prefab = "character_fire"
    inst.components.burnable.fxdata[1].followaschild = true
    inst.components.burnable:SetFXOffset(0, 93, 0)
    inst.components.burnable:SetBurnTime(5) 
    inst.components.burnable:SetOnIgniteFn(OnIgnite)
    inst.components.burnable:SetOnBurntFn(inst.Remove)
    MakeSmallPropagator(inst)

    MakeHauntableIgnite(inst)

    inst:WatchWorldState("phase", lightlight)
	lightlight(inst, TheWorld.state.phase)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.FireFliesLight = lightlight

	return inst
end

local function placer_fn()
    local inst = CreateEntity()

    inst:AddTag("CLASSIFIED")
    inst:AddTag("NOCLICK")
    inst:AddTag("placer")
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("madameweb_silkcocoon")
    inst.AnimState:SetBuild("madameweb_silkcocoon")
    inst.AnimState:PlayAnimation("placer")
    inst.AnimState:SetLightOverride(1)

    inst:AddComponent("placer")

    return inst
end

return	--Prefab("myth_bundle_unwrap",fx),
    Prefab("madameweb_silkcocoon_hanged", fn),
    Prefab("madameweb_silkcocoon_packed_placer", placer_fn),
    MakeContainer("madameweb_silkcocoon_container", "ui_bundle_2x2"),
    MakeBundle("madameweb_silkcocoon_packed", false),
	MakeWrap("madameweb_silkcocoon", "madameweb_silkcocoon_container", nil, false)
