local assets =
{
	Asset( "ANIM", "anim/madameweb.zip" ),
	Asset( "ANIM", "anim/madameweb_kissbuild.zip" ),
	Asset( "ANIM", "anim/ghost_madameweb_build.zip" ),
}

local skins =
{
	normal_skin = "madameweb",
	ghost_skin = "ghost_madameweb_build",
}

local base_prefab = "madameweb"

local tags = {"BASE" ,"MADAMEWEB", "CHARACTER"}

return CreatePrefabSkin("madameweb_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	skin_tags = tags,
	
	build_name_override = "madameweb",
	rarity = "Character",
})