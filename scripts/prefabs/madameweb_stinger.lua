local assets =
{
    Asset("ANIM", "anim/madameweb_stinger.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_stinger.xml"),
    Asset("ATLAS", "images/inventoryimages/madameweb_poisonstinger.xml"),
}

local prefabs =
{
    "impact",
}

local prefabs_yellow =
{
    "impact",
    "electrichitsparks",
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "madameweb_stinger", inst.swap_folder)
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_object")
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function onhit(inst, attacker, target)
    local impactfx = SpawnPrefab("impact")
    if impactfx ~= nil and target.components.combat then
        local follower = impactfx.entity:AddFollower()
        follower:FollowSymbol(target.GUID, target.components.combat.hiteffectsymbol, 0, 0, 0)
        if attacker ~= nil and attacker:IsValid() then
            impactfx:FacePoint(attacker.Transform:GetWorldPosition())
        end
    end
    inst:Remove()
end

local function onthrown(inst, data)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.components.inventoryitem.pushlandedevents = false
end

local function common(anim, num, removephysicscolliders)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("madameweb_stinger")
    inst.AnimState:SetBuild("madameweb_stinger")
    inst.AnimState:PlayAnimation(anim)

    inst:AddTag("sharp")
    inst:AddTag("weapon")
    inst:AddTag("projectile")
    inst:AddTag("myth_feizhen")
    inst:AddTag("silkcontaineritem")

    if removephysicscolliders then
        RemovePhysicsColliders(inst)
    end

    MakeInventoryFloatable(inst, "small", 0.05, {0.75, 0.5, 0.75})

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.swap_folder = "swap_"..num

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(26.67)
    inst.components.weapon:SetRange(8, 10)

    inst:AddComponent("projectile")
    inst.components.projectile:SetSpeed(60)
    inst.components.projectile:SetOnHitFn(onhit)
    inst.components.projectile:SetLaunchOffset(Vector3(-20, 0, 0))
    inst:ListenForEvent("onthrown", onthrown)
    -------

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("equippable")
    inst.components.equippable.restrictedtag = "madameweb"
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    inst.components.equippable.equipstack = true

    MakeHauntableLaunch(inst)

    return inst
end
local function sleepthrown(inst)
    inst.AnimState:PlayAnimation("fly1")
    inst:AddTag("NOCLICK")
    inst.persists = false
end

local function sleepattack(inst, attacker, target)
    if not target:IsValid() then
        return
    end
end

local function sleep()
    local inst = common("idle1", 1)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_stinger.xml"

    inst.components.weapon:SetOnAttack(sleepattack)
    inst.components.projectile:SetOnThrownFn(sleepthrown)

    return inst
end
local function firethrown(inst)
    inst.AnimState:PlayAnimation("fly2")
    inst:AddTag("NOCLICK")
    inst.persists = false
end

local function fireattack(inst, attacker, target)
    if not target:IsValid() then
        return
    end
    Myth_DoPoison(target)
end

local function fire()
    local inst = common("idle2", 2)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_poisonstinger.xml"

    inst.components.weapon:SetOnAttack(fireattack)
    inst.components.projectile:SetOnThrownFn(firethrown)
    return inst
end

return Prefab("madameweb_stinger", sleep, assets, prefabs),
       Prefab("madameweb_poisonstinger", fire, assets, prefabs)
