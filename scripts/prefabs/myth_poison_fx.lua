local assets =
{
	Asset("ANIM", "anim/myth_poison_fx.zip"),
	Asset("ANIM", "anim/myth_poisonbuild_1.zip"),
	Asset("ANIM", "anim/myth_poisonbuild_2.zip"),
	Asset("ANIM", "anim/myth_poisonbuild_3.zip"),
	Asset("ANIM", "anim/myth_poisonbuild_4.zip"),
	Asset("ANIM", "anim/myth_poisonbuild_5.zip"),
}

local function kill(inst)
	inst:Remove()
end

local function StopBubbles(inst)
	inst.AnimState:PushAnimation("level"..inst.level.."_pst", false)
	inst:ListenForEvent("animqueueover", kill)
end

local function setlevel(inst,level)
	inst.level = level
	inst.AnimState:PlayAnimation("level"..level.."_pre")
	inst.AnimState:PushAnimation("level"..level.."_loop", true)
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("myth_poison_fx")
	inst.AnimState:SetBuild("myth_poisonbuild_1")
    inst.AnimState:SetFinalOffset(2)

	inst:AddTag("fx")

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end

	inst.SetLevel = setlevel

	inst.StopBubbles = StopBubbles
	inst.persits = false

	return inst
end

return Prefab("myth_poison_fx", fn, assets)