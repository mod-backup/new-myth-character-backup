local assets =
{
    Asset("ANIM", "anim/madameweb_detoxify.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_detoxify.xml"),
}

local prefabs =
{
	"oceanfishingbobber_twig_projectile",
}

local function onuse(inst,doer)
    if doer and doer.components.myth_poisonable and doer.components.myth_poisonable.poisoned then
        doer.components.myth_poisonable:Cure(inst)
        return true
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("madameweb_detoxify")
    inst.AnimState:SetBuild("madameweb_detoxify")
    inst.AnimState:PlayAnimation("idle")

    MakeInventoryFloatable(inst)

    inst.onusesgname = "dolongaction"

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    -----------------
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_detoxify.xml"

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("tradable")

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = true
	inst.components.myth_use_inventory.canusescene  = true
	inst.components.myth_use_inventory:SetOnUseFn(onuse)

    MakeHauntableLaunch(inst)

    inst:AddComponent("inspectable")

    return inst
end

return Prefab("madameweb_detoxify", fn, assets, prefabs)
