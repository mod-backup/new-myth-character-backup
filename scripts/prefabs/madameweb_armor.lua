local assets =
{
    Asset("ANIM", "anim/madameweb_armor.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_armor.xml"),
}

local prefabs =
{
    "ash",
}

local SPIDER_TAGS = {"spider"}

local function spider_disable(inst)
    if inst.updatetask then
        inst.updatetask:Cancel()
        inst.updatetask = nil
    end
    local owner = inst.components.inventoryitem and inst.components.inventoryitem.owner
    if owner and owner.components.leader then
        if not owner:HasTag("spiderwhisperer") then
            if not owner:HasTag("playermonster")  then
                owner:RemoveTag("monster")
            end
            owner:RemoveTag("spiderdisguise")

            for k,v in pairs(owner.components.leader.followers) do
                if k:HasTag("spider") and k.components.combat and not owner:HasTag("madameweb") then
                    k.components.combat:SuggestTarget(owner)
                end
            end
            owner.components.leader:RemoveFollowersByTag("spider")
        else
            owner.components.leader:RemoveFollowersByTag("spider", function(follower)
                if follower and follower.components.follower then
                    if follower.components.follower:GetLoyaltyPercent() > 0 then
                        return false
                    else
                        return true
                    end
                end
            end)
        end
    end
end

local function spider_update(inst)
    local owner = inst.components.inventoryitem and inst.components.inventoryitem.owner
    if owner and owner.components.leader then
        owner.components.leader:RemoveFollowersByTag("pig")
        local x,y,z = owner.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x,y,z, TUNING.SPIDERHAT_RANGE, SPIDER_TAGS)
        for k,v in pairs(ents) do
            if v.components.follower and not v.components.follower.leader and not owner.components.leader:IsFollower(v) and owner.components.leader.numfollowers < 10 then
                owner.components.leader:AddFollower(v)
            end
        end
    end
end

local function spider_enable(inst)
    local owner = inst.components.inventoryitem and inst.components.inventoryitem.owner
    if owner and owner.components.leader then
        owner.components.leader:RemoveFollowersByTag("pig")
        owner:AddTag("monster")
        owner:AddTag("spiderdisguise")
    end
    inst.updatetask = inst:DoPeriodicTask(0.5, spider_update, 1)
end

local function spider_perish(inst)
    spider_disable(inst)
    inst:Remove()
end

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "madameweb_armor", "swap_body")
    if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end
    if inst.components.fueled ~= nil then
        inst.components.fueled:StartConsuming()
    end
    spider_enable(inst)
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end
    if inst.components.fueled ~= nil then
        inst.components.fueled:StopConsuming()
    end
    spider_disable(inst)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    --inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("madameweb_armor")
    inst.AnimState:SetBuild("madameweb_armor")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("waterproofer")
    inst:AddTag("1/2silkvalue")

    inst.Transform:SetScale(1.15, 1.15, 1.15)

    --inst.MiniMapEntity:SetIcon("backpack.png")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_armor.xml"
    inst.components.inventoryitem:SetOnDroppedFn(spider_disable)

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("madameweb_armor")
    inst.components.container.canbeopened = false

    inst:AddComponent("insulator")
    inst.components.insulator:SetInsulation(120)

    inst:AddComponent("waterproofer")
    inst.components.waterproofer:SetEffectiveness(1)

    inst:AddComponent("fueled")
    --inst.components.fueled.fueltype = FUELTYPE.SILK
    inst.components.fueled:InitializeFuelLevel(4800)
    inst.components.fueled:SetDepletedFn(spider_perish)
    inst.components.fueled.accepting = true

    MakeHauntableLaunchAndDropFirstItem(inst)

    return inst
end

return Prefab("madameweb_armor", fn, assets, prefabs)
