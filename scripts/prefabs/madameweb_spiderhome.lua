require("worldsettingsutil")

local prefabs =
{
    "spider",
    "spider_warrior",
    "silk",
    "spidereggsack",
    "spiderqueen",
}

local assets =
{
    Asset("ANIM", "anim/spider_cocoon.zip"),
    Asset("ANIM", "anim/madameweb_pitfall_cocoon.zip"),
}
local DIET = { FOODTYPE.MEAT }

local canmets = {
    meat = true,
    monstermeat = true,
    meat_dried = true,
    monstermeat_dried = true,
    cookedmeat = true,
    cookedmonstermeat = true,
}

local function OnKilled(inst)
    inst.AnimState:PlayAnimation("cocoon_dead")
    RemovePhysicsColliders(inst)
    inst.SoundEmitter:KillSound("loop")
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spiderLair_destroy")
    inst.components.madameweb_spiderspawn:SpawnSpider(nil,true)
end

local function OnHit(inst, attacker)
    inst.AnimState:PlayAnimation("cocoon_large_hit")
    inst.AnimState:PushAnimation("cocoon_large")
end

local function OnExtinguish(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spidernest_LP", "loop")
end

local function OnIgnite(inst)
    inst.SoundEmitter:KillSound("loop")
    DefaultBurnFn(inst)
end

local function OnFreeze(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")
    inst.AnimState:PlayAnimation("frozen_large", true)
    inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
end

local function OnThaw(inst)
    inst.AnimState:PlayAnimation("frozen_loop_pst_large", true)
    inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")
    inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
end

local function OnUnFreeze(inst)
    inst.AnimState:PlayAnimation("cocoon_large", true)
    inst.SoundEmitter:KillSound("thawing")
    inst.AnimState:ClearOverrideSymbol("swap_frozen")
end

local function displaynamefn(inst)
    local name = STRINGS.NAMES[string.upper("madameweb_spiderhome")]
    return name.."\n"..STRINGS.NAMES.MADAMEWEB_SPIDER_WARRIOR..":"..inst.spider_counts:value().."/6"
end

local function OnGetItemFromPlayer(inst, giver, item)
    if  item.prefab == "madameweb_spider_warrior" then
        local health = item.components.health and item.components.health:GetPercent() < 1 and  item.components.health.currenthealth  or nil
        inst.components.madameweb_spiderspawn:Spawn(health)
    elseif inst.components.madameweb_spiderspawn:CanTake() then
        inst.components.madameweb_spiderspawn:SpawnSpider(giver)
    end
end

local function AcceptTest(inst, item, giver)
    if giver:HasTag("madameweb") then
        if item.prefab == "madameweb_spider_warrior" then
            return inst.components.madameweb_spiderspawn:CanStore()
        elseif canmets[item.prefab] then
            return inst.components.madameweb_spiderspawn:CanTake()
        end
    end
end

local function MakeSpiderDenFn(den_level)
    return function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddGroundCreepEntity()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, .5)

        inst.MiniMapEntity:SetIcon("spiderden_" .. tostring(den_level) .. ".png")

        inst.AnimState:SetBank("spider_cocoon")
        inst.AnimState:SetBuild("madameweb_pitfall_cocoon")
        inst.AnimState:PlayAnimation("cocoon_large")
        inst.AnimState:HideSymbol("bedazzled_flare")

        inst:AddTag("structure")
        inst:AddTag("beaverchewable") -- by werebeaver
        inst:AddTag("hostile")
        inst:AddTag("hive")
        inst:AddTag("madameweb_spidenden")
        inst.Transform:SetScale(0.8, 0.8, 0.8)
        inst.spider_counts  = net_tinybyte(inst.GUID, "madameweb_spiderhome.spider_counts")

        inst.displaynamefn = displaynamefn
        inst:SetPrefabNameOverride("madameweb_poisonbeemine")
        MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.GroundCreepEntity:SetRadius(TUNING.SPIDERDEN_CREEP_RADIUS[1])
        inst.spider_counts:set(0)
        inst.data = {}

        inst:AddComponent("health")
        inst.components.health:SetMaxHealth(200)

        inst:AddComponent("lootdropper")

        MakeMediumBurnable(inst)
        inst.components.burnable:SetOnIgniteFn(OnIgnite)
        inst.components.burnable:SetOnExtinguishFn(OnExtinguish)
        -------------------

        inst:AddComponent("eater")
        inst.components.eater:SetDiet(DIET, DIET)
        ---------------------
        MakeMediumFreezableCharacter(inst)
        inst:ListenForEvent("freeze", OnFreeze)
        inst:ListenForEvent("onthaw", OnThaw)
        inst:ListenForEvent("unfreeze", OnUnFreeze)
        -------------------

        --inst:DoTaskInTime(0, OnInit)

        inst:AddComponent("trader")
        inst.components.trader:SetAcceptTest(AcceptTest)
        inst.components.trader.onaccept = OnGetItemFromPlayer

        inst:AddComponent("combat")
        inst.components.combat:SetOnHit(OnHit)
        inst:ListenForEvent("death", OnKilled)

        MakeMediumPropagator(inst)

        inst:AddComponent("inspectable")
        inst:AddComponent("madameweb_spiderspawn")

        inst:AddComponent("hauntable")
        inst.components.hauntable.cooldown = TUNING.HAUNT_COOLDOWN_MEDIUM

        MakeSnowCovered(inst)

        return inst
    end
end

return  Prefab("madameweb_spiderhome", MakeSpiderDenFn(3), assets, prefabs)