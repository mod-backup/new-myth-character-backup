GLOBAL.setmetatable(env,{__index=function(t,k) return GLOBAL.rawget(GLOBAL,k) end})
getmetatable(TheSim).__index.HasPlayerSkeletons = function() return true end

TUNING.MYTH_CHARACTER_MOD_OPEN = true --全局变量方便别的mod获取

PrefabFiles = {
	"monkey_king",
	"mk_jgb",
	"mk_jgb_pillar",
	"mk_striker",
	--"monkey_start_peach",
	"mk_dsf",
	"mk_dsf_fx",
	"mk_dsf_fx_small",

	"golden_armor_mk",
	"golden_hat_mk",
	"xzhat_mk",

	"neza",
	"neza_none",
	"armor_neza",
	"nz_lance",
	"nz_ring",

	--白骨白骨
	"white_bone",
	"white_bone_none",
	"bone_wand", --骨杖
	"bone_blade",
	"bone_whip", --骨鞭
	"bone_spike",
	"bone_mirror",
	"wb_armors",
	"white_bone_fog",
	"bone_pet",
	"wb_skeleton",
	"wb_heart",

	--pigsy
	"pigsy",
	"pigsy_none",
	"pigsy_rake",
	"pigsy_soil",
	"pigsy_sleepbed",
	"pigsy_hat",

	--yangjian
	"yangjian",
	"yangjian_none",
	"yj_spear",
	"yangjian_armor",
	"yangjian_hair",
	"yangjian_track",
	"skyhound",
	"skyhound_warg",
	"skyhound_fx",

	--玉兔 兔姬
	"myth_yutu",
	"myth_yutu_none",
	"yt_rabbithole",
	"yt_moonbutterfly",
	"medicine_pestle_myth", --擀面杖
	"yt_daoyao",
	"myth_yutu_heal_fx",
	"buffs_myth_c", --玉兔相关的新buff（新的buff可以写进这里）
	"yutu_items", --药粉与琵琶等道具
	"apricot_flower_fx",
	"laurel_flower_fx",
	"hawk_flower_fx",
	"myth_bamboo_basket",

	--特效
	"myth_fxs",
	"yangjian_buzzard",
	"yangjian_buzzard_spawn",

	--黑白无常篇
	"yama_commissioners",
	"yama_commissioners_none",
	"commissioners_items",
	"myth_yama_statue",
	"yama_soulfire_purple",
	"yama_soulfire_blue",
	"myth_plant_equinox_flower",
	"myth_cqf",
	"yama_mound",
	"yama_gravestone",
	"yama_fire_projetile",
	"yama_flyer_fx",
	"yama_resurrectionstatue",
	"myth_astralsanity",

	--特殊通用
	"myth_char_buffs",

	--盘丝娘娘
	"madameweb",
	"madameweb_none",
	"madameweb_net",
	"madameweb_pitfall",
	"madameweb_pitfall_cocoon",
	"madameweb_pitfall_ground",
	"madameweb_pitfall_trap",
	"madameweb_detoxify",
	"madameweb_stinger", --飞针
	"madameweb_beemine",
	"madameweb_poisonbeemine",
	"madameweb_spiderhome",
	"madameweb_spider_warrior",
	"madameweb_armor",
	"madameweb_silkcocoon",
	"myth_poison_fx",
	"madameweb_feisheng",
}

Assets = {
	Asset( "IMAGE", "images/monkey_king_item.tex" ),
    Asset( "ATLAS", "images/monkey_king_item.xml" ),

	Asset("SOUNDPACKAGE", "sound/WhiteBone.fev"),
    Asset("SOUND", "sound/WhiteBone.fsb"),
    Asset("SOUNDPACKAGE", "sound/MK.fev"),
    Asset("SOUND", "sound/MonkeyKing.fsb"),
    Asset("SOUNDPACKAGE", "sound/Nezha.fev"),
    Asset("SOUND", "sound/Nezha.fsb"),
    Asset("SOUNDPACKAGE", "sound/Drum.fev"),
    Asset("SOUND", "sound/Drum.fsb"),
    Asset("SOUNDPACKAGE", "sound/Xiao.fev"),
    Asset("SOUND", "sound/Xiao.fsb"),
    Asset("SOUNDPACKAGE", "sound/Myth_Gourd.fev"),
    Asset("SOUND", "sound/Myth_Gourd.fsb"),
    Asset("SOUNDPACKAGE", "sound/myth_pigsound.fev"),
    Asset("SOUND", "sound/myth_pigsound.fsb"),
    Asset("SOUNDPACKAGE", "sound/Pipa.fev"),
    Asset("SOUND", "sound/Pipa.fsb"),
    Asset("SOUNDPACKAGE", "sound/Cymbals.fev"),
    Asset("SOUND", "sound/Cymbals.fsb"),

    Asset("SOUNDPACKAGE", "sound/Sanxian.fev"),
    Asset("SOUND", "sound/Sanxian.fsb"),

	Asset("ATLAS_BUILD", "images/monkey_king_item.xml", 256),

	Asset( "ATLAS", "images/hud/white_bone_tab.xml" ),
	Asset( "IMAGE", "images/hud/white_bone_tab.tex" ),

	Asset( "ATLAS", "images/hud/yama_tab.xml" ),
	Asset( "IMAGE", "images/hud/yama_tab.tex" ),	

	Asset( "ATLAS", "images/hud/mada_tab.xml" ),
	Asset( "IMAGE", "images/hud/mada_tab.tex" ),

	Asset( "ATLAS", "images/thankyou_myth_gift.xml" ),
	Asset( "IMAGE", "images/thankyou_myth_gift.tex" ),
	
	Asset( "ATLAS", "images/hud/bone_inventorysolt.xml" ),
	Asset( "IMAGE", "images/hud/bone_inventorysolt.tex" ),

	Asset( "ATLAS", "images/hud/white_bonefogover.xml" ),
	Asset( "IMAGE", "images/hud/white_bonefogover.tex" ),

	Asset( "ATLAS", "images/hud/yutu_underground.xml" ),
	Asset( "IMAGE", "images/hud/yutu_underground.tex" ),

	Asset( "ATLAS", "images/hud/yama_status_health.xml" ),
	Asset( "IMAGE", "images/hud/yama_status_health.tex" ),
	
	Asset("ATLAS", "images/inventoryimages/mk_jgb_rec.xml"),
	Asset("ATLAS", "images/inventoryimages/yangjian_track.xml"),
	Asset("ATLAS", "images/inventoryimages/yangjian_ying.xml"),
	Asset("ATLAS", "images/inventoryimages/yt_rabbithole.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_hunger.xml"),	
	Asset("ATLAS", "images/inventoryimages/myth_madameweb.xml"),
	Asset("ATLAS", "images/inventoryimages/yt_daoyao.xml"),

	Asset( "IMAGE", "images/colour_cubes/yangjian_fullmoon_cc.tex"),
	Asset( "IMAGE", "images/colour_cubes/madameweb_vision_cc.tex"),

	Asset( "ATLAS", "images/map_icons/yj_eagle.xml" ),
	Asset( "IMAGE", "images/map_icons/yj_eagle.tex" ),
	
	Asset("ANIM", "anim/madameweb_wrap_bundle.zip"),
	Asset("ANIM", "anim/skyhound_ui.zip"),
	Asset("ANIM", "anim/skyhound_meter.zip"),
	Asset("ATLAS", "images/inventoryimages/skyhound.xml"),
    Asset("ANIM", "anim/mk_golden_armor.zip"),
    Asset("ANIM", "anim/white_boneui.zip"),
    Asset("ANIM", "anim/monkey_kingui.zip"),
    Asset("ANIM", "anim/yangjianui.zip"),
    Asset("ANIM", "anim/pigsy_marryui.zip"),
	Asset("ANIM", "anim/yama_commissionersui.zip"),
	Asset("ANIM", "anim/madamewebui.zip"),
	Asset("ANIM", "anim/madameweb_silkvalue.zip"),
    Asset("ANIM", "anim/nz36.zip"),
    Asset("ANIM", "anim/mk_miao.zip"),
    Asset("ANIM", "anim/mk_miao_ear.zip"),
    Asset("ANIM", "anim/white_bone_changefx.zip"),
    Asset("ANIM", "anim/white_bone_raisefx.zip"),	
    Asset("ANIM", "anim/white_bone_small_puff.zip"),
	Asset("ANIM", "anim/yj_eagle_anim.zip"),
	Asset("ANIM", "anim/yj_eagle_black.zip"),
	Asset("ANIM", "anim/yj_eagle_gold.zip"),
	Asset("ANIM", "anim/yj_eagle_hawk.zip"),
	Asset("ANIM", "anim/myth_yutu_under.zip"),
    Asset("ANIM", "anim/myth_playtimebar.zip"),
	Asset("ANIM", "anim/player_actions_shovel_myth.zip"),	
	Asset("ANIM", "anim/wb_redirect_health.zip"),
	Asset("ANIM", "anim/myth_apricot_wave.zip"),
	Asset("ANIM", "anim/player_mythkissbye.zip"),
	Asset("ANIM", "anim/player_mythfeisheng.zip"),
	Asset("ANIM", "anim/player_fzattack.zip"),
	Asset("ANIM", "anim/player_mythtumble.zip"),
	Asset("ANIM", "anim/player_use_bell_lotus.zip"),
	Asset("ANIM", "anim/player_actions_mkskill.zip"),
	Asset("ANIM", "anim/madameweb_explode.zip"),
	Asset("ANIM", "anim/madameweb_hang.zip"),
	Asset("ANIM", "anim/my_dshand.zip"),

	--蜘蛛变身
	Asset("ANIM", "anim/madameweb_spider1.zip"),
	Asset("ANIM", "anim/madameweb_spider2.zip"),
	Asset("ANIM", "anim/madameweb_spider3.zip"),
	Asset("ANIM", "anim/madameweb_spider4.zip"),

    --for strike fx
    Asset("ANIM", "anim/lavaarena_shadow_lunge.zip"),
    Asset("ANIM", "anim/monkey_king_lunge.zip"),
    Asset("ANIM", "anim/mk_jgb.zip"),

    --for skins build
	Asset("ANIM", "anim/ghost_monkey_king_build.zip"),
	Asset("ANIM", "anim/ghost_neza_build.zip"),
	Asset("ANIM", "anim/ghost_white_bone_build.zip"),
	Asset("ANIM", "anim/ghost_yangjian_build.zip"),
	Asset("ANIM", "anim/ghost_pigsy_build.zip"),
	Asset("ANIM", "anim/ghost_myth_yutu_build.zip"),

	--无常
	Asset("ATLAS", "images/inventoryimages/hat_commissioner_black.xml"),
	Asset("IMAGE", "images/inventoryimages/hat_commissioner_black.tex"),
	Asset("ATLAS", "images/inventoryimages/hat_commissioner_white.xml"),
	Asset("IMAGE", "images/inventoryimages/hat_commissioner_white.tex"),
	Asset("ATLAS", "images/inventoryimages/whip_commissioner.xml"),
	Asset("IMAGE", "images/inventoryimages/whip_commissioner.tex"),
	Asset("ATLAS", "images/inventoryimages/token_commissioner.xml"),
	Asset("IMAGE", "images/inventoryimages/token_commissioner.tex"),
	Asset("ATLAS", "images/inventoryimages/pennant_commissioner.xml"),
	Asset("IMAGE", "images/inventoryimages/pennant_commissioner.tex"),
	Asset("ATLAS", "images/inventoryimages/bell_commissioner.xml"),
	Asset("IMAGE", "images/inventoryimages/bell_commissioner.tex"),
	Asset("ATLAS", "images/inventoryimages/soul_ghast.xml"),
	Asset("IMAGE", "images/inventoryimages/soul_ghast.tex"),
	Asset("ATLAS", "images/inventoryimages/soul_specter.xml"),
	Asset("IMAGE", "images/inventoryimages/soul_specter.tex"),
	Asset("ATLAS", "images/inventoryimages/myth_bahy.xml"),
	Asset("IMAGE", "images/inventoryimages/myth_bahy.tex"),

	--玉兔重做tip
	Asset("ATLAS", "images/hud/jadehare_tab.xml"),
	Asset("IMAGE", "images/hud/jadehare_tab.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_hypnoticherb.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_hypnoticherb.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_lifeelixir.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_lifeelixir.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_charged.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_charged.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_improvehealth.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_improvehealth.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_coldeye.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_coldeye.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_becomestar.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_becomestar.tex"),
	Asset("ATLAS", "images/inventoryimages/powder_m_takeiteasy.xml"),
    Asset("IMAGE", "images/inventoryimages/powder_m_takeiteasy.tex"),
	Asset("ANIM", "anim/playguitar_myth.zip"),
	Asset("ATLAS", "images/inventoryimages/guitar_jadehare.xml"),
    Asset("IMAGE", "images/inventoryimages/guitar_jadehare.tex"),
	Asset("SOUNDPACKAGE", "sound/Jadehare.fev"),
    Asset("SOUND", "sound/Jadehare.fsb"),
}

function AddAssets(...)
	for _,v in ipairs({...})do table.insert(Assets, v) end
end

local skillimages = {
	"mk_hyon","mk_hyoff","mk_dsf","mk_yzqt","white_bone_off","white_bone_on","pigsy_on","pigsy_off",
	"skyeye_off","skyeye_on","yangjian_buzzard","skytrack","yutu_daoyao","yutu_pipa","yama_white","yama_black",
	"yama_bahy","madameweb_off","madameweb_on","myth_skill_button"
}

for _,v in ipairs(skillimages) do 
	AddAssets(
		Asset("ATLAS", "images/skills/"..v..".xml"),
		Asset("IMAGE", "images/skills/"..v..".tex"))
end

local characters ={
	"monkey_king",
	"neza",
	"white_bone",
	"pigsy",
	"yangjian",
	"myth_yutu",
	"yama_commissioners",
	"madameweb",
}

modimport("main/myth_character_health.lua")
modimport("main/myth_character_language.lua")
local FEMALE_CHARACTERS = {
	white_bone = true,
	myth_yutu = true,
	madameweb = true
}

for k,v in pairs(characters) do 
	AddAssets(
		Asset("ATLAS", "bigportraits/"..v.."_none.xml"),
		Asset("ATLAS", "images/saveslot_portraits/"..v..".xml"),
		Asset("ATLAS", "images/names_"..v..".xml" ),
		Asset("ATLAS", "images/avatars/self_inspect_"..v..".xml" ),
		Asset("ATLAS", "images/avatars/avatar_ghost_"..v..".xml" ),
		Asset("ATLAS", "images/avatars/avatar_"..v..".xml" ),
		Asset("ATLAS", "images/map_icons/"..v..'.xml')
	)
	AddMinimapAtlas("images/map_icons/"..v..".xml")
	AddModCharacter(v, FEMALE_CHARACTERS[v] and 'FEMALE' or 'MALE')

	modimport("main/"..v)
end

-----===选人界面 ui相关
CHARACTER_BUTTON_OFFSET.monkey_king = -41
CHARACTER_BUTTON_OFFSET.neza = -41
CHARACTER_BUTTON_OFFSET.white_bone = -51
CHARACTER_BUTTON_OFFSET.madameweb = -56
local mycharbutton = {
	white_bone = function(self,myhero)
		if myhero then
			if self.head_animstate then
				self.head_animstate:AddOverrideBuild("white_bone_hidehair")
			end
		else
			if self.head_animstate then
				self.head_animstate:ClearOverrideBuild("white_bone_hidehair")
			end			
		end
	end,
}
AddClassPostConstruct("widgets/redux/characterbutton",function(self)
	local old_SetCharacter = self.SetCharacter
	function self:SetCharacter(hero)
		old_SetCharacter(self,hero)
		for k, v in pairs(mycharbutton) do
			v(self,k==hero)
		end
	end
end)

local TEMPLATES = require "widgets/redux/templates"
local old_MakeUIStatusBadge = TEMPLATES.MakeUIStatusBadge
function TEMPLATES.MakeUIStatusBadge(_status_name, c)
	local old = old_MakeUIStatusBadge(_status_name, c)
	if old and old.ChangeCharacter then
		local biubiu = old.ChangeCharacter
		old.ChangeCharacter = function(self, character)
			biubiu(self, character)
			if  _status_name and _status_name == "health"  and  character and character == "yama_commissioners" then
				old.status_icon:SetTexture("images/hud/yama_status_health.xml", "yama_status_health.tex")
			end
		end
	end
	return  old
end
---========================

------------==============小地图图标
local map_icons = {
	"mk_jgb",
	"nz_lance",
	"nz_damask",
	"nz_ring",
	"pigsy_rake",
	"yj_eagle",
	"yangjian_armor",
	"yangjian_hair",
	"skyhound_tooth",
	"skyhound",
	"yj_spear",
	"bone_mirror",
	"hat_commissioner_black",
	"hat_commissioner_white",
	"whip_commissioner",
	"token_commissioner",
	"pennant_commissioner",
	"bell_commissioner",
	"myth_yama_statue1",
	"commissioner_black",
	"guitar_jadehare",
	"myth_bamboo_basket",
	"madameweb_silkcocoon_hanged",
	"madameweb_beemine",
}

for k,v in pairs(map_icons) do
	table.insert(Assets, Asset( "IMAGE", "images/map_icons/"..v..".tex" )) 
    table.insert(Assets, Asset( "ATLAS", "images/map_icons/"..v..".xml" ))
	AddMinimapAtlas("images/map_icons/"..v..'.xml')
end

RemapSoundEvent( "dontstarve/characters/monkey_king/death_voice", "monkey_king/monkey_king/death" )
RemapSoundEvent( "dontstarve/characters/monkey_king/hurt",      "monkey_king/monkey_king/hit" )
RemapSoundEvent( "dontstarve/characters/monkey_king/talk_LP", "monkey_king/monkey_king/talk_loop" )

RemapSoundEvent( "dontstarve/characters/neza/death_voice", "neza_sound/neza_sound/death" )
RemapSoundEvent( "dontstarve/characters/neza/hurt",      "neza_sound/neza_sound/hit" )
RemapSoundEvent( "dontstarve/characters/neza/talk_LP", "neza_sound/neza_sound/talk_loop" )

PREFAB_SKINS["monkey_king"] = {
	"monkey_king_none",
}

PREFAB_SKINS["neza"] = {
	"neza_none",
}

PREFAB_SKINS["white_bone"] = {
	"white_bone_none",
}

PREFAB_SKINS["pigsy"] = {
	"pigsy_none",
}

PREFAB_SKINS["yangjian"] = {
	"yangjian_none",
}

PREFAB_SKINS["myth_yutu"] = {
	"myth_yutu_none",
}

PREFAB_SKINS["yama_commissioners"] = {
	"yama_commissioners_none",
}

PREFAB_SKINS["madameweb"] = {
	"madameweb_none",
}

local MKRECIPE = STRINGS.MKRECIPE and CUSTOM_RECIPETABS[STRINGS.MKRECIPE] or RECIPETABS.REFINE
--[[一柱擎天
local mk_jgb_rec = AddRecipe("mk_jgb_rec", {Ingredient("mk_jgb", 1, "images/monkey_king_item.xml"), Ingredient(CHARACTER_INGREDIENT.SANITY, 30)}, MKRECIPE, TECH.NONE,
nil, nil, nil, nil, "monkey_king",
"images/inventoryimages/mk_jgb_rec.xml",
"mk_jgb_rec.tex")
mk_jgb_rec.sortkey = 1

local mk_dsf =  AddRecipe("mk_dsf", {Ingredient(CHARACTER_INGREDIENT.SANITY, 10)}, MKRECIPE, TECH.LOST,
nil, nil, nil, nil, "monkey_king",
"images/inventoryimages/mk_dsf.xml",
"mk_dsf.tex")
mk_dsf.sortkey = 2]]

--哪吒
AddRecipe("myth_lotus_flower",{Ingredient(CHARACTER_INGREDIENT.HEALTH, 10)}, MKRECIPE,  TECH.NONE,
{buildingstate = "form_log2"}, nil, nil, nil, "neza",
"images/inventoryimages/myth_lotus_flower.xml",
"myth_lotus_flower.tex")

---白骨部分
local WBRECIPE = AddRecipeTab( STRINGS.WBRECIPE, 511, "images/hud/white_bone_tab.xml", "white_bone_tab.tex", "white_bone")
AddRecipe("bone_blade",{Ingredient("boneshard", 4),Ingredient("livinglog", 1),Ingredient("nightmarefuel", 1)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/monkey_king_item.xml",
"bone_blade.tex")

AddRecipe("bone_whip",{Ingredient("boneshard", 4),Ingredient("livinglog", 1),Ingredient("nightmarefuel", 2)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/monkey_king_item.xml",
"bone_whip.tex")

AddRecipe("bone_wand",{Ingredient("boneshard", 4),Ingredient("livinglog", 2),Ingredient("purplegem", 2)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/monkey_king_item.xml",
"bone_wand.tex")

AddRecipe("wb_skeleton",{Ingredient("boneshard", 4)}, WBRECIPE,  TECH.NONE,
"wb_skeleton_placer", nil, nil, nil, "white_bone",
"images/monkey_king_item.xml", 
"wb_skeleton.tex",nil,
"skeleton")

STRINGS.NAMES.WB_SKELETON =  STRINGS.NAMES.SKELETON
STRINGS.RECIPE_DESC.WB_SKELETON = '建造一副合适的身躯'

AddRecipe("wb_heart",{Ingredient("boneshard", 2),Ingredient("spidergland", 2),Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/monkey_king_item.xml",
"wb_heart.tex")

AddRecipe("wb_armorlight",{Ingredient("boneshard", 4),Ingredient("silk", 12)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/wb_armorlight.xml",
"wb_armorlight.tex")

AddRecipe("wb_armorbone",{Ingredient("boneshard", 8),Ingredient("nightmarefuel", 4)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/wb_armorbone.xml",
"wb_armorbone.tex")

AddRecipe("wb_armorblood",{Ingredient("boneshard", 4),Ingredient("mosquitosack", 4)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/wb_armorblood.xml",
"wb_armorblood.tex")

AddRecipe("wb_armorstorage",{Ingredient("boneshard", 4),Ingredient("pigskin", 2)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/wb_armorstorage.xml",
"wb_armorstorage.tex")

AddRecipe("wb_armorgreed",{Ingredient("boneshard", 4),Ingredient("slurper_pelt", 2)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/wb_armorgreed.xml",
"wb_armorgreed.tex")

AddRecipe("bone_mirror",{Ingredient("boneshard", 6),Ingredient("silk", 6),Ingredient("bluegem", 2)}, WBRECIPE,  TECH.NONE,
nil, nil, nil, nil, "white_bone",
"images/inventoryimages/bone_mirror_1.xml",
"bone_mirror_1.tex")

--八戒八戒
AddRecipe("pigsy_sleepbed",{Ingredient("cutgrass", 3)},RECIPETABS.SURVIVAL,  TECH.NONE,
nil, nil, nil, nil, "pigsy",
"images/inventoryimages/pigsy_sleepbed.xml",
"pigsy_sleepbed.tex")

local pigsy_hat = AddRecipe("pigsy_hat",{Ingredient("silk", 3),Ingredient("goldnugget", 1)},MKRECIPE,  TECH.NONE,
nil, nil, nil, nil, "pigsy",
"images/inventoryimages/pigsy_hat.xml",
"pigsy_hat.tex")
pigsy_hat.sortkey = 1

--[[AddRecipe("yangjian_track",{Ingredient(CHARACTER_INGREDIENT.SANITY, 25)},MKRECIPE,  TECH.LOST,
{buildingstate = "myth_sg_pre"}, nil, nil, nil, "yangjian",
"images/inventoryimages/yangjian_track.xml",
"yangjian_track.tex")

AddRecipe("yangjian_buzzard_spawn",{Ingredient("smallmeat",1)},MKRECIPE,  TECH.NONE,
{buildingstate = "myth_sg_pre"}, nil, nil, nil, "yangjian",
"images/inventoryimages/yangjian_buzzard_spawn.xml",
"yangjian_buzzard_spawn.tex")]]

STRINGS.ACTIONS.CASTAOE.PIGSY_RAKE = STRINGS.ACTIONS.CASTAOE.LAVAARENA_HEAVYBLADE


--------------------------------------------------------------------------
--[[ 玉兔重做tip：玉兔相关科技 ]]
--------------------------------------------------------------------------

--神奇的官方竟然没有消耗饥饿制作的配方
CHARACTER_INGREDIENT.MYTH_HUNGER = "myth_hunger"
STRINGS.NAMES["MYTH_HUNGER"] = STRINGS.NAMES.HUNGER

--鹿角的prefab有三种，将三种鹿角的判断合在一个解锁方式里
CHARACTER_INGREDIENT.MYTH_DEER_ANTLER = "myth_deer_antler"
STRINGS.NAMES["MYTH_DEER_ANTLER"] = STRINGS.NAMES.DEER_ANTLER

CHARACTER_INGREDIENT.MYTH_MADAMEWEB = "myth_madameweb"
STRINGS.NAMES["MYTH_MADAMEWEB"] = "蛛丝值"


local MYTH_CHARACTER_INGREDIENT = {
	myth_hunger = true,
	myth_deer_antler = true,
	myth_madameweb = true,
}

local oldIsCharacterIngredient = GLOBAL.IsCharacterIngredient
if oldIsCharacterIngredient ~= nil then
	GLOBAL.IsCharacterIngredient =  function(ingredienttype)
		if ingredienttype and MYTH_CHARACTER_INGREDIENT[ingredienttype] then
			return true
		end
		return oldIsCharacterIngredient(ingredienttype)
	end
end

------蟾宫技艺栏
local tab_jadehare = AddRecipeTab(STRINGS.TAB_JADEHARE, 999, "images/hud/jadehare_tab.xml", "jadehare_tab.tex", "myth_yutu", false)

--[[----玉兔捣药
AddRecipe(
	"yt_daoyao", {
		Ingredient(CHARACTER_INGREDIENT.MYTH_HUNGER, 5, "images/inventoryimages/myth_hunger.xml")
	}, tab_jadehare, TECH.NONE, {buildingstate = "myth_sg_pre"}, nil, nil, nil, nil,
	"images/inventoryimages/yt_daoyao.xml", "yt_daoyao.tex"
)]]


--yt_daoyao.sortkey  = 1 --不需要啦

------玉兔曲子
GLOBAL.MYTH_YUTU_SONGS = {}

local songs = require("guitar_songs_myth")
for k, v in pairs(songs) do
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/"..v.name..".xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/"..v.name..".tex"))
	table.insert(MYTH_YUTU_SONGS,v.name)
	AddRecipe(
		v.name, {
			v.cost_hunger and  Ingredient(CHARACTER_INGREDIENT.MYTH_HUNGER, 5, "images/inventoryimages/myth_hunger.xml") or Ingredient(CHARACTER_INGREDIENT.SANITY, 5)
		}, tab_jadehare, TECH.LOST, {buildingstate = "myth_sg_pre"}, nil, nil, nil, nil,
		"images/inventoryimages/"..v.name..".xml", v.name..".tex"
	)
end
songs = nil

------玉兔药粉
AddRecipe(
	"powder_m_takeiteasy", {
		Ingredient("forgetmelots", 6),
		Ingredient("cactus_meat", 3)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 6, nil,
	"images/inventoryimages/powder_m_takeiteasy.xml", "powder_m_takeiteasy.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_takeiteasy")

AddRecipe(
	"powder_m_becomestar", {
		Ingredient("wormlight", 3),
		Ingredient("lightbulb", 6),
		Ingredient("goldnugget", 1)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 6, nil,
	"images/inventoryimages/powder_m_becomestar.xml", "powder_m_becomestar.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_becomestar")

AddRecipe(
	"powder_m_charged", {
		Ingredient("lightninggoathorn", 2),
		Ingredient("feather_canary", 1)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 3, nil,
	"images/inventoryimages/powder_m_charged.xml", "powder_m_charged.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_charged")
AddRecipe(
	"powder_m_improvehealth", {
		Ingredient("royal_jelly", 2),
		Ingredient("spidergland", 6),
		Ingredient("kelp", 1)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 3, nil,
	"images/inventoryimages/powder_m_improvehealth.xml", "powder_m_improvehealth.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_improvehealth")

AddRecipe(
	"powder_m_lifeelixir", {
		Ingredient("minotaurhorn", 1)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 3, nil,
	"images/inventoryimages/powder_m_lifeelixir.xml", "powder_m_lifeelixir.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_lifeelixir")
AddRecipe(
	"powder_m_coldeye", {
		Ingredient("deerclops_eyeball", 1),
		Ingredient(CHARACTER_INGREDIENT.MYTH_DEER_ANTLER, 1, "images/inventoryimages1.xml", nil, "deer_antler1.tex"), --用鹿角1号的图
		Ingredient("beefalowool", 1)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 3, nil,
	"images/inventoryimages/powder_m_coldeye.xml", "powder_m_coldeye.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_coldeye")
AddRecipe(
	"powder_m_hypnoticherb", {
		Ingredient("mandrake", 1),
		Ingredient("petals", 3)
	}, tab_jadehare, TECH.LOST, {no_deconstruction = true}, nil, nil, 3, nil,
	"images/inventoryimages/powder_m_hypnoticherb.xml", "powder_m_hypnoticherb.tex"
)
table.insert(MYTH_YUTU_SONGS,"powder_m_hypnoticherb")

------玉兔土遁
AddRecipe(
	"yt_rabbithole", {
		Ingredient("rabbit", 2),
		Ingredient("shovel", 1)
	}, tab_jadehare, TECH.SCIENCE_ONE, {placer = "yt_rabbithole_placer" , buildingstate = "myth_yutu_dig_start"} , 3, nil, nil, nil,
	"images/inventoryimages/yt_rabbithole.xml", "yt_rabbithole.tex", nil, "rabbithole"
)

------玉兔月蛾
AddRecipe(
	"yt_moonbutterfly", {
		Ingredient("petals", 1)
	}, tab_jadehare, TECH.NONE, {buildingstate = "summon_butterfly"}, nil, nil, nil, nil,
	nil, "moonbutterfly.tex"
)

--竹药娄
AddRecipe(
	"myth_bamboo_basket", {
		Ingredient("myth_bamboo", 3),
		Ingredient("rope", 2)
	}, tab_jadehare, TECH.SCIENCE_ONE, nil, nil, nil, nil, nil,
	"images/inventoryimages/myth_bamboo_basket.xml", "myth_bamboo_basket.tex"
)

--琵琶
AddRecipe(
	"guitar_jadehare", {
		Ingredient("moonglass", 4),
		Ingredient("driftwood_log", 1),
		Ingredient("silk", 1)
	}, tab_jadehare, TECH.NONE, nil, nil, nil, nil, nil,
	"images/inventoryimages/guitar_jadehare.xml", "guitar_jadehare.tex"
)

--黑白无常篇
local YAMARECIPE = AddRecipeTab( STRINGS.YAMARECIPE, 512, "images/hud/yama_tab.xml", "yama_tab.tex", "yama_commissioners")
AddRecipe("myth_yama_statue1",{Ingredient("cutstone", 3)},YAMARECIPE, TECH.NONE,
"myth_yama_statue1_placer", nil, nil, nil, "yama_commissioners",
"images/inventoryimages/myth_yama_statue1.xml",
"myth_yama_statue1.tex")

AddRecipe("myth_higanbana_item",{Ingredient("petals_evil", 12),Ingredient("butterfly", 1),Ingredient("butterflywings", 1)},YAMARECIPE, TECH.LOST,
nil, nil, nil, nil, "yama_commissioners",
"images/inventoryimages/myth_higanbana_item.xml",
"myth_higanbana_item.tex")

AddRecipe("myth_cqf",{Ingredient("papyrus", 1)},YAMARECIPE, TECH.LOST,
nil, nil, nil, nil, "yama_commissioners",
"images/inventoryimages/myth_cqf.xml",
"myth_cqf.tex")

--[[AddRecipe("myth_bahy",{Ingredient(CHARACTER_INGREDIENT.SANITY, 5)},YAMARECIPE, TECH.LOST,
nil, nil, nil, nil, "yama_commissioners",
"images/inventoryimages/myth_bahy.xml",
"myth_bahy.tex")]]


--盘丝
local MADAMEWEBTAB = AddRecipeTab( "盘丝", 513, "images/hud/mada_tab.xml", "mada_tab.tex", "madameweb")
--蛛丝
AddRecipe("madameweb_silk",{Ingredient(CHARACTER_INGREDIENT.MYTH_MADAMEWEB, 3,"images/inventoryimages/myth_madameweb.xml")},MADAMEWEBTAB, TECH.NONE,
{no_deconstruction = true}, nil, nil, nil, nil,
nil,nil,nil, "silk")
STRINGS.NAMES.MADAMEWEB_SILK = STRINGS.NAMES.SILK
--蜘蛛巢
AddRecipe("madameweb_spidereggsack",{Ingredient("silk", 12),Ingredient("spidergland", 6)},MADAMEWEBTAB, TECH.NONE,
{no_deconstruction = true}, nil, nil, nil, nil,
nil,nil,nil, "spidereggsack")
STRINGS.NAMES.MYTH_SPIDEREGGSACK = STRINGS.NAMES.SPIDEREGGSACK

--蛛丝罗网
AddRecipe("madameweb_net",{Ingredient("spidereggsack", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_net.xml","madameweb_net.tex")

AddRecipe("madameweb_pitfall",{Ingredient("silk", 3),Ingredient("spidergland", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_pitfall.xml","madameweb_pitfall.tex")

--盘丝蜂针
AddRecipe("madameweb_stinger",{Ingredient("silk", 1),Ingredient("stinger", 3)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, 3, nil,
"images/inventoryimages/madameweb_stinger.xml","madameweb_stinger.tex")
--盘丝毒针
AddRecipe("madameweb_poisonstinger",{Ingredient("madameweb_stinger", 3,"images/inventoryimages/madameweb_stinger.xml"),Ingredient("petals", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, 3, nil,
"images/inventoryimages/madameweb_poisonstinger.xml","madameweb_poisonstinger.tex")
--毒蜂茧
AddRecipe("madameweb_beemine",{Ingredient("silk", 3),Ingredient("bee", 4),Ingredient("petals", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_beemine.xml","madameweb_beemine.tex")
--盘丝蛛卵
AddRecipe("madameweb_poisonbeemine",{Ingredient("spidereggsack", 1),Ingredient("spidergland", 2),Ingredient("petals", 6)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_poisonbeemine.xml","madameweb_poisonbeemine.tex")

--盘丝披风
AddRecipe("madameweb_armor",{Ingredient("spiderhat", 1),Ingredient("silk", 12),Ingredient("tentaclespots", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_armor.xml","madameweb_armor.tex")

--蛛丝茧袋
AddRecipe("madameweb_silkcocoon",{Ingredient("silk", 6),Ingredient("petals", 2)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_silkcocoon.xml","madameweb_silkcocoon.tex")

AddRecipe("hua_internet_node_item", {Ingredient("goldnugget", 1), Ingredient("stinger", 1), Ingredient("silk", 4)},
MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/hua_internet_node_item.xml")

AddRecipe("hua_internet_node_sea_item", {Ingredient("hua_internet_node_item", 1), Ingredient("spider_water", 1)},
MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/hua_internet_node_sea_item.xml")

AddRecipe("hua_fake_spider_shoe", {Ingredient("petals", 6), Ingredient("silk", 12)},
MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/hua_fake_spider_shoe.xml")

--吊丝
AddRecipe("madameweb_feisheng",{Ingredient(CHARACTER_INGREDIENT.MYTH_MADAMEWEB, 10,"images/inventoryimages/myth_madameweb.xml")},MADAMEWEBTAB, TECH.NONE,
{buildingstate = "myth_sg_pre"}, nil, nil, nil, nil,
"images/inventoryimages/madameweb_feisheng.xml","madameweb_feisheng.tex")

AddRecipe("madameweb_detoxify",{Ingredient("spidergland", 1)},MADAMEWEBTAB, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/madameweb_detoxify.xml","madameweb_detoxify.tex")

--皮肤
modimport "skin.lua"

local other = {
	"myth_character_containers",
	"skyhound",
	"myth_rpc",
	"registerinventoryitematlas",
	"player_become_astral",
	"hua_internet",
	"myth_skill",
}
for i, v in ipairs(other) do
	modimport("main/"..v)
end

--提示订阅
-- AddPlayerPostInit(function(inst)
-- 	inst:DoTaskInTime(1, function(inst)
-- 		if inst == ThePlayer and rawget(GLOBAL, 'MK_MOD_LANGUAGE_SETTING') == nil then
-- 			local tp = require "widgets/redux/templates"
-- 			local sc = require "widgets/screen"
-- 			local w = Class(sc, function(self)
-- 				sc._ctor(self, "")
-- 				self:SetHAnchor(ANCHOR_MIDDLE)
--     			self:SetVAnchor(ANCHOR_MIDDLE)
-- 				local main = self:AddChild(tp.RectangleWindow(500, 250, "Reminder-提示", {
-- 					{text="subscribe-订阅", cb= function() VisitURL("https://steamcommunity.com/sharedfiles/filedetails/?id=1991746508") end},
-- 					{text="OK-我知道了", cb= function() TheFrontEnd:PopScreen() end},
-- 				}, 300, 
-- 				"Myth words mod has been split into two parts.\nClick subscribe button below to get more!\n"..
-- 				"为便于维护, 神话书说模组已分成了角色和主题两部分,\n关于炼丹炉、黑风大王和犀牛兄弟等内容, 请点击下方订阅!\n如遇物品丢失情况, 请开启主题mod, 然后回档!")
-- 				)
-- 			end)
-- 			TheFrontEnd:PushScreen(w())
-- 		end
-- 	end)
-- end)

AddSimPostInit(function()
	if rawget(GLOBAL, "Myth_RunCachedStr") then
		GLOBAL.Myth_RunCachedStr()
	end
end)