



local function onuse(inst,doer)
    if not inst.components.health:IsDead() then
        if doer.components.player_astral then
            doer.components.player_astral:SetPlayer(inst)
        end
        return true
    end
end


local function canuseinscene_myth(inst,doer)
    if doer and doer._is_player_astral and doer._is_player_astral:value() and not doer:HasTag("has_astral_player") then
        if not inst._is_player_astral:value() then
            return true
        end
    end
    return false
end

AddPlayerPostInit(function(inst)

    inst._is_player_astral = net_bool(inst.GUID, "myth.player_astral", "player_astraldirty")
    inst._is_player_astral_player = net_entity(inst.GUID, "myth.player_astral_player", "player_astral_playerdirty")
    inst._is_be_ghosted = net_bool(inst.GUID, "myth.player_be_ghosted", "player_ghosteddirty")

    inst.canuseinscene_myth = canuseinscene_myth
    inst.MYTH_USE_TYPE = "PLAYER"

	if TheWorld.ismastersim then

        inst._is_be_ghosted_players = {}
		inst:AddComponent("player_astral")

        inst:AddComponent("myth_use_inventory")
        inst.components.myth_use_inventory.canuse = false
        inst.components.myth_use_inventory:SetOnUseFn(onuse)
	end
end)

AddComponentPostInit("health", function(self)
	local oldDoDelta = self.DoDelta
	function self:DoDelta(amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
        if self.setastraltredirect ~= nil and self.setastraltredirect(self.inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb) then
            return 0 
        end
		return oldDoDelta(self,amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
	end
end)

AddComponentPostInit("temperature", function(self)
	local oldOnUpdate = self.OnUpdate
	function self:OnUpdate(...)
        if self.setastraltemp ~= nil then
            return
        end
		return oldOnUpdate(self,...)
	end
end)


AddComponentPostInit("freezable", function(self)
	local oldAddColdness = self.AddColdness
	function self:AddColdness(...)
        if self.inst._is_player_astral ~= nil and self.inst._is_player_astral:value()  then
            return
        end
		return oldAddColdness(self,...)
	end
end)

AddClassPostConstruct( "components/sanity_replica", function(self, inst)
	local old_IsGhostDrain = self.IsGhostDrain
	function self:IsGhostDrain(...)
		if self.inst._is_be_ghosted ~= nil and self.inst._is_be_ghosted:value() then
			return true
		end
		return old_IsGhostDrain(self,...)
	end
end)

local function Addwhite_boneui(self) 
    self.owner:ListenForEvent("player_astraldirty", function(inst)
        if  self.owner._is_player_astral:value() then
            self.stomach:Hide()
            self.heart:Hide()
        elseif not self.isghostmode then
            self.stomach:Show()
            self.heart:Show()
        end
    end,self.owner)
end
AddClassPostConstruct("widgets/statusdisplays", Addwhite_boneui)

AddStategraphPostInit('wilson',function(self)
	local funnyidle = self.states.funnyidle 
	if funnyidle then
		local old_enter = funnyidle.onenter
		function funnyidle.onenter(inst, ...)
			if old_enter then 
				old_enter(inst, ...)
			end
			if  inst._is_be_ghosted:value() and math.random() < 0.5 then
				if not inst.AnimState:IsCurrentAnimation("idle_inaction_sanity") then
					inst.AnimState:PlayAnimation("idle_inaction_sanity")
				end
			end
		end
	end
end)

--[[
AAAAA = SpawnPrefab("wilson")
 AAAAA.components.player_astral:SetPlayer(ThePlayer)

 AAAAA.components.player_astral:StopPlayer()

 ThePlayer.components.sanity._lunacy_sources:SetModifier("abc", true)
]]