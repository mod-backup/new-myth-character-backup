local assets =
{
	Asset( "ANIM", "anim/yama_commissioners.zip" ),
	Asset( "ANIM", "anim/yama_commissioners_black.zip" ),
	Asset( "ANIM", "anim/yama_status_health.zip" ),
	Asset( "ANIM", "anim/yama_status_soul.zip" ),
	Asset("ANIM", "anim/switch_role_myth.zip"),
	Asset( "ANIM", "anim/ghost_commissioners_build.zip" ),
}

local skins =
{
	normal_skin = "yama_commissioners",
	ghost_skin = "ghost_commissioners_build",
}

local base_prefab = "yama_commissioners"

local tags = {"yama_commissioners", "CHARACTER"}

return CreatePrefabSkin("yama_commissioners_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	build_name_override = "yama_commissioners",
	rarity = "Character",
})