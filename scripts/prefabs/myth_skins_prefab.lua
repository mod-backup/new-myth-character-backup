local assets = {}
for _,v in ipairs{
	"monkey_king_sea", "monkey_king_fire", "monkey_king_horse", "monkey_king_opera","monkey_king_ear","monkey_king_wine",
	"neza_green", "neza_fire","neza_pink","neza_rap",
	"white_bone_white", "white_bone_opera","white_bone_queen",
	"pigsy_marry", "pigsy_white", "pigsy_constellation",
	"yangjian_black", "yangjian_dao", "yangjian_gold","yangjian_hawk",
	"myth_yutu_frog", "myth_yutu_winter","myth_yutu_apricot","myth_yutu_laurel","myth_yutu_soprano",
	"yama_commissioners_lotus",
}do
	table.insert(assets, Asset("ATLAS", "bigportraits/"..v..".xml"))
end

local Skin = Mythwords_AddSkin
--(prefab, skin, skins, tags)

local MK_TAG = {"BASE","MONKEY_KING", "CHARACTER"}
local NZ_TAG = {"BASE","NEZA", "CHARACTER"}
local WB_TAG = {"BASE","WHITE_BONE", "CHARACTER"}
local PG_TAG = {"BASE", "PIGSY", "CHARACTER"}
local YJ_TAG = {"BASE", "YANGJIAN", "CHARACTER"}
local YT_TAG = {"BASE", "MYTH_YUTU", "CHARACTER"}
local YA_TAG = {"BASE", "YAMA_COMMISSIONERS", "CHARACTER"}
return 
	Skin('monkey_king', 'none', {normal_skin = 'monkey_king', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'sea', {normal_skin = 'monkey_king_sea', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'horse', {normal_skin = 'monkey_king_horse', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'fire', {normal_skin = 'monkey_king_fire', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'opera', {normal_skin = 'monkey_king_opera', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'ear', {normal_skin = 'monkey_king_ear', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	Skin('monkey_king', 'wine', {normal_skin = 'monkey_king_wine', ghost_skin = 'ghost_monkey_king_build'}, MK_TAG),
	
	Skin('neza', 'none', {normal_skin = 'neza', ghost_skin = 'ghost_neza_build'}, NZ_TAG),
	Skin('neza', 'green', {normal_skin = 'neza_green', ghost_skin = 'ghost_neza_build'}, NZ_TAG),
	Skin('neza', 'fire', {normal_skin = 'neza_fire', ghost_skin = 'ghost_neza_build'}, NZ_TAG),
	Skin('neza', 'pink', {normal_skin = 'neza_pink', ghost_skin = 'ghost_neza_build'}, NZ_TAG),
	Skin('neza', 'rap', {normal_skin = 'neza_rap', ghost_skin = 'ghost_neza_build'}, NZ_TAG),

	Skin('white_bone', 'none', {normal_skin = 'white_bone', ghost_skin = 'ghost_white_bone_build'}, WB_TAG),
	Skin('white_bone', 'white', {normal_skin = 'white_bone_white', ghost_skin = 'ghost_white_bone_build'}, WB_TAG),
	Skin('white_bone', 'opera', {normal_skin = 'white_bone_opera', ghost_skin = 'ghost_white_bone_build'}, WB_TAG),
	Skin('white_bone', 'queen', {normal_skin = 'white_bone_queen', ghost_skin = 'ghost_white_bone_build'}, WB_TAG),

	Skin('pigsy', 'none', {normal_skin = "pigsy", ghost_skin = 'ghost_pigsy_build'}, PG_TAG),
	Skin('pigsy', 'marry', {normal_skin = 'pigsy_marry', ghost_skin = 'ghost_pigsy_build'}, PG_TAG),
	Skin('pigsy', 'white', {normal_skin = 'pigsy_white', ghost_skin = 'ghost_pigsy_build'}, PG_TAG),
	Skin('pigsy', 'constellation', {normal_skin = 'pigsy_constellation', ghost_skin = 'ghost_pigsy_build'}, PG_TAG),

	Skin('yangjian', 'none', {normal_skin = "yangjian", ghost_skin = "ghost_yangjian_build"}, YJ_TAG),
	Skin('yangjian', 'black', {normal_skin = 'yangjian_black', ghost_skin = "ghost_yangjian_build"}, YJ_TAG),
	Skin('yangjian', 'dao', {normal_skin = 'yangjian_dao', ghost_skin = "ghost_yangjian_build"}, YJ_TAG),
	Skin('yangjian', 'gold', {normal_skin = 'yangjian_gold', ghost_skin = 'ghost_yangjian_build'}, YJ_TAG),
	Skin('yangjian', 'hawk', {normal_skin = 'yangjian_hawk', ghost_skin = 'ghost_yangjian_build'}, YJ_TAG),

	Skin('myth_yutu', 'none', {normal_skin = "myth_yutu", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),
	Skin('myth_yutu', 'frog', {normal_skin = "myth_yutu_frog", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),
	Skin('myth_yutu', 'winter', {normal_skin = "myth_yutu_winter", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),
	Skin('myth_yutu', 'apricot', {normal_skin = "myth_yutu_apricot", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),
	Skin('myth_yutu', 'laurel', {normal_skin = "myth_yutu_laurel", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),
	Skin('myth_yutu', 'soprano', {normal_skin = "myth_yutu_soprano", ghost_skin = "ghost_myth_yutu_build"}, YT_TAG),

	Skin('yama_commissioners', 'none', {normal_skin = "yama_commissioners", ghost_skin = "ghost_yama_commissioners_build"}, YA_TAG),
	Skin('yama_commissioners', 'lotus', {normal_skin = "yama_commissioners_lotus", ghost_skin = "ghost_yama_commissioners_build"}, YA_TAG),

	Prefab('myth_skins_prefab', function() return CreateEntity() end, assets)