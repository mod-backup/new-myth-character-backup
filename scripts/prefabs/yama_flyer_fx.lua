

local firefx = {
    white = "yama_fire_green",
    black = "yama_fire_purple",
}

local radius = 1
local function UpdateDeerOffsets(inst)
	local pt = inst:GetPosition()
	local theta = inst.theta
	inst.theta = inst.theta + .1
	local num = 3
	for _,v in pairs(inst.fires) do
		local offset = Vector3(radius * math.cos(theta), 1, -radius * math.sin(theta))
		v.formationpos =  offset
		v.Transform:SetPosition(v.formationpos:Get())
		theta = theta - (2*PI/num)
	end
end

local function SpawnDeer(inst)
    --local pos = inst:GetPosition()
    for k = 0 , 240, 120 do 
        local deer =  inst:SpawnChild(inst.fire_name)  -- SpawnPrefab(inst.fire_name)
        deer.hudu = k
        deer._light.Light:Enable(false)
        deer.Transform:SetPosition(radius * math.cos(math.rad(deer.hudu)), 1,radius * math.sin(math.rad(deer.hudu)))
        table.insert(inst.fires,deer)
    end
end

local function setowner(inst,owner)
    inst.owner = owner
    inst.build = owner.components.yama_transform:GetOtherState()
    inst.mini_mapplayer:set(inst.build)
    inst.fire_name = firefx[inst.build]
    inst.AnimState:SetBuild("commissioner_"..inst.build)

    inst.AnimState:OverrideSymbol("headbase", "hat_commissioner_"..inst.build, "headbase")
    inst.AnimState:OverrideSymbol("SWAP_FACE", "hat_commissioner_"..inst.build, "SWAP_FACE")

    owner.myth_flyer_fxs[inst.build] = inst
    inst:ListenForEvent("onremove",function()
        if inst.owner and inst.owner:IsValid() and inst.owner.myth_flyer_fxs then
            inst.owner.myth_flyer_fxs[inst.build] = nil
        end
    end)

    inst:ListenForEvent("onremove",function()
        inst:Remove()
    end,inst.owner)

    inst.theta = 0
	inst.fires = {}
	inst:DoTaskInTime(0,SpawnDeer)
	inst:DoPeriodicTask(0, UpdateDeerOffsets)   
end

local map = {
    black = "commissioner_black",
    white = "yama_commissioners",
}

local function onname(inst)
    inst.MiniMapEntity:SetIcon( (map[inst.mini_mapplayer:value()] or "yama_commissioner"  )..".tex" )
end

local function MakeMinion(prefab)
    local assets =
    {
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        --MakeGhostPhysics(inst, 1, 0.5)

        inst.Transform:SetFourFaced(inst)

        inst.AnimState:SetBank("wilson")
        inst.AnimState:PlayAnimation("idle_loop",true)
        inst.AnimState:SetMultColour(.5, .5, .5, .5)

        inst.AnimState:Hide("ARM_carry")

        inst.mini_mapplayer = net_string(inst.GUID, "yama_flyer_fx.entity","mapdirty")

        inst:ListenForEvent("mapdirty", onname)

        --inst.AnimState:Hide("HAT")
        --inst.AnimState:Hide("HAIR_HAT")

        inst.AnimState:Show("HAT")
        inst.AnimState:Show("HAIR_HAT")
        inst.AnimState:Hide("HAIR_NOHAT")
        inst.AnimState:Hide("HAIR")
    
        inst.AnimState:Show("HEAD")
        inst.AnimState:Hide("HEAD_HAT")

        inst:AddTag("scarytoprey")
        inst:AddTag("fx")
        inst:AddTag("NOBLOCK")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.SetOwer = setowner

        return inst
    end

    return Prefab(prefab, fn, assets)
end

return MakeMinion("yama_flyer_fx")
