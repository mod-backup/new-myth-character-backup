require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/wilsonstatue.zip"),
    Asset("MINIMAP_IMAGE", "resurrect"),
}

local prefabs =
{

}

local onuse

local function setowner(inst,owner)
    inst.player = owner

    inst.components.hunger.max = owner.components.hunger.max
    inst.components.health.maxhealth = owner.components.health.maxhealth

    inst.components.health:SetCurrentHealth(owner.components.health.currenthealth)
	inst.components.hunger.current = owner.components.hunger.current

	inst.components.hunger:DoDelta(0)
	inst.components.health:DoDelta(0)  

    --dumptable(owner.components.skinner)
    --print(owner.AnimState:GetBuild())

    local build = table.contains(DST_CHARACTERLIST,owner.prefab) and owner.prefab or owner.AnimState:GetBuild()

    inst.AnimState:SetBuild(build)

    inst.astral_player:set(owner)

    inst:ListenForEvent("death",inst.my_death) 

    inst:ListenForEvent("death",inst.player_death,inst.player)

    inst:ListenForEvent("sanitydelta",inst.sanitydelta,inst.player)
end

local function oneat(inst, data)
    local feeder  = data and data.feeder or nil
    if data and data.food then 
        if data.food.components.edible.sanityvalue >= 0  and inst.player ~= nil   then
            inst.player.components.sanity:DoDelta(data.food.components.edible.sanityvalue)
        end
        if data.food.prefab == "myth_huanhundan" then
            if inst.player ~= nil and  inst.player:IsValid()  then
                inst.player:SnapCamera()
                inst.player:ScreenFade(true, 0.5)                
                onuse(inst,inst.player)
            end
        end
    end
end

local function onname(inst)
    if inst.astral_player:value()  ~= nil then
        inst:SetPrefabNameOverride(inst.astral_player:value().prefab)
    end
end

local function canuseinscene_myth(inst,doer)
    return doer and doer == inst.astral_player:value()
end

onuse = function(inst,doer,data)
    if doer and doer == inst.player and not doer.components.health:IsDead()  then
        inst:RemoveEventCallback("sanitydelta",inst.sanitydelta,inst.player)
        inst:RemoveEventCallback("death",inst.player_death,inst.player)
        inst:RemoveEventCallback("death",inst.my_death)
        if data then
            if not doer.components.health:IsDead() then
                doer:SnapCamera()
                doer:ScreenFade(true, 0.5) 
                local x, y, z = inst.Transform:GetWorldPosition()
                doer.Transform:SetPosition(x, y, z)
                if doer.components.player_astral then
                    doer.components.player_astral:SetAstral(false)
                end   
                doer.components.health:SetVal(0, data.cause, data.afflicter)
            end 
            inst:Remove()
        else
            local x, y, z = inst.Transform:GetWorldPosition()
            local diefx = SpawnPrefab("lavaarena_player_revive_from_corpse_fx")
            diefx.Transform:SetPosition(x, y, z)

            local hunger_percent = inst.components.hunger:GetPercent()
            local health_percent = inst.components.health:GetPercent()     
            doer.components.hunger:SetPercent(hunger_percent)
            doer.components.health:SetPercent(health_percent)
            doer:Hide()
            doer.Transform:SetPosition(x, y, z)
            doer.components.health:SetInvincible(true)
            if doer.components.playercontroller ~= nil then
                doer.components.playercontroller:Enable(false)
            end
            if doer.components.talker ~= nil then
                doer.components.talker:ShutUp()
            end
    
            doer.sg:AddStateTag("busy")

            doer:DoTaskInTime(2.6,function()
                inst:Remove()
                if doer.components.player_astral then
                    doer.components.player_astral:SetAstral(false)
                end            
                doer.sg:GoToState("white_bone_rebirth", inst)
                doer:Show()
            end)
            inst.player = nil
            return true
        end
    elseif doer.replica.inventory and doer.replica.inventory:EquipHasTag("pennant_commissioner") then
        if inst.player ~= nil and  inst.player:IsValid() then
            inst.player:SnapCamera()
            inst.player:ScreenFade(true, 0.5)                
            onuse(inst,inst.player)
        end      
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, .3)

    inst.MiniMapEntity:SetIcon("resurrect.png")

    --inst:AddTag("structure")
    inst:AddTag("handfed")
    inst:AddTag("fedbyall")
    inst:AddTag("companion")
    inst:AddTag("character")

    inst.canuseinscene_myth = canuseinscene_myth
    inst.MYTH_USE_TYPE = "ASTRAL"
    inst.onusesgname = "doshortaction"

    inst.AnimState:SetBank("wilson")
    inst.AnimState:SetBuild("wilson")
    inst.AnimState:PlayAnimation("death2")

    inst.astral_player = net_entity(inst.GUID, "yama_resurrectionstatue.entity","namedirty")

    inst:ListenForEvent("namedirty", onname)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")

    inst:AddComponent("eater")

    inst:AddComponent("hunger")
    inst.components.hunger.hungerrate = 0.5

    inst:AddComponent("health")

    inst.SetOwner = setowner

    inst:ListenForEvent("oneat",oneat)

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
    inst.components.myth_use_inventory:SetOnUseFn(onuse)

    inst.sanitydelta = function(player,data)
        local newpercent = data and  data.newpercent or 1
        if newpercent <= 0  then
            if inst.player and inst.player:IsValid() and  inst.player.components.health then
                onuse(inst,inst.player) 
            end
        end
    end

    inst.player_death  = function(player,data)
        if inst.player and inst.player:IsValid() then
            onuse(inst,inst.player)
        end
    end
    inst.my_death = function(my,data)
        if inst.player and inst.player:IsValid() and not inst.player.components.health:IsDead() then
            onuse(inst,inst.player,data)
        end
    end

    inst:AddComponent("combat")

    return inst
end

return Prefab("yama_resurrectionstatue", fn, assets, prefabs)
