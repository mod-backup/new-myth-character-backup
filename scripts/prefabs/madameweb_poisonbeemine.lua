require "prefabutil"

local PushAnimations = {
    "grow_sac_to_small","grow_small_to_medium","grow_medium_to_large","cocoon_large",
}
local function ondeploy(inst, pt, deployer, rot)
    local structure = SpawnPrefab("madameweb_spiderhome")
    if structure ~= nil then
        structure.Transform:SetPosition(pt:Get())
        for _,v in ipairs(PushAnimations) do
            structure.AnimState:PushAnimation(v)
        end
        if inst.components.stackable then
            inst.components.stackable:Get():Remove()
        else
            inst:Remove()
        end
    end
end

local function BeeMine(name, alignment, skin, spawnprefab, isinventory)
    local assets =
    {
        Asset("ANIM", "anim/"..skin..".zip"),
        Asset("ATLAS", "images/inventoryimages/"..skin..".xml"),
        Asset("SOUND", "sound/bee.fsb"),
    }
    if name ~= "beemine" then
        table.insert(assets, Asset("MINIMAP_IMAGE", "beemine"))
    end

    local prefabs =
    {
        spawnprefab,
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.Transform:SetScale(0.8, 0.8, 0.8)

        inst.AnimState:SetBank("bee_mine")
        inst.AnimState:SetBuild(skin)
        inst.AnimState:PlayAnimation("idle")

        inst:AddTag("silkcontaineritem")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end


        inst:AddComponent("inspectable")
        inst:AddComponent("lootdropper")

        inst:AddComponent("stackable")
        --inst.components.stackable.maxsize = stack_size

        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem:SetSinks(true)
        inst.components.inventoryitem.atlasname = "images/inventoryimages/"..name..".xml"

        inst:AddComponent("deployable")
        inst.components.deployable.ondeploy = ondeploy
        inst.components.deployable:SetDeploySpacing(DEPLOYSPACING.PLACER_DEFAULT)

        MakeHauntableLaunch(inst)

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

local function posfn(inst)
    inst.AnimState:HideSymbol("bedazzled_flare")
    inst.Transform:SetScale(0.8, 0.8, 0.8)
end

return --BeeMine("madameweb_beemine", "player", "madameweb_beemine", "madameweb_bee", true),
    BeeMine("madameweb_poisonbeemine", "player", "madameweb_poisonbeemine", "madameweb_spider_warrior", true),
    --MakePlacer("madameweb_beemine_placer", "bee_mine", "madameweb_beemine", "idle")
    MakePlacer("madameweb_poisonbeemine_placer", "spider_cocoon", "madameweb_pitfall_cocoon", "cocoon_large",nil,nil,nil,nil,nil,nil,posfn)

--return MakeDeployableKitItem("madameweb_poisonbeemine", "twigs", bank, build, anim, assets, floatable_data, tags, burnable, deployable_data, stack_size)
