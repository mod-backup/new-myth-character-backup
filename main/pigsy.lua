
--quickpick
local function quickpick(sg)
    local old_harvest = sg.actionhandlers[ACTIONS.HARVEST].deststate
    sg.actionhandlers[ACTIONS.HARVEST].deststate = function(inst, action)
        if inst:HasTag("pigsy") and inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil and inst.replica.hunger:GetCurrent() >= 50 then
			return "doshortaction"
		end
		return old_harvest(inst, action)
    end

    local old_interact = sg.actionhandlers[ACTIONS.INTERACT_WITH].deststate
    sg.actionhandlers[ACTIONS.INTERACT_WITH].deststate = function(inst, action)
        if inst:HasTag("pigsy") and inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil and inst.replica.hunger:GetCurrent() >= 50 then
			return "doshortaction"
		end
		return old_interact(inst, action)
    end
end

local function quickpick_client(sg)
    local old_harvest = sg.actionhandlers[ACTIONS.HARVEST].deststate
    sg.actionhandlers[ACTIONS.HARVEST].deststate = function(inst, action)
        if inst:HasTag("pigsy") and inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil and inst.replica.hunger:GetCurrent() >= 50 then
			return "doshortaction"
		end
		return old_harvest(inst, action)
    end
end
AddStategraphPostInit("wilson", quickpick)
AddStategraphPostInit("wilson_client", quickpick_client)

--longbuild
AddStategraphState('wilson',
    State
    {
        name = "dopigsybuild",

        onenter = function(inst)
			inst.sg:GoToState("dolongaction", 1.5)
        end,
    }
)
local function longbuild(sg)
    local old_build = sg.actionhandlers[ACTIONS.BUILD].deststate
    sg.actionhandlers[ACTIONS.BUILD].deststate = function(inst, action)
        if inst:HasTag("pigsy") and not ( action.recipe and action.recipe:find("myth_flyskill")) then
			return "dopigsybuild"
		end
		return old_build(inst, action)
    end

    local old_sleep = sg.actionhandlers[ACTIONS.SLEEPIN].deststate
    sg.actionhandlers[ACTIONS.SLEEPIN].deststate = function(inst, action)
        if action.target ~= nil and action.target.prefab == "pigsy_sleepbed_item"  then
            return "pigsy_sleepbed"
        end
		return old_sleep(inst, action)
    end

    local old_tackle = sg.actionhandlers[ACTIONS.TACKLE].deststate
    sg.actionhandlers[ACTIONS.TACKLE].deststate = function(inst, action)
        if inst.prefab == "pigsy" then
            return "pigsy_tackle_pre"
        end
		return old_tackle(inst, action)
    end
end
--client不需要
AddStategraphPostInit("wilson", longbuild)


local function clientlongbuild(sg)
    local old_tackle = sg.actionhandlers[ACTIONS.TACKLE].deststate
    sg.actionhandlers[ACTIONS.TACKLE].deststate = function(inst, action)
        if inst.prefab == "pigsy" then
            return "pigsy_tackle_pre"
        end
		return old_tackle(inst, action)
    end
end
AddStategraphPostInit("wilson_client", clientlongbuild)

AddComponentPostInit("sleepingbaguser",function(self)
    local old = self.DoSleep
    function self:DoSleep(bed,...)
        if bed and bed:HasTag("no_watchphase") then
            self.bed = bed
            if self.sleeptask ~= nil then
                self.sleeptask:Cancel()
            end
            self.sleeptask = self.inst:DoPeriodicTask(self.bed.components.sleepingbag.tick_period, function() self:SleepTick() end)
            return
        end
        return old(self,bed,...)
    end
end)

local function DoYawnSound(inst)
    if inst.yawnsoundoverride ~= nil then
        inst.SoundEmitter:PlaySound(inst.yawnsoundoverride)
    elseif not inst:HasTag("mime") then
        inst.SoundEmitter:PlaySound((inst.talker_path_override or "dontstarve/characters/")..(inst.soundsname or inst.prefab).."/yawn")
    end
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
end

AddStategraphState('wilson',
    State{
        name = "pigsy_wakeup",
        tags = { "busy", "waking", "nomorph", "nodangle","nopredict" },
        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end
            if inst.AnimState:IsCurrentAnimation("bedroll") or
                inst.AnimState:IsCurrentAnimation("bedroll_sleep_loop") then
                inst.AnimState:PlayAnimation("bedroll_wakeup")
            elseif not (inst.AnimState:IsCurrentAnimation("bedroll_wakeup") or
                        inst.AnimState:IsCurrentAnimation("wakeup")) then
                inst.AnimState:PlayAnimation("wakeup")
            end
        end,
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
        end
    }
)

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

AddStategraphState('wilson',
    State{
        name = "pigsy_sleepbed",   
        tags = { "bedroll", "busy", "nomorph" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("yawn")
			inst.AnimState:PushAnimation("insomniac_dozy",false)
			
            if not IsFlying(inst) then
				local fx = SpawnPrefab("pigsy_sleepbed_fx")
				inst:AddChild(fx)
				inst.sleepfx = fx
			end
            SetSleeperSleepState(inst)
        end,

        timeline =
        {
            TimeEvent(15 * FRAMES, function(inst)
                DoYawnSound(inst)
            end),
        },
        events =
        {
            EventHandler("firedamage", function(inst)
                if inst.sg:HasStateTag("sleeping") then
                    inst.sg.statemem.iswaking = true
                    inst.sg:GoToState("wakeup")
                end
            end),
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst:GetBufferedAction() then
                        inst:PerformBufferedAction()
                        if inst.components.playercontroller ~= nil then
                            inst.components.playercontroller:Enable(true)
                        end
                        inst.sg:AddStateTag("sleeping")
                        inst.sg:AddStateTag("silentmorph")
                        inst.sg:RemoveStateTag("nomorph")
                        inst.sg:RemoveStateTag("busy")
                        inst.AnimState:PlayAnimation("sleep_loop", true)
                        inst.sleepbad_sound_task = inst:DoPeriodicTask(2, function()
                            inst.SoundEmitter:PlaySound("grotto/creatures/mole_bat/sleep")
                        end)
                    else
                        inst.sg.statemem.iswaking = true
                        inst.sg:GoToState("wakeup")
                    end
                end
            end),
        },
        onexit = function(inst)
			if inst.sleepfx  ~= nil then
				inst.sleepfx:Remove()
				inst.sleepfx = nil
            end
			if inst.sleepbad_sound_task  ~= nil then
				inst.sleepbad_sound_task:Cancel()
				inst.sleepbad_sound_task = nil
            end
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    }
)
--招架
local function IsWeaponEquipped(inst, weapon)
    return weapon ~= nil
        and weapon.components.equippable ~= nil
        and weapon.components.equippable:IsEquipped()
        and weapon.components.inventoryitem ~= nil
        and weapon.components.inventoryitem:IsHeldBy(inst)
end

local function DoTalkSound(inst)
    if inst.talksoundoverride ~= nil then
        inst.SoundEmitter:PlaySound(inst.talksoundoverride, "talk")
        return true
    elseif not inst:HasTag("mime") then
        inst.SoundEmitter:PlaySound((inst.talker_path_override or "dontstarve/characters/")..(inst.soundsname or inst.prefab).."/talk_LP", "talk")
        return true
    end
end

local function StopTalkSound(inst, instant)
    if not instant and inst.endtalksound ~= nil and inst.SoundEmitter:PlayingSound("talk") then
        inst.SoundEmitter:PlaySound(inst.endtalksound)
    end
    inst.SoundEmitter:KillSound("talk")
end

local function GetUnequipState(inst, data)
    return (inst:HasTag("wereplayer") and "item_in")
        or (data.eslot ~= EQUIPSLOTS.HANDS and "item_hat")
        or (not data.slip and "item_in")
        or (data.item ~= nil and data.item:IsValid() and "tool_slip")
        or "toolbroke"
        , data.item
end

local function oncombatparry(inst, data)
    inst.sg:AddStateTag("parrying")
        if data ~= nil then
            if data.direction ~= nil then
                inst.Transform:SetRotation(data.direction)
            end
            inst.sg.statemem.parrytime = data.duration
            inst.sg.statemem.item = data.weapon
            if data.weapon ~= nil then
                inst.components.combat.redirectdamagefn = function(inst, attacker, damage, weapon, stimuli)
                    return IsWeaponEquipped(inst, data.weapon)
                    and data.weapon.components.pigsy_rake ~= nil
					and data.weapon.components.pigsy_rake:TryParry(inst, attacker, damage, weapon, stimuli)
                    and data.weapon
                     or nil
            end
        end
    end
end


AddStategraphState('wilson_client',
    State{
    name = "pigsy_parry_pre",
    tags = { "preparrying", "busy" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("parry_pre")
        inst.AnimState:PushAnimation("parry_loop", true)

        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(2)
    end,

    onupdate = function(inst)
        if inst:HasTag("busy") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.AnimState:PlayAnimation("parry_pst")
            inst.sg:GoToState("idle")
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.AnimState:PlayAnimation("parry_pst")
        inst.sg:GoToState("idle", true)
    end,
    })

AddStategraphState('wilson',
    State{
        name = "pigsy_parry_pre",
        tags = { "preparrying", "busy", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("parry_pre")
            inst.AnimState:PushAnimation("parry_loop", true)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())

            inst:ListenForEvent("pigsy_combat_parry", oncombatparry)
			local equip = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) 
			if equip ~= nil and equip.components.pigsy_rake ~= nil then
				local direction = inst.Transform:GetRotation()
				inst:PushEvent("pigsy_combat_parry",{duration = 3, weapon = equip ,direction = direction})
			end
            inst:PerformBufferedAction()
            inst:RemoveEventCallback("pigsy_combat_parry", oncombatparry)
        end,

        events =
        {
            EventHandler("ontalk", function(inst)
                if inst.sg.statemem.talktask ~= nil then
                    inst.sg.statemem.talktask:Cancel()
                    inst.sg.statemem.talktask = nil
                    StopTalkSound(inst, true)
                end
                if DoTalkSound(inst) then
                    inst.sg.statemem.talktask =
                        inst:DoTaskInTime(1.5 + math.random() * .5,
                            function()
                                inst.sg.statemem.talktask = nil
                                StopTalkSound(inst)
                            end)
                end
            end),
            EventHandler("donetalking", function(inst)
                if inst.sg.statemem.talktalk ~= nil then
                    inst.sg.statemem.talktask:Cancel()
                    inst.sg.statemem.talktask = nil
                    StopTalkSound(inst)
                end
            end),
            EventHandler("unequip", function(inst, data)
                inst.sg:GoToState(GetUnequipState(inst, data))
            end),
        },

        ontimeout = function(inst)
            if inst.sg:HasStateTag("parrying") then
                inst.sg.statemem.parrying = true
                local talktask = inst.sg.statemem.talktask
                inst.sg.statemem.talktask = nil
                inst.sg:GoToState("pigsy_parry_idle", { duration = inst.sg.statemem.parrytime, pauseframes = 30, talktask = talktask })
            else
                inst.AnimState:PlayAnimation("parry_pst")
                inst.sg:GoToState("idle", true)
            end
        end,

        onexit = function(inst)
            if inst.sg.statemem.talktask ~= nil then
                inst.sg.statemem.talktask:Cancel()
                inst.sg.statemem.talktask = nil
                StopTalkSound(inst)
            end
            if not inst.sg.statemem.parrying then
                inst.components.combat.redirectdamagefn = nil
            end
        end,
    }
)

AddStategraphState('wilson',
    State{
        name = "pigsy_parry_idle",
        tags = { "notalking", "parrying", "nomorph" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()

            if data ~= nil and data.duration ~= nil then
                if data.duration > 0 then
                    inst.sg.statemem.task = inst:DoTaskInTime(data.duration, function(inst)
                        inst.sg.statemem.task = nil
                        inst.AnimState:PlayAnimation("parry_pst")
                        inst.sg:GoToState("idle", true)
                    end)
                else
                    inst.AnimState:PlayAnimation("parry_pst")
                    inst.sg:GoToState("idle", true)
                    return
                end
            end

            if not inst.AnimState:IsCurrentAnimation("parry_loop") then
                inst.AnimState:PushAnimation("parry_loop", true)
            end

            inst.sg.statemem.talktask = data ~= nil and data.talktask or nil

            if data ~= nil and (data.pauseframes or 0) > 0 then
                inst.sg:AddStateTag("busy")
                inst.sg:AddStateTag("pausepredict")

                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:RemotePausePrediction(data.pauseframes <= 7 and data.pauseframes or nil)
                end
                inst.sg:SetTimeout(data.pauseframes * FRAMES)
            else
                inst.sg:AddStateTag("idle")
            end
        end,

        ontimeout = function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("pausepredict")
            inst.sg:AddStateTag("idle")
        end,

        events =
        {
            EventHandler("ontalk", function(inst)
                if inst.sg.statemem.talktask ~= nil then
                    inst.sg.statemem.talktask:Cancel()
                    inst.sg.statemem.talktask = nil
                    StopTalkSound(inst, true)
                end
                if DoTalkSound(inst) then
                    inst.sg.statemem.talktask =
                        inst:DoTaskInTime(1.5 + math.random() * .5,
                            function()
                                inst.sg.statemem.talktask = nil
                                StopTalkSound(inst)
                            end)
                end
            end),
            EventHandler("donetalking", function(inst)
                if inst.sg.statemem.talktalk ~= nil then
                    inst.sg.statemem.talktask:Cancel()
                    inst.sg.statemem.talktask = nil
                    StopTalkSound(inst)
                end
            end),
            EventHandler("unequip", function(inst, data)
                if not inst.sg:HasStateTag("idle") then
                    inst.sg:GoToState(GetUnequipState(inst, data))
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.task ~= nil then
                inst.sg.statemem.task:Cancel()
                inst.sg.statemem.task = nil
            end
            if inst.sg.statemem.talktask ~= nil then
                inst.sg.statemem.talktask:Cancel()
                inst.sg.statemem.talktask = nil
                StopTalkSound(inst)
            end
            if not inst.sg.statemem.parrying then
                inst.components.combat.redirectdamagefn = nil
            end
        end,
    }
)

--猪猪房子不要害怕我
AddPrefabPostInit("pighouse", function(inst)
	if inst.components.playerprox then	
		local old_onnear = inst.components.playerprox.onnear
		inst.components.playerprox.onnear = function(house,player)
			if player and player.prefab == "pigsy" then
				return
			end
			if  old_onnear ~= nil then
				old_onnear(house,player)
			end
		end
	end
end)

if TUNING.FARM_PLANT_DRINK_LOW ~= nil then
	AddComponentPostInit("farmtiller", function(self)
		local old_Till = self.Till
		function self:Till(pt, doer,...)
			if self.inst.prefab == "pigsy_rake" then
				local pt1 = Point( TheWorld.Map:GetTileCenterPoint(pt:Get()) )
				local p2 = pt1 - pt
				local p3 = Point( (p2.x + 0.5) - (p2.x + 0.5)%1.333, 0, (p2.z + 0.5) - (p2.z + 0.5)%1.333 )
				pt = pt1 - p3	
			end
			return old_Till(self,pt, doer,...)
		end
	end)
end

--猪猪加油一直吃
AddPrefabPostInit("pigman", function(inst)
	if inst.components.trader then	
		local old_test = inst.components.trader.test
		inst.components.trader.test = function(inst,item, giver)
			if giver and giver.prefab == "pigsy" and inst.components.eater:CanEat(item) and item.components.edible.foodtype == FOODTYPE.VEGGIE then
				return true
			elseif old_test ~= nil then
				return old_test(inst,item, giver)
			end
		end
		local old_onaccept = inst.components.trader.onaccept
		inst.components.trader.onaccept = function(inst, giver, item)
			if giver and giver.prefab == "pigsy" and item.components.edible ~= nil and  item.components.edible.foodtype == FOODTYPE.VEGGIE then
				if giver.components.leader ~= nil and not (inst:HasTag("guard") or giver:HasTag("monster") or giver:HasTag("merm")) then
					if giver.components.minigame_participator == nil then
						giver:PushEvent("makefriend")
						giver.components.leader:AddFollower(inst)
					end
					inst.components.follower:AddLoyaltyTime(item.components.edible:GetHunger() * TUNING.PIG_LOYALTY_PER_HUNGER * 0.5)
					inst.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME * 0.5
				end
			elseif old_onaccept ~= nil then
				return old_onaccept(inst, giver, item)
			end
		end
	end
end)

--照料植物可以恢复sanity
AddComponentPostInit("farmplanttendable", function(self)
	local old_TendTo= self.TendTo  --脑残
    function self:TendTo(doer,...)
		local old = old_TendTo(self,doer,...)
		if old and doer and doer.prefab == "pigsy" then  --八戒
			doer.components.sanity:DoDelta(5)
        end
        return old
	end
end)

AddPrefabPostInit("petals_evil", function(inst)
	if inst.components.edible and inst.components.edible.oneaten ~= nil then
		local old = inst.components.edible.oneaten
		inst.components.edible.oneaten = function(inst, eater)
			if eater and eater.prefab == "pigsy" then
				return
			end
			return old(inst, eater)
		end
	end
end)

require "prefabs/veggies"
if GLOBAL.VEGGIES then
	for veggiename,veggiedata in pairs(GLOBAL.VEGGIES) do
	AddPrefabPostInit(veggiename.."_oversized", function(inst)
		if inst.components.workable and inst.components.workable.onfinish ~= nil then
			local old = inst.components.workable.onfinish
			inst.components.workable.onfinish = function(inst, worker)
				if worker and worker.prefab == "pigsy" then
					if inst.components.lootdropper and math.random() < 0.75 then
						inst.components.lootdropper:SpawnLootPrefab(math.random() < 0.75 and veggiename or veggiename.."_seeds" )
					end
				end
				return old(inst, worker)
			end
		end
	end)
	end
end


AddComponentPostInit("farmplantstress", function(self)
	local old_GetStressDescription = self.GetStressDescription
	function self:GetStressDescription(viewer,...)
		if viewer and viewer.prefab == "pigsy"then 
			if self.inst == viewer then
				return
			elseif not CanEntitySeeTarget(viewer, self.inst) then
				return GetString(viewer, "DESCRIBE_TOODARK")
			elseif self.inst.components.burnable ~= nil and self.inst.components.burnable:IsSmoldering() then
				return GetString(viewer, "DESCRIBE_SMOLDERING")
			end
	
			local stressors = {}
			for stressor, testfn in pairs(self.stressors_testfns) do
				if testfn(self.inst, self.stressors[stressor], false) then
					table.insert(stressors, stressor)
				end
			end

			if #stressors == 0 then
				return GetString(viewer, "DESCRIBE_PLANTHAPPY")
			elseif viewer:HasTag("pigsy") or (viewer.replica.inventory and viewer.replica.inventory:EquipHasTag("detailedplanthappiness")) then
				local stressor = shuffleArray(stressors)[1]
				return GetString(viewer, "DESCRIBE_PLANTSTRESSOR"..string.upper(stressor))
			else
				if #stressors >= 5 then
					return GetString(viewer, "DESCRIBE_PLANTVERYSTRESSED")
				else --if #stressors <= 4 then
					return GetString(viewer, "DESCRIBE_PLANTSTRESSED")
				end
			end	
		else
			return old_GetStressDescription(self,viewer,...)
		end
	end
end)

AddComponentAction("SCENE", "plantresearchable" , function(inst, doer, actions, right)
    if not right and doer:HasTag("pigsy")and (inst:HasTag("farmplantstress") or inst:HasTag("weedplantstress")) then
        table.insert(actions, ACTIONS.ASSESSPLANTHAPPINESS)
    end
end)

AddComponentAction("INVENTORY", "plantresearchable" , function(inst, doer, actions, right)
    if not right and doer:HasTag("pigsy")and (inst:HasTag("farmplantstress") or inst:HasTag("weedplantstress")) then
        table.insert(actions, ACTIONS.ASSESSPLANTHAPPINESS)
    end
end)

local function findhunter(value,tags)
	if value.children ~= nil and type(value.children) == "table" then
		for k1,v1 in ipairs(value.children) do
			if type(v1) == "table" then
				findhunter(v1,tags)
			end
		end
	elseif value.hunternotags ~= nil then
		for _,v2 in ipairs(tags) do
			if not table.contains(value.hunternotags,v2) then
				table.insert(value.hunternotags,v2)
			end
		end
	elseif value.hunterfn ~= nil then
		local old_hunterfn = value.hunterfn
		value.hunterfn = function(guy)
			for _,v3 in ipairs(tags) do
				if guy:HasTag(v3) then
					return false
				end
			end
			return old_hunterfn(guy)
		end
	end
end

AddBrainPostInit("pigbrain", function(self)
	for k, v in ipairs(self.bt.root.children) do
		if type(v) == "table" then
			findhunter(v,{"pigsy"})
		end
	end
end)

local function PlayMooseFootstep(inst, volume, ispredicted)
    --moose footstep always full volume
    inst.SoundEmitter:PlaySound("dontstarve/characters/woodie/moose/footstep", nil, nil, ispredicted)
    PlayFootstep(inst, volume, ispredicted)
end
local function playzhu(inst,anim,loop)
    local onwater = inst:IsOnOcean(false)
    if onwater then
        inst.AnimState:HideSymbol("hound_maw")
        inst.AnimState:HideSymbol("beefalo_hoof")
        inst.AnimState:PlayAnimation(inst.pig_transform_action.."_water_"..anim,loop)
    else
        inst.AnimState:ShowSymbol("hound_maw")
        inst.AnimState:ShowSymbol("beefalo_hoof")
        inst.AnimState:PlayAnimation(inst.pig_transform_action.."_"..anim,loop)
    end
end
--冲刺
AddStategraphState('wilson',State{
    name = "pigsy_tackle_pre",
    tags = { "busy" },
    onenter = function(inst)
        inst.components.locomotor:Stop()
        --inst.AnimState:PlayAnimation("charge_lag_pre")
        playzhu(inst,"walk_pre")
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(false)
        end
        inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength() - FRAMES)
    end,

    ontimeout = function(inst)
        inst:PerformBufferedAction()
        if inst.sg.currentstate.name == "pigsy_tackle_pre" then
            inst.sg.statemem.tackling = true
            inst.sg:GoToState("pigsy_tackle_start")
        end
    end,

    onexit = function(inst)
        if not inst.sg.statemem.tackling and inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end
    end,
})

AddStategraphState('wilson_client',State{
    name = "pigsy_tackle_pre",
    tags = { "busy" },
    onenter = function(inst)
        inst.components.locomotor:Stop()
        playzhu(inst,"walk_pre")
        inst:PerformPreviewBufferedAction()
        inst.sg:SetTimeout(2)
    end,

    onupdate = function(inst)
        if inst:HasTag("busy") then
            if inst.entity:FlattenMovementPrediction() then
                inst.sg:GoToState("idle", "noanim")
            end
        elseif inst.bufferedaction == nil then
            inst.sg:GoToState("idle")
        end
    end,

    ontimeout = function(inst)
        inst:ClearBufferedAction()
        inst.sg:GoToState("idle")
    end,
})

AddStategraphState('wilson',State{
    name = "pigsy_tackle_start",
    tags = { "busy", "nopredict", "nomorph", "nointerrupt" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        local anim = isonwater and inst.pig_transform_action.."_water_walk" or inst.pig_transform_action.."_walk"
        inst.AnimState:PlayAnimation(anim)
        ---===========================================
        inst.Physics:SetMotorVel(12, 0, 0)
        inst.Physics:ClearCollisionMask()
        inst.Physics:CollidesWith(COLLISION.WORLD)
        inst.Physics:CollidesWith(COLLISION.OBSTACLES)
        inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
        inst.Physics:CollidesWith(COLLISION.GIANTS)
        inst.sg.statemem.targets = {}
        inst.sg.statemem.edgecount = 0
        inst.sg.statemem.trailtask = inst:DoPeriodicTask(0, function(inst, data)
            if data.delay > 0 then
                data.delay = data.delay - 1
            else
                data.delay = math.random(4, 6)
                local x, y, z = inst.Transform:GetWorldPosition()
                local angle = inst.Transform:GetRotation() * DEGREES
                local fx = SpawnPrefab("plant_dug_small_fx")
                fx.Transform:SetPosition(x - math.cos(angle) * 1.6, 0, z + math.sin(angle) * 1.6)
                if math.random() < .5 then
                    fx.AnimState:SetScale(-1, 1)
                end
                local scale = .8 + math.random() * .5
                fx.Transform:SetScale(scale, scale, scale)
            end
        end,
        nil,
        { delay = 0 })
    end,

    timeline =
    {
        TimeEvent(4 * FRAMES, PlayMooseFootstep),
    },
    onupdate = function(inst)
        if inst.components.tackler ~= nil then
            if inst.components.tackler:CheckCollision(inst.sg.statemem.targets) then
                inst.sg.statemem.stopping = true
                inst.sg:GoToState("pigsy_tackle_collide")
            elseif not inst.components.tackler:CheckEdge() then
                inst.sg.statemem.edgecount = 0
            elseif inst.sg.statemem.edgecount < 3 then
                inst.sg.statemem.edgecount = inst.sg.statemem.edgecount + 1
            else
                inst.sg.statemem.stopping = true
                inst.sg:GoToState("pigsy_tackle_stop")
            end
        end
    end,
    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg.statemem.tackling = true
                inst.sg:GoToState("pigsy_tackle", {
                    targets = inst.sg.statemem.targets,
                    edgecount = inst.sg.statemem.edgecount,
                    trail = inst.sg.statemem.trailtask,
                    loop = 3,
                })
            end
        end),
    },
    onexit = function(inst)
        if not inst.sg.statemem.tackling then
            if inst.sg.statemem.trailtask ~= nil then
                inst.sg.statemem.trailtask:Cancel()
                inst.sg.statemem.trailtask = nil
            end
            inst.Physics:Stop()
            if inst._becomehpig:value() then
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.GROUND)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)
            end
            inst.Physics:Teleport(inst.Transform:GetWorldPosition())
            if not inst.sg.statemem.stopping and inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
        end
    end,
})

AddStategraphState('wilson',State{
    name = "pigsy_tackle",
    tags = { "busy", "nopredict", "nomorph", "nointerrupt" },

    onenter = function(inst, data)
        inst.sg.statemem.targets = data ~= nil and data.targets or nil
        inst.sg.statemem.edgecount = data ~= nil and data.edgecount or 0
        inst.sg.statemem.trailtask = data ~= nil and data.trail or nil
        inst.sg.statemem.loop = data ~= nil and data.loop or 0
        local anim = inst.pig_transform_action.."_walk"
        --inst.AnimState:PlayAnimation(anim)
        --if not inst.AnimState:IsCurrentAnimation(anim) then
            inst.AnimState:PlayAnimation(anim, true)
        --end
        inst.sg:SetTimeout(
            inst.sg.statemem.loop > 0 and
            inst.AnimState:GetCurrentAnimationLength() + .5 * FRAMES or
            inst.AnimState:GetCurrentAnimationLength() * math.random()
        )
    end,

    timeline =
    {
        TimeEvent(1 * FRAMES, PlayMooseFootstep),
        TimeEvent(4 * FRAMES, PlayMooseFootstep),
        TimeEvent(10 * FRAMES, PlayMooseFootstep),
    },

    onupdate = function(inst)
        if inst.components.tackler ~= nil then
            if inst.components.tackler:CheckCollision(inst.sg.statemem.targets) then
                inst.sg.statemem.stopping = true
                inst.sg:GoToState("pigsy_tackle_collide")
            elseif not inst.components.tackler:CheckEdge() then
                inst.sg.statemem.edgecount = 0
            elseif inst.sg.statemem.edgecount < 3 then
                inst.sg.statemem.edgecount = inst.sg.statemem.edgecount + 1
            else
                inst.sg.statemem.stopping = true
                inst.sg:GoToState("pigsy_tackle_stop")
            end
        end
    end,

    ontimeout = function(inst)
        if inst.sg.statemem.loop > 0 then
            inst.sg.statemem.tackling = true
            inst.sg:GoToState("pigsy_tackle", {
                targets = inst.sg.statemem.targets,
                edgecount = inst.sg.statemem.edgecount,
                trail = inst.sg.statemem.trailtask,
                loop = inst.sg.statemem.loop - 1,
            })
        else
            inst.sg.statemem.stopping = true
            inst.sg:GoToState("pigsy_tackle_stop")
        end
    end,

    onexit = function(inst)
        if not inst.sg.statemem.tackling then
            if inst.sg.statemem.trailtask ~= nil then
                inst.sg.statemem.trailtask:Cancel()
                inst.sg.statemem.trailtask = nil
            end
            inst.Physics:Stop()
            if inst._becomehpig:value() then
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.GROUND)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)
            end
            inst.Physics:Teleport(inst.Transform:GetWorldPosition())
            if not inst.sg.statemem.stopping and inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
        end
    end,
})

--撞击了会咋样！！
AddStategraphState('wilson',State{
    name = "pigsy_tackle_collide",
    tags = { "busy", "nopredict", "nomorph", "nointerrupt" },

    onenter = function(inst)
        if  inst.pig_transform_action == "pigsy" then
            inst.AnimState:PlayAnimation("pigsy_frozen_loop_pst",true)
        else
            inst.AnimState:PlayAnimation("koalefant_frozen_loop_pst",true)
        end
    end,
    timeline =
    {
        TimeEvent(8.5 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
        end),
        TimeEvent(32 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("nointerrupt")
        end),
        TimeEvent(35 * FRAMES, function(inst)
            inst.sg:GoToState("pig_zhuzhu_idle", true)
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("pig_zhuzhu_idle")
            end
        end),
    },
    onexit = function(inst)
        if inst._becomehpig:value() then
            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.GROUND)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
        end
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end
    end,
})

AddStategraphState('wilson',State{
    name = "pigsy_tackle_stop",
    tags = { "busy", "nopredict", "nomorph", "nointerrupt" },

    onenter = function(inst)
        --inst.AnimState:PlayAnimation("charge_pst")
        playzhu(inst,"walk_pst")
        inst.sg.statemem.speed = 12
        inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
        PlayMooseFootstep(inst)
        inst.SoundEmitter:PlaySound("dontstarve/characters/woodie/moose/slide")
    end,

    onupdate = function(inst)
        if inst.sg.statemem.speed > .1 then
            inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
            inst.sg.statemem.speed = inst.sg.statemem.speed * .75
        elseif inst.sg.statemem.speed > 0 then
            inst.Physics:Stop()
            inst.sg.statemem.speed = 0
        end
    end,

    timeline =
    {
        TimeEvent(20 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("nointerrupt")
        end),
        TimeEvent(22 * FRAMES, function(inst)
            inst.sg:GoToState("pig_zhuzhu_idle", true)
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("pig_zhuzhu_idle")
            end
        end),
    },

    onexit = function(inst)
        if inst._becomehpig:value() then
            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.GROUND)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
        end
        inst.Physics:Stop()
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end
    end,
})