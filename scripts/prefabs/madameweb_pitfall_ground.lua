local assets = {
    Asset("ANIM", "anim/madameweb_pitfall.zip"),
}

local TRAP_RADIUS = 3
local mine_test_fn = function(dude, inst)
    return not (dude.components.health ~= nil and
                dude.components.health:IsDead())
        and dude.components.combat:CanBeAttacked(inst)
end
local mine_test_tags = { "monster", "character", "animal" }
local mine_must_tags = { "_combat" }
local notags = {"INLIMBO","notraptrigger", "flying", "ghost", "playerghost", "spawnprotection","player","epic" }

local function CheckTrappable(guy)
    return guy.components.health == nil or not guy.components.health:IsDead()
end

local function cancatch(target)
   return not (target.components.follower and target.components.follower.leader and target.components.follower.leader:HasTag("player") )
end

local function TrapTest(inst)
    local guy = FindEntity(inst, TRAP_RADIUS, mine_test_fn, mine_must_tags, notags, mine_test_tags)
    if guy ~= nil and cancatch(guy) then
        inst.trap_test_task:Cancel()
        inst.trap_test_task = nil

        if CheckTrappable(guy) and guy:HasTag("canbetrapped") and not guy:HasTag("untrappable") then  
            --如果是那种可以被陷阱抓的 就生成一个陷阱抓住 别的就困住
            local tree = SpawnPrefab("madameweb_pitfall_trap")
            if tree ~= nil then
                tree.Transform:SetPosition(inst:GetPosition():Get())
                tree.components.trap.target = guy
                tree.components.trap:StopUpdating()
                tree:PushEvent("springtrap")
                tree.components.trap.target:PushEvent("trapped", {trap = tree})
            end
        else --其他目标 困住
            guy:PushEvent("trapped", {trap = inst})
            local tree = SpawnPrefab("madameweb_pitfall_cocoon")
            if tree ~= nil then
             tree.Transform:SetPosition(inst:GetPosition():Get())
                tree:Catch(guy)
            end
        end
        inst:Remove()
    end
end

local function OnWorkFinished(inst, worker)

    if worker and worker.components.inventory and worker:HasTag("player") then
        worker.components.inventory:GiveItem(SpawnPrefab("madameweb_pitfall"),nil,inst:GetPosition())
    end

    inst:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("madameweb_pitfall")
    inst.AnimState:SetBuild("madameweb_pitfall")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst:AddTag("trap")
    inst:AddTag("hive")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumBurnable(inst)
    MakeMediumPropagator(inst)

    inst:AddComponent("inspectable")

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_TINY)

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(OnWorkFinished)

    inst.trap_test_task = inst:DoPeriodicTask(0.75, TrapTest)

    return inst
end

return Prefab("madameweb_pitfall_ground", fn, assets)