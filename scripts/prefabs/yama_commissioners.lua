
local MakePlayerCharacter = require "prefabs/player_common"
local yama_soul_common = require("prefabs/yama_soul_common")

local assets = {
    Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
}
local prefabs = {}

local start_inv = {}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.YAMA_COMMISSIONERS
end

prefabs = FlattenTree({ prefabs, start_inv }, true)


--掉水惩罚
local function customtuningsfn(inst)
    if inst.components.yama_transform:IsWhite() then
        return TUNING.DROWNING_DAMAGE.DEFAULT
    else
        return
        {
            HEALTH_PENALTY = 0.375,
            HUNGER = 25,
            SANITY = 37.5,
            WETNESS = 100,
        }
    end
end

--理智相关
local easing = require("easing")
local function CustomSanityFn(inst, dt)
    local delta = 0
    if TheWorld.state.isday then --白天掉=理智
        delta = delta - 0.083333333333333
    end

    if TheWorld.state.israining  and inst.components.yama_transform:IsWhite() then --下雨 白无常也掉
        delta = delta - 0.083333333333333
    end
    if inst.components.yama_transform:IsBlack() then --黑无常 潮湿+0.5
        delta = delta + 0.5* easing.inSine(inst.components.moisture:GetMoisture(), 0, TUNING.MOISTURE_SANITY_PENALTY_MAX, inst.components.moisture:GetMaxMoisture())
    end
    return delta
end

local absorptionmodifiers = { --无常新修改
    -- 0.8, 0.8, 0.7, 0.6,
    1, 1, 0.9, 0.8,
}
local function Upgrade(inst, silent)
    inst.components.yama_transform:Upgrade()
    local absor = absorptionmodifiers[TheWorld.components.yama_statuemanager:GetLevel()] or 0.8
    inst.components.eater:SetAbsorptionModifiers(absor,absor,absor)
end

---灵魂掉落相关
local function IsValidVictim(victim)
    return yama_soul_common.HasSoul(victim) and victim.components.health and victim.components.health:IsDead()
end

local function OnRestoreSoul(victim)
    victim.noyamasoultask = nil
end

local function SpawnSoulAt(x, y, z, victim,nossb)
    local fx = SpawnPrefab(yama_soul_common.GetSoul(victim))
    fx.Transform:SetPosition(x, y, z)
    fx:Setup(victim)
    if not nossb then
        fx.sssbinfo = {prefab = victim.prefab , name = victim:GetBasicDisplayName() or "MISSING NAME", targettime = GetTime() + 10*480}
    end
end

--直接给
local function GetSoulNumber(hat, tag)
	local souls = hat.components.container:FindItem(function(item)
		return item:HasTag(tag)
	end)
	if souls ~= nil and souls.components.stackable ~= nil then
		return souls.components.stackable:StackSize() or 1
	else
		return 0
	end
end

--生成生成点
local function SpawnSoulsAt(victim, numsouls,nossb)
    local x, y, z = victim.Transform:GetWorldPosition()
    if numsouls == 2 then
        local theta = math.random() * 2 * PI
        local radius = .4 + math.random() * .1
        SpawnSoulAt(x + math.cos(theta) * radius, 0, z - math.sin(theta) * radius, victim,nossb)
        theta = GetRandomWithVariance(theta + PI, PI / 15)
        SpawnSoulAt(x + math.cos(theta) * radius, 0, z - math.sin(theta) * radius, victim,nossb)
    else
        SpawnSoulAt(x, y, z, victim,nossb)
        if numsouls > 1 then
            numsouls = numsouls - 1
            local theta0 = math.random() * 2 * PI
            local dtheta = 2 * PI / numsouls
            local thetavar = dtheta / 10
            local theta, radius
            for i = 1, numsouls do
                theta = GetRandomWithVariance(theta0 + dtheta * i, thetavar)
                radius = 1.6 + math.random() * .4
                SpawnSoulAt(x + math.cos(theta) * radius, 0, z - math.sin(theta) * radius, victim,nossb)
            end
        end
    end
end

local function GiveSouls(inst, num, pos,victim,nossb)
    local soul = yama_soul_common.GetSoul(victim)
    local soul_name = soul == "soul_specter_spawn"  and "soul_specter"  or "soul_ghast"
    local needspwan = 0
    for k = 1, num do 
        local hat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if hat ~= nil and hat.components.container ~= nil and hat:HasTag("soullostbox") and GetSoulNumber(hat, soul_name) < TUNING.STACK_SIZE_MEDITEM then
            local spawnsoul = SpawnPrefab(soul_name)
            hat.components.container:GiveItem(spawnsoul,nil,pos)
        else
            needspwan = needspwan + 1
        end
    end
    if needspwan ~= 0 then
        SpawnSoulsAt(victim, needspwan,nossb)
    end
end

local function OnMurdered(inst, data)
    local victim = data.victim
    if victim ~= nil and
        victim.noyamasoultask == nil and
        victim:IsValid() and
        (   not inst.components.health:IsDead() and
            yama_soul_common.HasSoul(victim)
        ) then
        victim.noyamasoultask = victim:DoTaskInTime(5, OnRestoreSoul)
        GiveSouls(inst, yama_soul_common.GetNumSouls(victim) * (data.stackmult or 1), inst:GetPosition(),victim)
    end
end

local function OnEntityDropLoot(inst, data)
    local victim = data.inst
    if victim ~= nil and
        victim.noyamasoultask == nil and
        victim:IsValid() and
        (   victim == inst or
            (   not inst.components.health:IsDead() and
                IsValidVictim(victim) and
                inst:IsNear(victim, TUNING.WORTOX_SOULEXTRACT_RANGE)
            )
        ) then

        victim.noyamasoultask = victim:DoTaskInTime(5, OnRestoreSoul)
        SpawnSoulsAt(victim, yama_soul_common.GetNumSouls(victim))
    end
end

local function OnEntityDeath(inst, data)
    if data.inst ~= nil and data.inst.components.lootdropper == nil then
        OnEntityDropLoot(inst, data)
    end
end

local function killfires(inst)
    if inst.soulfire1 and inst.soulfire1:IsValid()  then
        inst.soulfire1:Remove()
    end
    if inst.soulfire2 and inst.soulfire2:IsValid()  then
        inst.soulfire2:Remove()
    end
    if inst.soulfiretask then
        inst.soulfiretask:Cancel()
    end
    inst.soulfire1 = nil
    inst.soulfire2 = nil
    inst.soulfiretask = nil
end

local rad = 0.2
local function SpawnFire(inst)
    local fire = SpawnPrefab("yama_soulfire_purple")
    fire.entity:SetParent((inst.entity))
    fire.Transform:SetPosition(0,0,rad)
	inst.soulfire1 = fire

    fire = SpawnPrefab("yama_soulfire_blue")
    fire.entity:SetParent((inst.entity))
    fire.Transform:SetPosition(0,0,-rad)
	inst.soulfire2 = fire
end

local function onbecamehuman(inst)
    if inst._onentitydroplootfn == nil then
        inst._onentitydroplootfn = function(src, data) OnEntityDropLoot(inst, data) end
        inst:ListenForEvent("entity_droploot", inst._onentitydroplootfn, TheWorld)
    end
    if inst._onentitydeathfn == nil then
        inst._onentitydeathfn = function(src, data) OnEntityDeath(inst, data) end
        inst:ListenForEvent("entity_death", inst._onentitydeathfn, TheWorld)
    end
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "yama_commissioners_speed")

    inst.components.yama_transform:StopRegenSoul()
end

local function onbecameghost(inst)
    if inst._onentitydroplootfn ~= nil then
        inst:RemoveEventCallback("entity_droploot", inst._onentitydroplootfn, TheWorld)
        inst._onentitydroplootfn = nil
    end
    if inst._onentitydeathfn ~= nil then
        inst:RemoveEventCallback("entity_death", inst._onentitydeathfn, TheWorld)
        inst._onentitydeathfn = nil
    end

    inst.components.locomotor:SetExternalSpeedMultiplier(inst, "yama_commissioners_speed", 5/6)

    if inst.player_classified  and inst.player_classified.MapExplorer then
        inst.player_classified.MapExplorer:EnableUpdate(true)
    end

    inst.AnimState:SetBuild("twigs")

    inst.components.yama_transform:StartRegenSoul()

    SpawnFire(inst)
end

local function ondeath(inst)
    inst.components.yama_transform.soul = 0
end

local function onbecameyama(inst)
    inst.components.yama_transform:DoResurrection()
end

local function onload(inst,data)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
    if data then
        if data._yama_bahy then
            inst._yama_bahy:set(data._yama_bahy)
        end
    end
end

local flying  = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying() end

local function setsound(inst)
    local fx  = flying(inst) and inst.components.mk_flyer.fx
    if inst._commissioner_balck:value() then
        if fx and fx:IsValid() then
            for k in pairs(fx.fx)do
                if k:IsValid() then
                    k.AnimState:SetBuild("mk_cloudfx_purple")
                end
            end
        end
        if inst.flyerfx_wheel_fx then
            inst.flyerfx_wheel_fx.Light:SetColour(210 / 255, 59 / 255, 221 / 255)
        end
        inst.talker_path_override = 'Cymbals/' 
        inst.MiniMapEntity:SetIcon( "commissioner_black.tex" )
    else
        if fx and fx:IsValid() then
            for k in pairs(fx.fx)do
                if k:IsValid() then
                    k.AnimState:SetBuild("mk_cloudfx_blue")
                end
            end
        end
        if inst.flyerfx_wheel_fx then
            inst.flyerfx_wheel_fx.Light:SetColour(40 / 255, 264 / 255, 218 / 255)
        end
        inst.talker_path_override = 'Sanxian/' 
        inst.MiniMapEntity:SetIcon( "yama_commissioners.tex" )
    end
end

local function KillCircleFires(inst)
    if flying(inst) then
        return
    end
    if inst.components.debuffable:HasDebuff("whip_commissioner_buff") then
        return
    end

    for _,v in pairs(inst.circles_fires) do
        if v:IsValid() then
            v:Remove()
        end
	end

    inst.circles_fires = {}
    if inst.circles_fire_task then
        inst.circles_fire_task:Cancel()
        inst.circles_fire_task = nil
    end
end

local function kill_flyerfx(inst)
    for k, v in pairs(inst.myth_flyer_fxs) do 
        if v and v:IsValid() then
            v:Remove()
        end
    end
    KillCircleFires(inst)
end

local function doRemove(inst)
    for k, v in pairs(inst.myth_flyer_fxs) do 
        if v and v:IsValid() then
            v:Remove()
        end
    end
    for _,v in pairs(inst.circles_fires) do
        if v:IsValid() then
            v:Remove()
        end
	end

    inst.circles_fires = {}
    if inst.circles_fire_task then
        inst.circles_fire_task:Cancel()
        inst.circles_fire_task = nil
    end
end

local firefx = {
    white = "yama_fire_green",
    black = "yama_fire_purple",
}

local function replacefire(inst,build)
    for i,v in pairs(inst.circles_fires) do
        if v:IsValid() and v.prefab ~= firefx[build] then
            local new =  ReplacePrefab(v,firefx[build])
            new._light.Light:Enable(false)
            inst.circles_fires[i] = new
        end
	end
end

local function spawn_flyerfx(inst)
    local current = inst.components.yama_transform.current
    local other = inst.components.yama_transform:GetOtherState()
    if not (inst.myth_flyer_fxs[other] and inst.myth_flyer_fxs[other]:IsValid()) then
        local fx = SpawnPrefab("yama_flyer_fx")
        local x,y,z = inst.Transform:GetWorldPosition()
        fx.Transform:SetPosition(x,0,z)
        fx.Transform:SetRotation(inst.Transform:GetRotation())
        fx:SetOwer(inst)
    end

    if inst.myth_flyer_fxs[current] and inst.myth_flyer_fxs[current]:IsValid() then
        local x,y,z = inst.Transform:GetWorldPosition()
        local x1,y1,z1 = inst.myth_flyer_fxs[current].Transform:GetWorldPosition()
        inst.Transform:SetPosition(x1,y,z1)
        inst.myth_flyer_fxs[current]:Remove()
    end
    replacefire(inst,current)
end

local radius = 1
local function UpdateDeerOffsets(inst)
	local pt = inst:GetPosition()
	local theta = inst.circles_fire_theta
	inst.circles_fire_theta = inst.circles_fire_theta + .1
	local num = 3
	for _,v in pairs(inst.circles_fires) do
        if v:IsValid() then
		    local offset = Vector3(radius * math.cos(theta), 1, -radius * math.sin(theta))
		    v.formationpos = pt + offset
		    v.Transform:SetPosition(v.formationpos:Get())
		    theta = theta - (2*PI/num)
        end
	end
end

local function SpawnDeer(inst)
    local pos = inst:GetPosition()
    for k = 0 , 240, 120 do 
        local fire = SpawnPrefab(firefx[inst.components.yama_transform.current])
        fire._light.Light:Enable(false)
        fire.Transform:SetPosition(pos.x + radius * math.cos(math.rad(k)), 1,pos.z + radius * math.sin(math.rad(k)))
        table.insert(inst.circles_fires,fire)
    end
end

local function Spawn_Circle_Fires(inst)
    if next(inst.circles_fires)  ~= nil then
        replacefire(inst,inst.components.yama_transform.current)
        return 
    end
    inst.circles_fire_theta = 0
	SpawnDeer(inst)
	inst.circles_fire_task = inst:DoPeriodicTask(0, UpdateDeerOffsets)
end

local function bonusdamagefn(inst, target)
    if target and target:HasTag("ghost") then
        return 999999
    end
    return 0
end

local function OnIsRaining(inst, israining)
    if israining and not inst.components.health:IsDead() then
        if inst.components.yama_transform.current == "white"  then
            inst.components.talker:Say(STRINGS.YAMA_COMMISSIONERS_ISRAINING)
        else
            inst.components.talker:Say(STRINGS.YAMA_COMMISSIONERS_ISRAINING_BLACK)
        end
    end
end

local function onpreload(inst, data)
    if data and data.yama_transform then

        inst.components.yama_transform.current = data.yama_transform.current
        
        local state = data.yama_transform.current
        inst.components.health.currenthealth = data.yama_transform.states[state].current.currenthealth 
        inst.components.sanity.current = data.yama_transform.states[state].current.sanity 
        inst.components.hunger.current = data.yama_transform.states[state].current.hunger 
        local max = TheWorld.components.yama_statuemanager:IsMaxLevel() and "maxmax" or "max"
        inst.components.hunger.max = inst.components.yama_transform.sanwei[state][max].hunger
        inst.components.health.maxhealth = inst.components.yama_transform.sanwei[state][max].maxhealth
        inst.components.sanity.max = inst.components.yama_transform.sanwei[state][max].sanity
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
        inst.components.yama_transform.current = data.yama_transform.current
    end
end

local function onsave(inst, data)
    data._yama_bahy = inst._yama_bahy:value()
end

local function onlearn(inst)
	inst._yama_bahy:set(true)
end

local common_postinit = function(inst) 
	inst.soundsname = "sfx"
	inst.talker_path_override = 'Sanxian/' 

	inst.MiniMapEntity:SetIcon( "yama_commissioners.tex" )
    inst:AddTag("yama_commissioners")
    inst.yama_status_soul = net_byte(inst.GUID,"yama_status_soul", "yama_status_soul_dirty")
    inst._commissioner_balck = net_bool(inst.GUID,"yama_commissioners_black", "yama_commissioner_dirty")
    inst._yama_bahy = net_bool(inst.GUID,"yama_bahy", "yama_bahy_dirty")

    if inst.components.playervision then
        inst.components.playervision.SetGhostVision = function() end
    end

	inst:DoTaskInTime(0,function()
		if inst.replica.builder then
			local old_CanBuild = inst.replica.builder.CanBuild	
			inst.replica.builder.CanBuild = function(self,recname,...)
				if recname and (recname == "reviver"  or recname == "resurrectionstatue" ) then
					return false
				end
				return old_CanBuild(self,recname,...)
			end
		end
	end)
    inst:ListenForEvent("yama_commissioner_dirty", setsound)
end
    
local master_postinit = function(inst)

    inst.myth_flyer_fxs = {
        white = nil,
        black = nil,
    }

    inst.Myth_Learn_Skill = onlearn
	inst.circles_fires = {}

	inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

    inst.myth_notbeghost = true

    inst.components.sanity.night_drain_mult = 0
    --inst.components.sanity.neg_aura_mult = 0

	inst.components.foodaffinity:AddPrefabAffinity("bird_egg", TUNING.AFFINITY_15_CALORIES_HUGE)
    inst.components.foodaffinity:AddPrefabAffinity("bird_egg_cooked", TUNING.AFFINITY_15_CALORIES_HUGE)

	inst.components.health:SetMaxHealth(TUNING.YAMA_COMMISSIONERS_HEALTH)
	inst.components.hunger:SetMax(TUNING.YAMA_COMMISSIONERS_HUNGER)
	inst.components.sanity:SetMax(TUNING.YAMA_COMMISSIONERS_SANITY)
    inst.components.sanity.custom_rate_fn = CustomSanityFn
	
    inst.components.combat.damagemultiplier = 1
    inst.components.combat.bonusdamagefn  = bonusdamagefn

    if inst.components.drownable then
        inst.components.drownable.customtuningsfn = customtuningsfn
    end
	
	inst.components.hunger.hungerrate = 1 * TUNING.WILSON_HUNGER_RATE

    local old_SetVal  = inst.components.health.SetVal
    inst.components.health.SetVal = function(self,val, cause, afflicter)
        if self.minhealth and self.minhealth == 0 and (val <= self.minhealth) and
            self.inst.components.yama_transform:CanTransform() then
                self.inst.components.yama_transform:DoTransform()
            return
        end
        return old_SetVal(self,val, cause, afflicter)
    end

    --[[
        TIP: 之所以要改Equip函数，是因为，睡觉前官方做了 inst:OnSleepIn() 操作，
            这个操作会让玩家强制卸下手上装备的物品，并且该卸下不会返回玩家物品栏，
            等到醒来官方做了 inst:OnWakeUp() 操作后，将之前卸下的手部物品重新装备上。
            也就导致，白/黑无常睡着时还没进行 inst:OnWakeUp() 操作前，被杀死变成黑/白无常时，
            无常道具的自动变换功能出问题：手部道具已经被卸下，因此没有被变换机制识别到，然后变身成没死的无常
            之后，才把之前卸下的手部道具重新装备上，但此时无常的人物标签已不能让这个道具能被成功装备在手上。
            总之，修改这里就为了让之前卸下的手部物品重新装备时，能在失败后回到物品栏
    ]]--
    local Equip_old = inst.components.inventory.Equip
    inst.components.inventory.Equip = function(self, item, ...)
        if item ~= nil and item.components.equippable ~= nil and item:IsValid() then
            if item.components.equippable:IsRestricted(self.inst) and item:HasTag("commissioneritem") then
                item.isinequipslot = true --让无常道具变换机制能继续把新道具装备上，而不是单纯回到物品栏
                self:GiveItem(item)
                return false
            end
        end
        return Equip_old(self, item, ...)
    end

    inst:ListenForEvent("murdered", OnMurdered)

    inst:AddComponent("yama_transform")

    inst:ListenForEvent("yama_statuelevelchanged", function() Upgrade(inst) end, TheWorld)
    inst:ListenForEvent("death", ondeath)
    inst:WatchWorldState("israining", OnIsRaining)

    inst:DoTaskInTime(0.1, Upgrade)

    inst:ListenForEvent("onremove", doRemove)

    inst.KillSoulFire = killfires
    inst.SetYamaHealth = onbecameyama
    inst.Give_Yama_Souls = GiveSouls
    inst.Spawn_Flyerfx = spawn_flyerfx
    inst.Spawn_Circle_Fires = Spawn_Circle_Fires
    inst.Kill_Flyerfx = kill_flyerfx
    inst.KillCircleFires  = KillCircleFires
	inst.OnLoad = onload
    inst.OnSave = onsave
    inst.OnNewSpawn = onload
    inst.OnPreLoad = onpreload
end

return MakePlayerCharacter("yama_commissioners", prefabs, assets, common_postinit, master_postinit, start_inv)
