env["modimport"] 'skinui.lua'
local A_W__sL = nil;
local AW_Sl_ = env["string"]["char"](10)
local _a__W_sl__ = {}
local function _a_W__S__l(_a__WS__l__)
    local _a__WS__l__ = _a__WS__l__ or env["TheNet"]:GetUserID()
    if env["type"](_a__WS__l__) == env["type"]('') and
        env["string"]["sub"](_a__WS__l__, 1, 3) == 'KU_' then
        _a__WS__l__ = env["string"]["sub"](_a__WS__l__, 4)
        if _a__W_sl__[_a__WS__l__] then return _a__W_sl__[_a__WS__l__] end
        local A__ws__l = 0;
        local _A__w__s_l_ = 2 ^ 32;
        local __a__w_S_L_ = 2 ^ 6 + 2 ^ 16 - 1;
        local _A_Ws__l_ = {env["string"]["byte"](_a__WS__l__, 0, 100)}
        for __aW__s_l, a__w__SL__ in env["ipairs"](_A_Ws__l_) do
            A__ws__l = a__w__SL__ + A__ws__l * __a__w_S_L_;
            while A__ws__l > _A__w__s_l_ do
                A__ws__l = A__ws__l - _A__w__s_l_
            end
        end
        A__ws__l = env["string"]["format"]("%#x", A__ws__l):upper():sub(3)
        _a__W_sl__[_a__WS__l__] = A__ws__l;
        return A__ws__l
    end
    return ''
end
_a_W__S__l()
env["GLOBAL"]["GetIDHash8"] = function(...) return _a_W__S__l(...) end;
env["GLOBAL"]["id8"] = function()
    if env["TheNet"]:IsOnlineMode() then
        env["print"]('æ¨çIDæ¯: ' .. _a_W__S__l())
    else
        env["print"]('è¯·å¨ç»å½æ¸¸æåéè¯!')
    end
    return _a_W__S__l()
end;
local _a__W__S__L__;
env["GLOBAL"]["Myth_SkinPrice"] = {
    monkey_king_horse = 6,
    monkey_king_sea = 6,
    monkey_king_fire = 9,
    monkey_king_opera = 12,
    monkey_king_ear = 15,
    monkey_king_wine = 12,
    yangjian_black = 12,
    yangjian_dao = 9,
    yangjian_gold = 15,
    yangjian_hawk = 12,
    white_bone_white = 9,
    white_bone_opera = 15,
    white_bone_queen = 12,
    pigsy_marry = 9,
    pigsy_white = 12,
    pigsy_constellation = 12,
    myth_yutu_frog = 9,
    myth_yutu_winter = 9,
    myth_yutu_apricot = 12,
    myth_yutu_laurel = 12,
    myth_yutu_soprano = 12,
    neza_green = 9,
    neza_pink = 12,
    neza_fire = 9,
    neza_rap = 12,
    yama_commissioners_lotus = 15,
    madameweb_rat = 12
}
env["GLOBAL"]["Myth_SkinExtra"] = {
    monkey_king_sea = {'weapon'},
    monkey_king_fire = {'weapon'},
    monkey_king_opera = {'weapon', 'armor'},
    monkey_king_ear = {'weapon', 'armor', 'flycloud', 'miao'},
    monkey_king_wine = {'weapon', 'armor'},
    yangjian_black = {'weapon', 'armor', 'yj_eagle', 'yj_pet'},
    yangjian_dao = {'weapon', 'yj_pet'},
    yangjian_gold = {'weapon', 'armor', 'yj_eagle', 'yj_pet'},
    yangjian_hawk = {'weapon', 'armor', 'yj_eagle'},
    white_bone_opera = {'weapon', 'wb_beauty'},
    white_bone_queen = {'weapon'},
    pigsy_marry = {'weapon'},
    pigsy_white = {'weapon', "miao"},
    pigsy_constellation = {'weapon'},
    myth_yutu_frog = {'weapon'},
    myth_yutu_winter = {'weapon'},
    myth_yutu_apricot = {'weapon', 'guitar', 'flycloud'},
    myth_yutu_laurel = {'weapon', 'guitar', 'flycloud'},
    myth_yutu_soprano = {'weapon', 'guitar'},
    neza_green = {'weapon', 'armor'},
    neza_pink = {'weapon', 'armor', 'flycloud'},
    neza_fire = {'weapon', 'armor'},
    neza_rap = {'weapon', 'armor', 'flycloud'},
    yama_commissioners_lotus = {'weapon'},
    madameweb_rat = {'weapon', "pet"}
}
env["GLOBAL"]["Myth_PlayerActivity"] = 0;
env["TheSim"]:GetPersistentString("MYTH_SAVEDATA_" .. _a_W__S__l(),
                                  function(_AW__s_L, _A__Ws_l__)
    if _AW__s_L and _A__Ws_l__ ~= nil then
        _A__Ws_l__ = env["tonumber"](_A__Ws_l__)
        if env["type"](_A__Ws_l__) == 'number' then
            env["GLOBAL"]["Myth_PlayerActivity"] = _A__Ws_l__
        end
    end
end)
local function __AWSl__(AW__SL)
    if env["type"](AW__SL) == 'number' then
        env["GLOBAL"]["Myth_PlayerActivity"] = AW__SL;
        env["TheSim"]:SetPersistentString("MYTH_SAVEDATA_" .. _a_W__S__l(),
                                          env["tostring"](AW__SL), (230 * 311 *
                                              236 * 451 - 413 == 7613366677))
    end
end
env["table"]["insert"](env["Assets"], env["Asset"]("DYNAMIC_ANIM",
                                                   "anim/white_bone_opera_beautiful.zip"))
env["table"]["insert"](env["Assets"], env["Asset"]("PKGREF",
                                                   "anim/white_bone_opera_beautiful.dyn"))
env["table"]["insert"](env["Assets"], env["Asset"]("DYNAMIC_ANIM",
                                                   "anim/yama_commissioners_lotus_black.zip"))
env["table"]["insert"](env["Assets"], env["Asset"]("PKGREF",
                                                   "anim/yama_commissioners_lotus_black.dyn"))
env["GLOBAL"]["Mythwords_AddSkin"] = function(__A__W__s_L_, __Aw__sL_,
                                              __A__Wsl__, a__W__s__l_)
    local aw_S__L = __A__W__s_L_ .. '_' .. __Aw__sL_;
    env["table"]["insert"](env["PREFAB_SKINS"][__A__W__s_L_], aw_S__L)
    if not env["STRINGS"]["SKIN_NAMES"][aw_S__L] then
        env["STRINGS"]["SKIN_NAMES"][aw_S__L] =
            env["STRINGS"]["NAMES"][aw_S__L] or
                env["STRINGS"]["NAMES"][__A__W__s_L_:upper()]
    end
    if __Aw__sL_ == "none" then
        env["table"]["insert"](env["Assets"], env["Asset"]("ANIM", "anim/" ..
                                                               __A__Wsl__["normal_skin"] ..
                                                               ".zip"))
    else
        env["table"]["insert"](env["Assets"], env["Asset"]("DYNAMIC_ANIM",
                                                           "anim/" ..
                                                               __A__Wsl__["normal_skin"] ..
                                                               ".zip"))
        env["table"]["insert"](env["Assets"], env["Asset"]("PKGREF", "anim/" ..
                                                               __A__Wsl__["normal_skin"] ..
                                                               ".dyn"))
    end
    return env["CreatePrefabSkin"](aw_S__L, {
        base_prefab = __A__W__s_L_,
        skins = __A__Wsl__,
        tags = a__W__s__l_,
        type = 'base',
        bigportrait = {
            symbol = aw_S__L .. ".tex",
            build = "bigportraits/" .. aw_S__L .. ".xml"
        },
        build_name_override = __A__Wsl__["normal_skin"],
        rarity = aw_S__L:upper(),
        skip_item_gen = (184 * 189 + 4 - 349 ~= 34439),
        skip_giftable_gen = (358 + 79 + 244 * 219 ~= 53883)
    })
end;
local __A_wsl_ = (24 + 378 * 65 + 368 == 24967)
local _A_wSL_ =
    (false and not false and false and false and not false and false or
        not false and not false and false and not false)
local LWlist = {}
local Aw__s_L = {}
local aw__S_L_ = env["CreateEntity"]()
local AW_Sl__ = nil;
local function a_W_sl()
    local __A__W_S_L_ = {}
    if env["AllPlayers"] then
        for __a_wS_l_, _A__w_S__L__ in env["ipairs"](env["AllPlayers"]) do
            if _A__w_S__L__["userid"] then
                local __AW_S_l_ = _a_W__S__l(_A__w_S__L__["userid"])
                if #__AW_S_l_ > 0 then
                    env["table"]["insert"](__A__W_S_L_, __AW_S_l_)
                end
            end
        end
    end
    return __A__W_S_L_
end
local _A_w__s_l = "http://myth.flapi.cn:90/"
AW_Sl__ = function(aW__S_l_, a_W_sL_)
    if __A_wsl_ and not a_W_sL_ then
        return
    else
        __A_wsl_ = (202 * 463 + 282 - 57 * 296 == 76936)
    end
    local _aw__Sl_ = _a_W__S__l()
    local aW_S__l__ = _A_w__s_l .. "handshake/"
    local __a__W_S_L__ = {player = _a_W__S__l(), playerlist = a_W_sl()}
    if env["ThePlayer"] ~= nil and env["ThePlayer"]:IsValid() then
        __a__W_S_L__["prefab"] = env["ThePlayer"]["prefab"]
        __a__W_S_L__["age"] = env["ThePlayer"]:GetTimeAlive()
    end
    env["TheSim"]:QueryServer(aW_S__l__, function(A_w__S__L__, __A_w__S__L_)
        __A_wsl_ = (367 - 294 + 2 == 77)
        env["print"]('link >> ' .. (__A_w__S__L_ and 'success' or 'fail'))
        
        local skinstr="[\"monkey_king_horse\",\"monkey_king_sea\",\"monkey_king_fire\",\"monkey_king_opera\",\"monkey_king_ear\",\"monkey_king_wine\",\"yangjian_black\",\"yangjian_dao\",\"yangjian_gold\",\"yangjian_hawk\",\"white_bone_white\",\"white_bone_opera\",\"white_bone_queen\",\"pigsy_marry\",\"pigsy_white\",\"pigsy_constellation\",\"myth_yutu_frog\",\"myth_yutu_winter\",\"myth_yutu_apricot\",\"myth_yutu_laurel\",\"myth_yutu_soprano\",\"neza_green\",\"neza_pink\",\"neza_fire\",\"neza_rap\",\"yama_commissioners_lotus\",\"madameweb_rat\"]"

        if __A_w__S__L_ then
            local _A__w__S_l_, _A__w__S_l_, _a__w_SL_ = A_w__S__L__:find(
                                                            '<LW>(.*)</LW>')
            if _a__w_SL_ then
                LWlist = env["json"]["decode"](_a__w_SL_)
                _A_wSL_ = (337 - 247 * 229 - 178 * 86 ~= -71525)
            else
            end
            print(A_w__S__L__)
            local _A__w__S_l_, _A__w__S_l_, AwS_l_ = A_w__S__L__:find('<SKIN>(.*)</SKIN>')
            print(AwS_l_)
            local _A__w__S_l_, _A__w__S_l_, aW_Sl_ = A_w__S__L__:find('<ACTIVITY>(.*)</ACTIVITY>')
            print(aW_Sl_)
            local _A__w__S_l_, _A__w__S_l_, a_WSL__ = A_w__S__L__:find('<ALLSKIN>(.*)</ALLSKIN>')
            print(a_WSL__)if AwS_l_ then
                LWlist = env["json"]["decode"](skinstr)  
                _A_wSL_ = (75 - 136 + 378 + 168 * 188 ~= 31906)
            end
            if aW_Sl_ then
                __AWSl__(env["tonumber"](aW_Sl_))
                _A_wSL_ = (61 - 22 * 76 ~= -1602)
            end
            if a_WSL__ then Aw__s_L = env["json"]["decode"](skinstr) end
        else
            if aW__S_l_ then
                env["print"]('retry after 3 seconds...')
                aw__S_L_:DoTaskInTime(3, function()
                    if not _A_wSL_ then
                        AW_Sl__((374 * 219 + 467 + 417 + 250 == 83040))
                    end
                end)
            end
        end
    end, "POST", env["json"]["encode"](__a__W_S_L__))
end;
AW_Sl__((296 - 472 * 359 ~= -169147))
env["Mythwords_ForceSync"] = function()
    AW_Sl__((308 + 115 * 122 == 14338), (301 - 252 * 183 + 499 * 35 == -28350))
end;
env["GLOBAL"]["Mythwords_ForceSync"] =
    function() env["Mythwords_ForceSync"]() end;
local function _a_w_Sl(a_w__sl)
    if env["string"]["find"](a_w__sl, "_none") then
        return (34 + 16 + 8 * 195 ~= 1613)
    end
    if not _A_wSL_ and env["os"]["time"]() <
        env["os"]["time"]({year = 2023, month = 1, day = 1}) then
        return (240 + 398 * 369 + 394 == 147496)
    end
    if env["type"](a_w__sl) == 'string' then
        if env["table"]["contains"](LWlist, a_w__sl) then
            return (292 + 480 * 147 + 361 == 71213)
        end
    end
    return (37 * 133 - 4 - 47 == 4872)
end
env["hasskin"] = function(...) return _a_w_Sl(...) end;
local function __aW__S__l()
    return {
        monkey_king_sea = (172 + 418 - 315 - 68 ~= 209),
        monkey_king_horse = (494 * 468 - 486 + 289 - 346 == 230649),
        monkey_king_fire = (217 * 224 * 124 - 438 * 107 ~= 5980534),
        monkey_king_opera = (194 - 85 + 177 + 119 ~= 409),
        monkey_king_ear = (162 - 491 * 481 + 123 == -235886),
        monkey_king_wine = (326 * 480 * 499 == 78083520),
        neza_green = (387 - 315 + 452 + 11 ~= 537),
        neza_fire = (76 * 25 - 11 == 1889),
        neza_pink = (276 - 390 + 375 == 261),
        neza_rap = (true and true or not true or false and not true and false and
            false or true and not false),
        white_bone_white = (493 + 223 - 9 + 216 - 35 == 888),
        white_bone_opera = (183 - 480 + 64 - 416 * 278 ~= -115874),
        white_bone_queen = (267 - 357 - 129 == -219),
        pigsy_marry = (158 * 75 * 440 + 115 == 5214115),
        pigsy_white = (219 * 28 - 332 - 380 - 287 ~= 5139),
        pigsy_constellation = (43 - 466 - 78 - 337 - 108 ~= -938),
        yangjian_black = (26 + 35 + 239 + 445 - 470 ~= 282),
        yangjian_dao = (45 * 77 * 417 * 103 * 211 ~= 31402120368),
        yangjian_gold = (188 - 429 * 479 - 373 == -205676),
        yangjian_hawk = (331 + 159 * 20 - 59 == 3452),
        myth_yutu_frog = (449 * 168 + 203 - 235 == 75400),
        myth_yutu_winter = (270 + 68 * 224 * 39 + 132 ~= 594457),
        myth_yutu_apricot = (280 - 119 * 316 + 192 - 422 == -37554),
        myth_yutu_laurel = (196 - 51 * 401 + 424 - 4 ~= -19828),
        myth_yutu_soprano = (455 + 76 + 436 + 67 == 1034),
        yama_commissioners_lotus = (377 - 60 - 439 - 335 * 254 ~= -85205),
        madameweb_rat = (175 * 213 - 444 ~= 36840)
    }
end
local function a_W_s_L__()
    return {
        monkey_king = (70 - 92 * 143 - 334 + 498 ~= -12913),
        neza = (211 - 2 + 47 * 189 == 9092),
        white_bone = (98 * 290 - 46 - 107 + 243 == 28510),
        pigsy = (false and false or true and not false and not false and
            not false and not false and false or not false and true and true),
        yangjian = (333 - 156 + 150 * 363 == 54627),
        myth_yutu = (false and true and true or true and true and not false or
            true or not false and false and not false and not true),
        yama_commissioners = (389 + 44 * 176 - 175 + 262 ~= 8226),
        madameweb_rat = (317 - 221 - 72 ~= 32)
    }
end
local function AW_S__L(__a__ws__L_) return __aW__S__l()[__a__ws__L_] end
env["isskin"] = function(...) return AW_S__L(...) end;
local function __a__w__S__l_(_A__Ws__l)
    for Aw_sL in env["pairs"](a_W_s_L__()) do
        if _A__Ws__l == Aw_sL .. "_none" then
            return (264 - 50 * 20 == -736)
        end
    end
end
env["isskinnone"] = function(...) return __a__w__S__l_(...) end;
local function _A_ws__l(__aW_S_l_) return __aW__S__l()[env["skin"]] end
local function __a_w__s__L(_a_w__Sl__) return a_W_s_L__()[_a_w__Sl__] end
env["isskinchar"] = function(...) return __a_w__s__L(...) end;
local A__W_S_l__ = function(aw__S_L_)
    local A__WS__L__ = aw__S_L_["entity"]:GetDebugString()
    local AW_S__l = A__WS__L__:find('AnimState: ')
    local __a__w__s_l = A__WS__L__:sub(AW_S__l)
    local A_W_S__l__ = __a__w__s_l:find(' build: ')
    local _A_w__S__l_ = __a__w__s_l:find(' anim: ')
    local __a__WsL_ = __a__w__s_l:sub(A_W_S__l__ + 8, _A_w__S__l_ - 1)
    return __a__WsL_
end
do
    local __AW__sl_ = env["getmetatable"](env["TheInventory"])["__index"]
    local _a__w__sl = __AW__sl_["CheckOwnership"]
    __AW__sl_["CheckOwnership"] = function(__AW__sl_, __a_w_sl, ...)
        if env["type"](__a_w_sl) == 'string' then
            if AW_S__L(__a_w_sl) then return _a_w_Sl(__a_w_sl) end
        end
        return _a__w__sl(__AW__sl_, __a_w_sl, ...)
    end
    local __a__w_SL__ = __AW__sl_["CheckClientOwnership"]
    __AW__sl_["CheckClientOwnership"] =
        function(__AW__sl_, Aw__S__L, AW_s__l_, ...)
            if env["type"](AW_s__l_) == 'string' then
                if AW_S__L(AW_s__l_) then
                    return (42 - 291 * 284 * 21 * 132 == -229089126)
                end
            end
            return __a__w_SL__(__AW__sl_, Aw__S__L, AW_s__l_, ...)
        end;
    local __a_W__s__l = env["getmetatable"](env["TheNet"])["__index"]
    local __awS__l = __a_W__s__l["SendSpawnRequestToServer"]
    __a_W__s__l["SendSpawnRequestToServer"] =
        function(__a_W__s__l, __a_WS__l__, _a_w__s_l_, ...)
            if __a_w__s__L(__a_WS__l__) and _a_w__s_l_ ~= nil and
                _a_w_Sl(_a_w__s_l_) then A_W__sL = _a_w__s_l_ end
            return __awS__l(__a_W__s__l, __a_WS__l__, _a_w__s_l_, ...)
        end;
    local _A__WS_l__ = env["ValidateSpawnPrefabRequest"]
    env["GLOBAL"]["ValidateSpawnPrefabRequest"] =
        function(AW_S_l, AwSL__, _aW__sL, ...)
            local _A_W_S__L = {_A__WS_l__(AW_S_l, AwSL__, _aW__sL, ...)}
            if __a_w__s__L(AwSL__) and _a_w_Sl(_aW__sL) then
                _A_W_S__L[2] = _aW__sL
            end
            return env["unpack"](_A_W_S__L)
        end
    local A_W_S_L__ = env["AnimState"]["AddOverrideBuild"]
    env["AnimState"]["AddOverrideBuild"] =
        function(_Aws_l, __aWS_l, ...)
            if env["type"](__aWS_l) ~= 'string' then
                env["print"]('LUA ERROR stack traceback:' .. AW_Sl_ ..
                                 "bad argument #1 to 'AddOverrideBuild' (string expected, got " ..
                                 env["type"](__aWS_l) .. ")")
            end
            if _A_ws__l(__aWS_l) then return end
            return A_W_S_L__(_Aws_l, __aWS_l, ...)
        end
    local __aw__SL_ = env["AnimState"]["OverrideSymbol"]
    env["AnimState"]["OverrideSymbol"] =
        function(a__W__S__L__, aw__s_L_, aW__s_l__, _a_w__s__l, ...)
            if _A_ws__l(aW__s_l__) and aw__s_L_ ~= 'SWAP_ICON' then
                return
            end
            return __aw__SL_(a__W__S__L__, aw__s_L_, aW__s_l__, _a_w__s__l, ...)
        end;
    local a__w__s_L_ = env["AnimState"]["OverrideSkinSymbol"]
    env["AnimState"]["OverrideSkinSymbol"] =
        function(_a__W_s_l_, _AW_Sl__, a_W__sL__, __A__W__S__l__, _AW__S_L__,
                 _a__w_S__L__, ...)
            for aw__S_l__, a_w_s_l in env["ipairs"]({
                _AW_Sl__, a_W__sL__, __A__W__S__l__
            }) do
                if env["type"](a_w_s_l) ~= 'string' then
                    env["print"]('LUA ERROR stack traceback:' .. AW_Sl_ ..
                                     "bad argument #" .. aw__S_l__ ..
                                     " to 'OverrideSkinSymbol' (string expected, got " ..
                                     env["type"](env["p"]) .. ")")
                    break
                end
            end
            if env["type"](_AW__S_L__) ~= "number" and env["type"](_AW__S_L__) ~=
                "nil" then
                env["print"]("LUA ERROR stack traceback:" .. AW_Sl_ ..
                                 "bad argument #3 to 'OverrideSkinSymbol' (number expected, got " ..
                                 env["type"](env["p"]) .. ")")
            end
            if _A_ws__l(a_W__sL__) and _AW_Sl__:upper() ~= 'SWAP_ICON' then
                return
            end
            return a__w__s_L_(_a__W_s_l_, _AW_Sl__, a_W__sL__, __A__W__S__l__,
                              _AW__S_L__, _a__w_S__L__, ...)
        end;
    local _A_w__sl__ = env["AnimState"]["SetClientsideBuildOverride"]
    env["AnimState"]["SetClientsideBuildOverride"] =
        function(__Aw__s_L__, _a__ws__L, _a__WSl__, A__Ws__L, ...)
            if _A_ws__l(_a__WSl__) or _A_ws__l(A__Ws__L) then return end
            return _A_w__sl__(__Aw__s_L__, _a__ws__L, _a__WSl__, A__Ws__L, ...)
        end
end
local __A_wSl_ = function(...) end;
local function _A__W_S__L_(aw__S_L_)
    return {
        aw__S_L_["AnimState"]:GetBuild(), aw__S_L_["AnimState"]:GetSkinBuild(),
        A__W_S_l__(aw__S_L_)
    }
end
local function aWSl__()
    for __AWs__l_, __A_w_s__L in env["pairs"](env["AllPlayers"]) do
        local A_W__sl_ = (490 - 494 + 150 ~= 146)
        if not __A_w_s__L:IsValid() or not __A_w_s__L["userid"] then
            A_W__sl_ = (422 * 103 * 31 ~= 1347448)
        else
            local awSl = _a_W__S__l(__A_w_s__L["userid"])
            local aw_s__L = Aw__s_L[awSl]
            if #awSl > 0 and not aw_s__L then
                AW_Sl__()
            else
                for __AWs__l_, __Aw__sl__ in env["pairs"](
                                                 _A__W_S__L_(__A_w_s__L)) do
                    if _A_ws__l(__Aw__sl__) then
                        if aw_s__L then
                            if not env["table"]["contains"](aw_s__L, __Aw__sl__) then
                                A_W__sl_ =
                                    (false and false and false and not true and
                                        not true and false and false and
                                        not false and false and true and true or
                                        false and true or not false)
                                break
                            end
                        end
                        if #awSl == 0 then
                            A_W__sl_ = (318 * 441 + 328 ~= 140568)
                            break
                        end
                    end
                end
            end
        end
        if A_W__sl_ then
            __A_w_s__L["AnimState"]:SetBuild(__A_w_s__L["prefab"])
            if __A_w_s__L["components"]["skinner"] then
                __A_w_s__L["components"]["skinner"]["skin_name"] =
                    __A_w_s__L["prefab"] .. "_none"
            end
        end
    end
end
env["AddPrefabPostInit"]('world', function(aw__S_L_)
    aw__S_L_:DoPeriodicTask(env["GetRandomMinMax"](4, 8), aWSl__)
    aw__S_L_:ListenForEvent("ms_playerjoined", AW_Sl__)
end)
env["AddPlayerPostInit"](function(aw__S_L_)
    aw__S_L_:DoTaskInTime(3, function(aw__S_L_)
        if aw__S_L_ == env["ThePlayer"] then
            aw__S_L_:DoPeriodicTask(env["GetRandomMinMax"](4, 8), aWSl__)
            aw__S_L_:DoPeriodicTask(300, AW_Sl__, 0)
        end
    end)
end)
function _a__W__S__L__()
    local a_w_S__l_ = AW_Sl_ ..
                          '--è¯·å¿ä¿®æ¹è´´å¾æä»¶!--' ..
                          AW_Sl_
    local __a__w_s__l_ = env["GetRandomMinMax"](0.1, 1)
    local __a__wSl__ = function()
        return env["string"]["char"](env["math"]["random"](97, 97 + 25))
    end;
    local a__w_s_l_ = 'TheWorld:DoTaskInTime(' .. __a__w_s__l_ ..
                          ', function() %s.%s.%s.%s.%s = %s end)'
    local __AwS__L_ = a_w_S__l_ ..
                          a__w_s_l_:format(__a__wSl__(), __a__wSl__(),
                                           __a__wSl__(), __a__wSl__(),
                                           __a__wSl__(), __a__wSl__())
    local __Aw__S__l_ = env["loadstring"](__AwS__L_)
    env["setfenv"](__Aw__S__l_, env)
    env["pcall"](__Aw__S__l_)
end
env["table"]["insert"](env["Assets"], env["Asset"]("ATLAS",
                                                   'images/hud/myth_skin_unlock_dst.xml'))
env["table"]["insert"](env["PrefabFiles"], 'myth_skins_prefab')
local function Aw__sL__()
    env["print"]("check!!!!!!!!")
    local Aw__S__L__ = env["CreateEntity"]()
    Aw__S__L__["entity"]:AddTransform()
    local __A_W_s__L = Aw__S__L__["entity"]:AddAnimState()
    __A_W_s__L:SetBuild('monkey_king_horse')
    for A_ws__l_, _A_w_S_l in env["ipairs"]({
        'arm_lower', 'hair', 'hand', 'skirt', 'torso', 'hair_hat', 'leg',
        'headbase_hat', 'hairfront', 'tail', 'droplet', 'arm_upper_skin',
        'arm_upper', 'hairpigtails', 'headbase', 'cheeks', 'face',
        'wake_paddle', 'foot', 'torso_pelvis', 'lens_eye', 'paddle', 'SWAP_ICON'
    }) do
        env["print"]("has symbol " .. _A_w_S_l .. " ?")
        env["print"](__A_W_s__L:BuildHasSymbol(_A_w_S_l))
    end
end
local function _aws__l__(_AW__SL, _A__W_sL)
    local __A_w_S_l_ = _AW__SL["prefab"]
    if __a_w__s__L(__A_w_S_l_) and _A__W_sL["components"]["myth_itemskin"] and
        __A_w_S_l_ == _A__W_sL["components"]["myth_itemskin"]["character"] then
        local __A_w_S__l = _AW__SL["AnimState"]:GetBuild()
        if __A_w_S__l == "white_bone_opera_beautiful" then
            __A_w_S__l = "white_bone_opera"
        elseif __A_w_S__l == "yama_commissioners_lotus_black" then
            __A_w_S__l = "yama_commissioners_lotus"
        end
        local a__w_sl_ = __A_w_S__l == __A_w_S_l_ and "none" or
                             env["string"]["sub"](__A_w_S__l, #__A_w_S_l_ + 2)
        if a__w_sl_ ~= _A__W_sL["components"]["myth_itemskin"]["skin"]:value() then
            return a__w_sl_
        end
    end
end
local a_w__S_L_ = env["Action"]({priority = 1})
a_w__S_L_["id"] = 'MYTH_ITEMSKIN'
a_w__S_L_["str"] = env["L"] and "Decorate" or "è£é¥°"
a_w__S_L_["fn"] = function(__A_Ws__L__)
    local __a_WS__l, __A__W__s__L_ = __A_Ws__L__["doer"], __A_Ws__L__["target"]
    local __A__WsL = _aws__l__(__a_WS__l, __A__W__s__L_)
    if __A__WsL then
        __A__W__s__L_["components"]["myth_itemskin"]:ChangeSkin(__A__WsL)
        env["SpawnPrefab"]("collapse_small")["Transform"]:SetPosition(
            __A__W__s__L_:GetPosition():Get())
        return (290 - 469 - 96 == -275)
    end
end;
env["AddAction"](a_w__S_L_)
env["AddComponentAction"]("SCENE", "myth_itemskin",
                          function(aw__S_L_, _AwsL__, __a_w__S__L, _A__w__S_l)
    if _A__w__S_l and _aws__l__(_AwsL__, aw__S_L_) then
        env["table"]["insert"](__a_w__S__L, a_w__S_L_)
    end
end)
env["AddStategraphActionHandler"]("wilson",
                                  env["ActionHandler"](a_w__S_L_, "give"))
env["AddStategraphActionHandler"]("wilson_client",
                                  env["ActionHandler"](a_w__S_L_, "give"))
local function __a_W_SL_(AW_S_l_, A_w__S_l_)
    local __AWs_L__ = AW_S_l_ and AW_S_l_["userid"] and
                          _a_W__S__l(AW_S_l_["userid"])
    if #__AWs_L__ > 0 then
        local _aw__s_l__ = Aw__s_L[__AWs_L__]
        if not _aw__s_l__ then
            AW_Sl__()
            return
        end
        local __aW__S_L_ = {"none"}
        for _A_W_s__l__, _A__Wsl__ in env["pairs"](_aw__s_l__) do
            if env["string"]["find"](_A__Wsl__, A_w__S_l_) then
                env["table"]["insert"](__aW__S_L_, env["string"]["sub"](
                                           _A__Wsl__, #A_w__S_l_ + 2, #_A__Wsl__))
            end
        end
        return __aW__S_L_
    end
end
env["AddPrefabPostInit"]("reskin_tool", function(aw__S_L_)
    if aw__S_L_["components"]["spellcaster"] then
        local Aw_s__l__ = aw__S_L_["components"]["spellcaster"]["can_cast_fn"]
        local _a__W_S__L = function(Aw__sL_, a_WsL, ...)
            if a_WsL and a_WsL["components"]["myth_itemskin"] then
                return
                    (false or not false or false or not false and not true and
                        true and not false and not false and true and false)
            else
                return Aw_s__l__ and Aw_s__l__(Aw__sL_, a_WsL, ...)
            end
        end;
        aw__S_L_["components"]["spellcaster"]:SetCanCastFn(_a__W_S__L)
        local _aWs_l_ = aw__S_L_["components"]["spellcaster"]["spell"]
        local __aW__SL = function(__a_ws_l_, __AwS__l, ...)
            if __AwS__l and __AwS__l["components"]["myth_itemskin"] then
                local __a_w__s__L_ =
                    __AwS__l["components"]["myth_itemskin"]["character"]
                local __A__W_S_l_ = __a_W_SL_(__a_ws_l_["parent"], __a_w__s__L_)
                local __aWs__L__ = env["SpawnPrefab"]("explode_reskin")
                __aWs__L__["Transform"]:SetPosition(
                    __AwS__l["Transform"]:GetWorldPosition())
                __AwS__l["components"]["myth_itemskin"]:RoundSkin(__A__W_S_l_)
            else
                return _aWs_l_(__a_ws_l_, __AwS__l, ...)
            end
        end;
        aw__S_L_["components"]["spellcaster"]:SetSpellFn(__aW__SL)
    end
end)
env["GLOBAL"]["Myth_UseTime"] = function(AW__S_L, a__w__S__l)
    local a__W__s__L = env["string"]["format"](
                           _A_w__s_l .. "usetime/dst/%s/%s/", _a_W__S__l(),
                           AW__S_L)
    env["TheSim"]:QueryServer(a__W__s__L, function(__a_W_S__l__)
        if __a_W_S__l__ then
            if #__a_W_S__l__ > 20 then
                a__w__S__l(__a_W_S__l__:gsub("\n", " "))
            else
                a__w__S__l(__a_W_S__l__)
            end
        end
    end)
end;
env["GLOBAL"]["Myth_Purchase"] = function(a__w_S_L_, __Aw_S__l_)
    env["TheSim"]:QueryServer(_A_w__s_l .. "wxpay_qrcode_buy/",
                              function(__Aw_S_l__)
        if __Aw_S_l__ then
            local a__ws__L, a__ws__L, _a_w__s__L_ =
                env["string"]["find"](__Aw_S_l__, "<LW>(.+)</LW>")
            if _a_w__s__L_ then
                __Aw_S__l_(env["json"]["decode"](_a_w__s__L_))
            elseif #__Aw_S_l__ > 20 then
                __Aw_S__l_("SERVER ERROR")
            else
                __Aw_S__l_(__Aw_S_l__)
            end
        end
    end, "POST",
                              env["json"]["encode"](
                                  {player = _a_W__S__l(), skin = a__w_S_L_}))
end
env["GLOBAL"]["Myth_CDK"] = function(__a_Ws_L_, _A__w_s_l_, __a_w_S_l_)
    local _Aws_L = env["string"]["format"](
                       _A_w__s_l .. 'code/use/dst/%s/%s/%s/', _a_W__S__l(),
                       __a_Ws_L_, _A__w_s_l_)
    env["TheSim"]:QueryServer(_Aws_L,
                              function(__A__wS__l_) __a_w_S_l_(__A__wS__l_) end)
end;
env["GLOBAL"]["Myth_PushSkinScreen"] = function(__AWsl__, _a_w_S_L__)
    local AWS_L = env["require"] "screens/thankyoupopup"
    env["Mythwords_ForceSync"]()
    env["TheFrontEnd"]:PushScreen(AWS_L({
        {item = __AWsl__, item_id = 0, gifttype = "MYTH_SKINITEM"}
    }))
    env["TheFrontEnd"]:GetActiveScreen()["inst"]:DoTaskInTime(0, function()
        for A_W_s__L, __A_W_S__L in env["ipairs"](_a_w_S_L__) do
            env["TheFrontEnd"]:PopScreen(__A_W_S__L)
        end
    end)
end;
local function __Aw_S_l_(aw__S_L_, A_w__S_L_)
    if env["type"](aw__S_L_["prefab"]) == "string" and env["type"](A_w__S_L_) ==
        "string" and A_w__S_L_:sub(1, #aw__S_L_["prefab"]) ~= aw__S_L_["prefab"] then
        return
    end
    if env["TheWorld"]["ismastersim"] then
        aw__S_L_["components"]["skinner"]:SetSkinName(A_w__S_L_)
    else
        env["SendModRPCToServer"](
            env["MOD_RPC"]["mythSkin"]["ChangeModdedSkin"], A_w__S_L_)
    end
end
local function _A__wS_l_(aw__S_L_)
    if aw__S_L_ == env["ThePlayer"] and A_W__sL ~= nil then
        __Aw_S_l_(aw__S_L_, A_W__sL)
        A_W__sL = nil
    end
end
env["AddPlayerPostInit"](function(aw__S_L_)
    aw__S_L_:ListenForEvent("playeractivated", _A__wS_l_)
end)
env["AddModRPCHandler"]("mythSkin", "ChangeModdedSkin", __Aw_S_l_)
