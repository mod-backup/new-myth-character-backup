
local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()

    inst:AddTag("NOBLOCK")

	inst:AddTag("NOBLOCK")
	inst:AddComponent('sanityaura')
	inst.components.sanityaura.GetAura = function(self, ob, ...)
        return - 6/60
	end

    inst.persists = false
    
    return inst
end


local function ghostfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()

    inst:AddTag("NOBLOCK")

	inst:AddTag("NOBLOCK")
	inst:AddComponent('sanityaura')
	inst.components.sanityaura.GetAura = function(self, ob, ...)
        if inst.player ~= nil and inst.player == ob then
            return TUNING.SANITY_GHOST_PLAYER_DRAIN
        end
        return 0
	end

    inst.persists = false
    
    return inst
end


return Prefab("myth_astralsanity", fn),
    Prefab("myth_ghostedsanity", ghostfn)
