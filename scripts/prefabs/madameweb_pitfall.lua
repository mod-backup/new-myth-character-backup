local assets = {
    Asset("ANIM", "anim/madameweb_pitfall.zip"),
    Asset("ATLAS", "images/inventoryimages/madameweb_pitfall.xml"),
    Asset("ATLAS_BUILD", "images/inventoryimages/madameweb_pitfall.xml", 256),
}

local function ondeploy(inst, pt)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
    local tree = SpawnPrefab("madameweb_pitfall_ground")
    if tree ~= nil then
        tree.Transform:SetPosition(pt:Get())
        inst.components.stackable:Get():Remove()
    end
end

local function onpickup(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("madameweb_pitfall")
    inst.AnimState:SetBuild("madameweb_pitfall")
    inst.AnimState:PlayAnimation("net")

    inst:AddTag("cattoy")
    inst:AddTag("silkcontaineritem")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_LARGEITEM

    inst:AddComponent("inspectable")

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

    MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
    MakeSmallPropagator(inst)
    MakeHauntableLaunchAndIgnite(inst)

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/madameweb_pitfall.xml"
    inst.components.inventoryitem:SetOnPickupFn(onpickup)

    inst:AddComponent("tradable")

    inst:AddComponent("deployable")
    inst.components.deployable.ondeploy = ondeploy

    return inst
end

return Prefab("madameweb_pitfall", fn, assets),
MakePlacer("madameweb_pitfall_placer", "madameweb_pitfall", "madameweb_pitfall", "idle",true)